/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PV_SURFACEPNL_H__
#define __BP_PV_SURFACEPNL_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "wx.h"
#include <wx/event.h>
#include "datatype.h"

#define CHECK_BOX_RADIUS 15
#define BOX_FOCUS_OFFSET 1

class SurfaceColumnItem;

class SurfaceDrawUtil
{
public:
        static void DrawBitmap(wxDC &dc,const wxRect& rect,wxString picture);
        static void DrawBackground(wxDC& dc,const wxSize& size);
        static void DrawSurfaceColumnSeperator(wxDC& dc,const wxRect& rect);
        static void DrawSurfaceColumnFooter(wxDC& dc,const wxRect& rect,const wxString& footer);
        static void DrawSurfaceColumnHeader(wxDC& dc,const wxRect& rect,const wxString& header);
        static void DrawSurfaceColumnItem(wxDC& dc,const SurfaceColumnItem* const item);
        static void DrawFocusRect(wxDC& dc,const wxRect& rect);
        static void DrawString(wxDC &dc,const wxRect& rect,const wxString& string);
};
class SurfaceColumnItem
{
public:
        SurfaceColumnItem();
        bool HitTest(int x,int y);
        bool HitLBoxTest(int x,int y);
        bool HitRBoxTest(int x,int y);
        bool HitBoxTest(int x,int y);
        bool HitCheckTest(int x,int y);
        int GetWidth(void) const;
        int GetHeight(void) const;
        wxRect GetDrawAreaRect(void) const;
        wxRect GetBoxRect(void) const;
        wxRect GetLBoxRect(void) const;
        wxRect GetRBoxRect(void) const;
        wxRect GetLStringRect(void) const;
        wxRect GetRStringRect(void) const;
        wxRect GetFocusBoxRect(void) const;
        wxRect GetLFocusBoxRect(void) const;
        wxRect GetRFocusBoxRect(void) const;
        wxRect GetLDrawLineRect(void) const;
        wxRect GetRDrawLineRect(void) const;
        wxPoint GetCheckCenter(void) const;
        wxRect GetCheckRect(void) const;
        wxString GetLString(void) const;
        wxString GetRString(void) const;
        wxRect GetSeparatorRect(void) const;
public:
        static bool IsInRect(int x,int y,const wxRect rect);
public:
        //data
        int Index;
        double Energy;
        wxString Type;
        wxString leftStr;
        wxString rightStr;
        bool HasLeft;
        bool HasRight;
        bool HasMO;
        //gui
        int Top,Left,Right,Bottom;
        bool IsCheck;
        bool IsSelect;
        bool IsLSelect;
        bool IsRSelect;
        bool IsLFocus;
        bool IsRFocus;
        bool Separator;
};
//class SurfaceColumnItemArray;
WX_DECLARE_OBJARRAY(SurfaceColumnItem, SurfaceColumnItemArray);

class SurfaceColumn
{
public:
        SurfaceColumn()
        {
                ColumnHeader=wxEmptyString;
                ColumnFooter=wxEmptyString;
        }
        wxString ColumnHeader;
        wxString ColumnFooter;
        SurfaceColumnItemArray Column;
};
WX_DECLARE_OBJARRAY(SurfaceColumn, SurfaceColumnArray);

class wxSelChangeEvent : public wxCommandEvent
{
public:
        wxSelChangeEvent(const wxSelChangeEvent& event);
        wxSelChangeEvent(wxEventType commandType=wxEVT_NULL, int id=0);
        virtual ~wxSelChangeEvent();
        virtual wxEvent* Clone()const;

        void SetSelect(const wxIntArray& sel);
        void SetSelect(const wxIntArray& oldSel,const wxIntArray& newSel);
        wxIntArray GetNewSelect(void);
        wxIntArray GetLastSelect(void);
private:
         static wxIntArray m_lastSel,m_newSel;
private:
    DECLARE_DYNAMIC_CLASS(wxSelChangeEvent)
};
typedef void (wxEvtHandler::*wxSelChangeEventFunction)(wxSelChangeEvent&);
BEGIN_DECLARE_EVENT_TYPES()
        DECLARE_EVENT_TYPE(wxEVT_COMMAND_SEL_CHANGED,wxID_HIGHEST+3)
END_DECLARE_EVENT_TYPES()

#define EVT_COMMAND_SEL_CHANGED(id, fn)     DECLARE_EVENT_TABLE_ENTRY( wxEVT_COMMAND_SEL_CHANGED, id, wxID_ANY,          (wxObjectEventFunction) (wxEventFunction)  wxStaticCastEvent( wxSelChangeEventFunction, & fn ), (wxObject *) NULL ),

class SurfaceWindow :public wxScrolledWindow
{
        DECLARE_EVENT_TABLE();
public:
        static void SetColumnPositions(SurfaceColumnArray& array,int& width,int& height);
public:
        void ClearSurfaceData(void);
        void SetSelectedRowCol(int row,int col);
        void GetSelectedRowCol(int &row,int& col);
        void ResetDrawSize(void);
        void AppendColumns(const wxArrayString& gridInfo);
        void DumpMOSData(void);
        SurfaceColumnArray GetColumnData(void);
        void AppendColumns(SurfaceColumnArray columns);
        SurfaceWindow(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size =wxDefaultSize, long style = wxTAB_TRAVERSAL|wxVSCROLL|wxHSCROLL/*|wxScrolledWindowStyle*/  ,
                        const wxString& name = wxT("SurfaceWindow"));
        virtual ~SurfaceWindow();
private:
        SurfaceColumnArray columnArray;
        int drawWidth,drawHeight;
        int lineCount,lineLength;
        int selectedRow,selectedCol;
        bool IsPainting;
        wxBitmap* pBitmap;
private:
        void DrawOnBitmap(void);
        void ConvertStringToValue(wxString src,long& group,long& index,double& energy,bool& hasLeft,bool& hasRight,wxString& flag);
        SurfaceColumnItem ConvertStringToSCItem(wxString src);
        void SortColumnByEnergy(void);
        void GetCheckedRowCol(int& row,int& col);
        void ScreenToLogicalCoords(long &posX, long &posY);
        void ScreenToLogicalCoords(int& posX,int& posY);
        wxPoint GetStartPos(void);
        wxPoint ScreenToLogicalCoords(wxPoint pos)const;
        void SetScrollBars(void);
        bool CheckBoxSelect(int x,int y,bool IsMouseDown);
        bool CheckRBoxSelect(int x,int y,bool IsMouseDown);
        bool CheckLBoxSelect(int x,int y,bool IsMouseDown);
        bool CheckRFocus(int x,int y);
        bool CheckLFocus(int x,int y);
        bool CheckFocus(int x,int y);
        void CheckChecked(int x,int y);
        bool CheckSelect(int x,int y);
        void OnMouseMove(wxMouseEvent &event);
        void OnLeftMouseUp(wxMouseEvent &event);
        void OnLeftMouseDown(wxMouseEvent &event);
        void OnSize(wxSizeEvent &event);
        void OnPaint(wxPaintEvent& event);
        void OnScroll(wxScrollWinEvent& event);
        void Draw(wxDC& dc);
protected:
};

class SurfacePnl : public wxPanel
{
    DECLARE_EVENT_TABLE()
public:
        SurfacePnl(        wxWindow* parent,
                                wxWindowID id = wxID_ANY,
                                const wxPoint& pos = wxDefaultPosition,
                                const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30),
                                long style = wxTAB_TRAVERSAL,
                                const wxString& name = wxT("panelSurface"));
        virtual ~SurfacePnl();
        void SetSelectRow(int row);
        void AppendDenFile(wxArrayString denFileList);

        void ClearData(void);
private:
        SurfaceWindow* m_surfaceWin;
        SurfaceColumnArray m_dataSCA;
        wxTextCtrl* textCtrl;
        wxChoice* choiceMOS;
        wxChoice* choiceSet;
        bool IsInitData;
        //wxString controlDataFileName;
        //wxArrayString gridInfo;
        wxArrayString surfaceFileList;
        int surfaceStyle;
private:
        void SetMoSetChoices(int numOfChoice);
        void OnSelChange(wxSelChangeEvent &event);
        void OnChoiceMOSet(wxCommandEvent &event);
        void OnChoiceStyle(wxCommandEvent &event);
        void CreateGUIControls(void);
    int curmo;
};

#endif // !defined(__SURFACE_PANEL_H__)
