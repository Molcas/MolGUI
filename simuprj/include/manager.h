/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_MANAGER_H__
#define __BP_PRJ_MANAGER_H__

#include "wx.h"
#include "genericdlg.h"
#include "jobmonitorpnl.h"
#include "projectmanager.h"
#include "viewmanager.h"
class ConfigManager;
class ProjectManager;
class ViewManager;
//class LogManager;

class Manager : public wxEvtHandler {
public:
        static Manager* Get(void);
        static void Free(void);
        static void Shutdown(void);
        static bool IsAppShuttingDown(void);

        wxFrame* GetAppFrame(void) const;
    wxWindow* GetAppWindow(void) const;
    void SetAppFrame(wxFrame* frame);

        ConfigManager* GetConfigManager(void) const;
        ProjectManager* GetProjectManager(void) const;
        ViewManager* GetViewManager(void) const;

        GenericDlg* GetMonitorDlg(void);
        JobMonitorPnl* GetMonitorPnl(void);

private:
        Manager();
    ~Manager();

private:
        static bool m_appShuttingDown;
        wxFrame* m_pAppFrame;
        GenericDlg* m_pDlgMonitor;
        JobMonitorPnl* m_pPnlMonitor;
};

#endif
