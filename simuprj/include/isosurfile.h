/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PV_ISOGRID_H__
#define __BP_PV_ISOGRID_H__

#include "wx.h"
#include <wx/glcanvas.h>
#include <wx/gdicmn.h>
#ifdef _DARWIN_
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif
#include <stdio.h>
#include "wx/ffile.h"
#define MAXSTRTITLE 32                /* fixed value. don't touch! */

#define ExtraAtoms 256

typedef GLdouble point_t[3];
typedef GLfloat color_t[4];
typedef GLfloat color3_t[3];

#define MAXFILES 10
#define MAXPATH 256


#define MAX_STATUS_SIZE (0x400)

class Grid_t
{
public:
        Grid_t(){}
        ~Grid_t()
        {
                if(ByteCutOff)
                        delete[] ByteCutOff;
                if(pos)
                        delete[] pos;
                if(pos2)
                        delete[] pos2;
                if(dependence)
                        delete[] dependence;
                if(iType)
                        delete[] iType;
                if(values)
                        delete[] values;
        }
  int ngrid;
  int ngrid2;
  int nmo;
  int nmo2;
  int npoints;
  int npt[3];
  int block_size;
  int nBlock;
  //gv7 begin
  int ptotal;
  int isCutOff;
  double CutOff;
  int nPointsCutOff;
  int *ByteCutOff;
  //gv7 end
  long *pos;
  long *pos2;
  int *dependence;
  double len[3];
  double origin[3];
  double axisvec1[3], axisvec2[3], axisvec3[3];
  wxString title;
  char *iType;
  char Gtitle[81];
  double minval, maxval, guess;
  double currentvalue;
  double isod[21];
  double *values;
  int sel[4];
  int is_coord;
};

void iso_InitGridInformation (Grid_t *currentGridData);
int iso_LoadGridHeader (wxString filepath, int is_primary, int is_coord, float translate, int *isFreq,Grid_t * currentGridData);
int iso_LoadGrid (wxString filepath,int Current_Grid, int is_diff, int, double,int, int,Grid_t * currentGridData);
//int init_GridTitles (char *first);
char *iso_CurrentGridName (int is_primary);
int iso_CurrentGridSize (int *a, int *b, int *c);
int iso_CurrentGridLimits (double *dmin, double *dmax, double *guess,Grid_t * currentGridData);
int iso_CurrentGridAxesVector (double **xvec, double **yvec, double **zvec,Grid_t *currentGridData);
int iso_CurrentGridData (double **data,Grid_t *currentGridData);
int iso_CurrentGridOrigin (double *origina, double *originb, double *originc,Grid_t *currentGridData);

int what_is (char *this_, char *, int *is);
int InitGridListBox (char *ListTitles, char *firstTitle);
int iso_current_file (void);
/* int set_status (int, int);  */
int here_is (int n, int *, double *, int *, char *, char *);
int keep_this (int model, int, double, int);

void read_error (void);
void read2_error (void);
int show_coord (int show, int now);
int export_grd (char *filename);
int read_bin_stream (char *str, int max, wxFFile * fp);
int abfgets (int isBinary, char *str, int max, wxFFile * fp);
int fgetline (void *str, int max, wxFFile * fp);

int iso_Dependence (char *);
/* char *aListTitles (int); */
//int showORB(void);
int diff_grid(char *, char *, double);

#ifndef NO_C2_QUIRKS
#define NO_C2_QUIRKS

/*
static int Rescale;

static char ListTitles[2048];
*/
#endif
/* Info about molecule and grids */
typedef struct grid_info_st {
 int natoms;
 int natomsReal;
  double (*xyz)[3];
  char (*atomNames)[4];
  int *atomNums;
  int *outsym; //gv7
 int grd_sz[3];
 int ngrids;
 wxString titles;
 const double *axis1;
 const double *axis2;
 const double *axis3;
 double origin[3];
 int sel[4];
 int is_coord;

} grid_info_t;

int iso_GetInfo(struct grid_info_st* iptr,Grid_t *currentGridData);

typedef struct triangle_t_s {
  point_t vertex[3];
  point_t normal[3];
  double distance;
  unsigned char clr_idx; /* color index: 0, 1 */
  struct triangle_t_s* lnk_prev; /* link field for linked list */
} triangle_t;

struct multisrf_t_s;

typedef struct {
  triangle_t* head; /* list head */
  int n_triangles;

  point_t max_coor,min_coor; /* bounding box: for max. diameter calc. */

  struct multisrf_t_s* parent; /* parent msrf structure */
  int offset; /* index in parent */
  int is_valid;
  int is_sorted;
} surface_t;

typedef struct multisrf_t_s {
  triangle_t** index; /* array of pointers to triangles */
  int n_triangles; /* total number of triangles */

  surface_t** child; /* array of child surfaces */
  int n_surfaces; /* max. number of surfaces */
  int curmo;
} multisrf_t;


#define msrf_Set_Unsorted(self) ((self)->is_sorted=0)

multisrf_t* msrf_Init(multisrf_t*, int n_srf);
void msrf_Clean(multisrf_t*);

surface_t* msrf_New_Surface(multisrf_t*,int);
void msrf_Delete_Surface(surface_t*);

/* add triangle with normals */
triangle_t* srf_Add_Triangle(surface_t*,
                             double txyz[3][3],
                             double tgrd[3][3],
                             unsigned char clridx);

void Color_Copy(color_t to, const color_t from);
/* draw all surfaces in current OpenGL context */
void Make_Index(multisrf_t* );
void Sort_by_Depth(multisrf_t* );
void msrf_Draw(multisrf_t*, color_t[],int);

/* Calculate the max. diameter of the surfaces */
double msrf_Diameter(multisrf_t*);
typedef struct {
 /* MO data */
 int mo;
 double lev;

 /* Stepping */
 double lev_step; /* recalculated on each grid loading if n_lev_steps!=0 */
 double lev_min,lev_max;
 int n_lev_steps;
 int auto_lev; /* calculate the initial isolevel automatically */
 double translate;
 double translate_step;

 int isFreq;
 /* List of files */
wxString infile_mask;
 int cur_file;

 char myflist[MAXPATH][MAXFILES];
 /* GUI data -- colors */
 GLfloat transp;
 color_t neg_pos_color[2];
 color_t bgcolor;

 /* MO shown */
 surface_t** surf; /* dynamically allocated array */
 int cur_mo;
 int shown_mo;

 /* Other options and flags */
 int lock_scale;
 int hide_atoms;
 int hide_bonds;
 int show_axes;
 int hide_status;
 int anim;
 int move_light;
 int coord_input;
 /* Obsoleted, to be removed in future */
 char* outfile;
 int molden_flag;

 /* FIXME: 'Status' variables not belonging strictly to input */
 GLdouble max_diam; /* max. diameter of the system */
 int max_diam_ok;  /* diameter is recalculated and valid */
 int fullscreen;
 int atomshape;
 int style_atoms;
 int label_atoms;
 int labelg_atoms;
 int labeln_atoms;

// atom_data_t* atoms; /* array of atoms of the molecule (except coors) */
// list_t* bond_list;  /* head of list of bonds */
} input_data_t;
//void Do_LoadGridHeader(input_data_t &Input_Data,grid_info_t &Grid_info);
//void Do_LoadGrid(int mo, wxFFile* outf,input_data_t &Input_Data,grid_info_t &Grid_info,multisrf_t* All_Surfaces);
//void Do_RemoveGrid(int mo,input_data_t &Input_Data,grid_info_t &Grid_info,multisrf_t* All_Surfaces);
//void Do_ChangeIso(input_data_t &Input_Data,grid_info_t &Grid_info,multisrf_t* All_Surfaces);
//void Do_GridStep(int step,input_data_t &Input_Data,grid_info_t &Grid_info,multisrf_t* All_Surfaces);
//void Do_LoadFile(input_data_t &Input_Data,grid_info_t &Grid_info,multisrf_t* All_Surfaces);

#endif
