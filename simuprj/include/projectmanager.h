/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#ifndef __BP_PRJ_PROJECT_MANAGER_H__
#define __BP_PRJ_PROJECT_MANAGER_H__

#include "wx.h"

#include <wx/imaglist.h>

#include "simuprjdef.h"
#include "simuproject.h"
#include "execprocess.h"
#include "projectgroup.h"

extern int itemType;

//WX_DEFINE_ARRAY(SimuProject*, ProjectsArray);
WX_DEFINE_ARRAY(ProjectGroup *, PrjGrpArray);
/** ***********************************************************************
 * The entry point singleton for working with projects.
 * As all managers, this is a singleton class which you retrieve by asking
 * the global Manager singleton.
 * E.g. Manager::Get()->GetProjectManager()
 **************************************************************************
 * This class is designed to manage the projects structure opened in the
 * programme. It mainly deal with the infomation about the files of each
 * project and manage the tree structure in the left panel of the programme.
 */

class ProjectManager : public wxEvtHandler {
public:
    static ProjectManager *Get(void);
    static void Free(void);
public:
    void    AddProject(SimuProject *pProject);
    void    AddPrjGrp(ProjectGroup *pPrjGrp);
    /**
         * Retrieve the active project.
         * @return A pointer to the active project.
         */
    SimuProject  *GetActiveProject(void) { return m_pActiveProject; }
    ProjectGroup *GetActiveProjectGroup(void)const;
    wxArrayString GetAllPGName(void)const;
    /**
     * Set the active project. Use this function if you want to change the active project.
     * @param project A pointer to the new active project.
     */
    void SetActiveProject(SimuProject *project);
    void SetActiveProjectGroup(ProjectGroup *pgroup);
    /**
     * Retrieve an array of all the opened projects.
     * This is a standard wxArray containing pointers to projects.
     * @return A pointer to the array containing the projects.
     */
    ProjectsArray *GetProjects(void) { return m_pProjects; }
    SimuProject *GetProject(int projectId) const;
    /**
     * Load a project from disk. This function, internally, uses IsOpen()
     * so that the same project cannot be loaded twice.
     * @param filename The project file's filename.
     * @param activateIt Active the project after loading.
     * @return If the function succeeds, a pointer to the newly opened project
     * is returned. Else the return value is NULL.
     */
    SimuProject         *LoadProject(const wxString &filename, bool activateIt = true);
    //        bool                                 LoadProject(SimuProject* project, bool activateIt = true);
    ProjectGroup  *LoadPrjGrp(const wxString &fileName, bool activateIt = true);
    wxBitmap             GetBitmap(ProjectTreeItemType itemType) const;
    /**
    * @return A pointer to the project manager's tree.
    */
    wxTreeCtrl                     *GetTree(void) { return m_pTree; }
    wxTreeItemId             GetTreeRoot(void) {return m_treeRootID;}
    /**
    * Check if a project is open based on the project's filename.
    * @param filename The project's filename. Must be an absolute path.
    * @return A pointer to the project if it is open, or NULL if it is not.
    */
    void                             *IsOpen(const wxString &filename, bool isPG = false);
    bool                     IsEmpty();
    SimuProject                 *GetSelectedProject(void);

    void         SaveProject(SimuProject *pProject);
    void     SaveAll();

    void         ModifyTree(wxString &nodepath);
    void     CloseProjectGroup(ProjectGroup *pg = NULL);
    void         CloseProject(SimuProject *project);

    wxString     SetJobName(ProjectTreeItemData *item);
    bool         SubmitJob(void);
    bool         SubmitJob(SimuProject *project, wxString jobName, bool newjob);
    void         DoJobTerminated(long processId, JobRunningData *pJobData, int status);
    void    DoRemoteJobTerminated(wxString projectFile, wxString jobPath, int status);
    void         ShowSelectedItemView(wxString importFileName);  //SONG change it from private to public workshop2009
    bool         CanUndo(); //SONG add it for test
    void   PrepareInputFile(wxString inputfile);

private:
    ProjectManager();
    ~ProjectManager();

    void         InitTree(void);

    DECLARE_EVENT_TABLE();
    void         OnBeginEditTreeItem(wxTreeEvent &event);
    void         OnEndEditTreeItem(wxTreeEvent &event);
    void         OnTreeItemActivated(wxTreeEvent &event);
    void         OnTreeItemSel(wxTreeEvent &event);
    void         OnTreeItemMenu(wxTreeEvent &event);
    void         OnRightClick(wxMouseEvent &event);
    void         OnImportMolecule(wxCommandEvent &event);
public:
    //        void     OnPopAdd(wxCommandEvent& event);
    void     OnPopCopy(wxCommandEvent &event);
    void     OnPopDelete(wxCommandEvent &event);
    void     DoPopDelete();
    void     OnPopPaste(wxCommandEvent &event);
    void     OnPopDispJobResult(wxCommandEvent &event);
    void     OnPopOpen(wxCommandEvent &event);
    void     OnPopReload(wxCommandEvent &event);
    void     OnPopRemove(wxCommandEvent &event);
    void     OnPopRename(wxCommandEvent &event);
    void     OnShowJobItem(wxCommandEvent &event);

    void        OnPegaOpen(wxCommandEvent& event);
    void        OnOthersoftOpen(wxCommandEvent& event);
    void        OnMoldenOpen(wxCommandEvent& event);

    void         OnRemoveProjectResult(wxCommandEvent &event);
    void         ShowPopupMenu(wxTreeItemId itemId, const wxPoint &pos);
    //-------------XIA--------
public:
    int GetProjectsArrayNum();//
public:
    wxTreeItemId        GetCopyNodeId();
    wxTreeItemId        GetCopyJobNodeId();
    wxTreeItemId        GetCopyJobItemNodeId();
    bool        IsCopyNodeIdOk();
    bool        IsCopyJobNodeIdOk();
    bool        IsCopyJobItemNodeIdOk();
    void        SetCopyNodeId(wxTreeItemId itemId);
    void        SetCopyJobNodeId(wxTreeItemId itemId);
    void        SetCopyJobItemNodeId(wxTreeItemId itemId);
private:
    PrjGrpArray        *m_pPrjGrps;
    ProjectGroup             *m_pActivePrjGrp;
    ProjectsArray             *m_pProjects;//isolate project
    SimuProject                 *m_pActiveProject;
    wxTreeCtrl                     *m_pTree;
    wxTreeItemId             m_treeRootID;
    wxTreeItemId        m_copyNodeID;
    wxTreeItemId        m_copyJobNodeID;
    wxTreeItemId        m_copyJobItemNodeID;
    wxImageList             *m_pImages;
    bool                                 m_isLoadingWorkspace;

};

#endif
