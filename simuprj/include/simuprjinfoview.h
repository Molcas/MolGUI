/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef _SIMU_PROJECT_INFO_BROWSE_H_
#define _SIMU_PROJECT_INFO_BROWSE_H_
#include <wx/html/htmlwin.h>
#include <wx/splitter.h>
#include <wx/combobox.h>
#include <wx/filefn.h>
#include <wx/gdicmn.h>
#include <wx/listctrl.h>
#include <wx/imaglist.h>
#include <wx/button.h>
#include <wx/dir.h>
#include <wx/filesys.h>
#include <wx/event.h>
#include <wx/filedlg.h>
#include <wx/imaglist.h>

#include "wx.h"
#include "abstractview.h"
#include "envutil.h"

#define SIMU_PRJINFO_VIEW_TITLE wxT("Projects Information")
#define SIMU_PRJINFO_VIEW_PATH  EnvUtil::GetBaseDir()+wxT("/Projects Information")
#define PRJINFO_COLNUM 4

#define wxLIST_INFO_FILE_PATH  wxT("/config/projectinfo")
#define wxLIST_INFO_FILE_ROOT  wxT("ProjectInfo")
#define wxLIST_INFO_FILE_SUFFIX  wxT(".xml")
#define LIST_INFOF_AR_ELEM  "List"
//*************************************************************
// this class is designed to show projects information
//*************************************************************
class SimuPrjInfoView : public AbstractView
{
private:
    enum {
        NAME = 0,
        TYPE,
        CDATE,
        LMDATE,
        DESCRIP,
        FULLPATH
    };
    //for items in combobox seartch type:
#define wxCOMBO_SCH_CURLIST wxT("Current List")
#define wxCOMBO_SCH_ALLLIST wxT("All Lists")
#define wxCOMBO_SCH_SELPATH wxT("Select Path")
public:
    SimuPrjInfoView(wxWindow *parent, wxEvtHandler *evtHandler);
    ~SimuPrjInfoView();
public:
    void    CreateUI();
    void    InitView();
    void    InitColumn();
    bool    Close();
public:
    bool    AddListColumn(wxString title);
    bool    DelListColumn(wxString title);
    bool    DoOpenFile(wxString pathinfo);
    bool    LinkClicked(const wxHtmlLinkInfo &link);
    bool    UpdateListTable();
    bool    UpdateList();
    bool    UpdateInfoHtml();
    bool    UpdateView();
private:
    void    SaveLCFile(wxString fileName, wxArrayString &filelist);
    void    AddToLCFile(wxString fileName, wxString &file);
    void    DelFromLCFile(wxString fileName, wxString &file);
private:
    wxArrayString AnaFindStr(wxString findStr);
    wxArrayString FindInList(wxArrayString &strList, wxString findStr);
    wxArrayString FindInCurList(wxString findStr);
    wxArrayString FindInAllList(wxString findStr);
    wxArrayString FindInSelPath(wxString dir, wxString findStr);
private:
    void        AddItems();//Add the item in the m_lcItem into the wxListCtrl component;
    wxString GetColText(wxString textval, int index);
    wxString GetProjectInfo(wxString fullPathName);//e.g: /home/project/mol/mol.sip
    wxString GetPrjGrpInfo(wxString fullPathName);
    wxString GetInfoFileFullName(wxString fileName);
    void        UpdateListFromPath();
    void        UpdateListFromFile(wxString fileFullPath);
    bool        UpdateHtmlLtInfo();
    bool        UpdateHtmlLcInfo();
    void        SortItem(int index);
private:
    DECLARE_EVENT_TABLE();

    void    OnActiveItem(wxListEvent &event);
    void    OnAddToList(wxCommandEvent &event);
    void    OnBeginEdLtItem(wxListEvent &event);
    void    OnCbSchType(wxCommandEvent &event);
    void    OnClkLcColum(wxListEvent &event);
    void    OnDelFromList(wxCommandEvent &event);
    void    OnDelLtList(wxCommandEvent &event);
    void    OnDbClkSash(wxSplitterEvent &event);
    void    OnEndEdLtItem(wxListEvent &event);
    void    OnExpToList(wxCommandEvent &event);
    void    OnTxtSearch(wxCommandEvent &event);
    void    OnNewList(wxCommandEvent &event);
    void    OnRClickColumn(wxListEvent &event);
    void    OnRClkItem(wxListEvent &event);
    void    OnRenameList(wxCommandEvent &event);
    void    OnShowLT(wxCommandEvent &event);
    void    OnSetPathForDisp(wxCommandEvent &event);
    void    OnBtnSearch(wxCommandEvent &event);
    void    OnSelectItem(wxListEvent &event);
    void    OnWndLtHtSizeChange(wxSplitterEvent &event);
    void    OnWndLtLcSizeChange(wxSplitterEvent &event);
    //.................................................................//
    void        OnResize(wxSizeEvent &event);
private:
    wxSize         m_clientSize;
    wxSplitterWindow *m_pSpWnd;
    wxButton   *m_pShowLt;
    wxButton   *m_pAddToList;
    bool             m_isShowLt;
    wxSplitterWindow *m_pSpWndLT;
    wxPanel     *m_pListPan;
    wxPanel     *m_pHtmlPan;
    wxTextCtrl *m_pSearchText;
    wxComboBox  *m_pCbSearch;
    wxPanel     *m_pListTablePn;
    wxListCtrl  *m_pListTable;
    wxPanel     *m_pListPn;
    wxListCtrl  *m_pList;
    wxHtmlWindow *m_pHtmlInfo;
    //***************************//
    //        wxImageList                m_imageList;
    wxString        m_strTmp;
    wxArrayString   m_colName;
    int            *m_colNameId;//indicate which info is displayed in the ith column;  =0,1,2...
    wxArrayString   m_lcItem;     //(i)= name*type*createdate*lastModify*description*$filepath
    wxArrayString   m_lcTmpItem;
    //**************************//
    int                  m_isAccend[PRJINFO_COLNUM];
    unsigned int  m_ltIndex;
    unsigned int  m_lcIndex;
    int                  m_schType;
    wxString         m_htmlPath;
};
class PInfoHtmlWin : public wxHtmlWindow {
public:
    PInfoHtmlWin(wxWindow *parent, SimuPrjInfoView *viewparent, int id, const wxPoint &pos = wxDefaultPosition,
                 const wxSize &size = wxDefaultSize, long style = wxHW_DEFAULT_STYLE)
        : wxHtmlWindow(parent, id, pos, size, style),
          m_pOwner(viewparent) {
    }

    void OnLinkClicked(const wxHtmlLinkInfo &link) {
        if (m_pOwner) {
            if (!m_pOwner->LinkClicked(link)) {
                wxLaunchDefaultBrowser(link.GetHref());
            }
        }
    }

private:
    SimuPrjInfoView *m_pOwner;
};

class DlgExpList: public wxDialog {
public:
    DlgExpList(wxWindow *parent, wxArrayString &fileNames, wxWindowID id = wxID_ANY, const wxString &title = wxT("Set Job Name"),
               const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize,
               long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER);
    ~DlgExpList();
private:
    void    CreateUI();
    void    Init();
public:
    wxString    GetExpFile();
private:
    DECLARE_EVENT_TABLE();
    void    OnSelection(wxCommandEvent &event);
    void    OnButton(wxCommandEvent &event);
private:
    wxComboBox   *m_pCbExpFile;
    wxString          m_fileName;
    wxArrayString m_fileNames;
};

#endif
