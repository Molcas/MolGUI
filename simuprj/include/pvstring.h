/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PV_PVSTRING_H__
#define __BP_PV_PVSTRING_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MESA
#define msU_MSG_CONFIRM 1

class MyString {
public:
static void *xmalloc (size_t size);


static int mytoken (char *in, char c, char *out, char *innew);
   /* cut string _in_ at the position _c_
      in == out // c // innew
      return 1 if OK.
    */

static int myNext (char *, char, int *, int *);
   /*
      NOTE : input sring must be mydiet();
      while (myNext(in,' ', &shift, &len)==0)
      {
      strncpy(out,in+shift,len);
      out[len]=0;
      shift=shift+len+1;
      }

    */

static int mycut (char *in, char c, char *out);
   /* cut string _in_ at the position _c_
      in == out // c // ...
      return 1
    */

static char *strcatc (char *str, char c);
  /* strcat for char */
static char *strcatl (char *str, char c);
  /* strcatc if last char not c */

static char *chomp (char *str);
static char *chop (char *str);

static int myterminate (char *in, char *term);
    /* cut string, if any char from terminator are exist */

//int my7cut (char *in, char c);
   /* cut string _in_ at the position _c_
      out == out // c // ...
      return 1
    */

static char *myexchange (char *in, char a, char b);
   /*  replace in _in_ char _a_ to _b_
      return in.
    */


static int myfgets (char *str, int len, FILE * inp);
   /*  like fgets, but
    *    -remove \n
    *    -skip chars before next line
    *   return 0 at eof, else 1.
    */

static int mylastchar (char *str, char c);
   /*  is last char in str _c_ ?
    *   return 0 if not, else 1.
    */

static int mylastvchar (char *str, char c);
   /*  is last visible char in str _c_ ?
    *  return 0 is not, else 1.
    */

static int mylastdel (char *str, char c);
   /*  is last char in str is _c_ delete it
    */

static int mysubdelete (char *str, char *token, char c);
   /*  if token exist in str, cut token before char c
    */

static int mysubstitute (char *in, char *out, char *token, char *ins);
   /*  if token exist in in, substitute it to ins
    */
static int mysubtranc (char *in, char *token, char *ins);
   /*  substitute all token to ins on place. strlen(token)>=strlen(ins)
    */

static char *mydiet (char *str);
   /*  remove all extra space and \n from str
    */

static char *myextradiet (char *str, char c);
   /*  remove all c from str;
    */

static char *mydietspace (char *str);
   /*  remove all extra space  from str
    */

static char *strupr1 (char *str);
   /*  strupr
    */
static char *strlwr1 (char *str);
   /*  strlwr
    */

static int mycount (char *in, char c);
    /* return number of chars c in in */
static int mycount2 (char *in, char c, int n);
static int mycounts (char *in, char *sub, char c);

static int mycounts_next (char *in, char *sub, char c, char *next, int len);

static int mycutsub (char *in, char c);
   /* remove beginning of in */

static int msS_STR_TrimToken(char *line, char *token);
static void msU_MSG_ArgS(char *str);
static void msU_MSG_Display (int confirm, char *type, int *status);
static int msU_INT_RequestInterrupt (void);
static int echo(const char* s);
static int die(const char* s);
static int SayError(const char* msg);
static void *msS_MEM_Alloc(size_t size);
static int msU_INT_ReportStatus(char *line, int i, int *status);
static void msU_INT_ReportProgress (int ind);

};

#endif
