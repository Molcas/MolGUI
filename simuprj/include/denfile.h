/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_DENFILE_H__
#define __BP_MOL_DENFILE_H__

#include <wx/wfstream.h>
#include <wx/datstrm.h>
#include <wx/txtstrm.h>
#include "wx.h"
#include "chematom.h"
/*
used to deal with the density file
*/
class DenFile
{
public:
        //test the file type, if the file is ascii density file format, return true,else return false
        static bool IsAsciiDenFile(const wxString& fileName);
        //test the file type, if the file is binary density file format,return true,else return false
        static bool IsBinaryDenFile(const wxString& fileName);
        //test the given file, if the file is density file return true,else return false
        static bool IsDenFile(const wxString& fileName);
        //load the part of grid information in the binary density file
        static void LoadBinaryGridInfo(const wxString &strLoadedFileName,wxArrayString& gridInfoArray);
        //load the part of grid information in the ascii density file
        static void LoadAsciiGridInfo(const wxString &strLoadedFileName,wxArrayString& gridInfoArray);
        //load the binary density file
        static void LoadBinaryFile(const wxString &strLoadedFileName, AtomBondArray &appendChemArray, int *&atomLabels);
        //load the ascii density file
        static void LoadAsciiFile(const wxString &strLoadedFileName, AtomBondArray &appendChemArray, int *&atomLabels);
        //test the file  format,ascii return true,other return false
        static bool IsAsciiFile(const wxString fileName);
        //load the grid information from the density file
        static void LoadGridInfo(const wxString &strLoadedFileName,wxArrayString& gridInfoArray);
        //exchange element symbol to element id
        static ElemId GetElemId(wxString atomSymbol);
        //load the density file
        static void LoadFile(const wxString& strLoadedFileName, AtomBondArray& appendChemArray, int* atomLabels);
        //get the bond type between two elements
        static BondType CalculateBondType(int atom1Index, int atom2Index, AtomBondArray& atomBondArray);
        //search the bond list
        static bool SearchBondList(int atom1Id, int atom2Id, const int* bondList);

private:
        //read a string from the input stream
        static wxString GetNextString(wxDataInputStream& dis);
        //read a value from the input stream
        static double GetNextValue(wxDataInputStream &dis);

};

#endif // !defined(__BP_DEN_FILE_H__)
