/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "wx.h"

// project events
//const wxEventType EVT_SIMU_PROJECT_OPEN = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_CLOSE = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_SAVE = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_ACTIVATE = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_BEGIN_ADD_FILES = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_END_ADD_FILES = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_BEGIN_REMOVE_FILES = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_END_REMOVE_FILES = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_FILE_ADDED = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_FILE_REMOVED = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_POPUP_MENU = wxNewEventType();
//const wxEventType EVT_SIMU_PROJECT_TARGETS_MODIFIED = wxNewEventType();
const wxEventType EVT_SIMU_PROJECT_RENAMED = wxNewEventType();
