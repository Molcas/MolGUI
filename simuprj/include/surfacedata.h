/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PV_SURFACEDATA_H__
#define __BP_PV_SURFACEDATA_H__

#include "pvstring.h"
#include "isosurfile.h"
#include "commons.h"
#include "ctrlinst.h"

class SurfaceData;
class SurfaceThread : public wxThread {
        public:
                SurfaceThread(SurfaceData * surfaceData);
                // thread execution start here
                virtual void* Entry();
                virtual void OnExit();

        private:
                SurfaceData * m_surfaceData;
};


class SurfaceData : public AbstractSurface {
public:
        SurfaceData();
        ~SurfaceData();

        virtual void Init(const wxString fileName);
        virtual void RenderSurface(void);

        virtual int GetCurmo(void) const;
        virtual void SetCurmo(int curmo);
        virtual void LoadGrid(int mo, wxFFile *outf);

        void LoadGrids(int mo, wxFFile *outf);

        void SaveSrfs(wxString densityfile);
        void LoadSrf(int mo);
        void SaveSrf(int mo);
        void LoadSurface();
        void SaveSurface();
        void SetShowStyle(int style);
        void Translate(point_t &pos,double transX,double transY,double transZ);
        int GetMORowNum(void)
        {
                if(currentGridData)
                        return currentGridData->ngrid;
                else
                        return 0;
        }
        wxString GetMOInfo(int row,int col);
        void RotateWithZ(point_t &pos,double z);
        void RotateWithY(point_t &pos,double y);
        void RotateWithX(point_t &pos,double x);
        void RotateSurface(double transX, double transY, double transZ,float rotx,float roty,float rotz);
        void AllSurfaceInit();
        multisrf_t * GetAllSurfaces();
        void LoadGridHeader();
        grid_info_t GetGridInfo();


private:
        multisrf_t * All_Surfaces;
        grid_info_t Grid_info;
        input_data_t Input_Data;
        Grid_t * currentGridData;
        SurfaceThread * m_surfaceThread;
};

#endif
