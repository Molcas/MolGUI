/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_DLGSUMMARY_H__
#define __BP_PRJ_DLGSUMMARY_H__

#include "wx.h"

class DlgSummary : public wxDialog
{
    DECLARE_EVENT_TABLE()
public:
        wxArrayString GetSummary(void);
        void AppendSummary(wxString sumfile);
        void DoOk(void);
        //begin : hurukun : 19/11/2009
        DlgSummary(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Calculation Summary"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
//            long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER );
            long style = wxDEFAULT_DIALOG_STYLE);
        //end   : hurukun : 19/11/2009
        virtual ~DlgSummary();
private:
        wxStaticText* pstSummary[12];
private:
        void OnClose(wxCloseEvent &event);
        void CreateGUIControls(void);
};

#endif
