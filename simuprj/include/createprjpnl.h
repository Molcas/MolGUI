/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_CREATE_PRJ_PNL_H__
#define __BP_PRJ_CREATE_PRJ_PNL_H__


#include "wx.h"
#include <wx/combobox.h>
#include <wx/imaglist.h>

#include "genericdlg.h"
#include "simuproject.h"
#include "simuprjdef.h"

class CreatePrjPnl : public GenericPnl {
public:
        CreatePrjPnl(GenericDlg* parent, int pgoption=1,wxWindowID id = wxID_ANY);
        ~CreatePrjPnl();

public:
        wxString        GetPrjName()const;
        wxString        GetFullPath()const;
        wxString        GetPGName()const;
        wxString        GetDir()const{return m_strDir;}
    ProjectType GetPrjType();
        virtual bool OnButton(long style);

private:
        DECLARE_EVENT_TABLE();
        void         CreateGUIControls(void);
        void     OnCbxNoPg(wxCommandEvent& event);
        void         OnBtnDir(wxCommandEvent& event);
        void         OnText(wxCommandEvent& event);
        void     OnKey(wxCommandEvent&event);
        bool         ValidateData(void);

private:
    wxCheckBox* m_pCbxNoPg;
        wxComboBox*        m_pCombPG;
        wxRadioBox* m_pRbxPrjType;
        wxTextCtrl* m_pTxtPName;
        wxTextCtrl*        m_pTxtFullPath;
        wxString        m_strDir;
        wxString        m_fullPath;
        int               m_pgOption;//to determine if to show the project group chosen items.: 0->has prjgrp,do not show;1-> has and show prjgrp:2->no prjgrp
};

#endif
