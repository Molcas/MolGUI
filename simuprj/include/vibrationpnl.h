/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PV_VIBRATIONPNL_H__
#define __BP_PV_VIBRATIONPNL_H__
#include <wx/grid.h>
#include "wx.h"
#include "vibration.h"
#include "molview.h"
#include "projectfiles.h"

enum VibGridColumns
{
    Col_Frequency,
    Col_Symmetry,
    Col_Intensity,
        Col_MAX
};
#ifndef __GRID_CELL_CHECK_EDITOR_RENDERER__
#define __GRID_CELL_CHECK_EDITOR_RENDERER__
extern const wxString wxGRID_VALUE_ITEM;

class wxGridCellCheckEditor : public wxGridCellEditor
{
public:
        class ValueItem
        {
        public:
                bool check;
                wxString display;
                ValueItem()
                {
                        check=false;
                        display=wxEmptyString;
                }
        };

public:
        wxGridCellCheckEditor();
        virtual ~wxGridCellCheckEditor();

        virtual void Create(wxWindow* parent,wxWindowID id,wxEvtHandler* evtHandler);

        virtual void SetSize(const wxRect& rect);
        virtual void Show(bool show, wxGridCellAttr *attr = NULL);

        virtual bool IsAcceptedKey(wxKeyEvent& event);
        virtual void BeginEdit(int row, int col, wxGrid* grid);
        bool EndEdit(int row, int col, const wxGrid* grid, const wxString &oldval, wxString *newval);
        bool EndEdit(int row, int col, wxGrid* grid);
        virtual void ApplyEdit(int row, int col, wxGrid* grid);

        virtual void Reset();
        virtual void StartingClick();
        virtual void StartingKey(wxKeyEvent& event);

        virtual wxGridCellEditor * Clone() const;

        virtual wxString GetValue() const;

protected:
        wxCheckBox* CheckBox() const;
public:
        static ValueItem* ParseString(wxString value);
        static void UseStringValues(const wxString& valueTrue = _T("1"),const wxString& valueFalse = wxEmptyString);
        static bool IsTrueValue(const wxString& value);
public:
        void SetDisplayValue(wxString dis);
        void SetChecked(bool checked);
        wxString GetDisplayValue(void);
        void SetStartValue(bool checked,wxString dis=wxEmptyString);
        bool IsChecked(void);
private:
        ValueItem* m_startValue;
        static wxString ms_stringValues[2];
        DECLARE_NO_COPY_CLASS(wxGridCellCheckEditor);
};

class wxGridCellCheckRenderer : public wxGridCellRenderer
{
public:
        virtual wxGridCellRenderer * Clone() const;
        virtual wxSize GetBestSize(wxGrid& grid,wxGridCellAttr& attr,wxDC& dc,int row, int col);
        virtual void Draw(wxGrid& grid,wxGridCellAttr& attr,wxDC& dc,const wxRect& rect,int row, int col,bool isSelected);
        wxGridCellCheckRenderer();
        virtual ~wxGridCellCheckRenderer();
private:
        wxSize m_sizeCheckMark;
protected:
        void SetTextColoursAndFont(const wxGrid& grid,const wxGridCellAttr& attr,wxDC& dc,bool isSelected);
};
#endif

class VibrationPnl : public wxPanel
{
    DECLARE_EVENT_TABLE()
public:
        int GetVibCyle();
        VibrationPnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
                                        const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30),
                                        long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panelvibration"));
        virtual ~VibrationPnl();
        void ClearData(void);
        int GetSelectedRow(void)const;
        void DoVibration(int index=-1);
        void AddVibDisplayData(const IRDataArray& disArray);
        void OnTimer(MolView* view, AbstractProjectFiles* pFiles);
private:
        //static
        wxGrid* pGrid;
        wxScrolledWindow* pScrWin;
    wxSlider* pSliderCycle;
        wxSlider* pSliderExcursion;
        IRDataArray data;
        int m_selectedRow;
        bool m_firstclick;
private:
        void OnSlider(wxCommandEvent& event);
        void OnSize(wxSizeEvent& event);
        void OnClose(wxCloseEvent &event);
        void OnActivate(wxActivateEvent &event);
        void OnCellLeftClick( wxGridEvent& event );
        void AppendVibRow(const IRData& item);
        void CreateGUIControls(void);
        wxGrid * CreateGrid(wxWindow* parent,wxWindowID id);
};

#endif // !defined(__BP_VIBRATION_PANEL_H__)
