/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

//---------------------------------------------------------------------------
//
// Name:        simuprjfrm.h
// Author:      xychen
// Created:     2008-11-21 18:17:56
// Description: SimuPrjFrm class declaration
//
//---------------------------------------------------------------------------

#ifndef __BP_PRJ_SIMUPRJ_FRM_H__
#define __BP_PRJ_SIMUPRJ_FRM_H__

#include "wx.h"

#include <wx/aui/aui.h>
#include <wx/docview.h>
#include <wx/combobox.h>
#include "genericdlg.h"
#include <wx/html/helpctrl.h>
#include "ctrlinst.h"
#include "molglcanvas.h"
#include "molvieweventhandler.h"

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
////Header Include End

////Dialog Style Start
#define SimuPrjFrm_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxRESIZE_BORDER | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class SimuPrjFrm : public wxFrame {
        private:
                DECLARE_EVENT_TABLE();

        public:
                SimuPrjFrm(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("MolGUI"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = SimuPrjFrm_STYLE);
                virtual ~SimuPrjFrm();

                void         CreateGUI(void);
                void         AfterCreation(void);
                MolViewData* GetMolData(void) const;  //SONG add it for workshop 2009
                void         EnableMenuToolCtrlInst(void);  //SONG add it for workshop 2009
        void    UpdateRecentProjects(const wxString& fileName, bool isClearHis = false, bool isRemove = false);
                void    UpdateRecentPrjGrps(const wxString& fileName, bool isClearHis = false, bool isRemove = false);

        protected:
                void         DoUpdateLayout(void);
                void         UpdateFrameTitle(const wxString& fileName);  //SONG add it for workshop 2009
                //---------------
        public:
                void         EnableMenu(bool);
                void         EnableMenuTool(bool enabled);  //SONG add it for workshop 2009
                void     EnableTool(int toolId, bool enabled);
        private:
                //Do not add custom control declarations between
                //GUI Control Declaration Start and GUI Control Declaration End.
                //wxDev-C++ will remove them. Add custom code after the block.
                ////GUI Control Declaration Start
                ////GUI Control Declaration End
                void         CreateGUIControls();
                /**
                 * window state (such as postion, size, maximization, and so on.
                 */
                void         InitWindowState(void);
                void         SaveWindowState(void);

                void         OnClose(wxCloseEvent& event);

                void         CreateMenuBar(void);
                void         CreateToolBar(void);

//begin  : hurukun : 4/12/2009
                void        OnFileNew(wxCommandEvent& event);// create a new project group or project or directory in
                bool DoFileNew();
                void OnUpdateUIFileNew(wxUpdateUIEvent& event);
                void        OnFileOpen(wxCommandEvent& event);//open a existing project group or project.
                //deal with the selected item in the workspace tree;
        public:
                void        OnFileAdd(wxCommandEvent& event);
                void        OnUpdateUIFileAdd(wxUpdateUIEvent& event);
                void        OnFileSave(wxCommandEvent& event);// save a project group or a project or all project in a directory.
                void        OnUpdateUIFileCopy(wxUpdateUIEvent& event);
                void        OnUpdateUIFilePaste(wxUpdateUIEvent& event);
                void        OnUpdateUIFileSave(wxUpdateUIEvent& event);
                void        OnFileSaveAs(wxCommandEvent& event);//save a project group or a project or all project in a directory to a different directory.
                void        OnFileSaveAll(wxCommandEvent& event);//save all in current workspace.
                void        OnFileClose(wxCommandEvent& event);//close a project group.
                void        OnFileSaveClose(wxCommandEvent& event);
                void        OnToolProjectInfoList(wxCommandEvent& event);
                void        OnUpdateUIFileClose(wxUpdateUIEvent& event);
                void        OnFileCloseAll(wxCommandEvent& event);//close all project group.
                void        OnFileCloseAllView(wxCommandEvent& event);//close all view.
                void        OnUpdateUIFileCloseAllView(wxUpdateUIEvent& event);
                void        OnFileRemove(wxCommandEvent& event);//remove a project, directory from project group .Do not physically delete the files.
                void        OnUpdateUIFileRemove(wxUpdateUIEvent& event);
                void        OnFileDelete(wxCommandEvent& event);//remove a project, directory from project group .Do not physically delete the files.
                void        OnUpdateUIFileDelete(wxUpdateUIEvent& event);
                void        OnFileRemoveAll(wxCommandEvent& event);//To empty a  project group.  Do not physically delete the files.
        public:
                void DoFileOpen(wxString& fileName);


                wxString        GetCmdLine();
        private:
                void        OnResize(wxSizeEvent& event);
//end      : hurukun : 4/12/2009
//                void OnFileNewProject(wxCommandEvent& event);
//                void OnFileOpenProject(wxCommandEvent& event);

                void OnFileOpenRecent(wxCommandEvent& event);
                void OnFileOpenRecentPG(wxCommandEvent& event);
                void OnStartHereLink(wxCommandEvent& event);
                void OnStartHereVarSubstition(wxCommandEvent& event);
                void OnFileClearRecentProjects(wxCommandEvent& event);
                void OnFileClearRecentPrjGrps(wxCommandEvent& event);

                bool DoFileCloseCtrlInst(CtrlInst* pCtrlInst); //SONG add it for workshop2009
                int NeedSaveRenderingData(CtrlInst* pCtrlInst); //SONG add it for workshop2009
                bool SaveRenderingData(CtrlInst* pCtrlInst, bool isSaveAs); //SONG add it for workshop2009
//                bool DoFileCloseAll(void); //SONG add it for workshop2009
                void SaveandQuit(void);  //SONG add it for workshop2009
                void QuitwithoutSave(void); //SONG add it for workshop2009

                void OnFileImport(wxCommandEvent& event); //SONG add it for workshop2009
//                bool DoFileOpen(wxString& fileName);  //SONG add it for workshop2009
                bool DoFilePretreatment(wxString& fileName);  //SONG add it for workshop2009
//                bool CreateCtrlInst(wxString& fileName);  //SONG add it for workshop2009
//                void DoMoleculeBuildCommon(int eventId, bool isUIEvent, bool isMenu);  //SONG add it for workshop2009
                void RefreshGraphics(void);  //SONG add it for workshop2009
//                void UpdateRecentFiles(const wxString& newestFile, bool isClearHis); //SONG add it for workshop2009
                void DoCheckMenuWindow(int winId);  //SONG add it for workshop2009

                bool DoFileCloseAll(void);  //SONG add it for workshop2009
                bool DoFileCloseWithMenuUpdate(void);   //SONG add it for workshop2009
                void OnFileTest(wxCommandEvent& event); //SONG add it for test
                void OnEnvSetting(wxCommandEvent& event);
        public:
                void OnColrSetting(wxCommandEvent& event);
        private:
                void OnHostsSetting(wxCommandEvent& event);
                void OnMasterSetting(wxCommandEvent& event);

                void OnMolMenuCommand(wxCommandEvent& event);
                void OnMolToolCommand(wxCommandEvent& event);

                void OnShowMolecule(wxCommandEvent& event);
                void OnUpdateUIShowMolecule(wxUpdateUIEvent& event);

                void OnHelp(wxCommandEvent& event);
                void OnAbout(wxCommandEvent& event);//------XIA 09workshop
                void OnExit(wxCommandEvent& event);
                /**
                 * Set the fileName as the most recent project.
                 * @param fileName wxEmptyString means to load history file, or when clearing history
                 * @param icClearHis True to clear history.
                 * @param isRemove True to remove the fileName from the history.
                 */
        public:
                void        OnJobNew(wxCommandEvent& event);
                void         OnUpdateUIJobNew(wxUpdateUIEvent& event);
                void        OnJobSetting(wxCommandEvent& event);
                void         OnUpdateUIJobSetting(wxUpdateUIEvent& event);
                void         OnJobSubmit(wxCommandEvent& event);
                void         OnUpdateUIJobSubmit(wxUpdateUIEvent& event);
                void         OnJobMonitor(wxCommandEvent& event);
                void         OnUpdateUIJobMonitor(wxUpdateUIEvent& event);

        public:
                void OnViewCaptureImage(wxCommandEvent& event);
                void OnMenuOpen(wxMenuEvent & event);
        private:
                //Note: if you receive any error with these enum IDs, then you need to
                //change your old form code that are based on the #define control IDs.
                //#defines may replace a numeric value for the enum names.
                //Try copy and pasting the below block in your old form header files.
                enum {
                        ////GUI Enum Control ID Start
                        ////GUI Enum Control ID End
                        ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
                };
private:
                wxAuiManager m_auiManager;
                wxMenu*          m_pRecentProjectsMenu;
                wxMenu*          m_pRecentProjectGroupsMenu;
                wxToolBar*      m_pTbProject;
                wxToolBar*      m_pTbJob;
                wxComboBox*                m_combBCmdLine;

                wxHtmlHelpController* m_helpCtrl;
                MolGLCanvas* m_pGLCanvas;
                MolView* m_pMolView;
        MolViewData* m_molData;
//                wxTreeCtrl* m_pTree; //SONG add it for test

};

#endif
