/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_SIMUPROJECT_H__
#define __BP_PRJ_SIMUPROJECT_H__
#include <wx/treectrl.h>

#include "wx.h"
#include "projectfiles.h"
#include "simuprjdef.h"
#include "molview.h"

//#define PRJ_FILE_SUFFIX  .sip
//#define wxPRJ_FILE_SUFFIX  wxT(".sip")
//#define PRJ_FILE_SUFFIX_LEN 4

//**************************************************************************************************************
class ProjectTreeItemData;
class JobInfo {
public:
        JobInfo(wxString fileName);
        virtual ~JobInfo();
        void         SetDirty(bool isDirty);
        bool         IsDirty(void) const;
        virtual bool LoadFile(void) = 0;
        virtual bool SaveFile(void) = 0;

protected:
    wxString m_fileName;
        bool m_isDirty;
};
//***************************************************************************************************************
class JobNodeData{//help to manage the job tree struct of the project;
private:
    struct Node{
        wxTreeItemId id;
        wxString        name;
        Node*            next;
        Node()
        {
            name=wxEmptyString;
            next=NULL;
        }
        Node(wxTreeItemId theid,wxString    thename)
        {
            id=theid;
            name=thename;
            next=NULL;
        }
    };
public:
    JobNodeData();
    ~JobNodeData();
public:
    bool Add(wxTreeItemId id, wxString jobName);
    bool Del(wxString jobName);//the jobname in a project is unique.
    wxTreeItemId GetId(wxString jobName);
        bool Rename(const wxString& oldName,const wxString& newName);
private:
    Node *  m_head;
};
/**
 * A project is a collection of molecule and calculation result files.
 */
#define SIMUPRJ_JOB_ITEM_NUM    10
class SimuProject {
public:
    static wxString GetFileFilter(){return wxT("Project File (*.sip)|*.sip");}
    static wxString GetXmlFileRoot();
    static wxString         GetProjectType(ProjectType type);
    static ProjectType         GetProjectType(const wxString & strType);
public:
        SimuProject(const wxString& fullPathName = wxEmptyString,wxTreeItemId parentId=0);
    SimuProject(ProjectType projectType, const wxString& title, const wxString& fullPathName = wxEmptyString);
    ~SimuProject();
public:
    bool    AttachView(MolView * pView);
    void    Refresh(bool expand=false);
    void    BuildTree(wxTreeCtrl* pTree);
    void    BuildResultTree(wxString jobName,bool isExpandJob=false);
    void    DelJob(wxString jobName);
        void        DelJobOuterFile(wxString jobName,wxString fileName);
    void    AddMopacNode(wxString title);
    void    AddMopacNode();
    void    DelMopacJob(wxString title);
    void    CloseView(bool isAskForSave=true,bool default_save=false);
    void    Close(bool isAskForSave=true,bool default_save=false);
        void        Import();
    void    Load(wxTreeItemId parenId);
    void    LoadJobs();
        bool    Rename(const wxString & nTitle);
        bool    RenameJob(const wxString & jobName,const wxString & nName);
    bool    Save(void);
        bool    SaveAs(const wxString & strPrjFullName,bool isToLoad=false);
        void    ShowResultItem(ProjectTreeItemData *item);
public:
        // @return True if the project is modified in any way.
        bool                    IsDirty()  ;
        // Mark the project as modified or not.
        // @param modified If true, the project is marked as modified. If false, as not-modified.
        void                     SetDirty(bool modified = true) ;
        bool             IsChild()const{return m_isChild;}
        void             SetChild(bool isChild){m_isChild=isChild;}
        bool             IsPGChild(wxString pgFileName);
        wxString             GetFileName(void) const { return m_fullPathName; }
        wxString       GetNewFileName(wxString nTitle);
        wxString                GetOutfileSuffix();
        void                 SetFileName(wxString fullPathName);
        wxString             GetPathWithSep(void) const;
    ProjectType         GetType(void) const;
    int                             GetProjectId(void) const { return m_projectId; }
    wxString      GetMopacOutFilePath(wxString fileName);
    MolView*     GetView();

   // @manage The root item of this project in the project manager's tree.
        wxTreeItemId         GetProjectNode(void) { return m_projectNode; }
        void                 SetNodeId(wxTreeItemId id){m_projectNode=id;}
        // Changes project title.
    void                                         SetTitle(const wxString& title);
    const wxString&                        GetTitle(void) const { return m_title; };
    wxString                                 GetTreeLabel(void);

        /**
         * Build the project tree.
         * @param pTree The wxTreeCtrl to use.
         * @param root The tree item to use as root. The project is built as a child of this item.
         */
    void        SetJobInfo(JobInfo* jobInfo);
    JobInfo*   GetJobInfo(void) const;
        wxArrayString        GetJobs(void);

        wxString          GetResultDir(wxString resultName) const;
        wxString          GetResultFileName(wxString resultName) const;
        bool              RemoveResultDir(wxString resultName);
        AbstractProjectFiles* GetFiles(wxString resultName);
        bool                 HasMolecule();
        void                 RemoveFiles(wxString resultName);
        void                 UpdateViewByXyz();//update the .sim file according to the xyz file;
private:
        void         NotifyPlugins(wxEventType type);
        bool            SaveProjectFile();
private:
        int                             m_projectId;//used to identify the project with its view
        int               m_isDirty;
    ProjectType         m_type;
        wxString                 m_title;//e.g: tiny
        wxString                 m_fullPathName;//e.g: /home/project/mol/tiny/tiny.sip
        wxString       m_projectDesc;
        bool                             m_isModified;
        bool                             m_isLoaded;
        bool                 m_isChild;
        wxTreeItemId   m_projectNode;
        wxTreeItemId   m_mopacNode;
        wxArrayString  m_mopacFiles;
        bool                m_warnNewMolecule;
        JobInfo*                     m_pJobInfo;
        JobNodeData   m_jobNode;
//        wxArrayString m_fileList;

        ProjectFilesArray m_prjFilesArray;
};

#endif
