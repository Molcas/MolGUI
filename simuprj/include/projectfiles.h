/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_PROJECTFILES_H__
#define __BP_PRJ_PROJECTFILES_H__

#include "wx.h"
#include "datatype.h"
#include "irfile.h"
#include "vibration.h"
#include "simuprjdef.h"
#include <wx/treectrl.h>
class ProjectTreeItemData;
class SimuProject;

/**
 * The abstract class for all project types to process the calculation results.
 * All project types should extend this class to process its specific results.
 */
class AbstractProjectFiles {
public:
        bool HasSummary();
        void SetNeedSurface(bool);
        bool GetNeedSurface();
        bool HasSurfaceData();
        void SaveSurfaces();
        bool DoCalcSurface();
        bool HasVibrationData();
        wxString GetSummaryFileName(void);
        virtual wxString GetVibOutputFileName( );
        /**
         * @param simuProject
         * @param name The result directory name
         */
        AbstractProjectFiles(SimuProject* simuProject, wxString name);
        virtual ~AbstractProjectFiles();
        /**
         * @return The result name
         */
        wxString GetName(void) const;
        void         SetName(const wxString& name){m_name=name;}
        void SetStartTime(wxLongLong startTime);
        wxLongLong GetStartTime(void) const;

        virtual bool CreateInputFile(void);
        virtual wxString GetExecCommand(void);
        virtual wxString GetExecPara(void);
    virtual wxString GetExecLogName(void);
        virtual wxString GetOutputFileName(void);
        virtual wxString GetInputFileName(void);
        virtual         wxString GetOutFileName(wxString& fileSufffix);
    /**
     * @return True continue to invoke programes
     */
    virtual bool IsFinal(void);
    /**
     * Will be called just after job terminated.
     */
    virtual bool PostJobTerminated(bool isFinal);
    /**
     * Build result tree to represent calculation results
     */
        virtual bool HasOutput();
        virtual bool HasEnergy_Statistics();
        virtual bool HasError_Info();
        virtual int  HasDensity();
        virtual bool HasOptimization();
        virtual bool HasFrequence();
        virtual bool HasGssOrb();
        virtual bool HasInput();
        virtual bool HasScfOrb();
        virtual bool HasDump();

    virtual void BuildResultTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode);
        virtual bool BuildOuterFileTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode){return false;}
        virtual wxString GetSuffix(ProjectTreeItemType itemType){return wxEmptyString;}
        virtual void ShowResultItem(ProjectTreeItemData *item);
    virtual wxArrayString GetSurfaceDataFiles(void);
        virtual void InitVibrationData(void);
        VibPropertyDisItemArray& GetVibrationData(void);
        void ResetVibrationData(VibPropertyDisItemArray disArray);
        IRDatas* GetIRDatas(void);

protected:
        SimuProject* m_pSimuProject;
        wxString m_name;
        wxLongLong m_startTime;

protected:
        VibPropertyDisItemArray        m_vibArray;                                        //save data for display vibration
        IRDatas m_irs;
private:
        bool m_needsurface;
};

WX_DEFINE_ARRAY(AbstractProjectFiles*, ProjectFilesArray);
#endif
