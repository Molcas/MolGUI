/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

//---------------------------------------------------------------------------
//
// Name:        simuprjapp.h
// Author:      xychen
// Created:     2008-11-21 18:17:56
// Description:
//
//---------------------------------------------------------------------------

#ifndef __BP_PRJ_SIMUPRJ_APP_H__
#define __BP_PRJ_SIMUPRJ_APP_H__

#include "wx.h"

class SimuPrjApp : public wxApp {
        public:
                virtual bool OnInit();
                virtual int OnExit();
};

DECLARE_APP(SimuPrjApp);

#endif
