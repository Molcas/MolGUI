/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef _DLG_JOB_NAME_H_
#define _DLG_JOB_NAME_H_
#include "simuproject.h"
#include "wx/listctrl.h"

class DlgJobName:public wxDialog{
public:
    DlgJobName(wxWindow* parent, SimuProject*   pProject=NULL,wxWindowID id = wxID_ANY, const wxString& title = wxT("Set Job Name"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
            long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER, int flg = 0);
    ~DlgJobName();
private:
    void    CreateUI();
    void    InitLc();
public:
    wxString    GetJobName();
private:
    DECLARE_EVENT_TABLE();
    void    OnLcActiveItem(wxListEvent& event);
    void    OnButton(wxCommandEvent& event);
    void    OnText(wxCommandEvent& event);
    void    OnEnter(wxCommandEvent& event);
    void    OnRClickItem(wxListEvent& event);
    void    OnDeleteItem(wxCommandEvent& event);
private:
    wxTextCtrl*    m_txtJobName;
    wxListCtrl*     m_lcJobName;
    SimuProject*  m_pProject;
    int           dflag;
    long           m_selectedItem;

};
#endif
