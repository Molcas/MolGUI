/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

//hurukun: 2/12/2009

#ifndef  _PROJECT_GROUP_H_
#define _PROJECT_GROUP_H_
#include <wx/treectrl.h>
#include "simuproject.h"

//#define PG_FILE_SUFFIX  .spg
//#define wxPG_FILE_SUFFIX  wxT(".spg")
//#define PG_FILE_SUFFIX_LEN 4

// This class is designed to deal the father/child relationship in the project group;
// This is a N-children tree.
//example: /home/project/mol/mol.spg
//                                                   ../mol/tiny/timy.sip
//                                                   ../mol/tiny_b/tiny_b.sip
//                                                   ../mol/micro/arb/arb.sip
class ProjectGroup;
WX_DEFINE_ARRAY(SimuProject *, ProjectsArray);
//**************************************************************************************************************
//begin  : hurukun : 4/12/2009
class ProjectTreeItemData : public wxTreeItemData {
public:
    static wxString    GetFileName(SimuProject *project, wxString jobName, ProjectTreeItemType itemType);
public:
    ProjectTreeItemData(SimuProject *project, ProjectGroup *group, wxString  *pLogPath, ProjectTreeItemType type = ITEM_PROJECT);//for Node.;
    ProjectTreeItemData(SimuProject *project, wxString  logPath, ProjectTreeItemType type = ITEM_UNKNOWN);//for project file node
    ~ProjectTreeItemData();
public:
    ProjectTreeItemType GetType(void) const { return m_type; }
    void                SetType(ProjectTreeItemType type) {m_type = type;}
    SimuProject         *GetProject(void) const { return m_pProject; }
    void                SetProject(SimuProject *prj)  {  m_pProject = prj; }
    wxString            GetFileName(void) const;

    wxString            GetTitle()const;
    void                SetItemVal(wxString &nLogVal);
    wxString            GetItemVal()const {return *m_pPath;}
    ProjectGroup        *GetProjectGroup()const {return m_pPrjGrp;}
private:
    SimuProject         *m_pProject;//only the leaves of the tree has an avilable value of simuproject, otherwise NULL;
    ProjectGroup        *m_pPrjGrp;
    wxString            *m_pPath;//point to the logval of the node; example : /mol,  /mol/tiny.
    ProjectTreeItemType m_type;
    int                 m_dType;
    //end     : hurukun : 4/12/2009
};
//******************************************************************************************************************
class ProjectTree { //the value of the tree be modified in CheckNode() and Insert() and FileToTree();
    //the logval is used to maintain the tree struct; start from the project group name,
    //the phyval is used to manage the file system; start from the project group name or from the disk root;
    //both are recorded in the project group description file;
    //e.g : nodeLogVal=/mol/tiny/tiny.sip  :  nodePhyVal=./mol/tiny/tiny.sip or /home/project/mol/tiny/tiny.sip
private:
#define DEFAULT_CHD_NUM 5
    class Node {
    public:
        wxTreeItemId        nodeID;
        wxString        nodeLogVal;//used by the tree structure.
        wxString        nodePhyVal;//only the project-group and project node need to set this val, or empty;
        int                            cnum;//the num of child node.
        Node                **chd;
    public:
        Node()
        {
            nodeLogVal = wxEmptyString;
            nodePhyVal = wxEmptyString;
            cnum = 0;
            chd = new Node*[DEFAULT_CHD_NUM];
        }
        ~Node()
        {
            if(chd != NULL)
                delete chd;
            chd = NULL;
        }
    };
public:
    ProjectTree(wxTreeCtrl *tree, ProjectGroup *group);
    //        ProjectTree(const ProjectTree& tree);
    ~ProjectTree();
public:
    bool     Clear();//remove all node except the root node;
    void     CloseNode(wxTreeItemId itemid);//destroy the node in wxTreeCtrl;
    void     Close();//only destroy the structure in wxTreeCtrl;

    bool                        ExpandNode(wxString &logVal);
    wxArrayString        GetAllLvVal();//this function is for projectgroup file
    wxArrayString  GetChdLv(wxTreeItemId itemid);
    wxTreeItemId    GetRootNode()const;
    bool      HasRoot()const {return (m_root != NULL);}

    bool            InitRoot(wxString nodeLogVal, SimuProject *pProject = NULL);
    bool            Insert(wxString &nodeLogVal, wxString &nodePhyVal); //the only function to create the tree.//nodeVal example : /mol, /mol/micro/arb
    friend bool     MergeTree(ProjectTree *treeM, ProjectTree *treeS, wxString nLogVal); //add tree as the childe of node of nLogVal
    bool            Remove(wxTreeItemId        nodeID);
    bool            Rename(wxTreeItemId        nodeID, wxString &nTitle);
    void     RmRoot();
    bool            RnRoot(wxString nodeLogVal);
    bool     Save();//save all projects in the tree;
    bool     Save(wxString nodeLogVal);//save item and its children
    wxString        UniqeName(wxString &title, wxString &phyPath, wxString logPath, bool isMerge = false);
public:
    void     DispNodeChild(wxString   nLogVal, wxTreeItemId parentId); //display the node in the wxTreeCtrl
    void     DispNodeChild(Node *curNode, wxTreeItemId parentId);
    void     DispTree();
public:
    bool            IsEmpty() {return (m_root == NULL);}
    bool     IsRoot(wxTreeItemId id);
private:
    bool      AnalysisPath(wxString &nodeLogVal, wxArrayString &nodeValArray); //decompress path into father-child struct
    bool             Destroy(Node *node);//release all the node and item in the tree

    Node    *Find(wxString nodeLogVal);
    Node    *Find(wxTreeItemId        nodeID);
    Node    *Find(Node *parent, wxString &nodeLogVal);
    bool             GetLvVal(Node *node, wxArrayString        &leaves);
    bool             Remove(wxString nodeLogVal);
    bool      RenameChild(Node *parent, wxString prtVal, int odPrtLen);
    bool      Save(Node *parent);
    bool      UpdateMgNdChdVal(Node *node, wxString addLogVal);
private:
    Node                                *m_root;//point to the project group tree
    wxTreeCtrl             *m_tree;
    ProjectGroup        *m_group;
};
// This class is designed to manage projects that have something in common ,to open, display,save,... them together.
//example: /home/project/mol/mol.spg
//                                                   ./mol/tiny/timy.sip
//                                                   ./mol/tiny_bSimuProject/tiny_b.sip
//                                                   ./mol/micro/arb/arb.sip
class ProjectGroup {
public:
    static wxString     GetFileFilter() {return wxT("Project Group File (*.spg)|*.spg");}
    static wxString     GetXmlFileRoot();
public:
    ProjectGroup(wxString &fullPathName, wxTreeCtrl *tree);
    //        ProjectGroup(const ProjectGroup & pg);
    ~ProjectGroup();
public:
    bool    InitTree(wxTreeCtrl *tree);
    bool    IsRoot(wxTreeItemId id)const {return m_pProjectTree->IsRoot(id);}
    wxTreeItemId    GetRootNode()const {return m_pProjectTree->GetRootNode();}
public:
    bool            AddProject(wxString &fullPathName, wxString &logPath, SimuProject *pProject); //fullPathName:  /home/project/.../...  ; logPath: /mol/tiny/...
    bool     AddPG(wxTreeItemId itemid, wxString &fullPathName);
    bool     AddLevel(wxString &nLogVal);
    void     CloseNode(wxTreeItemId itemid);//delete the node from the wxTreeCtrl
    void     Close(bool isAskForSave = true, bool default_save = false); //Close the project group and release the resource.
    bool     DeleteNode(wxTreeItemId id);//remove node from the project group and delete file from the disk;
    void     Display();
    void     InsertNode(wxTreeItemId parent, wxTreeItemId nodeID);
    bool            Load(bool disp = true); //load the .spg file and construct the tree;
    bool            Load(wxString fullPathName);//fullPathName example: /home/project/mol2/mol2.spg
    bool     RemoveNode(wxTreeItemId id);//do not delete  project file;
    bool            RenameNode(wxTreeItemId        nodeID, wxString &nTitle); //do not deleteold  project file;
    bool            Save();//save all in the project group to file;
    bool            SaveAs(wxString &fullPathName);//save whole project group as another
    bool     SaveAs(wxTreeItemId id, wxString fullPGPath); //save part of the project group as another.
public:
    SimuProject   *GetChild(wxString fullPathName);
    SimuProject   *GetChild(int projectId);
    ProjectsArray *GetChildArray()const {return m_pProjects;}
    wxString               GetPath()const {return m_dir;}
    wxString               GetName()const {return m_name;}
    wxString          GetFullPathName();
    wxArrayString GetProjectsList();
    ProjectTree    *GetTree()const {return m_pProjectTree;}
    SimuProject   *IsOpenChd(wxString prjFileName);
    SimuProject   *IsOpenPGChd(wxString pgFileName);
    bool                IsDirty();
    void                SetDirty(bool isDirty);
    wxString       UniqeName(wxString &title, wxString &phyPath, wxString logPath = wxEmptyString, bool isMerge = false);
private:
    bool                CheckProjects(wxArrayString &projects);//to check if all the projects belong to the project group is existing.
    bool                CheckPgFile(wxString &fullPathName);
    wxString   FpnToPath(wxString &fullPathName);
    wxString   FpnToName(wxString &fullPathName);
    bool                FileToTree(wxString fullPathName);//load the project group stucture to the tree
    wxString   GetPGFileName();
    bool                TreeToFile(wxString fullPathName);
    wxString   ToAbsPath(wxString &phyPath);
    wxString   ToPhyPath(wxString &fullPathName);
    bool          WritePGFile(wxString fullPathName, wxArrayString &astrprojects, wxString desc = wxEmptyString);
private:
    wxString        m_dir; //path of the project group, for example: /home/project/mol
    wxString        m_name;// name of project group, for example : mol
    ProjectsArray         *m_pProjects;//the child-project;
    ProjectTree        *m_pProjectTree;
    int           m_isDirty;
};
bool     MergeTree(ProjectTree *treeM, ProjectTree *treeS, wxString nLogVal);
#endif
