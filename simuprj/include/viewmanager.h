/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_VIEW_MANAGER_H__
#define __BP_PRJ_VIEW_MANAGER_H__

#include "wx.h"

#include <wx/aui/auibook.h>

#include "abstractview.h"
#include "simuproject.h"

class ViewManager : public wxEvtHandler {
public:
        static ViewManager* Get(void);
        static void Free(void);

        wxAuiNotebook* GetNotebook(void) { return m_pNotebook; }
    void    ActivateView(const wxString& filename);
    void    ActivateView(const int        PID);
        void    AddView(AbstractView* view, ProjectTreeItemType itemType = ITEM_UNKNOWN, wxString prefix = wxEmptyString);

        bool    CloseCurrentView(bool isAskForSave=true,bool default_save=false);
        //bool    CloseViewBPID(int projectId, wxString name = wxEmptyString);
        bool    CloseViewBPID(int projectId, bool isAskForSave=true,bool default_save=false);
        bool    CloseAllView(bool isAskForSave=true,bool default_save=false);
        // Get the view index.
    AbstractView* FindView(int projectId);
    int                  GetViewIndex(AbstractView* view);
        AbstractView* GetView(int index);
        AbstractView* GetView(const wxString& filename);
        AbstractView* GetActiveView(void);
        int      GetViewCount(void);

            bool    RenameView(int projectId,wxString nTitle);
        void    SaveViews(AbstractView* view);
        void    SaveView(int projectId);
        bool    SaveViewAs(int projectId,const wxString & fileNameFull);

        AbstractView* IsOpen(const wxString& filename);
        void    ShowStartHereView(bool isShow);
        void    ShowPrjInfoView(bool isShow);
        void    ShowMolView(ProjectTreeItemData* treeItem, wxString importFileName, bool isShow);
        bool    ShowJobView(ProjectTreeItemData* treeItem, wxString jobName,bool isShow=true,bool isNewjob=true);
    void    UpdateProjectTitle(SimuProject* project);
private:
        ViewManager();
        ~ViewManager();

        DECLARE_EVENT_TABLE();
        void    OnViewPageClose(wxAuiNotebookEvent& event);
        void    OnViewPageChanged(wxAuiNotebookEvent& event);
        void    OnTimerVibration(wxTimerEvent& event);
        bool    CloseView(int viewIndex,bool isAskForSave=true,bool default_save=false);

private:
        wxAuiNotebook* m_pNotebook;
        wxTimer m_timerVib;
};

#endif
