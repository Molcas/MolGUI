/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_STARTHERE_VIEW_H__
#define __BP_PRJ_STARTHERE_VIEW_H__

#include "wx.h"

#include <wx/html/htmlwin.h>

#include "abstractview.h"

extern const wxString global_startHereTitle;

class StartHereView : public AbstractView {
public:
                StartHereView(wxWindow* parent, wxEvtHandler* evtHandler);
                virtual ~StartHereView();
public:
                /**
                 * Load page from the start_here.zip file.
                 */
                virtual void InitView(void);

                bool LinkClicked(const wxHtmlLinkInfo& link);
                void SetPageContent(const wxString& buffer);
                /**
                  * ask event listener to perform any more substitutions (such as history files)
                  * if the parent performs substitutions, it should call SetPageContent() on this
                  */
                void Reload(void);


        private:
        void CreateGUI(void);


        protected:
        wxHtmlWindow* m_pHtmlWin;
        wxString m_originalPageContent;

};

#endif
