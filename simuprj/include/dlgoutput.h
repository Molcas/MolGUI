/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PRJ_DLGOUTPUT_H__
#define __BP_PRJ_DLGOUTPUT_H__

#include "wx.h"
#include <wx/html/htmlwin.h>
//begin : hurukun : 2/12/2009
#define  BUFFERSIZE 200
#define  OUTPUT_HTMLFILE_NAME wxT("output_result_file.html")
#define  OUTPUT_HTMLFILE_XML_NAME wxT("output_result_file_xml.html")

class InfoHtmlWin;
class MolcasFileDisp{
    #define  NUM_MOLCAS_FILE_MARK 4
//    #define  BUFFERSIZE 200
    #define  C_ERROR_L_EX 2  //when detect the error mark '#' line ,there is character of the line that is not '#', here is the num of this kind of character.
    #define  NUM_ERROR_L  4 //the number of error mark line.

    #define  ERROR_MARK     '#'
    #define  COMP_MARK_S '+'
    #define  COMP_MARK_E '-'
    #define  DCOL_MARK       ':'
        enum{//the type of mark come up in the molcas output file.
                MK_NONE=-1,
                MK_ERROR=0,
                MK_DBCOLON,
                MK_COMPACT
        };
        struct DispStyle{//struct to bind the mark type to it's print color;
                int                   mark;
                wxString        color;
                DispStyle(int m,wxString c)
                {
                        mark=m;
                        color=c;
                }
                DispStyle(const DispStyle &ds)
                {
                        mark=ds.mark;
                        color=ds.color;
                }
                ~DispStyle(){}
        };
public:
        MolcasFileDisp();
        ~MolcasFileDisp();
public:
        static void DispFile(const wxString& fileName,wxString description,InfoHtmlWin* pDispWin);//display the output file with rich style;
        static bool IsShowCompact(int i);
        static void ShowCompact(int i,bool flag=true);
private:
    static void    DispOutputFile(const wxString& fileName,InfoHtmlWin* pDispWin);
    static void    DispSimpleFile(const wxString& fileName,InfoHtmlWin* pDispWin);
    static void    DispXmlDumpFile(const wxString& fileName,InfoHtmlWin* pDispWin);
private:
    static wxString        GetColor(int mark);//get color by mark;
    static bool        ErrorMarkLine(char * buffer,int* tempVar);//mark  : ####...
    static bool        ErrorMarkEnd(char * buffer,int* tempVar); //mark : #####...
    static bool   CompMarkBegin(char * buffer);//mark : ++
    static bool   MatchCompMark(FILE * fp,wxArrayString & compcont);//to check if the compress mark is in pair;
    static bool   CompMarkEnd(char * buffer);//mark : --
    static bool        DBColonLine(char* buffer);//mark: ::
private:
        static DispStyle        m_dispStyle[NUM_MOLCAS_FILE_MARK];
           static int        m_markType;
    static bool* m_pShowCompMk;
};
//end     : hurukun : 2/12/2009

class DlgOutput : public wxFrame
{
public:
        DlgOutput(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Output"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(900,610), long style = wxCAPTION | wxCLOSE_BOX | wxMINIMIZE_BOX | wxRESIZE_BORDER | wxFRAME_FLOAT_ON_PARENT);
        virtual ~DlgOutput();
public:
        void    AppendFile(wxString fileName,wxString description);
        void    ScrollToBottom();
        void    DoOk(void);
        bool    LinkClicked(const wxHtmlLinkInfo& link);
private:
        void    CreateGUI(void);
private:
    DECLARE_EVENT_TABLE();

        void    OnClose(wxCloseEvent& event);
        void        OnOk(wxCommandEvent& event);
        void    OnResize(wxSizeEvent& event);
//        void    OnPopupMenu(wxContextMenuEvent& event);
        void        Draw();
private:
        InfoHtmlWin* m_pInfoWin;
        wxButton*   m_pOk;
        wxString    m_fileName;
        wxString    m_desc;
        bool                m_show;
};
//#define wxUSE_CLIPBOARD
class InfoHtmlWin : public wxHtmlWindow {
public:
        InfoHtmlWin(wxWindow * parent,DlgOutput* pClsEnvHdl, int id, const wxPoint& pos = wxDefaultPosition,
                                const wxSize& size = wxDefaultSize, long style = wxHW_SCROLLBAR_AUTO|wxBORDER_SUNKEN)
                                : wxHtmlWindow(parent, id, pos, size, style),
            m_pOwner(pClsEnvHdl) {
        }

                void OnLinkClicked(const wxHtmlLinkInfo& link) {
                    if (m_pOwner) {
                        if (!m_pOwner->LinkClicked(link)) {
                                        wxLaunchDefaultBrowser(link.GetHref());
                        }
                    }
                }
                void OnCopy(wxCommandEvent& event);
private:
        DlgOutput* m_pOwner;
private:
        DECLARE_EVENT_TABLE();
        void    OnPopupMenu(wxMouseEvent& event);
        void    OnTextCopy(wxCommandEvent& event);
        void    OnKeyEvent(wxKeyEvent& event);
};

class TimerDlg : public wxDialog{
public:
    TimerDlg(wxWindow * parent, int id=wxID_ANY, wxString title=wxT(""),const wxPoint& pos = wxDefaultPosition,
                                const wxSize& size = wxSize(100,60), long style = wxDEFAULT_DIALOG_STYLE);
    ~TimerDlg(){}
private:
    void    CreateUI();
private:
    DECLARE_EVENT_TABLE();
    void OnTimer(wxTimerEvent& event);
private:
    wxTimer m_timer;
};
#endif
