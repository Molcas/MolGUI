/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PV_IRFILE_H__
#define __BP_PV_IRFILE_H__

#include "wx.h"

#ifdef _DARWIN_
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif
#include "chematom.h"
#include "xyzfile.h"

class IRData{
public:
double frequency;
wxString irtype;
double intensity;
AtomPosition3FArray irArray;
IRData();
void RotateWithZ(AtomPosition3F &pos,double z);
void RotateWithY(AtomPosition3F &pos,double y);
void RotateWithX(AtomPosition3F &pos,double x);
void RotateIRData(double transX, double transY, double transZ,float rotx,float roty,float rotz);
void Translate(AtomPosition3F &pos,double transX, double transY, double transZ);

};


WX_DECLARE_OBJARRAY(IRData,IRDataArray);

class IRFile {
public:
    static void LoadFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels);
        static void SaveFile(const wxString fileName,IRDataArray irdataarray);
};

class IRDatas{
public:
        IRDataArray& GetDeltaArray();
        float GetEnhanceSwing();
        void SetEnhanceSwing(float swing);
bool LoadFile(const wxString& strFileName);
void SaveFile(const wxString fileName);
void DrawIRLine(void);
IRDataArray& GetIRDataArray(void);
IRData& GetOldCoord(void);
IRDatas();
~IRDatas();
private:
        float enhanceswing;
        IRDataArray m_irdataarray;
        IRData oldcoord;
        IRDataArray m_deltaArray;
        int nStep;
};
#endif
