/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_PV_VIBRATION_H__
#define __BP_PV_VIBRATION_H__
#include "wx.h"
//#include "utils.h"
#include "chematom.h"
class VibPropertyDisItem
{
public:
        double Frequency;
        wxString Symmetry;
        double Intensity;
        VibPropertyDisItem()
        {
                Frequency=0.0;
                Symmetry=wxEmptyString;
                Intensity=0.0;
        }
        wxString ToString()
        {
                return wxString::Format(wxT("%f\t"),Frequency)
                                +Symmetry+wxString::Format(wxT("\t%f"),Intensity);
        }
};
WX_DECLARE_OBJARRAY(VibPropertyDisItem, VibPropertyDisItemArray);

class AtomInfo
{
public:
        double x,y,z;
        wxString type;
        AtomInfo()
        {
                x=y=z=0.0;
                type=wxEmptyString;
        }
        wxString ToString()
        {
                return type+wxString::Format(wxT(" \t %.4f\t %.4f\t %.4f"),x,y,z);
        }
};
WX_DECLARE_OBJARRAY(AtomInfo, AtomInfoArray);

class ModeForm{
public:
        ModeForm()
        {
                AtomPair=wxEmptyString;
                EnergyContribution=wxEmptyString;
                Radial=wxEmptyString;
        }
        wxString ToString(void)
        {
                return AtomPair+wxT("\t")+EnergyContribution+wxT("\t")+Radial;
        }
public:
        wxString AtomPair;
        wxString EnergyContribution;
        wxString Radial;
};
WX_DECLARE_OBJARRAY(ModeForm, ModeFormArray);

class VibrationItem{
public:
        VibrationItem()
        {
                ID=-1;
                Name=wxEmptyString;
                Freq=0.0;
                T_Dipole=0.0;
                Travel=0.0;
                RedMass=0.0;
                ModeForms.Clear();
                //NormalCoors.Clear();
                //MassWeCoors.Clear();
        }
        wxString ToString(void)
        {
                wxString str,temp;
                unsigned i;
                str=wxString::Format(wxT("%d\t"),ID)+Name;
                str+=wxString::Format(wxT("\nFreq:%f\tT-Dipole:%f\tTravel:%f\tRedMass:%f\n"),Freq,T_Dipole,Travel,RedMass);
                str+=wxT("Atom Pair\tEnergy Contribution\tRadial\n");
                for(i=0;i<ModeForms.GetCount();i++)
                        str+=ModeForms[i].ToString()+wxT("\n");
                str+=wxT("NORMAL COORDINATE ANALYSIS\n");
/*                temp=wxEmptyString;
                for(i=0;i<NormalCoors.GetCount();i++)
                        temp+=wxString::Format(wxT("%f\t"),NormalCoors[i]);
                str+=temp;
                str+=wxT("\nMASS-WEIGHTED COORDINATE ANALYSIS\n");
                temp=wxEmptyString;
                for(i=0;i<MassWeCoors.GetCount();i++)
                        temp+=wxString::Format(wxT("%f\t"),MassWeCoors[i]);
                str+=temp;
*/                return str;
        }
public:
        int ID;
        wxString Name;
        double Freq;
        double T_Dipole;
        double Travel;
        double RedMass;
        ModeFormArray ModeForms;
        //wxDoubleArray NormalCoors;
        //wxDoubleArray MassWeCoors;
};
WX_DECLARE_OBJARRAY(VibrationItem, VibrationItemArray);
class VibrationCoors{
public:
        int index,id;
        double Freq;
        wxString type;
        AtomPositionArray Coors;
        VibrationCoors()
        {
                id=index=BP_NULL_VALUE;
                Freq=0.0;
                type=wxEmptyString;
                Coors.Clear();
        }
        wxString ToString()
        {
                wxString ret=wxString::Format(wxT("Index=%d\tID=%d\tType="),index,id)+type;
                ret+=wxString::Format(wxT("\nFreq=%f\n"),Freq);
                ret+=ConvertToString(Coors);
                return ret;
        }
        static wxString ConvertToString(const AtomPositionArray& posArray)
        {
                wxString ret=wxEmptyString;
                for(unsigned i=0;i<posArray.GetCount();i++)
                {
                        ret+=wxString::Format(wxT("x=%f,y=%f,z=%f"),posArray[i].x,posArray[i].y,posArray[i].z);
                }
                return ret;
        }
};
WX_DECLARE_OBJARRAY(VibrationCoors, VibrationCoorsArray);

class Vibration
{
public:
        AtomInfoArray OrientCoors;
        VibrationItemArray VibItems;
        VibrationCoorsArray NormalCoors;
        Vibration()
        {
                VibItems.Clear();
                OrientCoors.Clear();
                NormalCoors.Clear();
        }
};
class VibFile
{
public:
        static void SaveFile(const wxString& fileName,const Vibration &vibInfo);
};
extern const wxString SEMI_OUT_VIBRATION_ORIENTATION_COORDINATE_FLAG;
extern const wxString SEMI_OUT_VIBRATION_NORMAL_COORDINATE_FLAG;
extern const wxString SEMI_OUT_VIBRATION_COORDINATE_FLAG;
extern const wxString SEMI_OUT_END_FLAG;
void ConstructSemiVibrationOrientCoors(AtomInfoArray& infos,const wxArrayString& data);
void ConstructSemiCoodinateAnalysis(VibrationCoorsArray& vibCoors,const wxArrayString& data);
void ConstructAbinCoodinateAnalysis(VibrationCoorsArray& vibCoors,const wxArrayString& data);
void ConstructAbinVibrationCoors(AtomInfoArray& infos,const wxArrayString& data);

#endif
