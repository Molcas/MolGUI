/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/dir.h>
#include <wx/datetime.h>

#include "projectgroup.h"
#include "envutil.h"
#include "fileutil.h"
#include "xmlfile.h"
#include "manager.h"
#include "projectmanager.h"
#include "simuprjdef.h"
#include "stringutil.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                        class ProjectTreeItemData
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ProjectTreeItemData::ProjectTreeItemData(SimuProject *project, ProjectGroup         *group, wxString *pLogPath, ProjectTreeItemType type)
{
    m_pProject = project;
    m_pPrjGrp = group;
    m_pPath = pLogPath;
    m_type = type;
}
ProjectTreeItemData::ProjectTreeItemData(SimuProject *project, wxString  logPath, ProjectTreeItemType type )
{
    m_pProject = project;
    m_pPrjGrp = NULL;
    m_pPath = new wxString(logPath);
    m_type = type;
}
ProjectTreeItemData::~ProjectTreeItemData()
{
    if(m_pProject != NULL)//the projects are managed and deleted in ProjectManager.
        m_pProject = NULL;
    m_pPrjGrp = NULL;
    if(m_type != ITEM_PROJECT && m_type != ITEM_PROJECTGROUP && m_type != ITEM_PROJECTDIR && m_pPath != NULL)
        delete m_pPath;
    m_pPath = NULL;
}
wxString ProjectTreeItemData::GetFileName(SimuProject *project, wxString jobName, ProjectTreeItemType itemType)
{
    if(project == NULL) {
        return wxEmptyString;
    }
    wxArrayString denslist;
    wxString dir = project->GetPathWithSep();
    wxString fileName = project->GetTitle();
    wxString fileExt = wxEmptyString;
    switch(itemType) {
    case ITEM_UNKNOWN:
    case ITEM_PROJECT:
    case ITEM_PROJECTGROUP:
    case ITEM_PROJECTDIR:
    case ITEM_JOB_RESULT:
        break;
    case ITEM_MOLECULE://.sim
        fileExt += wxMSD_FILE_SUFFIX;
        break;
    case ITEM_JOB_INFO:
        switch(project->GetType()) {
        case PROJECT_MOLCAS:
            fileExt += wxMINPUT_FILE_SUFFIX;
            break;
        case PROJECT_QCHEM://.xyz
            fileExt += wxMCD_FILE_SUFFIX;
            break;
        default:
            fileExt += wxT(".jinfo");
            break;
        }
        break;
    case ITEM_NEW_JOB:
        switch(project->GetType()) {
        case PROJECT_MOLCAS:
            fileExt += wxMINPUT_FILE_SUFFIX;
            break;
        case PROJECT_QCHEM://.xyz
            fileExt += wxMCD_FILE_SUFFIX;
            break;
        default:
            fileExt += wxT(".jinfo");
            break;
        }
        break;
    case ITEM_JOB_RESULT_SUMMARY:
        fileExt += wxT(".sum");
        break;
    case ITEM_JOB_RESULT_OUTPUT:
        fileExt += project->GetOutfileSuffix();
        break;
    case ITEM_JOB_RESULT_DENSITY:
        denslist = project->GetFiles(jobName)->GetSurfaceDataFiles();
        if(denslist.IsEmpty())
            fileExt = wxEmptyString;
        else
            fileExt = denslist[0];
        return fileExt;
    //break;
    case ITEM_JOB_RESULT_FREQUENCE:
        fileExt += wxT(".vib");
        break;
    default:
        break;
    }
    if(fileExt.IsEmpty()) {
        return wxEmptyString;
    } else {
        if(jobName.IsEmpty()) {
            return dir + fileName + fileExt;
        } else {
            return dir + jobName + platform::PathSep() + jobName + fileExt;
        }
    }
}
wxString        ProjectTreeItemData::GetFileName(void) const
{
    if(m_type == ITEM_MOLECULE || ((m_type == ITEM_JOB_INFO || m_type == ITEM_NEW_JOB) && (m_pProject->GetType() == PROJECT_QCHEM)))
        return GetFileName(m_pProject, wxEmptyString, m_type);
    else
        return GetFileName(m_pProject, GetTitle(), m_type);
}
wxString ProjectTreeItemData::GetTitle(void) const
{
    return m_pPath->AfterLast(platform::PathSep().GetChar(0));
}
void                  ProjectTreeItemData::SetItemVal(wxString &nLogVal)
{
    *m_pPath = nLogVal;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                        project tree;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ProjectTree::ProjectTree(wxTreeCtrl *tree, ProjectGroup *group)
{
    m_tree = tree;
    m_group = group;
    m_root = NULL;
}
ProjectTree::~ProjectTree()
{
    m_tree = NULL;
    m_group = NULL;
    if(m_root != NULL)
        Destroy( m_root);
    m_root = NULL;
}
//*****************************************************************************
//                        private functions;                                                                                                                           *
//*****************************************************************************
bool     ProjectTree::AnalysisPath(wxString &nodeLogVal, wxArrayString &nodeValArray)
{
    wxString    val = nodeLogVal;
    bool    hasProject = false;
    val = nodeLogVal.Right(PRJ_FILE_SUFFIX_LEN);
    if(!val.Cmp(wxPRJ_FILE_SUFFIX))//nodeVal contain .sip.
    {
        hasProject = true;
        val = nodeLogVal.BeforeLast(platform::PathSep().GetChar(0));
        nodeValArray.Add(val);
    } else {
        nodeValArray.Add(nodeLogVal);
        val = nodeLogVal;
    }
    val = val.BeforeLast(platform::PathSep().GetChar(0));
    while(!val.IsEmpty()) {
        nodeValArray.Add(val);
        val = val.BeforeLast(platform::PathSep().GetChar(0));
    }
    return hasProject;
}
bool        ProjectTree::Destroy(ProjectTree::Node *node)//dlete node ad its child node
{
    if(node == NULL)
        return true;
    if(node->cnum > 0)
    {
        Node *tmp;
        for(int i = 0; i < node->cnum; i++) {
            tmp = node->chd[i];
            Destroy(tmp);
        }
        node->cnum = 0;
    }
    if(node->chd != NULL)
        delete node->chd;
    node->chd = NULL;
    //  if(node->nodeID.IsOk()&&m_tree!=NULL)
    //      m_tree->Delete(node->nodeID);
    delete node;
    node = NULL;
    return true;
}
ProjectTree::Node *ProjectTree::Find(wxString nodeLogVal)    //e.g:  /mol/tiny
{
    wxArrayString         nodeValArray;
    wxString        val = wxEmptyString;
    AnalysisPath(nodeLogVal, nodeValArray);

    val = m_root->nodeLogVal;

    int        num = nodeValArray.GetCount();
    if(val.IsSameAs(nodeValArray[0]))
        return m_root;
    int        index = 1;
    //wxMessageBox(val);
    //wxMessageBox(nodeValArray[num-index]);
    if(val.Cmp(nodeValArray[num - index])) //the inserted node's root path is wrong
        return NULL;

    index++;
    Node *tmpParent = m_root;
    Node *tmp = NULL;
    while(index <= num) {
        tmp = Find(tmpParent, nodeValArray[num - index]);
        if(tmp == NULL)
            break;
        index++;
        tmpParent = tmp;
    }
    return tmp;
}
ProjectTree::Node *ProjectTree::Find(wxTreeItemId        nodeID)
{
    if(m_root == NULL)
        return NULL;
    if(nodeID == m_root->nodeID)
        return m_root;
    ProjectTreeItemData *item = NULL;

    item = m_root->nodeID.IsOk() ? (ProjectTreeItemData *)m_tree->GetItemData( nodeID) : NULL;
    if(item == NULL)
        return NULL;
    wxString        val = item->GetItemVal();
    return Find(val);
}
ProjectTree::Node *ProjectTree::Find(ProjectTree::Node *parent, wxString &nodeLogVal)
{
    if(parent == NULL)
        return NULL;
    Node        *tmp = NULL;
    wxString        val = wxEmptyString;
    for(int i = 0; i < parent->cnum; i++) {
        tmp = parent->chd[i];
        val = tmp->nodeLogVal;
        if(val.IsSameAs(nodeLogVal))
            return tmp;
    }
    return NULL;
}
bool        ProjectTree::GetLvVal(ProjectTree::Node *node, wxArrayString        &leaves)
{
    Node *tmp;
    if(node == NULL)
        return false;
    if(node->cnum == 0)
    {
        if(node == m_root)
            return true;
        leaves.Add(node->nodeLogVal);///?????????????????????
        leaves.Add(node->nodePhyVal);
        return true;
    }
    for(int i = 0; i < node->cnum; i++) {
        tmp = node->chd[i];
        GetLvVal(tmp, leaves);
    }
    return true;
}
bool            ProjectTree::Remove(wxString nodeLogVal)//delete node and its children by its nodeVal
{
    if(m_root == NULL)
        return false;
    if(nodeLogVal.IsEmpty())
        return false;
    wxArrayString         nodeValArray;
    wxString        val;
    AnalysisPath(nodeLogVal, nodeValArray);

    val = m_root->nodeLogVal;
    int        num = nodeValArray.GetCount();
    int        index = 1;
    if(val.Cmp(nodeValArray[num - index])) //the delete node's root path is wrong
        return false;

    index++;
    Node        *tmpParent = m_root, *tmp;
    while(index <= num) {
        tmp = Find(tmpParent, nodeValArray[num - index]);
        if(tmp == NULL)
            return false;
        if(index == num) //reach the node
        {
            bool    bmove = false;
            for(int i = 0; i < tmpParent->cnum; i++) {
                if(tmpParent->chd[i] == tmp)
                {
                    if(!Destroy(tmp))//iteratally
                        return false;
                    bmove = true;
                }
                if(bmove == true && i < tmpParent->cnum - 1)
                    tmpParent->chd[i] = tmpParent->chd[i + 1];
            }
            tmpParent->cnum--;
            if(tmpParent->cnum > 0 && tmpParent->cnum % DEFAULT_CHD_NUM == 0)
            {
                Node        **pchd = new Node*[tmpParent->cnum];
                if(pchd == NULL)
                    return false;
                if(tmpParent->chd == NULL)
                    return false;
                for(int i = 0; i < tmpParent->cnum; i++)
                    pchd[i] = tmpParent->chd[i];
                delete        tmpParent->chd;
                tmpParent->chd = pchd;
                pchd = NULL;
            }
            break;
        }
        index++;
        tmpParent = tmp;
    }
    return true;
}
bool     ProjectTree::RenameChild(Node *parent, wxString prtVal, int odPrtLen) //..??
{   //only change the logval of the child not the physic value;
    Node *tmp = NULL;
    int tmpLen = 0;
    for(int i = 0; i < parent->cnum; i++) {
        tmp = parent->chd[i];
        tmpLen = tmp->nodeLogVal.Len() - odPrtLen;
        if(tmpLen < 0)
            return false;
        tmp->nodeLogVal = prtVal + tmp->nodeLogVal.Right(tmpLen);
        //wxMessageBox(prtVal+wxT("--prtVal"));
        //wxMessageBox(tmp->nodeLogVal+wxT("--tmp->nodeLogVal"));
        // ...........................change the phyval.............................//
        if(tmp->nodePhyVal.IsEmpty() == false && tmp->nodePhyVal.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
        {
            wxString         oldPhyVal = tmp->nodePhyVal;
            wxChar      pathSep = platform::PathSep().GetChar(0);

            wxString         tmpPJTitle = tmp->nodePhyVal.AfterLast(pathSep);
            tmpPJTitle = tmpPJTitle.Left(tmpPJTitle.Len() - PRJ_FILE_SUFFIX_LEN);
            wxString         tmpPJDir = tmp->nodePhyVal.BeforeLast(pathSep).AfterLast(pathSep);

            wxString        tmpLogVal = tmp->nodeLogVal.BeforeLast(pathSep);

            tmp->nodePhyVal = tmp->nodePhyVal.BeforeLast(pathSep).BeforeLast(pathSep);
            //wxMessageBox(tmpPJTitle+wxT("--tmpPJTitle"));
            //wxMessageBox(tmpPJDir+wxT("--tmpPJDir"));
            //wxMessageBox(tmpLogVal+wxT("--tmpLogVal"));
            //wxMessageBox(tmp->nodePhyVal+wxT("--tmp->nodePhyVal"));
            if(prtVal.AfterFirst(pathSep).AfterFirst(pathSep).IsEmpty()) { //the root node has been renamed, so the physical path need to be changed.
                wxString rootTitle = prtVal.AfterFirst(platform::PathSep().GetChar(0));
                tmp->nodePhyVal = tmp->nodePhyVal.BeforeLast(pathSep);
                tmp->nodePhyVal = tmp->nodePhyVal + platform::PathSep() + rootTitle;
            }
            if(!tmpPJTitle.IsSameAs(tmpPJDir)) { //there shall be no '@' in the physical path
                tmp->nodePhyVal = tmp->nodePhyVal + platform::PathSep() + tmpPJTitle;

                wxString        tmpStr = tmpLogVal.AfterLast(pathSep);
                while(!tmpStr.IsEmpty()) {
                    tmpLogVal = tmpLogVal.BeforeLast(pathSep);
                    if(tmpLogVal.IsEmpty())
                        break;
                    tmp->nodePhyVal = tmp->nodePhyVal + wxT("@") + tmpStr;
                    tmpStr = tmpLogVal.AfterLast(pathSep);
                }
                tmp->nodePhyVal = tmp->nodePhyVal + platform::PathSep() + tmpPJTitle + wxPRJ_FILE_SUFFIX;
            } else {
                tmp->nodePhyVal = tmp->nodePhyVal + platform::PathSep() + tmpPJDir + platform::PathSep() + tmpPJTitle + wxPRJ_FILE_SUFFIX;;
            }
            //wxMessageBox(tmp->nodePhyVal+wxT("--tmp->nodePhyVal"));
            if(!oldPhyVal.IsSameAs(tmp->nodePhyVal)) {
                wxString    oldDir = oldPhyVal.BeforeLast(pathSep);
                wxString    newDir = tmp->nodePhyVal.BeforeLast(pathSep);
                FileUtil::CopyDirectory(oldDir, newDir);
                FileUtil::RemoveDirectory(oldDir);

                wxTreeItemId        nodeID = tmp->nodeID;
                ProjectTreeItemData *item = NULL;
                item = nodeID.IsOk() ? (ProjectTreeItemData *)m_tree->GetItemData( nodeID) : NULL;
                if(item != NULL)
                    item->GetProject()->SetFileName(tmp->nodePhyVal);
            }
            //wxMessageBox(tmp->nodePhyVal+wxT("--tmp->nodePhyVal"));

            //            wxString path=tmp->nodePhyVal.AfterLast(pathSep).BeforeLast(wxT('.'));
            //            SimuProject* pP=m_group->GetChild(tmp->nodePhyVal);
            //            path+=platform::PathSep()+path+wxPRJ_FILE_SUFFIX;
            //            tmp->nodePhyVal=tmp->nodePhyVal.BeforeLast(pathSep).BeforeLast(pathSep).BeforeLast(pathSep);
            //            if( tmp->nodePhyVal.IsEmpty()==false){
            //                tmp->nodePhyVal+=platform::PathSep()+prtVal.AfterLast(pathSep)+platform::PathSep()+path;
            //                if(pP!=NULL)
            //                    pP->SetFileName(tmp->nodePhyVal);
            //            }
        } else { //logical value
            tmp->nodePhyVal = wxT(".") + tmp->nodeLogVal;
        }
        //....................................................................................//
        RenameChild(tmp, prtVal, odPrtLen);
    }
    return true;
}
bool    ProjectTree::Save(Node *parent)
{
    Node    *tmp;
    if(parent->cnum == 0) //reach the leaf;
    {
        ProjectTreeItemData *item = parent->nodeID.IsOk() ? (ProjectTreeItemData *)m_tree->GetItemData(parent->nodeID) : NULL;
        if(item == NULL)
            return false;
        SimuProject *pProject = item->GetProject();
        if(pProject != NULL)
            return pProject->Save();
    } else {
        for(int i = 0; i < parent->cnum; i++) {
            tmp = parent->chd[i];
            Save(tmp);
        }
    }
    return true;
}
//******************************************************************************
//                            public functions;                                                                                                                           *
//******************************************************************************
void     ProjectTree::Close()
{
    m_tree->DeleteChildren(m_root->nodeID);
    m_tree->Delete(m_root->nodeID);
    Destroy(m_root);
    m_root = NULL;
}
void     ProjectTree::CloseNode(wxTreeItemId itemid)
{
    Node *tmp = Find(itemid);
    if(tmp == NULL)
        return;
    m_tree->DeleteChildren(tmp->nodeID);
    m_tree->Delete(tmp->nodeID);
    //    tmp->nodeID=0;
}
bool            ProjectTree::Clear()//remove all node except the root node;
{
    return Destroy(m_root);
}
void    ProjectTree::DispNodeChild(Node *curNode, wxTreeItemId parentId)
{
    if(curNode == NULL)
        return;
    if(!curNode->nodeID.IsOk())
    {
        //display the parent node;
        ProjectTreeItemData *itemData;
        wxString phyVal = curNode->nodePhyVal;
        ProjectTreeItemType nodeType = ITEM_PROJECTDIR;
        bool isProject = false;
        if(phyVal.IsEmpty()) {
            nodeType = ITEM_PROJECTDIR;
        } else if(phyVal.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)) {
            nodeType = ITEM_PROJECT;
            isProject = true;
        } else if(phyVal.Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX)) {
            nodeType = ITEM_PROJECTGROUP;
        }
        SimuProject *pProject = NULL;
        if(isProject == false)
            itemData = new ProjectTreeItemData(NULL, m_group, &(curNode->nodeLogVal), nodeType);
        else {
            pProject = m_group->GetChild(curNode->nodePhyVal);
            if(pProject == NULL)
                wxMessageBox(wxT("Error"));
            itemData = new ProjectTreeItemData(pProject, m_group, &(curNode->nodeLogVal), nodeType);
        }

        if(itemData == NULL)
            return ;
        int imgId = 1; //project;
        if(nodeType != ITEM_PROJECT)
            imgId = 8;
        curNode->nodeID = m_tree->AppendItem(parentId, curNode->nodeLogVal.AfterLast(platform::PathSep().GetChar(0)), imgId, imgId, itemData);

        if(isProject == true)
        {   //build project tree;
            pProject->SetNodeId(curNode->nodeID);
            pProject->BuildTree(m_tree);
            pProject->LoadJobs();
            return;
        }
    }
    //display the child node;
    Node *child = NULL;
    for(int i = 0; i < curNode->cnum; i++) {
        child = curNode->chd[i];
        DispNodeChild(child, curNode->nodeID);
    }
}
void    ProjectTree::DispNodeChild(wxString   nLogVal, wxTreeItemId parentId)
{
    Node *node = Find(nLogVal);
    if(node == NULL)
        return;
    DispNodeChild(node, parentId);
}
void    ProjectTree::DispTree()
{
    if(m_root == NULL)
        return;
    DispNodeChild(m_root, Manager::Get()->GetProjectManager()->GetTreeRoot());
}
bool        ProjectTree::ExpandNode(wxString &logVal)
{
    if(m_tree == NULL)
        return false;
    Node *node = Find(logVal);
    wxTreeItemId        item = node->nodeID;
    m_tree->Expand(item);
    return true;
}
wxArrayString        ProjectTree::GetAllLvVal()
{
    wxArrayString        leaves;
    Node *tmp = m_root;
    GetLvVal(tmp, leaves);
    return leaves;
}
wxArrayString  ProjectTree::GetChdLv(wxTreeItemId itemid)
{
    Node *node = Find(itemid);
    wxArrayString lv;
    lv.Clear();
    GetLvVal(node, lv);
    return lv;
}
wxTreeItemId   ProjectTree::GetRootNode()const
{
    if(m_root != NULL)
        return m_root->nodeID;
    else
        return 0;
}
bool            ProjectTree::InitRoot(wxString nodeLogVal, SimuProject *pProject)// e.g: nodeVal=/mol
{
    if(m_root == NULL)
        m_root = new        Node;
    if(m_root == NULL || m_tree == NULL)
        return false;
    m_root->nodeLogVal = nodeLogVal;
    m_root->nodePhyVal = wxT(".") + nodeLogVal;
    ProjectTreeItemData *itemData = new ProjectTreeItemData(pProject, m_group, &(m_root->nodeLogVal), ITEM_PROJECTGROUP);
    wxTreeItemId wsId = m_tree->GetRootItem();
    m_root->nodeID = m_tree->AppendItem(wsId, nodeLogVal.AfterLast(platform::PathSep().GetChar(0)), 8, 8, itemData);
    return true;
}
bool            ProjectTree::Insert(wxString &nodeLogVal, wxString &nodePhyVal)
{   //e.g : nodeLogVal=/mol/tiny/tiny.sip  (project); nodeLogVal=/mol/tiny (dir):
    //nodePhyVal=./mol/tiny/tiny.sip or /home/project/mol/tiny/tiny.sip(project);nodePhyVal=/mol/tiny (dir)
    if(m_root == NULL)
        return false;
    if(nodeLogVal.IsEmpty())
        return false;
    wxArrayString         nodeValArray;
    wxString        val;
    AnalysisPath(nodeLogVal, nodeValArray);

    val = m_root->nodeLogVal;

    int        num = nodeValArray.GetCount();
    if(num < 1)
        return false;
    int        index = 1;
    if(val.Cmp(nodeValArray[num - index])) //the inserted node's root path is wrong
        return false;

    index++;
    Node        *tmpParent = m_root, *tmp;
    while(index < num) {
        tmp = Find(tmpParent, nodeValArray[num - index]);
        if(tmp == NULL) //find the exits node path in the tree; tmpParent
            break;
        if(!tmp->nodePhyVal.IsEmpty() && tmp->nodePhyVal.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)) //a project dir can not have children.
            return false;
        index++;
        tmpParent = tmp;
    }
    while(index <= num) {
        tmp = new Node; //new node.
        if(tmp == NULL)
            return false;
        tmp->nodeLogVal = nodeValArray[num - index];
        if(index == num && !nodePhyVal.IsEmpty() && nodePhyVal.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)) {
            tmp->nodePhyVal = nodePhyVal;
        } else
            tmp->nodePhyVal = wxT(".") + tmp->nodeLogVal;

        tmpParent->cnum++;
        if(tmpParent->cnum > DEFAULT_CHD_NUM && tmpParent->cnum % DEFAULT_CHD_NUM == 1) // to molloc new memery
        {
            Node        **pchd = new Node*[tmpParent->cnum - 1 + DEFAULT_CHD_NUM];
            if(pchd == NULL)
            {
                tmpParent->cnum--;
                return false;
            }
            if(tmpParent->chd != NULL)
            {
                for(int i = 0; i < tmpParent->cnum - 1; i++) {
                    if(tmpParent->chd[i] != NULL)
                        pchd[i] = tmpParent->chd[i];
                }
                delete        tmpParent->chd;
            }
            tmpParent->chd = pchd;
            pchd = NULL;
        }
        tmpParent->chd[tmpParent->cnum - 1] = tmp;
        tmpParent = tmp;
        index++;
    }
    tmp = NULL;
    return true;
}
bool     MergeTree(ProjectTree *treeM, ProjectTree *treeS, wxString nLogVal)
{
    if(treeM == NULL || treeS == NULL || nLogVal.IsEmpty())
        return false;
    ProjectTree::Node *node = treeM->Find(nLogVal);
    if(node == NULL)
        return false;
    //connect the node
    node->cnum++;
    if(node->cnum > DEFAULT_CHD_NUM && node->cnum % DEFAULT_CHD_NUM == 1) // to molloc new memery
    {
        ProjectTree::Node        **pchd = new ProjectTree::Node*[node->cnum - 1 + DEFAULT_CHD_NUM];
        if(pchd == NULL)
        {
            node->cnum--;
            return false;
        }
        if(node->chd != NULL)
        {
            for(int i = 0; i < node->cnum - 1; i++) {
                if(node->chd[i] != NULL)
                    pchd[i] = node->chd[i];
            }
            delete        node->chd;
        }
        node->chd = pchd;
        pchd = NULL;
    }
    treeS->m_tree->Delete(treeS->m_root->nodeID);
    treeS->m_root->nodeID = 0;
    node->chd[node->cnum - 1] = treeS->m_root;
    //update the nodeLogVal of the new tree root
    treeS->m_root->nodeLogVal = nLogVal + treeS->m_root->nodeLogVal;
    //update the value of node's children;
    treeM->UpdateMgNdChdVal(treeS->m_root, nLogVal);
    return true;
}
bool     ProjectTree::UpdateMgNdChdVal(Node *node, wxString addLogVal)
{
    for(int i = 0; i < node->cnum; i++) {
        node->chd[i]->nodeLogVal = addLogVal + node->chd[i]->nodeLogVal;
        UpdateMgNdChdVal(node->chd[i], addLogVal);
    }
    return true;
}
bool     ProjectTree::IsRoot(wxTreeItemId id)
{
    return (m_root->nodeID == id);
}
bool            ProjectTree::Remove(wxTreeItemId        nodeID)//delete node and its children by its nodeID
{
    if(m_root->nodeID == nodeID)
        return Destroy(m_root);

    ProjectTreeItemData *item = NULL;
    item = nodeID.IsOk() ? (ProjectTreeItemData *)m_tree->GetItemData( nodeID) : NULL;
    if(item == NULL)
        return false;
    wxString        val = item->GetItemVal();

    CloseNode(nodeID);
    return Remove(val);
}
void     ProjectTree::RmRoot()
{
    m_tree->Delete(m_root->nodeID);
    //   Destroy(m_root);
}
bool            ProjectTree::RnRoot(wxString nodeLogVal)
{
    m_root->nodeLogVal = nodeLogVal;
    m_root->nodePhyVal = wxT(".") + nodeLogVal;
    return true;
}
bool     ProjectTree::Rename(wxTreeItemId        nodeID, wxString &nTitle)
{   //need to change all the value in its children, and refresh the wxTreeCtrl;
    if(nTitle.IsEmpty())
        return false;
    wxString outString = StringUtil::CheckString(nTitle);
    //        if(!outString.IsSameAs(nTitle))
    //    {
    //        wxMessageBox(wxT("Only the following character is legal: '0-9','a-z','A-Z','_'."));
    //        return false;
    //    }
    //wxMessageBox(outString);
    ProjectTreeItemData *item = NULL;
    item = nodeID.IsOk() ? (ProjectTreeItemData *)m_tree->GetItemData( nodeID) : NULL;
    if(item == NULL)
        return false;
    wxString        logVal = item->GetItemVal();
    SimuProject *pProject = item->GetProject();
    int     oldLogValLen = logVal.Len();

    wxString tmpLogVal = logVal.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + outString;
    //wxMessageBox(tmpLogVal);
    Node *tmpNode = Find(tmpLogVal);
    if(tmpNode != NULL) {
        return false;
    }

    Node *node = NULL;
    if(nodeID == m_root->nodeID)
        node = m_root;
    else
        node = Find(nodeID);
    if(node == NULL)  return false;
    if(!node->nodePhyVal.IsEmpty() && node->nodePhyVal.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX) && pProject != NULL) //update the phyval and the project
    {   //deal with the project node
        wxChar      pathsep = platform::PathSep().GetChar(0);
        wxString tmpPhyVal = node->nodePhyVal.BeforeLast(pathsep).BeforeLast(pathsep) + platform::PathSep() + outString + platform::PathSep() + outString + wxPRJ_FILE_SUFFIX;
        //wxMessageBox(nTitle);
        //wxMessageBox(tmpLogVal);
        //wxMessageBox(tmpPhyVal);
        tmpLogVal = UniqeName(nTitle, tmpPhyVal, tmpLogVal);
        //wxMessageBox(nTitle);
        //wxMessageBox(tmpLogVal);
        //wxMessageBox(tmpPhyVal);


        wxString    oldDir = node->nodePhyVal.BeforeLast(pathsep);
        wxString    newDir = tmpPhyVal.BeforeLast(pathsep);
        FileUtil::CopyDirectory(oldDir, newDir);
        FileUtil::RemoveDirectory(oldDir);
        pProject->SetTitle(outString);
        pProject->SetFileName(tmpPhyVal);
        //        if(pProject->Rename(nTitle)==false)
        //            return false;
        node->nodePhyVal = tmpPhyVal; //do not change the directory
    }
    logVal = tmpLogVal;
    /*        {// hurukun 0418
            wxMessageDialog mdlg(NULL, wxT("A level with the same name exists, would you like them to be merged the next time the project group is loaded?"),wxT("Warning"),wxYES_NO);
            if(mdlg.ShowModal()!=wxID_YES)
                return false;
        }*/
    //        if(RenameChild(node,logVal,oldLogValLen)==false)//rename the child
    //            return false;
    //        //merge them
    //        CloseNode(nodeID);
    //        int    chdNum=node->cnum+tmpNode->cnum;
    //        chdNum=((int)(chdNum/DEFAULT_CHD_NUM)+1)*DEFAULT_CHD_NUM;
    //        Node**        pchd=new Node*[chdNum];
    //        chdNum=0;
    //        for(int i=0;i<tmpNode->cnum;i++){
    //            pchd[chdNum]=tmpNode->chd[i];
    //            chdNum++;
    //        }
    //        for(int i=0;i<node->cnum;i++){
    //            pchd[chdNum]=node->chd[i];
    //            chdNum++;
    //        }
    //        tmpNode->cnum=tmpNode->cnum+node->cnum;
    //        if(node->chd!=NULL){
    //            delete node->chd;logVal=logVal.BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+nTitle;
    //    Node * tmpNode=Find(logVal);
    //    if(tmpNode!=NULL){
    //                return false;
    //        }
    //            node->cnum=0;
    //            node->chd=NULL;
    //        }
    //        if(tmpNode->chd!=NULL)
    //            delete tmpNode->chd;
    //        tmpNode->chd=pchd;
    //        Remove(node->nodeLogVal);
    //        DispTree();
    //        return true;
    //    }
    item->SetItemVal(logVal);//update logval
    m_tree->SetItemText(nodeID, outString);
    //deal with the dir node
    return RenameChild(node, logVal, oldLogValLen); //update the change to the children;
}
bool     ProjectTree::Save()//save all projects in the tree;
{
    return Save(m_root);
}
bool     ProjectTree::Save(wxString nodeLogVal)//save all projects in the tree;
{
    if(nodeLogVal.IsEmpty())
        return true;
    Node *tmp = Find(nodeLogVal);
    if(tmp != NULL)
        return Save(tmp);
    else
        return false;
}
wxString   ProjectTree::UniqeName(wxString &title, wxString &phyPath, wxString logPath, bool isMerge)
{
    //wxMessageBox(title);
    //wxMessageBox(logPath);
    //wxMessageBox(phyPath);
    wxString projectLogPath = logPath;
    if(m_root == NULL)
        return wxEmptyString;

    if(logPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
        logPath = logPath.BeforeLast(platform::PathSep().GetChar(0));

    wxArrayString allPathInfo = GetAllLvVal();
    int        itemPathCount = allPathInfo.GetCount();
    wxString tmpLogVal, tmpPhyVal, tmpStr;
    for(int i = 0; i < itemPathCount; i++) { //uniqe the logval
        tmpLogVal = allPathInfo[i++];
        if(i >= itemPathCount)
            return wxEmptyString;
        tmpPhyVal = allPathInfo[i];

        if(tmpLogVal.IsSameAs(logPath) && isMerge == false) //same name in the same level, uniqe the name and update the log/phy path
        {
            StringUtil::IncSufNumber(title);//update title
            logPath = logPath.BeforeLast(platform::PathSep().GetChar(0));
            logPath = logPath + platform::PathSep() + title; // update logical path

            if(!phyPath.IsEmpty() && phyPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)) {
                tmpStr = phyPath.BeforeFirst(wxCHAR_FN_SEP);
                //wxMessageBox(tmpStr);
                if(tmpStr.IsSameAs(phyPath)) {
                    tmpStr = phyPath.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0));
                } else {
                    tmpStr = tmpStr.BeforeLast(platform::PathSep().GetChar(0));
                }
                phyPath = tmpStr + platform::PathSep() + title + platform::PathSep() + title + wxPRJ_FILE_SUFFIX; //update physical path
            }
            //wxMessageBox(phyPath);
            i = -1;
        }

    }
    if(!phyPath.IsEmpty()) { //&&wxFileExists(phyPath)){//unique the phyval:expand the physical path with logical path info
        tmpStr = logPath.BeforeLast(platform::PathSep().GetChar(0));
        //wxMessageBox(tmpStr);
        wxString tmp = tmpStr.AfterLast(platform::PathSep().GetChar(0));
        phyPath = phyPath.BeforeLast(platform::PathSep().GetChar(0));
        while(!tmp.IsEmpty()) {
            tmpStr = tmpStr.BeforeLast(platform::PathSep().GetChar(0));
            if(tmpStr.IsEmpty())
                break;
            phyPath = phyPath + wxT("@") + tmp;
            tmp = tmpStr.AfterLast(platform::PathSep().GetChar(0));
        }
        phyPath = phyPath + platform::PathSep() + title + wxPRJ_FILE_SUFFIX;
        //wxMessageBox(phyPath);
        if(wxFileExists(phyPath)) { //if the extension can not uniqe the pysical path, ask user for advice.
            wxMessageDialog mdlg(NULL, wxT("A project with the name ") + title + wxT(" eixists, do you want to overwrite it?"), wxT("Warning"), wxYES_NO);
            if(mdlg.ShowModal() != wxID_YES)
                return wxEmptyString;
        }
    }
    if(projectLogPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
        logPath = logPath + platform::PathSep() + title + wxPRJ_FILE_SUFFIX;
    //wxMessageBox(logPath);
    //wxMessageBox(phyPath);
    return logPath;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                             projectgroup;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ProjectGroup::ProjectGroup(wxString &fullPathName, wxTreeCtrl *tree)
{
    m_dir = FpnToPath(fullPathName);
    m_name = FpnToName(fullPathName);
    m_pProjectTree = new ProjectTree(tree, this);
    m_pProjectTree->InitRoot(platform::PathSep() + m_name, NULL);
    m_isDirty = 1;
    m_pProjects = new ProjectsArray;
    m_pProjects->Clear();
}
ProjectGroup::~ProjectGroup()
{
    if(m_pProjectTree != NULL)
        delete m_pProjectTree;
    m_pProjectTree = NULL;
    int        count = m_pProjects->GetCount();
    SimuProject *pProject = NULL;
    for(int i = 0; i < count; i++) { //release the resource.
        pProject = m_pProjects->Item(i);
        if(pProject != NULL)
            delete pProject;
    }
    m_pProjects->Clear();
    delete m_pProjects;
    m_pProjects = NULL;
}
//******************************************************************************
//                                       private functions                                           *
//******************************************************************************
wxString  ProjectGroup::GetPGFileName()//get the full-path-name of the .spg file.
{
    return (m_dir + platform::PathSep() + m_name + wxPG_FILE_SUFFIX) ;
}
wxString ProjectGroup::GetXmlFileRoot()
{
    return (wxT("SimuProject-Group"));
}
wxString  ProjectGroup::FpnToPath(wxString &fullPathName)
{
    return fullPathName.BeforeLast(platform::PathSep().GetChar(0));
}
wxString  ProjectGroup::FpnToName(wxString &fullPathName)
{
    return fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
}
bool                ProjectGroup::CheckProjects(wxArrayString &projects)
{   //when load a goup or refresh the tree,to check if all the projects belong to the project group  exist,if not, upgrade it.
    bool    isDirty = false;
    if(projects.IsEmpty())
        return true;

    int count = projects.GetCount();
    wxString    phypath = wxEmptyString;
    for(int i = 0; i < count; i = i + 2) {
        phypath = projects[i + 1];
        phypath = ToAbsPath(phypath);
        if(!wxFileExists(phypath) && !wxDirExists(phypath))
        {
            isDirty = true;
            projects.RemoveAt(i);
            projects.RemoveAt(i);
            i = i - 2;
            count = count - 2;
        }
    }
    if(isDirty == true) //update the .spg file;
    {
        WritePGFile(GetPGFileName(), projects);
        return false;
    }
    return true;
}
bool                ProjectGroup::CheckPgFile(wxString &fullPathName)
{
    if(!wxFileExists(fullPathName)) {
        wxMessageBox(wxT("The file do not exist, please use an avaliable file."), wxT("ERROR"));
        return false;
    }

    wxString    val = GetXmlFileRoot();
    XmlFile xmlproject = XmlFile(val, fullPathName);
    wxArrayString        astrProjects = xmlproject.GetArrayString("projects");
    int     count = astrProjects.GetCount();
    if(count % 2 != 0) { //file has been destroyed
        wxMessageBox(wxT(" There is an error in the file's content, please use an avaliable file."), wxT("ERROR"));
        return false;
    }
    wxString phyval;
    for(int i = 0; i < count; i++) {
        val = astrProjects[i];
        phyval = astrProjects[i + 1];
        if(val.Left(1).IsSameAs(wxT("."))) { //file has been destroyed
            wxMessageBox(wxT(" There is an error in the file's content, please use an avaliable file."), wxT("ERROR"));
            return false;
        }
        val = val.AfterLast(platform::PathSep().GetChar(0));
        if(val.Len() > PRJ_FILE_SUFFIX_LEN) {
            val = val.Right(PRJ_FILE_SUFFIX_LEN);
            if(val.IsSameAs(wxPRJ_FILE_SUFFIX)) { //file has been destroyed
                wxMessageBox(wxT(" There is an error in the file's content, please use an avaliable file."), wxT("ERROR"));
                return false;
            }
        }
        phyval = phyval.AfterLast(platform::PathSep().GetChar(0));
        if(phyval.Len() > PRJ_FILE_SUFFIX_LEN) {
            phyval = phyval.Right(PRJ_FILE_SUFFIX_LEN);
            if(!phyval.IsSameAs(wxPRJ_FILE_SUFFIX)) { //file has been destroyed
                wxMessageBox(wxT(" There is an error in the file's content, please use an avaliable file."), wxT("ERROR"));
                return false;
            }
        }
        i++;
    }
    return true;
}
bool  ProjectGroup::FileToTree(wxString fullPathName)
{   //load the project group stucture to the tree
    if(!CheckPgFile(fullPathName))
        return false;
    wxString    val = GetXmlFileRoot();
    XmlFile xmlproject = XmlFile(val, fullPathName);
    wxArrayString        astrProjects = xmlproject.GetArrayString(PG_XML_ELEM_PROJECTS);

    CheckProjects(astrProjects);//to check if all the projects recorded exist.

    SimuProject *pProject = NULL;
    wxString    phyVal = wxEmptyString;
    for(unsigned int i = 0; i < astrProjects.GetCount(); i++) {
        val = astrProjects[i];
        phyVal = astrProjects[i + 1];
        if(!phyVal.Right(PRJ_FILE_SUFFIX_LEN).Cmp(wxPRJ_FILE_SUFFIX))// a new project file
        {
            wxString tmp = FpnToName(phyVal);
            val = val + platform::PathSep() + tmp + wxPRJ_FILE_SUFFIX;
            fullPathName = ToAbsPath(phyVal);

            pProject = new SimuProject(SimuProject::GetProjectType(EnvUtil::GetProjectType()), tmp, fullPathName);
            if(pProject == NULL)
                return false;
            m_pProjects->Add(pProject);//record the opened projects
            pProject->SetDirty(false);
            pProject->SetChild(true);
            //           Manager::Get()->GetProjectManager()->SetActiveProject(pProject);
        }
        phyVal = ToAbsPath(phyVal); //load absolute path to node;
        m_pProjectTree->Insert(val, phyVal);
        pProject = NULL;
        i++;
    }
    SetDirty(false);
    return        true;
}
bool                ProjectGroup::TreeToFile(wxString Desc)
{   //save the project group file: .spg
    wxArrayString projectsName;
    projectsName = m_pProjectTree->GetAllLvVal();

    for(unsigned int i = 1; i < projectsName.GetCount(); i = i + 2) { //store relative path to file
        //wxMessageBox(projectsName[i]);
        projectsName[i] = ToPhyPath(projectsName[i]);
        //wxMessageBox(projectsName[i]);
    }
    //wxMessageBox(GetPGFileName());
    WritePGFile(GetPGFileName(), projectsName);

    m_pProjectTree->Save();//save all projects in the tree;
    return true;
}
wxString   ProjectGroup::ToAbsPath(wxString &phyPath)
{
    wxString fullPathName;
    if(phyPath.GetChar(0) == wxT('.'))
        fullPathName = m_dir + phyPath.AfterFirst(wxT('.'));
    else
        fullPathName = phyPath;
    return fullPathName;
}
wxString   ProjectGroup::ToPhyPath(wxString &fullPathName)
{
    wxString phyPath;
    wxString pgdir = m_dir + platform::PathSep();
    int     len = pgdir.Len();
    if(!fullPathName.Left(len).Cmp(pgdir))//equal
        phyPath = wxT(".") + platform::PathSep() + fullPathName.Right(fullPathName.Len() - len);
    else
        phyPath = fullPathName;
    return phyPath;
}
bool          ProjectGroup::WritePGFile(wxString fullPathName, wxArrayString &astrprojects, wxString desc)
{
    if(fullPathName.IsEmpty())
        return false;
    if(!wxDirExists(fullPathName.BeforeLast(platform::PathSep().GetChar(0))))
        FileUtil::CreateDirectory(fullPathName.BeforeLast(platform::PathSep().GetChar(0)));

    wxString    xmlRoot = GetXmlFileRoot();
    wxString    createTime = wxEmptyString, lastModify = wxEmptyString;
    wxDateTime now = wxDateTime::Now();
    lastModify = now.FormatISODate(); //+now.FormatISOTime();
    if(!wxFileExists(fullPathName))
        createTime = lastModify;
    XmlFile xmlProject = XmlFile(xmlRoot,  fullPathName);
    if(createTime.IsEmpty())
        createTime = xmlProject.GetString(PG_XML_ELEM_CREATEDATE);
    if(desc.IsEmpty())
        desc = wxT("A common Project group");
    xmlProject.BuildElement(PG_XML_ELEM_DESCRIPTION, desc);
    xmlProject.BuildElement(PG_XML_ELEM_CREATEDATE, createTime);
    xmlProject.BuildElement(PG_XML_ELEM_LASTMODIFY, lastModify);
    xmlProject.BuildElement(PG_XML_ELEM_PROJECTS, astrprojects);
    xmlProject.Save();//save the .spg file;
    return true;
}
//******************************************************************************
//                                      public functions
//******************************************************************************
bool            ProjectGroup::AddLevel(wxString &nLogVal)//fullPathName: e.g : /mol/tiny/tiny.sip
{
    if(nLogVal.IsEmpty())
        return false;
    if(m_pProjectTree == NULL)
        return false;
    //wxMessageBox(nLogVal)        ;
    wxArrayString allPathInfo = m_pProjectTree->GetAllLvVal();
    int        itemPathCount = allPathInfo.GetCount();
    wxString tmpLogVal, tmpStr;
    for(int i = 0; i < itemPathCount; i++) { //uniqe the logval
        tmpLogVal = allPathInfo[i++];
        if(i >= itemPathCount)
            return wxEmptyString;
        tmpLogVal = tmpLogVal.BeforeLast(platform::PathSep().GetChar(0));
        if(tmpLogVal.IsSameAs(nLogVal))//same name in the same level, uniqe the  logpath
        {
            tmpStr = nLogVal.AfterLast(platform::PathSep().GetChar(0));
            StringUtil::IncSufNumber(tmpStr);//update title
            nLogVal = nLogVal.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + tmpStr;
            i = -1;
            //wxMessageBox(nLogVal);
        }

    }
    m_pProjectTree->Insert(nLogVal, nLogVal);
    m_pProjectTree->DispTree();
    return true;
}
bool            ProjectGroup::AddProject(wxString &fullPathName, wxString &logPath, SimuProject *pProject) //fullPathName: e.g : /mol/tiny/tiny.sip
{
    //   wxString phyPath=ToPhyPath(fullPathName);
    //wxMessageBox(logPath);
    //wxMessageBox(fullPathName);
    m_pProjects->Add(pProject);
    if(!m_pProjectTree->Insert(logPath, fullPathName))
        return false;
    m_pProjectTree->DispTree();
    m_pProjectTree->ExpandNode(logPath);
    pProject->SetChild(true);
    //        if(!Save())        return false;
    SetDirty(true);
    return true;
}
bool     ProjectGroup::AddPG(wxTreeItemId itemId, wxString &fullPathName)
{
    if(fullPathName.IsEmpty())
        return false;
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)Manager::Get()->GetProjectManager()->GetTree()->GetItemData( itemId) : NULL;
    if(item == NULL)
        return false;
    ProjectGroup *nPG = new ProjectGroup(fullPathName, Manager::Get()->GetProjectManager()->GetTree());
    nPG->Load(false);
    ProjectsArray *ps = nPG->GetChildArray();
    if(ps == NULL)
    {
        delete nPG;
        return false;
    }
    for(unsigned int i = 0; i < ps->GetCount(); i++)
        m_pProjects->Add(ps->Item(i));
    wxString    npLogVal = item->GetItemVal();
    MergeTree(m_pProjectTree, nPG->GetTree(), npLogVal);
    Display();
    Save();
    // wxRemoveFile(fullPathName);
    return true;
}
void     ProjectGroup::Close(bool isAskForSave, bool default_save)
{
    int count = m_pProjects->GetCount();
    SimuProject *pProject = NULL;
    for(int i = 0; i < count; i++) {
        pProject = m_pProjects->Item(i);
        if(pProject != NULL) {
            if(isAskForSave == true) {
                if(pProject->IsDirty()) {
                    wxMessageDialog mDlg(NULL, wxT("Do you want to save changes in project: ") + pProject->GetTitle() + wxT(" in project group: ") + GetName() + wxT("?"), wxT("Warning"), wxYES_NO);
                    if(mDlg.ShowModal() == wxID_YES) {
                        pProject->CloseView(false, true);
                    } else {
                        pProject->CloseView(false, false);
                    }
                } else {
                    pProject->CloseView(false, default_save);
                }
            } else {
                pProject->CloseView(false, default_save);
            }
            delete pProject;
        }
    }
    m_pProjects->Clear();
    m_pProjectTree->Close();
}
void           ProjectGroup::CloseNode(wxTreeItemId itemid)
{
    if(m_pProjectTree == NULL)
        return;
    m_pProjectTree->CloseNode(itemid);
}
void           ProjectGroup::Display()
{
    m_pProjectTree->DispTree();
}
bool           ProjectGroup::DeleteNode(wxTreeItemId id)
{
    ProjectTreeItemData *item = id.IsOk() ? (ProjectTreeItemData *)Manager::Get()->GetProjectManager()->GetTree()->GetItemData( id) : NULL;
    if(item == NULL) return false;
    wxArrayString projectsPath = m_pProjectTree->GetChdLv(id);
    m_pProjectTree->Remove(id);

    int ptcount = m_pProjects->GetCount();
    int cpcount = projectsPath.GetCount();
    SimuProject *pProject = NULL;
    wxString         prjPath = wxEmptyString;
    wxString  cpPath = wxEmptyString;
    for(int i = 1; i < cpcount; i = i + 2) { //delete projects from the pg and delete files;
        cpPath = projectsPath[i];
        if(!cpPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
            continue;
        for(int j = 0; j < ptcount; j++) {
            pProject = m_pProjects->Item(j);
            prjPath = pProject->GetFileName();
            if(prjPath.IsSameAs(cpPath))
            {
                pProject->Save();
                pProject->CloseView();
                delete pProject;
                m_pProjects->RemoveAt(j);
                FileUtil::RemoveDirectory(cpPath.BeforeLast(platform::PathSep().GetChar(0)));
                ptcount--; j--;
            }
        }
    }
    SetDirty(true);
    return true;
}
wxString     ProjectGroup::GetFullPathName()
{
    return this->GetPGFileName();
}
SimuProject   *ProjectGroup::GetChild(wxString fullPathName)
{
    int count = m_pProjects->GetCount();
    SimuProject *pProject = NULL;
    for(int i = 0; i < count; i++) {
        pProject = m_pProjects->Item(i);
        if(pProject != NULL)
        {
            if(pProject->GetFileName().IsSameAs(fullPathName))
                return pProject;
        }
    }
    return NULL;
}
SimuProject   *ProjectGroup::GetChild(int projectId)
{
    SimuProject *pProject = NULL;
    int count = m_pProjects->GetCount();
    for(int i = 0; i < count; i++) {
        pProject = m_pProjects->Item(i);
        if(pProject->GetProjectId() == projectId)
            return pProject;
    }
    return NULL;
}
void                ProjectGroup::InsertNode(wxTreeItemId parent, wxTreeItemId nodeID)
{   //the inserted node can be a project, a project group or a level contain one or several projects.
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL) return;
    ProjectTreeItemData *item = nodeID.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(nodeID) : NULL;
    if(item == NULL)  return;
    ProjectTreeItemData *itemParent = parent.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(parent) : NULL;
    if(itemParent == NULL)  return;
    wxString    parLogVal = itemParent->GetItemVal();
    if(item->GetType() == ITEM_PROJECT) {
        SimuProject    *pProject = item->GetProject();
        if(pProject == NULL)  return;
        wxString    projectName = pProject->GetTitle();
        wxString    logVal = parLogVal + platform::PathSep() + projectName + platform::PathSep() + projectName + wxPRJ_FILE_SUFFIX;
        wxString    newPrjPath = m_dir + platform::PathSep() + projectName + platform::PathSep() + projectName + wxPRJ_FILE_SUFFIX; //full path name of the new project file.

        logVal = UniqeName(projectName, newPrjPath, logVal);
        if(logVal.IsEmpty())
            return;
        //wxMessageBox(projectName);
        //wxMessageBox(logVal);
        //wxMessageBox(newPrjPath);

        wxString    oldPrjPath = pProject->GetFileName().BeforeLast(platform::PathSep().GetChar(0));

        wxString tmpPath = newPrjPath.BeforeLast(platform::PathSep().GetChar(0));

        FileUtil::CopyDirectory(oldPrjPath, tmpPath); //copy the project to new directory;

        AbstractView *view = Manager::Get()->GetViewManager()->FindView(pProject->GetProjectId());
        if(view != NULL) {
            view->SaveAs(newPrjPath);
        }
        pProject = new SimuProject(SimuProject::GetProjectType(EnvUtil::GetProjectType()), projectName, newPrjPath);
        if(pProject == NULL)  return;
        AddProject(newPrjPath, logVal, pProject);
    } else if(item->GetType() == ITEM_PROJECTDIR || item->GetType() == ITEM_PROJECTGROUP) {
        ProjectGroup   *pPG = item ->GetProjectGroup();
        if(pPG == NULL)   return;
        wxArrayString   chd = pPG->GetTree()->GetChdLv(nodeID);
        wxString        logVal = wxEmptyString, phyVal = wxEmptyString, nTitle;
        wxString        oldLogVal = item->GetItemVal().BeforeLast(platform::PathSep().GetChar(0)),
                        newPhyVal = GetPath(), newPPath = wxEmptyString;
        SimuProject    *pProject = NULL;

        logVal = chd[0];
        if(!oldLogVal.IsEmpty())
            logVal.Replace(oldLogVal, wxT(""));
        wxString tmpTitle = logVal.AfterFirst(platform::PathSep().GetChar(0)).BeforeFirst(platform::PathSep().GetChar(0));
        wxString tmpLogVal = parLogVal + platform::PathSep() + tmpTitle;
        //wxMessageBox(tmpLogVal);

        wxArrayString allPathInfo = m_pProjectTree->GetAllLvVal();
        int        itemPathCount = allPathInfo.GetCount();
        wxString tmpLv, tmpPv;
        for(int i = 0; i < itemPathCount; i++) { //uniqe the logval
            tmpLv = allPathInfo[i++];
            if(i >= itemPathCount)
                return ;
            tmpPv = allPathInfo[i];

            tmpLv = tmpLv.BeforeLast(platform::PathSep().GetChar(0));
            if(tmpLv.IsSameAs(tmpLogVal))//same name in the same level, uniqe the name and update the log/phy path
            {
                StringUtil::IncSufNumber(tmpTitle);//update title
                tmpLogVal = tmpLogVal.BeforeLast(platform::PathSep().GetChar(0));
                tmpLogVal = tmpLogVal + platform::PathSep() + tmpTitle; // update logical path
                i = -1;
            }
        }
        //wxMessageBox(tmpLogVal);
        bool        isRenewSubPGName = false;
        if(!tmpTitle.IsSameAs(logVal.AfterFirst(platform::PathSep().GetChar(0)).BeforeFirst(platform::PathSep().GetChar(0)))) {
            isRenewSubPGName = true;
        }

        for(int i = 0; i < (int)chd.GetCount(); i = i + 2) {
            logVal = chd[i];
            phyVal = chd[i + 1];
            if(!oldLogVal.IsEmpty())
                logVal.Replace(oldLogVal, wxT(""));
            if(isRenewSubPGName == true) {
                logVal = tmpLogVal + platform::PathSep() + logVal.AfterFirst(platform::PathSep().GetChar(0)).AfterFirst(platform::PathSep().GetChar(0));
            } else {
                logVal = parLogVal + logVal;
            }
            //wxMessageBox(logVal);
            nTitle = logVal.AfterLast(platform::PathSep().GetChar(0));
            if(!phyVal.IsEmpty() && phyVal.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
            {   //copy the project file to the dir.
                newPPath = newPhyVal + platform::PathSep() + nTitle + platform::PathSep() + nTitle + wxPRJ_FILE_SUFFIX;
                //wxMessageBox(nTitle);
                //wxMessageBox(logVal);
                //wxMessageBox(newPPath);
                logVal = UniqeName(nTitle, newPPath, logVal, true);
                //wxMessageBox(nTitle);
                //wxMessageBox(logVal);
                //wxMessageBox(newPPath);
                if(logVal.IsEmpty())
                    return;
                FileUtil::CopyDirectory(phyVal.BeforeLast(platform::PathSep().GetChar(0)), newPPath.BeforeLast(platform::PathSep().GetChar(0)));

                pProject = pPG->GetChild(phyVal);
                if(pProject != NULL) {
                    AbstractView *view = Manager::Get()->GetViewManager()->FindView(pProject->GetProjectId());
                    if(view != NULL) {
                        view->SaveAs(newPPath);
                    }
                }
                pProject = new SimuProject(SimuProject::GetProjectType(EnvUtil::GetProjectType()), nTitle, newPPath);
                AddProject(newPPath, logVal, pProject);
            }
        }
    } else { // if(item->GetType()==ITEM_PROJECTGROUP){

    }
    Display();
}
bool                ProjectGroup::IsDirty()
{
    unsigned int count = m_pProjects->GetCount();
    for(unsigned i = 0; i < count; i++) {
        if(m_pProjects->Item(i)->IsDirty() && m_isDirty == 0) {
            m_isDirty = m_isDirty + 1;
            break;
        }
    }
    return (m_isDirty > 0);
}
SimuProject  *ProjectGroup::IsOpenChd(wxString prjFileName)
{   //to check is the prjFileName project is a child of the cur pg
    if(prjFileName.IsEmpty())
        return NULL;
    int count = m_pProjects->GetCount();
    wxString tmp = wxEmptyString;
    SimuProject    *pP = NULL;
    for(int i = 0; i < count; i++) {
        pP = m_pProjects->Item(i);
        if(pP == NULL)
            continue;
        if(pP->GetFileName().IsSameAs(prjFileName))
            return pP;
    }
    return NULL;
}
SimuProject  *ProjectGroup::IsOpenPGChd(wxString pgFileName)
{   //to check if the children of the pgFileName pg is child of the cur pg
    if(pgFileName.IsEmpty())
        return NULL;
    wxString    val = GetXmlFileRoot();
    XmlFile xmlproject = XmlFile(val, pgFileName);
    wxArrayString        astrProjects = xmlproject.GetArrayString(PG_XML_ELEM_PROJECTS);
    int count = astrProjects.GetCount();
    SimuProject *pTmp = NULL;
    for(int i = 1; i < count; i = i + 2) {
        if(astrProjects[i].GetChar(0) == wxT('.')) //change relative path to absolute path.
            astrProjects[i] = pgFileName.BeforeLast(platform::PathSep().GetChar(0)) + astrProjects[i].AfterFirst(wxT('.'));
        pTmp = IsOpenChd(astrProjects[i]);
        if(pTmp != NULL)
            return pTmp;
    }
    return NULL;
}
bool            ProjectGroup::Load(bool disp)//load project
{
    if(m_dir.IsEmpty() || m_name.IsEmpty())
        return false;
    if(m_pProjectTree->IsEmpty())
        m_pProjectTree->InitRoot(platform::PathSep() + m_name, NULL);
    if(! FileToTree(GetFullPathName())) {
        m_pProjectTree->RmRoot();
        return false;
    }
    if(disp == true)
        Display();

    wxString logVal = platform::PathSep() + m_name;
    m_pProjectTree->ExpandNode(logVal);

    return true;
}
bool            ProjectGroup::Load(wxString  fullPathName)
{   //load another project group .fullPathName: e.g: /mol/tiny/tiny.spg
    wxString    name = fullPathName.AfterLast(platform::PathSep().GetChar(0));
    wxString    suftmp = name.Right(PG_FILE_SUFFIX_LEN);
    if(!suftmp.IsSameAs(wxPG_FILE_SUFFIX))
        return false;
    wxString odname = m_name;
    wxString odpath = m_dir;
    m_pProjectTree->RnRoot(platform::PathSep() + name);

    m_dir = fullPathName.BeforeLast(platform::PathSep().GetChar(0));
    m_name = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
    if(m_pProjectTree->IsEmpty())
        m_pProjectTree->InitRoot(platform::PathSep() + m_name, NULL);
    if(!FileToTree(fullPathName))
    {
        m_dir = odpath;
        m_name = odname;
        m_pProjectTree->Close();
        return false;
    }
    return true;
}
/*bool            ProjectGroup::Rename(wxString&  nProjectGroupName)
{
        wxString oldname=m_name;
        m_name=nProjectGroupName;
        if(! m_pProjectTree->RnRoot(platform::PathSep()+m_name))
        {
                m_name=oldname;
                return false;
        }
        if(!Save())
        {
                m_name=oldname;
                return false;
        }

        FileUtil::RemoveDirectory(m_dir+platform::PathSep()+oldname,true);

        return true;
}*/
bool     ProjectGroup::RemoveNode(wxTreeItemId id)
{   //remove node from the project tree;remove node from the wxTreeCtrl;remove projects from m_pProjects if exists;
    ProjectTreeItemData *item = id.IsOk() ? (ProjectTreeItemData *)Manager::Get()->GetProjectManager()->GetTree()->GetItemData( id) : NULL;
    if(item == NULL) return false;
    wxArrayString projectsPath = m_pProjectTree->GetChdLv(id);
    m_pProjectTree->Remove(id);

    int ptcount = m_pProjects->GetCount();
    int cpcount = projectsPath.GetCount();
    SimuProject *pProject = NULL;
    wxString         prjPath = wxEmptyString;
    wxString  cpPath = wxEmptyString;
    for(int i = 1; i < cpcount; i = i + 2) {
        cpPath = projectsPath[i];
        if(!cpPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
            continue;
        for(int j = 0; j < ptcount; j++) {
            pProject = m_pProjects->Item(j);
            prjPath = pProject->GetFileName();
            if(prjPath.IsSameAs(cpPath))
            {
                delete pProject;
                m_pProjects->RemoveAt(j);
                ptcount--; j--;
            }
        }
    }
    SetDirty(true);
    return true;
}
bool            ProjectGroup::RenameNode(wxTreeItemId        nodeID, wxString &nTitle)
{
    if(m_pProjectTree->IsRoot(nodeID))//rename the project group
    {
        wxString    oldDir = m_dir;
        wxString    tmpDir = m_dir.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + nTitle;
        wxString    newPgFile = tmpDir + platform::PathSep() + nTitle + wxPG_FILE_SUFFIX;
        //wxMessageBox(oldDir);
        //wxMessageBox(tmpDir);
        //wxMessageBox(newPgFile);
        if(wxDirExists(newPgFile.BeforeLast(platform::PathSep().GetChar(0)))) {
            return false;
        }
        /*                {hurukun 0418
                    wxMessageDialog mdlg(NULL, wxT("The directory with the same name exists, would you like to overwrite it?"),wxT("Warning"),wxYES_NO);
                    if(mdlg.ShowModal()!=wxID_YES)
                        return false;
                    if(Manager::Get()->GetProjectManager()->IsOpen(newPgFile,true)!=NULL){
                        wxMessageBox(wxT("Please close the project or project group with the same name at first!"),wxT("Warning"));
                        return false;
                    }
                    FileUtil::RemoveDirectory(newPgFile.BeforeLast(platform::PathSep().GetChar(0)));
                }*/
        FileUtil::CopyDirectory(oldDir, tmpDir);

        FileUtil::RemoveDirectory(oldDir);
        m_name = nTitle;
        m_dir = m_dir.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + nTitle;
        //wxMessageBox(m_dir);
    }
    if(!m_pProjectTree->Rename(nodeID, nTitle))
        return false;

    SetDirty(true);
    Save();
    return true;
}
bool            ProjectGroup::Save()
{   //update the file system according to the tree.save the project group file and all its projects' files.
    if(!wxDirExists(m_dir))
        FileUtil::CreateDirectory(m_dir);
    m_isDirty = 0;
    return TreeToFile(GetFullPathName());
}
bool            ProjectGroup::SaveAs(wxString &fullPathName)
{
    //        wxString opath=m_dir;
    //        wxString oname=m_name;
    //
    //    m_dir=FpnToPath(fullPathName);
    //    EnvUtil::SetProjectDir(m_dir.BeforeLast(platform::PathSep().GetChar(0)));
    //    m_name=FpnToName(fullPathName);
    //        m_pProjectTree->RnRoot(platform::PathSep()+m_name);

    if(! SaveAs(m_pProjectTree->GetRootNode(), fullPathName))
    {
        //                EnvUtil::SetProjectDir(opath);
        //                m_pProjectTree->RnRoot(platform::PathSep()+oname);
        //                m_dir=opath;
        //                m_name=oname;
        return false;
    }
    return true;
}
bool     ProjectGroup::SaveAs(wxTreeItemId id, wxString fullPGPath) //copyfile
{
    if(fullPGPath.IsEmpty())
        return false;
    ProjectTreeItemData *item = id.IsOk() ? (ProjectTreeItemData *)Manager::Get()->GetProjectManager()->GetTree()->GetItemData( id) : NULL;
    if(item == NULL) return false;
    wxArrayString lvNode = m_pProjectTree->GetChdLv(id);
    wxString   nPGName = fullPGPath.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));

    if(!wxDirExists(fullPGPath.BeforeLast(platform::PathSep().GetChar(0))))
        FileUtil::CreateDirectory(fullPGPath.BeforeLast(platform::PathSep().GetChar(0)));
    int lvNodeNum = lvNode.GetCount();
    int oldLogValLen = item->GetItemVal().Len(), newLogValLen = 0;
    for(int i = 0; i < lvNodeNum; i = i + 2) {
        newLogValLen = lvNode[i].Len() - oldLogValLen;
        if(newLogValLen <= 0)
            return false;
        lvNode[i] = lvNode[i].Right(newLogValLen);
        lvNode[i] = platform::PathSep() + nPGName + lvNode[i]; //change for pg file
        if(lvNode[i + 1].Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)) //save each projects as
        {
            SimuProject *pProject = GetChild(lvNode[i + 1]);
            wxString tmp = platform::PathSep() + lvNode[i + 1].BeforeLast(platform::PathSep().GetChar(0)).AfterLast(platform::PathSep().GetChar(0)) + platform::PathSep() + lvNode[i + 1].AfterLast(platform::PathSep().GetChar(0)),
                     path = fullPGPath.BeforeLast(platform::PathSep().GetChar(0)) + tmp;
            pProject->SaveAs(path, false);

            lvNode[i + 1] = wxT(".") + tmp;
        } else {
            lvNode[i + 1] = lvNode[i + 1].Right(newLogValLen); //change for pg file
            lvNode[i + 1] = wxT(".") + lvNode[i + 1];
        }
    }
    WritePGFile(fullPGPath, lvNode); //save pg file
    return true;
}
void     ProjectGroup::SetDirty(bool isDirty)
{
    if(isDirty == true)
        m_isDirty++;
    else
        m_isDirty--;

    if(m_isDirty < 0)
        m_isDirty = 0;
}
wxString   ProjectGroup::UniqeName(wxString &title, wxString &phyPath, wxString logPath, bool isMerge)
{
    //        wxString projectLogPath=logPath;
    if(m_pProjectTree == NULL)
        return wxEmptyString;
    return m_pProjectTree->UniqeName(title, phyPath, logPath, isMerge);
    /*        if(logPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
                    logPath=logPath.BeforeLast(platform::PathSep().GetChar(0));

            wxArrayString allPathInfo=m_pProjectTree->GetAllLvVal();
            int        itemPathCount=allPathInfo.GetCount();
            wxString tmpLogVal,tmpPhyVal,tmpStr;
            for(int i=0;i<itemPathCount;i++){//uniqe the logval
                tmpLogVal=allPathInfo[i++];
                if(i>=itemPathCount)
                    return wxEmptyString;
                tmpPhyVal=allPathInfo[i];

                if(tmpLogVal.IsSameAs(logPath)&&isMerge==false)//same name in the same level, uniqe the name and update the log/phy path
                {
                    StringUtil::IncSufNumber(title);//update title
                    logPath=logPath.BeforeLast(platform::PathSep().GetChar(0));
                    logPath=logPath+platform::PathSep()+title;// update logical path

                    if(!phyPath.IsEmpty()&&phyPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)){
                           tmpStr=phyPath.BeforeFirst(wxCHAR_FN_SEP);
    //wxMessageBox(tmpStr);
                        if(tmpStr.IsSameAs(phyPath)){
                                tmpStr=phyPath.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0));
                        }else{
                                tmpStr=tmpStr.BeforeLast(platform::PathSep().GetChar(0));
                        }
                            phyPath=tmpStr+platform::PathSep()+title+platform::PathSep()+title+wxPRJ_FILE_SUFFIX;//update physical path
                    }
    //wxMessageBox(phyPath);
                    i=-1;
                }

            }
            if(!phyPath.IsEmpty()&&wxFileExists(phyPath)){//unique the phyval:expand the physical path with logical path info
                tmpStr=logPath.BeforeLast(platform::PathSep().GetChar(0));
    //wxMessageBox(tmpStr);
                wxString tmp=tmpStr.AfterLast(platform::PathSep().GetChar(0));
                phyPath=phyPath.BeforeLast(platform::PathSep().GetChar(0));
                while(!tmp.IsEmpty()){
                     tmpStr=tmpStr.BeforeLast(platform::PathSep().GetChar(0));
                     if(tmpStr.IsEmpty())
                             break;
                     phyPath=phyPath+wxT("@")+tmp;
                     tmp=tmpStr.AfterLast(platform::PathSep().GetChar(0));
                }
                phyPath=phyPath+platform::PathSep()+title+wxPRJ_FILE_SUFFIX;
    //wxMessageBox(phyPath);
                if(wxFileExists(phyPath)){//if the extension can not uniqe the pysical path, ask user for advice.
                     wxMessageDialog mdlg(NULL,wxT("A project with the name ")+ title+ wxT("eixists, do you want to overwrite it?"),wxT("Warning"),wxYES_NO);
                     if(mdlg.ShowModal()!=wxID_YES)
                            return wxEmptyString;
                }
            }
            if(projectLogPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
                    logPath=logPath+platform::PathSep()+title+wxPRJ_FILE_SUFFIX;
    //wxMessageBox(logPath);
    //wxMessageBox(phyPath);
            return logPath;*/
}
