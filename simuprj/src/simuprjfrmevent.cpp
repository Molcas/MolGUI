/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
*               2021, Ignacio Fdez. Galván                             *
***********************************************************************/

#include <wx/image.h>
#include <wx/imagpng.h>
#include <wx/wxhtml.h>
#include <wx/statline.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/textdlg.h>

#include "simuprjfrm.h"
#include "auisharedwidgets.h"
#include "appglobals.h"
#include "configmanager.h"
#include "envutil.h"
#include "fileutil.h"
#include "tools.h"
#include "stringutil.h"

#include "manager.h"
#include "projectmanager.h"
#include "simuprjdef.h"
#include "simuproject.h"
#include "starthereview.h"
#include "viewmanager.h"
#include "createprjpnl.h"
#include "molview.h"

#include "molvieweventhandler.h"
#include "uiprops.h"
#include "ctrlinst.h"
#include "auisharedwidgets.h"
#include "qchemlogreading.h"
#include "jobmonitorpnl.h"
#include "envsettings.h"
#include "simuprjinfoview.h"
#include "ctrlids.h"
#include "dlgmachinelist.h"
#include "dlgmasterlist.h"

////Event Table Start
BEGIN_EVENT_TABLE(SimuPrjFrm, wxFrame)
    ////Manual Code Start
    EVT_MENU(MENU_ID_FILE_NEW, SimuPrjFrm::OnFileNew)
    EVT_MENU(MENU_ID_FILE_ADDPROJECT, SimuPrjFrm::OnFileAdd)
    EVT_MENU(MENU_ID_FILE_ADDLEVEL, SimuPrjFrm::OnFileAdd)
    EVT_MENU(MENU_ID_FILE_EXISTITEM, SimuPrjFrm::OnFileAdd)
    EVT_UPDATE_UI(MENU_ID_FILE_ADD, SimuPrjFrm::OnUpdateUIFileAdd)
    EVT_TOOL(TOOL_ID_PRJ_NEW, SimuPrjFrm::OnFileNew)
    EVT_MENU(MENU_ID_FILE_OPEN, SimuPrjFrm::OnFileOpen)
    EVT_TOOL(TOOL_ID_PRJ_OPEN, SimuPrjFrm::OnFileOpen)
    EVT_MENU_RANGE(MENU_ID_PROJECT0, MENU_ID_PROJECT9, SimuPrjFrm::OnFileOpenRecent)
    EVT_MENU_RANGE(MENU_ID_PROJECTGROUP0, MENU_ID_PROJECTGROUP9, SimuPrjFrm::OnFileOpenRecentPG)
    EVT_MENU(MENU_ID_FILE_RECENTPROJECTS_CLEARHISTORY, SimuPrjFrm::OnFileClearRecentProjects)
    EVT_MENU(MENU_ID_FILE_RECENTPROJECTGROUPS_CLEARHISTORY, SimuPrjFrm::OnFileClearRecentPrjGrps)

    EVT_MENU(MENU_ID_FILE_COPY, ProjectManager::OnPopCopy)
    EVT_UPDATE_UI(MENU_ID_FILE_COPY, SimuPrjFrm::OnUpdateUIFileCopy)
    EVT_MENU(MENU_ID_FILE_PASTE, ProjectManager::OnPopPaste)
    EVT_UPDATE_UI(MENU_ID_FILE_PASTE, SimuPrjFrm::OnUpdateUIFilePaste)

    EVT_MENU(MENU_ID_FILE_SAVE, SimuPrjFrm::OnFileSave)
    EVT_UPDATE_UI(MENU_ID_FILE_SAVE, SimuPrjFrm::OnUpdateUIFileSave)
    EVT_TOOL(TOOL_ID_PRJ_SAVE, SimuPrjFrm::OnFileSave)
    EVT_UPDATE_UI(TOOL_ID_PRJ_SAVE, SimuPrjFrm::OnUpdateUIFileSave)
    EVT_MENU(MENU_ID_FILE_SAVEALL, SimuPrjFrm::OnFileSaveAll)
    EVT_TOOL(TOOL_ID_PRJ_SAVEALL, SimuPrjFrm::OnFileSaveAll)
    EVT_MENU(MENU_ID_FILE_SAVEAS, SimuPrjFrm::OnFileSaveAs)

    //          EVT_MENU(MENU_ID_FILE_REMOVE, SimuPrjFrm::OnFileRemove)
    //           EVT_MENU(MENU_ID_FILE_REMOVEALL, SimuPrjFrm::OnFileRemoveAll)
    //           EVT_UPDATE_UI(MENU_ID_FILE_REMOVE, SimuPrjFrm::OnUpdateUIFileRemove)
    EVT_MENU(MENU_ID_FILE_DELETE, SimuPrjFrm::OnFileDelete)
    EVT_UPDATE_UI(MENU_ID_FILE_DELETE, SimuPrjFrm::OnUpdateUIFileDelete)

    EVT_MENU(MENU_ID_FILE_CLOSE, SimuPrjFrm::OnFileClose)
    EVT_UPDATE_UI(MENU_ID_FILE_CLOSE, SimuPrjFrm::OnUpdateUIFileClose)
    EVT_TOOL(TOOL_ID_PRJ_CLOSE, SimuPrjFrm::OnFileClose)
    EVT_UPDATE_UI(TOOL_ID_PRJ_CLOSE, SimuPrjFrm::OnUpdateUIFileClose)
    EVT_MENU(MENU_ID_FILE_CLOSEALL, SimuPrjFrm::OnFileCloseAll)
    EVT_MENU(MENU_ID_FILE_CLOSEALLVIEW, SimuPrjFrm::OnFileCloseAllView)
    EVT_UPDATE_UI(MENU_ID_FILE_CLOSEALLVIEW, SimuPrjFrm::OnUpdateUIFileCloseAllView)

    EVT_MENU(MENU_ID_FILE_PROJECT_INFO_LIST, SimuPrjFrm::OnToolProjectInfoList)
    EVT_TOOL(TOOL_ID_W_INFO_LIST, SimuPrjFrm::OnToolProjectInfoList)

    EVT_MENU(MENU_ID_FILE_IMPORTPROJECT, SimuPrjFrm::OnFileImport)
    EVT_MENU(MENU_ID_EDIT_ENVSETTING, SimuPrjFrm::OnEnvSetting)
    EVT_MENU(MENU_ID_EDIT_COLRSETTING, SimuPrjFrm::OnColrSetting)
    EVT_MENU(MENU_ID_SETTING_HOSTS, SimuPrjFrm::OnHostsSetting)
    EVT_MENU(MENU_ID_SETTING_MASTER, SimuPrjFrm::OnMasterSetting)
    EVT_MENU(MENU_ID_STARTHERE_LINK, SimuPrjFrm::OnStartHereLink)
    EVT_MENU(MENU_ID_STARTHERE_VAR_SUBSTITUTION, SimuPrjFrm::OnStartHereVarSubstition)

    EVT_MENU(MENU_ID_JOB_NEW, SimuPrjFrm::OnJobNew)
    EVT_UPDATE_UI(MENU_ID_JOB_NEW, SimuPrjFrm::OnUpdateUIJobNew)
    EVT_MENU(MENU_ID_JOB_SETTING, SimuPrjFrm::OnJobSetting)
    EVT_UPDATE_UI(MENU_ID_JOB_SETTING, SimuPrjFrm::OnUpdateUIJobSetting)
    EVT_MENU(MENU_ID_JOB_SUBMIT, SimuPrjFrm::OnJobSubmit)
    EVT_UPDATE_UI(MENU_ID_JOB_SUBMIT, SimuPrjFrm::OnUpdateUIJobSubmit)
    EVT_MENU(MENU_ID_JOB_MONITOR, SimuPrjFrm::OnJobMonitor)
    //                EVT_UPDATE_UI(MENU_ID_JOB_MONITOR, SimuPrjFrm::OnUpdateUIJobMonitor)

    EVT_TOOL(TOOL_ID_JOB_SUBMIT, SimuPrjFrm::OnJobSubmit)
    EVT_UPDATE_UI(TOOL_ID_JOB_SUBMIT, SimuPrjFrm::OnUpdateUIJobSubmit)
    EVT_TOOL(TOOL_ID_JOB_MONITOR, SimuPrjFrm::OnJobMonitor)
    //                EVT_UPDATE_UI(TOOL_ID_JOB_MONITOR, SimuPrjFrm::OnUpdateUIJobMonitor)

    EVT_TOOL(TOOL_ID_SHOW_MOLECULE, SimuPrjFrm::OnShowMolecule)
    EVT_UPDATE_UI(TOOL_ID_SHOW_MOLECULE, SimuPrjFrm::OnUpdateUIShowMolecule)

    EVT_MENU(wxID_HELP, SimuPrjFrm::OnHelp)
    EVT_MENU(wxID_ABOUT, SimuPrjFrm::OnAbout)

    EVT_MENU_RANGE(MENU_ID_MOL_START, MENU_ID_MOL_END, SimuPrjFrm::OnMolMenuCommand)
    EVT_TOOL_RANGE(TOOL_ID_MOL_START, TOOL_ID_MOL_END, SimuPrjFrm::OnMolToolCommand)

    EVT_MENU(wxID_EXIT, SimuPrjFrm::OnExit)
    //    EVT_UPDATE_UI(TOOL_ID_JOB_SUBMIT, SimuPrjFrm::OnUpdateUIJobSubmit)

    EVT_CLOSE(SimuPrjFrm::OnClose)

    EVT_MENU(MENU_ID_VIEW_CAPTURE_IMAGE, SimuPrjFrm::OnViewCaptureImage)

    EVT_SIZE(SimuPrjFrm::OnResize)

END_EVENT_TABLE()
////Event Table End
void SimuPrjFrm::OnMolMenuCommand(wxCommandEvent &event)
{
    //wxMessageBox(wxT("SimuPrjFrm::OnMolMenuCommand"));
    MolViewEventHandler::Get()->ProcessMenuEvent(event);
}
void SimuPrjFrm::OnMolToolCommand(wxCommandEvent &event)
{
    //wxMessageBox(wxT("SimuPrjFrm::OnMolToolCommand"));
    MolViewEventHandler::Get()->ProcessToolEvent(event);
}
//************************************************************************************************
//
//************************************************************************************************
//begin : hurukun : 7/12/2009

void SimuPrjFrm::OnClose(wxCloseEvent &event)
{
    if (Manager::Get()->GetProjectManager()->CanUndo())
    {
        wxMessageDialog dialog(this, wxT("Would you like to save changes?"), wxT("MolGUI"), //hurukun 0426
                               wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_INFORMATION);
        switch(dialog.ShowModal())
        {
        case wxID_YES:
            SaveandQuit();
            break;
        case wxID_NO:
            QuitwithoutSave();
            break;
        case wxID_CANCEL:
            break;
        }
    }
    else
    {
        SaveWindowState();
        Manager::Shutdown();
        Destroy();
    }
    EnableMenu(false);
}

void SimuPrjFrm::OnExit(wxCommandEvent &event)
{
    /*        if (Manager::Get()->GetProjectManager()->CanUndo())
            {
                    wxMessageDialog dialog(this, wxT("Would you like to save changes?"), wxT("Simu-Q"), hurukun 0426
                                                               wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_INFORMATION);
                    switch(dialog.ShowModal())
                    {
                    case wxID_YES:
                            SaveandQuit();
                            break;
                    case wxID_NO:
                            QuitwithoutSave();
                            break;
                    case wxID_CANCEL:
                            break;
                    }
        }else{
    */
    SaveWindowState();
    Manager::Shutdown();
    Destroy();
    //        }

    EnableMenu(false);
}
void SimuPrjFrm::SaveandQuit(void)
{
    Manager::Get()->GetProjectManager()->SaveAll();

    SaveWindowState();
    Manager::Shutdown();
    Destroy();
}
void SimuPrjFrm::QuitwithoutSave(void)
{
    SaveWindowState();
    Manager::Shutdown();
    Destroy();
}
//**********************************************************************************************************
void SimuPrjFrm::OnFileAdd(wxCommandEvent &event)
{
    int evid = event.GetId();
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    wxTreeItemId    itemId = pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData( itemId) : NULL;
    if(item == NULL)
        return;
    ProjectGroup *pg = item->GetProjectGroup();
    SimuProject *project = NULL;
    if(pg == NULL)
        return;
    if(evid == MENU_ID_FILE_ADDPROJECT)
    {
        GenericDlg dlg(Manager::Get()->GetAppWindow(), wxID_ANY, wxT("New Project"));
        CreatePrjPnl pnl(&dlg, 0);
        dlg.SetGenericPanel(&pnl);

        if(dlg.ShowModal() != BTN_OK)
            return;
        wxString    title = pnl.GetPrjName().AfterLast(platform::PathSep().GetChar(0));
        if(title.IsEmpty())
            title = pnl.GetPrjName();
        wxString fullPath = pnl.GetFullPath();
        wxString logPath = item->GetItemVal() + platform::PathSep() + pnl.GetPrjName() + platform::PathSep() + title + wxPRJ_FILE_SUFFIX;

        logPath = pg->UniqeName(title, fullPath, logPath);
        //        title=fullPath.AfterLast(platform::PathSep()).BeforeLast(wxT('.'));
        if(logPath.IsEmpty())
            return;
        project = new SimuProject(pnl.GetPrjType(), title, fullPath);
        if(project == NULL)
            return;
        EnvUtil::SetProjectType(SimuProject::GetProjectType(pnl.GetPrjType()));
        pg->AddProject(fullPath, logPath, project);
        project->Save();
        pg->Save();
        pg->SetDirty(false);
        Manager::Get()->GetProjectManager()->SetActiveProject(project);
    } else if(evid == MENU_ID_FILE_EXISTITEM)
    {
        wxString  filter = SimuProject::GetFileFilter() + wxT("|") + ProjectGroup::GetFileFilter();
        wxString defDir = EnvUtil::GetProjectDir();
        wxFileDialog fdlg(Manager::Get()->GetAppWindow(), wxT("Open"), defDir, wxEmptyString, filter, wxFD_OPEN);
        if(fdlg.ShowModal() != wxID_OK)
            return;

        wxString fullPath = fdlg.GetPath();
        if(Manager::Get()->GetProjectManager()->IsOpen(fullPath))
        {
            wxMessageBox(wxT("The Item has been opened already, please close it at first."), wxT("Warning"));
            return;
        }
        if(fullPath.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)) {
            wxString title = fullPath.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
            wxString logPath = item->GetItemVal() + platform::PathSep() + title + platform::PathSep() + title + wxPRJ_FILE_SUFFIX;
            project = new SimuProject(SimuProject::GetProjectType(EnvUtil::GetProjectType()), title, fullPath);
            if(project == NULL)
                return;
            pg->AddProject(fullPath, logPath, project);
            pg->SetDirty(true);
            Manager::Get()->GetProjectManager()->SetActiveProject(project);
        } else if(fullPath.Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX)) {
            pg->AddPG(itemId, fullPath);
            pg->SetDirty(true);
        }
    } else if(evid == MENU_ID_FILE_ADDLEVEL)
    {
        wxString level = wxEmptyString;
        wxString info = wxT("Different level can be seperated by \"") + platform::PathSep() + wxT("\".");

        wxTextEntryDialog   teDlg(NULL, info, wxT("New Group"));
        wxString outString = wxEmptyString;
        //       do{
        teDlg.SetValue(outString);
        if(teDlg.ShowModal() != wxID_OK)
            return;
        level = teDlg.GetValue();
        level = StringUtil::CheckString(level, true);
        //           if(!outString.IsSameAs(level))
        //               wxMessageBox(wxT("Only the following charaters are legal: '0-9', 'a-z', 'A-Z', '_'."));
        //       }while(!outString.IsSameAs(level));

        level = item->GetItemVal() + platform::PathSep() + level;
        //    level=FileUtil::NormalizeFileName(level);// hurukun 0418

        pg->AddLevel(level);
    }
    AbstractView *view = Manager::Get()->GetViewManager()->FindView(PRJINFO_PID);
    if(view != NULL)
        ((SimuPrjInfoView *)view)->UpdateView();
}
void SimuPrjFrm::OnUpdateUIFileAdd(wxUpdateUIEvent &event)
{
    if(Manager::Get()->GetProjectManager()->GetActiveProjectGroup() != NULL) {
        //       Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_ADD, true);
        //       Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_ADDPROJECT, true);
        //      Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_EXISTITEM, true);
    }
    else {
        //    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_ADD, false);
        //    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_ADDPROJECT, false);
        //    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_EXISTITEM, false);
    }
}
void SimuPrjFrm::OnFileClearRecentProjects(wxCommandEvent &event)
{
    wxArrayString pProjectsHistory = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetArrayString("RecentProjects");
    UpdateRecentProjects(wxEmptyString, true);
}
void SimuPrjFrm::OnFileClearRecentPrjGrps(wxCommandEvent &event)
{
    wxArrayString pProjectsHistory = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetArrayString("RecentProjectGroups");
    UpdateRecentPrjGrps(wxEmptyString, true);
}
void SimuPrjFrm::OnFileClose(wxCommandEvent &event)
{
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    wxTreeItemId itemid = pTree->GetSelection();
    if(itemid == Manager::Get()->GetProjectManager()->GetTreeRoot()) { //close all in worspace
        OnFileCloseAll(event);
        return;
    }
    ProjectTreeItemData *item = itemid.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemid) : NULL;
    SimuProject *pProject = NULL;
    ProjectGroup *pg = NULL;
    if(item != NULL) {
        pg = item->GetProjectGroup();
        if(pg != NULL) {
            Manager::Get()->GetProjectManager()->CloseProjectGroup(pg);
            return;
        }
        pProject = item->GetProject();
        if(pProject != NULL && pProject->IsChild() == false) {
            Manager::Get()->GetProjectManager()->CloseProject(pProject);
            return;
        } else if(pProject != NULL) {
            itemid = pProject->GetProjectNode();
            item = itemid.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemid) : NULL;
            pg = item->GetProjectGroup();
            if(pg != NULL) {
                Manager::Get()->GetProjectManager()->CloseProjectGroup(pg);
                return;
            }
        }
    }
}
void SimuPrjFrm::OnFileSaveClose(wxCommandEvent &event)
{
    OnFileSave(event);
    OnFileClose(event);
}
void SimuPrjFrm::OnUpdateUIFileClose(wxUpdateUIEvent &event)
{
    if(!Manager::Get()->GetProjectManager()->IsEmpty()) {
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_CLOSE, true);
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_CLOSEALL, true);
        m_pTbProject->EnableTool(TOOL_ID_PRJ_CLOSE, true);
        return;
    }
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_CLOSE, false);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_CLOSEALL, false);
    m_pTbProject->EnableTool(TOOL_ID_PRJ_CLOSE, false);
}
void SimuPrjFrm::OnFileCloseAll(wxCommandEvent &event)
{
    Manager::Get()->GetProjectManager()->CloseProjectGroup(NULL);
    Manager::Get()->GetProjectManager()->CloseProject(NULL);
    Manager::Get()->GetProjectManager()->GetTree()->DeleteChildren(Manager::Get()->GetProjectManager()->GetTreeRoot());
    EnableMenu(false);
}
void        SimuPrjFrm::OnFileCloseAllView(wxCommandEvent &event)
{
    Manager::Get()->GetViewManager()->CloseAllView(true, false);
}
void SimuPrjFrm::OnUpdateUIFileCloseAllView(wxUpdateUIEvent &event)
{
    ViewManager    *vm = Manager::Get()->GetViewManager();
    if(vm->GetActiveView()->GetProjectId() != STARTHERE_PID)
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_CLOSEALLVIEW, true);
    else
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_CLOSEALLVIEW, false);
}
void SimuPrjFrm::OnFileDelete(wxCommandEvent &event)
{
    Manager::Get()->GetProjectManager()->DoPopDelete();
}
void SimuPrjFrm::OnUpdateUIFileDelete(wxUpdateUIEvent &event)
{
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_DELETE, false);
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL)
        return;
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    wxTreeItemId itemid = pTree->GetSelection();
    ProjectTreeItemData *item = itemid.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemid) : NULL;
    if(item == NULL)
        return;
    ProjectTreeItemType type = item->GetType();
    if(type == ITEM_PROJECTGROUP || type == ITEM_PROJECT || type == ITEM_PROJECTDIR || type == ITEM_JOB_RESULT || type == ITEM_MOPACDIR || type == ITEM_MOPACOUTPUT) {
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_DELETE, true);
        return;
    } else if(type >= ITEM_JOB_RESULT_OUTER_FILE && type != ITEM_JOB_RESULT_OUTER_INPUT_MOLCAS && type <= ITEM_JOB_RESULT_OUTER_SCFORB_MOLCAS) {
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_DELETE, true);
        return;
    }

    //   Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_DELETE, false);
}
void SimuPrjFrm::OnFileNew(wxCommandEvent &event)
{
    DoFileNew();
}
bool SimuPrjFrm::DoFileNew()
{
    GenericDlg dlg(this, wxID_ANY, wxT("New Project"));
    CreatePrjPnl pnl(&dlg);
    dlg.SetGenericPanel(&pnl);
    SimuProject *project = NULL;

    if(dlg.ShowModal() != BTN_OK)
        return false;

    wxString    title = pnl.GetPrjName().AfterLast(platform::PathSep().GetChar(0));
    wxString    dir = pnl.GetDir() + platform::PathSep() + title;

    if(title.IsEmpty())
        title = pnl.GetPrjName();
    wxString    fullProjectPath = pnl.GetFullPath();
    wxString    logpath = platform::PathSep() + pnl.GetPGName() + platform::PathSep() + pnl.GetPrjName() + platform::PathSep() + title + wxPRJ_FILE_SUFFIX;

    wxString    pgpath = pnl.GetFullPath().BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0));
    wxString    name = pnl.GetPGName();
    wxString    fullPathName = pgpath + platform::PathSep() + name + wxPG_FILE_SUFFIX; //default dir has the same name with the project group.
    ProjectGroup *acpg = (ProjectGroup *)Manager::Get()->GetProjectManager()->IsOpen(fullPathName, true);

    //wxMessageBox(title);
    //wxMessageBox(fullProjectPath);
    //wxMessageBox(logpath);
    if(acpg != NULL)
        logpath = acpg->UniqeName(title, fullProjectPath, logpath);
    if(logpath.IsEmpty())
        return false;

    project = new SimuProject(pnl.GetPrjType(), title, fullProjectPath);

    if(project == NULL)
        return false;


    if(name.IsEmpty() && project != NULL) //sigle project, no project group is needed;
    {
        //        if(!wxDirExists(dir))
        //            if(!FileUtil::CreateDirectory(dir))
        //                return;
        wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
        ProjectTreeItemData *itemData = new ProjectTreeItemData(project, wxEmptyString, ITEM_PROJECT);
        wxTreeItemId item = pTree->AppendItem(pTree->GetRootItem(), title, 1, 1, itemData);
        project->SetNodeId(item);
        project->BuildTree(pTree);
        Manager::Get()->GetProjectManager()->AddProject(project);
        Manager::Get()->GetProjectManager()->SetActiveProject(project);
        project->Save();  //hurukun 0503
        project->SetDirty(false);
        pTree->Expand(item);
        UpdateRecentProjects(project->GetFileName());
        return true;
    }

    bool    isNewPrjGrp = false;
    if(acpg == NULL)
    {
        wxTreeCtrl *tree = Manager::Get()->GetProjectManager()->GetTree();
        acpg = new ProjectGroup(fullPathName, tree);
        if(acpg == NULL)
            return false;
        isNewPrjGrp = true;
    }

    acpg->AddProject(fullProjectPath, logpath, project);
    acpg->Save();
    acpg->SetDirty(false);

    if(project != NULL && acpg != NULL) {
        Manager::Get()->GetProjectManager()->SetActiveProjectGroup(acpg);
        if(isNewPrjGrp == true) {
            Manager::Get()->GetProjectManager()->AddPrjGrp(acpg);
            UpdateRecentPrjGrps(acpg->GetFullPathName());
        }
        Manager::Get()->GetProjectManager()->SetActiveProject(project);
    }
    return true;
}
void SimuPrjFrm::OnFileOpen(wxCommandEvent &event)
{
    wxString    filter = ProjectGroup::GetFileFilter() + wxT("|") + SimuProject::GetFileFilter();
    wxString fileName = Tools::GetFilePathFromUser(this, wxT("Open Project"), filter, wxFD_OPEN);
    //wxMessageBox(fileName);
    DoFileOpen(fileName);
}
void SimuPrjFrm::DoFileOpen(wxString &fileName)
{
    if(!StringUtil::IsEmptyString(fileName)) {
        if(!wxFileExists(fileName))
            return;
        // Make a check whether the project exists in current workspace
        if(Manager::Get()->GetProjectManager()->IsOpen(fileName) != NULL)
        {
            wxMessageBox(wxT("The file or something related to it has been opened."), wxT("Warning"));
            return;
        }

        //update the path of the current workspace.
        EnvUtil::SetProjectDir(fileName.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0)));
        wxString    suftmp = fileName.Right(PRJ_FILE_SUFFIX_LEN);
        if (suftmp.IsSameAs(wxPRJ_FILE_SUFFIX)) {
            Manager::Get()->GetProjectManager()->LoadProject(fileName);
            UpdateRecentProjects(fileName);
        } else if(suftmp.IsSameAs(wxPG_FILE_SUFFIX))
        {
            Manager::Get()->GetProjectManager()->LoadPrjGrp(fileName);
            UpdateRecentPrjGrps(fileName);
        }
        //     EnableMenu(true);
    }
}
void SimuPrjFrm::OnFileOpenRecent(wxCommandEvent &event)
{
    wxString label = m_pRecentProjectsMenu->GetLabel(event.GetId());
    label = label.AfterFirst(wxT(' '));
    if(label.IsEmpty())
        return;
    //bug: the wxMenu will auto-replace the '_' with '__'.
    label.Replace(wxT("__"), wxT("_"));
    if(!wxFileExists(label)) {
        wxMessageBox(wxT("Sorry, the file do not exists!"));
        if(label.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
            UpdateRecentProjects(label, false, true);
    }
    DoFileOpen(label);
}
void SimuPrjFrm::OnFileOpenRecentPG(wxCommandEvent &event)
{
    wxString label = m_pRecentProjectGroupsMenu->GetLabel(event.GetId());
    label = label.AfterFirst(wxT(' '));
    if(label.IsEmpty())
        return;
    //bug: the wxMenu will auto-replace the '_' with '__'.
    label.Replace(wxT("__"), wxT("_"));
    if(!wxFileExists(label)) {
        wxMessageBox(wxT("Sorry, the file do not exists!"));
        if(label.Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX))
            UpdateRecentPrjGrps(label, false, true);
    }
    DoFileOpen(label);
}
void        SimuPrjFrm::OnUpdateUIFileCopy(wxUpdateUIEvent &event)
{
    Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_COPY, false);
    if(Manager::Get()->GetProjectManager() == NULL)
        return ;
    wxTreeCtrl        *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    wxTreeItemId        selId = pTree->GetSelection();
    ProjectTreeItemData *item = selId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(selId) : NULL;
    if(item == NULL)
        return;
    ProjectTreeItemType type = item->GetType();

    if(type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR || type == ITEM_PROJECT || type == ITEM_JOB_RESULT || (type >= ITEM_JOB_RESULT_OUTPUT_MOLCAS && type <= ITEM_JOB_RESULT_SCFORB_MOLCAS)) {
        Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_COPY, true);
    }
}
void        SimuPrjFrm::OnUpdateUIFilePaste(wxUpdateUIEvent &event)
{
    Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_PASTE, false);
    if(Manager::Get()->GetProjectManager() == NULL)
        return ;
    wxTreeCtrl        *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    wxTreeItemId        selId = pTree->GetSelection();
    ProjectTreeItemData *item = selId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(selId) : NULL;
    if(item == NULL)
        return;
    ProjectTreeItemType type = item->GetType();

    if(type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR) {
        if(Manager::Get()->GetProjectManager()->IsCopyNodeIdOk()) {
            Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_PASTE, true);
            return;
        }
    }
    if(type == ITEM_PROJECT) {
        if(Manager::Get()->GetProjectManager()->IsCopyJobNodeIdOk()) {
            Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_PASTE, true);
            return;
        }
    }
    if(type == ITEM_JOB_RESULT) {
        if(Manager::Get()->GetProjectManager()->IsCopyJobItemNodeIdOk()) {
            Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_PASTE, true);
            return;
        }
    }
}
void SimuPrjFrm::OnFileSave(wxCommandEvent &event)//save a project group
{
    ProjectGroup *pg = Manager::Get()->GetProjectManager()->GetActiveProjectGroup();
    if(pg != NULL) {
        pg->Save();
        //       m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVE,false);
        ((SimuPrjFrm *)Manager::Get()->GetAppFrame())->EnableTool(TOOL_ID_PRJ_SAVE, false);
        Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_SAVE, false);
    }
    SimuProject *pP = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pP != NULL) {
        pP->Save();
        //       m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVE,false);
        ((SimuPrjFrm *)Manager::Get()->GetAppFrame())->EnableTool(TOOL_ID_PRJ_SAVE, false);
        Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_SAVE, false);
    }
}
void SimuPrjFrm::OnUpdateUIFileSave(wxUpdateUIEvent &event)
{
    if(!Manager::Get()->GetProjectManager()->IsEmpty()) {
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVEAS, true);
        Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVEALL, true);
        m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVEALL, true);
        ProjectGroup *pg = Manager::Get()->GetProjectManager()->GetActiveProjectGroup();
        SimuProject   *pP = Manager::Get()->GetProjectManager()->GetActiveProject();
        AbstractView *activeView = Manager::Get()->GetViewManager()->GetActiveView();
        CtrlInst *pCtrlInst = NULL;
        if(activeView != NULL && activeView->GetType() == VIEW_GL_BUILD)
            pCtrlInst = ((MolView *)activeView)->GetData()->GetActiveCtrlInst();
        if((pg != NULL && pg->IsDirty() == true) || (pP != NULL && pP->IsDirty() == true) || (pCtrlInst != NULL && pCtrlInst->IsDirty())) {
            Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVE, true);
            m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVE, true);
        }
        return;
    }
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVE, false);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVEAS, false);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVEALL, false);
    m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVE, false);
    m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVEALL, false);
}
void SimuPrjFrm::OnFileSaveAs(wxCommandEvent &event)
{
    wxTreeItemId    itemId = Manager::Get()->GetProjectManager()->GetTree()->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)Manager::Get()->GetProjectManager()->GetTree()->GetItemData( itemId) : NULL;
    if(item == NULL)
        return;
    wxString filter = wxEmptyString;
    if(item->GetType() == ITEM_PROJECT)
    {
        filter = SimuProject::GetFileFilter();
    } else if(item->GetType() == ITEM_PROJECTDIR || item->GetType() == ITEM_PROJECTGROUP)
    {
        filter = ProjectGroup::GetFileFilter();
    }
    wxString defDir = EnvUtil::GetProjectDir();
    if(item->GetType() == ITEM_JOB_RESULT)
        defDir = item->GetProject()->GetFileName().BeforeLast(platform::PathSep().GetChar(0));
    wxFileDialog fdlg(Manager::Get()->GetAppWindow(), wxT("Save As"), defDir, wxEmptyString, filter, wxFD_SAVE);
    if(fdlg.ShowModal() != wxID_OK)
        return;

    wxString fullPathName = fdlg.GetPath();
    //wxMessageBox(fullPathName);
    wxString title;
    if(fullPathName.Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX) || fullPathName.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX)) {
        title = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
        fullPathName = fullPathName.BeforeLast(wxT('.'));
    } else {
        title = fullPathName.AfterLast(platform::PathSep().GetChar(0));
    }
    //wxMessageBox(title);
    title = StringUtil::CheckInput(title);

    //wxMessageBox(title);
    if(title.IsEmpty())
        return;

    if(item->GetType() == ITEM_PROJECT)
    {
        SimuProject *pProject = item->GetProject();
        if(pProject == NULL)
            return;
        fullPathName = fullPathName + platform::PathSep() + title + wxPRJ_FILE_SUFFIX;
        pProject->SaveAs(fullPathName, true);
    } else if(item->GetType() == ITEM_PROJECTDIR)
    {
        ProjectGroup *pg = item->GetProjectGroup();
        if(pg == NULL)    return;
        fullPathName = fullPathName + platform::PathSep() + title + wxPG_FILE_SUFFIX;
        pg->SaveAs(itemId, fullPathName);
        Manager::Get()->GetProjectManager()->LoadPrjGrp(fullPathName, true);
    } else if(item->GetType() == ITEM_PROJECTGROUP)
    {
        ProjectGroup *pg = item->GetProjectGroup();
        if(pg == NULL)    return;
        fullPathName = fullPathName + platform::PathSep() + title + wxPG_FILE_SUFFIX;
        pg->SaveAs(fullPathName);
        Manager::Get()->GetProjectManager()->LoadPrjGrp(fullPathName, true);
    } else if(item->GetType() == ITEM_JOB_RESULT) {
        SimuProject *pProject = item->GetProject();
        if(pProject == NULL)
            return;
        wxString        oldJobPath = pProject->GetFileName();
        wxString        newJobPath = oldJobPath.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + title;
        oldJobPath = oldJobPath.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + item->GetTitle();
        FileUtil::CopyDirectory(oldJobPath, newJobPath);
        pProject->BuildResultTree(title);
    }
}
void SimuPrjFrm::OnFileSaveAll(wxCommandEvent &event)
{
    Manager::Get()->GetProjectManager()->SaveAll();
}
void SimuPrjFrm::OnFileRemove(wxCommandEvent &event)
{
}
void SimuPrjFrm::OnUpdateUIFileRemove(wxUpdateUIEvent &event)
{
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    wxTreeItemId itemid = pTree->GetSelection();
    ProjectTreeItemData *item = itemid.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemid) : NULL;
    if(item != NULL) {
        ProjectTreeItemType type = item->GetType();
        if(type == ITEM_PROJECTGROUP) {
            Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_REMOVEALL, true);
            Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_REMOVE, false);
        } else if(type == ITEM_PROJECT || type == ITEM_PROJECTDIR) {
            Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_REMOVE, true);
            Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_REMOVEALL, false);
        }
        return;
    }
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_REMOVE, false);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_REMOVEALL, false);
}
void SimuPrjFrm::OnFileRemoveAll(wxCommandEvent &event)
{
}
void SimuPrjFrm::OnFileImport(wxCommandEvent &event) //From Poroject/import file
{   //wxOK wxYES_DEFAULT
    SimuProject    *pProject;
    wxString             projectName;
    wxMessageDialog dialog(this, wxT("In order to import the other format file, you must create a new project first!"),
                           wxT("File Import Info"), wxYES_NO | wxICON_INFORMATION , wxDefaultPosition);
    //int        resmdlg;

    if(dialog.ShowModal() != wxID_YES)
        return;
    if(!DoFileNew())
        return;
    pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL)
        return;

    pProject->Import();
}/*
void SimuPrjFrm::OnImportStructure(wxCommandEvent& event)
{        //wxOK wxYES_DEFAULT
    SimuProject*    pProject=Manager::Get()->GetProjectManager()->GetActiveProject();
    wxString             projectName;
    wxMessageDialog dialog(this, wxT("In order to import the other format file, you must create a new project first!"),
        wxT("File Import Info"), wxYES_NO | wxICON_INFORMATION , wxDefaultPosition);
    int        resmdlg;
    if(pProject!=NULL){
        projectName=pProject->GetFileName().AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
         wxMessageDialog*pmdlg=NULL;
         if(pProject->IsDirty())
             pmdlg=new wxMessageDialog(NULL,wxT("The file will be imported to the active project: ")+projectName+wxT(" , and the molecule in the project will lost."),wxT("Warning"),wxYES_NO | wxICON_INFORMATION);
         else
            pmdlg=new wxMessageDialog(NULL,wxT("The file will be imported to the active project: ")+projectName+wxT(" ."),wxT("Warning"),wxYES_NO | wxICON_INFORMATION);
         resmdlg=pmdlg->ShowModal();
         if(pmdlg!=NULL)
                delete pmdlg;
    }
    if(pProject==NULL||resmdlg != wxID_YES){
        if(dialog.ShowModal() != wxID_YES)
            return;
         if(!DoFileNew())
            return;
        pProject=Manager::Get()->GetProjectManager()->GetActiveProject();
        if(pProject==NULL)
            return;
        projectName=pProject->GetFileName().AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
        wxMessageBox(wxT("The file will be imported to the active project:")+projectName+wxT("."));
    }

    wxString fileName = wxEmptyString,suffix=wxEmptyString;
    do{
        fileName = Tools::GetFilePathFromUser(Manager::Get()->GetAppWindow(), wxT("Import Molecule File"), wxT("XYZ Files (*.xyz)|*.xyz|Q-Chem Molecule File (*.in)|*.in|All Files (*.*)|*.*"), wxFD_OPEN);
        suffix=fileName.AfterLast(wxT('.'));
        if(fileName.IsEmpty())  return;
        if(!suffix.IsSameAs(wxT("in"))&&!suffix.IsSameAs(wxT("xyz"))){
            wxMessageBox(wxT("Unrecognized file type."),wxT("Warning"));
            continue;
        }else
            break;
    }while(!fileName.IsEmpty());
//wxMessageBox(fileName);
    if(!StringUtil::IsEmptyString(fileName))
    {
        ProjectGroup*   pPG=Manager::Get()->GetProjectManager()->GetActiveProjectGroup();
        if(pPG!=NULL)
            pPG->Save();
        Manager::Get()->GetProjectManager()->GetTree()->SelectItem(pProject->GetProjectNode());
        Manager::Get()->GetProjectManager()->ShowSelectedItemView(fileName);
        pProject->Save();
        Manager::Get()->GetViewManager()->SaveViews(NULL);  //save the import Q-Chem file to the project
        pProject->SetDirty(false);
    }
}*/
void SimuPrjFrm::OnEnvSetting(wxCommandEvent &event) {
    EnvSettings envsetDlg(this);
    if(envsetDlg.ShowModal() == wxID_OK)
    {
    }
}

void SimuPrjFrm::OnMasterSetting(wxCommandEvent &event) {
    DlgMasterList *masterlist = new DlgMasterList(this, wxID_ANY, wxT("Master Setting"));
    bool t = masterlist->GetMasterList();
    if(!t)return;
    masterlist->ShowModal();
}

void SimuPrjFrm::OnHostsSetting(wxCommandEvent &event) {
    DlgMachineList *machinelist = new DlgMachineList(this, wxID_ANY, wxT("Hosts List"));
    bool t = machinelist->GetMachineList();
    if(!t)return;
    machinelist->ShowModal();
}
void SimuPrjFrm::OnColrSetting(wxCommandEvent &event) {

    //        if (Manager::Get()->GetViewManager()->GetViewCount() <= 1) {
    //                wxMessageBox(wxT("There is no right view to set color!"));
    //                return ;
    //        }        else {
    //                if (Manager::Get()->GetViewManager()->GetActiveView()->GetTitle().Cmp(wxT("Start here")) == 0) {
    //                        wxMessageBox(wxT("It is not a right view to set color!"));
    //                        return ;
    //                }
    //        }
    wxColour colour;
    ColrSettings cdlg(this);
    if (wxID_OK == cdlg.ShowModal()) {
        colour = cdlg.GetColour();
        GetUIProps()->openGLProps.mainBgColor.SetColor(colour);
        GetUIProps()->openGLProps.previewBgColor.SetColor(colour);

        GetUIProps()->SaveColour(colour);
        if (MolViewEventHandler::Get()->GetMolView()) {
            AuiSharedWidgets::Get()->GetElemPnl()->RefreshGraphics();
            MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        }
    }

}

void SimuPrjFrm::OnStartHereLink(wxCommandEvent &event) {
    wxString link = event.GetString();
    if(link.IsSameAs(wxT("SIMU_CMD_NEW_PROJECT"))) {
        OnFileNew(event);
    } else if(link.IsSameAs(wxT("SIMU_CMD_OPEN_PROJECT"))) {
        OnFileOpen(event);
    } else if(link.IsSameAs(wxT("SIMU_CMD_PROJECT_INFO_LIST"))) {
        OnToolProjectInfoList(event);
        //        Manager::Get()->GetViewManager()->ShowPrjInfoView(true);
        //     Manager::Get()->GetViewManager()->ShowStartHereView(false);
    } else if(link.StartsWith(wxT("SIMU_CMD_OPEN_HISTORY_PROJECT_GROUP_"))) {
        link = link.Mid(wxString(wxT("SIMU_CMD_OPEN_HISTORY_PROJECT_GROUP_")).Len());

        wxString fileName = link;
        EnvUtil::SetProjectDir(fileName.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0)));
        DoFileOpen(fileName);
        UpdateRecentPrjGrps(fileName);
    } else if(link.StartsWith(wxT("SIMU_CMD_OPEN_HISTORY_PROJECT_"))) {
        link = link.Mid(wxString(wxT("SIMU_CMD_OPEN_HISTORY_PROJECT_")).Len());

        wxString fileName = link;
        EnvUtil::SetProjectDir(fileName.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0)));
        DoFileOpen(fileName);
        UpdateRecentProjects(fileName);
    }
}
void SimuPrjFrm::OnToolProjectInfoList(wxCommandEvent &event)
{
    if(Manager::Get()->GetViewManager()->FindView(PRJINFO_PID) != NULL) {
        Manager::Get()->GetViewManager()->ActivateView(PRJINFO_PID);
        //wxMessageBox(wxT("It has been opened."));
    } else {
        Manager::Get()->GetViewManager()->ShowPrjInfoView(true);
    }
}
void SimuPrjFrm::OnStartHereVarSubstition(wxCommandEvent &event) {
    AbstractView *startHereView = Manager::Get()->GetViewManager()->GetView(global_startHereTitle);
    if (!startHereView)
        return;

    // replace history vars
    wxString buf = event.GetString();
    wxString links;
    int itemCount = 0;

    wxArrayString pProjectGroupsHistory = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetArrayString("RecentProjectGroups");

    links = wxT("<B>Recent project Groups</B><BR>\n");
    if (pProjectGroupsHistory.GetCount()) {
        links << wxT("<UL>");
        itemCount = pProjectGroupsHistory.GetCount() > 5 ? pProjectGroupsHistory.GetCount() - 5 : 0;
        for (int i = pProjectGroupsHistory.GetCount() - 1; i >= itemCount; i--) {
            if(!wxFileExists(pProjectGroupsHistory[i]))
                continue;
            links << wxString::Format(wxT("<LI><A HREF=\"SIMU_CMD_OPEN_HISTORY_PROJECT_GROUP_"));
            links << pProjectGroupsHistory[i];
            links << wxT("\">");
            links << pProjectGroupsHistory[i];
            links << wxT("</A></LI>");
        }
        links << wxT("</UL><BR>");
    }
    else
        links << wxT("&nbsp;&nbsp;&nbsp;&nbsp;No recent project groups<br>\n");

    // update page
    buf.Replace(_T("SIMU_VAR_RECENT_FILES_AND_PROJECTGROUPS"), links);

    wxArrayString pProjectsHistory = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetArrayString("RecentProjects");

    links = wxT("<B>Recent projects</B><BR>\n");
    if (pProjectsHistory.GetCount()) {
        links << wxT("<UL>");
        itemCount = pProjectsHistory.GetCount() > 5 ? pProjectsHistory.GetCount() - 5 : 0;
        for (int i = pProjectsHistory.GetCount() - 1; i >= itemCount; i--) {
            if(!wxFileExists(pProjectsHistory[i]))
                continue;
            links << wxString::Format(wxT("<LI><A HREF=\"SIMU_CMD_OPEN_HISTORY_PROJECT_"));
            links << pProjectsHistory[i];
            links << wxT("\">");
            links << pProjectsHistory[i];
            links << wxT("</A></LI>");
        }
        links << wxT("</UL><BR>");
    }
    else
        links << wxT("&nbsp;&nbsp;&nbsp;&nbsp;No recent projects<br>\n");

    // update page
    buf.Replace(_T("SIMU_VAR_RECENT_FILES_AND_PROJECTS"), links);
    ((StartHereView *)startHereView)->SetPageContent(buf);
}
void SimuPrjFrm::OnShowMolecule(wxCommandEvent &event)
{
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL) {
        return;
    }
    SimuProject *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL) {
        return;
    }
    wxTreeItemId        projectNode = pProject->GetProjectNode(), tmpItemId;
    wxTreeCtrl        *pTree = Manager::Get()->GetProjectManager()->GetTree();
    int                childCount = pTree->GetChildrenCount(projectNode);
    wxTreeItemIdValue        cookie;
    tmpItemId = pTree->GetFirstChild(projectNode, cookie);
    ProjectTreeItemData *item = NULL;
    for(int i = 0; i < childCount; i++) {
        item = tmpItemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(tmpItemId) : NULL;
        if(item == NULL)
            continue;
        if(item->GetType() == ITEM_MOLECULE) {
            Manager::Get()->GetViewManager()->ShowMolView(item, wxEmptyString, true);
            break;
        }
        tmpItemId = pTree->GetNextChild(projectNode, cookie);
    }
}
void SimuPrjFrm::OnUpdateUIShowMolecule(wxUpdateUIEvent &event)
{
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL) {
        m_pTbJob->EnableTool(TOOL_ID_SHOW_MOLECULE, false);
        return;
    }
    SimuProject *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL) {
        m_pTbJob->EnableTool(TOOL_ID_SHOW_MOLECULE, false);
        return;
    } else {
        m_pTbJob->EnableTool(TOOL_ID_SHOW_MOLECULE, true);
    }
}
//...................................................................................................
void        SimuPrjFrm::OnJobNew(wxCommandEvent &event)
{
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL)
        return;
    wxTreeCtrl        *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    SimuProject        *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL)
        return;

    wxTreeItemId        prjNode = pProject->GetProjectNode();
    if(!prjNode.IsOk())
        return;

    int itemCount = pTree->GetChildrenCount(prjNode, false);
    wxTreeItemIdValue        cookie;
    wxTreeItemId        child = pTree->GetFirstChild(prjNode, cookie);
    ProjectTreeItemData *item;
    for(int j = 0; j < itemCount; j++) {
        item = child.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(child) : NULL;
        if(item == NULL)        break;
        if(item->GetType() == ITEM_NEW_JOB) {
            pTree->SelectItem(child);
            break;
        }
        child = pTree->GetNextChild(prjNode, cookie);
    }
    Manager::Get()->GetProjectManager()->ShowSelectedItemView(wxEmptyString);
}
void         SimuPrjFrm::OnUpdateUIJobNew(wxUpdateUIEvent &event)
{
    Tools::EnableMenu(GetMenuBar(), MENU_ID_JOB_NEW, false);
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL)
        return;
    if(Manager::Get()->GetProjectManager()->GetActiveProject() != NULL)
    {
        Tools::EnableMenu(GetMenuBar(), MENU_ID_JOB_NEW, true);
        return;
    }
}
void        SimuPrjFrm::OnJobSetting(wxCommandEvent &event)
{
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL)
        return;
    wxTreeCtrl        *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    SimuProject        *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL)
        return;

    wxTreeItemId        itemId = pTree->GetSelection();
    if(!itemId.IsOk())
        return;

    ProjectTreeItemData *item;
    item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemId) : NULL;
    if(item == NULL)
        return;

    wxTreeItemId        parentNode;
    if(item->GetType() == ITEM_JOB_INFO) { //job setting node
        pTree->SelectItem(itemId);
        Manager::Get()->GetProjectManager()->ShowSelectedItemView(wxEmptyString);
        return;
    } else if(item->GetType() == ITEM_JOB_RESULT) { //job node
        parentNode = itemId;
    } else if((item->GetType() >= ITEM_JOB_RESULT_INPUT_MOLCAS && item->GetType() <= ITEM_JOB_RESULT_XMLDUMP_XML_MOLCAS) || item->GetType() == ITEM_JOB_RESULT_MOLDEN || item->GetType() == ITEM_JOB_RESULT_ORB) {
        parentNode = pTree->GetItemParent(itemId);
    }


    int itemCount = pTree->GetChildrenCount(parentNode, false);
    wxTreeItemIdValue        cookie;
    wxTreeItemId        child = pTree->GetFirstChild(parentNode, cookie);

    for(int j = 0; j < itemCount; j++) {
        item = child.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(child) : NULL;
        if(item == NULL)        break;
        if(item->GetType() == ITEM_JOB_INFO) {
            pTree->SelectItem(child);
            Manager::Get()->GetProjectManager()->ShowSelectedItemView(wxEmptyString);
            break;
        }
        child = pTree->GetNextChild(parentNode, cookie);
    }

}

void         SimuPrjFrm::OnUpdateUIJobSetting(wxUpdateUIEvent &event)
{
    //wxMessageBox(wxT("SimuPrjFrm::OnUpdateUIJobSetting"));
    Tools::EnableMenu(GetMenuBar(), MENU_ID_JOB_SETTING, false);
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL) {
        //                Tools::EnableMenu(GetMenuBar(),MENU_ID_JOB_SETTING,false);
        return;
    }
    SimuProject *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL) {
        //                Tools::EnableMenu(GetMenuBar(),MENU_ID_JOB_SETTING,false);
        return;
    }
    wxTreeCtrl        *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL) {
        //                Tools::EnableMenu(GetMenuBar(),MENU_ID_JOB_SETTING,false);
        return;
    }
    wxTreeItemId        itemId = pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemId) : NULL;
    if(item == NULL) {
        //                Tools::EnableMenu(GetMenuBar(),MENU_ID_JOB_SETTING,false);
        return;
    }
    if(item->GetType() == ITEM_JOB_RESULT || item->GetType() == ITEM_JOB_INFO) {
        Tools::EnableMenu(GetMenuBar(), MENU_ID_JOB_SETTING, true);
        return;
    } else {
        //                Tools::EnableMenu(GetMenuBar(),MENU_ID_JOB_SETTING,false);
        return;
    }
}
void SimuPrjFrm::OnJobSubmit(wxCommandEvent &event) {
    //wxMessageBox(wxT("SimuPrjFrm::OnJobSubmit"));
    if(m_combBCmdLine != NULL) {
        wxString tmpStr = m_combBCmdLine->GetValue();
        if(!tmpStr.IsEmpty())
            m_combBCmdLine->Insert(tmpStr, 0);
    }
    Manager::Get()->GetProjectManager()->SubmitJob();
}
void SimuPrjFrm::OnUpdateUIJobSubmit(wxUpdateUIEvent &event)
{
    //wxMessageBox(wxT("SimuPrjFrm::OnUpdateUIJobSubmit"));
    Tools::EnableMenu(GetMenuBar(), MENU_ID_JOB_SUBMIT, false);
    //        m_pTbJob->EnableTool(TOOL_ID_JOB_SUBMIT,false);
    if(Manager::Get() == NULL || Manager::Get()->GetProjectManager() == NULL) {
        return;
    }
    SimuProject *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL) {
        m_pTbJob->EnableTool(TOOL_ID_JOB_SUBMIT, false);
        return;
    }
    wxTreeCtrl        *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL) {
        m_pTbJob->EnableTool(TOOL_ID_JOB_SUBMIT, false);
        return;
    }
    wxTreeItemId        itemId = pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemId) : NULL;
    if(item == NULL) {
        m_pTbJob->EnableTool(TOOL_ID_JOB_SUBMIT, false);
        return;
    }
    wxArrayString        jobs = pProject->GetJobs();
    if(jobs.GetCount() > 0) {
        ProjectTreeItemType type = item->GetType();
        if(type == ITEM_JOB_RESULT || type == ITEM_JOB_INFO || type == ITEM_JOB_RESULT_INPUT_MOLCAS) {
            m_pTbJob->EnableTool(TOOL_ID_JOB_SUBMIT, true);
            Tools::EnableMenu(GetMenuBar(), MENU_ID_JOB_SUBMIT, true);
            return;
        }
    }
    m_pTbJob->EnableTool(TOOL_ID_JOB_SUBMIT, false);
}
void SimuPrjFrm::OnJobMonitor(wxCommandEvent &event) {
    //  fprintf(stderr,"SimuPrjFrm::OnJobMonitor");
    //  wxMessageBox(wxT("Welcome to use Job Monitor test version!"));
    Manager::Get()->GetMonitorDlg()->Show();
    Manager::Get()->GetMonitorPnl()->GetRemoteJobInfoList();
}
void         SimuPrjFrm::OnUpdateUIJobMonitor(wxUpdateUIEvent &event)
{
    //fprintf(stderr,"SimuPrjFrm::OnUpdateUIJobMonitor");
}
//..................................................................................................
void SimuPrjFrm::OnHelp(wxCommandEvent &event) {
    if(m_helpCtrl == NULL) {
        m_helpCtrl = new wxHtmlHelpController;
        //begin: hurukun,04/11/2009
        //#define _MOLCAS
        //wxString path=EnvUtil::GetBaseDir().Left(EnvUtil::GetBaseDir().Length()-4) +wxT("/gui/doc/simupac/simupac.hhp");

        if(!m_helpCtrl->AddBook(wxFileName(EnvUtil::GetPrgResDir() + wxT("/doc/simupac/simupac.hhp")))) {
            // if use zip file, please uncomment the follwing line, and comment the above line.
            // and zip all files as simupac.zip
            //if(!m_helpCtrl->Initialize(EnvUtil::GetHomeDir() + wxT("/doc/simupac"))) {
            wxLogError(wxT("Cannot initialize the MolGUI help system."));
        }

        //end: hurukun,04/11/2009
        //------------XIA 09workshop--------
        if(!m_helpCtrl->AddBook(wxFileName(EnvUtil::GetPrgResDir() + wxT("/doc/ming/ming.hhp")))) {
        wxLogError(wxT("Cannot initialize the MING help system."));
        }
        //--------------------------
    }
    m_helpCtrl->DisplayContents();
}
//---------------------XIA 09workshop---------
void SimuPrjFrm::OnAbout(wxCommandEvent &event) {
    /*wxMessageBox(_T("Welcome to Simu-Q.\n\n(c) 2009 BNU"),
                    _T("About Simu-Q program"),
                    wxOK | wxICON_INFORMATION,
                    this);*/
    wxBoxSizer *topsizer;
    wxHtmlWindow *html;
    wxDialog dlg(this, wxID_ANY, wxString(_("About MolGUI")), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER);

    topsizer = new wxBoxSizer(wxVERTICAL);
    //begin: hurukun 04/11/2009

    wxString filePath = EnvUtil::GetPrgResDir() + wxT("/doc/about/about.html");

    //end : hurukun 04/11/2009
    html = new wxHtmlWindow(&dlg, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO);
    html -> SetBorders(0);
    //html -> LoadPage(wxT(EnvUtil::GetBaseDir()+wxT("/doc/about/about.html")));
    //begin: hurukun 04/11/2009
    html->LoadPage(filePath);
    // Alternate way to read the file so we can perform some search and replace.
    // The images referenced in the default start page can be found now because we used LoadPage() above...
    wxString buf;
    wxFileSystem *fs = new wxFileSystem;
    wxFSFile *fsFile = fs->OpenFile(filePath);
    if (fsFile) {
        wxInputStream *is = fsFile->GetStream();
        char tmp[1024] = {0};
        while (!is->Eof() && is->CanRead()) {
            memset(tmp, 0, sizeof(tmp));
            is->Read(tmp, sizeof(tmp) - 1);
            buf << StringUtil::CharPointer2String((const char *)tmp);
        }
        delete fsFile;
    }
    else
        buf = wxT("<HTML><BODY><H1>Welcome to ") + appglobals::appName + wxT("</H1><BR>The default start page seems to be missing...</BODY></HTML>");
    delete fs;

    wxString revInfo = wxT("Release ") + appglobals::appVersion + wxT(": Built on ") + appglobals::appBuildTimestamp;
    // perform var substitution
    buf.Replace(wxT("SIMU_VAR_REVISION_INFO"), revInfo);
    html->SetPage(buf);
    //end  : hurukun 04/11/2009
    //   html -> LoadPage(filePath);
    html -> SetInitialSize(wxSize(html -> GetInternalRepresentation() -> GetWidth() + 20,
                                  600
                                  )
                          );

    topsizer -> Add(html, 1, wxALL, 10);

#if wxUSE_STATLINE
    topsizer -> Add(new wxStaticLine(&dlg, wxID_ANY), 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
#endif // wxUSE_STATLINE

    wxButton *bu1 = new wxButton(&dlg, wxID_OK, _("OK"));
    bu1 -> SetDefault();

    topsizer -> Add(bu1, 0, wxALL | wxALIGN_RIGHT, 15);

    dlg.SetSizer(topsizer);
    topsizer->SetSizeHints(&dlg);
    dlg.Fit();

    dlg.ShowModal();
}
//end   : hurukun : 7/12/2009
//****************************************************************************************************
//
//****************************************************************************************************
bool SimuPrjFrm::DoFilePretreatment(wxString &fileName)
{
    if(!wxFileExists(fileName))
        return false;
    wxString lowerCaseFile = fileName.Lower();

    if(lowerCaseFile.EndsWith(wxT(".gm"))) {
    } else if(lowerCaseFile.EndsWith(wxT(".xyz"))) {
    } else if(lowerCaseFile.EndsWith(wxT(".den"))) {
    } else if(lowerCaseFile.EndsWith(wxT(".vib"))) {
    } else if(lowerCaseFile.EndsWith(wxT(".pdb"))) {
        wxString xyzFileName, xyzHFileName;
        xyzFileName = FileUtil::ChangeFileExt(fileName, wxT(".xyz"));
        xyzHFileName = FileUtil::ChangeFileExt(fileName, wxT("_h.xyz"));
    } else if(lowerCaseFile.EndsWith(wxT(".ir"))) {
    } else if(lowerCaseFile.EndsWith(wxT(".m2msi"))) {
    } else {
    }
    return true;
}
void SimuPrjFrm::DoCheckMenuWindow(int winId)
{
    int i;
    int count = MENU_ID_WIN_START + GetMolData()->GetCtrlInstCount();

    for (i = MENU_ID_WIN_START; i < count; i++) {
        MolViewEventHandler::Get()->GetWinMenu()->Check(i, i == winId);
    }

    int ctrlId = winId - MENU_ID_WIN_START;
    if (ctrlId != GetMolData()->GetActiveCtrlInstIndex()) {
        GetMolData()->SetActiveCtrlInstIndex(ctrlId);
        m_pMolView->SetMenuToolCtrlInst();
        m_pMolView->RefreshGraphics();
        UpdateFrameTitle(MolViewEventHandler::Get()->GetWinMenu()->GetLabel(winId));
        Tools::ShowStatus(wxString::Format(wxT("CtrlInst %d is selected"), ctrlId), 0);
    }
}
void SimuPrjFrm::EnableMenu(bool IsEnable)
{
}
void SimuPrjFrm::EnableMenuTool(bool enabled)
{
    Tools::EnableMenu(GetMenuBar(), wxID_CLOSE, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_FILE_CLOSE, enabled);
    //----09.6.5/XIA-------
    m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVE, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVE, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVEALL, enabled);
    //------------------------
    //Tools::EnableMenu(GetMenuBar(), MENU_ID_BUILD_VIEW, enabled);

    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_VIEW, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_BUILD_VIEW, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_ADD_FRAGMENT, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_BUILD_ADD_FRAGMENT, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_INSERT_FRAGMENT, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_BUILD_INSERT_FRAGMENT, enabled);

    //Tools::EnableMenu(GetMenuBar(), MENU_ID_WIN_CLOSE_ALL, enabled);
    Tools::EnableMenu(GetMenuBar(), wxID_FORWARD, enabled);
    Tools::EnableMenu(GetMenuBar(), wxID_BACKWARD, enabled);
}
void SimuPrjFrm::EnableMenuToolCtrlInst(void)
{
    Tools::ShowStatus(wxEmptyString, 2);

    bool enabled = false;
    CtrlInst *pCtrlInst = GetMolData()->GetActiveCtrlInst();

    enabled = false;
    if (pCtrlInst != NULL && pCtrlInst->IsDirty())
        enabled = true;
    Tools::EnableMenu(GetMenuBar(), wxID_SAVE, enabled);
    //--------------09.6.5/XIA--------
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_PRJ_SAVE, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVEALL, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_SAVE, enabled);
    //---------------------------------
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_FILE_SAVE, enabled);

    enabled = false;
    if (pCtrlInst != NULL && pCtrlInst->GetRenderingData()->GetAtomCount() > 0)
        enabled = true;
    Tools::EnableMenu(GetMenuBar(), wxID_SAVEAS, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_EXPORT_FILE, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_EXPORT_IMAGE, enabled);
    Tools::EnableMenu(GetMenuBar(), MENU_ID_FILE_EXPORT, enabled);
    m_pMolView->EnableMenuToolCtrlInst();
}
void SimuPrjFrm::EnableTool(int toolId, bool enabled)
{
    if(m_pTbProject->FindById(toolId)) {
        if (m_pTbProject->GetToolEnabled(toolId) != enabled) {
            m_pTbProject->EnableTool(toolId, enabled);
        }
        return;
    }
    if(m_pTbJob->FindById(toolId)) {
        if (m_pTbJob->GetToolEnabled(toolId) != enabled) {
            m_pTbJob->EnableTool(toolId, enabled);
        }
        return;
    }
}
void SimuPrjFrm::UpdateFrameTitle(const wxString &fileName)
{
    wxString title = fileName;
    if (StringUtil::IsEmptyString(title)) {
        //        title = wxGetApp().GetAppName();
    } else {
        //        title += wxT(" - ") + wxGetApp().GetAppName();
    }
    SetTitle(title);
}
bool SimuPrjFrm::DoFileCloseAll(void)
{
    CtrlInst *pCtrlInst = GetMolData()->GetActiveCtrlInst();
    //    while (GetMolData()->GetCtrlInstCount() > 0)
    while(pCtrlInst->CanUndo())
    {
        if (!DoFileCloseWithMenuUpdate()) {
            return false;
        }
    }
    return true;
}
bool SimuPrjFrm::DoFileCloseWithMenuUpdate(void)
{
    bool isClose = m_pMolView->Close();
    if (!isClose) return false;
    int index = GetMolData()->GetActiveCtrlInstIndex();
    int count = GetMolData()->GetCtrlInstCount();
    MolViewEventHandler::Get()->GetWinMenu()->Delete(MENU_ID_WIN_START + index);
    int i;
    wxString label;
    for (i = index + 1; i < count; i++) {
        label = MolViewEventHandler::Get()->GetWinMenu()->GetLabel(MENU_ID_WIN_START + i);
        MolViewEventHandler::Get()->GetWinMenu()->Delete(MENU_ID_WIN_START + i);
        MolViewEventHandler::Get()->GetWinMenu()->AppendCheckItem(MENU_ID_WIN_START + i - 1, label);
    }
    GetMolData()->RemoveCtrlInst(index);
    if (GetMolData()->GetCtrlInstCount() > 0) {
        GetMolData()->SetActiveCtrlInstIndex(GetMolData()->GetCtrlInstCount() - 1);
        DoCheckMenuWindow(MENU_ID_WIN_START + GetMolData()->GetCtrlInstCount() - 1);
    } else {
        GetMolData()->SetActiveCtrlInstIndex(BP_NULL_VALUE);
        GetMolData()->ResetUntitledWinCount();

        AuiSharedWidgets::Get()->ShowExclusivePanel(wxEmptyString);
        UpdateFrameTitle(wxT(""));
        EnableMenuTool(false);
        EnableMenuToolCtrlInst();
    }
    m_pMolView->RefreshGraphics();
    return true;
}

void SimuPrjFrm::OnViewCaptureImage(wxCommandEvent &event)
{
    wxImage::AddHandler(new wxBMPHandler);
    if (Manager::Get()->GetViewManager()->GetViewCount() <= 1) {
        wxMessageBox(wxT("There is no right view to capture image with!"));
        return ;
    } else {
        if (Manager::Get()->GetViewManager()->GetActiveView()->GetTitle().Cmp(wxT("Start here")) == 0) {
            wxMessageBox(wxT("It is not a right view to set capture image with!"));
            return ;
        }
    }


    wxString    filter = wxT("TIFF files (*.tiff)|*.tiff|JPG files (*.jpg)|*.jpg"
                             "|BMP files (*.bmp)|*.bmp|PNG files (*.png)|*.png");

    wxString defDir = EnvUtil::GetProjectDir();

    wxString path;

    wxString fname = Manager::Get()->GetViewManager()->GetActiveView()->GetTitle();

    wxFileDialog fileDialog(Manager::Get()->GetAppWindow(), wxT("Save Image..."), defDir, fname, filter, wxFD_SAVE);


    wxString filters[4] = { wxT(".tiff") , wxT(".jpg") , wxT(".bmp") , wxT(".png") };
    wxString name;

#if defined (__WINDOWS__)

    wxSize clientSize = Manager::Get()->GetViewManager()->GetActiveView()->GetClientSize();

    int x = 0;
    int y = 0;

    Manager::Get()->GetViewManager()->GetActiveView()->ClientToScreen(&x, &y);

    int width = clientSize.x;
    int height = clientSize.y;

    if (        AuiSharedWidgets::Get()->GetElemPnl()->IsShown() ) {

        int  xelemPnl = 0;
        int  yelemPnl = 0;

        AuiSharedWidgets::Get()->GetElemPnl()->ClientToScreen(&xelemPnl, &yelemPnl);

        width = xelemPnl - x - 7;
    }

    wxBitmap bitmap(width, height);

    wxScreenDC dc ;

    wxMemoryDC mdc;



    mdc.SelectObject(bitmap);

    mdc.Blit(0, 0, width, height, &dc, x ,  y );

    mdc.SelectObject(wxNullBitmap);


    wxImage image = bitmap.ConvertToImage();

#else
    MolViewEventHandler::Get()->GetMolView()->GetGLCanvas()->Draw();
    wxSize clientSize = MolViewEventHandler::Get()->GetMolView()->GetGLCanvas()->GetClientSize();

    wxClientDC clientdc(MolViewEventHandler::Get()->GetMolView()->GetGLCanvas());
    wxBitmap bitmap(clientSize.GetWidth(), clientSize.GetHeight());
    wxMemoryDC mdc;
    mdc.SelectObject(bitmap);

    mdc.Blit(0, 0, clientSize.GetWidth(), clientSize.GetHeight(), &clientdc, 0 , 0 );

    mdc.SelectObject(wxNullBitmap);
    wxImage image = bitmap.ConvertToImage();

    // it does not support saving alpha channel 
#endif
    while (1)
    {
        if (fileDialog.ShowModal() == wxID_OK) {

            path = fileDialog.GetPath();

            if ( wxNOT_FOUND == path.Find(wxChar('.')) ) {
                path += filters[fileDialog.GetFilterIndex()];
            }

            if (wxFile::Access(path.c_str(), wxFile::read)) {

                wxMessageDialog d(this, wxT("File exists! Override?"), wxT("Warning!"), wxYES_NO);

                if (d.ShowModal() == wxID_YES) {

                    if ( path.Right(4).IsSameAs(wxT(".jpg"))) {

                        image.SaveFile(path, wxBITMAP_TYPE_JPEG);

                        break;

                    } else if (path.Right(5).IsSameAs(wxT(".tiff")) ) {

                        image.SaveFile(path, wxBITMAP_TYPE_TIF);

                        break;

                    } else if (path.Right(4).IsSameAs(wxT(".bmp"))) {

                        image.SaveFile(path, wxBITMAP_TYPE_BMP);

                        break;
                    } else if (path.Right(4).IsSameAs(wxT(".png"))) {

                        image.SaveFile(path, wxBITMAP_TYPE_PNG);

                        break;
                    } else {

                        wxMessageDialog d(this, wxT("Format Error!"), wxT("Error!"), wxYES_NO);
                        d.ShowModal();

                    }

                }        else {
                    continue;
                }
            }        else {

                if ( path.Right(4).IsSameAs(wxT(".jpg"))) {

                    image.SaveFile(path, wxBITMAP_TYPE_JPEG);

                    break;

                } else if (path.Right(5).IsSameAs(wxT(".tiff")) ) {

                    image.SaveFile(path, wxBITMAP_TYPE_TIF);

                    break;

                } else if (path.Right(4).IsSameAs(wxT(".bmp"))) {

                    image.SaveFile(path, wxBITMAP_TYPE_BMP);

                    break;
                } else if (path.Right(4).IsSameAs(wxT(".png"))) {

                    image.SaveFile(path, wxBITMAP_TYPE_PNG);

                    break;
                } else {

                    wxMessageDialog d(this, wxT("Format Error!"), wxT("Error!"), wxYES_NO);
                    d.ShowModal();
                    continue;
                }
            }

        } else {
            break;
        }
    }

}
//.......................................................................
void        SimuPrjFrm::OnResize(wxSizeEvent &event) {
    wxSize        tmpSize;
    if(m_pTbProject != NULL)
        tmpSize = m_pTbProject->GetSize();
    if(m_pTbJob != NULL)
        tmpSize = m_pTbJob->GetSize();
    AuiSharedWidgets *widget = AuiSharedWidgets::Get();
    wxToolBar        *tmpBar = NULL;
    if(widget != NULL) {
        tmpBar = widget->GetToolBar(1);
        if(tmpBar != NULL) {
        }
        tmpBar = NULL;
        tmpBar = widget->GetToolBar(1);
        if(tmpBar != NULL) {
        }
    }

    //        wxString msg=wxString::Format(wxT("width=%d,height=%d"),event.GetSize().GetWidth(),event.GetSize().GetHeight());
    //        wxMessageBox(msg);
    event.Skip();
}
