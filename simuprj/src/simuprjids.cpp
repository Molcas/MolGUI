/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "simuprjdef.h"

const wxString STR_PNL_VIBRATION = wxT("PnlVibration");
const wxString STR_PNL_SURFACE = wxT("PnlSurface");

const wxString STR_TB_PROJECT = wxT("TbProject");
const wxString STR_TB_JOB = wxT("TbJob");
