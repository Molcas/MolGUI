/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "manager.h"

#include "configmanager.h"
#include "projectmanager.h"
#include "viewmanager.h"
#include "envsettings.h"

static Manager* managerInstance = NULL;

bool Manager::m_appShuttingDown = false;

Manager::Manager() {
        m_pAppFrame = NULL;
        m_pDlgMonitor = NULL;
        m_pPnlMonitor = NULL;
}

Manager::~Manager() {
    if(m_pDlgMonitor!=NULL)
        m_pDlgMonitor->Destroy();
    m_pDlgMonitor=NULL;
    if(m_pPnlMonitor!=NULL)
        m_pPnlMonitor->Destroy();
     m_pPnlMonitor=NULL;
}

Manager* Manager::Get(void) {
        if(!managerInstance)
                managerInstance = new Manager;
    return managerInstance;
}

void Manager::Free(void) {
    if(managerInstance!=NULL)
        delete managerInstance;
    managerInstance = NULL;
}

wxFrame* Manager::GetAppFrame() const {
    return m_pAppFrame;
}

wxWindow* Manager::GetAppWindow() const {
    return m_pAppFrame;
}

void Manager::SetAppFrame(wxFrame* frame) {
        m_pAppFrame = frame;
}

ConfigManager* Manager::GetConfigManager(void) const {
        return ConfigManager::Get();
}

ProjectManager* Manager::GetProjectManager(void) const {
        return ProjectManager::Get();
}

ViewManager* Manager::GetViewManager(void) const {
        return ViewManager::Get();
}

bool Manager::IsAppShuttingDown(void) {
        return m_appShuttingDown;
}

void Manager::Shutdown(void) {
    m_appShuttingDown = true;

        ProjectManager::Free();
        ViewManager::Free();
        ConfigManager::Free();
        EnvSettingsData::Free();
        Free();
}

GenericDlg* Manager::GetMonitorDlg(void) {
        if(!m_pDlgMonitor) {
                m_pDlgMonitor = new GenericDlg(GetAppFrame(), wxID_ANY, wxT("Job Monitor"));
                m_pPnlMonitor = new JobMonitorPnl(m_pDlgMonitor);
                m_pDlgMonitor->SetGenericPanel(m_pPnlMonitor);
        }
        return m_pDlgMonitor;
}

JobMonitorPnl* Manager::GetMonitorPnl(void) {
        if(!m_pPnlMonitor) {
                GetMonitorDlg();
        }
        return m_pPnlMonitor;
}
