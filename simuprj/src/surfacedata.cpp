/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
//#include <wx/sstream.h>
#include <wx/tokenzr.h>
#include "mcube.h"
#include "fileutil.h"
#include "surfacedata.h"
#include "tools.h"

const wxString FILE_BEGIN_TRIS = wxT("$BEGIN TRIS");
const wxString FILE_END_TRIS = wxT("$END TRIS");
const wxString FILE_BEGIN_TRI = wxT("$BEGIN TRI");
const wxString FILE_END_TRI = wxT("$END TRI");

#define MAXVERTS 20000

static GLfloat verts[MAXVERTS][3];
static GLfloat norms[MAXVERTS][3];
static GLint clr[MAXVERTS];
static GLint numverts;

SurfaceThread::SurfaceThread(SurfaceData * surfaceData):wxThread()
{
m_surfaceData=surfaceData;
}

void SurfaceThread::OnExit(){
delete m_surfaceData;
}

void *SurfaceThread::Entry(){
        int i;
        for (i=0;i<m_surfaceData->GetGridInfo().ngrids;i++){
        m_surfaceData->GetAllSurfaces()->curmo =i;
        m_surfaceData->LoadGrids(i,0);
        m_surfaceData->SaveSrf(i);
        wxThread::Sleep(100);
        }
        return NULL;
}


SurfaceData::SurfaceData()
{
}

SurfaceData::~SurfaceData()
{
        if(All_Surfaces){
                delete[] All_Surfaces;
                All_Surfaces=NULL;}
        if (currentGridData){
                iso_InitGridInformation( currentGridData);
                currentGridData=NULL;}
                m_surfaceThread=NULL;


}

void SurfaceData::LoadGridHeader()
{
int i;
if (!iso_LoadGridHeader(Input_Data.infile_mask,1,Input_Data.coord_input,Input_Data.translate,&Input_Data.isFreq,currentGridData)) {
   MyString::SayError("Can't load grid header.\n");
   exit(1);
 }
#ifdef _DEBUG_D
 Debug("Grid header loaded");
#endif
 iso_GetInfo(&Grid_info,currentGridData);
#ifdef _DEBUG_D
 Debug("nx=%d ny=%d nz=%d natoms=%d ngrids=%d",
       Grid_info.grd_sz[0],Grid_info.grd_sz[1],Grid_info.grd_sz[2],
       Grid_info.natomsReal,
       Grid_info.ngrids
      );
#endif
// UpdateStatus();
 Input_Data.max_diam_ok=0; /* diameter is changed */

 /* ? FIXME: move to separate function and separate file? For it's not a wrapper...*/
 /* allocate array for grids */
 if (Input_Data.surf) { free(Input_Data.surf); }
 Input_Data.surf=(surface_t**)malloc(sizeof(surface_t*)*Grid_info.ngrids);
 for (i=0; i<Grid_info.ngrids; i++) {
   Input_Data.surf[i]=0;
 }
if (Input_Data.cur_mo==0) {
   Input_Data.cur_mo=Grid_info.ngrids-1;
 } else {
   Input_Data.cur_mo--;
 }

}

void SurfaceData::Init(const wxString fileName)
{
Input_Data.hide_atoms=0;
Input_Data.hide_bonds=0;
Input_Data.hide_status=0;
Input_Data.anim=0;
Input_Data.transp=0.699999988f;
Input_Data.n_lev_steps=100;
Input_Data.lev_step=0.0;
Input_Data.fullscreen=0;
Input_Data.atomshape=0;
Input_Data.style_atoms=0;
Input_Data.label_atoms=0;
Input_Data.labeln_atoms=1;
Input_Data.labelg_atoms=1;
Input_Data.bgcolor[0]=1.0;
Input_Data.bgcolor[1]=1.0;
Input_Data.bgcolor[2]=1.0;
Input_Data.translate=0;
Input_Data.translate_step=0.1;
Input_Data.coord_input=0;
Input_Data.cur_file=0;
Input_Data.isFreq=0;

 /* levels, MO, transparency */
Input_Data.lev=0.04;
Input_Data.auto_lev=0;
Input_Data.cur_mo=0;
  /* Stepping through isolevels */
Input_Data.surf=NULL;
Input_Data.infile_mask=fileName;
//natomsReal=0;
currentGridData=new Grid_t();
currentGridData->values=NULL;

}

multisrf_t * SurfaceData::GetAllSurfaces()
{
return All_Surfaces;
}

int SurfaceData::GetCurmo(void) const{
return All_Surfaces->curmo;
}

void SurfaceData::SetCurmo(int mo){
All_Surfaces->curmo=mo;
for (int i=0; i<All_Surfaces->n_surfaces; i++) {
   surface_t* child=All_Surfaces->child[i];
   if (child) {
     child->is_valid=0;
         child->is_sorted=0;
   }
 }
}

void SurfaceData::LoadGrid(int mo, wxFFile *outf)
{
surface_t* srf;
 int do_minus=1;
 double* data_ptr;
 double rad[3];
 double dummy,min,max;
/*
 if(Input_Data.coord_input==2)
 {
 return;
 }
*/
 if (mo<0) { /* special case: "force reload", used in 1st-time loading */
   mo=Input_Data.cur_mo;
   Input_Data.cur_mo=-1;
 } else { /* general case */
//   if (Input_Data.surf[mo]) return; /* already shown */
 }

 //if (mo!=Input_Data.cur_mo) { /* not yet loaded */
        //wxMessageBox(wxT("iso_LoadGrid"));
   if (!iso_LoadGrid(Input_Data.infile_mask,mo,0,1,0,0,Input_Data.coord_input,currentGridData)) {
     MyString::SayError("Can't load grid from file.\n");
     exit(1);
 //  }
#ifdef _DEBUG_D
   Debug("Grid loaded");
#endif
   Input_Data.cur_mo=mo;
        //wxMessageBox(wxT("for newly loaded grid"));
   /* for newly loaded grid */
   /* calculate isolevel limits */
   iso_CurrentGridLimits(&min,&max,&dummy,currentGridData);
   if (min<0. && max<0.) {
     Input_Data.lev_min=-max;
     Input_Data.lev_max=-min;
   } else if (min<0. && max>0.) {
     Input_Data.lev_min=0.;
     Input_Data.lev_max=max;
   } else {
     Input_Data.lev_min=min;
     Input_Data.lev_max=max;
   }

   /* calculate isolevel step, if necessary */
   if (Input_Data.n_lev_steps) {
     Input_Data.lev_step
       =(Input_Data.lev_max-Input_Data.lev_min)/Input_Data.n_lev_steps;
   }

   if (Input_Data.auto_lev) {
     Input_Data.lev=0.75*Input_Data.lev_min+0.25*Input_Data.lev_max;
   }
 }
        //wxMessageBox(wxT("iso_CurrentGridData"));
 iso_CurrentGridData(&data_ptr,currentGridData);
        //wxMessageBox(wxT("iso_CurrentGridData OK"));
 rad[0]=Grid_info.axis1[0];
 rad[1]=Grid_info.axis2[1];
 rad[2]=Grid_info.axis3[2];

 if (outf) { /* output to file, not to multisurface */
        //wxMessageBox(wxT("mcubes"));
    mcubes(0,
           Grid_info.grd_sz[0],Grid_info.grd_sz[1],Grid_info.grd_sz[2],
           data_ptr,Grid_info.origin,Input_Data.lev,rad,do_minus);
        //wxMessageBox(wxT("mcubes ok"));
    return;
 }
 srf=Input_Data.surf[mo]=msrf_New_Surface(All_Surfaces,mo);
         //wxMessageBox(wxT("Surface added"));
#ifdef _DEBUG_D
 Debug("Surface added, srf=%p",srf);
#endif
 /* do_minus=(mo==Grid_info.ngrids-1)?0:1; only positive, if density */
 do_minus=1;

 mcubes(srf,
        Grid_info.grd_sz[0],Grid_info.grd_sz[1],Grid_info.grd_sz[2],
        data_ptr,Grid_info.origin,Input_Data.lev,rad,do_minus);
#ifdef _DEBUG_D
 Debug("mcubes() done");
#endif
// UpdateStatus();
 Input_Data.max_diam_ok=0; /* diameter is changed */
 Input_Data.shown_mo=mo; /* this MO is shown */
#ifdef _DEBUG_D
 Debug("Load Grid done");
#endif
}

void SurfaceData::AllSurfaceInit()
{
All_Surfaces=msrf_Init(0,Grid_info.ngrids);
All_Surfaces->curmo=0;
}

void SurfaceData::RotateSurface(double transX, double transY, double transZ,float rotx,float roty,float rotz)
{
int i;
 for (i=0; i<All_Surfaces->n_triangles; i++) {
   triangle_t* triangle=All_Surfaces->index[i];
   int j;
   for (j=0; j<3; j++) {
//         Translate(triangle->normal[j],-transX,-transY,-transZ);
         Translate(triangle->vertex[j],-transX,-transY,-transZ);
     RotateWithX(triangle->normal[j],rotx);
     RotateWithX(triangle->vertex[j],rotx);
     RotateWithY(triangle->normal[j],roty);
     RotateWithY(triangle->vertex[j],roty);
//     RotateWithZ(triangle->normal[j],rotz);
     RotateWithZ(triangle->vertex[j],rotz);
//         Translate(triangle->normal[j],transX,transY,transZ);
         Translate(triangle->vertex[j],transX,transY,transZ);
  }
 }
}

void SurfaceData::RotateWithX(point_t &pos,double x)
{
        double tempY, tempZ;
        double cosAlpha = cos(x * RADIAN);
        double sinAlpha = sin(x * RADIAN);

        tempY = pos[1] * cosAlpha - pos[2] * sinAlpha;
        tempZ = pos[1] * sinAlpha + pos[2] * cosAlpha;

        pos[1] = tempY;
        pos[2] = tempZ;

}

void SurfaceData::RotateWithY(point_t &pos, double y)
{
        double tempX, tempZ;
        double cosAlpha = cos(y * RADIAN);
        double sinAlpha = sin(y * RADIAN);

        tempX = pos[0] * cosAlpha + pos[2] * sinAlpha;
        tempZ = pos[0] * (-sinAlpha) + pos[2] * cosAlpha;

        pos[0] = tempX;
        pos[2] = tempZ;

}

void SurfaceData::RotateWithZ(point_t &pos, double z)
{
        double tempX, tempY;
        double cosAlpha = cos(z * RADIAN);
        double sinAlpha = sin(z * RADIAN);

        tempX = pos[0] * cosAlpha - pos[0] * sinAlpha;
        tempY = pos[1] * sinAlpha + pos[1] * cosAlpha;

        pos[0] = tempX;
        pos[1] = tempY;
}

wxString SurfaceData::GetMOInfo(int row, int col)
{
wxString tmp,grdinfo;
wxStringTokenizer tkn(Grid_info.titles, wxT(","));
int i=row;
while(tkn.HasMoreTokens() && i>=0){
        tmp=tkn.GetNextToken();
        i--;
}
wxStringTokenizer tkn2(tmp, wxT(" "));
i=col;
while(tkn2.HasMoreTokens() && i>0){
        grdinfo=tkn2.GetNextToken();
        i--;
}
return grdinfo;
}

//DEL void SurfaceData::LoadGH()
//DEL {
//DEL         wxString strLine;
//DEL         wxFileInputStream fis(wxT("abc.M2Msi"));
//DEL         wxTextInputStream tis(fis);
//DEL         wxString strTmp;
//DEL     wxStringTokenizer tzk(strLine);
//DEL         for(int i=0;i<10;i++){
//DEL         strLine = tis.ReadLine();
//DEL     strTmp=tzk.GetNextToken();
//DEL         strTmp=tzk.GetNextToken();
//DEL         strTmp=tzk.GetNextToken();
//DEL         }
//DEL }

void SurfaceData::Translate(point_t &pos,double transX, double transY, double transZ)
{
pos[0]+=transX;
pos[1]+=transY;
pos[2]+=transZ;
}


void SurfaceData::RenderSurface(void)
{
//        SaveSrf();
        color_t red={ 1.0F, 0.0F, 0.0F, 0.4F };
         color_t green={ 0.0F, 1.0F, 0.0F, 0.4F };
         color_t cl[2];
         Color_Copy(cl[0],red);
         Color_Copy(cl[1],green);
/*
         if(m_renderingStyle==2) {
                 glDisable(GL_LIGHTING);
         }
        if (m_renderingStyle==0){
                glEnable(GL_BLEND); // 打开混合
                glDisable(GL_DEPTH_TEST); // 关闭深度测试
        }

        if(m_renderingStyle==2){
        glBegin(GL_LINES);
        }else{
        glBegin(GL_TRIANGLES);
        }

         for (int i=0; i<numverts; i=i+3) {
           int j;
                 glColor4fv(cl[clr[int(i/3)]]);
                if(m_renderingStyle==2){
                   glVertex3fv(verts[i+0]);
                   glVertex3fv(verts[i+1]);
                   glVertex3fv(verts[i+1]);
                   glVertex3fv(verts[i+2]);
                }else{
                   for (j=0; j<3; j++) {
                         glNormal3fv(norms[i+j]);
                         glVertex3fv(verts[i+j]);
                   }
                }
           }
         glEnd();
*/
         msrf_Draw(All_Surfaces,cl,m_renderingStyle);
         if(m_renderingStyle==2) {
                 glEnable(GL_LIGHTING);
         }
        if (m_renderingStyle==0){
                glDisable(GL_BLEND); // 打开混合
                glEnable(GL_DEPTH_TEST); // 关闭深度测试
        }

}

void SurfaceData::SaveSurface()
{
        wxString fileName=_T("c://test.srf");
    int i,j;

    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        wxString format = wxEmptyString;
        surface_t * srf=0;
        tos<<wxString::Format(wxT("%d"),All_Surfaces->n_surfaces)<<endl;
        for(int nchild=0;nchild<All_Surfaces->n_surfaces;nchild++){
                srf=All_Surfaces->child[nchild];
                if(srf){
                //        tos << FILE_BEGIN_TRIS << endl;
                        triangle_t_s * pri=0;
                        pri=srf->head;
                        tos<<wxString::Format(wxT("%d"),srf->n_triangles)<<endl;
                        tos<<wxString::Format(wxT("%d"),srf->offset)<<endl;
                        for (int mi=0; mi<3; mi++) {
                                tos<<wxString::Format(wxT("%.8f"),srf->max_coor[mi])<<endl;
                                tos<<wxString::Format(wxT("%.8f"),srf->min_coor[mi])<<endl;
                        }
                        tos<<wxString::Format(wxT("%d"),srf->is_sorted)<<endl;
                        tos<<wxString::Format(wxT("%d"),srf->is_valid)<<endl;
                        for(i = 0; i < srf->n_triangles; i++) {

                //                tos << FILE_BEGIN_TRI << endl;

                                tos<<pri->clr_idx<<endl;
                                tos<<wxString::Format(wxT("%.8f"),pri->distance )<<endl;
                                for(j=0;j<3;j++){
                                   format = wxT(" %.8f %.8f %.8f\n");
                                        tos << wxString::Format(format, pri->normal[j][0],pri->normal[j][1],pri->normal[j][2] );
                                        tos << wxString::Format(format, pri->vertex[j][0],pri->vertex[j][1],pri->vertex[j][2] );
                                }
                                pri=pri->lnk_prev;
                //                tos << FILE_END_TRI << endl;
                        }
                //        tos << FILE_END_TRIS << endl;
                }
        }
}


void SurfaceData::LoadSurface()
{
        wxString fileName=_T("c://test.srf");
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        AllSurfaceInit();
        tis >> All_Surfaces->n_surfaces;
//        for(int i=0;i<All_Surfaces->n_surfaces;i++){
                surface_t* new_child=0;
                new_child=(surface_t*)malloc(sizeof(surface_t));
                new_child->head=0;
                new_child->parent=All_Surfaces;
                tis>>new_child->n_triangles;
                tis>>new_child->offset;
                for (int mi=0; mi<3; mi++) {
                        tis>>new_child->max_coor[mi];
                        tis>>new_child->min_coor[mi];
                }
                tis>>new_child->is_sorted;
                tis>>new_child->is_valid;

                for(int ti = 0; ti < new_child->n_triangles; ti++) {
                        triangle_t * tri=(triangle_t*)malloc(sizeof(triangle_t));
                        int clr;
                        tis>>clr;
                        tri->clr_idx=clr;
                        tis>>tri->distance;
                        for(int tj=0;tj<3;tj++){
                               tis>>tri->normal[tj][0];
                                tis>>tri->normal[tj][1];
                                tis>>tri->normal[tj][2];
                                tis>>tri->vertex[tj][0];
                                tis>>tri->vertex[tj][1];
                                tis>>tri->vertex[tj][2];
                        }
                        tri->lnk_prev=new_child->head;
                        new_child->head=tri;
                }
                All_Surfaces->child[0]=new_child;
//        }
}

void SurfaceData::SaveSrf(int mo)
{
 int i;

  if (All_Surfaces->child[All_Surfaces->curmo]){
         if(!All_Surfaces->child[All_Surfaces->curmo]->is_valid) {
                if (All_Surfaces->index) free(All_Surfaces->index);
                 Make_Index(All_Surfaces);
                }

          if(!All_Surfaces->child[All_Surfaces->curmo]->is_sorted) {
                Sort_by_Depth(All_Surfaces);
                }
        wxString fileName;
        fileName.Printf(wxT("%d.srf"),mo);
        fileName=Input_Data.infile_mask+fileName;
        wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        wxString format = wxEmptyString;
        for (i=0; i<All_Surfaces->n_triangles; i++) {
           triangle_t* pri=All_Surfaces->index[i];
           int j;
                tos<<pri->clr_idx<<endl;
                for(j=0;j<3;j++){
                        format = wxT(" %.8f %.8f %.8f\n");
                        tos << wxString::Format(format, pri->normal[j][0],pri->normal[j][1],pri->normal[j][2] );
                        tos << wxString::Format(format, pri->vertex[j][0],pri->vertex[j][1],pri->vertex[j][2] );
                }
        }
}
}

void SurfaceData::LoadSrf(int mo)
{
    int test = 0;
    wxString fileName;
    fileName.Printf(wxT("%d.srf"), mo);
    fileName = Input_Data.infile_mask + fileName;

    FILE *f = wxFopen(fileName, _T("r"));
    if (!f)
    {
        wxString msg = _T("Couldn't read ");
        msg += fileName;
        wxMessageBox(msg);
        return;
    }

    numverts = 0;
    int iclr = 0;
    while (!feof(f) && numverts < MAXVERTS)
    {
        if(numverts % 3 == 0) {
            test = fscanf(f, "%d", &clr[iclr]);
            if(test != 1 || test == EOF)
            {
                wxMessageBox(_T("Undefined data in ")+fileName);
                fclose(f);
                return;
            }
            iclr++;
        }
        test = fscanf( f, "%f %f %f  %f %f %f",
                       &norms[numverts][0], &norms[numverts][1], &norms[numverts][2],
                       &verts[numverts][0], &verts[numverts][1], &verts[numverts][2]);
        numverts++;

    }

    numverts--;

    wxPrintf(_T("%d vertices, %d triangles\n"), numverts, numverts - 2);

    fclose(f);

}

void SurfaceData::SaveSrfs(wxString densityfile)
{
wxString fileName;
fileName.Printf(wxT("%d.srf"),0);
fileName=Input_Data.infile_mask+fileName;
if(!wxFileExists(fileName)){
        Init(densityfile);
        LoadGridHeader();
        AllSurfaceInit();
        LoadGrids(-1,0);
        m_surfaceThread = new SurfaceThread(this);
//        threadId = m_remoteThread->GetId();
        if(m_surfaceThread->Create() != wxTHREAD_NO_ERROR) {
                Tools::ShowInfo(wxT("Sorry, cannot create thread to save srf!"));
                }
        if(m_surfaceThread->Run() != wxTHREAD_NO_ERROR) {
                Tools::ShowInfo(wxT("Sorry, cannot start thread to save srf!"));
                }
        }
        return;
}

void SurfaceData::LoadGrids(int mo, wxFFile *outf)
{
LoadSrf(mo);
return;

}

grid_info_t SurfaceData::GetGridInfo(){
return Grid_info;
}
