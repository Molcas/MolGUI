/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/filename.h>
#include <wx/dir.h>

#include "projectfiles.h"
#include "simuprjdef.h"
#include "simuproject.h"
#include "fileutil.h"
#include "stringutil.h"
#include "envutil.h"
#include "tools.h"

#include "envsettings.h"
#include "manager.h"
#include "execprocess.h"
#include "surfacedata.h"
#include "projectgroup.h"

AbstractProjectFiles::AbstractProjectFiles(SimuProject* simuProject, wxString name) {
        m_pSimuProject = simuProject;
        m_name = name;
}

AbstractProjectFiles::~AbstractProjectFiles() {
}

wxString AbstractProjectFiles::GetName(void) const {
        return m_name;
}

bool AbstractProjectFiles::CreateInputFile(void) {
        return false;
}

wxString AbstractProjectFiles::GetExecCommand(void) {
        return wxEmptyString;
}

wxString AbstractProjectFiles::GetInputFileName(void) {
        return wxEmptyString;
}
wxString AbstractProjectFiles::GetOutputFileName(void) {
        return wxEmptyString;
}
wxString AbstractProjectFiles::GetOutFileName(wxString& fileSufffix)
{
        return wxEmptyString;
}
wxString AbstractProjectFiles::GetSummaryFileName() {
        return m_pSimuProject->GetResultFileName(m_name) + wxT(".sum");
}

wxString AbstractProjectFiles::GetExecPara(void) {
        return wxEmptyString;
}

wxString AbstractProjectFiles::GetExecLogName(void) {
        return wxEmptyString;
}

bool AbstractProjectFiles::IsFinal(void) {
        return false;
}

bool AbstractProjectFiles::PostJobTerminated(bool isFinal) {
        return true;
}

void AbstractProjectFiles::BuildResultTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode) {
        ProjectTreeItemData* itemData = NULL;
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_SUMMARY);
        pTree->AppendItem(jobResultNode, wxT("Summary"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_OUTPUT);
        pTree->AppendItem(jobResultNode, wxT("Output"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);

        if(HasSurfaceData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_DENSITY);
                pTree->AppendItem(jobResultNode, wxT("Density"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        }
        if(HasVibrationData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_FREQUENCE);
                pTree->AppendItem(jobResultNode, wxT("Vibration"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
                InitVibrationData();
        }

}

wxArrayString AbstractProjectFiles::GetSurfaceDataFiles(void) {
        wxArrayString fileArray;
        wxString fileName = m_pSimuProject->GetResultFileName(m_name);

        if(wxFileExists(fileName + wxT(".dens"))){
                fileArray.Add(fileName + wxT(".dens"));
        }else {
                if(wxFileExists(fileName + wxT("_a.dens")) ){
                        fileArray.Add(fileName + wxT("_a.dens"));
                }
                if(wxFileExists(fileName + wxT("_b.dens")) ){
                        fileArray.Add(fileName + wxT("_b.dens"));
                }
        }
        return fileArray;
/*        wxArrayString files;
        files.Clear();
        return files;
        */
}

void AbstractProjectFiles::InitVibrationData(void) {

}

void AbstractProjectFiles::ResetVibrationData(VibPropertyDisItemArray disArray) {
        m_vibArray.Clear();
        m_vibArray=disArray;
}

VibPropertyDisItemArray& AbstractProjectFiles::GetVibrationData() {
        return m_vibArray;
}

IRDatas* AbstractProjectFiles::GetIRDatas(void){
        return &(m_irs);
}

void AbstractProjectFiles::SetStartTime(wxLongLong startTime) {
        m_startTime = startTime;
}

wxLongLong AbstractProjectFiles::GetStartTime(void) const {
        return m_startTime;
}

wxString AbstractProjectFiles::GetVibOutputFileName()
{
//        return wxEmptyString;
        return m_pSimuProject->GetResultFileName(m_name) + wxT(".vib");
}

bool AbstractProjectFiles::HasVibrationData()
{
        return wxFileExists(GetVibOutputFileName());
}

bool AbstractProjectFiles::DoCalcSurface()
{
        wxString inputFileName = m_pSimuProject->GetResultDir(m_name) + platform::PathSep() + wxT("surface.info");;
        wxString outputFileName =m_pSimuProject->GetResultFileName(m_name);
        if(!wxFileExists(inputFileName)){
                Tools::ShowError(inputFileName + wxT("  does not exist!"));
                return false;
        }
        wxString execDir = EnvSettingsData::Get()->GetChemSoftDir(PROJECT_DENS);
        wxString controlFile=execDir+platform::PathSep()+wxT("a.control");
        execDir += platform::PathSep() + EnvUtil::GetProgramName(wxT("density"));
        if(!wxFileExists(execDir)) {
                Tools::ShowError(execDir + wxT("  does not exist!"));
                return false;
        }
        wxString execPara = wxT(" \"")+controlFile+wxT("\" \"") + inputFileName + wxT("\" \"") + outputFileName + wxT("\"");
        wxString execOutputDir = m_pSimuProject->GetResultDir(m_name);
        wxString projectName = m_pSimuProject->GetTitle();

        JobRunningData* pJobData = new JobRunningData(m_pSimuProject->GetProjectId(), m_pSimuProject->GetType(), m_pSimuProject->GetTitle());
        pJobData->SetProjectFile(m_pSimuProject->GetFileName());
        pJobData->SetOutputDir(execOutputDir);
        pJobData->SetCommand(execDir, execPara);
        pJobData->SetFinal(true);
        Manager::Get()->GetMonitorPnl()->ExecLocalJob(pJobData);
        return true;
}

bool AbstractProjectFiles::HasSurfaceData()
{
        wxArrayString fileList=GetSurfaceDataFiles();
        for(unsigned i = 0; i < fileList.GetCount(); i++) {
                if(wxFileExists(fileList[i])) {
                        return true;
                }
        }
        return false;
}

void  AbstractProjectFiles::SaveSurfaces(){
        wxArrayString fileList=GetSurfaceDataFiles();
        for(unsigned i = 0; i < fileList.GetCount(); i++) {
                if(wxFileExists(fileList[i])) {
                        SurfaceData* pSurfaceData = new SurfaceData;
                        pSurfaceData->SaveSrfs(fileList[i]);
//                        delete pSurfaceData;
                }
        }
        return;

}

bool AbstractProjectFiles::GetNeedSurface()
{
return m_needsurface;
}

void AbstractProjectFiles::SetNeedSurface(bool needsurface)
{
m_needsurface=needsurface;
}

bool AbstractProjectFiles::HasSummary()
{
wxString sumfile;
sumfile=GetSummaryFileName();
if (!wxFileExists(sumfile))return false;
wxFileInputStream fis(sumfile);
wxTextInputStream tis(fis);
wxString strflag=tis.ReadLine();
long flag;

if(!strflag.ToLong(&flag))flag=0;
if (flag==1)
{
        return false;
}
else
return true;
}

void AbstractProjectFiles::ShowResultItem(ProjectTreeItemData *item){
return;
}

bool AbstractProjectFiles::HasInput()
{
        return false;

}
bool AbstractProjectFiles::HasOutput()
{
        return false;

}
bool AbstractProjectFiles::HasEnergy_Statistics(){
        return false;
}
bool AbstractProjectFiles::HasError_Info(){
        return false;
}
int AbstractProjectFiles::HasDensity(){
        wxArrayString fileList=GetSurfaceDataFiles();
        for(unsigned i = 0; i < fileList.GetCount(); i++) {
                if(wxFileExists(fileList[i])) {
                        return i+1;
                }
        }
        return -1;
}
bool AbstractProjectFiles::HasOptimization(){
        return false;

}
bool AbstractProjectFiles::HasFrequence(){
        return wxFileExists(GetVibOutputFileName());
}
bool AbstractProjectFiles::HasGssOrb()
{
        return false;
}
bool AbstractProjectFiles::HasScfOrb()
{
        return false;
}
bool AbstractProjectFiles::HasDump()
{
        return false;
}
