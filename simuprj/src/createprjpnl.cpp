/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "createprjpnl.h"

#include <wx/filename.h>
#include <wx/dir.h>
#include <wx/checkbox.h>

#include "envutil.h"
#include "fileutil.h"
#include "appglobals.h"
#include "configmanager.h"
#include "tools.h"
#include "stringutil.h"

#include "manager.h"
#include "projectmanager.h"
#include "simuproject.h"
#include "projectgroup.h"

//int CTRL_ID_PROJECT_TYPE = wxNewId();
int CTRL_CRTPNL_ID_TXT_PRJNAME = wxNewId();
int CTRL_CRTPNL_ID_BTN_DIR = wxNewId();
int CTRL_CRTPNL_ID_TEXT_FILENAME = wxNewId();
int CTRL_CRTPNL_ID_COMB_PGNAME = wxNewId();
int CTRL_CRTPNL_ID_CBX_NOPRJGRP = wxNewId();
int CTRL_RB_PROJECTTYPE=wxNewId();

BEGIN_EVENT_TABLE(CreatePrjPnl, wxPanel)
    EVT_CHECKBOX(CTRL_CRTPNL_ID_CBX_NOPRJGRP,CreatePrjPnl::OnCbxNoPg)
    EVT_BUTTON(CTRL_CRTPNL_ID_BTN_DIR, CreatePrjPnl::OnBtnDir)
        EVT_TEXT(CTRL_CRTPNL_ID_TXT_PRJNAME, CreatePrjPnl::OnText)
        EVT_TEXT_ENTER(CTRL_CRTPNL_ID_TXT_PRJNAME, CreatePrjPnl::OnKey)
        EVT_TEXT(CTRL_CRTPNL_ID_COMB_PGNAME, CreatePrjPnl::OnText)
//        EVT_CHAR(CreatePrjPnl::OnKey)
END_EVENT_TABLE()


CreatePrjPnl::CreatePrjPnl(GenericDlg* parent, int pgoption,wxWindowID id) : GenericPnl(parent, id)
{
    m_pCbxNoPg=NULL;
        m_pTxtPName = NULL;
        m_strDir=wxEmptyString;
        m_pTxtFullPath = NULL;
        m_fullPath=wxEmptyString;
        m_pCombPG=NULL;
        m_pgOption=pgoption;
        CreateGUIControls();
        if(m_pTxtFullPath!=NULL)
        m_pTxtFullPath->SetEditable(false);
    this->SetFocus();
}
CreatePrjPnl::~CreatePrjPnl()
{
}
void CreatePrjPnl::CreateGUIControls(void)
{
    wxBoxSizer* bs = new wxBoxSizer(wxVERTICAL);
        wxString        pgName=wxEmptyString;
        ProjectGroup *pPG=Manager::Get()->GetProjectManager()->GetActiveProjectGroup();
        if(pPG)
                pgName=pPG->GetName();
    else
        pgName=wxEmptyString;

        wxStaticText* labele = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
        wxStaticText* labele2 = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
        wxStaticText* label=NULL;
        //wxButton* button=NULL;
    if(m_pgOption!=0){//new project
        wxBoxSizer* groupSizer = new wxBoxSizer(wxHORIZONTAL);
        label = new wxStaticText(this, wxID_ANY, wxT("Project Group:"), wxDefaultPosition, wxSize(200, 25));
        m_pCbxNoPg=new wxCheckBox(this,CTRL_CRTPNL_ID_CBX_NOPRJGRP,wxT("No project group."),wxDefaultPosition,wxSize(150,25));
        groupSizer->Add(label,0,wxEXPAND);
        groupSizer->Add(m_pCbxNoPg,0,wxEXPAND);
        labele = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
        bs->Add(labele, 0, wxEXPAND);
        bs->Add(groupSizer, 0, wxEXPAND);

        wxArrayString pgItems=Manager::Get()->GetProjectManager()->GetAllPGName();
        wxBoxSizer* gSPGName = new wxBoxSizer(wxHORIZONTAL);
        m_pCombPG = new wxComboBox(this, CTRL_CRTPNL_ID_COMB_PGNAME, pgName, wxDefaultPosition, wxSize(330, 25),pgItems,wxCB_DROPDOWN);
        m_pCombPG->SetFocus();
        gSPGName->Add(m_pCombPG, 1, wxEXPAND);
        labele = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
        gSPGName->Add(labele, 1,wxEXPAND);
        bs->Add(gSPGName, 0, wxEXPAND);
        SetMinSize(wxSize(350, 185));
    }else{
        SetMinSize(wxSize(350, 125));
    }
    wxBoxSizer *groupSizer2 = new wxBoxSizer(wxHORIZONTAL);
    label = new wxStaticText(this, wxID_ANY, wxT("Project Name:"), wxDefaultPosition, wxSize(200, 25));
    labele2 = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
    m_pTxtPName = new wxTextCtrl(this, CTRL_CRTPNL_ID_TXT_PRJNAME, wxEmptyString, wxDefaultPosition, wxSize(200, 25), wxTE_PROCESS_ENTER);
    groupSizer2->Add(m_pTxtPName, 1, wxEXPAND);
    groupSizer2->Add(labele2, 0, wxEXPAND);
    labele = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
    bs->Add(labele, 0, wxEXPAND);
    bs->Add(label, 0, wxEXPAND);
    bs->Add(groupSizer2, 0, wxEXPAND);

    //wxBoxSizer *prjtypeSizer=new wxBoxSizer(wxHORIZONTAL);
    wxString m_rbPrjTypeChoices[] = { wxT("Molcas"), wxT("QChem"), wxT("SimuCal"), wxT("TeraChem") };
    int m_rbPrjTypeNChoices = sizeof( m_rbPrjTypeChoices ) / sizeof( wxString );
    m_pRbxPrjType = new wxRadioBox( this, CTRL_RB_PROJECTTYPE, wxT("Project Type:"), wxDefaultPosition, wxDefaultSize, m_rbPrjTypeNChoices, m_rbPrjTypeChoices, 4, wxRA_SPECIFY_COLS );
    m_pRbxPrjType->SetSelection( 0);
    labele = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
    bs->Add(labele,0,wxEXPAND);
    bs->Add(m_pRbxPrjType,0,wxEXPAND);
    m_pRbxPrjType->Hide();  //comment this line to show project type option.

/*        wxBoxSizer* folderSizer = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* labele2 = new wxStaticText(this, wxID_ANY, wxT(" "), wxDefaultPosition, wxSize(45, 10));
        label = new wxStaticText(this, wxID_ANY, wxT("Path:"), wxDefaultPosition, wxSize(200, 25));
        m_pTxtFullPath = new wxTextCtrl(this,CTRL_CRTPNL_ID_TEXT_FILENAME, wxEmptyString, wxDefaultPosition, wxSize(200, 25));
         button = new wxButton(this, CTRL_CRTPNL_ID_BTN_DIR, wxT("..."), wxDefaultPosition, wxSize(45, 25));
        folderSizer->Add(m_pTxtFullPath, 1, wxEXPAND);
        folderSizer->Add(button, 0, wxEXPAND);
        bs->Add(labele2, 0, wxEXPAND);
        bs->Add(label, 0, wxEXPAND);
        bs->Add(folderSizer, 0, wxEXPAND);
*/
        SetSizer(bs);
        SetAutoLayout(true);

        if(pPG!=NULL){
                m_strDir=pPG->GetPath();
                m_strDir=m_strDir.BeforeLast(platform::PathSep().GetChar(0));
        }else
        m_strDir=EnvUtil::GetProjectDir();
    if(pPG!=NULL&&m_pgOption==0)//new project ,belong to project group, do not show project group option.
        m_strDir=pPG->GetPath();
//        m_pTxtFullPath->SetValue(m_strDir);
}
wxString        CreatePrjPnl::GetPrjName()const
{
    if(m_pTxtPName!=NULL){
                wxString tmp=m_pTxtPName->GetValue();
        return StringUtil::CheckString(tmp);
    }else
        return wxEmptyString;
}
wxString        CreatePrjPnl::GetFullPath()const
{
    return m_fullPath;
}

wxString        CreatePrjPnl::GetPGName()const
{
    if(m_pCombPG!=NULL&&!m_pCbxNoPg->IsChecked()){
                wxString tmp=m_pCombPG->GetValue();
        return StringUtil::CheckString(tmp);
    }else
        return wxEmptyString;
}
void  CreatePrjPnl::OnCbxNoPg(wxCommandEvent& event)
{
    if(m_pCbxNoPg==NULL)
        return ;
    if(m_pCbxNoPg->IsChecked())
    {        m_pgOption=2;
        m_pCombPG->Enable(false);
    }else{
                m_pgOption=1;
        m_pCombPG->Enable(true);
    }
}
void CreatePrjPnl::OnBtnDir(wxCommandEvent& event)
{
        wxString lastVisitDir = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetString("lastVisitDir");
        wxDirDialog dirDialog(this, wxEmptyString, lastVisitDir);
        if (dirDialog.ShowModal() == wxID_OK) {
                m_strDir=dirDialog.GetPath();
                OnText(event);
        }
}
void CreatePrjPnl::OnKey(wxCommandEvent& event)
{
//    if(event.GetKeyCode()==WXK_RETURN)
//        wxMessageBox(wxT("Return;"));
        OnButton(BTN_OK);
//        event.SetId(wxID_OK);
        ((GenericDlg*)m_pParent)->HideDialog(BTN_OK);
//        event.Skip();
//        event.Veto();
}
void CreatePrjPnl::OnText(wxCommandEvent& event)
{
    //.....Check for the Legitimacy of the input char.....//
    if(m_pCombPG!=NULL){
        wxString tmpPGName=m_pCombPG->GetValue();
        if(!tmpPGName.IsEmpty()){
            wxString    outString=StringUtil::CheckInput(tmpPGName);
            if(!outString.IsSameAs(tmpPGName))
            {
                wxBell();
                m_pCombPG->SetValue(outString);
            }
        }
    }
    if(m_pTxtPName!=NULL)
    {
        wxString tmpPrjName=m_pTxtPName->GetValue();
        if(!tmpPrjName.IsEmpty()){
            wxString    outString=StringUtil::CheckInput(tmpPrjName);
            if(!outString.IsSameAs(tmpPrjName))
            {
                wxBell();
                m_pTxtPName->SetValue(outString);
            }
        }
    }
    //..........................................................................//
/*        wxString fileName = wxEmptyString;
        ProjectGroup *pPG=Manager::Get()->GetProjectManager()->GetActiveProjectGroup();
         if(m_pgOption==0&&pPG!=NULL){
        m_strDir=pPG->GetPath();
        fileName=m_strDir;
   }else if(m_pgOption==1){
                 if(m_strDir.Last()!=platform::PathSep().GetChar(0))
                        fileName=m_strDir+platform::PathSep()+GetPGName();
                 else
                        fileName=m_strDir+GetPGName();
    }else
        fileName=m_strDir;
        if(fileName.Last()!=platform::PathSep().GetChar(0))
                fileName+=platform::PathSep();

        if(!fileName.IsEmpty()) {
//                fileName = FileUtil::NormalizeFileName(fileName);
        }

        wxString title = GetPrjName();//project name
        title=StringUtil::CheckString(title);
        if(!title.IsEmpty()) {
//            wxString    tmp=title.AfterLast(platform::PathSep().GetChar(0));
//            if(tmp.IsEmpty())
            fileName += title + platform::PathSep() + title + wxPRJ_FILE_SUFFIX;
 //       else
//           fileName += tmp + platform::PathSep() + tmp + wxPRJ_FILE_SUFFIX;
        }
        if(!fileName.IsEmpty()) {
//                fileName = FileUtil::NormalizeFileName(fileName);
        }
        m_pTxtFullPath->SetValue(fileName);
//wxMessageBox(fileName);*/
}

bool CreatePrjPnl::OnButton(long style) {
        if(style == BTN_OK) {
                if(!ValidateData()) {
                        return false;
                }
        }

        return true;
}

bool CreatePrjPnl::ValidateData(void) {
    if(m_pgOption==1&&m_pCbxNoPg->IsChecked()==false&&m_pCombPG->GetValue().IsEmpty()){
        wxMessageBox(wxT("Please enter the project group name if you want to use it."),wxT("Warning"));
        return false;
    }
    ProjectGroup *pPG=Manager::Get()->GetProjectManager()->GetActiveProjectGroup();
        wxString dir=wxEmptyString;
        if(m_pgOption==1)
        dir = m_strDir+platform::PathSep()+GetPGName();
    else
        dir=m_strDir;

        if(StringUtil::IsEmptyString(dir))
        {
                Tools::ShowError(wxT("Please enter the project directory."));
                return false;
        }
    dir = FileUtil::NormalizeFileName(dir);
//wxMessageBox(dir);
    wxString tmppgf=wxEmptyString;

        tmppgf=dir+platform::PathSep()+GetPGName()+wxPRJ_FILE_SUFFIX ;// to check if the specified folder contains a project
        if(pPG==NULL&&wxFileExists(tmppgf)){
//wxMessageBox(wxT("Warning"));
                wxMessageDialog dialog(this, wxT("The specified project group folder contains a project, do you want to continue?"),
                        dir, wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_INFORMATION);
            if(dialog.ShowModal() != wxID_YES) {
                      return false;
            }
        }

    if(pPG==NULL)
        tmppgf=dir+platform::PathSep()+GetPGName()+wxPG_FILE_SUFFIX ;
    else
        tmppgf=pPG->GetFullPathName();
    if(wxFileExists(tmppgf))
    {
        if(Manager::Get()->GetProjectManager()->IsOpen(tmppgf)==NULL){//the specified project group exists, but not opened.
            wxMessageDialog dialog(this,wxT("The project group exists, do you want to overwrite it?\n If not, you can enter another name or open the project\ngroup  before creating a new project."),tmppgf,wxYES_NO|wxICON_INFORMATION);
            if(dialog.ShowModal() == wxID_YES) {
                FileUtil::RemoveDirectory(dir, true);
            }else
                return false;
        }
    }
    wxString title = GetPrjName();
    title=title.AfterLast(platform::PathSep().GetChar(0));
    if(title.IsEmpty())
        title=m_pTxtPName->GetValue();
        if(StringUtil::IsEmptyString(title))
         {
                Tools::ShowError(wxT("Please enter the project title."));
                return false;
        }
        dir =dir+platform::PathSep()+ title;
//wxMessageBox(dir);
//        FileUtil::CreateDirectory(dir);
//wxMessageBox(m_strDir);
        wxString fileName;
        if(m_pgOption==0){
                fileName=m_strDir+platform::PathSep()+title+platform::PathSep()+title+ wxPRJ_FILE_SUFFIX;
        }else if(m_pgOption==1){
                fileName=m_strDir+platform::PathSep()+GetPGName()+platform::PathSep()+title+platform::PathSep()+title+ wxPRJ_FILE_SUFFIX;
        }else{
                fileName=m_strDir+platform::PathSep()+title+platform::PathSep()+title+ wxPRJ_FILE_SUFFIX;
        }
//wxMessageBox(fileName);
        m_fullPath=fileName;
        if((pPG==NULL)&&wxFileExists(fileName)) {
                if(Manager::Get()->GetProjectManager()->IsOpen(fileName)) {
                        Tools::ShowError(wxT("There is a project with the same name has been open."));
                        return false;
                }else {
                        wxMessageDialog dialog(this, wxT("The project exists, do you like to overwrite it? \nIf so, all files in the project will be removed."),
                        dir, wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_INFORMATION);
                        if(dialog.ShowModal() == wxID_YES) {
//wxMessageBox(dir);
                            FileUtil::RemoveDirectory(dir, true);
                                        FileUtil::CreateDirectory(dir);
                        }else {
                                    return false;
                        }
                }
        }else if((pPG==NULL)){// new project folder is same as exists project group folder
                fileName=fileName.Left(fileName.Len()-PRJ_FILE_SUFFIX_LEN)+wxPG_FILE_SUFFIX;
                if(wxFileExists(fileName)){
                        wxMessageDialog dialog(this, wxT("The specified project folder contains a project group, do you want to continue?"),
                        dir, wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_INFORMATION);
                if(dialog.ShowModal() != wxID_YES) {
                                    return false;
                }
                }
        }

        return true;
}

ProjectType CreatePrjPnl::GetPrjType(){
    ProjectType prjtype = PROJECT_UNKNOWN;
    switch(m_pRbxPrjType->GetSelection()) {
    case 0:
        prjtype = PROJECT_MOLCAS;
        break;
    case 1:
        prjtype = PROJECT_QCHEM;
        break;
    case 2:
        prjtype = PROJECT_SIMUCAL;
        break;
    case 3:
        prjtype = PROJECT_TERACHEM;
    default:
        prjtype = PROJECT_UNKNOWN;
    }
    return prjtype;
}
