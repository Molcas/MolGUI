/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "starthereview.h"

#include "appglobals.h"
#include "envutil.h"
#include "stringutil.h"
#include "resource.h"
#include "simuprjdef.h"

const wxString global_startHereTitle = wxT("Start here");

int CTRL_HTML_WIN = wxNewId();

class StartHtmlWin : public wxHtmlWindow {
    public:
        StartHtmlWin(StartHereView* parent, int id, const wxPoint& pos = wxDefaultPosition,
                                const wxSize& size = wxDefaultSize, long style = wxHW_SCROLLBAR_AUTO)
                                : wxHtmlWindow(parent, id, pos, size, style),
            m_pOwner(parent) {
        }

                void OnLinkClicked(const wxHtmlLinkInfo& link) {
                    if (m_pOwner) {
                        if (!m_pOwner->LinkClicked(link)) {
                                        wxLaunchDefaultBrowser(link.GetHref());
                        }
                    }
                }

    private:
        StartHereView* m_pOwner;
};

StartHereView::StartHereView(wxWindow* parent, wxEvtHandler* evtHandler) : AbstractView(parent, evtHandler, VIEW_START_HERE) {
        m_pHtmlWin = NULL;
        m_viewId=STARTHERE_PID;
        m_originalPageContent = wxEmptyString;
        SetTitle(global_startHereTitle);
        //begin : hurukun : 06/11/2009
    //SetFileName(EnvUtil::GetPrgRootDir() + platform::PathSep() + global_startHereTitle);
        SetFileName(global_startHereTitle);
        //end   : hurukun : 06/11/2009
        CreateGUI();
}

StartHereView::~StartHereView()
{
}

void StartHereView::CreateGUI(void)
{
        wxBoxSizer* bs = new wxBoxSizer(wxVERTICAL);
        m_pHtmlWin = new StartHtmlWin(this, CTRL_HTML_WIN);
        bs->Add(m_pHtmlWin, 1, wxEXPAND);
    SetSizer(bs);
    SetAutoLayout(true);

        wxFont systemFont = wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT);
        int sizes[7] = {0};
        for(int i = 0; i < 7; i++)
        sizes[i] = systemFont.GetPointSize();
        m_pHtmlWin->SetFonts(wxEmptyString, wxEmptyString, sizes);

}
void StartHereView::InitView(void)
{

        wxString filePath = EnvUtil::GetPrgResDir() + platform::PathSep() + wxT("res") + platform::PathSep() + wxT("prj") + platform::PathSep() + wxT("start_here.html");
 
         m_pHtmlWin->LoadPage(filePath);
         // Alternate way to read the file so we can perform some search and replace.
    // The images referenced in the default start page can be found now because we used LoadPage() above...
    wxString buf;
    wxFileSystem* fs = new wxFileSystem;
    wxFSFile* fsFile = fs->OpenFile(filePath);
    if (fsFile) {
            wxInputStream* is = fsFile->GetStream();
            char tmp[1024] = {0};
            while (!is->Eof() && is->CanRead()) {
                    memset(tmp, 0, sizeof(tmp));
                    is->Read(tmp, sizeof(tmp) - 1);
                    buf << StringUtil::CharPointer2String((const char*)tmp);
            }
        delete fsFile;
    }
    else
        buf = wxT("<HTML><BODY><H1>Welcome to ") + appglobals::appName + wxT("</H1><BR>The default start page seems to be missing...</BODY></HTML>");
    delete fs;

        wxString revInfo = wxT("Release ") + appglobals::appVersion + wxT(": Built on ") + appglobals::appBuildTimestamp;
    // perform var substitution
    buf.Replace(wxT("SIMU_VAR_REVISION_INFO"), revInfo);
    m_pHtmlWin->SetPage(buf);

    m_originalPageContent = buf; // keep a copy of original for Reload()
    Reload();

}

void StartHereView::SetPageContent(const wxString& buffer)
{
         m_pHtmlWin->SetPage(buffer);
}

bool StartHereView::LinkClicked(const wxHtmlLinkInfo& link)
{
        wxString href = link.GetHref();
        if (href.StartsWith(_T("SIMU_CMD_"))) {
        wxCommandEvent evt(wxEVT_COMMAND_MENU_SELECTED, MENU_ID_STARTHERE_LINK);
        evt.SetString(link.GetHref());
        wxPostEvent(m_pEvtHandler, evt);
        return true;
    }

        return false;
}

void StartHereView::Reload(void)
{
    if (m_pEvtHandler) {
        wxCommandEvent evt(wxEVT_COMMAND_MENU_SELECTED, MENU_ID_STARTHERE_VAR_SUBSTITUTION);
        evt.SetString(m_originalPageContent);
        m_pEvtHandler->ProcessEvent(evt); // direct call
    }
}
