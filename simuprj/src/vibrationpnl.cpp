/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/tokenzr.h>

#include "vibrationpnl.h"
#include "stringutil.h"
#include "tools.h"

//////////////////////////////////////////////////////////////////////
// wxGridCellCheckEditor Class
//////////////////////////////////////////////////////////////////////
//IMPLEMENT_DYNAMIC_CLASS(wxGridCellCheckEditor, wxGridCellEditor);
/////////////////////////////////////////////////////////////////
const wxString wxGRID_VALUE_ITEM = wxT("VIBVALUEITEM");
wxString wxGridCellCheckEditor::ms_stringValues[2] = { wxT("FALSE"), wxT("TRUE") };
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

wxGridCellCheckEditor::wxGridCellCheckEditor()
{
    m_startValue = NULL;
}

wxGridCellCheckEditor::~wxGridCellCheckEditor()
{
    if(m_startValue)
        delete m_startValue;
}
void wxGridCellCheckEditor::Create(wxWindow *parent, wxWindowID id, wxEvtHandler *evtHandler)
{
    wxString label = wxEmptyString;
    if(m_startValue != NULL)
        label = m_startValue->display;
    //wxMessageBox(wxT("Create:\nlabel=")+label);
    m_control = new wxCheckBox(parent, id, label,
                               wxDefaultPosition, wxDefaultSize,
                               wxNO_BORDER);

    wxGridCellEditor::Create(parent, id, evtHandler);
    //if(IsCreated())
    //{
    //        wxMessageBox(wxT("in wxGridCellCheckEditor::Create create"));
    //}
}

wxGridCellCheckEditor::ValueItem *wxGridCellCheckEditor::ParseString(wxString value)
{
    wxArrayString strArr = wxStringTokenize(value, wxT(" #\t\n\r"), wxTOKEN_STRTOK);
    wxASSERT_MSG(strArr.GetCount() >= 1, wxT("Input value is not correct!"));
    ValueItem *item = new ValueItem;
    item->check = (strArr[0].MakeUpper().Cmp(ms_stringValues[1]) == 0);
    item->display = value.Right(value.Len() - value.Find(wxT("#")) - 1);
    return item;
}


void wxGridCellCheckEditor::BeginEdit(int row, int col, wxGrid *grid)
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    //wxMessageBox(wxT("BeginEdit"));
    if (grid->GetTable()->CanGetValueAs(row, col, wxGRID_VALUE_ITEM))
    {
        if(m_startValue == NULL)
        {
            m_startValue = (ValueItem *)grid->GetTable()->GetValueAsCustom(row, col, wxGRID_VALUE_ITEM);
        }
        else
        {
            ValueItem *item = (ValueItem *)grid->GetTable()->GetValueAsCustom(row, col, wxGRID_VALUE_ITEM);
            m_startValue->check = item->check;
            m_startValue->display = item->display;
            delete item;
        }
    }
    else
    {
        if(m_startValue == NULL)
        {
            m_startValue = ParseString( grid->GetTable()->GetValue(row, col) );
        }
        else
        {
            ValueItem *item = ParseString( grid->GetTable()->GetValue(row, col) );
            m_startValue->check = item->check;
            m_startValue->display = item->display;
            delete item;
        }
    }
    wxASSERT_MSG(m_startValue, wxT("The wxGridCellCheckEditor::ValueItem must be created first!"));

    CheckBox()->SetLabel(m_startValue->display);
    CheckBox()->SetValue(m_startValue->check);
    CheckBox()->SetFocus();
}

bool wxGridCellCheckEditor::EndEdit(int row, int col, wxGrid *grid)
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    wxASSERT_MSG(m_startValue, wxT("The wxGridCellEditor must be created first!"));

    bool changed = false;
    bool value = CheckBox()->GetValue();
    wxString label = CheckBox()->GetLabel();
    if ( value != m_startValue->check || label.Cmp(m_startValue->display) != 0 )
        changed = true;

    if ( changed )
    {
        ValueItem item;
        item.check = value;
        item.display = label;
        wxGridTableBase *const table = grid->GetTable();
        if ( table->CanGetValueAs(row, col, wxGRID_VALUE_ITEM) )
            table->SetValueAsCustom(row, col, wxGRID_VALUE_ITEM, &item);
        else
            table->SetValue(row, col, GetValue());
    }

    return changed;
}

bool wxGridCellCheckEditor::EndEdit(int row, int col, const wxGrid *grid, const wxString &oldval, wxString *newval)
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    wxASSERT_MSG(m_startValue, wxT("The wxGridCellEditor must be created first!"));

    bool changed = false;
    bool value = CheckBox()->GetValue();
    wxString label = CheckBox()->GetLabel();
    if ( value != m_startValue->check || label.Cmp(m_startValue->display) != 0 )
        changed = true;

    if ( changed )
    {
        ValueItem item;
        item.check = value;
        item.display = label;
        wxGridTableBase *const table = grid->GetTable();
        if ( table->CanGetValueAs(row, col, wxGRID_VALUE_ITEM) )
            table->SetValueAsCustom(row, col, wxGRID_VALUE_ITEM, &item);
        else
            table->SetValue(row, col, GetValue());
    }

    return changed;
}

void wxGridCellCheckEditor::ApplyEdit(int row, int col, wxGrid *grid)
{

} 

void wxGridCellCheckEditor::Reset()
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    wxASSERT_MSG(m_startValue, wxT("The wxGridCellEditor must be created first!"));
    CheckBox()->SetValue(m_startValue->check);
    CheckBox()->SetLabel(m_startValue->display);
}

wxGridCellEditor *wxGridCellCheckEditor::Clone() const
{
    wxGridCellCheckEditor *editor = new wxGridCellCheckEditor;
    editor->m_startValue = new ValueItem;
    editor->m_startValue->check = m_startValue->check;
    editor->m_startValue->display = m_startValue->display;
    return editor;
}

wxString wxGridCellCheckEditor::GetValue() const
{
    return ms_stringValues[CheckBox()->GetValue()] + wxT("#") + CheckBox()->GetLabel();
}

void wxGridCellCheckEditor::SetSize(const wxRect &rect)
{
    bool resize = false;
    wxSize size = m_control->GetSize();
    // check if the checkbox is not too big/small for this cell
    wxSize sizeBest = m_control->GetBestSize();
    if ( !(size == sizeBest) )
    {
        // reset to default size if it had been made smaller
        size = sizeBest;

        resize = true;
    }

    if ( resize )
    {
        m_control->SetSize(size);
    }

    // position it in the centre of the rectangle (TODO: support alignment?)

#if defined(__WXGTK__) || defined (__WXMOTIF__)
    // the checkbox without label still has some space to the right in wxGTK,
    // so shift it to the right
    size.x -= 8;
#elif defined(__WXMSW__)
    // here too, but in other way
    size.x += 1;
    size.y -= 2;
#endif

    int hAlign = wxALIGN_CENTRE;
    int vAlign = wxALIGN_CENTRE;
    if (GetCellAttr())
        GetCellAttr()->GetAlignment(& hAlign, & vAlign);

    int x = 0, y = 0;
    if (hAlign == wxALIGN_LEFT)
    {
        x = rect.x + 2;

#ifdef __WXMSW__
        x += 2;
#endif

        y = rect.y + rect.height / 2 - size.y / 2;
    }
    else if (hAlign == wxALIGN_RIGHT)
    {
        x = rect.x + rect.width - size.x - 2;
        y = rect.y + rect.height / 2 - size.y / 2;
    }
    else if (hAlign == wxALIGN_CENTRE)
    {
        x = rect.x + rect.width / 2 - size.x / 2;
        y = rect.y + rect.height / 2 - size.y / 2;
    }

    m_control->Move(x, y);
}
void wxGridCellCheckEditor::Show(bool show, wxGridCellAttr *attr)
{
    m_control->Show(show);

    if ( show )
    {
        wxColour colBg = attr ? attr->GetBackgroundColour() : *wxLIGHT_GREY;
        CheckBox()->SetBackgroundColour(colBg);
    }
}

bool wxGridCellCheckEditor::IsAcceptedKey(wxKeyEvent &event)
{
    if ( wxGridCellEditor::IsAcceptedKey(event) )
    {
        int keycode = event.GetKeyCode();
        switch ( keycode )
        {
        case WXK_SPACE:
        case '+':
        case '-':
            return true;
        }
    }

    return false;
}

void wxGridCellCheckEditor::StartingClick()
{
    CheckBox()->SetValue(!CheckBox()->GetValue());
}

void wxGridCellCheckEditor::StartingKey(wxKeyEvent &event)
{
    int keycode = event.GetKeyCode();
    switch ( keycode )
    {
    case WXK_SPACE:
        CheckBox()->SetValue(!CheckBox()->GetValue());
        break;

    case '+':
        CheckBox()->SetValue(true);
        break;

    case '-':
        CheckBox()->SetValue(false);
        break;
    }
}

wxCheckBox *wxGridCellCheckEditor::CheckBox() const
{
    return (wxCheckBox *)m_control;
}

bool wxGridCellCheckEditor::IsChecked()
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    return CheckBox()->GetValue();
}

void wxGridCellCheckEditor::SetStartValue(bool checked, wxString dis)
{
    //wxASSERT_MSG(m_control,wxT("The wxGridCellEditor must be created first!"));
    if(!m_startValue)
        m_startValue = new ValueItem;
    m_startValue->check = checked;
    m_startValue->display = dis;
    if(IsCreated())
    {
        wxMessageBox(wxT("SetLabel"));
        CheckBox()->SetValue(m_startValue->check);
        CheckBox()->SetLabel(m_startValue->display);
    }
}

wxString wxGridCellCheckEditor::GetDisplayValue()
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    return CheckBox()->GetLabel();
}

void wxGridCellCheckEditor::SetChecked(bool checked)
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    CheckBox()->SetValue(checked);
}

void wxGridCellCheckEditor::SetDisplayValue(wxString dis)
{
    wxASSERT_MSG(m_control, wxT("The wxGridCellEditor must be created first!"));
    CheckBox()->SetLabel(dis);
}

void wxGridCellCheckEditor::UseStringValues(const wxString &valueTrue, const wxString &valueFalse)
{
    ms_stringValues[false] = valueFalse;
    ms_stringValues[true] = valueTrue;
}
bool wxGridCellCheckEditor::IsTrueValue(const wxString &value)
{
    return value == ms_stringValues[true];
}
//////////////////////////////////////////////////////////////////////
// wxGridCellCheckRenderer Class
//////////////////////////////////////////////////////////////////////
// between checkmark and box
static const wxCoord wxGRID_CHECKMARK_MARGIN = 2;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

wxGridCellCheckRenderer::wxGridCellCheckRenderer()
{

}

wxGridCellCheckRenderer::~wxGridCellCheckRenderer()
{

}

void wxGridCellCheckRenderer::Draw(wxGrid &grid, wxGridCellAttr &attr, wxDC &dc, const wxRect &rect, int row, int col, bool isSelected)
{
    wxGridCellRenderer::Draw(grid, attr, dc, rect, row, col, isSelected);
    //get the label and the value of the cell
    wxString value = grid.GetCellValue(row, col);
    wxGridCellCheckEditor::ValueItem *item = wxGridCellCheckEditor::ParseString(value);
    wxString label = item->display;
    if(StringUtil::IsEmptyString(label))
        wxMessageBox(wxT("In wxGridCellCheckRenderer::Draw label is Empty"));
    bool check = item->check;
    delete item;
    // draw a check mark in the centre (ignoring alignment - TODO)
    wxSize size = GetBestSize(grid, attr, dc, row, col);
    dc.SetFont(attr.GetFont());
    wxSize labelSize = dc.GetTextExtent(label);
    labelSize.x += 2;
    labelSize.y += 2;
    // don't draw outside the cell
    wxCheckBox *checkbox = new wxCheckBox(&grid, wxID_ANY, wxEmptyString);
    wxSize markSize = checkbox->GetBestSize();
    delete checkbox;
    // draw a border around checkmark
    int vAlign, hAlign;
    attr.GetAlignment(&hAlign, &vAlign);

    wxRect rectBorder, rectLabel;
    if (hAlign == wxALIGN_CENTRE)
    {
        rectBorder.x = rect.x + rect.width / 2 - size.x / 2;
        rectBorder.y = rect.y + rect.height / 2 - size.y / 2;
        rectBorder.width = markSize.x;
        rectBorder.height = markSize.y;
        rectLabel.x = rectBorder.x + rectBorder.width + 2;
        rectLabel.y = rectBorder.y;
        rectLabel.width = labelSize.x;
        rectLabel.height = labelSize.y;
    }
    else if (hAlign == wxALIGN_LEFT)
    {
        rectBorder.x = rect.x + 2;
        rectBorder.y = rect.y + rect.height / 2 - size.y / 2;
        rectBorder.width = markSize.x;
        rectBorder.height = markSize.y;
        rectLabel.x = rectBorder.x + rectBorder.width + 2;
        rectLabel.y = rectBorder.y;
        rectLabel.width = labelSize.x;
        rectLabel.height = labelSize.y;
    }
    else if (hAlign == wxALIGN_RIGHT)
    {
        rectBorder.x = rect.x + rect.width - markSize.x - 2;
        rectBorder.y = rect.y + rect.height / 2 - size.y / 2;
        rectBorder.width = markSize.x;
        rectBorder.height = markSize.y;
        rectLabel.x = rect.x + rect.width - markSize.x - 4 - labelSize.x;
        rectLabel.y = rectBorder.y;
        rectLabel.width = labelSize.x;
        rectLabel.height = labelSize.y;
    }
    if ( check )
    {
        wxRect rectMark = rectBorder;

#ifdef __WXMSW__
        // MSW DrawCheckMark() is weird (and should probably be changed...)
        rectMark.Inflate(-wxGRID_CHECKMARK_MARGIN / 2);
        rectMark.x++;
        rectMark.y++;
#else
        rectMark.Inflate(-wxGRID_CHECKMARK_MARGIN);
#endif

        dc.SetTextForeground(attr.GetTextColour());
        dc.DrawCheckMark(rectMark);
    }

    dc.SetBrush(*wxTRANSPARENT_BRUSH);
    dc.SetPen(wxPen(attr.GetTextColour(), 1, wxPENSTYLE_SOLID));
    dc.DrawRectangle(rectBorder);
    if(isSelected)
        dc.DrawRectangle(rect);
    // now we only have to draw the text
    SetTextColoursAndFont(grid, attr, dc, isSelected);
    //grid.DrawTextRectangle(dc, label,rectLabel, hAlign, vAlign);
    if(StringUtil::IsEmptyString(label))
        wxMessageBox(wxT("draw nothing"));
    dc.DrawLabel(label, rectLabel, hAlign | vAlign);
    //dc.DrawLabel(label,rect, hAlign| vAlign);
}
wxSize wxGridCellCheckRenderer::GetBestSize(wxGrid &grid, wxGridCellAttr &attr, wxDC &dc, int row, int col)
{
    // compute it only once (no locks for MT safeness in GUI thread...)
    if ( !m_sizeCheckMark.x )
    {
        wxString value = grid.GetCellValue(row, col);
        wxGridCellCheckEditor::ValueItem *item = wxGridCellCheckEditor::ParseString(value);
        // get checkbox size
        wxCheckBox *checkbox = new wxCheckBox(&grid, wxID_ANY, item->display);
        delete item;
        wxSize size = checkbox->GetBestSize();
        wxCoord checkSize = size.y + 2 * wxGRID_CHECKMARK_MARGIN;

        // FIXME wxGTK::wxCheckBox::GetBestSize() gives "wrong" result
#if defined(__WXGTK__) || defined(__WXMOTIF__)
        checkSize -= size.y / 2;
#endif

        delete checkbox;

        m_sizeCheckMark.x = size.x + 2 * wxGRID_CHECKMARK_MARGIN;
        m_sizeCheckMark.y = checkSize;
    }

    return m_sizeCheckMark;
}

wxGridCellRenderer *wxGridCellCheckRenderer::Clone() const
{
    return new wxGridCellCheckRenderer;
}
void wxGridCellCheckRenderer::SetTextColoursAndFont(const wxGrid &grid, const wxGridCellAttr &attr, wxDC &dc, bool isSelected)
{
    dc.SetBackgroundMode( wxTRANSPARENT );

    // TODO some special colours for attr.IsReadOnly() case?

    // different coloured text when the grid is disabled
    if ( grid.IsEnabled() )
    {
        if ( isSelected )
        {
            dc.SetTextBackground( grid.GetSelectionBackground() );
            dc.SetTextForeground( grid.GetSelectionForeground() );
        }
        else
        {
            dc.SetTextBackground( attr.GetBackgroundColour() );
            dc.SetTextForeground( attr.GetTextColour() );
        }
    }
    else
    {
        dc.SetTextBackground(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
        dc.SetTextForeground(wxSystemSettings::GetColour(wxSYS_COLOUR_GRAYTEXT));
    }

    dc.SetFont( attr.GetFont() );
}
//////////////////////////////////////////////////////////////////////
//VibrationPnl
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
static const wxString VibGridHeaders[Col_MAX] = {
    wxT("Frequency"), wxT("Symmetry"), wxT("Intensity")
};
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(VibrationPnl, wxPanel)
    EVT_ACTIVATE(VibrationPnl::OnActivate)
    EVT_CLOSE(VibrationPnl::OnClose)
    EVT_GRID_CELL_LEFT_CLICK(VibrationPnl::OnCellLeftClick)
    EVT_SIZE(VibrationPnl::OnSize)
    EVT_SLIDER(wxID_ANY, VibrationPnl::OnSlider)
END_EVENT_TABLE()
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VibrationPnl::VibrationPnl(wxWindow *parent, wxWindowID id, const wxPoint &pos,
                           const wxSize &size, long style, const wxString &name ) : wxPanel(parent, id, pos, size, style, name)
{
    CreateGUIControls();
    m_selectedRow = -1;
    m_firstclick = true;
}

VibrationPnl::~VibrationPnl()
{
    if(pGrid != NULL)
        delete pGrid;
    pGrid = NULL;
}
wxGrid *VibrationPnl::CreateGrid(wxWindow *parent, wxWindowID id)
{
    wxGrid *grid = new wxGrid(parent, id, wxDefaultPosition);

    grid->CreateGrid(0, Col_MAX);
    grid->RegisterDataType(wxGRID_VALUE_ITEM, new wxGridCellCheckRenderer, new wxGridCellCheckEditor);

    wxGridCellAttr *attrFrequency = new wxGridCellAttr,
    *attrSymmetry = new wxGridCellAttr ,
    *attrIntensity = new wxGridCellAttr;

    attrFrequency->SetReadOnly();
    attrFrequency->SetEditor(new wxGridCellCheckEditor);
    attrFrequency->SetRenderer(new wxGridCellCheckRenderer);

    attrSymmetry->SetReadOnly();
    attrIntensity->SetReadOnly();


    for(int i = 0; i < Col_MAX; i++)
        grid->SetColLabelValue(i, VibGridHeaders[i]);

    grid->SetColAttr(Col_Frequency, attrFrequency);
    grid->SetColAttr(Col_Symmetry, attrSymmetry);
    grid->SetColAttr(Col_Intensity, attrIntensity);
    grid->SetColFormatCustom(Col_Frequency, wxGRID_VALUE_ITEM);
    return grid;
}
void VibrationPnl::AddVibDisplayData(const IRDataArray &disArray)
{
    int rowCount = pGrid->GetNumberRows();
    if(rowCount > 0)
        pGrid->DeleteRows(0, rowCount);
    data.Clear();
    m_selectedRow = -1;
    data = disArray;
    for(unsigned i = 0; i < data.GetCount(); i++)
    {
        AppendVibRow(data[i]);
    }
    pGrid->Fit();
    pScrWin->FitInside();
    Refresh();
}

void VibrationPnl::CreateGUIControls()
{
    wxBoxSizer *bsTop = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(bsTop);
    pScrWin = new wxScrolledWindow(
        this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
        wxSUNKEN_BORDER | wxScrolledWindowStyle);

    pScrWin->SetScrollRate( 5, 5 );

    pGrid = CreateGrid(pScrWin, wxID_ANY);

    pGrid->Fit();
    pScrWin->SetClientSize(pGrid->GetSize());

    wxFlexGridSizer *fgsCon = new wxFlexGridSizer(2, 2, 0, 0);
    fgsCon->AddGrowableCol(1);
    wxStaticText *stCon = new wxStaticText(this, wxID_ANY, wxT("Frames/Cycle:"),
                                           wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
    pSliderCycle = new wxSlider(this, wxID_ANY, 50, 10, 300, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL | wxSL_AUTOTICKS);
    pSliderCycle->SetTickFreq(20);
    wxStaticText *stExcursion = new wxStaticText(this, wxID_ANY, wxT("Excursion:"),
            wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
    pSliderExcursion = new wxSlider(this, wxID_ANY, 1, 0, 10, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL | wxSL_AUTOTICKS);
    pSliderExcursion->SetTickFreq(1);
    fgsCon->Add(stCon, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, 5);
    fgsCon->Add(pSliderCycle, 1, wxALIGN_CENTER | wxALL, 5);
    fgsCon->Add(stExcursion, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, 5);
    fgsCon->Add(pSliderExcursion, 1, wxALIGN_CENTER | wxALL, 5);


    bsTop->Add(pScrWin, 1, wxEXPAND | wxALL, 5);
    bsTop->Add(fgsCon, 0, wxEXPAND | wxALL, 5);
    Layout();

}


void VibrationPnl::AppendVibRow(const IRData &item)
{
    int rowCount = pGrid->GetNumberRows();
    pGrid->AppendRows(1);
    pGrid->SetCellValue(rowCount, Col_Frequency, wxString::Format(wxT("FALSE#%.2f"), item.frequency));
    pGrid->SetCellValue(rowCount, Col_Symmetry, item.irtype);
    pGrid->SetCellValue(rowCount, Col_Intensity, wxString::Format(wxT("%.2f"), item.intensity));
}

void VibrationPnl::OnCellLeftClick(wxGridEvent &event)
{
    int row = event.GetRow(),
        col = event.GetCol();
    //   int    x=0,y=0;
    //   pScrWin->GetViewStart(&x,&y);

    if(col != Col_Frequency)
    {
        event.Skip();
        return;
    }
    for(int i = 0; i < pGrid->GetNumberRows(); i++)
    {
        if(i != row)
        {
            pGrid->SetCellValue(i, Col_Frequency, wxString::Format(wxT("FALSE#%.2f"), data[i].frequency));
        }
        else
        {
            wxGridCellCheckEditor::ValueItem *item = wxGridCellCheckEditor::ParseString(pGrid->GetCellValue(row, Col_Frequency));
            bool check = item->check;
            delete item;
            if(check)
            {
                pGrid->SetCellValue(row, Col_Frequency, wxString::Format(wxT("FALSE#%.2f"), data[i].frequency));
                DoVibration();
            }
            else
            {
                pGrid->SetCellValue(row, Col_Frequency, wxString::Format(wxT("TRUE#%.2f"), data[i].frequency));
                DoVibration(row);
            }
        }
    }
    if(m_firstclick) {
        pScrWin->Scroll(0, 0);
        m_firstclick = false;
    }
    event.Skip();
}




void VibrationPnl::DoVibration(int index)
{
    //        wxMessageBox(wxString::Format(wxT("row=%d"),index));
    m_selectedRow = index;
}

int VibrationPnl::GetSelectedRow() const
{
    return m_selectedRow;
}

void VibrationPnl::OnActivate(wxActivateEvent &event)
{
    wxMessageBox(wxT("OnActivate"));
}

void VibrationPnl::OnClose(wxCloseEvent &event)
{
    wxMessageBox(wxT("OnClose"));
    event.Skip();
}

void VibrationPnl::ClearData()
{
    int rowCount = pGrid->GetNumberRows();
    if(rowCount > 0)
        pGrid->DeleteRows(0, rowCount);
    m_selectedRow = -1;
}

void VibrationPnl::OnSize(wxSizeEvent &event)
{
    //pGrid->Fit();
    pScrWin->SetClientSize(pGrid->GetSize());
    pScrWin->FitInside();
    Layout();
    Refresh();
}

void VibrationPnl::OnSlider(wxCommandEvent &event)
{
    //wxMessageBox(wxString::Format(wxT("%d"),pSliderExcursion->GetValue()));
    ////((SimuGuiFrm*)GetParent())->SetVibrationExcursionCycle(pSliderExcursion->GetValue(),pSliderCycle->GetValue());
}

void VibrationPnl::OnTimer(MolView *view, AbstractProjectFiles *pFiles) {
    static bool irflag = false;
    IRDataArray m_irdataarray;
    IRData oldirdata;
    AtomBondArray m_atomarray;
    IRDataArray m_deltaarray;
    int row;
    unsigned int i = 0, m_xyz = 1;
    float enhanceswing;
    static int oldrow;
    static int oldenhanceswing;
    row = GetSelectedRow();
    enhanceswing = pSliderExcursion->GetValue();
    pFiles->GetIRDatas()->SetEnhanceSwing(enhanceswing);
    oldirdata = pFiles->GetIRDatas()->GetOldCoord();
    if (row == -1 || oldrow != row || oldenhanceswing != enhanceswing) {
        oldrow = row;
        oldenhanceswing = (int)enhanceswing;
        view->GetData()->GetActiveCtrlInst()->GetRenderingData()->SetAtomArray(oldirdata.irArray, 3, 1);
        view->RefreshGraphics();
        return;
    }
    m_irdataarray = pFiles->GetIRDatas()->GetIRDataArray();
    m_atomarray = view->GetData()->GetActiveCtrlInst()->GetRenderingData()->GetAtomBondArray();
    m_deltaarray = pFiles->GetIRDatas()->GetDeltaArray();

    unsigned int irlen;
    irlen = m_deltaarray[row].irArray.Count() - 1;
    while(m_deltaarray[row].irArray[i].x == 0 && i < irlen)i++;
    if(i == irlen && m_deltaarray[row].irArray[i].x == 0) {
        i = 0;
        m_xyz = 2;
        while(m_deltaarray[row].irArray[i].y == 0  && i < irlen)i++;
    }
    if(i == irlen && m_deltaarray[row].irArray[i].y == 0) {
        i = 0;
        m_xyz = 3;
        while(m_deltaarray[row].irArray[i].z == 0  && i < irlen)i++;
    }
    if(i > irlen && m_deltaarray[row].irArray[i].z == 0) {
        m_xyz = 0;
        return;
    }

    float x1 = 0, x2 = 0, x3 = 0;
    if (irflag)        {
        switch(m_xyz) {
        case 1:
            x1 = oldirdata.irArray[i].x;
            x2 = m_deltaarray[row].irArray[i].x * enhanceswing;
            x3 = m_atomarray[i].atom.pos.x;
            break;

        case 2:
            x1 = oldirdata.irArray[i].y;
            x2 = m_deltaarray[row].irArray[i].y * enhanceswing;
            x3 = m_atomarray[i].atom.pos.y;
            break;

        case 3:
            x1 = oldirdata.irArray[i].z;
            x2 = m_deltaarray[row].irArray[i].z * enhanceswing;
            x3 = m_atomarray[i].atom.pos.z;

        }
        if (x2 > 0) {
            if((x3 + x2) > (x1 + x2 * 10)) {
                irflag = !irflag;
                return;
            }
        } else {
            if((x3 + x2) < (x1 + x2 * 10)) {
                irflag = !irflag;
                return;
            }
        }
        view->GetData()->GetActiveCtrlInst()->GetRenderingData()->SetAtomArray(m_deltaarray[row].irArray, 1, enhanceswing);
    }
    else {

        //                GetUIData()->GetActiveCtrlInst()->GetRenderingData()->SetAtomArray(oldirdata.irArray,3,1);
        switch(m_xyz) {
        case 1:
            x1 = oldirdata.irArray[i].x;
            x2 = m_deltaarray[row].irArray[i].x * enhanceswing;
            x3 = m_atomarray[i].atom.pos.x;
            break;

        case 2:
            x1 = oldirdata.irArray[i].y;
            x2 = m_deltaarray[row].irArray[i].y * enhanceswing;
            x3 = m_atomarray[i].atom.pos.y;
            break;

        case 3:
            x1 = oldirdata.irArray[i].z;
            x2 = m_deltaarray[row].irArray[i].z * enhanceswing;
            x3 = m_atomarray[i].atom.pos.z;

        }
        if (x2 > 0) {
            if((x3 - x2) <= (x1)) {
                irflag = !irflag;
                return;
            }
        } else {
            if((x3 - x2) >= (x1)) {
                irflag = !irflag;
                return;
            }
        }
        view->GetData()->GetActiveCtrlInst()->GetRenderingData()->SetAtomArray(m_deltaarray[row].irArray, 2, enhanceswing);

    }
    view->RefreshGraphics();

}

int VibrationPnl::GetVibCyle()
{
    return pSliderCycle->GetValue();
}
