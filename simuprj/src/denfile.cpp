/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/datstrm.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>

#include "denfile.h"
#include "xyzfile.h"
#include "tools.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
const wxString DEN_ATOM_NUM_FLAG=wxT("Natom");
const wxString DEN_GRID_NUM_FLAG=wxT("N_of_Grids");
const wxString DEN_GRID_INFO_FLAG=wxT("GridName");
const wxString DEN_DELIM=wxT("=\r\n");

//search the bond list
bool DenFile::SearchBondList(int atom1Id, int atom2Id, const int *bondList)
{
        return XYZFile::SearchBondList(atom1Id,atom2Id,bondList);
}
//calculate bond type between two elements
BondType DenFile::CalculateBondType(int atom1Index, int atom2Index, AtomBondArray &atomBondArray)
{
        return XYZFile::CalculateBondType(atom1Index,atom2Index,atomBondArray);
}
//Load density file
/*
strLoadedFileName[in]: density file to load
appendChemArray[in,out]:the atombondarray to construct
atomLabels[in,out]: the number of elments
*/
void DenFile::LoadFile(const wxString &strLoadedFileName, AtomBondArray &appendChemArray, int *atomLabels)
{
        if(IsAsciiFile(strLoadedFileName))
                LoadAsciiFile(strLoadedFileName,appendChemArray,atomLabels);
        else
                LoadBinaryFile(strLoadedFileName,appendChemArray,atomLabels);
}
// get the element id by the symbol
ElemId DenFile::GetElemId(wxString atomSymbol)
{
    int i = 0;
    for(i = 0; i < ELEM_COUNT; i++) {
        if(atomSymbol.First(ELEM_NAMES[i]) == 0) {
            return (ElemId)(i + 1);
        }
    }
    return ELEM_NULL;
}
/*
load the grid information in the density file
strLoadedFileName[in]: the file name of density file
gridInfoArray[out]: the grid information to get from the density file
*/
void DenFile::LoadGridInfo(const wxString &strLoadedFileName, wxArrayString &gridInfoArray)
{
        if(IsAsciiFile(strLoadedFileName))
                LoadAsciiGridInfo(strLoadedFileName,gridInfoArray);
        else
                LoadBinaryGridInfo(strLoadedFileName,gridInfoArray);
}
/*
test the file format, ascii return true
*/
bool DenFile::IsAsciiFile(const wxString fileName)
{
        wxFileInputStream fis(fileName);
        wxDataInputStream tis(fis);
        wxUint8 flag;
        tis>>flag;
        //wxMessageBox(wxString::Format(wxT("%d"),flag));
        if(flag==48)
                return true;
        else
                return false;
}

void DenFile::LoadAsciiFile(const wxString &strLoadedFileName, AtomBondArray &appendChemArray, int *&atomLabels)
{
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
    ElemInfo* eleInfoOption = NULL;
        int i = 0, atom2Index;
        BondType bondType = BOND_TYPE_1;
        wxString atomSymbol = wxEmptyString;
    ElemId elemId;
    double x, y, z;
    long atomNumber;

        int atomCountBase = appendChemArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;
        strLine = tis.ReadLine();
        //wxMessageBox(strLine);
        while(strLine.First(DEN_ATOM_NUM_FLAG)!=0)
        {
                strLine = tis.ReadLine();
        //wxMessageBox(strLine);
        }
        wxString flagStr;
    wxStringTokenizer token1(strLine,DEN_DELIM);
        flagStr=token1.GetNextToken();
        //wxMessageBox(wxT("Flag=")+flagStr);
    token1.GetNextToken().ToLong(&atomNumber); // the number of atoms

    for(i = 0; i < atomNumber; i++) {
        strLine = tis.ReadLine();       // read one line
                //wxMessageBox(strLine);
        wxStringTokenizer tzk(strLine);
        AtomBond atomBond = AtomBond();
        atomSymbol = tzk.GetNextToken();
        elemId = GetElemId(atomSymbol);
        tzk.GetNextToken().ToDouble(&x);
        tzk.GetNextToken().ToDouble(&y);
        tzk.GetNextToken().ToDouble(&z);
        atomBond.atom.elemId = elemId;
                if(atomBond.atom.elemId == ELEM_H) {
                        atomBond.atom.elemId = DEFAULT_HYDROGEN_ID;
                }
        atomBond.atom.pos.x = x;
        atomBond.atom.pos.y = y;
        atomBond.atom.pos.z = z;

        eleInfoOption =GetElemInfo((ElemId)atomBond.atom.elemId);

        atomBond.atom.SetDispRadius(eleInfoOption->dispRadius);
        atomBond.atom.label = ++atomLabels[ELEM_PREFIX + atomBond.atom.elemId];

        appendChemArray.Add(atomBond);
    }
        //wxMessageBox(wxT("begin create bond type"));
    //create bond type
    for(i = atomCountBase; i < atomCountBase + atomNumber; i++) {

            for(atom2Index = i + 1; atom2Index < atomCountBase + atomNumber; atom2Index++) {
            bondType = CalculateBondType(i, atom2Index, appendChemArray);

            if (bondType == BOND_TYPE_0) {
            }else if (bondType == BOND_TYPE_1 || bondType == BOND_TYPE_2 || bondType == BOND_TYPE_3 || bondType == BOND_TYPE_4) {
                appendChemArray[i].bonds[appendChemArray[i].bondCount].atomIndex = atom2Index;
                appendChemArray[i].bonds[appendChemArray[i].bondCount].bondType = bondType;

                appendChemArray[atom2Index].bonds[appendChemArray[atom2Index].bondCount].atomIndex = i;
                appendChemArray[atom2Index].bonds[appendChemArray[atom2Index].bondCount].bondType = bondType;

                appendChemArray[i].bondCount++;
                appendChemArray[atom2Index].bondCount++;
            }else if (bondType == BOND_TYPE_5) {
                Tools::ShowError(wxT("Until now we have not supported the fourth bond list!"));
            }else {
                Tools::ShowError(wxT("The bond type is wrong!!!"));
            }
        }
    }
        //wxMessageBox(wxT("end create bond type"));
}

void DenFile::LoadBinaryFile(const wxString &strLoadedFileName, AtomBondArray &appendChemArray, int *&atomLabels)
{
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxDataInputStream dis(fis);
    ElemInfo* eleInfoOption = NULL;
        int i = 0, atom2Index;
        BondType bondType = BOND_TYPE_1;
        wxString atomSymbol = wxEmptyString;
    ElemId elemId;
    double x, y, z;
    long atomNumber;

        int atomCountBase = appendChemArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;

        strLine = GetNextString(dis);
        //wxMessageBox(strLine);

        while(strLine.First(DEN_ATOM_NUM_FLAG)!=0)
        {
                strLine = GetNextString(dis);
        //wxMessageBox(strLine);
        }
        strLine = GetNextString(dis);
        strLine.ToLong(&atomNumber);// the number of atoms

    for(i = 0; i < atomNumber; i++)
        {
        AtomBond atomBond = AtomBond();
                atomSymbol=GetNextString(dis);
        elemId = GetElemId(atomSymbol);
                x=GetNextValue(dis);
                y=GetNextValue(dis);
                z=GetNextValue(dis);
        atomBond.atom.elemId = elemId;
                if(atomBond.atom.elemId == ELEM_H) {
                        atomBond.atom.elemId = DEFAULT_HYDROGEN_ID;
                }
        atomBond.atom.pos.x = x;
        atomBond.atom.pos.y = y;
        atomBond.atom.pos.z = z;

        eleInfoOption =GetElemInfo((ElemId)atomBond.atom.elemId);

        atomBond.atom.SetDispRadius(eleInfoOption->dispRadius);
        atomBond.atom.label = ++atomLabels[ELEM_PREFIX + atomBond.atom.elemId];

        appendChemArray.Add(atomBond);
    }
        //wxMessageBox(wxT("begin create bond type"));
    //create bond type
    for(i = atomCountBase; i < atomCountBase + atomNumber; i++) {

            for(atom2Index = i + 1; atom2Index < atomCountBase + atomNumber; atom2Index++) {
            bondType = CalculateBondType(i, atom2Index, appendChemArray);

            if (bondType == BOND_TYPE_0) {
            }else if (bondType == BOND_TYPE_1 || bondType == BOND_TYPE_2 || bondType == BOND_TYPE_3 || bondType == BOND_TYPE_4) {
                appendChemArray[i].bonds[appendChemArray[i].bondCount].atomIndex = atom2Index;
                appendChemArray[i].bonds[appendChemArray[i].bondCount].bondType = bondType;

                appendChemArray[atom2Index].bonds[appendChemArray[atom2Index].bondCount].atomIndex = i;
                appendChemArray[atom2Index].bonds[appendChemArray[atom2Index].bondCount].bondType = bondType;

                appendChemArray[i].bondCount++;
                appendChemArray[atom2Index].bondCount++;
            }else if (bondType == BOND_TYPE_5) {
                Tools::ShowError(wxT("Until now we have not supported the fourth bond list!"));
            }else {
                Tools::ShowError(wxT("The bond type is wrong!!!"));
            }
        }
    }
        //wxMessageBox(wxT("end create bond type"));
    //XYZFile::SaveFile(GetLogDir() + wxT("m2msidata.txt"), appendChemArray, false);
}

void DenFile::LoadAsciiGridInfo(const wxString &strLoadedFileName, wxArrayString &gridInfoArray)
{
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
        strLine = tis.ReadLine();
        //wxMessageBox(strLine);
        while(strLine.First(DEN_GRID_NUM_FLAG)!=0)
        {
                strLine = tis.ReadLine();
        //wxMessageBox(strLine);
        }
        long gridNumber;
        wxString flagStr;
    wxStringTokenizer token2(strLine,DEN_DELIM);
        flagStr=token2.GetNextToken();
    token2.GetNextToken().ToLong(&gridNumber); // the number of grids
        strLine = tis.ReadLine();
        //wxMessageBox(strLine);
        while(strLine.First(DEN_GRID_INFO_FLAG)!=0)
        {
                strLine = tis.ReadLine();
        //wxMessageBox(strLine);
        }
        for(long i=0;i<gridNumber;i++)
        {
                wxStringTokenizer token3(strLine,DEN_DELIM);
                flagStr=token3.GetNextToken();
                gridInfoArray.Add(token3.GetNextToken()); // the gridinfo
                strLine = tis.ReadLine();
        //wxMessageBox(strLine);
    }
}

void DenFile::LoadBinaryGridInfo(const wxString &strLoadedFileName, wxArrayString &gridInfoArray)
{
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxDataInputStream dis(fis);
        strLine = GetNextString(dis);
        //wxMessageBox(strLine);
        while(strLine.First(DEN_GRID_NUM_FLAG)!=0)
        {
                strLine = GetNextString(dis);
        //wxMessageBox(strLine);
        }
        long gridNumber;
        strLine = GetNextString(dis);
        strLine.ToLong(&gridNumber);// the number of grids
        while(strLine.First(DEN_GRID_INFO_FLAG)!=0)
        {
                strLine = GetNextString(dis);
        }
        wxString tempStr;
        double value;
        for(long i=0;i<gridNumber-1;i++)
        {
                strLine=wxEmptyString;
                tempStr=GetNextString(dis);
                strLine+=tempStr;
                tempStr=GetNextString(dis);
                strLine+=wxT(" ")+tempStr;
                value=GetNextValue(dis);
                strLine+=wxString::Format(wxT(" %f "),value);
                value=GetNextValue(dis);
                strLine+=wxString::Format(wxT(" (%f) "),value);
                tempStr=GetNextString(dis);
                strLine+=wxT(" ")+tempStr;
                GetNextString(dis);
                gridInfoArray.Add(strLine); // the gridinfo
        //wxMessageBox(strLine);
    }
        strLine=wxEmptyString;
        tempStr=GetNextString(dis);
        strLine+=tempStr;
        tempStr=GetNextString(dis);
        strLine+=wxT(" (")+tempStr+wxT(")");
        gridInfoArray.Add(strLine); // the gridinfo
        //wxMessageBox(strLine);
}

wxString DenFile::GetNextString(wxDataInputStream &dis)
{
        wxUint8 buffer;
        wxString strReturn=wxEmptyString;
        char link='_';
        while(buffer=dis.Read8(),!isalnum(buffer) && buffer!=link)
                ;
        while(isalnum(buffer)|| buffer==link)
        {
                strReturn+=buffer;
                buffer=dis.Read8();
        }
        return strReturn;
}

double DenFile::GetNextValue(wxDataInputStream &dis)
{
        wxUint8 buffer;
        wxString strValue=wxEmptyString;
        char point='.';
        char oper='-';
        double value;
        while(buffer=dis.Read8(),!isdigit(buffer) && buffer!=oper)
                ;
        while(isdigit(buffer) || buffer==(int)point || buffer==oper)
        {
                strValue+=buffer;
                buffer=dis.Read8();
        }
        strValue.ToDouble(&value);
        return value;
}

bool DenFile::IsDenFile(const wxString &fileName)
{
        if(IsAsciiDenFile(fileName))
                return true;
        if(IsBinaryDenFile(fileName))
                return true;
        return false;
}

bool DenFile::IsBinaryDenFile(const wxString &fileName)
{
        wxString strLine;
        wxFileInputStream fis(fileName);
        wxDataInputStream dis(fis);
        int i = 0;
        wxString atomSymbol = wxEmptyString;
    long elemId,wordnum=0;
    long atomNumber;
        bool find=false;

        while(!fis.Eof())
        {
                strLine = GetNextString(dis);
                //wxMessageBox(strLine);
                if(strLine.First(DEN_ATOM_NUM_FLAG)==0)
                {
                        find=true;
                        //wxMessageBox(strLine);
                        strLine = GetNextString(dis);
                        strLine.ToLong(&atomNumber);// the number of atoms
                        for(i = 0; i < atomNumber; i++)
                        {
                                atomSymbol=GetNextString(dis);
                                elemId = GetElemId(atomSymbol);
                                if(elemId == ELEM_NULL)
                                        return false;
                                GetNextValue(dis);
                                GetNextValue(dis);
                                GetNextValue(dis);
                                if(fis.Eof())
                                        return false;
                        }
                        break;
                }
                wordnum++;
                if(wordnum>10 && !find)
                        return false;
        }
        return true;
}

bool DenFile::IsAsciiDenFile(const wxString &fileName)
{
        wxString strLine;
        wxArrayString strArr;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        int i = 0;
    long elemId,line=0;
    long atomNumber;
        bool find=false;

        while(!fis.Eof())
        {
                strLine = tis.ReadLine();
                if((strLine.First(DEN_ATOM_NUM_FLAG)==0))
                {
                        strArr=wxStringTokenize(strLine,DEN_DELIM,wxTOKEN_STRTOK);
                        strArr[1].ToLong(&atomNumber);
                        if(atomNumber<=0)
                                return false;
                        for(i = 0; i < atomNumber; i++)
                        {
                                strLine = tis.ReadLine();       // read one line
                                //wxMessageBox(strLine);
                                strArr=wxStringTokenize(strLine,wxT(" \t\n\r"),wxTOKEN_STRTOK);
                                if(strArr.GetCount()!=4)
                                        return false;
                                elemId = GetElemId(strArr[0]);
                                if(elemId == ELEM_NULL)
                                        return false;
                                if(fis.Eof())
                                        return false;
                        }
                        break;
                }
                line++;
                if(line>10 && !find)
                        return false;
        }
        return true;
}
