/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/file.h>
#include <wx/filename.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/textfile.h>
#include <wx/dcbuffer.h>
#include <wx/tokenzr.h>

#include "surfacepnl.h"
#include "surfacedata.h"
#include "denfile.h"
#include "ctrlinst.h"
#include "molvieweventhandler.h"
#include "projectmanager.h"
//----------XIA---------
#include "manager.h"
#include "simuproject.h"
#include "molview.h"
#include "abstractview.h"
#include "viewmanager.h"
#include <wx/arrimpl.cpp>

/////////////////////////////////////////////////////////////////////
//Static function
/////////////////////////////////////////////////////////////////////


static wxArrayString ConstructGridInfosFromDenFile(const wxString& fileName)
{
        wxArrayString gridInfos;
        if(!wxFileExists(fileName))
                return gridInfos;
        if(DenFile::IsAsciiDenFile(fileName))
                DenFile::LoadAsciiGridInfo(fileName,gridInfos);
        else if(DenFile::IsBinaryDenFile(fileName))
                DenFile::LoadBinaryGridInfo(fileName,gridInfos);
        return gridInfos;
}
//////////////////////////////////////////////////////////////////////
// const
//////////////////////////////////////////////////////////////////////
const int SurfaceColumnItemWidth=240;
const int SurfaceColumnItemHeight=40;
//////////////////////////////////////////////////////////////////////
// object array
//////////////////////////////////////////////////////////////////////
WX_DEFINE_OBJARRAY(SurfaceColumnItemArray);
WX_DEFINE_OBJARRAY(SurfaceColumnArray);
////////////////////////////////////////////////////////////////////////////
//Class SurfaceDrawUtil
///////////////////////////////////////////////////////////////////////////
void SurfaceDrawUtil::DrawSurfaceColumnItem(wxDC& dc,const SurfaceColumnItem *const item)
{
        wxRect rect;
        wxString string;
        if(item->HasMO)
        {
                dc.SetPen(wxPen(*wxBLACK_PEN));
                dc.SetBrush(wxBrush(*wxWHITE_BRUSH));
                dc.DrawCircle(item->GetCheckCenter(),CHECK_BOX_RADIUS);
                if(item->IsCheck)
                {
                        dc.SetPen(wxPen(*wxTRANSPARENT_PEN));
                        dc.SetBrush(wxBrush(*wxRED_BRUSH));
                        rect=item->GetCheckRect();
                        dc.DrawEllipse(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
                }
        }
        if(item->IsSelect)
        {
                rect=item->GetDrawAreaRect();
                dc.SetPen(wxPen(*wxTRANSPARENT_PEN));
                dc.SetBrush(wxBrush(wxT("yellow"),wxBRUSHSTYLE_SOLID));
                dc.DrawRectangle(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
        }
        rect=item->GetLStringRect();
        string=item->GetLString();
        DrawString(dc,rect,string);
        rect=item->GetLDrawLineRect();
        dc.SetPen(wxPen(*wxBLACK_PEN));
        dc.DrawLine(rect.GetLeft(),rect.GetTop(),rect.GetRight(),rect.GetTop());
        rect=item->GetBoxRect();
        dc.SetPen(wxPen(*wxBLACK_PEN));
        dc.SetBrush(wxBrush(*wxBLUE_BRUSH));
        dc.DrawRectangle(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
        if(item->IsLFocus && item->IsRFocus)
        {
                rect=item->GetFocusBoxRect();
                DrawFocusRect(dc,rect);
        }
        else
        {
                if(item->IsLFocus)
                {
                        rect=item->GetLFocusBoxRect();
                        DrawFocusRect(dc,rect);
                }
                if(item->IsRFocus)
                {
                        rect=item->GetRFocusBoxRect();
                        DrawFocusRect(dc,rect);
                }
        }
        dc.SetPen(wxPen(*wxBLACK_PEN));
        dc.SetBrush(wxBrush(*wxGREEN_BRUSH));
        if(item->HasLeft && item->HasRight)
        {
                if(item->IsLSelect && item->IsRSelect)
                {
                        rect=item->GetBoxRect();
                        dc.DrawRectangle(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
                }
                else
                {
                        if(item->IsLSelect)
                        {
                                rect=item->GetLBoxRect();
                                dc.DrawRectangle(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
                        }
                        if(item->IsRSelect)
                        {
                                rect=item->GetRBoxRect();
                                dc.DrawRectangle(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
                        }
                }
        }
        else
        {
                if(item->HasLeft)
                {
                        if(item->IsLSelect)
                        {
                                rect=item->GetLBoxRect();
                                dc.DrawRectangle(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
                        }
                }
                if(item->HasRight)
                {
                        if(item->IsRSelect)
                        {
                                rect=item->GetRBoxRect();
                                dc.DrawRectangle(rect.GetLeft(),rect.GetTop(),rect.GetWidth(),rect.GetHeight());
                        }
                }
        }
        if(item->HasLeft)
        {
                rect=item->GetLBoxRect();
                DrawBitmap(dc,rect,CheckImageFile(wxT("mosleft.png"),false));
        }
        if(item->HasRight)
        {
                rect=item->GetRBoxRect();
                DrawBitmap(dc,rect,CheckImageFile(wxT("mosright.png"),false));
        }
        rect=item->GetRDrawLineRect();
        dc.SetPen(wxPen(*wxBLACK_PEN));
        dc.DrawLine(rect.GetLeft(),rect.GetTop(),rect.GetRight(),rect.GetTop());
        rect=item->GetRStringRect();
        string=item->GetRString();
        DrawString(dc,rect,string);
        if(item->Separator)
        {
                dc.SetPen(wxPen(*wxGREEN, 2, wxPENSTYLE_DOT));
                rect=item->GetSeparatorRect();
                dc.DrawLine(rect.GetLeft(),rect.GetTop(),rect.GetRight(),rect.GetBottom());
        }
}

void SurfaceDrawUtil::DrawSurfaceColumnHeader(wxDC& dc,const wxRect& rect,const wxString& header)
{
        DrawString(dc,rect,header);
}

void SurfaceDrawUtil::DrawSurfaceColumnFooter(wxDC& dc,const wxRect& rect,const wxString& footer)
{
        DrawString(dc,rect,footer);
}

void SurfaceDrawUtil::DrawSurfaceColumnSeperator(wxDC& dc,const wxRect& rect)
{
    dc.SetPen(wxPen(*wxBLACK, 2, wxPENSTYLE_SOLID));
    dc.DrawLine(rect.GetLeft()+rect.GetWidth()/2,rect.GetTop(),rect.GetLeft()+rect.GetWidth()/2,rect.GetBottom());
}

void SurfaceDrawUtil::DrawBackground(wxDC& dc,const wxSize& size)
{
    //wxColour bgColour = GetBackgroundColour();
    wxColour bgColour = wxColour(*wxWHITE);
    //if (!bgColour.Ok())
    //    bgColour =wxSystemSettings::GetColour(wxSYS_COLOUR_3DFACE);
    //dc.SetBrush(wxBrush(bgColour));
        dc.SetBrush(wxBrush(*wxWHITE_BRUSH));
    dc.SetPen(wxPen(bgColour, 1));
    wxRect windowRect(wxPoint(0, 0), size);
    dc.DrawRectangle(windowRect);
}

void SurfaceDrawUtil::DrawString(wxDC &dc,const wxRect& rect,const wxString& string)
{
    wxFont font(12, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    dc.SetFont(font);
        wxSize strSize=dc.GetTextExtent(string);
        int offsetX=wxMax(0,(rect.GetWidth()-strSize.GetWidth())/2);
        int offsetY=wxMax(0,(rect.GetHeight()-strSize.GetHeight())/2);
        wxPoint pt=wxPoint(rect.GetLeft()+offsetX,rect.GetTop()+offsetY);
    dc.SetBackgroundMode(wxTRANSPARENT);
    dc.SetTextForeground(*wxBLACK);
    dc.SetTextBackground(*wxWHITE);
    dc.DrawText(string, pt);
}

void SurfaceDrawUtil::DrawBitmap(wxDC &dc,const wxRect& rect,wxString picture)
{
    if(!picture.IsEmpty())
    {
                wxFileName filename(picture);
                //if(!filename.FileExists())
                //{
                //filename.MakeRelativeTo(wxGetCwd());
                //filename.Normalize(wxPATH_NORM_LONG|wxPATH_NORM_DOTS|wxPATH_NORM_TILDE|wxPATH_NORM_ABSOLUTE);
                        //wxMessageBox(wxT("Sorry, file ")+filename.GetFullPath()+wxT(" does not exist."));
                picture=filename.GetFullPath();
                //}
                wxString str=filename.GetExt();
                str=str.Upper();
                //wxMessageBox(str);
                wxBitmap bmp;
                wxImage image;
                if(str.Cmp(wxT("BMP"))==0)
                {
                        bmp=wxBitmap(picture, wxBITMAP_TYPE_BMP);
                }
                else if(str.Cmp(wxT("GIF"))==0)
                {
                        image.LoadFile(picture,  wxBITMAP_TYPE_GIF);
                        bmp=wxBitmap(image);
                }
                else if(str.Cmp(wxT("XBM"))==0)
                {
                        bmp=wxBitmap(picture, wxBITMAP_TYPE_XBM);
                }
                else if(str.Cmp(wxT("XPM"))==0)
                {
                        bmp=wxBitmap(picture, wxBITMAP_TYPE_XPM);
                }
                else if(str.Cmp(wxT("PNG"))==0)
                {
                        bmp=wxBitmap(picture, wxBITMAP_TYPE_PNG);
                }
                else if(str.Cmp(wxT("JPEG"))==0)
                {
                        bmp=wxBitmap(picture, wxBITMAP_TYPE_JPEG);
                }
                else
                {
                        bmp=wxBitmap(picture, wxBITMAP_TYPE_JPEG);
                }
                //wxMessageBox(wxT("In MyDrawBox::Draw"));
        //wxBitmap bmp(wxT("../Picture/benzene.bmp"), wxBITMAP_TYPE_BMP);
                int posX,posY;
                posX=rect.GetLeft()+wxMax(0,(rect.GetWidth()-bmp.GetWidth())/2);
                posY=rect.GetTop()+wxMax(0,(rect.GetHeight()-bmp.GetHeight())/2);
                //wxMessageBox(wxString::Format(wxT("width=%d,height=%d"),width,height));
        dc.DrawBitmap(bmp, posX,posY,true);
    }
}

void SurfaceDrawUtil::DrawFocusRect(wxDC& dc,const wxRect &rect)
{
        dc.SetPen(wxPen(*wxWHITE, 1, wxPENSTYLE_SOLID));
        dc.DrawLine(rect.GetLeft(),rect.GetTop(),rect.GetRight(),rect.GetTop());
        dc.DrawLine(rect.GetLeft(),rect.GetTop(),rect.GetLeft(),rect.GetBottom());
        dc.SetPen(wxPen(wxColor(wxT("GRAY")), 1, wxPENSTYLE_SOLID));
        dc.DrawLine(rect.GetLeft(),rect.GetBottom(),rect.GetRight(),rect.GetBottom());
        dc.DrawLine(rect.GetRight(),rect.GetTop(),rect.GetRight(),rect.GetBottom());
}
////////////////////////////////////////////////////////////////////////
//class SurfaceColumnItem
////////////////////////////////////////////////////////////////////////
SurfaceColumnItem::SurfaceColumnItem()
{
        Index=0;
        Energy=0;
        Type=wxEmptyString;
        leftStr=wxEmptyString;
        rightStr=wxEmptyString;
        HasLeft=false;
        HasRight=false;
        HasMO=false;
        Top=Left=Right=Bottom=0;
        IsSelect=false;
        IsLSelect=false;
        IsRSelect=false;
        IsLFocus=false;
        IsRFocus=false;
        Separator=false;
        IsCheck=false;
}
bool SurfaceColumnItem::HitTest(int x,int y)
{
        if(HitBoxTest(x,y))
                return false;
        else
                return IsInRect(x,y,GetDrawAreaRect());
}
bool SurfaceColumnItem::HitLBoxTest(int x,int y)
{
        wxRect rect=GetLBoxRect();
        rect.width-=10;
        return IsInRect(x,y,rect);
}
bool SurfaceColumnItem::HitRBoxTest(int x,int y)
{
        wxRect rect=GetRBoxRect();
        rect.width-=10;
        return IsInRect(x,y,rect);
}
bool SurfaceColumnItem::HitBoxTest(int x,int y)
{
        return IsInRect(x,y,GetBoxRect());
}
bool SurfaceColumnItem::HitCheckTest(int x,int y)
{
        wxPoint checkCenter=GetCheckCenter();
        int dis=(int)sqrt(pow(x-checkCenter.x,2)+pow(y-checkCenter.y,2));
        return (dis<=CHECK_BOX_RADIUS);
}
bool SurfaceColumnItem::IsInRect(int x,int y,const wxRect rect)
{
        if(x<rect.GetLeft() || x>rect.GetRight()|| y<rect.GetTop() || y> rect.GetBottom())
                return false;
        return true;
}
int SurfaceColumnItem::GetWidth(void) const
{
        return Right-Left;
}
int SurfaceColumnItem::GetHeight(void) const
{
        return Bottom-Top;
}
wxRect SurfaceColumnItem::GetDrawAreaRect(void) const
{
        wxRect ret;
        ret.x=Left+GetWidth()/6;
        ret.y=Top;
        ret.width=GetWidth()*5/6;
        ret.height=GetHeight();
        return ret;
}
wxRect SurfaceColumnItem::GetBoxRect(void) const
{
        wxRect ret;
        ret.x=(int)(Left+GetWidth()*0.5);
        ret.y=Top;
        ret.width=GetWidth()/6;
        ret.height=GetHeight();
        return ret;
}
wxRect SurfaceColumnItem::GetLBoxRect(void) const
{
        wxRect ret;
        ret.x=(int)(Left+GetWidth()*0.5);
        ret.y=Top;
        ret.width=GetWidth()/12;
        ret.height=GetHeight();
        return ret;
}
wxRect SurfaceColumnItem::GetRBoxRect(void) const
{
        wxRect ret;
        ret.x=Left+GetWidth()*7/12;
        ret.y=Top;
        ret.width=GetWidth()/12;
        ret.height=GetHeight();
        return ret;
}
wxRect SurfaceColumnItem::GetLStringRect(void) const
{
        wxRect ret;
        ret.x=Left+GetWidth()/6;
        ret.y=Top;
        ret.width=GetWidth()*2/9;
        ret.height=GetHeight();
        return ret;
}
wxRect SurfaceColumnItem::GetRStringRect(void) const
{
        wxRect ret;
        ret.x=Left+GetWidth()*7/9;
        ret.y=Top;
        ret.width=GetWidth()*2/9;
        ret.height=GetHeight();
        return ret;
}

wxRect SurfaceColumnItem::GetFocusBoxRect(void) const
{
        wxRect ret;
        ret.x=(int)(Left+GetWidth()*0.5+BOX_FOCUS_OFFSET);
        ret.y=Top+BOX_FOCUS_OFFSET;
        ret.width=GetWidth()/6-BOX_FOCUS_OFFSET*2;
        ret.height=GetHeight()-BOX_FOCUS_OFFSET*2;
        return ret;
}
wxRect SurfaceColumnItem::GetLFocusBoxRect(void) const
{
        wxRect ret;
        ret.x=(int)(Left+GetWidth()*0.5+BOX_FOCUS_OFFSET);
        ret.y=Top+BOX_FOCUS_OFFSET;
        ret.width=GetWidth()/12-BOX_FOCUS_OFFSET*2;
        ret.height=GetHeight()-BOX_FOCUS_OFFSET*2;
        return ret;
}
wxRect SurfaceColumnItem::GetRFocusBoxRect(void) const
{
        wxRect ret;
        ret.x=Left+GetWidth()*7/12+BOX_FOCUS_OFFSET;
        ret.y=Top+BOX_FOCUS_OFFSET;
        ret.width=GetWidth()/12-BOX_FOCUS_OFFSET*2;
        ret.height=GetHeight()-BOX_FOCUS_OFFSET*2;
        return ret;
}
wxRect SurfaceColumnItem::GetLDrawLineRect(void) const
{
        wxRect ret;
        ret.x=Left+GetWidth()*7/18;
        ret.y=Top+GetHeight()/2;
        ret.width=GetWidth()/9;
        ret.height=0;
        return ret;
}
wxRect SurfaceColumnItem::GetRDrawLineRect(void) const
{
        wxRect ret;
        ret.x=Left+GetWidth()*2/3;
        ret.y=Top+GetHeight()/2;
        ret.width=GetWidth()/9;
        ret.height=0;
        return ret;
}
wxPoint SurfaceColumnItem::GetCheckCenter(void) const
{
        wxPoint ret;
        ret.x=Left+GetWidth()/12;
        ret.y=Top+GetHeight()/2;
        return ret;
}
wxRect SurfaceColumnItem::GetCheckRect(void) const
{
        wxRect rect;
        wxPoint center=GetCheckCenter();
        rect.x=center.x-CHECK_BOX_RADIUS;
        rect.y=center.y-CHECK_BOX_RADIUS;
        rect.width=CHECK_BOX_RADIUS*2;
        rect.height=CHECK_BOX_RADIUS*2;
        return rect;
}
wxString SurfaceColumnItem::GetLString(void) const
{/*
        wxString strLeft;
        strLeft.Printf(wxT("%d "),Index);
        if(!Type.IsEmpty())
                strLeft+=wxT("(")+Type+wxT(")");
        return strLeft;*/
        return leftStr;
}
wxString SurfaceColumnItem::GetRString(void) const
{
        wxString strRight;
        if(rightStr.IsEmpty())
                strRight.Printf(wxT("%.4f"),Energy);
        else
                strRight=rightStr;
        return strRight;
}
wxRect SurfaceColumnItem::GetSeparatorRect(void) const
{
        wxRect ret;
        ret.x=Left;
        ret.y=Bottom;
        ret.width=GetWidth();
        ret.height=0;
        return ret;
}
/////////////////////////////////////////////////////////////////////////
// CLASS wxSelChangeEvent
/////////////////////////////////////////////////////////////////////////
DEFINE_EVENT_TYPE(wxEVT_COMMAND_SEL_CHANGED)
IMPLEMENT_DYNAMIC_CLASS(wxSelChangeEvent, wxCommandEvent)
///////////////////////////////////////////////////////////////////////
wxIntArray wxSelChangeEvent::m_lastSel;
wxIntArray wxSelChangeEvent::m_newSel;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
wxSelChangeEvent::wxSelChangeEvent(wxEventType commandType, int id)
:wxCommandEvent(commandType,id)
{

}
wxSelChangeEvent::wxSelChangeEvent(const wxSelChangeEvent &event)
:wxCommandEvent(event)
{
}

wxSelChangeEvent::~wxSelChangeEvent()
{

}


wxEvent* wxSelChangeEvent::Clone() const
{
        return new wxSelChangeEvent(*this);
}

void wxSelChangeEvent::SetSelect(const wxIntArray &oldSel, const wxIntArray &newSel)
{
        m_lastSel.Clear();
        m_newSel.Clear();
        m_lastSel=oldSel;
        m_newSel=newSel;
}

void wxSelChangeEvent::SetSelect(const wxIntArray &sel)
{
        m_lastSel.Clear();
        m_lastSel=m_newSel;
        m_newSel=sel;
}
wxIntArray wxSelChangeEvent::GetLastSelect()
{
        return m_lastSel;
}

wxIntArray wxSelChangeEvent::GetNewSelect()
{
        return m_newSel;
}

////////////////////////////////////////////////////////////////////////
//Class SurfaceWindow
///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// event table
//////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(SurfaceWindow,wxScrolledWindow /*wxPanel*/)
        EVT_SIZE(SurfaceWindow::OnSize)
        EVT_PAINT(SurfaceWindow::OnPaint)
        EVT_SCROLLWIN(SurfaceWindow::OnScroll)
        EVT_LEFT_DOWN(SurfaceWindow::OnLeftMouseDown)
        EVT_LEFT_UP(SurfaceWindow::OnLeftMouseUp)
        EVT_MOTION(SurfaceWindow::OnMouseMove)
END_EVENT_TABLE()
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SurfaceWindow::SurfaceWindow(wxWindow* parent, wxWindowID id, const wxPoint& pos,
            const wxSize& size, long style, const wxString& name )
                        : wxScrolledWindow /*wxPanel*/(parent, id, pos, size, style | wxSUNKEN_BORDER, name)
{
        //CreateGUIControls();
        SetBackgroundStyle(wxBG_STYLE_CUSTOM);
        drawWidth=drawHeight=0;
        //cursor.x=cursor.y=0;
        IsPainting=false;
        pBitmap=NULL;
        selectedRow=selectedCol=-1;

}

SurfaceWindow::~SurfaceWindow()
{
        if(pBitmap)
                delete pBitmap;
}


void SurfaceWindow::AppendColumns(SurfaceColumnArray columns)
{
        columnArray=columns;
        SetColumnPositions(columnArray,drawWidth,drawHeight);
        ResetDrawSize();
        SetScrollBars();
        Refresh();
        //DumpMOSData();
        //cursor.x=cursor.y=0;
}


void SurfaceWindow::OnPaint(wxPaintEvent &event)
{
        //static bool IsBusy=false;
        //DumpMOSData();
        //if(!IsBusy)
        //{
                //IsBusy=true;
        wxBufferedPaintDC bufferDC(this);
        PrepareDC(bufferDC);
        if(!IsPainting)
        {
                DrawOnBitmap();
                IsPainting=false;
        }
        bufferDC.SetBrush(wxBrush(*wxWHITE_BRUSH));
        bufferDC.Clear();
        //bufferDC.Blit(start.x,start.y,cw,ch,&memDC,start.x,start.y/*,wxOR_REVERSE*/);
        //bufferDC.Blit(0,0,cw,ch,&memDC,0,0/*,wxOR_REVERSE*/);
        bufferDC.DrawBitmap(*pBitmap,0,0,true);
        //event.Skip();
}
void SurfaceWindow::DrawOnBitmap(void)
{
        wxMemoryDC memDC;
        IsPainting = true;
        if(!pBitmap)
        pBitmap = new wxBitmap(drawWidth, drawHeight);
        memDC.SelectObject(*pBitmap);
        Draw(memDC);
        //pBitmap->SaveFile(GetLogDir() + wxT("mosview.bmp"),wxBITMAP_TYPE_BMP);
}
void SurfaceWindow::ResetDrawSize()
{
        if(pBitmap)
                delete pBitmap;
        //resize the bitmap
        pBitmap = new wxBitmap(drawWidth, drawHeight);
}
void SurfaceWindow::Draw(wxDC &dc)
{
        unsigned int col,row;
        int headerHeight=SurfaceColumnItemHeight,left,top,footerHeight=SurfaceColumnItemHeight;
        wxRect rect;
        SurfaceDrawUtil::DrawBackground(dc,wxSize(drawWidth,drawHeight));
        for(col=0,left=0;col<columnArray.GetCount();col++,left+=SurfaceColumnItemWidth+5)
        {
                rect=wxRect(left,0,SurfaceColumnItemWidth,headerHeight);
                SurfaceDrawUtil::DrawSurfaceColumnHeader(dc,rect,columnArray[col].ColumnHeader);
                for(row=0,top=headerHeight;row<columnArray[col].Column.GetCount();row++)
                {
                        SurfaceDrawUtil::DrawSurfaceColumnItem(dc,&(columnArray[col].Column[row]));
                        top+=columnArray[col].Column[row].GetHeight();
                }
                rect=wxRect(left,top,SurfaceColumnItemWidth,footerHeight);
                SurfaceDrawUtil::DrawSurfaceColumnFooter(dc,rect,columnArray[col].ColumnFooter);
        }
}

void SurfaceWindow::OnLeftMouseDown(wxMouseEvent &event)
{
        long posX,posY;
        event.GetPosition(&posX, &posY);
        //posX+=cursor.x*SurfaceColumnItemHeight;
        //posY+=cursor.y*SurfaceColumnItemHeight;
        //int rx,ry;
        //CalcUnscrolledPosition(posX,posY,&rx,&ry);
        //posX=rx;
        //posY=ry;
        //posX+=cursor.x*SurfaceColumnItemHeight;
        //posY+=cursor.y*SurfaceColumnItemHeight;
        ScreenToLogicalCoords(posX,posY);
        CheckChecked(posX,posY);
        CheckSelect(posX,posY);
        if(!CheckLBoxSelect(posX,posY,true))
                if(!CheckRBoxSelect(posX,posY,true))
                        CheckBoxSelect(posX,posY,true);
        Refresh();
        int row,col;
        GetCheckedRowCol(row,col);
        selectedRow=row;
        selectedCol=col;
//        wxMessageBox(wxString::Format(wxT("%d"),row)+wxT(",")+wxString::Format(wxT("%d"),col));
        wxIntArray sel;
        sel.Add(selectedRow);
        sel.Add(selectedCol);
        wxSelChangeEvent selEvent(wxEVT_COMMAND_SEL_CHANGED, wxID_ANY);
        selEvent.SetSelect(sel);
        selEvent.SetEventObject(this);
        GetParent()->GetEventHandler()->ProcessEvent(selEvent);
        //if(row>=0 && col>=0)
        //        ((DlgMOS*)GetParent())->ShowSurface(row,col,columnArray[col].Column[row]);
}

void SurfaceWindow::OnLeftMouseUp(wxMouseEvent &event)
{
        long posX,posY;
        event.GetPosition(&posX, &posY);
/*        int offsetX,offsetY;
        CalcScrolledPosition(0,0,&(offsetX),&(offsetY));
        wxString msg;
        msg.Printf(wxT("offsetX=%d,offsetY=%d"),offsetX,offsetY);
        int vx,vy;
        GetViewStart(&vx,&vy);
        msg.Printf(wxT("vX=%d,vY=%d"),vx,vy);
        wxMessageBox(msg);
        msg.Printf(wxT("cursorY=%d"),posY);
        wxMessageBox(msg);
        int rx,ry;
        CalcUnscrolledPosition(posX,posY,&rx,&ry);
        posX=rx;
        posY=ry;
        //posX+=cursor.x*SurfaceColumnItemHeight;
        //posY+=cursor.y*SurfaceColumnItemHeight;
*/
        ScreenToLogicalCoords(posX,posY);
        if(!CheckLBoxSelect(posX,posY,false))
                if(!CheckRBoxSelect(posX,posY,false))
                        CheckBoxSelect(posX,posY,false);
        //msg.Printf(wxT("cursorY=%d"),posY);
        //wxMessageBox(msg);
        Refresh();
        //----------XIA--------
        /*
        MolView* pView = static_cast<MolView*>(GetActiveView());
        if(!pView) return;
   SimuProject* pProject = Manager::Get()->GetProjectManager()->GetProject(pView->GetProjectId());
   if(!pProject) return;

   if(pProject->IsDirty())
      wxMessageBox(wxT("hello"));
   */

}

void SurfaceWindow::OnMouseMove(wxMouseEvent &event)
{
        long posX,posY;
        event.GetPosition(&posX, &posY);
/*        int rx,ry;
        CalcUnscrolledPosition(posX,posY,&rx,&ry);
        posX=rx;
        posY=ry;
*/        //posX+=cursor.x*SurfaceColumnItemHeight;
        //posY+=cursor.y*SurfaceColumnItemHeight;
        ScreenToLogicalCoords(posX,posY);
        if(!CheckLFocus(posX,posY))
                if(!CheckRFocus(posX,posY))
                        CheckFocus(posX,posY);
        Refresh();
}

void SurfaceWindow::SetColumnPositions(SurfaceColumnArray &array,int& width,int& height)
{
        unsigned int row,col;
        int headerHeight=SurfaceColumnItemHeight,left,top,maxheight;
//        wxString temp;
        height=0;
        for(col=0,left=0;col<array.GetCount();col++,left+=SurfaceColumnItemWidth+5)
        {
                maxheight=0;
                for(row=0,top=headerHeight;row<array[col].Column.GetCount();row++,top+=SurfaceColumnItemHeight+5)
                {
                        array[col].Column[row].Top=top;
                        array[col].Column[row].Bottom=top+SurfaceColumnItemHeight;
                        array[col].Column[row].Left=left;
                        array[col].Column[row].Right=left+SurfaceColumnItemWidth;
/*                        temp.Printf(wxT("Top=%d,Bottom=%d,Left=%d,Right=%d"),
                                                array[col].Column[row].Top,
                                                array[col].Column[row].Bottom,
                                                array[col].Column[row].Left,
                                                array[col].Column[row].Right);
                        wxMessageBox(temp);

*/                        maxheight=array[col].Column[row].Bottom;
                }
                maxheight+=SurfaceColumnItemHeight;
                if(maxheight>height)
                        height=maxheight;
        }
        width=SurfaceColumnItemWidth*array.GetCount();
}

bool SurfaceWindow::CheckFocus(int x,int y)
{
        int row,col;
        int selRow,selCol;
        selCol=selRow=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HitBoxTest(x,y))
                        {
                                selCol=col;
                                selRow=row;
                        }
                }
        }
        if(selCol>=0 && selRow>=0)
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                if(row==selRow && col==selCol)
                                {
                                        columnArray[col].Column[row].IsLFocus= true;//!columnArray[col].Column[row].IsLFocus;
                                        columnArray[col].Column[row].IsRFocus= true;//columnArray[col].Column[row].IsLFocus;
                                }
                                else
                                {
                                        columnArray[col].Column[row].IsLFocus= false;//!columnArray[col].Column[row].IsLFocus;
                                        columnArray[col].Column[row].IsRFocus= false;//columnArray[col].Column[row].IsLFocus;
                                }
                        }
                }
                return true;
        }
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        columnArray[col].Column[row].IsLFocus= false;//!columnArray[col].Column[row].IsLFocus;
                        columnArray[col].Column[row].IsRFocus= false;//columnArray[col].Column[row].IsLFocus;
                }
        }
        return false;
}

bool SurfaceWindow::CheckSelect(int x,int y)
{
        int row,col;
        int selRow,selCol;
        selCol=selRow=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HitTest(x,y))
                        {
                                selCol=col;
                                selRow=row;
                        }
                }
        }
        if(selCol>=0 && selRow>=0)
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                if(row==selRow && col==selCol)
                                {
                                        columnArray[col].Column[row].IsSelect= !columnArray[col].Column[row].IsSelect;
                                        return true;
                                }
                        }
                }
        }
        return false;
}

SurfaceColumnArray SurfaceWindow::GetColumnData()
{
        return columnArray;
}

void SurfaceWindow::CheckChecked(int x, int y)
{
        int row,col;
        int rowChecked=-1,colChecked=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HasMO)
                        {
                                if(columnArray[col].Column[row].HitCheckTest(x,y))
                                {
                                        colChecked=col;
                                        rowChecked=row;
                                }
                        }
                }
        }
        if(rowChecked>=0 && colChecked>=0 )
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                /*
                                if(row==rowChecked && col==colChecked)
                                        columnArray[col].Column[row].IsCheck=true;
                                else
                                        columnArray[col].Column[row].IsCheck=false;
                                */
                                if(row==rowChecked && col==colChecked)
                                        columnArray[col].Column[row].IsCheck=!columnArray[col].Column[row].IsCheck;
                                else
                                        columnArray[col].Column[row].IsCheck=false;

                        }
                }
        }
}

void SurfaceWindow::DumpMOSData()
{
        wxFileName fileName=wxFileName(wxT("logs")+ wxFileName::GetPathSeparators() +wxT("mosviewdata.dat"));
        fileName.Normalize(wxPATH_NORM_LONG|wxPATH_NORM_DOTS|wxPATH_NORM_TILDE|wxPATH_NORM_ABSOLUTE);
        fileName.MakeRelativeTo(wxGetCwd());
        fileName.Normalize(wxPATH_NORM_LONG|wxPATH_NORM_DOTS|wxPATH_NORM_TILDE|wxPATH_NORM_ABSOLUTE);
    wxFileOutputStream fos(fileName.GetFullPath());
    wxTextOutputStream tos(fos);
        unsigned int i,j;
    for(i = 0; i < columnArray.GetCount(); i++)
        {
                tos<<columnArray[i].ColumnHeader<<endl;
                for(j=0;j<columnArray[i].Column.GetCount();j++)
                {
                        tos<<wxT("\t")
                                <<columnArray[i].Column[j].GetLString()
                                <<wxT("\t")
                                <<columnArray[i].Column[j].GetRString()
                                <<endl;
                }
                tos<<columnArray[i].ColumnFooter<<endl;
                tos<<endl;
    }
}

bool SurfaceWindow::CheckLFocus(int x, int y)
{
        int row,col;
        int selRow,selCol;
        selCol=selRow=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HitLBoxTest(x,y))
                        {
                                selCol=col;
                                selRow=row;
                        }
                }
        }
        if(selCol>=0 && selRow>=0)
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                if(row==selRow && col==selCol)
                                {
                                        columnArray[col].Column[row].IsLFocus= true;//!columnArray[col].Column[row].IsLFocus;
                                }
                                else
                                {
                                        columnArray[col].Column[row].IsLFocus= false;//!columnArray[col].Column[row].IsLFocus;
                                }
                        }
                }
                return true;
        }
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        columnArray[col].Column[row].IsLFocus= false;//!columnArray[col].Column[row].IsLFocus;
                }
        }
        return false;
}

bool SurfaceWindow::CheckRFocus(int x, int y)
{
        int row,col;
        int selRow,selCol;
        selCol=selRow=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HitRBoxTest(x,y))
                        {
                                selCol=col;
                                selRow=row;
                        }
                }
        }
        if(selCol>=0 && selRow>=0)
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                if(row==selRow && col==selCol)
                                {
                                        columnArray[col].Column[row].IsRFocus= true;//columnArray[col].Column[row].IsLFocus;
                                }
                                else
                                {
                                        columnArray[col].Column[row].IsRFocus= false;//columnArray[col].Column[row].IsLFocus;
                                }
                        }
                }
                return true;
        }
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        columnArray[col].Column[row].IsRFocus= false;//columnArray[col].Column[row].IsLFocus;
                }
        }
        return false;
}

bool SurfaceWindow::CheckLBoxSelect(int x, int y,bool IsMouseDown)
{
        int row,col;
        int selRow,selCol;
        selCol=selRow=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HitLBoxTest(x,y))
                        {
                                selCol=col;
                                selRow=row;
                        }
                }
        }
        if(selCol>=0 && selRow>=0)
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                if(row==selRow && col==selCol)
                                {
                                        columnArray[col].Column[row].IsLSelect=IsMouseDown;
                                }
                                else
                                {
                                        columnArray[col].Column[row].IsLSelect=false;
                                }
                        }
                }
                return true;
        }
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        columnArray[col].Column[row].IsLSelect=false;
                }
        }
        return false;
}

bool SurfaceWindow::CheckRBoxSelect(int x, int y,bool IsMouseDown)
{
        int row,col;
        int selRow,selCol;
        selCol=selRow=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HitRBoxTest(x,y))
                        {
                                selCol=col;
                                selRow=row;
                        }
                }
        }
        if(selCol>=0 && selRow>=0)
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                if(row==selRow && col==selCol)
                                        columnArray[col].Column[row].IsRSelect=IsMouseDown;// !columnArray[col].Column[row].IsRSelect;
                                else
                                        columnArray[col].Column[row].IsRSelect=false;// !columnArray[col].Column[row].IsRSelect;
                        }
                }
                return true;
        }
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        columnArray[col].Column[row].IsRSelect=false;// !columnArray[col].Column[row].IsRSelect;
                }
        }
        return false;
}

bool SurfaceWindow::CheckBoxSelect(int x, int y,bool IsMouseDown)
{
        int row,col;
        int selRow,selCol;
        selCol=selRow=-1;
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                        if(columnArray[col].Column[row].HitBoxTest(x,y))
                        {
                                selCol=col;
                                selRow=row;
                        }
                }
        }
        if(selCol>=0 && selRow>=0)
        {
                for(col=0;col<(int)columnArray.GetCount();col++)
                {
                        for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                        {
                                if(row==selRow && col==selCol)
                                {
                                        columnArray[col].Column[row].IsLSelect=IsMouseDown;// !columnArray[col].Column[row].IsLSelect;
                                        columnArray[col].Column[row].IsRSelect=IsMouseDown;// columnArray[col].Column[row].IsLSelect;
                                }
                                else
                                {
                                        columnArray[col].Column[row].IsLSelect=false;// !columnArray[col].Column[row].IsLSelect;
                                        columnArray[col].Column[row].IsRSelect=false;// columnArray[col].Column[row].IsLSelect;
                                }
                        }
                }
                return true;
        }
        for(col=0;col<(int)columnArray.GetCount();col++)
        {
                for(row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                                columnArray[col].Column[row].IsLSelect=false;// !columnArray[col].Column[row].IsLSelect;
                                columnArray[col].Column[row].IsRSelect=false;// columnArray[col].Column[row].IsLSelect;
                }
        }
        return false;
}

void SurfaceWindow::OnSize(wxSizeEvent &event)
{
        SetScrollBars();
        Refresh();
        event.Skip();
}

void SurfaceWindow::SetScrollBars()
{
        int cw,ch;
        //wxString msg;
        //GetVirtualSize(&cw,&ch);
        //msg.Printf(wxT("Virtual Size:w=%d,h=%d"),cw,ch);
        //wxMessageBox(msg);
        GetClientSize(&cw,&ch);
        //msg.Printf(wxT("Client Size:w=%d,h=%d"),cw,ch);
        //wxMessageBox(msg);
        //ClientToScreen(&cw,&ch);
        int orientation,position,thumbSize,range;
        if(drawWidth>cw)
        {
                orientation=wxHORIZONTAL;
                position=0;
                thumbSize=cw/SurfaceColumnItemHeight;
                range=drawWidth/SurfaceColumnItemHeight;
        }
        else
        {
                orientation=wxHORIZONTAL;
                position=0;
                thumbSize=cw/SurfaceColumnItemHeight;
                range=0;
        }
        lineLength=wxMax(0,range-thumbSize);
        SetScrollbar(orientation, position, thumbSize, range);
        if(drawHeight>ch)
        {
                orientation=wxVERTICAL;
                position=0;
                thumbSize=ch/SurfaceColumnItemHeight;
                range=drawHeight/SurfaceColumnItemHeight;
        }
        else
        {
                orientation=wxVERTICAL;
                position=0;
                thumbSize=ch/SurfaceColumnItemHeight;
                range=0;
        }
        lineCount=wxMax(0,range-thumbSize);
        SetScrollbar(orientation, position, thumbSize, range);
        if(drawWidth>cw || drawHeight>ch)
        SetScrollbars(SurfaceColumnItemHeight,SurfaceColumnItemHeight,
                                        drawWidth/SurfaceColumnItemHeight,
                                        drawHeight/SurfaceColumnItemHeight);
        //cursor.x=cursor.y=0;
        Refresh();
}

void SurfaceWindow::OnScroll(wxScrollWinEvent &event)
{
        //DumpMOSData();
/*        int offsetX,offsetY;
        CalcScrolledPosition(0,0,&(offsetX),&(offsetY));
    if (HasCapture())
        {
        if (event.GetOrientation() == wxHORIZONTAL)
                {
            if (event.GetEventType() == wxEVT_SCROLLWIN_LINEUP)
                        {
                cursor.x-=offsetX;
            }
                        else if (event.GetEventType() == wxEVT_SCROLLWIN_LINEDOWN)
                        {
                cursor.x+=offsetX;
            }
        }
                else if (event.GetOrientation() == wxVERTICAL)
                {
            if (event.GetEventType() == wxEVT_SCROLLWIN_LINEUP)
                        {
                cursor.y-=offsetY;
            } else if (event.GetEventType() == wxEVT_SCROLLWIN_LINEDOWN)
                        {
                cursor.y+=offsetY;
            }
        }
    }
*/        //CalcScrolledPosition(0,0,&(cursor.x),&(cursor.y));
        Refresh();
        event.Skip();
}

wxPoint SurfaceWindow::ScreenToLogicalCoords(wxPoint pos) const
{
    int vX, vY;
    GetViewStart(&vX, &vY);
        pos.x+=vX*SurfaceColumnItemHeight;
        pos.y+=vY*SurfaceColumnItemHeight;
        return pos;
}

wxPoint SurfaceWindow::GetStartPos()
{
        return ScreenToLogicalCoords(wxPoint(0,0));
}

void SurfaceWindow::ScreenToLogicalCoords(int &posX, int &posY)
{
    int vX, vY;
    GetViewStart(&vX, &vY);
        posX+=vX*SurfaceColumnItemHeight;
        posY+=vY*SurfaceColumnItemHeight;
}

void SurfaceWindow::ScreenToLogicalCoords(long &posX, long &posY)
{
    int vX, vY;
    GetViewStart(&vX, &vY);
        posX+=vX*SurfaceColumnItemHeight;
        posY+=vY*SurfaceColumnItemHeight;
}

//DEL void SurfaceWindow::AppendColumns(const wxString fileName)
//DEL {
//DEL         wxFileInputStream fis(fileName);
//DEL         wxTextInputStream tis(fis);
//DEL         wxString strLine,tempStr;
//DEL         wxArrayString words=GetMolcasMOSpecialWords();
//DEL         wxArrayString twords=GetMolcasOutputFileSpecialWords();
//DEL         SurfaceColumnArray columns;
//DEL         SurfaceColumn        column;
//DEL         SurfaceColumnItem item;
//DEL         wxArrayString orbitalArray,energyArray,numArray;
//DEL         unsigned int i;
//DEL         if(words.GetCount()<3)
//DEL                 return;
//DEL         for(i=0;i<words.GetCount();i++)
//DEL         {
//DEL                 twords.Remove(words[i]);
//DEL         }
//DEL         while(!fis.Eof())
//DEL         {
//DEL                 strLine=tis.ReadLine();
//DEL                 if(FindSpecialWordsInString(words[0],strLine) && !FindSpecialWordsInArray(twords,strLine,false))
//DEL                 {
//DEL                         strLine.Replace(words[0],wxEmptyString);
//DEL                         //wxMessageBox(strLine);
//DEL                         if(!HasWordInString(strLine))
//DEL                         {
//DEL                                 //wxMessageBox(strLine);
//DEL                                 wxStringTokenizer token(strLine,wxT(" \t\r\n"));
//DEL                                 while(token.HasMoreTokens())
//DEL                                 {
//DEL                                         tempStr=token.GetNextToken();
//DEL                                         if(!tempStr.IsEmpty())
//DEL                                         {
//DEL                                                 //wxMessageBox(tempStr);
//DEL                                                 orbitalArray.Add(tempStr);
//DEL                                         }
//DEL                                 }
//DEL                         }
//DEL                 }
//DEL                 else if(FindSpecialWordsInString(words[1],strLine) && !FindSpecialWordsInArray(twords,strLine,false))
//DEL                 {
//DEL                         strLine.Replace(words[1],wxEmptyString);
//DEL                         if(!HasWordInString(strLine))
//DEL                         {
//DEL                                 //wxMessageBox(strLine);
//DEL                                 wxStringTokenizer token(strLine,wxT(" \t\r\n"));
//DEL                                 while(token.HasMoreTokens())
//DEL                                 {
//DEL                                         tempStr=token.GetNextToken();
//DEL                                         if(!tempStr.IsEmpty())
//DEL                                         {
//DEL                                                 //wxMessageBox(tempStr);
//DEL                                                 energyArray.Add(tempStr);
//DEL                                         }
//DEL                                 }
//DEL                         }
//DEL                 }
//DEL                 else if(FindSpecialWordsInString(words[2],strLine) && !FindSpecialWordsInArray(twords,strLine,false))
//DEL                 {
//DEL                         strLine.Replace(words[2],wxEmptyString);
//DEL                         if(!HasWordInString(strLine))
//DEL                         {
//DEL                                 //wxMessageBox(strLine);
//DEL                                 wxStringTokenizer token(strLine,wxT(" \t\r\n"));
//DEL                                 while(token.HasMoreTokens())
//DEL                                 {
//DEL                                         tempStr=token.GetNextToken();
//DEL                                         if(!tempStr.IsEmpty())
//DEL                                         {
//DEL                                                 //wxMessageBox(tempStr);
//DEL                                                 numArray.Add(tempStr);
//DEL                                         }
//DEL                                 }
//DEL                         }
//DEL                 }
//DEL         }
//DEL         if(numArray.GetCount()!= energyArray.GetCount() ||
//DEL                 numArray.GetCount()!= orbitalArray.GetCount()||
//DEL                 orbitalArray.GetCount()!= energyArray.GetCount())
//DEL                 return;
//DEL         double dv;
//DEL         long lv;
//DEL         //wxMessageBox(wxT("Adding data"));
//DEL         for(i=0;i<orbitalArray.GetCount();i++)
//DEL         {
//DEL                 orbitalArray[i].ToLong(&lv);
//DEL                 item.Index=(int)lv;
//DEL                 energyArray[i].ToDouble(&dv);
//DEL                 item.Energy=dv;
//DEL                 numArray[i].ToLong(&lv);
//DEL                 switch(lv)
//DEL                 {
//DEL                 case 2:
//DEL                         item.HasLeft=true;
//DEL                         item.HasRight=true;
//DEL                         break;
//DEL                 case 1:
//DEL                         item.HasLeft=true;
//DEL                         item.HasRight=false;
//DEL                         break;
//DEL                 default:
//DEL                         item.HasLeft=false;
//DEL                         item.HasRight=false;
//DEL                         break;
//DEL                 }
//DEL                 item.HasMO=true;
//DEL                 column.Column.Add(item);
//DEL         }
//DEL         columns.Add(column);
//DEL         columnArray=columns;
//DEL         SortColumnByEnergy();
//DEL         SetColumnPositions(columnArray,drawWidth,drawHeight);
//DEL         SetScrollBars();
//DEL         ResetDrawSize();
//DEL         Refresh();
//DEL         DumpMOSData();
//DEL }

void SurfaceWindow::GetCheckedRowCol(int &row, int &col)
{
        unsigned int i,j;
        row=col=-1;
        for(i=0;i<columnArray.GetCount();i++)
        {
                for(j=0;j<columnArray[i].Column.GetCount();j++)
                {
                        if(columnArray[i].Column[j].IsCheck)
                        {
                                row=j;
                                col=i;
                                return;
                        }
                }
        }
}

void SurfaceWindow::SortColumnByEnergy()
{
        unsigned int i,j,k;
        SurfaceColumnItem item;
        for(i=0;i<columnArray.GetCount();i++)
        {
                for(j=0;j<columnArray[i].Column.GetCount();j++)
                {
                        for(k=j+1;k<columnArray[i].Column.GetCount();k++)
                        {
                                if(columnArray[i].Column[j].Energy < columnArray[i].Column[k].Energy)
                                {
                                        item=columnArray[i].Column[k];
                                        columnArray[i].Column[k]=columnArray[i].Column[j];
                                        columnArray[i].Column[j]=item;
                                }
                        }
                        columnArray[i].Column[j].Index=columnArray[i].Column.GetCount()-j;
                }
        }

}

void SurfaceWindow::AppendColumns(const wxArrayString &gridInfo)
{
        wxString strLine;
        SurfaceColumnArray columns;
        SurfaceColumn        column;
        SurfaceColumnItem item;
        columnArray.Clear();
        for(unsigned int i=0;i<gridInfo.GetCount();i++)
        {
                item=ConvertStringToSCItem(gridInfo[i]);
                column.Column.Add(item);
        }
        columns.Add(column);
        columnArray=columns;
        SetColumnPositions(columnArray,drawWidth,drawHeight);
        ResetDrawSize();
        SetScrollBars();
        Refresh();
        //DumpMOSData();
}
SurfaceColumnItem SurfaceWindow::ConvertStringToSCItem(wxString src)
{
        SurfaceColumnItem item;
        wxString strLine=src.Trim().Trim(false);
        wxString delim=wxT(" ()\t\r\n");
        long group, index,lv;
        double energy;
        bool hasLeft,hasRight;
        wxString flag;
        wxArrayString tokens=wxStringTokenize(strLine,delim,wxTOKEN_STRTOK);
        if(tokens.GetCount()>=5)
        {
                tokens[0].ToLong(&group);
                tokens[1].ToLong(&index);
                tokens[2].ToDouble(&energy);
                tokens[3].ToLong(&lv);
                //wxMessageBox(wxString::Format(wxT("lv=%d"),lv));
                switch(lv)
                {
                case 2:
                        hasLeft=true;
                        hasRight=true;
                        break;
                case 1:
                        hasLeft=true;
                        hasRight=false;
                        break;
                default:
                        hasLeft=false;
                        hasRight=false;
                        break;
                }
                flag=tokens[4];
                item.leftStr=tokens[0]+wxT("_")+tokens[1];
                item.Index=(int)index;
                item.HasLeft=hasLeft;
                item.HasRight=hasRight;
                item.HasMO=true;
                item.Energy=energy;
                item.Type=flag;
        }
        else
        {
                item.leftStr=tokens[0];
                item.rightStr=tokens[1];
                item.Index=0;
                item.HasLeft=false;
                item.HasRight=false;
                item.HasMO=true;
        }
        return item;
}

void SurfaceWindow::ConvertStringToValue(wxString src,long& group,long& index,double& energy,bool& hasLeft,bool& hasRight,wxString& flag)
{
        wxString strLine=src.Trim().Trim(false);
        wxString delim=wxT(" ()\t\r\n");
        wxArrayString tokens=wxStringTokenize(strLine,delim,wxTOKEN_STRTOK);
        //DisplayArrayString(tokens);
    //wxASSERT_MSG(tokens.GetCount()>=5,strLine);
        if(tokens.GetCount()>=5)
        {
                tokens[0].ToLong(&group);
                //wxMessageBox(wxString::Format(wxT("group=%d"),group));
                tokens[1].ToLong(&index);
                //wxMessageBox(wxString::Format(wxT("index=%d"),index));
                tokens[2].ToDouble(&energy);
                //wxMessageBox(wxString::Format(wxT("energy=%f"),energy));
                long lv;
                tokens[3].ToLong(&lv);
                //wxMessageBox(wxString::Format(wxT("lv=%d"),lv));
                switch(lv)
                {
                case 2:
                        hasLeft=true;
                        hasRight=true;
                        break;
                case 1:
                        hasLeft=true;
                        hasRight=false;
                        break;
                default:
                        hasLeft=false;
                        hasRight=false;
                        break;
                }
                flag=tokens[4];
        }
        else
        {
                group=0;
                index=0;
                energy=0.0;
                hasLeft=true;
                hasRight=true;
                flag=strLine;
        }
}

void SurfaceWindow::GetSelectedRowCol(int &row, int &col)
{
        row=selectedRow;
        col=selectedCol;
}

void SurfaceWindow::SetSelectedRowCol(int crow, int ccol)
{
//        wxMessageBox(wxString::Format(wxT("%d"),crow)+wxT(",")+wxString::Format(wxT("%d"),ccol));
        for(int col=0;col<(int)columnArray.GetCount();col++)
        {
                for(int row=0;row<(int)columnArray[col].Column.GetCount();row++)
                {
                                columnArray[col].Column[row].IsCheck=false;
                }
        }
        if(crow>=0&&ccol>=0){
//        wxMessageBox(wxString::Format(wxT("%d"),(int)columnArray[ccol].Column.GetCount())+wxT(",")+wxString::Format(wxT("%d"),(int)columnArray.GetCount()));
    columnArray[ccol].Column[crow].IsCheck=true;
        selectedRow=crow;
        selectedCol=ccol;
    }
    Refresh();
}

void SurfaceWindow::ClearSurfaceData()
{
        columnArray.Clear();
        Refresh();
}
/////////////////////////////////////////////////////////////////////
//Class SurfacePnl
/////////////////////////////////////////////////////////////////////
enum
{
        SURFACE_PANLE_CHOICE_STYLE,
        SURFACE_PANLE_TEXT_ISO_VALUE,
        SURFACE_PANLE_CHOICE_MO_SET
};

/////////////////////////////////////////////////////////////////////
//event table
/////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(SurfacePnl, wxPanel)
        EVT_CHOICE(SURFACE_PANLE_CHOICE_STYLE,SurfacePnl::OnChoiceStyle)
        EVT_CHOICE(SURFACE_PANLE_CHOICE_MO_SET,SurfacePnl::OnChoiceMOSet)
        EVT_COMMAND_SEL_CHANGED(wxID_ANY,SurfacePnl::OnSelChange)
END_EVENT_TABLE()

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SurfacePnl::SurfacePnl(wxWindow* parent, wxWindowID id, const wxPoint& pos,
            const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name)
{
        CreateGUIControls();
}

SurfacePnl::~SurfacePnl()
{
        if(m_surfaceWin){
                delete m_surfaceWin;
                m_surfaceWin=NULL;}
}

void SurfacePnl::SetSelectRow(int row)
{
    m_surfaceWin->SetSelectedRowCol(row,0);
  //  wxMessageBox(wxT("SetSelectRow Over"));
    wxIntArray sel;
        sel.Add(row);
        sel.Add(0);
        wxSelChangeEvent selEvent(wxEVT_COMMAND_SEL_CHANGED, wxID_ANY);
        selEvent.SetSelect(sel);
        selEvent.SetEventObject(this);
        GetEventHandler()->ProcessEvent(selEvent);

}

void SurfacePnl::CreateGUIControls()
{
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        SetSizer(bsTop);
        wxStaticText* stTile= new wxStaticText(this, wxID_ANY,wxT("Structure/Surface:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

        m_surfaceWin=new SurfaceWindow(this,wxID_ANY,wxDefaultPosition,wxSize(100,150));

        wxFlexGridSizer *pGSDetail= new wxFlexGridSizer(2,2,0,0);
        pGSDetail->AddGrowableCol(1);
        //pGSDetail->AddGrowableCol(3);
        //wxBoxSizer* bsDetail = new wxBoxSizer(wxHORIZONTAL);
/*        wxStaticText* stText1=new wxStaticText(this, wxID_ANY,wxT("Iso Value:"),
                                                                        wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        textCtrl = new wxTextCtrl(this, SURFACE_PANLE_TEXT_ISO_VALUE,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(60, 25));
*/
        wxStaticText* stText2=new wxStaticText(this, SURFACE_PANLE_CHOICE_MO_SET,wxT("MO Set:"),
                                                                        wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        wxArrayString value;//value.Add(wxT("Double Occupied"));value.Add(wxT("ALPHA"));value.Add(wxT("BETA"));
        choiceMOS=new wxChoice(this,SURFACE_PANLE_CHOICE_MO_SET,wxDefaultPosition, wxDefaultSize,value);
        wxStaticText* stText3=new wxStaticText(this, wxID_ANY,wxT("Style:"),
                                                                        wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        wxArrayString value2;value2.Add(wxT("Transparent"));value2.Add(wxT("Solid"));value2.Add(wxT("Mesh"));
        choiceSet=new wxChoice(this,SURFACE_PANLE_CHOICE_STYLE,wxDefaultPosition, wxDefaultSize,value2);
        choiceSet->SetSelection(1);
        //bsDetail->Add(stText,0,wxALL|wxALIGN_RIGHT,5);
        //bsDetail->Add(choice,0,wxALL|wxALIGN_CENTER,5);
        //pGSDetail->Add(stText1,0,wxALL|wxALIGN_LEFT,5);
        //pGSDetail->Add(textCtrl,0,wxALL|wxALIGN_LEFT,5);
        pGSDetail->Add(stText2,0,wxALL|wxALIGN_LEFT,5);
        pGSDetail->Add(choiceMOS,0,wxALL|wxALIGN_LEFT,5);
        pGSDetail->Add(stText3,0,wxALL|wxALIGN_LEFT,5);
        pGSDetail->Add(choiceSet,0,wxALL|wxALIGN_LEFT,5);


        //wxBoxSizer* bsOper = new wxBoxSizer(wxHORIZONTAL);
        //wxButton* btnOK=new wxButton(this, wxID_OK, wxEmptyString, wxPoint(10, 10), wxDefaultSize);
        //wxButton* btnCancel=new wxButton(this, wxID_CANCEL, wxEmptyString, wxPoint(10, 10), wxDefaultSize);
        //bsOper->Add(btnOK, 0,wxALIGN_RIGHT | wxALL, 5);
        //bsOper->Add(btnCancel, 0,wxALIGN_LEFT | wxALL, 5);

        bsTop->Add(stTile,0,wxALL|wxALIGN_LEFT,5);
        bsTop->Add(m_surfaceWin,1,wxALL|wxEXPAND,5);
        //bsTop->Add(bsDetail,0,wxALL|wxALIGN_RIGHT,5);
        bsTop->Add(pGSDetail,0,wxALL|wxALIGN_CENTER,5);
        //bsTop->Add(bsOper,0,wxALL|wxALIGN_CENTER,5);
        Layout();
        curmo=-1;
}


void SurfacePnl::OnChoiceStyle(wxCommandEvent &event)
{
        int surfaceStyle=event.GetSelection();
        CtrlInst* pCtrlInst = MolViewEventHandler::Get()->GetMolView()->GetData()->GetActiveCtrlInst();
        if(pCtrlInst && pCtrlInst->GetSurface()) {
                pCtrlInst->GetSurface()->SetRenderingStyle(surfaceStyle);
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

void SurfacePnl::OnChoiceMOSet(wxCommandEvent &event)
{
        int surfaceFileIndex=event.GetSelection();
        wxArrayString gridInfos=ConstructGridInfosFromDenFile(surfaceFileList[surfaceFileIndex]);
        CtrlInst* pCtrlInst = MolViewEventHandler::Get()->GetMolView()->GetData()->GetActiveCtrlInst();
        //controlDataFileName=surfaceFileList[0];
        m_dataSCA.Clear();
        m_surfaceWin->AppendColumns(gridInfos);
        m_dataSCA=m_surfaceWin->GetColumnData();
        if(pCtrlInst && pCtrlInst->GetSurface()) {
                pCtrlInst->GetSurface()->SetRendering(false);
//                pCtrlInst->GetSurface()->Init(surfaceFileList[surfaceFileIndex]);
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        //pCtrlInst->isShowSurface=true;
        //wxGetApp().GetMainFrame()->RefreshGraphics();
}

void SurfacePnl::OnSelChange(wxSelChangeEvent &event)
{
        int row,col;
        wxIntArray sel=event.GetNewSelect();
        row=sel[0];
        col=sel[1];
        CtrlInst* pCtrlInst = MolViewEventHandler::Get()->GetMolView()->GetData()->GetActiveCtrlInst();
        //wxMessageBox(wxString::Format(wxT("row=%d,col=%d"),row,col));
        if(pCtrlInst && pCtrlInst->GetSurface()) {
                if(row>=0 && col >=0)
            curmo=row;
        if(curmo>=0){
                pCtrlInst->GetSurface()->LoadGrid(curmo,0);
                pCtrlInst->GetSurface()->SetCurmo(curmo);
                pCtrlInst->GetSurface()->SetRendering(true);
                }
//                else
//                {
//                        pCtrlInst->GetSurface()->SetRendering(false);
//                }
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        MolViewEventHandler::Get()->GetMolView()->SetCurrentMO(curmo);
}

void SurfacePnl::ClearData()
{
        m_dataSCA.Clear();
        m_surfaceWin->ClearSurfaceData();
//        CtrlInst * pCtrlInst = MolViewEventHandler::Get()->GetMolView()->GetData()->GetActiveCtrlInst();
        //wxMessageBox(wxString::Format(wxT("row=%d,col=%d"),row,col));
//        pCtrlInst->isShowSurface=false;
        Refresh();
}


//DEL void SurfacePnl::AppendDenFile(wxString denFileName)
//DEL {
//DEL         wxArrayString gridInfos=ConstructGridInfosFromDenFile(denFileName);
//DEL         //controlDataFileName=denFileName;
//DEL         m_dataSCA.Clear();
//DEL         m_surfaceWin->AppendColumns(gridInfos);
//DEL         m_dataSCA=m_surfaceWin->GetColumnData();
//DEL         Refresh();
//DEL }


void SurfacePnl::AppendDenFile(wxArrayString denFileList)
{
//    wxMessageBox(wxT("AppendDenFile"));
        surfaceFileList=denFileList;
        CtrlInst * pCtrlInst = MolViewEventHandler::Get()->GetMolView()->GetData()->GetActiveCtrlInst();
        if(surfaceFileList.GetCount()<=0)
                return;
        SetMoSetChoices(surfaceFileList.GetCount());
        wxArrayString gridInfos=ConstructGridInfosFromDenFile(surfaceFileList[0]);
        //controlDataFileName=surfaceFileList[0];
        m_dataSCA.Clear();
        m_surfaceWin->AppendColumns(gridInfos);
        m_dataSCA=m_surfaceWin->GetColumnData();
        if(pCtrlInst->GetSurface() == NULL)
        {
                //wxMessageBox(wxT("InitSurfaceData"));
                //pCtrlInst->InitSurfaceData(surfaceFileList[0]);
                if(wxFileExists(surfaceFileList[0])) {
                        SurfaceData* pSurfaceData = new SurfaceData;
                        pSurfaceData->Init(surfaceFileList[0]);
                        pSurfaceData->LoadGridHeader();
                        pSurfaceData->AllSurfaceInit();
                        pCtrlInst->SetSurface(pSurfaceData);
                }
        }else{
                pCtrlInst->GetSurface()->SetRendering(false);
        }

        Refresh();
}

void SurfacePnl::SetMoSetChoices(int numOfChoice)
{
        choiceMOS->Clear();
        wxArrayString items;
        if(numOfChoice==1){
                items.Add(wxT("Double Occupied"));
        }else if(numOfChoice==2){
                items.Add(wxT("ALPHA"));
                items.Add(wxT("BETA"));
        }
        /*
        for(unsigned i=0; i< items.GetCount(); i++){
                wxMessageBox(items[i]);
        }*/
        choiceMOS->Append(items);
        choiceMOS->SetSelection(0);
}
