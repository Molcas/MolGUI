/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MAC__
#include <malloc.h>
#endif
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <ctype.h>
#include "isosurfile.h"
#include "pvstring.h"

#define MAXMODEL 200

static int currentModelData_model [MAXMODEL];
static int currentModelData_number[MAXMODEL];
static double currentModelData_iso[MAXMODEL];
static int currentModelData_twopic[MAXMODEL];

static int memModels;
static int ORBoff;

static int SecondTime=0;
static double (*xyz)[3];
static char (*atomNames)[4];
static int *atomNums;
static int natoms;
static int natomsReal;
static int isBinary;
static int isPacked;
static int PackingFlags;
static int *outsym; //gv7
//static Grid_t *currentGridData = NULL;

#define MIN(x,y) (x<y? x : y)
#define MAX(x,y) (x>y? x : y)
#define MAXCHARS 500
#define R529 0.529177249f

#define MAXPATH 256

static wxString FilePath;
static wxString FilePath2;
static wxString SFilePath;

//static char TypeChange[2048]; //gv6
//gv7 begin
//static int  iTypeChange[1024];
//static int  oTypeChange[1024];
//static char cTypeChange[1024];
static int  nTypeChange;
//gv7 end

//static char ThisTitle[32];


static int RefDen;
/* Packing parameters */
struct packing_params_t {
  int range; /* really, half range */
  int nbytes; /* bytes per packed value */

  double xlimit[4];
  double xdelta[3];
  int ylimit[4];
  int ydelta[3];

  int ileft,iright;
};

typedef unsigned char byte_t;

//void setTitle(int);
//void setNextType(void); //gv7
//void setType(int); //gv7

static wxFFile * OpenGridFile(wxString fname, int is_primary,int);
static int ReadBlock(double* buf, int np, wxFFile* fp);
static int getPackingParams(const char* line, struct packing_params_t* prm);
static void unpackData(double* dest,
                       byte_t* packed_data, unsigned int packed_val,
                       int ndata,
                       const struct packing_params_t* prm);

//static int LoadMolden(FILE* ); //gv7
void
forget_models(void)
{
memModels=0;
return;
}


void
iso_InitGridInformation (Grid_t *currentGridData)
{
  /* Free the grid storage structure */
  if (currentGridData)
    {
      if (currentGridData->values)
        free (currentGridData->values);
      if (currentGridData->pos)
        free (currentGridData->pos);
  //    if (currentGridData->title)
    //    free (currentGridData->title);
      if (currentGridData->iType)
        free (currentGridData->iType);
      if (currentGridData->dependence)
        free (currentGridData->dependence);
      free (currentGridData);
      currentGridData = NULL;
    }
  memModels = 0;
 // if (xyz)
 //   free (xyz);

}

wxString
iso_CurrentGridName (int is_primary,Grid_t *currentGridData)
{
  //char* ptr;
        int pos;
  if (!currentGridData)
    {
      //strcpy (FilePath, "Undefined");
      FilePath=wxT("Undefined");
      return FilePath;
    }

  if (is_primary)
    {
          //ptr=strrchr(FilePath,'/');
      pos = FilePath.Find(_T('/'),true);
      //if (ptr == NULL )
      if (pos == wxNOT_FOUND )
      //ptr = FilePath;
      SFilePath = FilePath;
      else
                //ptr++
        SFilePath = FilePath.Mid(pos+1);
    }
  else
    {
      pos = FilePath2.Find(_T('/'),true);
      //ptr = strrchr (FilePath2, '/');
      //if (ptr == NULL)
      if (pos == wxNOT_FOUND )
        //ptr = FilePath2;
                SFilePath=FilePath2;
      else
        //ptr++;
        SFilePath = FilePath2.Mid(pos+1);
    }

  //strcpy (SFilePath, ptr);
  return SFilePath;

}

int
iso_current_file (Grid_t *currentGridData)
{
  if (currentGridData)
    {
      return 1;
    }
  return 0;
}

int
iso_CurrentGridSize (int *a, int *b, int *c,Grid_t * currentGridData)
{
  if (currentGridData)
    {
      *a = currentGridData->npt[0];
      *b = currentGridData->npt[1];
      *c = currentGridData->npt[2];
      return 1;
    }
  return 0;
}

/*
int
init_GridTitles (char *first)
{
  char *ptr;
  if (currentGridData)
    {
      strncpy (first, currentGridData->title, MAXSTRTITLE);
      ptr = strchr (first, ',');
      if (ptr != NULL)
        *ptr = 0;
      return 1;
    }
  return 0;
}
*/
/* char *
   aListTitles (int Reverse_order)
{


  if (currentGridData)
    {

     if(Reverse_order)
     {
      instr=(char *) xmalloc(sizeof(char)*(1+strlen( ));
      outstr=(char *) xmalloc(sizeof(char)*(1+strlen(currentGridData->title) ));
      strcpy(instr,currentGridData->title);
       outstr[0]=0;
       while( (tempstr=strrchr(instr,','))!=NULL)
       {
         strcat(outstr,tempstr+1);
         *(tempstr)=0;
         strcatc(outstr,',');
       }
       strcat(outstr,instr);
       strcpy(currentGridData->title,outstr);
       free(instr);
       free(outstr);

     }

      return currentGridData->title;
    }
  return NULL;
}
*/
int
iso_CurrentGridIsod (int i, double *iso_threshold_value,Grid_t *currentGridData)
{
  int is;
  double xa, xb, a;
  is=i/5;
  if(is>19) is=19;
  if(is<0) is=0;
  xa=currentGridData->isod[is];
  xb=currentGridData->isod[is+1];
  a=(i-5*is)/5.0;
  *iso_threshold_value=xa*(1-a)+xb*a;
  return 1;
}

int
iso_CurrentGridAxesVector (double **xvec, double **yvec, double **zvec,Grid_t *currentGridData)
{
  if (currentGridData)
    {
      *xvec = currentGridData->axisvec1;
      *yvec = currentGridData->axisvec2;
      *zvec = currentGridData->axisvec3;
      return 1;
    }
  return 0;
}

int
iso_CurrentGridOrigin (double *origina, double *originb, double *originc,Grid_t *currentGridData)
{
  if (currentGridData)
    {
      *origina = currentGridData->origin[0];
      *originb = currentGridData->origin[1];
      *originc = currentGridData->origin[2];
      return 1;
    }
  return 0;
}

int
iso_CurrentGridLimits (double *dmin, double *dmax, double *guess,Grid_t *currentGridData)
{
  if (currentGridData)
    {
      *dmin = currentGridData->minval;
      *dmax = currentGridData->maxval;
      *guess = currentGridData->guess;
      return 1;
    }
  return 0;
}

int
iso_CurrentGridData (double **data,Grid_t *currentGridData)
{
  if (currentGridData)
    {
      *data = currentGridData->values;
      return 1;
    }
  return 0;
}

int
iso_LoadGrid (wxString filepath,int Current_Grid, int is_diff, int AutoIso, double FixedIso, int ShowProgress, int is_coord,Grid_t *currentGridData)
{
  wxFFile *fp, *fp2=0;
  int i, bReadOK;
  int /*j,*/ x, y, z, iBlock;
  char line[MAXCHARS];
  int status;
  float ind;
  double g;
  int itek=0;
/*  int guess[10]; */
  double *readbuffer;
  double *readbuffer2=0;
  int Current_Grid2 = -1;
  int itime = 1;
  double r1 = 0, r2 = 0;
  int sflag = 0;
  double bestiso =0.1;
  int np;
  int ptotal; //gv 7
  bReadOK=0;
  if(is_coord ==2 ) return 1;
   fp=new wxFFile();
   FilePath=filepath;
  /* Try opening the file */
  //if (isBinary == 0)
          fp->Open( FilePath, wxT("rb"));
  //else
  //   fp = fopen (FilePath, "rb");

  if (!fp)
    {
      puts (FilePath.mb_str());

      MyString::msU_MSG_ArgS (const_cast<char*>("Error during opening grid file\nAre you sure that the file still exists?"));
      MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
      Current_Grid = 0;
      return 0;
    }
  else
    {
      if (Current_Grid > currentGridData->ngrid || Current_Grid < 0)
        {
        if(Current_Grid==-1) return 0;
          MyString::msU_MSG_ArgS (const_cast<char*>("Grid Number is out of range\nDo you use correct Grid datafile?"));
          MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
        /*  Current_Grid = 0; */
          return 0;
        }
      if (is_diff)
        {
          //if (isBinary == 0)
         //     fp2->Open (wxString::Format(wxT("%s"), FilePath2), wxT("r"));
                 fp2->Open(FilePath2, wxT("r"));
          //else
           // fp2 = fopen (FilePath2, "rb");
          if (!fp2)
            {
              MyString::msU_MSG_ArgS (const_cast<char*>("Error during opening grid file\nAre you sure that the file still exists?"));
              MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
    /*          Current_Grid = 0; */
              return 0;
            }

          Current_Grid2 = currentGridData->dependence[Current_Grid];

          if (Current_Grid2 == -1)
            {
              MyString::msU_MSG_ArgS (const_cast<char*>("There is no corresponding grid!\n"));
              MyString::msU_MSG_Display (msU_MSG_CONFIRM,const_cast<char*>( "Warn_Msg"), &status);
              return 0;
            }
        }
/* take a title*/

//      setTitle(Current_Grid);
/* we DO NOT NEED memory reallocation for grid values. We do it once at the beginning! */

      if (is_diff)
        readbuffer2 =
          (double *) calloc (currentGridData->block_size, sizeof (double));

      readbuffer =
        (double *) calloc (currentGridData->block_size, sizeof (double));

    metka:

      iBlock = 0;
      x = y = z = 0;
      ptotal = currentGridData->npt[0] * currentGridData->npt[1] *
        currentGridData->npt[2];

      for (i = 0; i < ptotal; i++, x++)       /* loop thro' file */
        {
          if (i == currentGridData->block_size * iBlock)
            {
/* show progress indicator */

              ind = i * 1.0 / currentGridData->npoints;
                          if(ShowProgress)
                           {
                                                 MyString:: msU_INT_ReportProgress ((int)ind);
                                                  if (MyString::msU_INT_RequestInterrupt () != 0)
                                                        {
                                                          MyString::msU_MSG_ArgS (const_cast<char*>(" The data wasn't read correctly! "));
                                                          MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
                                                        return 0;
                                                        }
                           }
              fp->Seek(currentGridData->pos[Current_Grid *currentGridData->nBlock + iBlock]);


              if (is_diff)
                fp2->Seek (currentGridData->pos2[Current_Grid2 * currentGridData->nBlock +iBlock]);
              iBlock++;

              np = currentGridData->block_size;
              if (iBlock == currentGridData->nBlock)
                np = currentGridData->npoints -
                  currentGridData->block_size * (currentGridData->nBlock -1);
                          //gv 7 begin
        if(currentGridData->isCutOff)
        {
          /* calculate number of uncutted data in this block */
            int ic=0, ii;
            for(ii=0; ii<np; ii++)
             {
               if(currentGridData->ByteCutOff[ii+i]==1) ic++;
             }
              ReadBlock(readbuffer,ic,fp);

        }
        else
        {
              ReadBlock(readbuffer,np,fp);
        }

                //          fgetline(line,MAXCHARS,fp);
              if (is_diff)
                {
                  fgetline(readbuffer2,np,fp2);
                }
              itek = 0;



            }
          if (x == currentGridData->npt[0])
            {
              x = 0;
              y++;
              if (y == currentGridData->npt[1])
                {
                  y = 0;
                  z++;
                }
            }
       //gv7 begin
                  if (!is_diff)
               {
                if(currentGridData->isCutOff && currentGridData->ByteCutOff[i]==0)
                  {
                  currentGridData->values[i]=0.0;
                  itek--;
                  }
                else
                  currentGridData->values[i] = readbuffer[itek];
                }
              else
                {
                  if (itime == 1)
                    {
                      r1 = r1 + fabs (readbuffer[itek] - readbuffer2[itek]);
                      r2 = r2 + fabs (readbuffer[itek] + readbuffer2[itek]);
                    }
                if(currentGridData->isCutOff && currentGridData->ByteCutOff[i]==0)
                  {
                  currentGridData->values[i] = 0.0;
                  }
                else
                  {
                  currentGridData->values[i] =
                    readbuffer[itek] - readbuffer2[itek] * itime;
                  }
                  if (!sflag && readbuffer2[itek] < -0.02)
                    sflag = 1;

                }
              itek++;
  //gv7 end

          /* For first point, set the min&max values;
             for subsequent points, conditionally set.
           */
          if (i == 0)
            {
              currentGridData->minval = 10000.0;
              currentGridData->maxval = -10000.0;
            }
          else
            {
              /* let's cut off huge values */
              if (currentGridData->values[i] > 2)
                currentGridData->values[i] = 2;

              currentGridData->minval =
                MIN (currentGridData->minval, currentGridData->values[i]);
              currentGridData->maxval =
                MAX (currentGridData->maxval, currentGridData->values[i]);
            }
        }                       /* endof loop */

/* extra check that cutted information is correctly read
   if(currentGridData->isCuOff)
   {
      for (i = 0; i < currentGridData->npoints; i++)
      {
         if(currentGridData->values[i] != 0.0 && currentGridData->ByteCutOff[i]==0)
         printf("caboom\n");
      }
   }
*/

      if (is_diff
          && currentGridData->maxval - currentGridData->minval < 0.00001)
        {
          MyString::msU_MSG_ArgS (const_cast<char*>(" The data files seems to be identical! "));
          MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("Warn_Msg"), &status);
          return 0;
        }

/* printf("%f %f\n",r1,r2); */

      if (is_diff && sflag && r2 - 0.01 < r1 && itime == 1)
        {
          itime = -1;
          goto metka;
        }

      bReadOK = 1;
      /* try to find best initial guess for grid */

        {
        int iguess;
        double cutoff[21];
        double gguess[21];
                double mysmall;
                double mybig;
        double searchval;
        int isd=0;
        if(currentGridData->title.Find(wxT("Dens"))!=wxNOT_FOUND) isd=1;

          g = MAX(fabs(currentGridData->maxval),fabs(currentGridData->minval)) / 20;
          for (iguess = 0; iguess < 21; iguess++)
            {
            gguess[iguess] = 0;
            cutoff[iguess] = (iguess-.001)*g ;
            if(iguess==0)cutoff[0]=0;
          for (i = 0; i < currentGridData->npoints; i++)

            if(fabs(currentGridData->values[i])>cutoff[iguess])
              {
              if(isd)
              gguess[iguess]+=currentGridData->values[i];
              else
              gguess[iguess]+=currentGridData->values[i]*currentGridData->values[i];
              }
            }
           for(i=1; i<21; i++)
           {
           searchval=gguess[0]*(i/20.) ;
          for (iguess = 0; iguess < 20; iguess++)
            {
            if(gguess[iguess]>=searchval && searchval>gguess[iguess+1])
              {
              mybig = gguess[iguess] ;
              mysmall = gguess[iguess+1] ;
              if( fabs(mybig-mysmall) < 1e-10)
                bestiso = .5*(cutoff[iguess]+cutoff[iguess+1]) ;
              else
              bestiso = ((searchval-mysmall)/(mybig-mysmall))*(cutoff[iguess+1]-cutoff[iguess])+cutoff[iguess] ;
              }
             }
              if( bestiso > currentGridData->maxval)
                bestiso = -bestiso ;
              currentGridData->isod[i]=bestiso;


            }

      }

      if (AutoIso || is_diff)
      {
        currentGridData->guess =currentGridData->isod[14];
          sprintf (line, "%7.0le", currentGridData->guess);
          sscanf (line, "%lf", &currentGridData->guess);
        }
      else
        currentGridData->guess = FixedIso;
      /* close the file and fly away */
      fp->Close();
      free (readbuffer);
      if (is_diff)
        {
          free (readbuffer2);
          fp2->Close();
        }
    }

  return bReadOK;
}

int
iso_LoadGridHeader (wxString filepath, int is_primary, int is_coord, float translate,int *isFreq,Grid_t *currentGridData)
{
  wxFFile *fp;
  char  *ptr, *ptr1;
 // int iptr,iptr1=0;
  int i, bReadOK = 0;
  int ptotal;
  int iGrid, j;
  int iBlock, cBlock;
  char line[MAXCHARS];
  int status;
  float ind;
  int ntemp;
//  char DepLabel[10];
  //int ii;
  int isel;
  float myR529;
  int nCooInFile=1;
  //gv7 begin
  int i0,i1,i2, ie0,ie1,ie2,icount, ishow, iaia;
  double pp[3];
//gv7 end
/*** a.g.: I can't see this <censored> error-checking code any more!!! :-E
     C-language certainly lacks support of exceptions. :(
     Let's define a macro as a workaround...
***/
#define EC_abfgets(_buf_,_count_,_fd_) \
if (!abfgets(isBinary,_buf_,_count_,_fd_)) { \
  printf("DEBUG: read error at %d\n",__LINE__); \
  read_error(); \
  _fd_->Close(); \
  return 0; \
}
/** end of #define **/

   myR529=R529;
   if(!is_primary) SecondTime++; else SecondTime=0;

   /* open file, read header, fill global flags: */
   fp=OpenGridFile(filepath,is_primary,is_coord);

   if (fp) {
     /* Free the old structure if set up */
 //    if (is_primary)
 //    iso_InitGridInformation (currentGridData);

     /* Make a new one */
  //   if (is_primary)
  //      currentGridData = (Grid_t *) calloc (1, sizeof (Grid_t));

    *isFreq=0;
    currentGridData->is_coord=is_coord;
     if (is_coord == 2) {
                        strcpy (currentGridData->Gtitle, const_cast<char*>("Structure"));
                        }

     else
                         {

     EC_abfgets(line, MAXCHARS, fp);
     if (is_primary) {
       strcpy (currentGridData->Gtitle, line);
       MyString::mydiet (currentGridData->Gtitle);
     }
                         }

   //gv7 begin
         if (strstr(line,"[MOLDEN") !=NULL )
     {
     /* for the moment we assume: that next line is one of : GEOCONV, FREQ, ATOMS */
  //    LoadMolden(fp);
      return 1;
     }
//gv7 end
         natomsReal=0;
     MyString::msS_STR_TrimToken (line, const_cast<char*>("="));
     sscanf (line, "%d", &natomsReal);

     while(natomsReal == 0)  {
           EC_abfgets(line, MAXCHARS, fp);
           MyString::msS_STR_TrimToken (line, const_cast<char*>("="));
       sscanf (line, "%d", &natomsReal);
     }
     if (is_coord == 2) {
          EC_abfgets(line, MAXCHARS, fp);
/*  analyze comment line*/
     for (i=0; i<4; i++) currentGridData->sel[i]=-1;
          MyString::strlwr1(line);
//          myR529=1.0;
//          if (strstr (line,"bohr") != NULL) myR529=R529;
//          if (strstr (line,"a.u.") != NULL) myR529=R529;
/* prepear to freq or structure */
          ptr=strstr(line,"freq");
          if(ptr !=NULL) { nCooInFile=2; *isFreq=1; }
          ptr=strstr(line,"select");
          if(ptr != NULL)
            {
              MyString::mydiet(ptr);
              ptr=ptr+7;
              ptr1=strchr(ptr,' ');
              if(ptr1 != NULL) *ptr1=0;
              isel=MyString::mycount(ptr,'-')+1;

              MyString::mysubtranc(ptr,const_cast<char*>("-"),const_cast<char*>(" "));
              if(isel > 4) isel=4;
              switch (isel)
              {
                 case 1: sscanf(ptr,"%d", &currentGridData->sel[0]); break;
                 case 2: sscanf(ptr,"%d %d", &currentGridData->sel[0],&currentGridData->sel[1]); break;
                 case 3: sscanf(ptr,"%d %d %d", &currentGridData->sel[0],&currentGridData->sel[1],&currentGridData->sel[2]); break;
                 case 4: sscanf(ptr,"%d %d %d %d", &currentGridData->sel[0],&currentGridData->sel[1],&currentGridData->sel[2],&currentGridData->sel[3]); break;


              }


            }
       }
     nTypeChange=0;
     natoms=natomsReal+ExtraAtoms;
     xyz = (double (*)[3]) MyString::msS_MEM_Alloc (sizeof (double) * 3 * natoms *nCooInFile);
     atomNames = (char (*)[4]) MyString::msS_MEM_Alloc (sizeof (char) * 4 * natoms);
     atomNums = (int (*)) MyString::msS_MEM_Alloc (sizeof (int)  * natoms);
         outsym = (int (*)) MyString::xmalloc (sizeof (int)  * natoms); //gv7

      for (i = 0; i < natomsReal; i++) {
        EC_abfgets(line, MAXCHARS, fp);
       MyString::myexchange(line,'\t',' ');
       MyString::mydiet(line);
        for (j = 0; j < 4; j++) {
          if (isalpha (line[j]))
            atomNames[i][j] = line[j];
          else {
            atomNames[i][j] = 0;
            break;
          }
        }
        atomNames[i][3] = 0;
        MyString::msS_STR_TrimToken (line, const_cast<char*>(" "));
        sscanf (line, "%lf %lf %lf", &xyz[i][0], &xyz[i][1],
                &xyz[i][2]);
        xyz[i][0] *= myR529;
        xyz[i][1] *= myR529;
        xyz[i][2] *= myR529;
       if (is_coord >0 && *isFreq==0 )
        {
        xyz[i][0] += translate;

        }
       /* printf("=>%s %lf %lf %lf\n", atomNames[i],xyz[i][0],xyz[i][1],xyz[i][2]); */
      }

      for (i=natomsReal; i<natomsReal*nCooInFile; i++)
      {
        EC_abfgets(line, MAXCHARS, fp);
        sscanf (line, "%lf %lf %lf", &xyz[i][0], &xyz[i][1],
                &xyz[i][2]);
        xyz[i][0] *= myR529;
        xyz[i][1] *= myR529;
        xyz[i][2] *= myR529;

       if (is_coord >0)
        {
        xyz[i][0] += translate;

        }

      }
/* Calculate Nums  */


     for (i=0; i<natomsReal; i++)
     {
      atomNums[i]=1;
          outsym[i]=1;//gv7
      for(j=0; j<i; j++)
      {
        if(strcmp(atomNames[i],atomNames[j])==0)
               {
               atomNums[i]=atomNums[i]+1;
               }
      }
     }


     if (is_coord == 2) {
    /* just in case place some data  */
           currentGridData->nmo=1;
           currentGridData->ngrid=1;
           currentGridData->npoints=1;
           currentGridData->ptotal=1; //gv7
           currentGridData->block_size=1;
           currentGridData->nBlock=1;
           currentGridData->npt[0]=0;
           currentGridData->npt[1]=0;
           currentGridData->npt[2]=0;
    //   currentGridData->title =;
           currentGridData->title.Append(wxT("- NONE -,"));
    fp->Close();
    return 1;
                        }

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line, const_cast<char*>("="));
      if (is_primary)
        sscanf (line, "%d", &(currentGridData->nmo));
      else
        sscanf (line, "%d", &(currentGridData->nmo2));

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>( "="));
      if (is_primary)
        sscanf (line, "%d", &(currentGridData->ngrid));
      else
        sscanf (line, "%d", &(currentGridData->ngrid2));

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>( "="));
      if (is_primary)
        sscanf (line, "%d", &(currentGridData->npoints));
      else {
        sscanf (line, "%d", &ntemp);
        if (ntemp - currentGridData->npoints) {
          read2_error ();
         fp->Close();
          return 0;
        }
      }

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line, const_cast<char*>("="));
      if (is_primary)
        sscanf (line, "%d", &(currentGridData->block_size));
      else {
        sscanf (line, "%d", &ntemp);
        if (ntemp - currentGridData->block_size) {
          read2_error ();
          fp->Close();
          return 0;
        }
      }


      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line, const_cast<char*>("="));
      if (is_primary)
        sscanf (line, "%d", &(currentGridData->nBlock));
      else {
        sscanf (line, "%d", &ntemp);
        if (ntemp - currentGridData->nBlock) {
          read2_error ();
          fp->Close();
          return 0;
        }
      }

/* well, from now we don't performed checking of secondary grid. Viva GIGO! */
//gv7 begin
          EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>( "="));
      if (is_primary)
        sscanf (line, "%d", &(currentGridData->isCutOff));
      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
      if (is_primary)
        sscanf (line, "%lf", &(currentGridData->CutOff));

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
      if (is_primary)
        sscanf (line, "%d", &(currentGridData->nPointsCutOff));
//gv7 end
      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
      if (is_primary)
        sscanf (line, "%d %d %d", &(currentGridData->npt[0]),
                &(currentGridData->npt[1]), &(currentGridData->npt[2]));

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
      if (is_primary) {
        sscanf (line, "%lf %lf %lf", &(currentGridData->origin[0]),
                &(currentGridData->origin[1]),
                &(currentGridData->origin[2]));
        currentGridData->origin[0] *= R529;
        currentGridData->origin[1] *= R529;
       currentGridData->origin[2] *= R529;
      }

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
      if (is_primary) {
        sscanf (line, "%lf %lf %lf", &(currentGridData->axisvec1[0]),
                &(currentGridData->axisvec1[1]),
                &(currentGridData->axisvec1[2]));
        currentGridData->axisvec1[0] *= R529;
        currentGridData->axisvec1[1] *= R529;
        currentGridData->axisvec1[2] *= R529;
      }


      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
      if (is_primary) {
        sscanf (line, "%lf %lf %lf", &(currentGridData->axisvec2[0]),
                &(currentGridData->axisvec2[1]),
                &(currentGridData->axisvec2[2]));
        currentGridData->axisvec2[0] *= R529;
        currentGridData->axisvec2[1] *= R529;
        currentGridData->axisvec2[2] *= R529;
      }//error:

      EC_abfgets(line, MAXCHARS, fp);
      MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
      if (is_primary) {
        sscanf (line, "%lf %lf %lf", &(currentGridData->axisvec3[0]),
                &(currentGridData->axisvec3[1]),
                &(currentGridData->axisvec3[2]));
        currentGridData->axisvec3[0] *= R529;
       currentGridData->axisvec3[1] *= R529;
        currentGridData->axisvec3[2] *= R529;
      }

//gv7 begin
/* if iscutoff set we have to allocate and set ByteCutOff */
      if(currentGridData->isCutOff)
        {
          ptotal=(currentGridData->npt[0]+1)*(currentGridData->npt[1]+1)*(currentGridData->npt[2]+1);
          currentGridData->ByteCutOff = (int (*)) MyString::xmalloc (sizeof (int)  * ptotal);
          ie0=currentGridData->npt[0]+1;
          ie1=currentGridData->npt[1]+1;
          ie2=currentGridData->npt[2]+1;
          icount=0;
          for (i0=0; i0<ie0; i0++)
          {
          for (i1=0; i1<ie1; i1++)
          {
          for (i2=0; i2<ie2; i2++)
          {
            pp[0]=(currentGridData->origin[0]+currentGridData->axisvec1[0]*i0/(ie0-1)+currentGridData->axisvec2[0]*i1/(ie1-1)+currentGridData->axisvec3[0]*i2/(ie2-1));
            pp[1]=(currentGridData->origin[1]+currentGridData->axisvec1[1]*i0/(ie0-1)+currentGridData->axisvec2[1]*i1/(ie1-1)+currentGridData->axisvec3[1]*i2/(ie2-1));
            pp[2]=(currentGridData->origin[2]+currentGridData->axisvec1[2]*i0/(ie0-1)+currentGridData->axisvec2[2]*i1/(ie1-1)+currentGridData->axisvec3[2]*i2/(ie2-1));
          ishow=0;
          for(i=0; i<natomsReal; i++)
          {
            iaia=0;
            if(fabs(xyz[i][0]-pp[0]) < currentGridData->CutOff) iaia++;
            if(fabs(xyz[i][1]-pp[1]) < currentGridData->CutOff) iaia++;
            if(fabs(xyz[i][2]-pp[2]) < currentGridData->CutOff) iaia++;

            if(iaia==3) ishow=1;
          }
          if(ishow==1) currentGridData->ByteCutOff[icount]=1; else currentGridData->ByteCutOff[icount]=0;
/*          printf("%d",currentGridData->ByteCutOff[icount]); */
         icount++;
          }}}

/*
          icc=0;
          for(i=0; i<ptotal; i++)
               if(currentGridData->ByteCutOff[i]==1) icc++;
            printf("ic=  %d \n",icc);
*/
        }

//gv7 end
      if (is_primary)
        ntemp = currentGridData->ngrid;
      else
        ntemp = currentGridData->ngrid2;

      if (ntemp == 0)
        {
          MyString::msU_MSG_ArgS (const_cast<char*>(" Grid file doesn't contain any data!"));
          MyString::msU_MSG_Display (msU_MSG_CONFIRM,const_cast<char*>( "ERR_Msg"), &status);
          fp->Close();
          return 0;

        }

      /* convert interval total to point total */
      if (is_primary)
        for (i = 0; i < 3; i++)
          currentGridData->npt[i]++;

      /* calculate the number of points to be read */
      ptotal = currentGridData->npt[0] * currentGridData->npt[1] *
        currentGridData->npt[2];

      if (ptotal - currentGridData->npoints)
        {
//          fprintf(stderr,"DEBUG: ptotal=%d npoints=%d\n",ptotal,currentGridData->npoints);
          MyString::msU_MSG_ArgS (const_cast<char*>(" The internal structure of file is broken:! "));
          MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
          fp->Close();
          return 0;
        }
      if (is_primary)
        {
          currentGridData->pos =
            (long *) calloc (currentGridData->nBlock * currentGridData->ngrid,
                             sizeof (long));

                  //wxString::wxString(currentGridData->title);
                          // (char *) calloc (MAXSTRTITLE * currentGridData->ngrid + 10,sizeof (char));

          currentGridData->iType =
            (char *) calloc ( currentGridData->ngrid,
                             sizeof (char));

          currentGridData->dependence =
            (int *) calloc (currentGridData->ngrid, sizeof (int));

          memModels = 0;


          currentGridData->values =
            (double *) calloc (ptotal, sizeof (double));
        }
      else
        {
          currentGridData->pos2 =
            (long *) calloc (currentGridData->nBlock *
                             currentGridData->ngrid2, sizeof (long));


/*
        if(currentGridData->ngrid==currentGridData->ngrid2
           && currentGridData->nmo==currentGridData->nmo2)
        {
        for(i=0; i<currentGridData->ngrid; i++)
         currentGridData->dependence[i]=i;
        }
        else
        {
        for(i=0; i<currentGridData->ngrid; i++)
         currentGridData->dependence[i]=-1;
        }
 */

/*
 *     printf(">>%d %d %d %d\n", currentGridData->ngrid, currentGridData->ngrid2,
 *        currentGridData->nmo,currentGridData->nmo2);
 */
        }


      if (is_primary)
        {
          //strcpy (currentGridData->title, "- NONE -,");
         // currentGridData->title.Empty();
                  currentGridData->title=wxT("- NONE -,");
                  RefDen = -1;
          for (i = 0; i < currentGridData->ngrid; i++)
            {
              currentGridData->dependence[i] = -1;
              EC_abfgets(line, MAXCHARS, fp);
              MyString::msS_STR_TrimToken (line,const_cast<char*>("="));
              if (strstr (line, "Dens") != NULL)
                RefDen = i;

              ptr = strchr (line, '\n');
              if (ptr != NULL)
                *ptr = 0;
              //strcat (currentGridData->title, line);
              currentGridData->title.Append(wxString::Format(wxT("%s"), line));
                          if (i < currentGridData->ngrid - 1)
                //strcat (currentGridData->title, ",");
                                currentGridData->title.Append(wxT(","));
            }
        }
      else
        {
   /*       for (i = 0; i < currentGridData->ngrid2; i++)
            {
              EC_abfgets(line, MAXCHARS, fp);

              msS_STR_TrimToken (line,const_cast<char*>("="));
              strncpy (DepLabel, line, 10);
              DepLabel[9] = 0;
              //ptr = strstr (currentGridData->title, DepLabel);
              iptr=currentGridData->title->Find(wxT(DepLabel));
                          if (iptr)
                {
                  *(ptr) = '*';
                  ii = ptr - currentGridData->title;
                  ii = mycount2 (currentGridData->title, ',', ii);


                  currentGridData->dependence[ii - 1] = i;
                }

              if (strstr (line, "Dens") != NULL)
                {
                  currentGridData->dependence[RefDen] = i;
                }

            }
                  */

        }

      cBlock = currentGridData->block_size;

      for (iBlock = 0; iBlock < currentGridData->nBlock; iBlock++) {
        /* Cerius2 visualisation bells&whistles */
        ind = iBlock * 1.0 / currentGridData->nBlock;

        MyString::msU_INT_ReportStatus (const_cast<char*>("READ_FILE"), 1, &status);
        MyString::msU_INT_ReportStatus (const_cast<char*>("READ_FILE2"), 2, &status);

        MyString::msU_INT_ReportProgress ((int)ind);
        if (MyString::msU_INT_RequestInterrupt () != 0)
        {
           MyString::msU_MSG_ArgS (const_cast<char*>(" Header of grid wasn't read correctly! "));
           MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);

           fp->Close();
           return 0;
        };


         /* Correct cBlock for the last block */
         if (iBlock == currentGridData->nBlock - 1)
                         //gv7 begin
                 {
                   if(currentGridData->isCutOff==1)
                         {
                                cBlock =
                                  currentGridData->nPointsCutOff - (currentGridData->nBlock -
                                                                                          1) * currentGridData->block_size;
                         }
                         else
                         {
                                cBlock =
                                  currentGridData->npoints - (currentGridData->nBlock -
                                                                                          1) * currentGridData->block_size;
                                 }

                 }//gv7end
 //           cBlock =
   //           currentGridData->npoints - (currentGridData->nBlock -
     //                                     1) * currentGridData->block_size;

         if (is_primary)
           ntemp = currentGridData->ngrid;
         else
           ntemp = currentGridData->ngrid2;



         for (iGrid = 0; iGrid < ntemp; iGrid++)  { /* each grid inside block...*/
           /* get title: */
           EC_abfgets(line, MAXCHARS, fp);
           if (line[0] != 'T') {
            fp->Close();
             return 0;
           }

           /* store the block offset in the file: */
           if (is_primary) {
             currentGridData->pos[iGrid * currentGridData->nBlock +
                                   iBlock] = fp->Tell();
           } else {
             currentGridData->pos2[iGrid * currentGridData->nBlock +
                                   iBlock] = fp->Tell();
           }

           /* skip to next block -- fake-read cBlock values */
           if (!ReadBlock(0, cBlock, fp)) {
//             fprintf(stderr,"DEBUG: fail to skip %d values\n",cBlock);
             read_error();
             fp->Close();
             return 0;
           }

         } /* end for grids */
       } /* end for blocks */

      bReadOK = 1;
      if (is_primary) {
        ORBoff=fp->Tell();; /* for later use */
      }
      /*  let's check correctness later */
     fp->Close();
    }

  if (bReadOK)
    {

      /* Locate the file name
      fname = filepath + strlen (filepath) - 1;
      while (fname != filepath && *fname != '/')
        fname--;
      if (*fname == '/')
        fname++;
      */
      /* Record the file name */
/*      if (filepath.Length() > MAXPATH)
        {
          msU_MSG_ArgS (" Too long Path! ");
          msU_MSG_Display (msU_MSG_CONFIRM, "ERR_Msg", &status);
          return 0;
        }
*/
      if (is_primary)
        {
          FilePath=filepath;
          //FilePath2[]=0;
          FilePath2=wxEmptyString;
        }
      else
        FilePath2=filepath;
    }


  MyString::msU_INT_ReportStatus (const_cast<char*>("READ_FILE2"), 1, &status);
  MyString::msU_INT_ReportStatus (const_cast<char*>("READ_FILE2"), 2, &status);

  return bReadOK;
#undef EC_abfgets
}


int what_is (char *it, char *all, int *is)
{
  int i, j;
  char *ptr;

  strcat (it, ",");
  if (all[strlen (all) - 1] != ',')
    strcat (all, ",");
  j = 0;
  if ((ptr = strstr (all, it)) != NULL)
    {

      for (i = 0; i < ptr - all; i++)
        if (all[i] == ',')
          {
            j = j + 1;
          }
      *is = j - 1;
    }
  MyString::mylastdel (all, ',');
  MyString::mylastdel (it, ',');
  return 0;

}

int
here_is (int model, int *Current_Grid, double *val, int *twopic,
         char *thistitle, char *alltitles)
{
  int i;
  int flag = 0;
  char *ptr;


  for (i = 0; i < memModels; i++)
    {
      if (model == currentModelData_model[i])
        {
          flag = 1;
          break;
        }
    }
  if (flag)
    {
/*   printf("Known model %d\n", model);
 */
      *val = currentModelData_iso[i];
      *twopic = currentModelData_twopic[i];
      *Current_Grid = currentModelData_number[i];
      ptr = alltitles;
      for (i = 0; i < *Current_Grid + 1; i++)
        {
          ptr = strchr (ptr, ',') + 1;
        }
      strncpy (thistitle, ptr, MAXSTRTITLE);
      thistitle[MAXSTRTITLE - 1] = 0;
      if ((ptr = strchr (thistitle, ','))!=NULL)
        *ptr = 0;
    }
  else
    {

/*
 printf("New model? total:%d\n",memModels);
*/
      for (i = 0; i < memModels; i++)
/*
 printf("%d %d\n",i, currentModelData_model[i]);
 */
        return -1;
    }



  return 0;
}

int
keep_this (int model, int Current_Grid, double val, int twopic)
{
  int i;
  int flag;
/*      printf(">k>%d\n",Current_Grid);
 */

  if (!model)
    return -1;

  flag = 0;

  for (i = 0; i < memModels; i++)
    {
      if (model == currentModelData_model[i])
        {
          flag = 1;
          break;
        }
    }

  if (flag)
    {
      currentModelData_model[i] = model;
      currentModelData_number[i] = Current_Grid;
      currentModelData_iso[i] = val;
      currentModelData_twopic[i] = twopic;

    }
  else
    {

      currentModelData_model[memModels] = model;
      currentModelData_number[memModels] = Current_Grid;
      currentModelData_iso[memModels] = val;
      currentModelData_twopic[memModels] = twopic;
      memModels++;
      if (memModels > MAXMODEL)
        {
          MyString::die (const_cast<char*>("Maximum stored model number is reached\n"));
          memModels--;
          return -1;
        }

    }

  return 0;
}

int
iso_Dependence (char *in,Grid_t *currentGridData)
{

  int i, i1, i2;
  int status;
  char *buffer;
  buffer = (char *) calloc (strlen (in) + 1, sizeof (char));

  for (i = 0; in[i] != 0; i++)
    if (in[i] == '=' || in[i] == '\n' || in[i] == ',')
      buffer[i] = ' ';
    else
      buffer[i] = in[i];
  MyString::mydiet (buffer);

metka:
  i = sscanf (buffer, "%d %d", &i1, &i2);
  if (i == 2)
    {
      if (i1 < 1 || i1 > currentGridData->ngrid || i2 < 1
          || i2 > currentGridData->ngrid2)
        {
          MyString::msU_MSG_ArgS (const_cast<char*>(" Wrong set up of grid dependence ! "));
          MyString::msU_MSG_Display (msU_MSG_CONFIRM,const_cast<char*>( "ERR_Msg"), &status);
        }
      else
        {
          currentGridData->dependence[i1 - 1] = i2;
        }
      MyString::msS_STR_TrimToken (buffer, const_cast<char*>(" "));
      MyString::msS_STR_TrimToken (buffer,const_cast<char*>( " "));
      puts (buffer);
      goto metka;
    }



  free (buffer);
  return 0;

}

/****************************************************************************************/
/* int
set_status (int Current_Grid, int what)
{
  return 0;
} */

#define MAX_attached 10

int
export_grd (char *filename,Grid_t *currentGridData)
{
  wxFFile *inp;
  wxString tmpstr,format;
  int ix, iy, iz, i1, i2, i3, ic;
  int status;
  inp=new wxFFile();
  if (!inp->Open (wxString::Format(wxT("%s"), filename), wxT("w")))
    {
      MyString::msU_MSG_ArgS (const_cast<char*>(" Can't open file for output file! "));
      MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
      return 0;
    }

  tmpstr= wxT("Temporary grid\n(1p,e12.5)\n");
  inp->Write(tmpstr,tmpstr.Len());
  format = wxT(" %7.3e %7.3e %7.3e  90.000  90.000  90.000\n");
  tmpstr= wxString::Format(format,
           currentGridData->axisvec1[0],
           currentGridData->axisvec2[1], currentGridData->axisvec3[2]);
  inp->Write(tmpstr,tmpstr.Len());

//  fprintf (inp, " %7.3f %7.3f %7.3f  90.000  90.000  90.000\n",
//           currentGridData->axisvec1[0],
//           currentGridData->axisvec2[1], currentGridData->axisvec3[2]);

  ix = currentGridData->npt[0] - 1;
  iy = currentGridData->npt[1] - 1;
  iz = currentGridData->npt[2] - 1;

  format = wxT(" %4d %4d %4d\n");
  tmpstr= wxString::Format(format, ix, iy, iz);
  inp->Write(tmpstr,tmpstr.Len());

 // fprintf (inp, " %4d %4d %4d\n", ix, iy, iz);
  format = wxT("    1 %4d %4d %4d %4d %4d %4d\n");
  tmpstr= wxString::Format(format, -ix / 2, ix - ix / 2,
           -iy / 2, iy - iy / 2, -iz / 2, iz - iz / 2);
  inp->Write(tmpstr,tmpstr.Len());

 // fprintf (inp, "    1 %4d %4d %4d %4d %4d %4d\n", -ix / 2, ix - ix / 2,
//           -iy / 2, iy - iy / 2, -iz / 2, iz - iz / 2);
  ix++;
  iy++;
  iz++;

  for (i1 = 0; i1 < iz; i1++)
    {

      for (i2 = 0; i2 < iy; i2++)
        for (i3 = 0; i3 < ix; i3++)
          {
            ic = i1 + i2 * iz + i3 * iy * iz;
  //          fprintf (inp, "%12.5E\n", currentGridData->values[ic]);
                          format = wxT("%12.5E\n");
                          tmpstr= wxString::Format(format, currentGridData->values[ic]);
                          inp->Write(tmpstr,tmpstr.Len());

                }
    }

  inp->Close();
  return 1;
}

void
read_error (void)
{
  int status;
  MyString::msU_MSG_ArgS (const_cast<char*>(" The internal structure of grid file is broken! "));
  MyString::msU_MSG_Display (msU_MSG_CONFIRM,const_cast<char*>( "ERR_Msg"), &status);
  return;
}

void
read2_error (void)
{
  int status;
  MyString::msU_MSG_ArgS (const_cast<char*>("You can draw difference grid only for the same net!"));
  MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
  return;
}


/** Read block/record from unformatted Fortran file.
 Returns:
   -1  : on failure
   number of bytes
   read into buffer : on success

   If buf==NULL, fakes read.
***/
static
int
read_bin_block(void *buf, int bufsz, wxFFile * fp)
{
 int rec_len,rec_len1;
 int rl;
 rl=fp->Read(&rec_len, sizeof(rec_len));
 if (rl==-1) return -1;
 if (buf) {
   if (rec_len>bufsz) return -1;
        rl=fp->Read(buf, rec_len);
        if (rl==-1) return -1;
 } else {
   fp->Seek(rec_len,wxFromCurrent);
 }
 rl=fp->Read(&rec_len1, sizeof(rec_len1));
 if (rl==-1) return -1;
 if (rec_len1!=rec_len) return -1;
 return rec_len;
}


/** Read string from unformatted Fortran file
 Returns:
   0  : on failure
   1  : on success
***/
int
read_bin_stream(char *str, int max, wxFFile * fp)
{
 int j;
 j=read_bin_block(str,max-1,fp);
 if (j==-1) return 0;

 str[j]=0;
 return 1;
}

int fgetline (void *str, int max, wxFFile * fp)
{
  int i = 1,j=0,l;
  char *p;
  l=fp->Read(str, max);
  if (l!=-1)
  {
        p=(char *)str;
        while(p && *p!=10 && *p!=0){
                p++,j++;}
        *p=0;
        fp->Seek(-1*(l-j-1),wxFromCurrent);
  }else
    i = 0;
  return i;
}

int
abfgets (int isBinary, char *str, int max, wxFFile * fp)
{
  int i = 1;
  if (isBinary)
    i = read_bin_stream (str, max, fp);
  else
    i = fgetline(str, max, fp);
  return i;

}

/* Added by GAlexV */
int iso_GetInfo(struct grid_info_st* iptr,Grid_t *currentGridData)
{
 int i;

 if (!currentGridData) return 0;

 iptr->natoms=natoms;
 iptr->natomsReal=natomsReal;

 iptr->xyz=(double (*)[3])xyz;
 iptr->atomNames=(char (*)[4])atomNames;
 iptr->atomNums=(int (*))atomNums;

 for (i=0; i<3; i++) {
  iptr->grd_sz[i]=currentGridData->npt[i];
 }
 iptr->ngrids=currentGridData->ngrid;

 iptr->titles=wxString(currentGridData->title);
 iptr->is_coord=currentGridData->is_coord;


 iso_CurrentGridAxesVector((double**)&iptr->axis1,
                           (double**)&iptr->axis2,
                           (double**)&iptr->axis3,
                                                   currentGridData);
 iso_CurrentGridOrigin(iptr->origin,iptr->origin+1,iptr->origin+2,currentGridData);
 for (i=0; i<4; i++) {
 iptr->sel[i]=currentGridData->sel[i]-1;
 }
 return 1;
}


/***
 Open file and read file header,
 determine file format: ascii/binary, packed, packing algorithm....

 In: file path
 Returns: (FILE* fd) on success, NULL on failure
 Globals: sets isBinary, isPacked,....
 Notes:
  1) after this function you can read on 'essential' data from file
  2) in case of error, file is closed
  3) there's possible race condition: if file appears to be binary,
     we re-open it again as binary under the same name; one can substitute
     it at that time.
***/
static wxFFile* OpenGridFile(wxString fname, int is_primary,int is_coord)
{
 wxFFile* fp;
 int old_mode=isBinary;
 char line[MAXCHARS];
 double g=0.;
 int status;
 fp=new wxFFile();
  if (!fp->Open( fname, wxT("rb"))) { //fp->Open(wxT(fname), "rb")
   return 0;
 }

  if(is_coord==2) {
                   isBinary=0;
                   isPacked=0;
                   return fp;
                  }

 if (fgetline(line, MAXCHARS,fp)) {
   switch (line[0]) {
     case '0':
       isBinary=0;
       isPacked=0;
       break;

     case '1':
       isBinary=0;
       isPacked=1;

       /* and read flags (currently unused): */
       if (! (fp->Read(line, MAXCHARS)<= 0 && sscanf(line,"%d",&PackingFlags)==1) )
       {
         read_error();
         fp->Close();
         return 0;
       }
       break;

     default:
       isBinary=1;
       break;
   }
 }
 else  {
   read_error();
   fp->Close();
   return 0;
 }

 if (!is_primary && old_mode!=isBinary) {
   MyString::msU_MSG_ArgS (const_cast<char*>("You try to compare GRIDs with different formats!"));
   MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
   fp->Close();
   return 0;
 }

if (isBinary == 1) {
   int ok = 0;
   fp->Close();
if (!fp->Open( fname, wxT("rb"))) { //fp->Open(wxT(fname), "rb")
   return 0;
 }
   if (!read_bin_stream (line, 80, fp)) {
      MyString::msU_MSG_ArgS (const_cast<char*>("Unsupported GRID datafile format!"));
      MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
      fp->Close();
      return 0;
   }

  /* check : can we read data from this binary file? */

   if ( line[0]=='a' && read_bin_block(&g,sizeof(g),fp)==sizeof(g) ) {
     if (fabs(g-1999.0)<0.001) { /* ok, old binary format */
       ok=1;
       isPacked=0;
     } else if (fabs(g-2003.9)<0.001) { /* ok, new binary packed format */
       isPacked=1;
       /* and read flags (currently unused): */
       if (read_bin_block(&PackingFlags,sizeof(PackingFlags),fp)==sizeof(PackingFlags)) {
         ok=1;
       }
     }
   }

   if (!ok) {
     MyString::msU_MSG_ArgS(const_cast<char*>("Wrong binary format! \n try to recalculate grid with ascii keyword!"));
     MyString::msU_MSG_Display (msU_MSG_CONFIRM, const_cast<char*>("ERR_Msg"), &status);
     fp->Close();
     return 0;
   }
 } /* END isBinary==1 */

 return fp;
}


/***
 Read or skip block. When reading, unpack values if necessary.

 In: fd  -- file descriptor
     buf -- pointer to buffer to read into
     np  -- expected number of values in block (buffer length, in doubles)
 Returns: success flag
 Notes:
  1) If pointer is NULL, does fake read (i.e. skips the block)
  2) After successfull reading/skipping the file pointer is positioned
     just after the block (presumably at the beginning of the next one)
***/
static int ReadBlock(double* buf, int np, wxFFile* fp)
{
 int j;
 char line[MAXCHARS];
 struct packing_params_t pk_prm;
 byte_t* packed_data;

 if (!isPacked) {
   if (isBinary) {
     return read_bin_block(buf,np*sizeof(*buf),fp); /* Works even if buf==0, see read_bin_block() */
   }

   for (j=0; j<np; j++) {
     if (fgetline(line, MAXCHARS,fp)==-1) return 0;
     if (buf) {
       if (sscanf(line,"%lf",buf+j)!=1) return 0;
     }
   }
   return j;
 }

 if (!abfgets(isBinary,line,MAXCHARS,fp)) return 0;
 if (!getPackingParams(line,&pk_prm)) return 0;

 if (isBinary) {
   if (buf==0) { /* just skipping block */
     return read_bin_block(0,np*sizeof(*buf),fp);
   }
   packed_data=(byte_t *)calloc(np,pk_prm.nbytes);
   if (read_bin_block(packed_data,np*pk_prm.nbytes,fp)==-1) {
    free(packed_data);
    return 0;
   }
   unpackData(buf,packed_data,0,np,&pk_prm); /* call to unpack array */
   free(packed_data);
 } else { /* ascii data */
   for (j=0; j<np; j++) {
     if (fp->Read(line,MAXCHARS)<= 0) return 0;
     if (buf) {
       unsigned int val;
       if (sscanf(line,"%d",&val)!=1) return 0;
       unpackData(buf+j,0,val,0,&pk_prm); /* call to unpack single int-value */
     }
   }
 }
 return np;
}


/***
  Parse header, fill a structure with packing params.
***/
static int getPackingParams(const char* line, struct packing_params_t* prm)
{
 int flags;
 int i;

 if (sscanf(line,"BHeader= %d %lf %lf %lf %lf %d %d %d",
              &flags,                 /* not used yet */
              &(prm->xlimit[0]),      /* xmin */
              &(prm->xlimit[1]),      /* xleft */
              &(prm->xlimit[2]),      /* xright */
              &(prm->xlimit[3]),      /* xmax */
              &(prm->ydelta[0]),      /* dy1 */
              &(prm->ydelta[1]),      /* dy2 */
              &(prm->ydelta[2])       /* dy3 */
      )!=8) {
    return 0;
 }
 prm->ileft=0;  prm->iright=2;
 if (prm->ydelta[0]==0) {
   prm->ileft++;
   prm->xdelta[0]=0.;
 }
 if (prm->ydelta[2]==0) {
   prm->iright--;
   prm->xdelta[2]=0.;
 }

 prm->ylimit[0]=0;
 prm->ylimit[1]=prm->ydelta[0];
 prm->ylimit[2]=prm->ylimit[1] + prm->ydelta[1];
 prm->ylimit[3]=prm->ylimit[2] + prm->ydelta[2];
 prm->range=prm->ylimit[3];

 prm->nbytes=(prm->range==128)?1:2;

 for (i=prm->ileft; i<=prm->iright; i++) {
   prm->xdelta[i]=prm->xlimit[i+1] - prm->xlimit[i];
 }
/******
 fprintf(stderr,"LIMITS:\n"
                "range=%d\nxlimit=%f %f %f %f\nxdelta=%f %f %f\n"
                "ylimit=%d %d %d %d\nydelta=%d %d %d\n",
                prm->range,
                prm->xlimit[0],prm->xlimit[1],prm->xlimit[2],prm->xlimit[3],
                prm->xdelta[0],prm->xdelta[1],prm->xdelta[2],
                prm->ylimit[0],prm->ylimit[1],prm->ylimit[2],prm->ylimit[3],
                prm->ydelta[0],prm->ydelta[1],prm->ydelta[2]
        );
*****/
 return 1;
}


/***
 Unpack data either from array of bytes or from integer value.
 In: dest -- array to unpack into
     packed_data -- packed values, represented as 1-byte or MSB-first 2-bytes
     packed_val  -- alternatively, single integer to unpack
     ndata  -- length (number of values, not size in bytes!) of packed_data,
               or, the same, length (in doubles) of the dest buffer
     prm -- packing parameters

***/
static void unpackData(double* dest,
                       byte_t* packed_data, unsigned int packed_val,
                       int ndata,
                       const struct packing_params_t* prm)
{
 if (packed_data==0) { ndata=1; } /* we're called to unpack single value */

 while (ndata--) {
   if (packed_data) {  /* we're called to unpack array */
     packed_val=*(packed_data++);
     if (prm->nbytes==2) { /* 2 bytes, MSB first!!! */
       packed_val<<=8;
       packed_val += *(packed_data++);
     }
   } /* else use supplied single value of packed_val */

   /** Unpacking begins. **/
   /** Inlining -- for effectiveness. Separate block -- for clarity **/
   {
     int i;
     double x=0.0;
     int y=packed_val; /* for brevity */
     int minus=0;

     y-=prm->range; /* BEWARE: reperesentable int range and sign! */
     if (y<0) { y=-y; minus=1; }

     if (y<prm->ylimit[0])  { x=prm->xlimit[0]; goto x_done; }
     if (y>=prm->ylimit[3]) { x=prm->xlimit[3]; goto x_done; }

     for (i=prm->ileft; i<=prm->iright; i++) {
       if (y>=prm->ylimit[i] && y<prm->ylimit[i+1]) {
         x=((double)(y - prm->ylimit[i]))/ prm->ydelta[i] * prm->xdelta[i] + prm->xlimit[i];
         goto x_done;
       }
     }
     x_done:
     *(dest++)=(minus?-x:x);
   }
   /** Unpacking ends. **/
 }  /* cycle through array ends */
}
/*
void setTitle(int Current_Grid)
{
char *it;
char *ptr, *ptr1;
int i;
 it=(char *) xmalloc(sizeof(char)*(1+strlen(currentGridData->title)));

 strcpy(it,currentGridData->title);
 ptr=it;


 for(i=0; i<Current_Grid+1; i++)
 {
  ptr=strchr(ptr,',');
  if(ptr!=NULL) ptr++;
 }
 ptr1=strchr(ptr,',');
 if(ptr1!=NULL) *ptr1=0;

 strcpy(ThisTitle,ptr);

 free(it);
 return;
}

void getCurTitle(char *tmp)
{
 strcpy(tmp,ThisTitle);
 return;

}
*/
/*
void setType(int i)
{
char currentTitle[64];
char *tempptr, *tempstr;
char NewChar[3];

 strcpy(currentTitle,ThisTitle);


    tempptr=strstr(nTypeChange,currentTitle);
    if(tempptr!=NULL)
      {
        tempstr=strchr(tempptr,'\n');
        if(tempstr!=NULL && *(tempstr+1)!='\n' )
           {
            strcpy(tempptr,tempstr+1);
           }
         else
           {
            *tempptr=0;
           }
      }


 strcat(TypeChange, currentTitle);
 strcat(TypeChange," -> ");

    switch (i) {
      case 0: strcat (TypeChange,"Unknown"); break;
      case 1: strcat (TypeChange,"Frozen");   strcat(NewChar,"F*");   break;
      case 2: strcat (TypeChange,"Inactive"); strcat(NewChar,"I*");   break;
      case 3: strcat (TypeChange,"RAS1");     strcat(NewChar,"1*");   break;
      case 4: strcat (TypeChange,"RAS2");     strcat(NewChar,"2*");   break;
      case 5: strcat (TypeChange,"RAS3");     strcat(NewChar,"3*");   break;
      case 6: strcat (TypeChange,"Secondary");strcat(NewChar,"S*");   break;
      case 7: strcat (TypeChange,"Deleted");  strcat(NewChar,"D*");   break;
      default: break;

    }
  strcatc(TypeChange,'\n');
  puts("Modify Index:");
  puts(TypeChange);

return;
}
*/
/* Forward declarations of internal functions */

static void srf_Clean(surface_t*); /* cleanup surface */
static void Compute_Distances(multisrf_t*);

static void srf_Clean(surface_t*); /* cleanup surface */

/* Initializer/constructor */
multisrf_t* msrf_Init(multisrf_t* self, int n_srf)
{
 if (self==0) {
   self=(multisrf_t*)calloc(sizeof(multisrf_t),1);
 }

 self->index=0;

 self->n_surfaces=n_srf;
 self->child=(surface_t**)calloc(sizeof(surface_t*),n_srf);

 return self;
}

/* Cleanup */
void msrf_Clean(multisrf_t* self)
{
 int i;

 for (i=0; i<self->n_surfaces; i++) {
   surface_t* child=self->child[i];
   if (child) {
     srf_Clean(child);
     free(child);
   }
 }

 if (self->index) free(self->index);
 self->index=0;
 //self->is_valid=0;
}

/* Construct & init new surface */
surface_t* msrf_New_Surface(multisrf_t* self,int mo)

{
// int child_no;
 surface_t* new_child=0;
 int i;

 //for (child_no=0; child_no<self->n_surfaces; child_no++) {
 //  if (self->child[mo]==0) goto place; /* place for a new surface */
 //}
 /* if we're here, there's no place for a new surface */
 //return 0;

 //place:
 new_child=(surface_t*)malloc(sizeof(surface_t));
 new_child->head=0;
 new_child->n_triangles=0;

 new_child->parent=self;
 new_child->offset=mo;
 new_child->is_valid=0;
 new_child->is_sorted=0;
 for (i=0; i<3; i++) {
   new_child->max_coor[i]=new_child->min_coor[i]=0;
 }

 //self->is_valid=0; /* invalidate triangle array */
 self->child[mo]=new_child;
 return new_child;
}


/* Delete a surface */
void msrf_Delete_Surface(surface_t* self)
{
 multisrf_t* parent=self->parent;

 /* assertion */
 if (parent->child[self->offset]!=self) { /* can't happen */
   MyString::SayError(const_cast<char*>("msrf_Delete_Surface(): structure corrupted"));
   exit(1);
 }

 //parent->is_valid=0; /* invalidate triangle array */
 parent->child[self->offset]=0; /* free the slot */
 srf_Clean(self);
 free(self);
}

/* add triangle with normals */
triangle_t* srf_Add_Triangle(surface_t* self,
                             double txyz[3][3],
                             double tgrd[3][3],
                             unsigned char clr)
{
 triangle_t* tri=(triangle_t*)malloc(sizeof(triangle_t));
 int vi;

 if (self->n_triangles==0) { /* the 1st triangle */
   int k;
   for (k=0; k<3; k++) {
     self->min_coor[k]=self->max_coor[k]=txyz[k][0]; /* init min/max vaues */
   }
 }

 for (vi=0; vi<3; vi++) { /* for each vertex */
   int k;
   for (k=0; k<3; k++) { /* for each coor */
     tri->vertex[vi][k]=txyz[k][vi];
     tri->normal[vi][k]=tgrd[k][vi];

     /* update bounding box coors */
     if (self->max_coor[k] < txyz[k][vi]) { self->max_coor[k]=txyz[k][vi]; }
     if (self->min_coor[k] > txyz[k][vi]) { self->min_coor[k]=txyz[k][vi]; }
   }
 }
 tri->distance=0;
 tri->clr_idx=clr;

 /* add this node-triangle to list */
 tri->lnk_prev=self->head;
 self->head=tri;

 self->n_triangles++;

 self->is_valid=0; /* invalidate array */
 return tri;
}

void Color_Copy(color_t to, const color_t from)
{
 unsigned int i;
 for (i=0; i<sizeof(color_t)/sizeof(to[0]); i++) {
   to[i]=from[i];
 }
}

/* draw all surfaces in current OpenGL context */

void msrf_Draw(multisrf_t* self, color_t color[],int mode)
{
 int i;

 if (self->child[self->curmo]){
         if(!self->child[self->curmo]->is_valid) {
                if (self->index) free(self->index);
                 Make_Index(self);
                }

          if(!self->child[self->curmo]->is_sorted) {
                Sort_by_Depth(self);
                }

if (mode==0){
        glEnable(GL_BLEND); // 打开混合
        glDisable(GL_DEPTH_TEST); // 关闭深度测试
         }
if(mode==2){
glBegin(GL_LINES);
}else{
glBegin(GL_TRIANGLES);
}
 for (i=0; i<self->n_triangles; i++) {
   triangle_t* triangle=self->index[i];
   int j;
         glColor4fv(color[triangle->clr_idx]);
        if(mode==2){
           glVertex3dv(triangle->vertex[0]);
           glVertex3dv(triangle->vertex[1]);
           glVertex3dv(triangle->vertex[1]);
           glVertex3dv(triangle->vertex[2]);
        }else{
           for (j=0; j<3; j++) {
                 glNormal3dv(triangle->normal[j]);
                 glVertex3dv(triangle->vertex[j]);
           }
        }
   }
 glEnd();
if(mode==0){
glEnable(GL_DEPTH_TEST); // 打开深度测试
glDisable(GL_BLEND); // 关闭混合
 }
 }
}



/* Internal functions */

/* cleanup a surface */
static void srf_Clean(surface_t* self)
{
 triangle_t* tp=self->head;

 while (tp) { /* go through the linked list of triangles */
   triangle_t* link=tp->lnk_prev;
   free(tp);
   tp=link;
 }
}

/* comparison function for depth sorting */
static int compare_triangles(const void* tptr1, const void* tptr2)
{
 const triangle_t* tre1_ptr=*(const triangle_t **)tptr1;
 const triangle_t* tre2_ptr=*(const triangle_t **)tptr2;

 if (tre1_ptr->distance > tre2_ptr->distance) return -1;
 if (tre1_ptr->distance < tre2_ptr->distance) return  1;
 /* Debug("Triangles are eq: %g %g",tre1_ptr->distance,tre2_ptr->distance); */
 return 0;
}

/* sort all triangles */
void Sort_by_Depth(multisrf_t* self)
{
 /* recalculate distances */
 Compute_Distances(self);
 /* reorder triangle indices, sorting by depth */
 qsort(self->index,self->n_triangles,sizeof(triangle_t*),compare_triangles);

 /* mark as sorted */
 self->child[self->curmo]->is_sorted=1;
}

static void Compute_Distances(multisrf_t* self)
{
 GLdouble mvm[16];
 int idx;

/*
 if (!Surface.index) {
   Make_Surface_Index();
 }
*/
 glGetDoublev(GL_MODELVIEW_MATRIX,mvm); /* get model-view matrix */
 for (idx=0; idx<self->n_triangles; idx++) {
   GLdouble avg_z=0;
   triangle_t* tri=self->index[idx];
   int ivert;
   for (ivert=0; ivert<3; ivert++) {
     GLdouble z_dist=0;
     int k;
     for (k=0; k<3; k++) {
       z_dist+=tri->vertex[ivert][k]*mvm[2+4*k];
     }
     z_dist+=mvm[2+4*3]; /* add 4th component of scalar product */

     avg_z+=z_dist;
   }
   tri->distance=-avg_z/3;
 }
}

/* Construct "index" array of pointers to all triangles */
void Make_Index(multisrf_t* self)
{
// int ichild;
 triangle_t** tptr;
 surface_t* child;

 /* compute total number of triangles */
 self->n_triangles=0;
 //for (ichild=0; ichild<self->n_surfaces; ichild++) {
   child=self->child[self->curmo];
   if (child) {
     self->n_triangles+=child->n_triangles;
   }
 //}

 /* allocate memory for array */
 self->index=tptr=(triangle_t**)calloc(self->n_triangles,sizeof(triangle_t*));

 /* fill the array: */
 /* for each child surface */
 //for (ichild=0; ichild<self->n_surfaces; ichild++) {
   child=self->child[self->curmo];
   triangle_t* link;
   int i;

   if (!child) {
   MyString::SayError(const_cast<char*>("Make_Index(): inconsistency, no surface!"));
       exit(1);
   };

   /* for each triangle within surface */
   for (i=0, link=child->head; i<child->n_triangles; i++) {
     if (!link) { /* can't happen */
       MyString::SayError(const_cast<char*>("Make_Index(): inconsistency, too few triangles!"));
       exit(1);
     }
     *(tptr++)=link; /* store pointer to triangle */
     link=link->lnk_prev;
   } /* endfor: by triangles */

   if (link) { /* can't happen */
     MyString::SayError(const_cast<char*>("Make_Index(): inconsistency, too many triangles!"));
     exit(1);
   }
 //} /* endfor: by surfaces */

 /* mark as valid, but unsorted */
 self->child[self->curmo]->is_valid=1;
 self->child[self->curmo]->is_sorted=0;
}

/*FIXME: move to some header file? */
int mcubes( surface_t* surf,
           int nx,int ny,int nz, double fvals[], double origin[3],
           double cntval,double rad[3],int do_minus);


/* FIXME: It's not strictly 'wrapper', will be moved */
/* Calculate max. diameter */
GLdouble Calc_Diameter(input_data_t &Input_Data,grid_info_t &Grid_info,multisrf_t* All_Surfaces)
{
 int k;
 double diam=0.;

 if (Input_Data.max_diam_ok) return Input_Data.max_diam; /* don't recalculate if not changed */

 for (k=0; k<3; k++) { /* for each coordinate */
   GLdouble max_c=Grid_info.xyz[0][k]; /* 1st atom */
   GLdouble min_c=max_c;
   int i;
   double d;

   for (i=1; i<Grid_info.natomsReal; i++) { /* for each atom */
     double coor=Grid_info.xyz[i][k];
     if (max_c<coor) { max_c=coor; }
     if (min_c>coor) { min_c=coor; }
   }
  if( Grid_info.is_coord==0)
  {
   for (i=0; i<Grid_info.ngrids; i++) { /* for each orbital... */
     surface_t* srf=Input_Data.surf[i];
     if (!srf) continue;           /* ... which is displayed */
     if (max_c < srf->max_coor[k]) { max_c=srf->max_coor[k]; }
     if (min_c > srf->min_coor[k]) { min_c=srf->min_coor[k]; }
   }
  }
   d=max_c-min_c;
   if (d<0.2) d=0.2; /* avoid empty intervals (zero-sized BBoxes) */

   diam+=d*d;
 }
 diam=sqrt(diam);

 diam*=1.05; /* 5% margins */
 Input_Data.max_diam_ok=1;
 return (Input_Data.max_diam=diam);
}
