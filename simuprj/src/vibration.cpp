/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>

#include "vibration.h"
#include "datatype.h"

#include <wx/arrimpl.cpp>
WX_DEFINE_OBJARRAY(VibPropertyDisItemArray);
WX_DEFINE_OBJARRAY(AtomInfoArray);
WX_DEFINE_OBJARRAY(ModeFormArray);
WX_DEFINE_OBJARRAY(VibrationItemArray);
WX_DEFINE_OBJARRAY(VibrationCoorsArray);
//WX_DEFINE_OBJARRAY(wxDouble2Array);
const wxString SEMI_OUT_VIBRATION_ORIENTATION_COORDINATE_FLAG=wxT("Cartesian Coordinates (Angstroms)");
const wxString SEMI_OUT_VIBRATION_NORMAL_COORDINATE_FLAG=wxT("Normal Coordinates and Frequencies (cm-1)");
const wxString SEMI_OUT_VIBRATION_COORDINATE_FLAG=wxT("No.");
const wxString SEMI_OUT_END_FLAG=wxT("----");

const wxString VIB_FILE_BEGIN_VIBS=wxT("$BEGIN IRS");
const wxString VIB_FILE_END_VIBS=wxT("$END IRS");
const wxString VIB_FILE_BEGIN_VIB_ITEM=wxT("$BEGIN IR");
const wxString VIB_FILE_END_VIB_ITEM=wxT("$END IR");

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
static void DisplayArrayString(const wxArrayString& data)
{
        wxString text=wxEmptyString;
        for(unsigned i=0;i<data.GetCount();i++)
        {
                text+=data[i]+wxT("\n");
        }
        wxMessageBox(text);
}

static wxIntArray FindPointInArray(const wxArrayString& src)
{
        wxIntArray ins;
        for(unsigned i=0;i<src.GetCount();i++)
        {
                if(src[i].Find(wxT('.'))!=wxNOT_FOUND)
                        ins.Add(i);
        }
        return ins;
}
static wxString GetLinkedString(const wxArrayString& src,int from,int to)
{
        if(from <0 || to<0 || from>to || from>=(int)src.GetCount() || to >=(int)src.GetCount())
                return wxEmptyString;
        wxString linkedString=wxEmptyString;
        for(int i=from;i<=to;i++)
        {
                linkedString+=src[i];
        }
        return linkedString;
}
*/
/**
* Construct COORDINATE ANALYSIS
No.    1       2       3       4       5       6       7       8
              1 T2    1 T2    1 T2    1 E     1 E     2 T2    2 T2    2 T2
            1253.4  1253.4  1253.4  1261.5  1261.5  2721.2  2721.2  2721.2
         1  0.0031  0.0196  0.0618  0.0000  0.0000  0.0189 -0.0249 -0.0298
         2  0.0567  0.0274 -0.0110  0.0000  0.0000 -0.0112  0.0292 -0.0299
         3 -0.0277  0.0584 -0.0156  0.0000  0.0000 -0.0370 -0.0216 -0.0062
         4  0.0014  0.0087  0.0266  0.0001 -0.0001 -0.1872  0.2471  0.2961
         5 -0.2657 -0.1285  0.0515 -0.2494  0.0174 -0.0056  0.0146 -0.0147
         6  0.1297 -0.2734  0.0731  0.0173  0.2494 -0.0183 -0.0106 -0.0029
         7 -0.1038 -0.1246 -0.2367 -0.2351  0.0165 -0.0493  0.1124 -0.0783
         8 -0.0127 -0.0353 -0.0979 -0.0832  0.0058  0.1600 -0.3379  0.1646
         9  0.1296 -0.2734  0.0730 -0.0174 -0.2494 -0.0184 -0.0106 -0.0031
        10 -0.0057  0.0225 -0.2850  0.1034 -0.2118  0.1107  0.0296  0.0860
        11 -0.2530  0.0334  0.0578  0.1764  0.1324  0.1380  0.0740  0.1280
        12  0.1512  0.0070  0.0841 -0.1440  0.0099  0.2302  0.0924  0.2442
        13  0.0714 -0.1398 -0.2415  0.1317  0.1955 -0.0992 -0.0926  0.0514
        14 -0.1441 -0.1965  0.1193  0.1563 -0.1555 -0.1589 -0.0989  0.0787
        15 -0.0807 -0.1556 -0.0447  0.1440 -0.0099  0.2471  0.1857 -0.1650
   No.    9
              1 A1
            2827.3
         1  0.0000
         2  0.0000
         3  0.0000
         4  0.2500
         5  0.0000
         6  0.0000
         7 -0.0833
         8  0.2357
         9  0.0000
        10 -0.0833
        11 -0.1178
        12 -0.2041
        13 -0.0833
        14 -0.1179
        15  0.2041
                ***********************************************************************
   No.    1       2       3

              1 A'    2 A'    3 A'

           -1434.1  1753.7  2122.9

   x  C  1  0.0442 -0.0326  0.3522
   y  C  1  0.0645  0.0509  0.0114
   z  C  1  0.0000  0.0000  0.0000
   x  N  2  0.0220  0.0399 -0.3044
   y  N  2 -0.0679  0.0196  0.0148
   z  N  2  0.0000  0.0000  0.0000
   x  H  3 -0.8321 -0.1666  0.0327
   y  H  3  0.1754 -0.8794 -0.3413
   z  H  3  0.0000  0.0000  0.0000


* @param vibCoors[in,out]: the Coors to construct;
* @param data[in]:the semi vibration description
**/
void ConstructSemiCoodinateAnalysis(VibrationCoorsArray& vibCoors,const wxArrayString& data)
{
        unsigned i,j,k,base,baseIndex,rootCount=1,count,total,index=0,divisor=data.GetCount();
        wxArrayString temp,typeArray;
        wxString delim=wxT(" \t\n\r");
        long lvalue;
        double dvalue;
        int nvalue;
        VibrationCoors vibCoor;
        AtomPositionArray posArray;
        //DisplayArrayString(data);
        //First, get the next root line
        for(i=1; i<data.GetCount() ;i++)
        {
                if(data[i].StartsWith(SEMI_OUT_VIBRATION_COORDINATE_FLAG))
                {
                        divisor=i;
                        break;
                }
        }
        for(i=0;i<(data.GetCount()/divisor);i++)
        {
                base=i*divisor;
                //anayse base line;
                temp.Clear();
                temp=wxStringTokenize(data[base],delim,wxTOKEN_STRTOK);
                count=temp.GetCount()-rootCount;
                vibCoors.Insert(vibCoor,0,count);
        }
        //////////////////////////////////////////////
        posArray.Clear();
        posArray.Insert(AtomPosition(),0,divisor/3-1);
        baseIndex=0;//for col
        for(i=0;i<data.GetCount()/divisor;i++)
        {
                base=i*divisor;//for row
                //No.    1       2       3       4       5       6       7       8
                //anayse base line;
                temp.Clear();
                temp=wxStringTokenize(data[base],delim,wxTOKEN_STRTOK);
                total=temp.GetCount()-rootCount;
                count=0;
                for(j=rootCount;j<temp.GetCount();j++,count++)
                {
                        temp[j].ToLong(&lvalue);
                        nvalue=(int)lvalue;
                        vibCoors[baseIndex+count].index=nvalue;//save for ids;
                }
                //1 T2    1 T2    1 T2    1 E     1 E     2 T2    2 T2    2 T2
                temp.Clear();
                temp=wxStringTokenize(data[base+1],delim,wxTOKEN_STRTOK);
                count=0;
                for(j=0;j<temp.GetCount();j++,count++)
                {
                        //temp[j].ToLong(&lvalue);
                        //nvalue=(int)lvalue;
                        //vibCoors[baseIndex+count].id=nvalue;
                        wxString virtype=temp[j];
                        vibCoors[baseIndex+count].type=virtype;
         }
                //1253.4  1253.4  1253.4  1261.5  1261.5  2721.2  2721.2  2721.2
                temp.Clear();
                temp=wxStringTokenize(data[base+2],delim,wxTOKEN_STRTOK);
                count=0;
                for(j=0;j<temp.GetCount();j++,count++)
                {
                        temp[j].ToDouble(&dvalue);
                        vibCoors[baseIndex+count].Freq=dvalue;
                        vibCoors[baseIndex+count].Coors=posArray;
                }
                baseIndex+=total;
        }
        baseIndex=0;//for col
        for(i=0;i<data.GetCount()/divisor;i++)
        {
                base=i*divisor;//for row
                //No.    1       2       3       4       5       6       7       8
                //anayse base line;
                temp.Clear();
                temp=wxStringTokenize(data[base],delim,wxTOKEN_STRTOK);
                total=temp.GetCount()-rootCount;
                //1  0.0031  0.0196  0.0618  0.0000  0.0000  0.0189 -0.0249 -0.0298
                //2  0.0567  0.0274 -0.0110  0.0000  0.0000 -0.0112  0.0292 -0.0299
                //3 -0.0277  0.0584 -0.0156  0.0000  0.0000 -0.0370 -0.0216 -0.0062
                //4  0.0014  0.0087  0.0266  0.0001 -0.0001 -0.1872  0.2471  0.2961
                //x  C  1  0.0442 -0.0326  0.3522
                //y  C  1  0.0645  0.0509  0.0114
                //z  C  1  0.0000  0.0000  0.0000
                //x  N  2  0.0220  0.0399 -0.3044

                for(j=3,index=0;j<divisor;j++,index++)
                {
                        //1  0.0031  0.0196  0.0618  0.0000  0.0000  0.0189 -0.0249 -0.0298
                        temp.Clear();
                        temp=wxStringTokenize(data[base+j],delim,wxTOKEN_STRTOK);
                        //DisplayArrayString(temp);
                        if(data[base+j].StartsWith(wxT("x")) || data[base+j].StartsWith(wxT("y")) || data[base+j].StartsWith(wxT("z")))
                                count=3;
                        else
                                count=1;
                        for(k=count;k<temp.GetCount();k++)
                        {
                                temp[k].ToDouble(&dvalue);
                                //wxMessageBox(wxString::Format(wxT("%f"),dvalue));
                                vibCoors[baseIndex+k-count].Coors[j/3-1][(j-3)%3]=dvalue;
                                //wxMessageBox(wxString::Format(wxT("[%d].Coors[%d][%d]=%f"),baseIndex+k-count,j/3-1,(j-3)%3,dvalue));
                        }
                }
                baseIndex+=total;
        }


}
/**
* Construct Vibration ORIENTATION Coors From Semi Vibration ORIENTATION Data
* @param infos[in,out]: the Coors to construct;
* @param data[in]:the semi vibration description
**/
void ConstructSemiVibrationOrientCoors(AtomInfoArray& infos,const wxArrayString& data)
{
        wxArrayString temp;
        wxString delim=wxT(" \t\n\r");
        AtomInfo info;
        double dvalue;
        infos.Clear();
        //DisplayArrayString(data);
        for(unsigned i=0;i<data.GetCount();i++)
        {
                //NO.       ATOM         X         Y         Z
                temp.Clear();
                temp=wxStringTokenize(data[i],delim,wxTOKEN_STRTOK);
                info=AtomInfo();
                //atom
                info.type=temp[1];
                //x
                temp[2].ToDouble(&dvalue);
                info.x=dvalue;
                //y
                temp[3].ToDouble(&dvalue);
                info.y=dvalue;
                //z
                temp[4].ToDouble(&dvalue);
                info.z=dvalue;
                //add info
                infos.Add(info);
        }
}
/**
* Construct Vibration Item From Semi Vibration Description Data
* @param vibration[in,out]: the vibration to construct;
* @param data[in]:the semi vibration description
**/
/*
           -320.66                 967.58                 967.58
               A                      E                      E
         X      Y      Z        X      Y      Z        X      Y      Z
   1   0.000  0.000 -0.025    0.000  0.170  0.000    0.170  0.000  0.000
   2   0.357 -0.202 -0.015    0.009 -0.154  0.460   -0.214  0.059  0.214
   3  -0.004  0.410 -0.015   -0.016 -0.228 -0.045   -0.139  0.034 -0.505
   4  -0.353 -0.208 -0.015   -0.068 -0.169 -0.415   -0.199 -0.018  0.291
   5   0.000  0.000  0.025    0.051  0.144  0.000    0.144 -0.051  0.000
   6  -0.400  0.064  0.015   -0.026 -0.134 -0.481   -0.220  0.041  0.043
   7   0.145 -0.379  0.015    0.000 -0.205  0.278   -0.149  0.067  0.395
   8   0.255  0.315  0.015   -0.074 -0.192  0.203   -0.162 -0.007 -0.438


            1044.34                1343.34                1343.34
               A                      E                      E
         X      Y      Z        X      Y      Z        X      Y      Z
   1   0.000  0.000  0.657    0.399  0.000  0.000    0.000 -0.399  0.000
   2   0.024  0.004  0.149   -0.165  0.064  0.227   -0.082  0.080 -0.342
   3  -0.016  0.019  0.149   -0.038 -0.008 -0.410   -0.009  0.207 -0.026
   4  -0.009 -0.023  0.149   -0.164 -0.082  0.182    0.065  0.080  0.368
   5   0.000  0.000 -0.655   -0.403  0.021  0.000    0.021  0.403  0.000
   6  -0.016  0.017 -0.152    0.207 -0.036  0.017    0.005 -0.048 -0.439
   7  -0.007 -0.023 -0.152    0.070 -0.074 -0.388    0.043 -0.185  0.205
   8   0.023  0.006 -0.152    0.105  0.064  0.372   -0.095 -0.149  0.234
* @param vibCoors[in,out]: the Coors to construct;
* @param data[in]:the semi vibration description
**/
void ConstructAbinCoodinateAnalysis(VibrationCoorsArray& vibCoors,const wxArrayString& data)
{
        unsigned line,i,j,baseIndex,vibCount,count;
        wxArrayString tokens;
        wxString delim=wxT(" \t\n\r");
        double dvalue;
        VibrationCoors vibCoor;
        AtomPositionArray posArray;
        wxIntArray starts,ends;
        //DisplayArrayString(data);
        //first: find the start line and the end line of vibration info
        starts.Add(0);
        for(line=1; line< data.GetCount();line++)
        {
                if(data[line-1].IsEmpty() && !data[line].IsEmpty())
                        starts.Add(line);
                else if(!data[line-1].IsEmpty() && data[line].IsEmpty())
                        ends.Add(line-1);
        }
        if(!data[data.GetCount()-1].IsEmpty())
                ends.Add(data.GetCount()-1);
        wxASSERT_MSG(starts.GetCount()==ends.GetCount(),wxT("Error for not pipei"));
        //second: get the total vib coor
        for(i=0 ; i< starts.GetCount() ; i++)
        {
                tokens=wxStringTokenize(data[starts[i]],delim,wxTOKEN_STRTOK);
                vibCoors.Insert(vibCoor,0,tokens.GetCount());
        }
        baseIndex=0;
        for( count=0 ; count < starts.GetCount() ; count++)
        {
                /*
                            1044.34                1343.34                1343.34
               A                      E                      E
         X      Y      Z        X      Y      Z        X      Y      Z
   1   0.000  0.000  0.657    0.399  0.000  0.000    0.000 -0.399  0.000
   2   0.024  0.004  0.149   -0.165  0.064  0.227   -0.082  0.080 -0.342
   3  -0.016  0.019  0.149   -0.038 -0.008 -0.410   -0.009  0.207 -0.026
   4  -0.009 -0.023  0.149   -0.164 -0.082  0.182    0.065  0.080  0.368
   5   0.000  0.000 -0.655   -0.403  0.021  0.000    0.021  0.403  0.000
   6  -0.016  0.017 -0.152    0.207 -0.036  0.017    0.005 -0.048 -0.439
   7  -0.007 -0.023 -0.152    0.070 -0.074 -0.388    0.043 -0.185  0.205
   8   0.023  0.006 -0.152    0.105  0.064  0.372   -0.095 -0.149  0.234
                */
                //set the line to the start line
                line=starts[count];
                //                            1044.34                1343.34                1343.34
                tokens=wxStringTokenize(data[line],delim,wxTOKEN_STRTOK);
                vibCount=tokens.GetCount();
                for(i=0 ; i< tokens.GetCount(); i++)
                {
                        tokens[i].ToDouble(&dvalue);
                        vibCoors[baseIndex+i].Freq=dvalue;
                        vibCoors[baseIndex+i].id=baseIndex+i+1;
                }
                line++;
                //               A                      E                      E
                tokens=wxStringTokenize(data[line],delim,wxTOKEN_STRTOK);
                for(i=0 ; i< tokens.GetCount(); i++)
                {
                        vibCoors[baseIndex+i].type=tokens[i];
                }
                line+=2;
                //set the pos array
                posArray.Clear();
                posArray.Insert(AtomPosition(),0,ends[count]-line+1);
                for(i=0 ; i< vibCount; i++)
                        vibCoors[baseIndex+i].Coors=posArray;
                //   1   0.000  0.000  0.657    0.399  0.000  0.000    0.000 -0.399  0.000
                for(i=line; i <=(unsigned)ends[count]; i++)
                {
                        tokens=wxStringTokenize(data[i],delim,wxTOKEN_STRTOK);
                        for(j=1; j < tokens.GetCount(); j++)
                        {
                                tokens[j].ToDouble(&dvalue);
                                vibCoors[baseIndex+(j-1)/3].Coors[i-line][(j-1)%3]=dvalue;
                        }
                }
                baseIndex+=vibCount;
        }

}

void VibFile::SaveFile(const wxString& fileName,const Vibration &vibInfo)
{
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        unsigned i,j;
        tos << wxString::Format(wxT("%d"),vibInfo.OrientCoors.GetCount()) << endl;
        tos << wxT("#Generated with SimuGUI") << endl;
        for(i=0;i<vibInfo.OrientCoors.GetCount();i++)
        {
                tos << vibInfo.OrientCoors[i].ToString() << endl;
        }
        tos << endl;
        tos << VIB_FILE_BEGIN_VIBS << endl;
        for(i=0;i<vibInfo.NormalCoors.GetCount();i++)
        {
                tos << VIB_FILE_BEGIN_VIB_ITEM << endl;
                tos << wxString::Format(wxT("%f"),vibInfo.NormalCoors[i].Freq) <<endl;
                tos << vibInfo.NormalCoors[i].type <<endl;
                for(j=0;j<vibInfo.NormalCoors[i].Coors.GetCount();j++)
                {
                        tos << vibInfo.NormalCoors[i].Coors[j].ToString() <<endl;
                }
                tos << VIB_FILE_END_VIB_ITEM <<endl;
                if(i!=vibInfo.NormalCoors.GetCount()-1)
                        tos << endl;
        }
        tos << VIB_FILE_END_VIBS <<endl;

}
/**
* Construct Vibration ORIENTATION Coors FOR ABIN
                Cartesian Coordinates (Angstroms)
    Atom          X             Y             Z
    ----    ------------- ------------- -------------

    C    1     0.0000000     0.0000000     0.0000000
    H    2     0.3566090    -1.0088260     0.0000000
    H    3     0.3566830     0.5043770    -0.8736600
    H    4    -1.0700000     0.0000000     0.0000000
    O    5     0.4766890     0.6740740     1.1676010
    H    6     1.3897020     0.9391050     1.0343150
* @param infos[in,out]: the Coors to construct;
* @param data[in]:the semi vibration description
**/
void ConstructAbinVibrationCoors(AtomInfoArray& infos,const wxArrayString& data)
{
        wxArrayString tokens;
        wxString delim=wxT(" \t\n\r");
        AtomInfo info;
        double dvalue;
        infos.Clear();
        //DisplayArrayString(data);
        for(unsigned i=0;i<data.GetCount();i++)
        {
                //ATOM   NO.   X         Y         Z
                tokens.Clear();
                tokens=wxStringTokenize(data[i],delim,wxTOKEN_STRTOK);
                info=AtomInfo();
                //atom
                info.type=tokens[0];
                //x
                tokens[2].ToDouble(&dvalue);
                info.x=dvalue;
                //y
                tokens[3].ToDouble(&dvalue);
                info.y=dvalue;
                //z
                tokens[4].ToDouble(&dvalue);
                info.z=dvalue;
                //add info
                infos.Add(info);
        }
}
