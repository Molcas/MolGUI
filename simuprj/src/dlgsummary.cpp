/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsummary.cpp: implementation of the DlgSummary class.
//
//////////////////////////////////////////////////////////////////////

#include "dlgsummary.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgSummary, wxDialog)
    EVT_CLOSE(DlgSummary::OnClose)
END_EVENT_TABLE()
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DlgSummary::DlgSummary(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos,
            const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style)
{
        CreateGUIControls();
}

DlgSummary::~DlgSummary()
{

}

void DlgSummary::CreateGUIControls()
{
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        SetSizer(bsTop);
        pstSummary[0]= new wxStaticText(this, wxID_ANY,wxEmptyString,
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

        wxBoxSizer* bsDetail = new wxBoxSizer(wxHORIZONTAL);

        //wxBoxSizer* bsLeft = new wxBoxSizer(wxVERTICAL);
        wxGridSizer* gsLeft = new wxGridSizer(12, 1, 0, 0);
        int i;
        for(i=0;i<12;i++)
        {
                pstSummary[i+1]=new wxStaticText(this, wxID_ANY,wxEmptyString,
                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
                gsLeft->Add(pstSummary[i+1], 0, wxALIGN_LEFT/*|wxALIGN_CENTER_HORIZONTAL*/, 5);
        }

        //wxBoxSizer* bsRight = new wxBoxSizer(wxVERTICAL);
        bsDetail->Add(gsLeft,0,wxALL|wxALIGN_CENTER,5);
        wxButton*        btnOK=new wxButton(this, wxID_OK, wxEmptyString, wxPoint(10, 10), wxDefaultSize);

        bsTop->Add(pstSummary[0],0,wxALL|wxALIGN_CENTER,5);
        bsTop->Add(bsDetail,0,wxALL|wxALIGN_CENTER,5);
        bsTop->Add(btnOK,0,wxALL|wxALIGN_CENTER,5);
        Layout();
        bsTop->Fit(this);
        bsTop->SetSizeHints(this);
//        Refresh();
}

void DlgSummary::DoOk()
{
        if(IsModal())
        {
                EndModal(wxID_OK);
        }
        else
        {
                SetReturnCode(wxID_OK);
                this->Show(false);
        }
}

void DlgSummary::OnClose(wxCloseEvent &event)
{
        DoOk();
}

void DlgSummary::AppendSummary(wxString sumfile)
{
        wxFileInputStream fis(sumfile);
    wxTextInputStream tis(fis);
         wxString strLine;
        int i=0;
        strLine=tis.ReadLine();
         while(!fis.Eof() && i<12)
         {
      //  wxMessageBox(strLine);
                if(!strLine.IsNull()&&!strLine.IsEmpty()){
            pstSummary[i]->SetLabel(strLine);
            i++;}
                strLine=tis.ReadLine();
        }
        SetSize(wxSize(400,100+20*i));
        Layout();
//        Refresh();
}

wxArrayString DlgSummary::GetSummary()
{
        wxArrayString ret;
        for(int i=0;i<12;i++)
        {
                ret.Add(pstSummary[i]->GetLabel());
        }
        return ret;
}
