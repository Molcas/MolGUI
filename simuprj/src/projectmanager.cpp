/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
*               2021, Ignacio Fdez. Galván                             *
***********************************************************************/

#include "projectmanager.h"

#include <wx/filename.h>
#include <wx/dir.h>
#include <wx/tokenzr.h>
#include "envutil.h"
#include "fileutil.h"
#include "tools.h"
#include "stringutil.h"
#include "appglobals.h"

#include "manager.h"
#include "viewmanager.h"
#include "projectfiles.h"

#include "envsettings.h"
#include "ctrlinst.h"
#include "ctrlids.h"
#include "molvieweventhandler.h"
#include "createprjpnl.h"
#include "simuprjinfoview.h"
#include "mingutil.h"
////////////////
#include "simuprjfrm.h"
#include "dlgsshinfo.h"
#include "resource.h"
#include "molcasfiles.h"
#include "dlgjobname.h"
#include "dlgoutput.h"
#include "xmlfile.h"
#include "dlgremotelist.h"

//end   : hurukun : 1/12/2009

static ProjectManager *managerInstance = NULL;

//int itemType = -1;

int CTRL_PRJMAN_ID_PROJECT_TREE = wxNewId();
int POPUPMENU_PRJMAN_ID_CLOSE = wxNewId();
int POPUPMENU_PRJMAN_ID_COPY = wxNewId();
int POPUPMENU_PRJMAN_ID_RELOAD = wxNewId();
int POPUPMENU_PRJMAN_ID_REMOVE = wxNewId();
int POPUPMENU_PRJMAN_ID_RENAME = wxNewId();
int POPUPMENU_PRJMAN_ID_SAVE = wxNewId();
int POPUPMENU_PRJMAN_ID_SAVEAS = wxNewId();
int POPUPMENU_PRJMAN_ID_DELETE = wxNewId();
int POPUPMENU_PRJMAN_ID_PROJECT_SETTINGS = wxNewId();
int POPUPMENU_PRJMAN_ID_IMPORT_MOLECULE = wxNewId();
int POPUPMENU_PRJMAN_ID_OPEN = wxNewId();
int POPUPMENU_PRJMAN_ID_PASTE = wxNewId();
int POPUPMENU_PRJMAN_ID_REMOVE_PROJECT_RESULT = wxNewId();
//......................................................................................//
//now there are 7 items, if the num changes, the SimuProject' member need to be updated.
int POPUPMENU_PRJMAN_ID_SHOW_INPUT = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_OUTPUT = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_ERROR = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_DENSITY = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_GSSORB = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_SCFORB = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_ENERGY = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_OPTIMIZATION = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_FREQUENCE = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_DUMP = wxNewId();

int POPUPMENU_PRJMAN_ID_SHOW_PEGAMOID = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT = wxNewId();
int POPUPMENU_PRJMAN_ID_SHOW_MOLDEN = wxNewId();

BEGIN_EVENT_TABLE(ProjectManager, wxEvtHandler)
    EVT_TREE_BEGIN_LABEL_EDIT(CTRL_PRJMAN_ID_PROJECT_TREE, ProjectManager::OnBeginEditTreeItem)
    EVT_TREE_END_LABEL_EDIT(CTRL_PRJMAN_ID_PROJECT_TREE, ProjectManager::OnEndEditTreeItem)
    EVT_TREE_ITEM_ACTIVATED(CTRL_PRJMAN_ID_PROJECT_TREE, ProjectManager::OnTreeItemActivated)
    EVT_TREE_ITEM_MENU(CTRL_PRJMAN_ID_PROJECT_TREE, ProjectManager::OnTreeItemMenu)
    EVT_TREE_SEL_CHANGED(wxID_ANY, ProjectManager::OnTreeItemSel)
    //EVT_TREE_ITEM_RIGHT_CLICK(CTRL_PRJMAN_ID_PROJECT_TREE, ProjectManager::OnTreeItemMenu)
    //EVT_RIGHT_DOWN(ProjectManager::OnRightClick)

    EVT_MENU(MENU_ID_FILE_ADDPROJECT, SimuPrjFrm::OnFileAdd)
    EVT_MENU(MENU_ID_FILE_ADDLEVEL, SimuPrjFrm::OnFileAdd)
    EVT_MENU(MENU_ID_FILE_EXISTITEM, SimuPrjFrm::OnFileAdd)
    EVT_MENU(POPUPMENU_PRJMAN_ID_CLOSE, SimuPrjFrm::OnFileClose)
    EVT_MENU(POPUPMENU_PRJMAN_ID_COPY, ProjectManager::OnPopCopy)
    EVT_MENU(POPUPMENU_PRJMAN_ID_DELETE, ProjectManager::OnPopDelete)
    EVT_MENU(POPUPMENU_PRJRES_ID_TEXT, ProjectManager::OnPopDispJobResult)
    EVT_MENU(POPUPMENU_PRJRES_ID_GV, ProjectManager::OnPopDispJobResult)
    EVT_MENU(POPUPMENU_PRJMAN_ID_OPEN, ProjectManager::OnPopOpen)
    EVT_MENU(POPUPMENU_PRJMAN_ID_PASTE, ProjectManager::OnPopPaste)
    EVT_MENU(POPUPMENU_PRJMAN_ID_RELOAD, ProjectManager::OnPopReload)
    EVT_MENU(POPUPMENU_PRJMAN_ID_REMOVE, ProjectManager::OnPopRemove)
    EVT_MENU(POPUPMENU_PRJMAN_ID_RENAME, ProjectManager::OnPopRename)
    EVT_MENU(POPUPMENU_PRJMAN_ID_SAVE, SimuPrjFrm::OnFileSave)
    EVT_MENU(POPUPMENU_PRJMAN_ID_SAVEAS, SimuPrjFrm::OnFileSaveAs)
    EVT_MENU(POPUPMENU_PRJMAN_ID_SHOW_PEGAMOID, ProjectManager::OnPegaOpen)
    EVT_MENU(POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT, ProjectManager::OnOthersoftOpen)
    EVT_MENU(POPUPMENU_PRJMAN_ID_SHOW_MOLDEN, ProjectManager::OnMoldenOpen)
    EVT_MENU_RANGE(POPUPMENU_PRJMAN_ID_SHOW_INPUT, POPUPMENU_PRJMAN_ID_SHOW_DUMP, ProjectManager::OnShowJobItem)

    EVT_MENU(POPUPMENU_PRJMAN_ID_IMPORT_MOLECULE, ProjectManager::OnImportMolecule)
    EVT_MENU(POPUPMENU_PRJMAN_ID_REMOVE_PROJECT_RESULT, ProjectManager::OnRemoveProjectResult)
END_EVENT_TABLE()

ProjectManager::ProjectManager()
{
    m_pActiveProject = NULL;
    m_pActivePrjGrp = NULL;
    m_pPrjGrps = new PrjGrpArray;
    m_pPrjGrps->Clear();
    m_pProjects = new ProjectsArray;
    m_pProjects->Clear();
    m_pTree = NULL;
    m_pImages = NULL;
    m_isLoadingWorkspace = false;
    InitTree();
    // Event handling. This must be THE LAST THING activated on startup.
    Manager::Get()->GetAppWindow()->PushEventHandler(this);
}
ProjectManager::~ProjectManager()
{
    Manager::Get()->GetAppWindow()->RemoveEventHandler(this);
    if(m_pImages != NULL)
        delete m_pImages;
    m_pImages = NULL;
    if(m_pTree != NULL)
        m_pTree->Destroy();
    m_pTree = NULL;
    int i = 0;
    int count = m_pPrjGrps->GetCount();
    for(i = 0; i < count; i++) {
        m_pActivePrjGrp = m_pPrjGrps->Item(i);
        if(m_pActivePrjGrp != NULL)
            delete m_pActivePrjGrp;
    }
    m_pPrjGrps->Clear();
    delete m_pPrjGrps;
    m_pPrjGrps = NULL;
    m_pActivePrjGrp = NULL;

    count = m_pProjects->GetCount();
    for(i = 0; i < count; i++) {
        m_pActiveProject = m_pProjects->Item(i);
        if(m_pActiveProject != NULL)
            delete m_pActiveProject;
    }
    m_pProjects->Clear();
    delete m_pProjects;
    m_pProjects = NULL;
    m_pActiveProject = NULL;
}

ProjectManager *ProjectManager::Get(void)
{
    if(!managerInstance)
        managerInstance = new ProjectManager;
    return managerInstance;
}
bool ProjectManager::CanUndo()
{
    MolView *pView = MolViewEventHandler::Get()->GetMolView();
    if(pView) {
        CtrlInst *pCtrlInst = pView->GetData()->GetActiveCtrlInst();
        bool returnvalue = pCtrlInst->CanUndo();
        if(pCtrlInst->IsDirty())returnvalue = true;
        return returnvalue;
    }
    else
        return false;
}
void ProjectManager::Free(void)
{
    if(managerInstance != NULL)
        delete managerInstance;
    managerInstance = NULL;
}
void ProjectManager::InitTree(void)
{
    static const wxString imgs[] = {
        wxT("workspace.png"),
        wxT("project.png"),
        wxT("project-closed.png"),
        wxT("molecule.png"),
        wxT("job-info.png"),
        wxT("job-results.png"),
        wxT("job-result-exist.png"),
        wxT("job-result-notexist.png"),
        wxT("group.png"),
        wxEmptyString
    };

    m_pImages = new wxImageList(16, 16);
    m_pTree = new wxTreeCtrl(Manager::Get()->GetAppWindow(), CTRL_PRJMAN_ID_PROJECT_TREE, wxDefaultPosition,
                             wxDefaultSize, wxTR_EDIT_LABELS | wxTR_DEFAULT_STYLE | wxBORDER_SUNKEN);

    int i = 0;
    wxBitmap bmp;
    while (!imgs[i].IsEmpty()) {

        bmp = wxBitmap(EnvUtil::GetPrgResDir() + wxTRUNK_PRJ_IMG_PATH + imgs[i], wxBITMAP_TYPE_PNG);

        m_pImages->Add(bmp);
        i++;
    }
    m_pTree->SetImageList(m_pImages);
    //Manager::Get()->GetViewManager()->GetNotebook()->SetImageList(m_pImages);
    m_pTree->Freeze();
    m_treeRootID = m_pTree->AddRoot(wxT("Workspace"), TREE_IMAGE_WORKSPACE, TREE_IMAGE_WORKSPACE);
    m_pTree->Expand(m_treeRootID);
    m_pTree->Thaw();
}
wxBitmap ProjectManager::GetBitmap(ProjectTreeItemType itemType) const
{
    wxBitmap bitmap;
    switch(itemType) {
    case ITEM_MOLECULE:
        bitmap = m_pImages->GetBitmap(TREE_IMAGE_MOLECULE);
        break;
    case ITEM_JOB_INFO:
        bitmap = m_pImages->GetBitmap(TREE_IMAGE_JOB_INFO);
        break;
    case ITEM_JOB_RESULT_SUMMARY:
    case ITEM_JOB_RESULT_OUTPUT:
    case ITEM_JOB_RESULT_DENSITY:
    case ITEM_JOB_RESULT_FREQUENCE:
        bitmap = m_pImages->GetBitmap(TREE_IMAGE_JOB_RESULT_EXIST);
        break;
    default:
        break;
    }

    return bitmap;
}
void    ProjectManager::AddProject(SimuProject *pProject)
{
    if(pProject == NULL)
        return;
    m_pProjects->Add(pProject);
}
void    ProjectManager::AddPrjGrp(ProjectGroup *pPrjGrp)
{
    if(pPrjGrp == NULL)
        return;
    m_pPrjGrps->Add(pPrjGrp);
}
void     ProjectManager::CloseProjectGroup(ProjectGroup *pg)
{   //delete the projectgroup and all project belong to it;
    m_pActiveProject = NULL;
    int count = m_pPrjGrps->GetCount();
    ProjectGroup   *tmp = NULL;
    if(pg == NULL) { //close all of them
        m_pActivePrjGrp = NULL;
        for(int i = 0; i < count; i++) {
            tmp = m_pPrjGrps->Item(i);
            if(tmp != NULL) //the project group has been opened;
            {
                tmp->Close(true, false);
                delete tmp;
                m_pPrjGrps->RemoveAt(i);
                i--;
                count--;
            }
        }
        m_pPrjGrps->Clear();
        return;
    }
    for(int i = 0; i < count; i++) {
        tmp = m_pPrjGrps->Item(i);
        if(tmp != NULL && tmp == pg) //the project group has been opened;
        {
            if(m_pActivePrjGrp == pg) {
                m_pActivePrjGrp = NULL;
                if(i > 0)
                    m_pActivePrjGrp = m_pPrjGrps->Item(0);
                else if(count > 2)
                    m_pActivePrjGrp = m_pPrjGrps->Item(1);
            }

            pg->Close(true, false);
            if(m_pActivePrjGrp == pg)
                m_pActivePrjGrp = NULL;
            delete pg;
            m_pPrjGrps->RemoveAt(i);//the ProjectGroup object will not be auto-deleted.
            return;
        }
    }
}
void ProjectManager::CloseProject(SimuProject *project)
{
    SimuProject *curPrj = NULL;
    if(project == NULL)//close all
    {
        for (unsigned int i = 0; i < m_pProjects->GetCount() ;  i++) {
            curPrj = m_pProjects->Item(i);
            curPrj->Close(true, true);
            delete curPrj;
        }
        m_pProjects->Clear();
        m_pActiveProject = NULL;
    } else {
        for (unsigned int i = 0; i < m_pProjects->GetCount() ; i++) {
            curPrj = m_pProjects->Item(i);
            if( project == curPrj) {//close specific item;
                curPrj->Close(true, true);
                delete curPrj;
                m_pProjects->RemoveAt(i);
                if(m_pActiveProject == curPrj)
                    m_pActiveProject = NULL;
                return;
            }
        }
    }
}
void ProjectManager::DoJobTerminated(long processId, JobRunningData *pJobData, int status)
{
    SimuProject *pProject = GetProject(pJobData->GetProjectId());
    if(pProject) {
        wxString name = pJobData->GetOutputDir();
        name = FileUtil::GetDirectoryName(name);
        bool isfinal = pJobData->IsFinal();
        //wxMessageBox(wxT("ProjectManager::DoJobTerminated1"));
        wxString runningTime = StringUtil::FormatTime(pJobData->GetEndTime() - pProject->GetFiles(name)->GetStartTime());
        wxString info = wxString::Format(wxT("Process %ld terminated with status %d, running time "), processId, status) + runningTime;
        info = wxT("Project ") + pProject->GetTitle() + wxT(" Job ") + name + wxT(" ") + info;
        if(status != 0 && status != 3)
        {
            wxMessageBox(wxT("Project ") + pProject->GetTitle() + wxT(": Job ") + name +wxT(" FAILED.\nMolcas is probably unavailable.\nPlease check Molcas driver path in Environment Settings."),wxT("Job ERROR"),wxICON_ERROR);
            wxString errorFile = pProject->GetResultFileName(name) + wxT(".error");
            if(wxFile::Exists(errorFile))
            {
                DlgOutput *pdlg = new DlgOutput(Manager::Get()->GetAppWindow(), wxID_ANY);
                pdlg->AppendFile(errorFile, errorFile);
                pdlg->SetLabel(errorFile);
                pdlg->Show(true);
            }else{
                wxMessageBox(wxT("Unable to find error output."),wxT("Job ERROR"),wxICON_ERROR);
            }
        }

        if(status != 3 && pProject->GetFiles(name)->PostJobTerminated(isfinal)) { 
            //status==3 means kill the job manually
            wxString xyzDirchar = pProject->GetResultFileName(name) + wxT(".Opt.xyz");
            if(wxFileExists(xyzDirchar)) {
                MolView *pView = (MolView *)Manager::Get()->GetViewManager()->FindView(pProject->GetProjectId());
                if(pView) {
                    CtrlInst *pCtrlInst = pView->GetData()->GetActiveCtrlInst();
                    pCtrlInst->LoadFile(xyzDirchar);
                    pCtrlInst->SetDirty(true);
                    pView->RefreshGraphics();
                }
                wxString oldXYZFile = pProject->GetResultFileName(name) + wxT(".xyz");
                wxCopyFile(xyzDirchar, oldXYZFile);

            }
            pProject->BuildResultTree(name);

            Tools::ShowStatus(info, 1);
        }
        pProject->BuildResultTree(name);
        pProject->Save();
    }
}
ProjectGroup *ProjectManager:: GetActiveProjectGroup(void)const
{
    return m_pActivePrjGrp;
}
wxArrayString ProjectManager::GetAllPGName(void)const
{
    wxArrayString pgNames;
    pgNames.Clear();

    for(unsigned int i = 0; i < m_pPrjGrps->GetCount(); i++) {
        pgNames.Add(m_pPrjGrps->Item(i)->GetName());
    }
    return pgNames;
}
wxTreeItemId        ProjectManager::GetCopyNodeId()
{
    return m_copyNodeID;
}
wxTreeItemId        ProjectManager::GetCopyJobNodeId()
{
    return m_copyJobNodeID;
}
wxTreeItemId        ProjectManager::GetCopyJobItemNodeId()
{
    return m_copyJobItemNodeID;
}
SimuProject  *ProjectManager::GetSelectedProject(void)
{
    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item && item->GetProject()) {
        return item->GetProject();
    }
    return NULL;
}
SimuProject  *ProjectManager::GetProject(int projectId) const
{
    SimuProject *pProject = NULL;
    unsigned int i = 0;
    for (i = 0; i < m_pProjects->GetCount(); i++) {
        pProject = m_pProjects->Item(i);
        if(pProject->GetProjectId() == projectId) {
            return pProject;
        }
    }
    ProjectGroup *pg = NULL;

    for(i = 0; i < m_pPrjGrps->GetCount(); i++) {
        pg = m_pPrjGrps->Item(i);
        pProject = pg->GetChild(projectId);
        if(pProject != NULL)
            return pProject;
    }
    return NULL;
}
int                 ProjectManager::GetProjectsArrayNum()
{
    return (int)m_pProjects->GetCount();
}
void  *ProjectManager::IsOpen(const wxString &fileName, bool isPG)
{
    if (fileName.IsEmpty())
        return NULL;
    wxString val = fileName.Right(PG_FILE_SUFFIX_LEN);
    SimuProject *p = NULL;
    int i = 0;
    if(val.IsSameAs(wxPG_FILE_SUFFIX))//open a project group
    {
        int count = m_pPrjGrps->GetCount();
        ProjectGroup   *pg = NULL;
        for(i = 0; i < count; i++) {
            pg = m_pPrjGrps->Item(i);
            if(pg != NULL && fileName.IsSameAs(pg->GetFullPathName())) //the project group has been opened;
                return pg;
            if(isPG == false) {
                p = pg->IsOpenPGChd(fileName);
                if(p != NULL)
                    return p;
            }
        }
        count = m_pProjects->GetCount();
        for (i = 0; i < count; i++) {//search the opened no-child projects.
            p = m_pProjects->Item(i);
            if(p->IsPGChild(fileName)) {
                return p;
            }
        }
        return NULL;
    }
    val = fileName.Right(PRJ_FILE_SUFFIX_LEN);

    if(val.IsSameAs(wxPRJ_FILE_SUFFIX)) {
        int count = m_pProjects->GetCount();
        for (i = 0; i < count; i++) {//search the opened no-child projects.
            p = m_pProjects->Item(i);
            if(p != NULL && p->GetFileName().IsSameAs(fileName, platform::windows == false))
                return p;
        }
        count = m_pPrjGrps->GetCount();
        ProjectGroup   *pg = NULL;
        for(i = 0; i < count; i++) {
            pg = m_pPrjGrps->Item(i);
            if(pg != NULL) //the project group has been opened;
            {
                p = pg->GetChild(fileName);
                if(p != NULL)
                    return p;
            }
        }
    }
    return NULL;
}
bool  ProjectManager::IsEmpty()
{
    int count = 0;
    if(m_pPrjGrps != NULL)
        count += m_pPrjGrps->GetCount();
    if(m_pProjects != NULL)
        count += m_pProjects->GetCount();
    return (count == 0);
}
bool        ProjectManager::IsCopyNodeIdOk()
{
    return        (m_copyNodeID.IsOk());
}
bool        ProjectManager::IsCopyJobNodeIdOk()
{
    return        (m_copyJobNodeID.IsOk());
}
bool        ProjectManager::IsCopyJobItemNodeIdOk()
{
    return        (m_copyJobItemNodeID.IsOk());
}
SimuProject  *ProjectManager::LoadProject(const wxString &fullPathName, bool activateIt)
{
    //wxMessageBox(wxT("LoadProject:")+fullPathName);
    wxString title = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
    SimuProject *pProject = new SimuProject(fullPathName, m_treeRootID);

    if(pProject == NULL)
        return NULL;
    AddProject(pProject);//record the project ;
    pProject->SetDirty(false);
    if(activateIt)
        SetActiveProject(pProject);
    return pProject;
}
ProjectGroup *ProjectManager::LoadPrjGrp(const wxString &fileName, bool activateIt)
{
    ProjectGroup *acpg = NULL;

    wxTreeCtrl *tree = Manager::Get()->GetProjectManager()->GetTree();
    if(tree == NULL)
        return NULL;
    wxString fullPathName = fileName;
    acpg = new ProjectGroup(fullPathName, tree);
    if(acpg == NULL)
        return NULL;
    if(acpg->Load()) {
        SetActiveProjectGroup(acpg);
        AddPrjGrp(acpg);
    } else {
        delete acpg;
        acpg = NULL;
    }
    acpg->SetDirty(false);
    return acpg;
}
void ProjectManager::SaveProject(SimuProject *pProject)
{
    if(pProject) {
        pProject->Save();
    }
}
void ProjectManager::SaveAll()
{
    ProjectGroup *pgtmp = NULL;
    int i = 0;
    int pgcount = m_pPrjGrps->GetCount();
    for(i = 0; i < pgcount; i++) {
        pgtmp = m_pPrjGrps->Item(i);
        pgtmp->Save();
    }
    pgtmp = NULL;
    SimuProject *pProject = NULL;
    int pcount = m_pProjects->GetCount();
    for(i = 0; i < pcount; i++) {
        pProject = m_pProjects->Item(i);
        pProject->Save();
    }
    pProject = NULL;
}
void ProjectManager::SetActiveProject(SimuProject *project)
{
    if (project == m_pActiveProject || m_pTree == NULL)
        return;
    if (m_pActiveProject)
        m_pTree->SetItemBold(m_pActiveProject->GetProjectNode(), false);
    m_pActiveProject = project;
    if (m_pActiveProject) {
        wxTreeItemId tid = m_pActiveProject->GetProjectNode();
        if(tid)
            m_pTree->SetItemBold(m_pActiveProject->GetProjectNode(), true);
    }
    if (m_pActiveProject)
        m_pTree->EnsureVisible(m_pActiveProject->GetProjectNode());
}
void ProjectManager::SetActiveProjectGroup(ProjectGroup *pgroup)
{
    if (pgroup == m_pActivePrjGrp || m_pTree == NULL)
        return;
    if (m_pActivePrjGrp)
        m_pTree->SetItemBold(m_pActivePrjGrp->GetRootNode(), false);
    m_pActivePrjGrp = pgroup;
    if (m_pActivePrjGrp) {
        wxTreeItemId tid = m_pActivePrjGrp->GetRootNode();
        if(tid)
            m_pTree->SetItemBold(tid, true);
        m_pTree->EnsureVisible(tid);
    }
}
void        ProjectManager::SetCopyNodeId(wxTreeItemId itemId)
{
    m_copyNodeID = itemId;
}
void        ProjectManager::SetCopyJobNodeId(wxTreeItemId itemId)
{
    m_copyJobNodeID = itemId;
}
void        ProjectManager::SetCopyJobItemNodeId(wxTreeItemId itemId)
{
    m_copyJobItemNodeID = itemId;
}
void ProjectManager::OnShowJobItem(wxCommandEvent &event)
{
    //    bool   isCheck=event.IsChecked();
    //    int     id=event.GetId();
    int i = 0;
    wxTreeItemId    itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item == NULL) return;
    wxString    jobName = item->GetTitle();

    int     count = m_pProjects->GetCount();
    SimuProject *pP = NULL;
    for(i = 0; i < count; i++) {
        pP = m_pProjects->Item(i);
        if(pP != NULL)
            pP->LoadJobs();
    }
    ProjectsArray *tmpPArray = NULL;
    count = m_pPrjGrps->GetCount();
    ProjectGroup   *pPG = NULL;
    pP = NULL;
    for(i = 0; i < count; i++) {
        pPG = m_pPrjGrps->Item(i);
        if(pPG != NULL) {
            tmpPArray = pPG->GetChildArray();
            for(unsigned int j = 0; j < tmpPArray->GetCount(); j++) {
                pP = tmpPArray->Item(j);
                if(pP != NULL)
                    pP->LoadJobs();
            }
        }
    }
}
void ProjectManager::ShowPopupMenu(wxTreeItemId itemId, const wxPoint &pos)
{
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item == NULL)
        return;
    SimuProject *pP = item->GetProject();
    if(pP != NULL)
        SetActiveProject(pP);
    ProjectGroup   *pPG = item->GetProjectGroup();
    if(pPG != NULL)
        SetActiveProjectGroup(pPG);

    m_pTree->SelectItem(itemId);

    wxString caption;
    //        ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item == NULL) return;
    wxMenu menu;
    ProjectTreeItemType type = item->GetType();
    // std::cout << "ProjectManager::ShowPopupMenu: " << type << std::endl;
    wxMenu *addmenu = new wxMenu();
    addmenu->Append(MENU_ID_FILE_ADDPROJECT, wxT("New project  ..."), wxT("Add new project into the project group."));
    addmenu->Append(MENU_ID_FILE_ADDLEVEL, wxT("New group  ..."), wxT("Add new level into the project group."));
    addmenu->Append(MENU_ID_FILE_EXISTITEM, wxT("Exist Item ..."), wxT("Add project group or project into the project group."));

    if(type == ITEM_PROJECTGROUP) {
        menu.Append(POPUPMENU_PRJMAN_ID_SAVE, wxT("Save"), wxT("Save the item Recursively."));
        if(item->GetProjectGroup()->IsDirty())
            menu.Enable(POPUPMENU_PRJMAN_ID_SAVE, true);
        else
            menu.Enable(POPUPMENU_PRJMAN_ID_SAVE, false);
        menu.AppendSubMenu(addmenu, wxT("Add ..."));
        menu.Append(POPUPMENU_PRJMAN_ID_COPY, wxT("Copy"), wxT("Copy the project group."));
        menu.Append(POPUPMENU_PRJMAN_ID_PASTE, wxT("Paste"), wxT("Insert the copyed item."));
        menu.Append(POPUPMENU_PRJMAN_ID_RELOAD, wxT("Reload"), wxT("Reload the project group."));
        menu.AppendSeparator();
        menu.Append(POPUPMENU_PRJMAN_ID_CLOSE, wxT("Close"), wxT("Close the project group."));
        menu.Append(POPUPMENU_PRJMAN_ID_DELETE, wxT("Delete"), wxT("Physically delete the project under this item."));
        menu.AppendSeparator();
    } else if(type == ITEM_PROJECTDIR) {
        menu.AppendSubMenu(addmenu, wxT("Add ..."));
        menu.Append(POPUPMENU_PRJMAN_ID_COPY, wxT("Copy"), wxT("Copy the selected item."));
        menu.Append(POPUPMENU_PRJMAN_ID_PASTE, wxT("Paste"), wxT("Insert the copyed item."));
        menu.AppendSeparator();
        menu.Append(POPUPMENU_PRJMAN_ID_REMOVE, wxT("Remove"), wxT("Remove the item from the project group."));
        menu.Append(POPUPMENU_PRJMAN_ID_DELETE, wxT("Delete"), wxT("Physically delete the project under this item."));
        menu.AppendSeparator();
    } else if(type == ITEM_PROJECT) {
        menu.Append(POPUPMENU_PRJMAN_ID_COPY, wxT("Copy"), wxT("Copy the project."));
        menu.Append(POPUPMENU_PRJMAN_ID_PASTE, wxT("Paste"), wxT("Paste the jobs."));
        if(item->GetProject() != NULL && item->GetProject()->IsChild() == true)
            menu.Append(POPUPMENU_PRJMAN_ID_REMOVE, wxT("Remove"), wxT("Remove the project from the project group."));
        else
            menu.Append(POPUPMENU_PRJMAN_ID_CLOSE, wxT("Close"), wxT("Close the project ."));
        menu.Append(POPUPMENU_PRJMAN_ID_DELETE, wxT("Delete"), wxT("Physically delete the project file."));
        menu.AppendSeparator();
        menu.Append(POPUPMENU_PRJMAN_ID_RELOAD, wxT("Reload"), wxT("Reload the project."));
    } else {
        if(type == ITEM_JOB_RESULT) {
            menu.Append(POPUPMENU_PRJMAN_ID_COPY, wxT("Copy"));
            menu.Append(POPUPMENU_PRJMAN_ID_PASTE, wxT("Paste"));
            menu.AppendSeparator();
            menu.Append(POPUPMENU_PRJMAN_ID_DELETE, wxT("Delete"));
        } else if(type == ITEM_MOPACOUTPUT || type == ITEM_MOPACDIR) {
            menu.Append(POPUPMENU_PRJMAN_ID_DELETE, wxT("Delete"));
        } else if(type >= ITEM_JOB_RESULT_OUTPUT_MOLCAS && type <= ITEM_JOB_RESULT_SCFORB_MOLCAS) {
            wxString itemName = item->GetTitle();
            wxString typeFile = FileUtil::GetMolcasFileType(itemName);
            // wxString itemNamePath = item->GetType;
            // std::cout << "ShowPopupMenu type 18 itemName: " << itemName << " typeFile " << typeFile << std::endl;
            // std::cout << "ShowPopupMenu type 18 itemNamepath: " << itemNamePath << std::endl;
            if(typeFile==wxT("xyz")){
                menu.Append(POPUPMENU_PRJMAN_ID_SHOW_MOLDEN, wxT("Open with Molden"));
                menu.Append(POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT, wxT("Open with Other"));
                wxString ospath = EnvSettingsData::Get()->GetOthersoftPath();
                if (ospath == wxEmptyString){
                    menu.Enable(POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT, false);
                }
            }
            if(typeFile==wxT("molden")){
                menu.Append(POPUPMENU_PRJMAN_ID_SHOW_PEGAMOID, wxT("Open with Pegamoid"));
                menu.Append(POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT, wxT("Open with Other"));
                menu.Append(POPUPMENU_PRJMAN_ID_SHOW_MOLDEN, wxT("Open with Molden"));
                if (itemName.Right(11).IsSameAs(wxT(".geo.molden"))){
                    menu.Enable(POPUPMENU_PRJMAN_ID_SHOW_PEGAMOID, false);
                }
                wxString ospath = EnvSettingsData::Get()->GetOthersoftPath();
                if (ospath == wxEmptyString){
                    menu.Enable(POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT, false);
                }
            }
            if(typeFile==wxT("h5")){
                menu.Append(POPUPMENU_PRJMAN_ID_SHOW_PEGAMOID, wxT("Open with Pegamoid"));
                menu.Append(POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT, wxT("Open with Other"));
                wxString ospath = EnvSettingsData::Get()->GetOthersoftPath();
                if (ospath == wxEmptyString){
                    menu.Enable(POPUPMENU_PRJMAN_ID_SHOW_OTHERSOFT, false);
                }
            }
            
        //---------------------------
            menu.AppendSeparator();
            menu.Append(POPUPMENU_PRJMAN_ID_COPY, wxT("Copy"));
        } else if(type >= ITEM_JOB_RESULT_OUTER_FILE && type != ITEM_JOB_RESULT_OUTER_INPUT_MOLCAS && type <= ITEM_JOB_RESULT_OUTER_SCFORB_MOLCAS) {
            menu.Append(POPUPMENU_PRJMAN_ID_DELETE, wxT("Delete"));
        }
        wxString fileName = item->GetFileName();
        // std::cout << "ShowPopupMenu type: " << fileName << std::endl;
        if(!fileName.IsEmpty()) {
            if(wxFileName::FileExists(fileName)) {
                menu.Append(POPUPMENU_PRJMAN_ID_OPEN, wxT("Open"));
            } else {
                if(type == (ProjectTreeItemType)TREE_IMAGE_JOB_RESULTS) {
                    menu.Append(POPUPMENU_PRJMAN_ID_OPEN, wxT("New"));
                }
            }
        }
    }
    if(type == ITEM_PROJECT || type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR || type == ITEM_JOB_RESULT) {
        menu.Append(POPUPMENU_PRJMAN_ID_RENAME, wxT("Rename"), wxT("Rename the item."));
        menu.Append(POPUPMENU_PRJMAN_ID_SAVEAS, wxT("Save As"), wxT("Save the item Recursively to a different path."));
    }
    if(type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR) {
        if(m_copyNodeID.IsOk())
            menu.Enable(POPUPMENU_PRJMAN_ID_PASTE, true);
        else
            menu.Enable(POPUPMENU_PRJMAN_ID_PASTE, false);
    }
    if(type == ITEM_PROJECT) {
        if(m_copyJobNodeID.IsOk())
            menu.Enable(POPUPMENU_PRJMAN_ID_PASTE, true);
        else
            menu.Enable(POPUPMENU_PRJMAN_ID_PASTE, false);
    }
    if(type == ITEM_JOB_RESULT) {
        if(m_copyJobItemNodeID.IsOk())
            menu.Enable(POPUPMENU_PRJMAN_ID_PASTE, true);
        else
            menu.Enable(POPUPMENU_PRJMAN_ID_PASTE, false);
    }

    if (menu.GetMenuItemCount() != 0)
        m_pTree->PopupMenu(&menu, pos);
}
void ProjectManager::ShowSelectedItemView(wxString importFileName)
{

    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item) {
        SimuProject *pProject = item->GetProject();
        if(pProject == NULL)
            return;
        SetActiveProject(pProject);
        ProjectTreeItemType type = item->GetType();
        // std::cout <<  "ShowSelectedItemView: " << type << std::endl;
        //        wxMessageBox(wxT("type == ")+wxString::Format(wxT("%d"),type));
        if(type == ITEM_PROJECT || type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR) {
            // std::cout <<  "ShowSelectedItemView ITEM_PROJECT: " << type << std::endl;
            if(!importFileName.IsEmpty()) {
                //               item->SetType(ITEM_MOLECULE);
                Manager::Get()->GetViewManager()->ShowMolView(item, importFileName, true);
            }
        } else if(type == ITEM_NEW_JOB) {//show the job view, and do the job;
            if(pProject->Save() == false)
                return;
            if(!pProject->HasMolecule()) {
                wxMessageBox(wxT("Sorry, please create the molecule at first"), wxT("Error"));
                return;
            }
            AbstractView *view = Manager::Get()->GetViewManager()->FindView(pProject->GetProjectId());
            if(view)
                Manager::Get()->GetViewManager()->SaveViews(view);
            wxString jobName = SetJobName(item);
            if(jobName.IsEmpty())//hurukun 0418
            {
                return;
            }

            wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
            if(wxFileExists(fileName))
                wxRemoveFile(fileName);

            if(Manager::Get()->GetViewManager()->ShowJobView(item, jobName, true, true)) {
                SubmitJob(pProject, jobName, true);
            }
            // std::cout <<  "ShowSelectedItemView looking for 18: " << type << std::endl;
        } else if(type == ITEM_JOB_INFO) {
            // wxMessageBox(wxT("type == ITEM_JOB_INFO")+wxString::Format(wxT("%d"),type));
            wxString  jobName = item->GetTitle();
            wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
            if(wxFileExists(fileName))
                wxRemoveFile(fileName);

            if(Manager::Get()->GetViewManager()->ShowJobView(item, jobName, true, false)) {
                SubmitJob(pProject, jobName, false);
            }
            // std::cout <<  "ShowSelectedItemView looking for 18 ITEM_JOB_INFO: " << type << std::endl;
        } else {
            //   wxMessageBox(wxT("type == ELSE")+wxString::Format(wxT("%d"),type));
            if(importFileName.IsEmpty() && (item->GetType() != ITEM_MOLECULE)) //job results
            {
                //        wxTreeItemId itemId = m_pTree->GetSelection();
                pProject->ShowResultItem(item);
                return;
            } else
                // std::cout <<  "ShowSelectedItemView: " << type << std::endl;
                Manager::Get()->GetViewManager()->ShowMolView(item, importFileName, true);//show molecule
            // std::cout <<  "ShowSelectedItemView looking for 18 final?: " << type << std::endl;
        }
        // std::cout <<  "ShowSelectedItemView looking for 18 final end: " << std::endl;
    } else {
        // std::cout <<  "ShowSelectedItemView looking for 18 wxBell: " << std::endl;
        wxBell();
    }
    // std::cout <<  "ShowSelectedItemView looking outside: " << std::endl;
}
wxString     ProjectManager::SetJobName(ProjectTreeItemData *item)
{
    wxString    jobName = wxEmptyString;
    if(item->GetProject() == NULL)
        return jobName;
    DlgJobName  dlg(Manager::Get()->GetAppWindow(), item->GetProject());
    if(dlg.ShowModal() == wxID_OK)
    {
        jobName = dlg.GetJobName();
    }
    return  jobName;
}
bool ProjectManager::SubmitJob(void)
{
    SimuProject *project = GetActiveProject();
    if(project == NULL) {
        Tools::ShowError(wxT("Please select a project!"));
        return false;
    }
    wxTreeItemId        itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item == NULL)        return false;
    wxString        jobName = wxEmptyString;
    ProjectTreeItemType type = item->GetType();
    if(type == ITEM_JOB_RESULT || type == ITEM_JOB_INFO || type == ITEM_JOB_RESULT_INPUT_MOLCAS) { //run the job
        jobName = item->GetTitle();
    } else {
        wxMessageBox(wxT("Please select a job at first."));
        return false;
    }
    //wxMessageBox(jobName);
    //wxMessageBox(wxT("ProjectManager::SubmitJob(void)"));
    if(jobName.IsEmpty())
        return false;
    return SubmitJob(project, jobName, false);
}

void ProjectManager::PrepareInputFile(wxString inputfile)
{
    wxArrayString strarray;
    //wxArrayString strreturn;
    //wxFileName finputfile(inputfile);
    wxFileName *pxyzfile;
    wxString tmpinputfile = inputfile + wxT(".bak");
    if(!wxFileExists(tmpinputfile))
        wxCopyFile(inputfile, tmpinputfile, true);
    else
        wxCopyFile(tmpinputfile, inputfile, true);
    wxFileInputStream fis(inputfile);
    wxTextInputStream tis(fis);
    wxString strLine;
    wxString xyzfile;
    bool isfile = false;
#ifdef __DEBUG__
    fprintf(stderr, "PrepareInputFile");
#endif
    while(!fis.Eof()) {
        strLine = tis.ReadLine();
        strLine.Trim();
        strLine.Trim(false);
        //wxMessageBox(strLine);
        if(isfile) {
            isfile = false;
            xyzfile = strLine;
            pxyzfile = new wxFileName(xyzfile);
            strLine = wxT("/cygdrive/") + strLine.Left(1) + pxyzfile->GetPathWithSep(wxPATH_UNIX) + pxyzfile->GetFullName();
        } else if(strLine.Cmp(wxT("LOAD")) == 0 || strLine.Cmp(wxT("FILELOAD")) == 0 || strLine.Cmp(wxT("FILE")) == 0 || strLine.Cmp(wxT("HEXT")) == 0 || strLine.Cmp(wxT("POTE")) == 0 || strLine.Cmp(wxT("OBSE")) == 0) {
            isfile = true;
        } else if(strLine.Left(5).Cmp(wxT("COORD")) == 0) {
            xyzfile = strLine.Right(strLine.Len() - 6);
            pxyzfile = new wxFileName(xyzfile);
            strLine = wxT("COORD=/cygdrive/") + xyzfile.Left(1) + pxyzfile->GetPathWithSep(wxPATH_UNIX) + pxyzfile->GetFullName();
        } else {
            int pos = 0;
            pos = StringUtil::StringMatch(strLine, wxT(">include "));
            if(pos >= 0) {
                wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                xyzfile = strArr[0];
                pxyzfile = new wxFileName(xyzfile);
                strLine = wxT(">>>> include /cygdrive/") + strLine.Left(1) + pxyzfile->GetPathWithSep(wxPATH_UNIX) + pxyzfile->GetFullName();
            } else {
                pos = StringUtil::StringMatch(strLine, wxT(">copy "));
                if(pos >= 0) {
                    wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                    xyzfile = strArr[0];
                    pxyzfile = new wxFileName(xyzfile);
                    strLine = wxT(">>>>>copy /cygdrive/") + strLine.Left(1) + pxyzfile->GetPathWithSep(wxPATH_UNIX) + pxyzfile->GetFullName();
                } else {
                    pos = StringUtil::StringMatch(strLine, wxT(">link -force "));
                    if(pos >= 0) {
                        wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                        xyzfile = strArr[0];
                        pxyzfile = new wxFileName(xyzfile);
                        strLine = wxT(">>>>>link -force /cygdrive/") + strLine.Left(1) + pxyzfile->GetPathWithSep(wxPATH_UNIX) + pxyzfile->GetFullName();
                    } else {
                        pos = StringUtil::StringMatch(strLine, wxT(">link force "));
                        if(pos >= 0) {
                            wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                            xyzfile = strArr[0];
                            pxyzfile = new wxFileName(xyzfile);
                            strLine = wxT(">>>>>link force /cygdrive/") + strLine.Left(1) + pxyzfile->GetPathWithSep(wxPATH_UNIX) + pxyzfile->GetFullName();
                        } else {
                            pos = StringUtil::StringMatch(strLine, wxT(">link "));
                            if(pos >= 0) {
                                wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                                xyzfile = strArr[0];
                                pxyzfile = new wxFileName(xyzfile);
                                strLine = wxT(">>>>>link /cygdrive/") + strLine.Left(1) + pxyzfile->GetPathWithSep(wxPATH_UNIX) + pxyzfile->GetFullName();
                            }
                        }
                    }
                }
            }
        }
        strarray.Add(strLine);
    }

    wxFileOutputStream fos(inputfile);
    wxTextOutputStream tos(fos);
    //wxMessageBox(wxT("output"));
    for(unsigned int i = 0; i < strarray.GetCount(); i++) {
        tos << strarray[i] << endl;
    }
}

bool ProjectManager::SubmitJob(SimuProject *project, wxString jobName, bool newjob)
{
    if(project == NULL || jobName.IsEmpty())
        return false;
    if(ViewManager::Get()->GetActiveView() == ViewManager::Get()->FindView(project->GetProjectId())) {
        wxCommandEvent        event;
        event.SetId( MENU_ID_BUILD_VIEW);
        MolViewEventHandler::Get()->ProcessMenuEvent(event);
    }

    wxString resultName = jobName;
    if(!FileUtil::CreateDirectory(project->GetResultDir(resultName))) {
        Tools::ShowError(wxT("Failed to create dir ") + project->GetResultDir(resultName));
        return false;
    }


    AbstractProjectFiles *pFiles = project->GetFiles(resultName);
    if(pFiles == NULL || !pFiles->CreateInputFile()) {
        Tools::ShowError(wxT("Job canceled or Failed to create input file!"));
        //FileUtil::RemoveDirectory(project->GetResultDir(resultName),true);
        return false;
    }

    wxString execDir;
    if(project->GetType() != PROJECT_MOLCAS) {
        DlgRemoteList *dlgremotelist = new DlgRemoteList(Manager::Get()->GetAppWindow());
        if(dlgremotelist->ShowModal() == wxID_CANCEL)return false;
    }
    execDir = EnvSettingsData::Get()->GetChemSoftDir(project->GetType());

    long remotejob = 0;
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
    if(wxFileExists(fileName)) {
        wxString strLine;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        strLine = tis.ReadLine();
        strLine.ToLong(&remotejob);
    }

    wxString execCommand = pFiles->GetExecCommand();
    //wxMessageBox(execCommand);
    if(!StringUtil::IsEmptyString(execCommand)) {
        execDir +=  platform::PathSep() + execCommand;
    }


    Tools::ShowStatus(wxT("Job is running!"), 1);

#if defined (__WINDOWS__)
    if(project->GetType() == PROJECT_MOLCAS && remotejob != 1) {
        PrepareInputFile(pFiles->GetInputFileName());
        //                        Tools::ShowError(wxT("Sorry!Molcas don't have windows version!Please submit your job to a server which you can use."));
        //                        return false;
    }
    if(project->GetType() != PROJECT_MOLCAS && !wxFileExists(execDir)) {
        Tools::ShowError(execDir + wxT(" does not exist!Please install it and set the enviroment!"));
        return false;
    }
#endif

    wxString execPara = pFiles->GetExecPara();
    //wxMessageBox(execPara);
    wxString execOutputDir = project->GetResultDir(resultName);
    wxString projectName = project->GetTitle();
    JobRunningData *pJobData = new JobRunningData(project->GetProjectId(), project->GetType(), project->GetTitle());
    //        Tools::ShowError(execDir + wxT(" , ")+execPara);
    pJobData->SetJobName(jobName);
    pJobData->SetProjectFile(project->GetFileName());
    pJobData->SetOutputDir(execOutputDir);
    wxString         cmdPara = ((SimuPrjFrm *)(Manager::Get()->GetAppFrame()))->GetCmdLine();
    pJobData->SetCommand(execDir, execPara + wxT(" ") + cmdPara);
    //        wxMessageBox(execDir+wxT(", ")+execPara+wxT(", ")+cmdPara);
    pJobData->SetInputFileName(pFiles->GetInputFileName());
    pJobData->SetFinal(pFiles->IsFinal());
    //wxString output = project->GetFiles()->GetExecLogName();
    //Tools::ExecModule(execDir, output + wxT(".log"), output + wxT("_ERROR.log"));
    pFiles->SetStartTime(wxGetLocalTimeMillis());
    wxString cwd = wxGetCwd();
    wxSetWorkingDirectory(execOutputDir);
    Manager::Get()->GetMonitorPnl()->ExecJob(pJobData);
    wxSetWorkingDirectory(cwd);
    //        delete pJobData;

    return true;
}
//****************************************************************************************
//          event handle function
//****************************************************************************************
void ProjectManager::OnBeginEditTreeItem(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    //only allow  editing project title
    if(item == NULL)
    {
        event.Veto();
        return;
    }
    ProjectTreeItemType type = item->GetType();
    if(item && type == ITEM_PROJECT) {
        m_pTree->SetItemText(itemId, item->GetProject()->GetTitle());
        event.Skip();
    } else if(item && type == ITEM_JOB_RESULT) {
        m_pTree->SetItemText(itemId, item->GetTitle());
        event.Skip();
    } else if(item && (type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR)) {
        m_pTree->SetItemText(itemId, item->GetTitle());
        event.Skip();
    } else {
        event.Veto();
    }
}
void ProjectManager::OnEndEditTreeItem(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    wxString label = event.GetLabel();
    label = StringUtil::CheckInput(label, false);
    if(label.IsEmpty()) {
        event.Veto();
        return;
    }
    m_pTree->SetItemText(itemId, label);

    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item == NULL)  return;
    ProjectGroup   *pPG = item->GetProjectGroup();
    ProjectTreeItemType type = item->GetType();

    wxString    fileName = wxEmptyString;
    SimuProject *pProject = item->GetProject();
    wxString    oldTitle = item->GetTitle();
    if((type == ITEM_PROJECT || type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR) && !label.IsEmpty()) {
        //begin : hurukun : 20/11/2009
        if(pProject != NULL && pProject->IsChild() == false) {
            if(pProject->Rename(label) == false) {
                event.Veto();
                return;
            }
            Manager::Get()->GetViewManager()->UpdateProjectTitle(item->GetProject());
            ((SimuPrjFrm *)Manager::Get()->GetAppFrame())->UpdateRecentProjects(fileName);
        } else if(item->GetProjectGroup()->RenameNode(itemId, label)) //failure
        {
            if(item->GetProject() != NULL)
                Manager::Get()->GetViewManager()->UpdateProjectTitle(item->GetProject());
            else {
                fileName = pPG->GetFullPathName();
                ((SimuPrjFrm *)Manager::Get()->GetAppFrame())->UpdateRecentPrjGrps(fileName);
            }
        }
        event.Veto();
    } else if(type == ITEM_JOB_RESULT) {
        if(pProject == NULL) {
            event.Veto();
            return;
        }

        wxString jobFileName = pProject->GetFiles(label)->GetInputFileName(); //pProject->GetResultFileName(label);
        //if(wxFileExists(jobFileName+wxMINPUT_FILE_SUFFIX)){
        if(wxFileExists(jobFileName)) {
            m_pTree->SetItemText(itemId, oldTitle);
            event.Veto();
            return;
        }
        //wxMessageBox(jobFileName);
        //wxMessageBox(item->GetItemVal());
        int itemCount = m_pTree->GetChildrenCount(itemId, false);
        wxTreeItemIdValue        cookie;
        wxTreeItemId        child = m_pTree->GetFirstChild(itemId, cookie);
        item->SetItemVal(label);
        for(int i = 0; i < itemCount; i++) {
            item = child.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(child) : NULL;
            if(item == NULL)        return;
            if(item->GetType() == ITEM_JOB_RESULT_ORB || item->GetType() == ITEM_JOB_RESULT_MOLDEN || item->GetType() == ITEM_JOB_RESULT_OUTER_FILE) {
                int subItemCount = m_pTree->GetChildrenCount(child, false);
                wxTreeItemIdValue        subCookie;
                wxTreeItemId        subChild = m_pTree->GetFirstChild(child, subCookie);
                ProjectTreeItemData *subItem;// = child.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(child) : NULL;
                //                                if(subItem==NULL)  return;
                //                                subItem->SetItemVal(label);
                for(int j = 0; j < subItemCount; j++) {
                    subItem = subChild.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(subChild) : NULL;
                    if(subItem == NULL)        break;
                    subItem->SetItemVal(label);
                    subChild = m_pTree->GetNextChild(child, subCookie);
                }
            }
            item->SetItemVal(label);
            child = m_pTree->GetNextChild(itemId, cookie);
        }
        pProject->RenameJob(oldTitle, label);
        event.Veto();
        //wxMessageBox(item->GetTitle());
    } else {
        event.Skip();
    }
    AbstractView *view = Manager::Get()->GetViewManager()->FindView(PRJINFO_PID);
    if(view != NULL)
        ((SimuPrjInfoView *)view)->UpdateView();
}
void ProjectManager::OnImportMolecule(wxCommandEvent &event)
{
    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item) {
        wxString fileName = Tools::GetFilePathFromUser(Manager::Get()->GetAppWindow(), wxT("Import Molecule File"), wxT("XYZ Files (*.xyz)|*.xyz"), wxFD_OPEN);
        if(!StringUtil::IsEmptyString(fileName)) {
            ShowSelectedItemView(fileName);
        }
    }

}
void ProjectManager::OnRemoveProjectResult(wxCommandEvent &event)
{
    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item && item->GetType() == ITEM_JOB_RESULT) {
        wxMessageDialog dialog(Manager::Get()->GetAppFrame(), wxT("Do you want to remove ") + item->GetTitle() + wxT(" from the project?"),
                               appglobals::appName, wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_INFORMATION);
        if(dialog.ShowModal() == wxID_YES && Manager::Get()->GetViewManager()->CloseViewBPID(item->GetProject()->GetProjectId())) {
            if(item->GetProject()->RemoveResultDir(item->GetTitle())) {
                m_pTree->Delete(itemId);
            }
        }
    }
}
void ProjectManager::OnPopCopy(wxCommandEvent &event)
{
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    wxTreeItemId    itemId = pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData( itemId) : NULL;
    if(item == NULL)
        return;
    ProjectTreeItemType type = item->GetType();
    if(type == ITEM_PROJECT || type == ITEM_PROJECTDIR || type == ITEM_PROJECTGROUP) {
        Manager::Get()->GetProjectManager()->SetCopyNodeId(itemId);
        Manager::Get()->GetProjectManager()->SetCopyJobNodeId(0);
        Manager::Get()->GetProjectManager()->SetCopyJobItemNodeId(0);
    } else if(type == ITEM_JOB_RESULT) {
        Manager::Get()->GetProjectManager()->SetCopyNodeId(0);
        Manager::Get()->GetProjectManager()->SetCopyJobNodeId(itemId);
        Manager::Get()->GetProjectManager()->SetCopyJobItemNodeId(0);
    } else {
        Manager::Get()->GetProjectManager()->SetCopyNodeId(0);
        Manager::Get()->GetProjectManager()->SetCopyJobNodeId(0);
        Manager::Get()->GetProjectManager()->SetCopyJobItemNodeId(itemId);
    }
}
void ProjectManager::OnPopDelete(wxCommandEvent &event)
{
    DoPopDelete();
}
void ProjectManager::DoPopDelete()
{
    wxTreeItemId    itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData( itemId) : NULL;
    if(item == NULL)
        return;
    ProjectGroup *pPG = item->GetProjectGroup();

    wxString        msg;
    if(item->GetType() == ITEM_PROJECTGROUP) {
        msg = wxT("the files of project group: ") + item->GetTitle();
    } else if(item->GetType() == ITEM_PROJECTDIR) {
        msg = wxT("the files of sub-project group: ") + item->GetTitle() + wxT(" of project group: ") + item->GetProjectGroup()->GetName();
    } else if(item->GetType() == ITEM_PROJECT) {
        msg = wxT("the files of project: ") + item->GetTitle();
    } else if(item->GetType() == ITEM_JOB_RESULT) {
        msg = wxT("the files of job: ") + item->GetTitle() + wxT(" of project: ") + item->GetProject()->GetTitle();
    } else if(item->GetType() == ITEM_MOPACOUTPUT) {
        msg = wxT("the result of mopac: ") + item->GetTitle();
    } else if(item->GetType() == ITEM_MOPACDIR) {
        msg = wxT("all the result of mopac");
    } else {
        msg = wxT(" the files related to the selected item");
    }
    wxMessageDialog mDlg(NULL, wxT("Do you really want to delete ") + msg + wxT(" ?"), wxT("Warning"), wxYES_NO);
    if(mDlg.ShowModal() != wxID_YES)
        return;

    if(pPG != NULL) {
        if(item->GetType() == ITEM_PROJECTGROUP) {
            if(m_pActivePrjGrp == pPG)
                m_pActivePrjGrp = NULL;
            wxString  fileDir = pPG->GetPath();
            CloseProjectGroup(pPG);
            FileUtil::RemoveDirectory(fileDir);

        } else
            pPG->DeleteNode(itemId);
        m_pActiveProject = NULL;
    } else if(item->GetType() == ITEM_PROJECT) {
        SimuProject *pP = item->GetProject();
        if(m_pActiveProject == pP)
            m_pActiveProject = NULL;
        if(pP == NULL)
            return;
        wxString  fileDir = pP->GetFileName().BeforeLast(platform::PathSep().GetChar(0));
        CloseProject(pP);
        FileUtil::RemoveDirectory(fileDir);
    } else if(item->GetType() == ITEM_JOB_RESULT) {
        SimuProject *pP = item->GetProject();
        if(pP == NULL)
            return;
        pP->DelJob(item->GetTitle());

        m_pTree->DeleteChildren(itemId);
        m_pTree->Delete(itemId);
    } else if(item->GetType() == ITEM_MOPACOUTPUT) {
        SimuProject *pP = item->GetProject();
        if(pP == NULL)
            return;
        pP->DelMopacJob(item->GetTitle());
        m_pTree->Delete(itemId);
    } else if(item->GetType() == ITEM_MOPACDIR) {
        SimuProject *pP = item->GetProject();
        if(pP == NULL)
            return;
        pP->DelMopacJob(wxT("ALL"));
        m_pTree->DeleteChildren(itemId);
        m_pTree->Delete(itemId);
    } else if(item->GetType() == ITEM_JOB_RESULT_OUTER_FILE) {
        SimuProject *pP = item->GetProject();
        if(pP == NULL)
            return;
        pP->DelJobOuterFile(item->GetTitle(), wxT("ALL"));
        m_pTree->DeleteChildren(itemId);
        m_pTree->Delete(itemId);
    } else if(item->GetType() >= ITEM_JOB_RESULT_OUTER_FILE && item->GetType() != ITEM_JOB_RESULT_OUTER_INPUT_MOLCAS && item->GetType() <= ITEM_JOB_RESULT_OUTER_SCFORB_MOLCAS) {
        SimuProject *pP = item->GetProject();
        if(pP == NULL)
            return;
        pP->DelJobOuterFile(item->GetTitle(), m_pTree->GetItemText(itemId));
        m_pTree->Delete(itemId);
    }


    AbstractView *view = Manager::Get()->GetViewManager()->FindView(PRJINFO_PID);
    if(view != NULL)
        ((SimuPrjInfoView *)view)->UpdateView();
}
void ProjectManager::OnPopDispJobResult(wxCommandEvent &event)
{
    /*        int        ID=event.GetId();
            wxTreeCtrl* pTree=m_pTree;
            wxTreeItemId    itemId=pTree->GetSelection();
        ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemId) : NULL;
        if(item==NULL)  return;
            ProjectTreeItemType type=item->GetType();
            SimuProject* pProject=item->GetProject();
            wxString        outfile,title=item->GetTitle();
            switch(type){
                    case ITEM_JOB_RESULT_GUESSORB_MOLDEN_MOLCAS:
                            outfile=pProject->GetResultFileName(title)+wxT(".guessorb.molden");
                            title=wxT("guessorb.molden");
                            break;
                    default:
                            break;
            }
    //wxMessageBox(outfile);
            if(ID==POPUPMENU_PRJRES_ID_TEXT){
                    DlgOutput dlgOutput(Manager::Get()->GetAppWindow(),wxID_ANY);
                DlgGridSelect dlggridselect(Manager::Get()->GetAppWindow(),wxID_ANY);
            dlgOutput.AppendFile(outfile,title);ProjectTreeItemType type=item->GetType();
            dlgOutput.ShowModal();
            }else if(ID==POPUPMENU_PRJRES_ID_GV){
                    wxString proc=wxT("pymolcas gv ")+outfile;
            int processId = wxExecute(proc, wxEXEC_SYNC);
            }
    //        wxMessageBox(wxT("OK"));*/ 
}

void ProjectManager::OnPopOpen(wxCommandEvent &event)
{
    ShowSelectedItemView(wxEmptyString);
}

void ProjectManager::OnPegaOpen(wxCommandEvent &event)
{    
    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    // SimuProject* pProject=item->GetProject();
    wxString fileName = item->GetItemVal();
    // wxString filePath = pProject->GetResultDir(fileName);//->GetResultFileName("");//FileUtil::GetDirectoryName(fileName);//FileUtil::GetPath(fileName);
    wxString pegaCmd = EnvUtil::GetPegamoidDir() + wxT(" ") + fileName;
    wxExecute(pegaCmd, wxEXEC_ASYNC);
    // std::cout << "OnPegaOpen filePath "  << fileName << " pegaCmd " << pegaCmd << std::endl;
}

void ProjectManager::OnOthersoftOpen(wxCommandEvent &event)
{    
    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    // SimuProject* pProject=item->GetProject();
    wxString fileName = item->GetItemVal();
    // wxString filePath = pProject->GetResultDir(fileName);//->GetResultFileName("");//FileUtil::GetDirectoryName(fileName);//FileUtil::GetPath(fileName);
    wxString othersoftCmd = EnvUtil::GetOthersoftDir() + wxT(" ") + fileName;
    wxExecute(othersoftCmd, wxEXEC_ASYNC);
    // std::cout << "OnOthersoftOpen filePath "  << fileName << " othersoftCmd " << othersoftCmd << std::endl;
}

void ProjectManager::OnMoldenOpen(wxCommandEvent &event)
{
    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    wxString fileName = item->GetItemVal();
    wxString moldenCmd = EnvUtil::GetMoldenPath() + wxT(" ") + fileName;
    wxExecute(moldenCmd, wxEXEC_ASYNC);
    // std::cout << "OnMoldenOpen filePath " << fileName << " moldenCmd " << moldenCmd << std::endl;
}

void ProjectManager::OnPopPaste(wxCommandEvent &event)
{
    wxTreeCtrl         *pTree = Manager::Get()->GetProjectManager()->GetTree();
    wxTreeItemId    itemId = pTree->GetSelection(), sourceId;
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemId) : NULL;
    if(item == NULL)  return;
    SimuProject *destProject = item->GetProject();


    ProjectTreeItemType type = item->GetType();
    if(type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR) {
        if(!Manager::Get()->GetProjectManager()->IsCopyNodeIdOk())
            return;
        ProjectGroup   *curPG = item->GetProjectGroup();
        if(curPG == NULL)
            return;
        sourceId = Manager::Get()->GetProjectManager()->GetCopyNodeId();
        curPG->InsertNode(itemId, sourceId);
        curPG->Save();
    } else if(type == ITEM_PROJECT) {
        sourceId = Manager::Get()->GetProjectManager()->GetCopyJobNodeId();
        if(!sourceId.IsOk())
            return;

        ProjectTreeItemData *sourceItem = sourceId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(sourceId) : NULL;
        if(sourceItem == NULL)  return;
        SimuProject *sourceProject = sourceItem->GetProject();
        if(sourceProject == NULL)        return;
        
        wxString sourceJobName = sourceItem->GetTitle();
        wxString sourceJobDir = sourceProject->GetResultFileName(sourceJobName);
        sourceJobDir = sourceJobDir.BeforeLast(platform::PathSep().GetChar(0));
        //wxMessageBox(sourceJobDir);

        if(destProject == NULL)        return;
        wxArrayString        destJobs = destProject->GetJobs();
        for(unsigned int i = 0; i < destJobs.GetCount(); i++) { //uniqe the job name
            if(destJobs[i].IsSameAs(sourceJobName)) {
                StringUtil::IncSufNumber(sourceJobName);
                i = -1;
            }
        }
        wxString destJobDir = destProject->GetFileName();
        destJobDir = destJobDir.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + sourceJobName;
        //wxMessageBox(destJobDir);
        FileUtil::CopyDirectory(sourceJobDir, destJobDir);
        destProject->BuildResultTree(sourceJobName);
        destProject->Save();
    } else if(type == ITEM_JOB_RESULT) {
        sourceId = Manager::Get()->GetProjectManager()->GetCopyJobItemNodeId();
        if(!sourceId.IsOk())
            return;

        ProjectTreeItemData *sourceItem = sourceId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(sourceId) : NULL;
        if(sourceItem == NULL)  return;
        SimuProject *sourceProject = sourceItem->GetProject();
        if(sourceProject == NULL)        return;

        wxString destJobName = item->GetTitle();
        wxString sourceFileName, sourceFile;
        wxString destJobDir = destProject->GetPathWithSep() + item->GetItemVal()+platform::PathSep();

        ProjectTreeItemType sourceType = sourceItem->GetType();
        if(sourceType == ITEM_JOB_RESULT_MOLDEN || sourceType == ITEM_JOB_RESULT_ORB) { 
            //copy several files;
            int subItemCount = pTree->GetChildrenCount(sourceId, false);
            wxTreeItemIdValue        subCookie;
            wxTreeItemId        subChild = pTree->GetFirstChild(sourceId, subCookie);
            ProjectTreeItemData *subItem;

            for(int j = 0; j < subItemCount; j++) {
                subItem = subChild.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(subChild) : NULL;
                if(subItem == NULL)        break;

                sourceFileName = wxFileName::FileName(subItem->GetItemVal()).GetFullName();
                sourceFile = subItem->GetItemVal();
                
                wxString newName = destJobDir+sourceFileName;
                //TODO: Propose an overwrite option
                while(!wxCopyFile(sourceFile, newName, FALSE))
                {
                    newName = wxFileName::FileName(newName).GetFullName();
                    wxTextEntryDialog dlg(pTree, wxT("File already exists. Please choose another name."), wxT("File copy"), newName);
                    if(dlg.ShowModal() == wxID_OK)
                        newName = destJobDir+dlg.GetValue();
                    else
                        return;
                }

                subChild = pTree->GetNextChild(sourceId, subCookie);
            }
        } else if(sourceType >= ITEM_JOB_RESULT_OUTPUT_MOLCAS && sourceType <= ITEM_JOB_RESULT_SCFORB_MOLCAS) { 
            //copy a single file
            wxString destJobDir = destProject->GetPathWithSep() + item->GetItemVal()+platform::PathSep();
            sourceFile = sourceItem->GetItemVal();
            sourceFileName = wxFileName::FileName(sourceFile).GetFullName();

            wxString newName = sourceFileName;
            wxTextEntryDialog dlg(pTree, wxT("Rename the file?"), wxT("File copy"), newName);
            if(dlg.ShowModal() == wxID_OK)
                newName = destJobDir+dlg.GetValue();
            else
                return;
            while(!wxCopyFile(sourceFile, newName, FALSE))
            {
                newName = wxFileName::FileName(newName).GetFullName();
                wxTextEntryDialog dlg(pTree, wxT("File already exists. Please choose another name."), wxT("File copy"), newName);
                if(dlg.ShowModal() == wxID_OK)
                    newName = destJobDir+dlg.GetValue();
                else
                    return;
            }
        }
        destProject->BuildResultTree(destJobName, TRUE);
        destProject->Save();
    }
}
void ProjectManager::OnPopReload(wxCommandEvent &event)
{
    wxTreeItemId    itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData( itemId) : NULL;
    if(item == NULL)
        return;

    if(item->GetType() == ITEM_PROJECT)
    {
        SimuProject *pProject = item->GetProject();
        if(pProject == NULL)
            return;
        pProject->CloseView();
        wxTreeItemIdValue val;
        itemId = m_pTree->GetFirstChild(itemId, val);
        ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData( itemId) : NULL;
        Manager::Get()->GetViewManager()->ShowMolView(item, wxEmptyString, true);
    } else if (item->GetType() == ITEM_PROJECTGROUP)
    {
        ProjectGroup *pg = item->GetProjectGroup();
        if(pg == NULL)
            return;
        pg->Close();
        pg->Load();
    }
}
void ProjectManager::OnPopRemove(wxCommandEvent &event)
{
    wxMessageDialog dlg(NULL, wxT("Do you want to remove the item and its child-items from the project group?"), wxT("Warning"), wxYES_NO);
    if(dlg.ShowModal() != wxID_YES)
        return;
    wxTreeItemId itemId = m_pTree->GetSelection();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData( itemId) : NULL;
    item->GetProjectGroup()->RemoveNode(itemId);
    m_pActiveProject = NULL;
}
void ProjectManager::OnPopRename(wxCommandEvent &event)
{
    wxTreeItemId itemId = m_pTree->GetSelection();
    m_pTree->EditLabel(itemId);
}
void ProjectManager::OnTreeItemMenu(wxTreeEvent &event)
{
    ShowPopupMenu(event.GetItem(), event.GetPoint());
    // std::cout << "ProjectManager::OnTreeItemMenu: " << std::endl;
    event.Skip();
}
void ProjectManager::OnTreeItemActivated(wxTreeEvent &event)
{
    if(m_pTree == NULL)
        return;
    wxTreeItemId itemId = event.GetItem();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item == NULL)  return;
    ProjectTreeItemType type = item->GetType();
    // std::cout <<  "OnTreeItemActivated: " << type << std::endl;
    if(type == ITEM_PROJECT || type == ITEM_PROJECTGROUP || type == ITEM_PROJECTDIR) {
        if(!m_pTree->IsExpanded(itemId))
            m_pTree->Expand(itemId);
        else
            m_pTree->Collapse(itemId);

        if(item->GetProjectGroup() != NULL)
            SetActiveProjectGroup(item->GetProjectGroup());
        if(type == ITEM_PROJECT && m_pActiveProject != item->GetProject()) {
            if(m_pActiveProject != NULL)
                m_pTree->SetItemBold(m_pActiveProject->GetProjectNode(), false);
            SetActiveProject(item->GetProject());
        }
    } else if(type == ITEM_JOB_RESULT || type == ITEM_JOB_RESULT_DIR || type == ITEM_JOB_RESULT_ORB || type == ITEM_JOB_RESULT_MOLDEN || type == ITEM_JOB_RESULT_OUTER_FILE || type == ITEM_MOPACDIR) {
        if(!m_pTree->IsExpanded(itemId))
            m_pTree->Expand(itemId);
        else
            m_pTree->Collapse(itemId);
    }

    ShowSelectedItemView(wxEmptyString);
}
void ProjectManager::OnTreeItemSel(wxTreeEvent &event)
{
    wxTreeItemId    itemId = event.GetItem();
    ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)m_pTree->GetItemData(itemId) : NULL;
    if(item == NULL) {
        Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_ADD, false);
        return;
    }
    SimuProject *pP = item->GetProject();
    if(pP != NULL)
        SetActiveProject(pP);
    ProjectGroup   *pPG = item->GetProjectGroup();
    ProjectTreeItemType type = item->GetType();
    // std::cout <<  "OnTreeItemSel: " << type << std::endl;
    if(type == ITEM_PROJECTGROUP) {
        Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_ADD, true);
        SetActiveProjectGroup(pPG);
    } else if(type == ITEM_PROJECTDIR) {
        Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_ADD, true);
        SetActiveProjectGroup(item->GetProjectGroup());
    } else {
        Tools::EnableMenu(Manager::Get()->GetAppFrame()->GetMenuBar(), MENU_ID_FILE_ADD, false);
        SetActiveProjectGroup(NULL);
    }
}

void ProjectManager::DoRemoteJobTerminated(wxString projectFile, wxString jobPath, int status)
{
    //       wxMessageBox(projectFile);
    //      wxMessageBox(jobPath);

    SimuProject *pProject = (SimuProject *)IsOpen(projectFile, false);
    if(!pProject) {
        pProject = LoadProject(projectFile, true);
    }
    //        if(pProject) {
    wxString name = jobPath;//pJobData->GetOutputDir();
    name = FileUtil::GetDirectoryName(name);
    bool isfinal = status; //pJobData->IsFinal();
    //wxMessageBox(wxT("DoRemoteJobTerminated"));
    if(pProject->GetFiles(name)->PostJobTerminated(isfinal)) {
        wxString xyzDirchar = pProject->GetResultFileName(name) + wxT(".Opt.xyz");
        if(wxFileExists(xyzDirchar)) {
            //        MolView * pView=MolViewEventHandler::Get()->GetMolView();
            MolView *pView = (MolView *)Manager::Get()->GetViewManager()->FindView(pProject->GetProjectId());
            if(pView) {
                CtrlInst *pCtrlInst = pView->GetData()->GetActiveCtrlInst();
                pCtrlInst->LoadFile(xyzDirchar);
                pCtrlInst->SetDirty(true);
                pView->RefreshGraphics();
            }
        }
        pProject->BuildResultTree(name);
        //                wxString runningTime = StringUtil::FormatTime(pJobData->GetEndTime() - pProject->GetFiles(name)->GetStartTime());
        //                        wxString info = wxString::Format(wxT("Process terminated with status %d, running time "),status);// + runningTime;
        //                        info = wxT("Project ") + pProject->GetTitle() + wxT(" Job ") + name + wxT(" ") + info;
        //                        Tools::ShowStatus(info, 1);
        pProject->Save();
    }
    //}
}
