/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

// dlgoutput.cpp: implementation of the DlgOutput class.
//
//////////////////////////////////////////////////////////////////////
#include <wx/timer.h>
#include <wx/xml/xml.h>

#include "dlgoutput.h"
//begin : hurukun : 18/11/2009
//#ifdef  __MOLCAS__
//#include <wx/timer.h>
#include <stdio.h>
#include "stringutil.h"
#include "envutil.h"
//#endif

//EVT_LEFT_DCLICK(
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    class to deal with the molcas output file.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //MolcasFileDisp::m_dispStyle[0]=DispStyle('#');
MolcasFileDisp::DispStyle        MolcasFileDisp::m_dispStyle[]=
{
        MolcasFileDisp::DispStyle(MK_NONE,wxT("#000000")),
        MolcasFileDisp::DispStyle(MK_ERROR,wxT("#FF0000")),
        MolcasFileDisp::DispStyle(MK_COMPACT,wxT("#005AfA")),
        MolcasFileDisp::DispStyle(MK_DBCOLON,wxT("#F216D1"))
};

int MolcasFileDisp::m_markType=MK_NONE;
bool* MolcasFileDisp::m_pShowCompMk=NULL;
MolcasFileDisp::MolcasFileDisp()
{
}
MolcasFileDisp::~MolcasFileDisp()
{
    if(m_pShowCompMk!=NULL)
        delete[] m_pShowCompMk;
}
void    MolcasFileDisp::DispFile(const wxString& fileName,wxString description,InfoHtmlWin* pDispWin)
{
//    if(description.IsSameAs(wxT("Output"))){
        //DispSimpleFile(fileName, pDispWin);
//        DispOutputFile(fileName, pDispWin);
     if(description.IsSameAs(wxT("Summary"))){
        DispXmlDumpFile(fileName, pDispWin);
    }else{
//        DispSimpleFile(fileName, pDispWin);
        DispOutputFile(fileName, pDispWin);
    }
}
void    MolcasFileDisp::DispOutputFile(const wxString& fileName,InfoHtmlWin* pDispWin)
{
// deal with  the output file of molcas.
// text line start with : ':: ' will be high lightend
// text area start and end with a line of : '#' will be high lightend
//text area start with: '++' and end with '--' will be hidden at first.
        FILE * fp;
        char * fname;
        char  buffer[BUFFERSIZE];
        int    compactCount=0,compactLine=0;//to record the line of '++' mark appears.
        wxString strContent;

        StringUtil::String2CharPointer(&fname,fileName);
        if((fp=fopen(fname,"r"))==NULL)
        {
                wxMessageBox(wxT("Fail to load file!"));
                return;
        }
        if(pDispWin==NULL)
        return;
    wxString    htmlPath=EnvUtil::GetPrgResDir()+platform::PathSep()+wxT("res")+platform::PathSep()+wxT("prj")+platform::PathSep()+OUTPUT_HTMLFILE_NAME;
    pDispWin->LoadPage(htmlPath);
    wxString buf;
    wxFileSystem* fs = new wxFileSystem;
    wxFSFile* fsFile = fs->OpenFile(htmlPath);
    if (fsFile) {
        wxInputStream* is = fsFile->GetStream();
        char tmp[1024] = {0};
        while (!is->Eof() && is->CanRead()) {
            memset(tmp, 0, sizeof(tmp));
            is->Read(tmp, sizeof(tmp) - 1);
            buf << StringUtil::CharPointer2String((const char*)tmp);
        }
        delete fsFile;
    }
    else
        buf = wxT("<HTML><BODY><H1>Sorry, The default content page seems to be missing. </H1></BODY></HTML>");
    delete fs;
    wxString links=wxEmptyString;

        int tempVar=0;
        wxString        color;
        while(fgets(buffer, BUFFERSIZE, fp) != NULL) {
                if(m_markType==MK_ERROR)//error mark area;
                {
                           if(ErrorMarkEnd(buffer,&tempVar))
                        {
                                m_markType=MK_NONE;
                        }
                }else{
                        if(ErrorMarkLine(buffer,&tempVar))
                        {
                                m_markType=MK_ERROR;
                        }
                }
        if(m_markType==MK_DBCOLON)//emphasize line, only a sigal line
                {
                    m_markType=MK_NONE;
                }else if(DBColonLine(buffer)){
                                m_markType=MK_DBCOLON;
                }
                if(m_markType==MK_COMPACT)//check the area need to be compressed;
                {
                   if(CompMarkEnd(buffer))
                   {
                       m_markType=MK_NONE;
                       compactLine=0;
            }else{
                if(m_pShowCompMk==NULL||(m_pShowCompMk!=NULL&&m_pShowCompMk[compactCount-1]==false))
                    continue;
            }
                }else{
                  if(CompMarkBegin(buffer))
                        {
                m_markType=MK_COMPACT;
                                if(m_pShowCompMk!=NULL&&m_pShowCompMk[compactCount]==true){//expand, change the "++" to "--"
                                        int strLen=strlen(buffer);
                                        for(int i=0;i<strLen;i++){//to search the error mark line .
                                                if(buffer[i]!=COMP_MARK_S&&buffer[i]!=' ')
                                                        break;
                                                if(buffer[i]==COMP_MARK_S)
                                                {
                                                        if(i<strLen-1&&buffer[i+1]==COMP_MARK_S){
                                                                buffer[i]='-';
                                                                buffer[i+1]='-';
                                                                break;
                                                        }
                                                }
                                        }
                                }
                compactCount++;
                        }
        }
                strContent=StringUtil::CharPointer2String(buffer);
                color=GetColor(m_markType);
 //       strContent.Replace(wxT(" "),wxT("&ensp;"));
//        strContent.Replace(wxT("\\n "),wxT("\\0"));
//        int tmpspech=strContent.Find(wxT('<'));
//        if(tmpspech>0){
//                strContent=strContent.SubString(0,tmpspech)+wxT("&lt;")+strContent.SubString(tmpspech,strContent.Length());
//        }
        strContent.Replace(wxT("<"),wxT("&lt;"));
        strContent.Replace(wxT(">"),wxT("&gt;"));
        if(m_markType==MK_COMPACT&&compactLine==0)
        {
            compactLine=1;
            links << wxT("<A HREF=\"")+wxString::Format(wxT("%d"),compactCount)+wxT("\">");
            links <<strContent;
            links << wxT("</A>");
        }else{
             links <<wxT("<font color=\"")+color+wxT("\">");
             links<<strContent;
             links <<wxT("</font>");
        }
        }
        fclose(fp);

        buf.Replace(_T("DISP_FILE_CONTENT"), links);
    pDispWin->SetPage(buf);
    if(m_pShowCompMk==NULL){
        m_pShowCompMk=new bool[compactCount];
        for(int i=0;i<compactCount;i++)
            m_pShowCompMk[i]=false;
    }
}
void    MolcasFileDisp::DispSimpleFile(const wxString& fileName,InfoHtmlWin* pDispWin)
{
    FILE * fp;
        char * fname;
        char  buffer[BUFFERSIZE];
        wxString strContent;

        StringUtil::String2CharPointer(&fname,fileName);
        if((fp=fopen(fname,"r"))==NULL)
        {
                wxMessageBox(wxT("Fail to load file!"));
                return;
        }
        if(pDispWin==NULL)
        return;
    wxString    htmlPath=EnvUtil::GetPrgResDir()+platform::PathSep()+wxT("res")+platform::PathSep()+wxT("prj")+platform::PathSep()+OUTPUT_HTMLFILE_NAME;
    pDispWin->LoadPage(htmlPath);
    wxString buf;
    wxFileSystem* fs = new wxFileSystem;
    wxFSFile* fsFile = fs->OpenFile(htmlPath);
    if (fsFile) {
        wxInputStream* is = fsFile->GetStream();
        char tmp[1024] = {0};
        while (!is->Eof() && is->CanRead()) {
            memset(tmp, 0, sizeof(tmp));
            is->Read(tmp, sizeof(tmp) - 1);
            buf << StringUtil::CharPointer2String((const char*)tmp);
        }
        delete fsFile;
    }
    else
        buf = wxT("<HTML><BODY><H1>Sorry,The default content page seems to be missing. </H1></BODY></HTML>");
    delete fs;
    wxString links=wxEmptyString;

        wxString        color;
        while(fgets(buffer, BUFFERSIZE, fp) != NULL) {
            strContent=StringUtil::CharPointer2String(buffer);
         links <<wxT("<tr><td>");
                 strContent.Replace(wxT(">"),wxT("&gt"));
                 strContent.Replace(wxT("<"),wxT("&lt"));
         links<<strContent;
         links <<wxT("</td></tr>");
        }
        fclose(fp);
        buf.Replace(_T("DISP_FILE_CONTENT"), links);
    pDispWin->SetPage(buf);
}
void    MolcasFileDisp::DispXmlDumpFile(const wxString& fileName,InfoHtmlWin* pDispWin)
{
        wxString strContent;
    if(!wxFileExists(fileName))
        return;

        if(pDispWin==NULL)
        return;
    wxString    htmlPath=EnvUtil::GetPrgResDir()+platform::PathSep()+wxT("res")+platform::PathSep()+wxT("prj")+platform::PathSep()+OUTPUT_HTMLFILE_XML_NAME;
    pDispWin->LoadPage(htmlPath);
    wxString buf;
    wxFileSystem* fs = new wxFileSystem;
    wxFSFile* fsFile = fs->OpenFile(htmlPath);
    if (fsFile) {
        wxInputStream* is = fsFile->GetStream();
        char tmp[1024] = {0};
        while (!is->Eof() && is->CanRead()) {
            memset(tmp, 0, sizeof(tmp));
            is->Read(tmp, sizeof(tmp) - 1);
            buf << StringUtil::CharPointer2String((const char*)tmp);
        }
        delete fsFile;
    }
    else
        buf = wxT("<HTML><BODY><H1>Sorry,The default content page seems to be missing. </H1></BODY></HTML>");
    delete fs;
    wxString links=wxEmptyString;
        wxString        color,rcolor=wxT("'#f20056'");
wxXmlDocument doc;
if (!doc.Load(fileName))
    return ;

// start processing the XML file
if (doc.GetRoot()->GetName() != wxT("root"))
    return ;
wxString    nodeName,nodeVal,propvalue;
wxXmlNode *schild = doc.GetRoot()->GetChildren(),*child;
int count=1;
while(schild){
links<<wxT("<tr><td bgcolor=")+rcolor+wxT(">");
nodeName=schild->GetName();
links<<nodeName<<wxT("</td><td align='right' bgcolor=")+rcolor+wxT(">");
propvalue = schild->GetAttribute(wxT("value"), wxT(""));
links<<propvalue+wxT("</td></tr>");

child=schild->GetChildren();
schild=schild->GetNext();
while (child) {
    if(count%2==1)
        color=wxT("'#f0e3b0'");
    else
        color=wxT("'#ffffff'");
    count++;
    links<<wxT("<tr><td bgcolor=")+color+wxT(">");
    nodeName=child->GetName();
//    links<<nodeName+wxT("</td><td>");
    // process properties
    propvalue = child->GetAttribute(wxT("appear"), wxT(""));
    if(propvalue.IsEmpty())
        propvalue=nodeName;
    links<<propvalue+wxT("</td><td align='right' bgcolor=")+color+wxT(">");
    propvalue = child->GetAttribute(wxT("nx"), wxT(""));
    // process text enclosed by <tag1></tag1>
    if(!propvalue.IsEmpty())
    {
        long    num;
        if(propvalue.ToLong(&num))
        {
            nodeVal = child->GetNodeContent();
            wxString tmp=wxEmptyString;
            links<<wxT("<table>");
            for(long i=0;i<num;i++){
                tmp=nodeVal.BeforeFirst(wxT(' '));
                nodeVal=nodeVal.AfterFirst(wxT(' '));
                links<<wxT("<td>")+tmp+wxT("</td>");
            }
             links<<wxT("<td>")+nodeVal+wxT("</td></table>");

        }
        links<<wxT("</td></tr>");
    }else{
        nodeVal = child->GetNodeContent();
        links<<nodeVal+wxT("</td></tr>");
    }

    child = child->GetNext();
}
//links<<wxT("<tr><td align='center' bgcolor=")+rcolor+wxT(">...</td><td align='right' bgcolor=")+rcolor+wxT("</td>...</tr>");
}

        buf.Replace(_T("DISP_FILE_CONTENT"), links);
    pDispWin->SetPage(buf);
}
wxString MolcasFileDisp::GetColor(int mark)
{
        for(int i=0;i<NUM_MOLCAS_FILE_MARK;i++)
                if(m_dispStyle[i].mark==mark)
                        return m_dispStyle[i].color;
        return wxEmptyString;
}
 bool        MolcasFileDisp::ErrorMarkLine(char * buffer,int* tempVar)//mark  : ####...
{
                int strLen=strlen(buffer);
                int count=C_ERROR_L_EX;
                for(int i=0;i<strLen;i++){//to search the error mark line .
                        if(buffer[i]!=ERROR_MARK&&buffer[i]!=' ')
                                break;
                        if(buffer[i]==ERROR_MARK)
                                count++;
                }
                if(count==strLen&&count>C_ERROR_L_EX)
                {
                        (*tempVar)++;//this variable is used by ErrorMarkEnd() to find the end of error mark area;
                        return true;
                }
        return false;
}
 bool        MolcasFileDisp::ErrorMarkEnd(char * buffer,int* tempVar) //mark : #####...
{
                if(ErrorMarkLine(buffer,tempVar))
                        return false;
                if((*tempVar)==NUM_ERROR_L)
                {
                        *tempVar=0;
                        return true;
                }
                return false;
}
 bool    MolcasFileDisp:: CompMarkBegin(char * buffer)//mark : ++
{
        int strLen=strlen(buffer);
        for(int i=0;i<strLen;i++){//to search the error mark line .
                if(buffer[i]!=COMP_MARK_S&&buffer[i]!=' ')
                                break;
                if(buffer[i]==COMP_MARK_S)
                {
                        if(i<strLen-1&&buffer[i+1]==COMP_MARK_S)
                                return true;
                        else
                                break;
                }
        }
        return false;
}
bool     MolcasFileDisp::MatchCompMark(FILE * fp,wxArrayString & compcont)
{//to find out if the mark: ++ and -- is in pair.

        return false;
}
 bool        MolcasFileDisp::CompMarkEnd(char * buffer)//mark : --
{
        int strLen=strlen(buffer);
        for(int i=0;i<strLen;i++){//to search the error mark line .
                if(buffer[i]!=COMP_MARK_E&&buffer[i]!=' ')
                                break;
                if(buffer[i]==COMP_MARK_E)
                {
                        if(i<strLen-2&&buffer[i+1]==COMP_MARK_E&&buffer[i+2]!=COMP_MARK_E)
                                return true;
                        else
                                break;
                }
        }
        return false;
}
 bool        MolcasFileDisp::DBColonLine(char* buffer)//mark: ::
{
        int strLen=strlen(buffer);
        for(int i=0;i<strLen;i++){//to search the error mark line .
                if(buffer[i]!=DCOL_MARK&&buffer[i]!=' ')
                                break;
                if(buffer[i]==DCOL_MARK)
                {
                        if(i<strLen-1&&buffer[i+1]==DCOL_MARK)
                                return true;
                        else
                                break;
                }
        }
        return false;
}
bool MolcasFileDisp::IsShowCompact(int i)
{
    if(m_pShowCompMk==NULL)
        return false;
    else
        return m_pShowCompMk[i];
}
void MolcasFileDisp::ShowCompact(int i,bool flag)
{
    if(m_pShowCompMk[i]!=flag)
        m_pShowCompMk[i]=flag;
    else if(flag==true)
        m_pShowCompMk[i]=false;
}
////////////////////////////////////////////////////////////////////
//  class   TimerDlg
////////////////////////////////////////////////////////////////////
int DLG_TIMER_ID=wxNewId();
BEGIN_EVENT_TABLE(TimerDlg, wxDialog)
    EVT_TIMER(DLG_TIMER_ID, TimerDlg::OnTimer)
END_EVENT_TABLE()
TimerDlg::TimerDlg(wxWindow * parent, int id, wxString title,const wxPoint& pos,const wxSize& size , long style )
                                : wxDialog(parent, id, title,pos, size, style),m_timer(this, DLG_TIMER_ID){
                                    CreateUI();
                                    m_timer.Start(100);
}
void TimerDlg::OnTimer(wxTimerEvent& event)
{
    m_timer.Stop();
    EndModal(0);
}
void TimerDlg::CreateUI()
{
    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
    wxStaticText* descr = new wxStaticText( this, wxID_STATIC,wxT("Wating ..."),wxPoint(25,30),wxSize(50,25));
    bsTop->Add(descr,0,wxEXPAND);
}
int MENU_ID_COPY_TEXT=wxNewId();
BEGIN_EVENT_TABLE(InfoHtmlWin, wxHtmlWindow)
        EVT_RIGHT_UP(InfoHtmlWin::OnPopupMenu)
        EVT_MENU(MENU_ID_COPY_TEXT,InfoHtmlWin::OnTextCopy)
        EVT_CHAR(InfoHtmlWin::OnKeyEvent)
END_EVENT_TABLE()
//end   : hurukun : 18/11/2009
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgOutput, wxFrame)
    EVT_CLOSE(DlgOutput::OnClose)
    EVT_SIZE(DlgOutput::OnResize)
    EVT_BUTTON(wxID_OK,DlgOutput::OnOk)
//        EVT_CONTEXT_MENU(DlgOutput::OnPopupMenu)
END_EVENT_TABLE()
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DlgOutput::DlgOutput(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame(parent, id, title, pos, size, style)
{
     this -> SetMaxSize(wxSize(2000,2000));
     m_fileName = wxEmptyString;
     m_desc = wxEmptyString;
     m_pOk = NULL;
     m_pInfoWin = NULL;
     CreateGUI();
}

DlgOutput::~DlgOutput()
{

}

void DlgOutput::Draw(){
        if(m_show){
                MolcasFileDisp::DispFile(m_fileName,m_desc,m_pInfoWin);
        }
}
void DlgOutput::CreateGUI()
{
     wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
     SetSizer(bsTop);
    // std::cout <<  "DlgOutput::CreateGUI: " << std::endl;
     wxStaticText* descr = new wxStaticText(this, wxID_STATIC, wxT("Information:"), wxPoint(30,5), wxSize(680,20), 0);
     bsTop -> Add(descr, 0, wxALIGN_LEFT|wxALL, 5);
    
     wxSize sz=this->GetClientSize();
     m_pInfoWin = new InfoHtmlWin(this, this, wxID_ANY, wxPoint(10,30), wxSize(sz.GetWidth()-20, sz.GetHeight()-100));
     bsTop -> Add(m_pInfoWin,1,wxALL|wxEXPAND,5);
     m_pOk = new wxButton(this, wxID_OK, wxT("&OK"), wxPoint(sz.GetWidth()-100,sz.GetHeight()-60), wxDefaultSize, 0);
     bsTop -> Add(m_pOk,0,wxALL|wxALIGN_RIGHT,5);
     m_show = false;
}

void DlgOutput::ScrollToBottom()
{
    if(!m_pInfoWin)
        return;
    int x=-1;
    int y=-1;
    m_pInfoWin->GetVirtualSize(&x,&y);
    m_pInfoWin->Scroll(-1,y);
}

void DlgOutput::DoOk()
{
//        if(IsModal())
//        {
//                EndModal(wxID_OK);
//        }
//        else
//        {
//                SetReturnCode(wxID_OK);
                this->Show(false);
                this->Destroy();
//        }
}

void DlgOutput::OnClose(wxCloseEvent &event)
{
        DoOk();
}
void DlgOutput::OnOk(wxCommandEvent& event)
{
        DoOk();
}
void DlgOutput::AppendFile(wxString fileName, wxString description)
{
        if(!wxFileExists(fileName))
                return;
        this->SetTitle(description);
        m_desc=description;
        m_fileName=fileName;
        m_show=true;
        Draw();
}
bool DlgOutput::LinkClicked(const wxHtmlLinkInfo& link)
{
    if(m_pInfoWin==NULL)
        return false;
    wxString href = link.GetHref();
    long id;
    if(!href.ToLong(&id))
        return false;

    int x,y;
    m_pInfoWin->GetViewStart(&x,&y);
    MolcasFileDisp::ShowCompact(id-1);
    MolcasFileDisp::DispFile(m_fileName,m_desc,m_pInfoWin);
    TimerDlg  tdlg(this);
    tdlg.ShowModal();
    m_pInfoWin->Scroll(x,y);
    return true;
}
void DlgOutput::OnResize(wxSizeEvent& event)
{

    m_pInfoWin->SetSize(event.GetSize().GetWidth()-20,event.GetSize().GetHeight()-100);
        Draw();
    event.Skip();

 //   if(m_pInfoWin!=NULL)
  //      delete m_pInfoWin;
  //  m_pInfoWin=new InfoHtmlWin(this,this, wxID_ANY, wxPoint(10,40), wxSize(sz.GetWidth()-10,sz.GetHeight()-100));
  //  MolcasFileDisp::DispFile(m_fileName,m_desc,m_pInfoWin);
//        if(m_pOk!=NULL)
 //       delete m_pOk;
//        m_pOk = new wxButton( this, wxID_OK, wxT("&OK"),wxPoint(sz.GetWidth()-100,sz.GetHeight()-50), wxDefaultSize, 0 );
}

void   InfoHtmlWin::OnPopupMenu(wxMouseEvent& event){
    wxMenu menu;
    menu.Append(MENU_ID_COPY_TEXT, wxT("Copy"),wxT("Copy the selected text."));
    PopupMenu(&menu,event.m_x,event.m_y);
}
void    InfoHtmlWin::OnTextCopy(wxCommandEvent& event){
//    wxMessageBox(wxT("OK"));
    this->CopySelection();
}
void    InfoHtmlWin::OnKeyEvent(wxKeyEvent& event){
    if(!event.ControlDown()){
        return ;
    }
    if(event.GetKeyCode()==1){
        this->SelectAll();
    }else if(event.GetKeyCode()==3){
        this->CopySelection();
    }
}
