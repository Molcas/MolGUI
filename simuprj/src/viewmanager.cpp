/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "viewmanager.h"
#include "simuprjfrm.h"
#include "envutil.h"
#include "fileutil.h"
#include "genericdlg.h"
#include "tools.h"
#include "manager.h"
#include "projectmanager.h"
#include "starthereview.h"
#include "simuprjinfoview.h"
#include "simuprjdef.h"
#include "molview.h"
#include "molvieweventhandler.h"
#include "auisharedwidgets.h"
#include "simujobpnl.h"
#include "gmfile.h"
#include "surfacepnl.h"
#include "vibrationpnl.h"
#include "mingpnl.h"
#include "dlgoutput.h"
#include "dlgsummary.h"

static ViewManager* managerInstance = NULL;

int CTRL_ID_VIEW_MANAGER = wxNewId();
int CTRL_ID_TIMER_VIB = wxNewId();

BEGIN_EVENT_TABLE(ViewManager, wxEvtHandler)
        EVT_AUINOTEBOOK_PAGE_CHANGED(CTRL_ID_VIEW_MANAGER, ViewManager::OnViewPageChanged)
        EVT_AUINOTEBOOK_PAGE_CLOSE(CTRL_ID_VIEW_MANAGER, ViewManager::OnViewPageClose)
        EVT_TIMER(CTRL_ID_TIMER_VIB, ViewManager::OnTimerVibration)
END_EVENT_TABLE()

ViewManager::ViewManager() : m_timerVib(this, CTRL_ID_TIMER_VIB)
{
        m_pNotebook = new wxAuiNotebook(Manager::Get()->GetAppWindow(), CTRL_ID_VIEW_MANAGER);
        Manager::Get()->GetAppWindow()->PushEventHandler(this);
        //Manager::Get()->GetAppWindow()->PushEventHandler(MolViewEventHandler::Get());
}
ViewManager::~ViewManager()
{
        //Manager::Get()->GetAppWindow()->RemoveEventHandler(MolViewEventHandler::Get());
        if(m_timerVib.IsRunning()) m_timerVib.Stop();
        if(m_pNotebook!=NULL)
        delete m_pNotebook;
        Manager::Get()->GetAppWindow()->RemoveEventHandler(this);
}
ViewManager* ViewManager::Get(void)
 {
        if(!managerInstance)
                managerInstance = new ViewManager;
    return managerInstance;
}
void   ViewManager::Free(void)
{
    if(managerInstance!=NULL)
        delete managerInstance;
    managerInstance = NULL;
}
void   ViewManager::ActivateView(const wxString& filename)
 {
    wxString normalizedFilename = FileUtil::NormalizeFileName(filename);
    AbstractView* view = NULL;
    for (unsigned i = 0; i < m_pNotebook->GetPageCount(); i++) {
        view = GetView(i);
        if (!view) continue;
        wxString fname = view->GetFileName();
        // windows must use case-insensitive comparison
        if (fname.IsSameAs(normalizedFilename, platform::windows == false)) {
                        m_pNotebook->SetSelection(i);
                        break;
                }
    }
}
void   ViewManager::ActivateView(const int PID)
 {
    AbstractView* view = NULL;
    for (unsigned i = 0; i < m_pNotebook->GetPageCount(); i++) {
        view = GetView(i);
        if (!view) continue;
        if(view->GetProjectId()==PID){
                        m_pNotebook->SetSelection(i);
                        break;
                }
    }
}
void   ViewManager::AddView(AbstractView* view, ProjectTreeItemType itemType, wxString prefix)
 {
    if(GetViewIndex(view) == -1) {
        m_pNotebook->AddPage(view, view->GetViewTitle(prefix), true);
        wxBitmap bitmap = Manager::Get()->GetProjectManager()->GetBitmap(itemType);
        if(bitmap.IsOk()) {
                        m_pNotebook->SetPageBitmap(m_pNotebook->GetPageCount()-1, bitmap);
                }
        m_pNotebook->Refresh();
    }
}
bool   ViewManager::CloseCurrentView(bool isAskForSave,bool default_save)
{
        bool isClosed = CloseView(m_pNotebook->GetSelection(),isAskForSave,default_save);
        if(isClosed) {
                m_pNotebook->DeletePage(m_pNotebook->GetSelection());
        }
        return isClosed;
}
bool   ViewManager::CloseView(int viewIndex,bool isAskForSave,bool default_save)
{
//wxMessageBox(wxT("close view"));
        AbstractView* view = GetView(viewIndex);
        if(view && view->Close(isAskForSave,default_save)) {
                view->EnableMenuTool(false);
                int index = -1;
                if(viewIndex == GetViewCount() - 1 && GetViewCount() - 2 >= 0) {
                        index = GetViewCount() - 2;
                }else if(viewIndex < GetViewCount() - 1) {
                        index = viewIndex + 1;
                }
                if(index != -1) {
                        view = GetView(index);
                        if(view) {
                                view->EnableMenuTool(true);
                                view->UpdateMenuTool();
                        }
                }
                return true;
        }
        return false;
}
bool   ViewManager::CloseViewBPID(int projectId,bool isAskForSave,bool default_save)
{
        AbstractView* view = NULL;
        bool isClose = true;
        for(int i = GetViewCount() - 1; i >=0 ; i--) {
                view = GetView(i);
                if(projectId == BP_NULL_VALUE || view->GetProjectId() == projectId) {
                //        if(name.IsEmpty() || name.Cmp(view->GetTitle()) == 0) {
                                if(view->Close(isAskForSave,default_save)) {
                                        view->EnableMenuTool(false);
                                        m_pNotebook->DeletePage(i);
                                }else {
                                        isClose = false;
                                        break;
                                }
                //        }
                }
        }
        view = GetActiveView();
        if(view) {
                view->EnableMenuTool(true);
                view->UpdateMenuTool();
        }
        return isClose;
}
bool   ViewManager::CloseAllView(bool isAskForSave,bool default_save)
{
    int count=GetViewCount();
    for(int i=count-1;i>0;i--)
        CloseCurrentView(isAskForSave,default_save);
    return true;
}
AbstractView* ViewManager::FindView(int projectId)
{
    AbstractView* view = NULL;
        for(int i = GetViewCount() - 1; i >=0 ; i--) {
                view = GetView(i);
                if(view!=NULL && view->GetProjectId() == projectId) {
                        return view;
                }
        }
        return NULL;
}
int     ViewManager::GetViewIndex(AbstractView* view)
{
    for(unsigned i = 0; i < m_pNotebook->GetPageCount(); i++) {
        if (m_pNotebook->GetPage(i) == view)
            return i;
    }
    return -1;
}
int     ViewManager::GetViewCount(void)
{
        return m_pNotebook->GetPageCount();
}
AbstractView* ViewManager::GetView(int index)
{
        if(index >= 0 && index < (int)m_pNotebook->GetPageCount()) {
                AbstractView* view = static_cast<AbstractView*>(m_pNotebook->GetPage(index));
                return view;
        }
        return NULL;
}
AbstractView* ViewManager::GetView(const wxString& filename)
{
        return IsOpen(filename);
}
AbstractView* ViewManager::GetActiveView(void)
{
        return GetView(m_pNotebook->GetSelection());
}
AbstractView* ViewManager::IsOpen(const wxString& filename)
{
    wxString normalizedFilename = FileUtil::NormalizeFileName(filename);
    AbstractView* view = NULL;
    for (unsigned i = 0; i < m_pNotebook->GetPageCount(); i++) {
        view = GetView(i);
        if (!view) continue;
        wxString fname = view->GetFileName();
        // windows must use case-insensitive comparison
        if (fname.IsSameAs(normalizedFilename, platform::windows == false))
            return view;
    }

    return NULL;
}
bool ViewManager::RenameView(int projectId,wxString nTitle)
{
    AbstractView * pView=FindView(projectId);
    if(pView==NULL)
        return true;
    wxString    title=pView->GetTitle();
    wxString    fullpath=pView->GetFileName();


    wxString prefix=fullpath.BeforeFirst(wxCHAR_FN_SEP);
    if(prefix.IsSameAs(fullpath)){
        prefix=fullpath.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+nTitle;
    }else{
              prefix=prefix.BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+nTitle;
        prefix=prefix+wxFN_SEP+fullpath.AfterFirst(wxCHAR_FN_SEP);
        prefix=prefix.BeforeLast(platform::PathSep().GetChar(0));
    }

    fullpath=prefix+platform::PathSep()+nTitle+wxPRJ_FILE_SUFFIX;

//    fullpath=fullpath.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+nTitle+platform::PathSep()+nTitle+wxMSD_FILE_SUFFIX;
    m_pNotebook->SetPageText(GetViewIndex(pView),nTitle+wxT("::Molecule"));
    pView->SetTitle(nTitle);
    pView->SetFileName(fullpath);
    return true;
}
void ViewManager::ShowStartHereView(bool isShow)
 {
    AbstractView* startHereView = Get()->GetView(global_startHereTitle);
    if (isShow) {
                if(!startHereView) {
                startHereView = new StartHereView(m_pNotebook, Manager::Get()->GetAppWindow());
      //          startHereView->SetProjectId(STARTHERE_PID);
                AddView(startHereView, ITEM_UNKNOWN, wxEmptyString);
                startHereView->InitView();
                }
        } else if (!isShow && startHereView) {
        startHereView->Destroy();
        }
}
void ViewManager::ShowPrjInfoView(bool isShow)
 {
    AbstractView* prjInfoView = Get()->GetView(SIMU_PRJINFO_VIEW_TITLE);
    if (isShow) {
                if(!prjInfoView) {
                prjInfoView = new SimuPrjInfoView(m_pNotebook, Manager::Get()->GetAppWindow());
    //            prjInfoView->SetProjectId(PRJINFO_PID);
                AddView(prjInfoView, ITEM_UNKNOWN, wxEmptyString);
                }
        } else if (!isShow && prjInfoView) {
        prjInfoView->Destroy();
        }
}
void ViewManager::ShowMolView(ProjectTreeItemData* treeItem, wxString importFileName, bool isShow)
{
    if(treeItem->GetType()==ITEM_JOB_RESULT)return;
        wxString fileName =wxEmptyString;
    SimuProject* pProject=treeItem->GetProject();
//wxMessageBox(importFileName);
        if(importFileName.IsEmpty())
                    fileName=treeItem->GetFileName();
    else{
            if(pProject==NULL)
                        return;
                fileName=pProject->GetFileName();
                //begin  wangyuanlong  12/01
                if(fileName.AfterLast(wxT('.')).IsSameAs(wxT("sim"))){
            }else{
                fileName=FileUtil::ChangeFileExt(fileName,wxMSD_FILE_SUFFIX);
        }//end wangyuanlong 12/01
            }
        if(IsOpen(fileName) != NULL&&importFileName.IsEmpty())
        {
                if(isShow)
                {
                        ActivateView(fileName);
                }else
                {
                        //view->Destroy();
                }
        }
        else
        {
/*                if(importFileName.IsEmpty()&&treeItem->GetProject()->GetType()==PROJECT_MOLCAS && (treeItem->GetType()!= ITEM_MOLECULE))
                {
                        treeItem->GetProject()->ShowResultItem(treeItem->GetType(),treeItem->GetTitle());
                        return;
                }
                if(treeItem->GetType() == ITEM_JOB_RESULT_SUMMARY)
                {
                        DlgSummary dlgsummary(Manager::Get()->GetAppWindow(),wxID_ANY);
                        wxString sumfile=treeItem->GetProject()->GetFiles(treeItem->GetTitle())->GetSummaryFileName();
                        dlgsummary.AppendSummary(sumfile);
                        dlgsummary.ShowModal();
                }
                else if(treeItem->GetType() == ITEM_JOB_RESULT_INPUT)
                {
                        DlgOutput dlgOutput(Manager::Get()->GetAppWindow(),wxID_ANY);
                        wxString infile=treeItem->GetProject()->GetFiles(treeItem->GetTitle())->GetInputFileName();
                        dlgOutput.AppendFile(infile,wxT("InPut"));
                        dlgOutput.ShowModal();
                }
                else if(treeItem->GetType() == ITEM_JOB_RESULT_OUTPUT)
                {
                        DlgOutput dlgOutput(Manager::Get()->GetAppWindow(),wxID_ANY);
                        wxString outfile=treeItem->GetProject()->GetFiles(treeItem->GetTitle())->GetOutputFileName();
                        dlgOutput.AppendFile(outfile,wxT("OutPut"));
                        dlgOutput.ShowModal();
                }
                else
                {

                        ViewType viewType = VIEW_GL_DISPLAY;
                        if(treeItem->GetType() == ITEM_MOLECULE||!importFileName.IsEmpty())
                        {
                                viewType = VIEW_GL_BUILD;
                        }else if(treeItem->GetType() == ITEM_JOB_RESULT_DENSITY)
                        {
                                viewType = VIEW_GL_DISPLAY_SURFACE;
                        }else if(treeItem->GetType() == ITEM_JOB_RESULT_FREQUENCE)
                        {
                                viewType = VIEW_GL_DISPLAY_VIBRATION;
                        }
//                        ViewType        viewType = VIEW_GL_BUILD;
                        MolView* molView =(MolView*)FindView(pProject->GetProjectId());
                        bool        newview=false;
                        if(molView==NULL){//||importFileName.IsEmpty()){
                molView=new MolView(m_pNotebook, Manager::Get()->GetAppWindow(), viewType);
                                newview=true;
                        }
            if(molView==NULL)
                                return;
            molView->SetFileName(fileName);
            molView->SetProjectId(pProject->GetProjectId());
            molView->SetMenuBar(Manager::Get()->GetAppFrame()->GetMenuBar());
            molView->InitView();
                        if(!importFileName.IsEmpty()){
//wxMessageBox(importFileName);
                                wxString        dataFile=pProject->GetFileName();
                                dataFile=FileUtil::ChangeFileExt(dataFile,wxMSD_FILE_SUFFIX);
//wxMessageBox(dataFile);
*                                if(wxFileExists(dataFile)){
                                        wxMessageDialog mdlg(NULL,wxT("Do you want to overwrite the old molecule?"),wxT("Warning"),wxYES_NO | wxICON_INFORMATION);
                                        if(mdlg.ShowModal() != wxID_YES)
                                                    return;
                                }*
                                molView->ImportFile(importFileName);
                        }
                   molView->SetTitle(pProject->GetTitle());
//wxMessageBox(treeItem->GetTitle());
                        if(newview==true)
                           AddView(molView, treeItem->GetType(), pProject->GetTitle());
//                }
*/
                        ViewType viewType = VIEW_GL_DISPLAY;
                        if(treeItem->GetType() == ITEM_MOLECULE)
                        {
//wxMessageBox(wxT("OK"));
                                viewType = VIEW_GL_BUILD;
                        }else if(treeItem->GetType() == ITEM_JOB_RESULT_DENSITY)
                        {
                                viewType = VIEW_GL_DISPLAY_SURFACE;
                        }else if(treeItem->GetType() == ITEM_JOB_RESULT_FREQUENCE)
                        {
                                viewType = VIEW_GL_DISPLAY_VIBRATION;
                        }
                        MolView* molView=NULL;
                        if(!importFileName.IsEmpty()){
                viewType = VIEW_GL_BUILD;
                SimuProject* pProject=Manager::Get()->GetProjectManager()->GetActiveProject();
                if(pProject!=NULL){
                    molView=pProject->GetView();
                }
            }
            if(molView==NULL){
                             molView = new MolView(m_pNotebook, Manager::Get()->GetAppWindow(), viewType);

                        molView->SetFileName(fileName);
                        molView->SetProjectId(pProject->GetProjectId());
                        molView->SetMenuBar(Manager::Get()->GetAppFrame()->GetMenuBar());
                        molView->SetTitle(treeItem->GetTitle()); //the title(jobname) is import for display density and vib
                    molView->InitView();
        }
                    molView->ImportFile(importFileName);
                    //begin : hurukun : 13/11/2009
                        //molView->SetTitle(treeItem->GetResultName());
                        //end   : hurukun : 13/11/2009
                    AddView(molView, treeItem->GetType(), pProject->GetTitle());


        }
}
bool ViewManager::ShowJobView(ProjectTreeItemData* treeItem,wxString jobName, bool isShow,bool isNewjob)
 {
     if(treeItem==NULL||treeItem->GetProject()==NULL)
        return false;
        GenericDlg dlg(Manager::Get()->GetAppWindow(), wxID_ANY, treeItem->GetProject()->GetTitle() + wxT("::") + SimuProject::GetProjectType(treeItem->GetProject()->GetType()) + wxT(" Job Settings"));
        dlg.SetMinSize(wxSize(1000,700));
        GenericPnl* pnl = NULL;
        wxString tmpfile=treeItem->GetFileName(treeItem->GetProject(),jobName,treeItem->GetType());
//        FileUtil::CreateDirectory(tmpfile.BeforeLast(platform::PathSep().GetChar(0)));

        switch(treeItem->GetProject()->GetType()) {
                case PROJECT_SIMUCAL:
                        pnl = new SimuJobPnl(&dlg, treeItem, wxID_ANY,isNewjob);
                        break;
                case PROJECT_MOLCAS:
                        pnl = new MingPnl(&dlg, wxID_ANY,jobName,isNewjob);
                         if(wxFileExists(tmpfile)) {//file do exists.
                                ((MingPnl*)pnl)->DoFileOpen(tmpfile, false);
                        }else {
                                ((MingPnl*)pnl)->UpdateFrameTitle(tmpfile);
                        }
                        break;
                //add for qchem by bao begin
        case PROJECT_QCHEM:
                         Manager::Get()->GetProjectManager()->SubmitJob(treeItem->GetProject(),jobName,isNewjob);
            break;
                //add for qchem by bao end
        default:
                        break;
        }
        if(pnl != NULL) {
                dlg.SetGenericPanel(pnl);
                int decision = dlg.ShowModal();
//                if(!wxFileExists(tmpfile))
//                        FileUtil::RemoveDirectory(tmpfile.BeforeLast(platform::PathSep().GetChar(0)));
        if(decision==BTN_SAVE){
            SimuProject* pP=treeItem->GetProject();
            if(pP!=NULL){
                pP->BuildResultTree(jobName);
                pP->GetFiles(jobName);
                pP->Save();
            }
        }
 //       if(treeItem->GetProject()->GetView()==NULL)
//            treeItem->GetProject()->UpdateSimByXyz();
                if(BTN_SUBMIT != decision) {
                        delete pnl;
                        pnl=NULL;
                        return false;
                }
                delete pnl;
                pnl=NULL;
                return true;
        }else
        return false;
}
void ViewManager::SaveView(int projectId)
{
        AbstractView* curView = NULL;
        for(int i = GetViewCount() - 1; i >=0 ; i--) {
                curView = GetView(i);
                if(curView!=NULL&&curView->GetProjectId() == projectId) {
                        curView->Save();
                        return;
                }
        }
}
void ViewManager::SaveViews(AbstractView* view)
{
        AbstractView* curView = NULL;
        for(int i = GetViewCount() - 1; i >=0 ; i--) {
                curView = GetView(i);
                if(view == NULL || view == curView) {
                        curView->Save();
                }
        }
}
bool ViewManager::SaveViewAs(int projectId,const wxString & projectFileName)
{
        AbstractView* view = NULL;
        for(int i = GetViewCount() - 1; i >=0 ; i--) {
                view = GetView(i);
                if(view!=NULL&&view->GetProjectId()==projectId)
                {
                    wxString tmp=FileUtil::ChangeFileExt(projectFileName,wxMSD_FILE_SUFFIX);
                    view->SetFileName(tmp);
                    tmp=tmp.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
                    view->SetTitle(tmp);
                        view->Save();
                        m_pNotebook->SetPageText(GetViewIndex(view),tmp+wxT("::Molecule"));
                        return true;
                }
        }
        return false;
}
void   ViewManager::UpdateProjectTitle(SimuProject* project)
{
        AbstractView* view = NULL;
        for(unsigned i = 0; i < m_pNotebook->GetPageCount(); i++) {
                view = GetView(i);
                if(view->GetProjectId() == project->GetProjectId()) {
                        m_pNotebook->SetPageText(i, view->GetViewTitle(project->GetTitle()));
                }
    }
}
//************************************************************************************************
//      event handle function
//************************************************************************************************
void ViewManager::OnViewPageClose(wxAuiNotebookEvent& event)
{//the page has been or will be deleted automatically.do not delete it in the closeview() function.
        if(!CloseView(event.GetSelection())) {
                event.Veto();
        }
        Tools::ShowStatus(wxT(" "),2);
}
void ViewManager::OnViewPageChanged(wxAuiNotebookEvent& event)
{
        bool timer = false;
        AbstractView* oldView = GetView(event.GetOldSelection());
        AbstractView* newView = GetView(event.GetSelection());
        if(oldView && newView) {
//                if(oldView->GetType() != newView->GetType()) {
                        oldView->EnableMenuTool(false);
                        newView->EnableMenuTool(true);
//                }
                    newView->UpdateMenuTool();
        }
//        wxMessageBox(wxT("before newView"));
        if(newView) {
                SimuProject* pProject = Manager::Get()->GetProjectManager()->GetProject(newView->GetProjectId());
                if(pProject!=NULL)
                Manager::Get()->GetProjectManager()->SetActiveProject(pProject);
                if(newView->GetType() == VIEW_GL_DISPLAY_SURFACE || newView->GetType() == VIEW_GL_DISPLAY_VIBRATION) {
                        if(newView->GetType() == VIEW_GL_DISPLAY_SURFACE && pProject) {
/*                                SurfacePnl* pSurfacePnl = static_cast<SurfacePnl*>(AuiSharedWidgets::Get()->GetWindow(STR_PNL_SURFACE));
                                if(pSurfacePnl) {
                                        wxArrayString desFiles = pProject->GetFiles(newView->GetTitle())->GetSurfaceDataFiles();
                                        pSurfacePnl->AppendDenFile(desFiles);
                                }

*/
//        wxMessageBox(wxT("before Ininsurfaceorvib"));
                newView->InitSurfaceOrVib();
                AuiSharedWidgets::Get()->ShowExclusiveAuiWidget(STR_PNL_SURFACE);
    //                Manager::Get()->GetAppFrame()->GetMenuBar()->Enable(MENU_ID_VIEW_CAPTURE_IMAGE, true);


                        }else if(newView->GetType() == VIEW_GL_DISPLAY_VIBRATION && pProject) {
        /*                        VibrationPnl* pVibPnl = static_cast<VibrationPnl*>(AuiSharedWidgets::Get()->GetWindow(STR_PNL_VIBRATION));
                                if(pVibPnl) {
                                        AbstractProjectFiles* pFiles = pProject->GetFiles(newView->GetTitle());
        //                                wxMessageBox(pFiles->GetVibOutputFileName());
                                        pFiles->GetIRDatas()->LoadFile(pFiles->GetVibOutputFileName());

                                        pVibPnl->AddVibDisplayData(pFiles->GetIRDatas()->GetIRDataArray());//pProject->GetFiles(newView->GetTitle())->GetVibrationData());

                                }
        */
                newView->InitSurfaceOrVib();
                AuiSharedWidgets::Get()->ShowExclusiveAuiWidget(STR_PNL_VIBRATION);
                                timer = true;
                        }
                }else if(newView->GetType() ==VIEW_GL_BUILD){
//                        MolViewEventHandler::Get()->GetMolView()->GetGLCanvas()->InitFont();
                        CtrlInst* pCtrlInst = MolViewEventHandler::Get()->GetMolView()->GetData()->GetActiveCtrlInst();
                        if(pCtrlInst->isShowSymmetry)
                                MolViewEventHandler::Get()->DoCalcSymmetry();
                        else
                                Tools::ShowStatus(wxT(""), 2);
                        AuiSharedWidgets::Get()->ShowExclusiveAuiWidget(wxEmptyString);
                }else{
                        AuiSharedWidgets::Get()->ShowExclusiveAuiWidget(wxEmptyString);
                }
        }


        if(timer && !m_timerVib.IsRunning()) {
                m_timerVib.Start(50);
        }else {
                m_timerVib.Stop();
        }
        //refresh the save button;

}
void ViewManager::OnTimerVibration(wxTimerEvent& event) {
        VibrationPnl* pVibPnl = static_cast<VibrationPnl*>(AuiSharedWidgets::Get()->GetWindow(STR_PNL_VIBRATION));
        if(!pVibPnl) return;
        int timerCyle=pVibPnl->GetVibCyle();
        static int oldtimerCyle=0;
        if (timerCyle!=oldtimerCyle){
                oldtimerCyle=timerCyle;
                m_timerVib.Stop();
//                delete m_timerVib;
//            m_timerVib = new wxTimer(this, TIMER_ID);
                m_timerVib.Start(timerCyle);
        }
        MolView* pView = static_cast<MolView*>(GetActiveView());
        if(!pView) return;
        SimuProject* pProject = Manager::Get()->GetProjectManager()->GetProject(pView->GetProjectId());
        if(!pProject) return;
        AbstractProjectFiles* pFiles = pProject->GetFiles(pView->GetTitle());
        pVibPnl->OnTimer(pView, pFiles);
}
