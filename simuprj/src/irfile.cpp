/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>

#include "measure.h"
#include "commons.h"
#include "irfile.h"
#include "tools.h"

#include <wx/arrimpl.cpp>


const wxString FILE_BEGIN_IRS = wxT("$BEGIN IRS");
const wxString FILE_END_IRS = wxT("$END IRS");
const wxString FILE_BEGIN_IR = wxT("$BEGIN IR");
const wxString FILE_END_IR = wxT("$END IR");

WX_DEFINE_OBJARRAY(IRDataArray);

IRData::IRData(){
frequency=0;
irtype=wxEmptyString;
intensity=0;
}

void IRFile::LoadFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels)
{
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
        ElemInfo* eleInfoOption = NULL;
        int i = 0, atom2Index;
        BondType bondType = BOND_TYPE_1;
        wxString atomSymbol = wxEmptyString;
    long atomId;
    double x, y, z;
    long atomNumber;

        int atomCountBase = appendAtomBondArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;
        strLine = tis.ReadLine();
    strLine.ToLong(&atomNumber); // the number of atoms
    tis.ReadLine();         // omit the comment line

    for(i = 0; i < atomNumber; i++) {
        strLine = tis.ReadLine();       // read one line
        wxStringTokenizer tzk(strLine);
        AtomBond atomBond = AtomBond();
        atomSymbol = tzk.GetNextToken();
        atomId = GetElemId(atomSymbol);
        tzk.GetNextToken().ToDouble(&x);
        tzk.GetNextToken().ToDouble(&y);
        tzk.GetNextToken().ToDouble(&z);
        atomBond.atom.elemId = (int)atomId;
        if(atomBond.atom.elemId == ELEM_H) {
                atomBond.atom.elemId = (ElemId)DEFAULT_HYDROGEN_ID;
        }
        atomBond.atom.pos.x = x;
        atomBond.atom.pos.y = y;
        atomBond.atom.pos.z = z;
        appendAtomBondArray.Add(atomBond);
    }

    //create bond type
    for(i = atomCountBase; i < atomCountBase + atomNumber; i++) {
        eleInfoOption = GetElemInfo((ElemId)appendAtomBondArray[i].atom.elemId);

        appendAtomBondArray[i].atom.SetDispRadius(eleInfoOption->dispRadius);
        appendAtomBondArray[i].atom.label = ++atomLabels[ELEM_PREFIX + appendAtomBondArray[i].atom.elemId];

            for(atom2Index = i + 1; atom2Index < atomCountBase + atomNumber; atom2Index++) {
            bondType = XYZFile::CalculateBondType(i, atom2Index, appendAtomBondArray);

            if (bondType == BOND_TYPE_0) {
            }else if (bondType == BOND_TYPE_1 || bondType == BOND_TYPE_2 || bondType == BOND_TYPE_3 || bondType == BOND_TYPE_4) {
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].atomIndex = atom2Index;
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].bondType = bondType;

                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].atomIndex = i;
                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].bondType = bondType;

                appendAtomBondArray[i].bondCount++;
                appendAtomBondArray[atom2Index].bondCount++;
            }else if (bondType == BOND_TYPE_5) {
                Tools::ShowError(wxT("Until now we have not supported the fourth bond list!"));
            }else {
                Tools::ShowError(wxT("The bond type is wrong!!!"));
            }
        }
    }
}

void IRFile::SaveFile(const wxString fileName,IRDataArray irdataarray) {
    unsigned int i,j;

    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        wxString format = wxEmptyString;

    tos << FILE_BEGIN_IRS << endl;


    for(i = 0; i < irdataarray.GetCount(); i++) {
                tos << FILE_BEGIN_IR << endl;
                tos<<wxString::Format(wxT("%.8f %.8f"),irdataarray[i].frequency,irdataarray[i].intensity)<<endl;
                tos<<irdataarray[i].irtype<<endl;
                for(j=0;j<irdataarray[i].irArray.GetCount();j++){
                   format = wxT(" %.8f %.8f %.8f\n");
            tos << wxString::Format(format, irdataarray[i].irArray[j].x,irdataarray[i].irArray[j].y,irdataarray[i].irArray[j].z);
        }
                tos << FILE_END_IR << endl;
    }
        tos << FILE_END_IRS << endl;

}

IRDatas::IRDatas(){
m_irdataarray.Empty();
enhanceswing=1;
nStep=10;
}
IRDatas::~IRDatas(){
m_irdataarray.Clear();
}

IRDataArray& IRDatas::GetIRDataArray(){
return m_irdataarray;
}

IRData& IRDatas::GetOldCoord(){
return oldcoord;
}

bool IRDatas::LoadFile(const wxString& strFileName)
{
        wxString strLine;
        wxFileInputStream fis(strFileName);
        wxTextInputStream tis(fis);
    double x, y, z;
         int i = 0;
        wxString atomSymbol = wxEmptyString;
    long atomNumber;
    m_irdataarray.Clear();
        m_deltaArray.Clear();
        oldcoord.irArray.Clear();
        strLine = tis.ReadLine();
    strLine.ToLong(&atomNumber); // the number of atoms
    tis.ReadLine();         // omit the comment line

    for(i = 0; i < atomNumber; i++) {
        strLine = tis.ReadLine();       // read one line
        wxStringTokenizer tzk(strLine);
        atomSymbol = tzk.GetNextToken();
                AtomPosition3F atom=AtomPosition3F();
        tzk.GetNextToken().ToDouble(&x);
        tzk.GetNextToken().ToDouble(&y);
        tzk.GetNextToken().ToDouble(&z);
        atom.x = (float)x;
        atom.y = (float)y;
        atom.z = (float)z;
                oldcoord.irArray.Add(atom);
    }

         while(!fis.Eof()&&FILE_BEGIN_IRS.Cmp(strLine=tis.ReadLine()) != 0);
        while(!fis.Eof()&& FILE_END_IRS.Cmp(strLine) != 0)
        {//IRS BEGIN
//            wxMessageBox(wxT("end_irs")+strLine);
                IRData irdata = IRData();
                IRData deltaIrdata=IRData();
                while(!fis.Eof()&&FILE_BEGIN_IR.Cmp(strLine) != 0)strLine=tis.ReadLine();
                strLine=tis.ReadLine();
                wxStringTokenizer tzk(strLine);
                double frequency;
                double intensity;
                tzk.GetNextToken().ToDouble(&frequency);
                tzk.GetNextToken().ToDouble(&intensity);
                wxString irtype;
                strLine=tis.ReadLine();
                irtype=strLine;
                irdata.frequency=frequency;
                irdata.intensity=intensity;
                irdata.irtype=irtype;
                i=0;
                while(!fis.Eof()&&FILE_END_IR.Cmp(strLine = tis.ReadLine()) != 0)
                {//IR BEGIN
        //    wxMessageBox(wxT("end_ir")+strLine);
            wxStringTokenizer tzk(strLine);
                        AtomPosition3F atom=AtomPosition3F();
                        AtomPosition3F deltaatom=AtomPosition3F();
            tzk.GetNextToken().ToDouble(&x);
            tzk.GetNextToken().ToDouble(&y);
            tzk.GetNextToken().ToDouble(&z);
            atom.x = (float)x+oldcoord.irArray[i].x;
            atom.y = (float)y+oldcoord.irArray[i].y;
            atom.z = (float)z+oldcoord.irArray[i].z;
                        irdata.irArray.Add(atom);
            deltaatom.x = (float)(x/nStep);
            deltaatom.y = (float)(y/nStep);
            deltaatom.z = (float)(z/nStep);
                        deltaIrdata.irArray.Add(deltaatom);
                        i++;
        }//IR END
                m_irdataarray.Add(irdata);
                m_deltaArray.Add(deltaIrdata);
                strLine = tis.ReadLine();
          //  wxMessageBox(wxT("end_end_irs")+strLine);
        }//IRS END

//        IRFile::SaveFile(wxT("c://test.txt"),m_irdataarray);
        return true;
}

void IRDatas::SaveFile(const wxString fileName)
{
    unsigned int i,j;

    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        wxString format = wxEmptyString;

    tos << FILE_BEGIN_IRS << endl;


    for(i = 0; i < m_irdataarray.GetCount(); i++) {
                tos << FILE_BEGIN_IR << endl;
                tos<<wxString::Format(wxT("%.8f"),m_irdataarray[i].frequency)<<endl;
                tos<<m_irdataarray[i].irtype<<endl;
                for(j=0;j<m_irdataarray[i].irArray.GetCount();j++){
                   format = wxT(" %.8f %.8f %.8f\n");
            tos << wxString::Format(format, m_irdataarray[i].irArray[j].x,m_irdataarray[i].irArray[j].y,m_irdataarray[i].irArray[j].z);
        }
                tos << FILE_END_IR << endl;
    }
        tos << FILE_END_IRS << endl;
}

void IRData::RotateWithZ(AtomPosition3F &pos,double z){
        double tempX, tempY;
        double cosAlpha = cos(z * RADIAN);
        double sinAlpha = sin(z * RADIAN);

        tempX = pos.x * cosAlpha - pos.x * sinAlpha;
        tempY = pos.y * sinAlpha + pos.y * cosAlpha;

        pos.x = tempX;
        pos.y = tempY;

}
void IRData::RotateWithY(AtomPosition3F &pos,double y){
        double tempX, tempZ;
        double cosAlpha = cos(y * RADIAN);
        double sinAlpha = sin(y * RADIAN);

        tempX = pos.x * cosAlpha + pos.z * sinAlpha;
        tempZ = pos.x * (-sinAlpha) + pos.z * cosAlpha;

        pos.x = tempX;
        pos.z = tempZ;

}
void IRData::RotateWithX(AtomPosition3F &pos,double x){
        double tempY, tempZ;
        double cosAlpha = cos(x * RADIAN);
        double sinAlpha = sin(x * RADIAN);

        tempY = pos.y * cosAlpha - pos.z * sinAlpha;
        tempZ = pos.y * sinAlpha + pos.z * cosAlpha;

        pos.y = tempY;
        pos.z = tempZ;

}
void IRData::RotateIRData(double transX, double transY, double transZ,float rotx,float roty,float rotz){
for (unsigned i=0;i<irArray.Count();i++){
     RotateWithX(irArray[i],rotx);
     RotateWithY(irArray[i],roty);
     RotateWithY(irArray[i],roty);
        }
}

void IRData::Translate(AtomPosition3F &pos,double transX, double transY, double transZ)
{
pos.x+=transX;
pos.y+=transY;
pos.z+=transZ;
}

void IRDatas::DrawIRLine(void){

        GLfloat ctrlpoints[4][3]={
        {-4.0f,-4.0f,0.0f},{-2.0f,4.0f,0.0f},
        {2.0f,-4.0f,0.0f},{4.0f,4.0f,0.0f}
        };

//        glClearColor(0.0,0.0,0.0,1.0);
        glMap1f(GL_MAP1_VERTEX_3,0.0,1.0,3,4,&ctrlpoints[0][0]);
        glEnable(GL_MAP1_VERTEX_3);
        glShadeModel(GL_FLAT);
        int i;
//        glClear(GL_COLOR_BUFFER_BIT);
        glColor3f(1.0,1.0,1.0);
        glBegin(GL_LINE_STRIP);
        for(i=0;i<30;i++)
                glEvalCoord1f((GLfloat)i/30.0);
        glEnd();
}

void IRDatas::SetEnhanceSwing(float swing)
{
enhanceswing=swing;
}

float IRDatas::GetEnhanceSwing()
{
return enhanceswing;
}


IRDataArray& IRDatas::GetDeltaArray()
{
return m_deltaArray;
}
