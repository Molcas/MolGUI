/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/dialog.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/dir.h>
#include "simuprjdef.h"
#include "envutil.h"
#include "dlgjobname.h"
#include "fileutil.h"
#include "stringutil.h"
#include "manager.h"
#include "simuproject.h"
#include <wx/string.h>

int CTRL_JOBNAME_ID_TXT_JOBNAME=wxNewId();
int CTRL_JOBNAME_ID_LC_JOBNAME=wxNewId();
int CTRL_JOBNAME_ID_BT_OK=wxNewId();
int CTRL_JOBNAME_ID_BT_CANCLE=wxNewId();
int POP_MENU_ITEM_DELETE=wxNewId();
BEGIN_EVENT_TABLE(DlgJobName,wxDialog)
        EVT_LIST_ITEM_ACTIVATED(wxID_ANY, DlgJobName::OnLcActiveItem)
        EVT_BUTTON(wxID_ANY, DlgJobName::OnButton)
        EVT_TEXT(wxID_ANY,DlgJobName::OnText)
        EVT_LIST_ITEM_RIGHT_CLICK(CTRL_JOBNAME_ID_LC_JOBNAME,DlgJobName::OnRClickItem)
        EVT_TEXT_ENTER(CTRL_JOBNAME_ID_TXT_JOBNAME,DlgJobName::OnEnter)

        EVT_MENU(POP_MENU_ITEM_DELETE,DlgJobName::OnDeleteItem)
END_EVENT_TABLE()

DlgJobName::DlgJobName(wxWindow* parent,SimuProject*   pProject,wxWindowID id,const wxString& title,const wxPoint& pos,const wxSize& size,
                                        long style, int flg)
{
    wxDialog::Create(parent,id,title,pos,wxSize(280,290),style);
    m_pProject=pProject;
    CreateUI();
    InitLc();
    dflag =  flg;
     m_selectedItem=-1;
}
DlgJobName::~DlgJobName()
{}
void DlgJobName::CreateUI()
{
//        wxString jobName=wxString::Format(wxT("%s%d"),wxT("Job"),::wxGetUTCTime());
    wxBoxSizer* bs=new  wxBoxSizer(wxVERTICAL);

    wxStaticText*   label=new    wxStaticText(this,wxID_ANY,wxT("Please specify the job name:"),wxPoint(10,10),wxSize(200,25));
    bs->Add(label,1,wxEXPAND);

    m_txtJobName=new    wxTextCtrl(this,CTRL_JOBNAME_ID_TXT_JOBNAME,wxT(""),wxPoint(10,40),wxSize(250,25),wxTE_PROCESS_ENTER);
//        m_txtJobName->AppendText(jobName);
    m_txtJobName->SetFocus();
    bs->Add(m_txtJobName,1,wxEXPAND);

    label=new    wxStaticText(this,wxID_ANY,wxT("Existing job names:"),wxPoint(10,75),wxSize(200,25));
    bs->Add(label,1,wxEXPAND);

    m_lcJobName=new     wxListCtrl(this,CTRL_JOBNAME_ID_LC_JOBNAME,wxPoint(10,100),wxSize(250,120),wxLC_LIST|wxLC_SINGLE_SEL|wxBORDER_SUNKEN);
    bs->Add(m_lcJobName,1,wxEXPAND);

    wxBoxSizer* bsBt=new  wxBoxSizer(wxHORIZONTAL);
    wxButton*  bt=new    wxButton(this,CTRL_JOBNAME_ID_BT_OK,wxT("Ok"),wxPoint(120,230),wxSize(60,25));
    bsBt->Add(bt,1,wxEXPAND);
    bt=new    wxButton(this,CTRL_JOBNAME_ID_BT_CANCLE,wxT("Cancel"),wxPoint(190,230),wxSize(60,25));
    bsBt->Add(bt,1,wxEXPAND);
    bs->Add(bsBt,1,wxEXPAND);
}
void DlgJobName::InitLc()
{
    if(m_pProject==NULL)
        return;

    wxString dir=m_pProject->GetFileName().BeforeLast(platform::PathSep().GetChar(0));
    wxDir   theDir(dir);
    wxString    fileName=wxEmptyString;
    bool cont = theDir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
    wxString    tmp;
    wxArrayString   existJob;
    while ( cont ){
        tmp=m_pProject->GetFiles(fileName)->GetInputFileName();//dir+platform::PathSep()+fileName+platform::PathSep()+fileName+wxMINPUT_FILE_SUFFIX;
//        wxMessageBox(tmp);
        if(wxFileExists(tmp))
            existJob.Add(fileName);
        cont = theDir.GetNext(&fileName);
    }
//    wxListItem item;
//    item.SetMask(wxLIST_MASK_STATE|wxLIST_MASK_TEXT|wxLIST_MASK_DATA);
//    item.SetImage(-1);
//    item.SetAlign(wxLIST_FORMAT_LEFT);
//    item.SetText(wxT("Existing Jobs"));
//    m_lcJobName->InsertColumn(0, item);
//    m_lcJobName->SetColumnWidth(0,250);
    existJob.Sort();
    for(unsigned int i=0;i<existJob.GetCount();i++){
        tmp=existJob[i];
//        int index=m_lcJobName->InsertItem(i, tmp);
        m_lcJobName->InsertItem(i, tmp);
        m_lcJobName->SetItemData(i,i);//data is larger  the item index, used fo UpdateInfoHtml();
 //       if(i%2==0)
 //           m_lcJobName->SetItemBackgroundColour(index, wxColour(0xFE,0xF9,0xD3));
    }
//        m_txtJobName->SetValue(wxString::Format(wxT("%s%d"),wxT("Job"),::wxGetUTCTime()));
//        m_txtJobName->SetSelection(-1,-1);
}
wxString    DlgJobName::GetJobName()
{
    wxString jobName=wxEmptyString;
    if(m_txtJobName!=NULL)
        jobName=m_txtJobName->GetValue();
    return jobName;
}
///////////////////////////////////////////////////////////////
void DlgJobName::OnButton(wxCommandEvent& event)
{

    int id=event.GetId();
    if(id==CTRL_JOBNAME_ID_BT_OK)
    {
   wxString jobName=m_txtJobName->GetValue();


                if (1 == dflag && jobName.IsEmpty()) {
                        if (!wxFileExists(jobName)) {
                                wxMessageBox(wxT("Please select a job first!"));
                                return;
                        }
                }

                if(jobName.IsEmpty()){
                        jobName=wxString::Format(wxT("%s%d"),wxT("Job"),::wxGetUTCTime());
                        m_txtJobName->SetValue(jobName);
                }
        wxString dir=m_pProject->GetFileName().BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+jobName;
                jobName=dir+platform::PathSep()+jobName+wxMINPUT_FILE_SUFFIX;

                        if(wxFileExists(jobName) && 0 == dflag) {
                                wxMessageDialog mdlg(this,wxT("The job file exists, do you want to overwrite it?"),wxT("warning"),wxYES_NO);
            if(mdlg.ShowModal()==wxID_YES){
                FileUtil::RemoveDirectory(dir);
 //               FileUtil::CreateDirectory(dir);
                EndModal(wxID_OK);
            }else
                return;
                        }

                        if (1 == dflag) {
                                if(!wxFileExists(jobName)) {
                                        wxMessageBox (wxT("No such job!"));
                                        return ;
                                }
                        }

 //       FileUtil::CreateDirectory(dir);
        EndModal(wxID_OK);
    }else{
        EndModal(wxID_CANCEL);
    }
}
void DlgJobName::OnEnter(wxCommandEvent& event)
{
        event.SetId(CTRL_JOBNAME_ID_BT_OK);
        OnButton(event);;
}
void DlgJobName::OnLcActiveItem(wxListEvent& event)
{
    wxString    name=event.GetText();
    m_txtJobName->SetValue(name);
}
void DlgJobName::OnText(wxCommandEvent& event)
{
    if(m_txtJobName!=NULL)
    {
        wxString tmpJobName=m_txtJobName->GetValue();
        if(!tmpJobName.IsEmpty()){
            wxString    outString=StringUtil::CheckInput(tmpJobName);
            if(!outString.IsSameAs(tmpJobName))
            {
                wxBell();
                m_txtJobName->SetValue(outString);
            }
        }
    }
}
void    DlgJobName::OnRClickItem(wxListEvent& event){

    m_selectedItem=event.GetData();
    wxMenu popMenu;
    popMenu.Append(POP_MENU_ITEM_DELETE,wxT("Delete"));
    m_lcJobName->PopupMenu(&popMenu, event.GetPoint());

}
void    DlgJobName::OnDeleteItem(wxCommandEvent& event){
    if(m_lcJobName==NULL)
         return ;
    wxString itemText=m_lcJobName->GetItemText(m_selectedItem);
//wxMessageBox(itemText);
    SimuProject* pProject=Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject==NULL)
        return;
    wxString str=pProject->GetFileName().BeforeLast(platform::PathSep().GetChar(0));
    str+=platform::PathSep()+itemText;
    FileUtil::RemoveDirectory(str);
    //..................................................................

    m_lcJobName->DeleteItem(m_selectedItem);
    //..................................................................
    wxTreeItemId prjNodeId=pProject->GetProjectNode();
    if(!prjNodeId.IsOk())
               return;
    wxTreeCtrl * pTree=Manager::Get()->GetProjectManager()->GetTree();
    if(pTree==NULL)
        return;
    wxTreeItemIdValue        subCookie;
    int subItemCount=pTree->GetChildrenCount(prjNodeId,false);
    wxTreeItemId        subChild=pTree->GetFirstChild(prjNodeId,subCookie);
    ProjectTreeItemData *subItem;
    for(int j=0;j<subItemCount;j++){
        subItem = subChild.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(subChild) : NULL;
            if(subItem==NULL)        break;
        wxString tmpSuf=subItem->GetTitle();
        if(tmpSuf.IsSameAs(itemText)){
            pTree->SelectItem(subChild);
            Manager::Get()->GetProjectManager()->DoPopDelete();
        }
        subChild=pTree->GetNextChild(prjNodeId,subCookie);
    }
}
