/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "resource.h"
#include <wx/scrolwin.h>

#include "simuprjinfoview.h"
#include "simuprjfrm.h"
#include "manager.h"
#include "fileutil.h"
#include "stringutil.h"
#include "xmlfile.h"
#include "projectgroup.h"
#include "envutil.h"
#include "envsettings.h"
#include "simuprjdef.h"

int CTRL_PRJINFO_ID_TEXT_SEARCH = wxNewId();
//int CTRL_PRJINFO_ID_BTN_SEARCH=wxNewId();
int CTRL_PRJINFO_ID_CB_SEARCH = wxNewId();
int CTRL_PRJINFO_ID_BTN_SEARCH = wxNewId();
int CTRL_PRJINFO_ID_HTML_INFO = wxNewId();
int CTRL_PRJINFO_ID_BTN_SHOW_LT = wxNewId();
int CTRL_PRJINFO_SPLWND_LT_HT = wxNewId();
int CTRL_PRJINFO_SPLWND_LB_LC = wxNewId();
int CTRL_PROJECT_INFO_LIST = wxNewId();
int CTRL_PROJECT_LIST_TABLE = wxNewId();
int CTRL_PRJINFO_ID_BTN_NEW_LT = wxNewId();
int CTRL_PRJINFO_ID_BTN_ADDITEM_LC = wxNewId();
//.................................................................................//
int POPUPMENU_ID_LC_ADD = wxNewId();
int POPUPMENU_ID_LC_DELETE = wxNewId();
int POPUPMENU_ID_LC_OPEN = wxNewId();
int POPUPMENU_ID_LC_EXPORT = wxNewId();

int POPUPMENU_ID_LT_SETPATH = wxNewId();
int POPUPMENU_ID_LT_RENAME = wxNewId();
int POPUPMENU_ID_LT_ADD = wxNewId();
int POPUPMENU_ID_LT_DELETE = wxNewId();

BEGIN_EVENT_TABLE(SimuPrjInfoView, AbstractView)
    EVT_SPLITTER_SASH_POS_CHANGED(CTRL_PRJINFO_SPLWND_LB_LC, SimuPrjInfoView::OnWndLtLcSizeChange)
    EVT_SPLITTER_SASH_POS_CHANGED(CTRL_PRJINFO_SPLWND_LT_HT, SimuPrjInfoView::OnWndLtHtSizeChange)
    EVT_SPLITTER_DCLICK(wxID_ANY, SimuPrjInfoView::OnDbClkSash)
    EVT_TEXT(CTRL_PRJINFO_ID_TEXT_SEARCH, SimuPrjInfoView::OnTxtSearch)
    EVT_COMBOBOX(CTRL_PRJINFO_ID_CB_SEARCH, SimuPrjInfoView::OnCbSchType)
    //...........................................................................................................//
    EVT_BUTTON(CTRL_PRJINFO_ID_BTN_SEARCH, SimuPrjInfoView::OnBtnSearch)
    EVT_BUTTON(CTRL_PRJINFO_ID_BTN_SHOW_LT, SimuPrjInfoView::OnShowLT)
    EVT_BUTTON(CTRL_PRJINFO_ID_BTN_NEW_LT, SimuPrjInfoView::OnNewList)
    EVT_BUTTON(CTRL_PRJINFO_ID_BTN_ADDITEM_LC, SimuPrjInfoView::OnAddToList)
    EVT_MENU(POPUPMENU_ID_LC_ADD, SimuPrjInfoView::OnAddToList)
    EVT_MENU(POPUPMENU_ID_LC_DELETE, SimuPrjInfoView::OnDelFromList)
    EVT_MENU(POPUPMENU_ID_LC_EXPORT, SimuPrjInfoView::OnExpToList)
    EVT_MENU(POPUPMENU_ID_LT_SETPATH, SimuPrjInfoView::OnSetPathForDisp)
    EVT_MENU(POPUPMENU_ID_LT_RENAME, SimuPrjInfoView::OnRenameList)
    EVT_MENU(POPUPMENU_ID_LT_ADD, SimuPrjInfoView::OnNewList)
    EVT_MENU(POPUPMENU_ID_LT_DELETE, SimuPrjInfoView::OnDelLtList)
    //............................................................................................................//
    EVT_LIST_COL_CLICK(wxID_ANY, SimuPrjInfoView::OnClkLcColum)
    EVT_LIST_COL_RIGHT_CLICK(wxID_ANY, SimuPrjInfoView::OnRClickColumn)
    EVT_LIST_ITEM_SELECTED(wxID_ANY, SimuPrjInfoView::OnSelectItem)
    EVT_LIST_ITEM_ACTIVATED(wxID_ANY, SimuPrjInfoView::OnActiveItem)
    EVT_LIST_ITEM_RIGHT_CLICK(wxID_ANY, SimuPrjInfoView::OnRClkItem)
    EVT_LIST_BEGIN_LABEL_EDIT(CTRL_PROJECT_LIST_TABLE, SimuPrjInfoView::OnBeginEdLtItem)
    EVT_LIST_END_LABEL_EDIT(CTRL_PROJECT_LIST_TABLE, SimuPrjInfoView::OnEndEdLtItem)
    //................................................................................................................//
    EVT_SIZE(SimuPrjInfoView::OnResize)
    //  EVT_RIGHT_DOWN(SimuPrjInfoView::OnRClkItem)
END_EVENT_TABLE()

SimuPrjInfoView::SimuPrjInfoView(wxWindow *parent, wxEvtHandler *evtHandler) : AbstractView(parent, evtHandler, VIEW_PROJECTINFO)
{
    m_viewId = PRJINFO_PID;
    m_pSpWnd = NULL;
    m_pShowLt = NULL;
    m_pAddToList = NULL;
    m_isShowLt = true;
    m_pSpWndLT = NULL;
    m_pListPan = NULL;
    m_pHtmlPan = NULL;
    m_pListTablePn = NULL;
    m_pSearchText = NULL;
    m_pCbSearch = NULL;
    m_htmlPath = EnvUtil::GetPrgResDir() + platform::PathSep() + wxT("res/prj/project_info.html");
    m_pListPn = NULL;
    m_pList = NULL;
    m_pHtmlInfo = NULL;
    m_pListTable = 0;
    m_ltIndex = 0;
    m_lcIndex = 0;
    m_schType = 1;
    m_colName.Clear();
    m_colNameId = NULL;
    m_lcItem.Clear();
    m_lcTmpItem.Clear();
    for(int i = 0; i < PRJINFO_COLNUM; i++)
        m_isAccend[i] = false;

    SetTitle(SIMU_PRJINFO_VIEW_TITLE);
    SetFileName(SIMU_PRJINFO_VIEW_PATH);

    wxSize   listSize = parent->GetClientSize();
    m_clientSize = listSize; //must be initialized before CreateUI;
    CreateUI();
    if(m_pList != NULL)
        InitView();
}
SimuPrjInfoView::~SimuPrjInfoView()
{
    if(m_colNameId != NULL)
    {
        delete m_colNameId;
        m_colNameId = NULL;
    }
    m_colName.Clear();
    m_lcItem.Clear();
    m_lcTmpItem.Clear();
}
//************************************************************************************
//  private function
//************************************************************************************
void SimuPrjInfoView::AddItems()
{
    int countItem = m_lcItem.GetCount();
    int countColumn = m_colName.GetCount();

    wxListItem  tmpLItem;
    wxString     tmpStr = wxEmptyString;
    if(m_colNameId == NULL)
        return ;
    int index = 0;
    for(int i = 0; i < countItem; i++) {
        tmpStr = GetColText(m_lcItem[i], m_colNameId[0]);
        index = m_pList->InsertItem(i, tmpStr, -1);
        m_pList->SetItemData(i, i + 1); //data is larger  the item index, used fo UpdateInfoHtml();
        if(i % 2 == 0)
            m_pList->SetItemBackgroundColour(index, wxColour(0xFE, 0xF9, 0xD3));
        for(int j = 1; j < countColumn; j++) {
            tmpStr = GetColText(m_lcItem[i], m_colNameId[j]);
            m_pList->SetItem(index, j, tmpStr);
        }
    }
}
wxArrayString SimuPrjInfoView::AnaFindStr(wxString findStr)
{
    wxArrayString result;
    result.Clear();
    wxString    tmpS = wxEmptyString, tmpQ = wxEmptyString;
    if(findStr.Find(wxT('*')) != wxNOT_FOUND || findStr.Find(wxT('?')) != wxNOT_FOUND) {
        while(true) {
            tmpS = findStr.BeforeFirst(wxT('*'));
            tmpQ = findStr.BeforeFirst(wxT('?'));
            if(tmpS.IsEmpty() && tmpQ.IsEmpty()) {
                break;
            } else if(tmpS.IsEmpty() || (tmpQ.IsEmpty() == false && tmpS.Len() > tmpQ.Len())) { //take the short one
                result.Add(tmpQ);
                findStr = findStr.AfterFirst(wxT('?'));
            } else if(tmpQ.IsEmpty() || (tmpS.IsEmpty() == false && tmpQ.Len() > tmpS.Len())) {
                result.Add(tmpS);
                findStr = findStr.AfterFirst(wxT('*'));
            } else
                break;
        }
    }
    if(!findStr.IsEmpty())
        result.Add(findStr);
    return result;
}
wxArrayString SimuPrjInfoView::FindInList(wxArrayString &strList, wxString findStr)
{
    wxArrayString result, tmpCol;
    tmpCol.Clear();
    result = strList;
    if(findStr.IsEmpty())
        return result;
    findStr = findStr.Lower();
    wxArrayString findArrayStr = AnaFindStr(findStr);
    //search in strList;
    int count, fcount = findArrayStr.GetCount();
    wxString    tmpC = wxEmptyString, tmpS = wxEmptyString;
    for(int i = 0; i < fcount; i++) {
        tmpC = findArrayStr[i];
        count = result.GetCount();
        for(int j = 0; j < count; j++) {
            tmpS = GetColText(result[j], NAME);
            tmpS = tmpS.Lower();
            if(tmpS.Len() > tmpC.Len()) {
                if(tmpS.Contains(tmpC)) {
                    bool isNew = true;
                    for(unsigned int id = 0; id < tmpCol.GetCount(); id++) {
                        if(tmpCol[id].IsSameAs(result[j])) {
                            isNew = false;
                            break;
                        }
                    }
                    if(isNew == true)
                        tmpCol.Add(result[j]);
                }
            } else if(tmpS.IsSameAs(tmpC)) {
                bool isNew = true;
                for(unsigned int id = 0; id < tmpCol.GetCount(); id++) {
                    if(tmpCol[id].IsSameAs(result[j])) {
                        isNew = false;
                        break;
                    }
                }
                if(isNew == true)
                    tmpCol.Add(result[j]);
            }
        }
        result = tmpCol;
        tmpCol.Clear();
    }

    return result;
}
wxArrayString SimuPrjInfoView::FindInCurList(wxString str)
{
    return FindInList(m_lcItem, str);
}
wxArrayString SimuPrjInfoView::FindInAllList(wxString str)
{
    wxArrayString strList;
    strList.Clear();

    wxString listDir = EnvUtil::GetPrgRootDir() + wxLIST_INFO_FILE_PATH;
    if(!wxDirExists(listDir))
        return strList;
    wxArrayString   fileList = FileUtil::GetFileBySuffix(listDir, wxLIST_INFO_FILE_SUFFIX);
    int fcount = fileList.GetCount();
    wxString tmpFile = wxEmptyString, info = wxEmptyString;
    wxArrayString filePrjList;
    int i = 0;
    for(i = 0; i < fcount; i++) {
        tmpFile = fileList[i]; //deal with each list file;
        filePrjList.Clear();

        XmlFile xmlLF(wxLIST_INFO_FILE_ROOT, listDir + platform::PathSep() + tmpFile);
        filePrjList = xmlLF.GetArrayString(LIST_INFOF_AR_ELEM); //abs path in xmlLF
        int num = filePrjList.GetCount();
        info = wxEmptyString;
        int j = 0;
        for( j = 0; j < num; j++) {
            if(!wxFileExists(filePrjList[j])) {
                filePrjList.RemoveAt(j);
                num = filePrjList.GetCount();
                j--;
                continue;
            }
            if(filePrjList[j].Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
                info = GetProjectInfo(filePrjList[j]);
            else if(filePrjList[j].Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX))
                info = GetPrjGrpInfo(filePrjList[j]);
            if(info.IsEmpty())
                continue;
            info = info + filePrjList[j];
            strList.Add(info);
        }
    }
    wxArrayString defStr = FindInSelPath(EnvUtil::GetDefWorkPath(), str);
    fcount = defStr.GetCount();
    for(i = 0; i < fcount; i++) {
        strList.Add(defStr[i]);
    }
    return FindInList(strList, str);
}
wxArrayString SimuPrjInfoView::FindInSelPath(wxString dir, wxString str)
{
    wxArrayString strList;
    strList.Clear();

    if(!wxDirExists(dir))
        return strList;

    wxString tmpNew, filename, info;
    wxArrayString  fileList, dirList;
    fileList.Clear();
    dirList.Clear();
    dirList.Add(dir);
    wxDir   theDir;
    int flag = wxDIR_DIRS, num = 0;
    while(dirList.GetCount() > 00) {
        dir = dirList[dirList.GetCount() - 1];
        dirList.RemoveAt(dirList.GetCount() - 1);

        theDir.Open(dir);
        num = 0;

        bool cont = theDir.GetFirst(&filename, wxEmptyString, flag);
        while ( cont ) { //put all sip and spg info into the infoList;
            if(filename.IsSameAs(wxT(".")) || filename.IsSameAs(wxT("..")))
                continue;
            fileList.Clear();
            tmpNew = dir + platform::PathSep() + filename;
            dirList.Add(tmpNew);
            fileList = FileUtil::GetFileBySuffix(tmpNew, wxPRJ_FILE_SUFFIX); //check sip file
            num = fileList.GetCount();
            int i = 0;
            for(i = 0; i < num; i++) {
                filename = tmpNew + platform::PathSep() + fileList[i];
                info = GetProjectInfo(filename);
                info = info + filename;
                strList.Add(info);
            }
            fileList.Clear();
            fileList = FileUtil::GetFileBySuffix(tmpNew, wxPG_FILE_SUFFIX); //check spg file
            num = fileList.GetCount();
            for(i = 0; i < num; i++) {
                filename = tmpNew + platform::PathSep() + fileList[i];
                info = GetPrjGrpInfo(filename);
                info = info + filename;
                strList.Add(info);
            }
            cont = theDir.GetNext(&filename);
        }
    }
    return FindInList(strList, str);
}
wxString SimuPrjInfoView::GetColText(wxString textval, int index) //index=0,1,....
{
    wxString text = wxEmptyString;
    int     colNum = 6;
    for(int i = 0; i < colNum; i++) {
        text = textval.BeforeFirst(wxT('*'));
        if(index == i)
            break;
        textval = textval.AfterFirst(wxT('*'));
    }
    return text;
}
wxString SimuPrjInfoView::GetProjectInfo(wxString fullPathName)//e.g: /home/project/mol/mol.sip
{
    if(!wxFileExists(fullPathName))
        return wxEmptyString;
    wxString info = wxEmptyString, tmp = wxEmptyString;
    XmlFile xmlProject = XmlFile(SimuProject::GetXmlFileRoot(), fullPathName);
    tmp = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.')); //name
    info = info + tmp + wxT("*");
    info = info + wxT("project") + wxT("*"); //type
    tmp = xmlProject.GetString(PRJ_XML_ELEM_CREATEDATE);
    info = info + tmp + wxT("*"); //create time
    tmp = xmlProject.GetString(PRJ_XML_ELEM_LASTMODIFY);
    info = info + tmp + wxT("*"); //last modify time
    tmp = xmlProject.GetString(PRJ_XML_ELEM_DESCRIPTION);
    info = info + tmp + wxT("*"); //last modify time

    return info;
}
wxString SimuPrjInfoView::GetPrjGrpInfo(wxString fullPathName)
{
    if(!wxFileExists(fullPathName))
        return wxEmptyString;
    wxString info = wxEmptyString, tmp = wxEmptyString;
    XmlFile xmlProject = XmlFile(ProjectGroup::GetXmlFileRoot(), fullPathName);
    tmp = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.')); //name
    info = info + tmp + wxT("*");
    info = info + wxT("project-group") + wxT("*"); //type
    tmp = xmlProject.GetString(PG_XML_ELEM_CREATEDATE);
    info = info + tmp + wxT("*"); //create time
    tmp = xmlProject.GetString(PG_XML_ELEM_LASTMODIFY);
    info = info + tmp + wxT("*"); //last modify time
    tmp = xmlProject.GetString(PG_XML_ELEM_DESCRIPTION);
    info = info + tmp + wxT("*"); //last modify time

    return info;
}
wxString SimuPrjInfoView::GetInfoFileFullName(wxString fileName)
{
    return EnvUtil::GetPrgRootDir() + wxLIST_INFO_FILE_PATH + platform::PathSep() + fileName + wxLIST_INFO_FILE_SUFFIX;
}
void       SimuPrjInfoView::SortItem(int index)
{   //sort the string in the m_lcItem by the index-th column;
    //we solve this problem by adding the index-th string to the beginning of the string;
    int icount = m_lcItem.GetCount();
    wxString tmp = wxEmptyString;
    int i = 0;
    for(i = 0; i < icount; i++) { //add the index-th colum text to the beginning of the string;
        tmp = GetColText(m_lcItem[i], index);
        m_lcItem[i] = tmp + wxT("*") + m_lcItem[i];
        //  wxMessageBox(m_lcItem[i]);
    }
    m_lcItem.Sort(m_isAccend[index]);
    for(i = 0; i < icount; i++) { //remove the addded string;
        m_lcItem[i] = m_lcItem[i].AfterFirst(wxT('*'));
        //  wxMessageBox(m_lcItem[i]);
    }
#ifndef __MAC__
    wxListItem item;
    item.SetMask(wxLIST_MASK_STATE | wxLIST_MASK_TEXT | wxLIST_MASK_DATA | wxLIST_MASK_IMAGE);
    item.SetImage(-1);
    item.SetAlign(wxLIST_FORMAT_LEFT);

    int count = m_colName.GetCount();
    for(int i = 0; i < count; i++) {
        item.SetText(m_colName[i]);
        if(i == index) {
            if(m_isAccend[index] == true)
                item.SetImage(0);
            else
                item.SetImage(1);
        } else {
            item.SetImage(-1);
        }
        m_pList->SetColumn(i, item);
        //       m_pList->SetColumnWidth(i, width );
        //       m_colNameId[i]=i;
    }
#endif
}
//************************************************************************************
//  public function
//************************************************************************************
void SimuPrjInfoView::CreateUI()
{
    wxSize   listSize(m_clientSize.GetWidth(), int(m_clientSize.GetHeight() * 0.6));

    m_pSpWnd = new wxSplitterWindow(this, CTRL_PRJINFO_SPLWND_LT_HT, wxDefaultPosition, m_clientSize);
    m_pSpWnd->SetMinimumPaneSize(100);
    m_pListPan = new wxPanel(m_pSpWnd, wxNewId(), wxDefaultPosition, wxDefaultSize, wxBORDER_SUNKEN | wxSP_3D | wxTAB_TRAVERSAL | wxFULL_REPAINT_ON_RESIZE);
    m_pHtmlPan = new wxPanel(m_pSpWnd);
    m_pSpWnd->SplitHorizontally(m_pListPan, m_pHtmlPan);
    wxBoxSizer *bs = new wxBoxSizer(wxVERTICAL);
    bs->Add(m_pSpWnd, 1, wxEXPAND);
    bs->Add(m_pListPan, 1, wxEXPAND);
    bs->Add(m_pHtmlPan, 1, wxEXPAND);

    wxBoxSizer *groupSizer = new wxBoxSizer(wxHORIZONTAL);
    m_pShowLt  = new wxButton(m_pListPan, CTRL_PRJINFO_ID_BTN_SHOW_LT, wxT("Hide List"), wxPoint(10, 10), wxSize(75, 25));
    groupSizer->Add(m_pShowLt);
    wxButton *button  = new wxButton(m_pListPan, CTRL_PRJINFO_ID_BTN_NEW_LT, wxT("New List"), wxPoint(100, 10), wxSize(75, 25));
    groupSizer->Add(button);
    m_pAddToList  = new wxButton(m_pListPan, CTRL_PRJINFO_ID_BTN_ADDITEM_LC, wxT("Add To List"), wxPoint(205, 10), wxSize(105, 25));
    m_pAddToList->Enable(false);
    groupSizer->Add(m_pAddToList);
    int     baseXSearch = m_clientSize.GetWidth() - 475 > 320 ? m_clientSize.GetWidth() - 475 : 320;
    wxStaticText *label = new wxStaticText(m_pListPan, wxID_ANY, wxT("Find: "), wxPoint(baseXSearch + 40, 15), wxSize(30, 25));
    groupSizer->Add(label);
    m_pSearchText = new wxTextCtrl(m_pListPan, CTRL_PRJINFO_ID_TEXT_SEARCH, wxEmptyString, wxPoint(baseXSearch + 80, 10), wxSize(170, 25));
    groupSizer->Add(m_pSearchText);
    label = new wxStaticText(m_pListPan, wxID_ANY, wxT(" In "), wxPoint(baseXSearch + 250, 15), wxSize(20, 25));
    groupSizer->Add(label);
    wxString    initVal = wxCOMBO_SCH_CURLIST;
    wxArrayString choices;
    choices.Add(wxCOMBO_SCH_CURLIST);
    choices.Add(wxCOMBO_SCH_ALLLIST);
    choices.Add(wxCOMBO_SCH_SELPATH);
    //choices.Add(wxT("All disk"));
    m_pCbSearch = new wxComboBox(m_pListPan, CTRL_PRJINFO_ID_CB_SEARCH, initVal, wxPoint(baseXSearch + 270, 10), wxSize(130, 25), choices, wxCB_DROPDOWN | wxCB_READONLY);
    groupSizer->Add(m_pCbSearch);
    //        button = new wxButton(m_pListPan, CTRL_PRJINFO_ID_BTN_SEARCH, wxT("Begin"), wxPoint(baseXSearch+410,10), wxSize(45, 25));
    //        groupSizer->Add(button);

    bs->Add(groupSizer, 1, wxEXPAND);

    wxSize  szLP = m_pListPan->GetClientSize();
    m_pSpWndLT = new wxSplitterWindow(m_pListPan, CTRL_PRJINFO_SPLWND_LB_LC, wxPoint(0, 45), wxSize(szLP.GetWidth(), szLP.GetHeight() - 50));
    m_pSpWndLT->SetMinimumPaneSize(100);
    m_pListTablePn = new wxPanel(m_pSpWndLT, wxNewId(), wxDefaultPosition, wxDefaultSize, wxBORDER_SUNKEN);
    m_pListPn = new wxPanel(m_pSpWndLT, wxNewId(), wxDefaultPosition, wxDefaultSize, wxBORDER_SUNKEN);
    m_pSpWndLT->SplitVertically(m_pListTablePn, m_pListPn, 200);

    int style = wxLC_REPORT | wxLC_SINGLE_SEL | wxBORDER_SUNKEN | wxLC_VRULES | wxLC_ALIGN_LEFT;
    wxSize szTP = m_pListTablePn->GetClientSize();
    szTP += wxSize(2, 2);
    m_pListTable = new wxListCtrl(m_pListTablePn, CTRL_PROJECT_LIST_TABLE, wxDefaultPosition, szTP, wxLC_EDIT_LABELS | style);
    szLP = m_pListPn->GetClientSize();
    szLP += wxSize(2, 2);
    m_pList = new wxListCtrl(m_pListPn, CTRL_PROJECT_INFO_LIST, wxDefaultPosition, szLP, style);

    bs->Add(m_pSpWndLT, 1, wxEXPAND);
    bs->Add(m_pListTablePn, 1, wxEXPAND);
    bs->Add(m_pListPn, 1, wxEXPAND);
    bs->Add(m_pListTable, 1, wxEXPAND);
    bs->Add(m_pList, 1, wxEXPAND);
    m_pListPan->SetMinSize(wxSize(600, 100));

    label = new wxStaticText(m_pHtmlPan, wxID_ANY, wxT("Information for selected item:"), wxPoint(10, 5), wxDefaultSize);
    bs->Add(label, 1, wxEXPAND);
    wxSize  szHP = m_pHtmlPan->GetClientSize();
    m_pHtmlInfo = new PInfoHtmlWin(m_pHtmlPan, this, CTRL_PRJINFO_ID_HTML_INFO, wxPoint(0, 35), wxSize(szHP.GetWidth(), szHP.GetHeight() - 60));
    bs->Add(m_pHtmlInfo, 1, wxEXPAND);
    //    m_pListPan->SetMinSize(wxSize(600,100));
    // m_pHtmlInfo->SetEditable(false);
    //**************************************************
    SetAutoLayout(true);
}
void SimuPrjInfoView::InitView()
{
    UpdateListTable();
    //..................................//
    InitColumn();
    UpdateList();
    m_lcTmpItem = m_lcItem;
    //..................................//
    UpdateInfoHtml();
}
void SimuPrjInfoView::InitColumn()
{
    wxImageList *imageList = new wxImageList(16, 16);
    wxBitmap bmp;

    bmp = wxBitmap(EnvUtil::GetPrgResDir() + wxTRUNK_PRJ_IMG_PATH + wxT("sortup.png"), wxBITMAP_TYPE_PNG);

    imageList->Add(bmp);

    bmp = wxBitmap(EnvUtil::GetPrgResDir() + wxTRUNK_PRJ_IMG_PATH + wxT("sortdown.png"), wxBITMAP_TYPE_PNG);

    imageList->Add(bmp);
    m_pList->AssignImageList(imageList, wxIMAGE_LIST_SMALL);
    m_colName.Clear();
    wxString  title = wxT("Name"); //+wxString::Format(wxT("%c"),30);
    m_colName.Add(title);
    m_colName.Add(wxT("Type"));
    m_colName.Add(wxT("Create Date"));
    m_colName.Add(wxT("Last Modify"));
    //   m_colName.Add(wxT("Description"));

    int count = m_colName.GetCount();
    m_colNameId = new int[count];
    int width = m_pList->GetClientSize().GetWidth() / count;

    width = width > 100 ? width : 100; //the minimum size for each column is 100

    wxListItem item;
    //   item.SetId(1);
    item.SetMask(wxLIST_MASK_STATE | wxLIST_MASK_TEXT | wxLIST_MASK_DATA | wxLIST_MASK_IMAGE);
    //  item.SetStateMask(wxLIST_STATE_SELECTED);
    //   item.SetState(wxLIST_STATE_SELECTED);
    item.SetImage(-1);
    item.SetAlign(wxLIST_FORMAT_LEFT);

    for(int i = 0; i < count; i++) {
        item.SetText(m_colName[i]);
        m_pList->InsertColumn(i, item);
        m_pList->SetColumnWidth(i, width );
        m_colNameId[i] = i;
    }
}
void SimuPrjInfoView::AddToLCFile(wxString fileName, wxString &file)
{
    if(file.IsEmpty())
        return;
    wxString fullPathName = GetInfoFileFullName(fileName);
    if(!wxFileExists(fullPathName))
        return;
    XmlFile xmlLF(wxLIST_INFO_FILE_ROOT, fullPathName);
    wxArrayString   files = xmlLF.GetArrayString(LIST_INFOF_AR_ELEM);
    int count = files.GetCount();
    wxString    tmpStr = wxEmptyString;
    for(int i = 0; i < count; i++) {
        tmpStr = files[i];
        if(tmpStr.IsSameAs(file))
        {
            wxMessageBox(wxT("The item exists."), wxT("Warning"));
            return;
        }
    }
    files.Add(file);
    xmlLF.BuildElement(LIST_INFOF_AR_ELEM, files);
    xmlLF.Save();
}
bool SimuPrjInfoView::AddListColumn(wxString title)
{
    return true;
}
bool SimuPrjInfoView::Close()
{
    if(m_pList != NULL) {
        m_pList->ClearAll();
        delete m_pList;
        m_pList = NULL;
    }
    if(m_pSearchText != NULL) {
        delete m_pSearchText;
        m_pSearchText = NULL;
    }
    if(m_pHtmlInfo != NULL) {
        delete m_pHtmlInfo;
        m_pHtmlInfo = NULL;
    }
    return true;
}
void SimuPrjInfoView::DelFromLCFile(wxString fileName, wxString &file)
{
    if(file.IsEmpty())
        return;
    wxString fullPathName = GetInfoFileFullName(fileName);
    if(!wxFileExists(fullPathName))
        return;
    XmlFile xmlLF(wxLIST_INFO_FILE_ROOT, fullPathName);
    wxArrayString   files = xmlLF.GetArrayString(LIST_INFOF_AR_ELEM);
    int count = files.GetCount();
    wxString    tmpStr = wxEmptyString;
    for(int i = 0; i < count; i++) {
        tmpStr = files[i];
        if(tmpStr.IsSameAs(file))
        {
            files.RemoveAt(i);
            break;
        }
    }
    xmlLF.BuildElement(LIST_INFOF_AR_ELEM, files);
    xmlLF.Save();
}
bool SimuPrjInfoView::DelListColumn(wxString title)
{
    return true;
}
bool SimuPrjInfoView::DoOpenFile(wxString pathinfo)
{
    if(pathinfo.StartsWith(wxT("SIMU_CMD_OPEN_FILE_")))
    {
        pathinfo = pathinfo.Mid(wxString(wxT("SIMU_CMD_OPEN_FILE_")).Len());
        ((SimuPrjFrm *)Manager::Get()->GetAppFrame())->DoFileOpen(pathinfo);
    } else  if(pathinfo.StartsWith(wxT("SIMU_CMD_OPEN_DIR_")))
    {
        wxString   pgInfo = m_lcItem[m_lcIndex];
        pgInfo = GetColText(pgInfo, FULLPATH);
        //open the middle level
    }
    return true;
}
bool SimuPrjInfoView::LinkClicked(const wxHtmlLinkInfo &link)
{
    wxString href = link.GetHref();
    if (href.StartsWith(_T("SIMU_CMD_"))) {
        DoOpenFile(link.GetHref());
        return true;
    }
    return false;
}
bool SimuPrjInfoView::UpdateListTable()
{   //the list table info is strored in ./config/ListTable.xml
    m_pListTable->ClearAll();
    wxListItem item;
    item.SetMask(wxLIST_MASK_STATE | wxLIST_MASK_TEXT | wxLIST_MASK_DATA);
    item.SetImage(-1);
    item.SetAlign(wxLIST_FORMAT_LEFT);
    item.SetText(wxT("List File Name"));
    m_pListTable->InsertColumn(0, item);
    m_pListTable->SetColumnWidth(0, m_pListTable->GetClientSize().GetWidth());

    wxString ltItem = wxT("Recent Workspace");
    m_pListTable->InsertItem(0, ltItem);
    m_pListTable->SetItemData(0, 0);

    wxArrayString infoFiles = FileUtil::GetFileBySuffix(EnvUtil::GetPrgRootDir() + wxLIST_INFO_FILE_PATH, wxLIST_INFO_FILE_SUFFIX);

    int count = infoFiles.Count();
    infoFiles.Sort();
    for(int i = 0; i < count; i++) {
        ltItem = infoFiles.Item(i).BeforeLast(wxT('.'));
        m_pListTable->InsertItem(i + 1, ltItem);
        m_pListTable->SetItemData(i + 1, i + 1);
    }
    return true;
}
bool SimuPrjInfoView::UpdateList()
{
    switch(m_ltIndex) {
    case 0:
        UpdateListFromPath();
        break;
    default:
        if(m_ltIndex >= (unsigned int)m_pListTable->GetItemCount())
            return false;
        wxString fileFullPath = GetInfoFileFullName(m_pListTable->GetItemText(m_ltIndex));
        UpdateListFromFile(fileFullPath);
        break;
    }
    return true;
}
void SimuPrjInfoView::UpdateListFromPath()
{   //update the m_lcItem based on path;
    wxString path = EnvSettingsData::Get()->GetDefWorkPath();

    if(!wxDirExists(path))
        return ;

    wxString tmpNew, filename, info;
    wxArrayString  fileList;
    m_lcItem.Clear();

    wxDir   theDir(path);
    int flag = wxDIR_DIRS, num = 0;
    bool cont = theDir.GetFirst(&filename, wxEmptyString, flag);
    while ( cont ) { //put all sip and spg info into the infoList;
        if(filename.IsSameAs(wxT(".")) || filename.IsSameAs(wxT("..")))
            continue;
        fileList.Clear();
        tmpNew = path + platform::PathSep() + filename;
        fileList = FileUtil::GetFileBySuffix(tmpNew, wxPRJ_FILE_SUFFIX); //check sip file
        num = fileList.GetCount();
        int i = 0;
        for(i = 0; i < num; i++) {
            filename = tmpNew + platform::PathSep() + fileList[i];
            info = GetProjectInfo(filename);
            info = info + filename;
            m_lcItem.Add(info);
        }
        fileList.Clear();
        fileList = FileUtil::GetFileBySuffix(tmpNew, wxPG_FILE_SUFFIX); //check spg file
        num = fileList.GetCount();
        for(i = 0; i < num; i++) {
            filename = tmpNew + platform::PathSep() + fileList[i];
            info = GetPrjGrpInfo(filename);
            info = info + filename;
            m_lcItem.Add(info);
        }
        cont = theDir.GetNext(&filename);
    }
    m_pList->DeleteAllItems();
    //    SortItem(0);
    AddItems();
}
void SimuPrjInfoView::UpdateListFromFile(wxString fileFullPath)
{
    m_pList->DeleteAllItems();
    if(!wxFileExists(fileFullPath))
        return;
    XmlFile xmlLF(wxLIST_INFO_FILE_ROOT, fileFullPath);
    m_lcItem.Clear();
    wxArrayString fileList = xmlLF.GetArrayString(LIST_INFOF_AR_ELEM); //abs path in xmlLF
    int num = fileList.GetCount();
    wxString info = wxEmptyString;
    for(int i = 0; i < num; i++) {
        if(!wxFileExists(fileList[i])) {
            fileList.RemoveAt(i);
            num = fileList.GetCount();
            i--;
            continue;
        }
        if(fileList[i].Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX))
            info = GetProjectInfo(fileList[i]);
        else if(fileList[i].Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX))
            info = GetPrjGrpInfo(fileList[i]);
        if(info.IsEmpty())
            continue;
        info = info + fileList[i];
        m_lcItem.Add(info);
    }
    //    SortItem(0);
    AddItems();
}
bool SimuPrjInfoView::UpdateInfoHtml()
{
    if(m_lcIndex == 0)
        return UpdateHtmlLtInfo();
    else {
        m_lcIndex = m_lcIndex - 1;
        return UpdateHtmlLcInfo();
    }
}
bool SimuPrjInfoView::UpdateHtmlLtInfo()
{
    if(m_htmlPath.IsEmpty())
        return false;
    if(m_pHtmlInfo == NULL)
        return false;

    wxString fileFullPath = GetInfoFileFullName(m_pListTable->GetItemText(m_ltIndex));
    if(fileFullPath.IsEmpty())
        return false;

    m_pHtmlInfo->LoadPage(m_htmlPath);
    wxString buf;
    wxFileSystem *fs = new wxFileSystem;
    wxFSFile *fsFile = fs->OpenFile(m_htmlPath);
    if (fsFile) {
        wxInputStream *is = fsFile->GetStream();
        char tmp[1024] = {0};
        while (!is->Eof() && is->CanRead()) {
            memset(tmp, 0, sizeof(tmp));
            is->Read(tmp, sizeof(tmp) - 1);
            buf << StringUtil::CharPointer2String((const char *)tmp);
        }
        delete fsFile;
    }
    else
        buf = wxT("<HTML><BODY><H1>Sorry,The default start page seems to be missing. </H1></BODY></HTML>");
    delete fs;
    //write the name and description to the html view;
    wxString links;
    links = m_pListTable->GetItemText(m_ltIndex);
    buf.Replace(_T("SIMU_VAR_PRJ_NAME"), links);
    if(m_ltIndex == 0) {
        links = wxT("Lists of the project groups and projects in the particular directory.");
        buf.Replace(_T("SIMU_VAR_PRJ_DESCRIPTION"), links);
        links = EnvUtil::GetDefWorkPath();
        buf.Replace(_T("SIMU_VAR_PRJ_DIRECTORY"), links);
    } else {
        links = wxT("Lists of the project groups and projects added by users.");
        buf.Replace(_T("SIMU_VAR_PRJ_DESCRIPTION"), links);
        links = fileFullPath;
        buf.Replace(_T("SIMU_VAR_PRJ_DIRECTORY"), links);
    }
    links = wxT("All of the items are listed above");
    buf.Replace(_T("SIMU_VAR_PROJECT_INFO_LIST"), links);
    m_pHtmlInfo->SetPage(buf);
    return true;
}
bool SimuPrjInfoView::UpdateHtmlLcInfo()
{
    if(m_htmlPath.IsEmpty())
        return false;
    if(m_pHtmlInfo == NULL)
        return false;
    if(m_lcIndex >= m_lcItem.GetCount())
        return false;
    wxString xmlFilePath = GetColText(m_lcItem[m_lcIndex], FULLPATH);
    if(xmlFilePath.IsEmpty())
        return false;
    XmlFile xmlProject = XmlFile(SimuProject::GetXmlFileRoot(), xmlFilePath);

    //get the content of the html file;
    m_pHtmlInfo->LoadPage(m_htmlPath);
    wxString buf;
    wxFileSystem *fs = new wxFileSystem;
    wxFSFile *fsFile = fs->OpenFile(m_htmlPath);
    if (fsFile) {
        wxInputStream *is = fsFile->GetStream();
        char tmp[1024] = {0};
        while (!is->Eof() && is->CanRead()) {
            memset(tmp, 0, sizeof(tmp));
            is->Read(tmp, sizeof(tmp) - 1);
            buf << StringUtil::CharPointer2String((const char *)tmp);
        }
        delete fsFile;
    }
    else
        buf = wxT("<HTML><BODY><H1>Sorry,The default start page seems to be missing. </H1></BODY></HTML>");
    delete fs;
    //write the name and description to the html view;
    wxString links;
    links << wxString::Format(wxT("<A HREF=\"SIMU_CMD_OPEN_FILE_"));
    links << xmlFilePath;
    links << wxT("\">");
    links << xmlFilePath.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
    links << wxT("</A>");
    buf.Replace(_T("SIMU_VAR_PRJ_NAME"), links);
    links = GetColText(m_lcItem[m_lcIndex], DESCRIP);
    buf.Replace(_T("SIMU_VAR_PRJ_DESCRIPTION"), links);
    links = GetColText(m_lcItem[m_lcIndex], FULLPATH);
    buf.Replace(_T("SIMU_VAR_PRJ_DIRECTORY"), links);

    wxString    pgDir = xmlFilePath.BeforeLast(platform::PathSep().GetChar(0));
    if(xmlFilePath.Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX))
    {
        links = wxEmptyString;
        wxArrayString chdProjects = xmlProject.GetArrayString("Projects");
        int     cCount = chdProjects.GetCount();
        links << wxT("<B>Child Items:</B><BR>\n");
        wxString logPath = wxEmptyString, absPath = wxEmptyString, logVal = wxEmptyString, tmpdir = wxEmptyString;;
        if (cCount > 0) {
            links << wxT("<UL>");
            for (int i = 0; i < cCount; i = i + 2) {
                logPath = chdProjects[i];
                absPath = chdProjects[i + 1];

                if(absPath.GetChar(0) == wxT('.')) //get the absolute project path: *.sip
                    absPath = pgDir + absPath.AfterFirst(wxT('.'));

                tmpdir = wxEmptyString;
                logPath = logPath.AfterFirst(platform::PathSep().GetChar(0));
                logVal = logPath.BeforeFirst(platform::PathSep().GetChar(0));
                links << wxString::Format(wxT("<LI><A HREF=\"SIMU_CMD_OPEN_FILE_"));//the first is the .spg file;
                links << xmlFilePath;
                links << wxT("\">");
                links << logVal;
                links << wxT("</A>&nbsp;>>&nbsp;");

                tmpdir = tmpdir + platform::PathSep() + logVal;
                logPath = logPath.AfterFirst(platform::PathSep().GetChar(0));
                logVal = logPath.BeforeFirst(platform::PathSep().GetChar(0));

                while(!logPath.AfterFirst(platform::PathSep().GetChar(0)).IsEmpty()) {
                    tmpdir = tmpdir + platform::PathSep() + logVal;
                    //          links << wxString::Format(wxT("<A HREF=\"SIMU_CMD_OPEN_DIR_"));
                    //          links << tmpdir;
                    //          links << wxT("\">");
                    links << logVal;
                    //          links << wxT("</A>&nbsp;>>&nbsp;");
                    links << wxT("&nbsp;>>&nbsp;");
                    logPath = logPath.AfterFirst(platform::PathSep().GetChar(0));
                    logVal = logPath.BeforeFirst(platform::PathSep().GetChar(0));
                }
                //reach the project part;
                links << wxString::Format(wxT("<A HREF=\"SIMU_CMD_OPEN_FILE_"));
                links << absPath;
                links << wxT("\">");
                links << logPath;
                links << wxT("</A></LI>");
            }
            links << wxT("</UL><BR>");
        }
    } else
        links = wxT("&nbsp;&nbsp;&nbsp;&nbsp;No child items<br>\n");
    // update page
    buf.Replace(_T("SIMU_VAR_PROJECT_INFO_LIST"), links);

    m_pHtmlInfo->SetPage(buf);
    return true;
}
bool SimuPrjInfoView::UpdateView()
{
    UpdateListTable();
    UpdateList();
    UpdateInfoHtml();
    return true;
}
void SimuPrjInfoView::SaveLCFile(wxString fileName, wxArrayString &filelist)
{
    if(!wxDirExists(EnvUtil::GetPrgRootDir() + wxLIST_INFO_FILE_PATH))
        FileUtil::CreateDirectory(EnvUtil::GetPrgRootDir() + wxLIST_INFO_FILE_PATH);

    wxString fullPathName = GetInfoFileFullName(fileName);
    XmlFile xmlLF(wxLIST_INFO_FILE_ROOT, fullPathName);
    xmlLF.BuildElement(LIST_INFOF_AR_ELEM, filelist);
    xmlLF.Save();
}
//*******************************************************************
//                   event handle functions                                     *
//*******************************************************************
void SimuPrjInfoView::OnActiveItem(wxListEvent &event)//choose a item;
{
    if(event.GetId() == CTRL_PROJECT_INFO_LIST)
    {
        int index = event.GetData();
        wxString tmp = m_lcItem[index - 1];
        tmp = GetColText(tmp, FULLPATH);

        ((SimuPrjFrm *)Manager::Get()->GetAppFrame())->DoFileOpen(tmp);
    }
}
void SimuPrjInfoView::OnAddToList(wxCommandEvent &event)
{
    if(m_ltIndex == 0)
        return;
    wxString    dir = EnvUtil::GetProjectDir(), filter = wxEmptyString;
    filter = filter + wxT("Project Group (*") + wxPG_FILE_SUFFIX + wxT(")|*") + wxPG_FILE_SUFFIX + wxT("|Simu-Project (*") + wxPRJ_FILE_SUFFIX + wxT(")|*") + wxPRJ_FILE_SUFFIX;
    wxFileDialog    fdlg(this, wxT("Chose a file"), dir, wxEmptyString, filter);
    if(fdlg.ShowModal() != wxID_OK)
        return;
    wxString    filePath = fdlg.GetPath();
    wxString    listName = m_pListTable->GetItemText(m_ltIndex);
    AddToLCFile(listName, filePath);
    UpdateList();
}
void SimuPrjInfoView::OnBeginEdLtItem(wxListEvent &event)
{
    if(event.GetData() == 0)
    {
        event.Veto();
    } else {
        m_strTmp = event.GetLabel();
        event.Skip();
    }
}
void SimuPrjInfoView::OnBtnSearch(wxCommandEvent &event) //select a dir to display by button;
{
    wxString tmp = wxEmptyString;
    if(m_pSearchText == NULL)
        return;
    tmp = m_pSearchText->GetValue();
    if(tmp.IsEmpty())
        return;
    m_pList->DeleteAllItems();
    //   m_lcItem.Clear(); will be used in FindInCurList();
    switch(m_schType) {
    case 1://current list
        m_lcItem = FindInCurList(tmp);
        break;
    case 2://all lists
        m_lcItem = FindInAllList(tmp);
        break;
    case 3://selected path
        wxString dir = m_pCbSearch->GetValue();
        m_lcItem = FindInSelPath(dir, tmp);
        break;
    }
    AddItems();
}
void SimuPrjInfoView::OnCbSchType(wxCommandEvent &event)
{
    wxString    chtype = m_pCbSearch->GetValue();
    if(chtype.IsSameAs(wxCOMBO_SCH_CURLIST)) {
        m_schType = 1;
    } else if(chtype.IsSameAs(wxCOMBO_SCH_ALLLIST)) {
        m_schType = 2;
    } else if(chtype.IsSameAs(wxCOMBO_SCH_SELPATH)) {
        m_schType = 3;
        wxString    dir = EnvUtil::GetProjectDir();
        wxDirDialog    dirDlg(this, wxT("Chose a Directory"), dir);
        if(dirDlg.ShowModal() != wxID_OK)
            return;
        dir = dirDlg.GetPath();
        m_pCbSearch->SetValue(dir);
    }
}
void SimuPrjInfoView::OnClkLcColum(wxListEvent &event)
{
    if(event.GetId() == CTRL_PROJECT_INFO_LIST)
    {
        int index = event.GetColumn();

        m_pList->DeleteAllItems();
        SortItem(index);
        AddItems();
        if(m_isAccend[index] == true)
            m_isAccend[index] = false;
        else
            m_isAccend[index] = true;
    } else if(event.GetId() == CTRL_PROJECT_LIST_TABLE) {

    }
}
void SimuPrjInfoView::OnDelFromList(wxCommandEvent &event)
{
    wxString    filePath = GetColText(m_lcItem[m_lcIndex], FULLPATH);
    wxString    listName = m_pListTable->GetItemText(m_ltIndex);
    DelFromLCFile(listName, filePath);
    UpdateList();
}
void SimuPrjInfoView::OnDelLtList(wxCommandEvent &event)
{
    if(m_ltIndex == 0)
        return;
    wxMessageDialog     mdlg(this, wxT("Do you realy want to delete the list from you computer?"), wxT("Warning"), wxYES_NO);
    if(mdlg.ShowModal() != wxID_YES)
        return;
    wxString title = m_pListTable->GetItemText(m_ltIndex);
    m_pListTable->DeleteItem(m_ltIndex);
    m_ltIndex = 0;
    wxString fullFilePath = GetInfoFileFullName(title);
    if(wxFileExists(fullFilePath))
        wxRemoveFile(fullFilePath);
    UpdateListTable();
}
void SimuPrjInfoView::OnDbClkSash(wxSplitterEvent &event)
{
}
void SimuPrjInfoView::OnRenameList(wxCommandEvent &event)
{
    m_pListTable->EditLabel(m_ltIndex);
}
void SimuPrjInfoView::OnEndEdLtItem(wxListEvent &event)
{
    int id = event.GetData();
    wxString    fname = event.GetLabel();
    if(fname.IsEmpty())
        return;
    wxString    oldname = m_strTmp;
    wxArrayString   tmp;

    int count = m_pListTable->GetItemCount();
    wxString    strItem = wxEmptyString;
    for(int i = 0; i < count; i++) {
        if(i == id)
            continue;
        strItem = m_pListTable->GetItemText(i);
        if(strItem.IsSameAs(fname))
        {
            wxMessageBox(wxT("The list with this name exists, please use another name"), wxT("Warning"));
            m_pListTable->SetItemText(id, oldname);
            m_pListTable->EditLabel(id);
            return;
        }
    }
    if(wxFileExists(GetInfoFileFullName(oldname)))
    {
        oldname = GetInfoFileFullName(oldname);
        fname = GetInfoFileFullName(fname);
        wxRenameFile(oldname, fname);
    } else {
        SaveLCFile(fname, tmp);
    }
    // UpdateListTable();
}
void SimuPrjInfoView::OnExpToList(wxCommandEvent &event)
{   //create a new list,record the project info and update the list table;
    wxArrayString        listFiles = FileUtil::GetFileBySuffix(EnvUtil::GetPrgRootDir() + wxLIST_INFO_FILE_PATH, wxLIST_INFO_FILE_SUFFIX);
    //        int i=0;
    if(m_ltIndex > 0)
        listFiles.RemoveAt(m_ltIndex - 1);
    for(unsigned int i = 0; i < listFiles.GetCount(); i++) {
        listFiles[i] = listFiles[i].BeforeLast(wxT('.'));
    }
    DlgExpList        dlg(this, listFiles);

    if(dlg.ShowModal() != wxID_OK)
        return;
    wxString        fileName = dlg.GetExpFile();

    for(unsigned int i = 0; i < listFiles.GetCount(); i++) {
        if(fileName.IsSameAs(listFiles[i]))
        {   //the list exist;
            wxString fileFullPath = EnvUtil::GetPrgRootDir() + wxLIST_INFO_FILE_PATH + platform::PathSep() + fileName + wxLIST_INFO_FILE_SUFFIX;
            if(!wxFileExists(fileFullPath))
                return;
            XmlFile xmlLF(wxLIST_INFO_FILE_ROOT, fileFullPath);
            wxArrayString fileList = xmlLF.GetArrayString(LIST_INFOF_AR_ELEM); //abs path in xmlLF
            wxString        newItem = GetColText(m_lcItem[m_lcIndex], FULLPATH);
            for(unsigned int j = 0; j < fileList.GetCount(); j++)
                if(newItem.IsSameAs(fileList[j]))
                    return;
            fileList.Add(newItem);
            SaveLCFile(fileName, fileList);

            return;
        }
    }
    //the list do not exist;
    if(m_isShowLt == false)
        return;
    int count = m_pListTable->GetItemCount();
    wxString ltItem = wxT("New Export List");
    m_pListTable->InsertItem(count, ltItem);
    m_pListTable->SetItemData(count, count);
    wxArrayString items;
    items.Clear();
    for(unsigned int i = 0; i < m_lcItem.GetCount(); i++)
        items.Add(GetColText(m_lcItem[i], FULLPATH));
    SaveLCFile(ltItem, items);
    m_pListTable->EditLabel(count);

}
void SimuPrjInfoView::OnWndLtHtSizeChange(wxSplitterEvent &event)
{   //resize the child windows to suit the new splitter window
    int heightL = event.GetSashPosition() - 50, //the event.GetSashPosition() return the y cord base on the panel.
        width = m_pSpWnd->GetClientSize().GetWidth(),
        heightHtml = m_pSpWnd->GetClientSize().GetHeight() - event.GetSashPosition() - 67;

    int widthLt = 0;

    if(m_isShowLt == true) {
        if(m_pListTable != NULL) {
            m_pListTable->ClearAll();
            m_pListTable->Destroy();
            widthLt = m_pListTablePn->GetSize().GetWidth();
        }
        m_pSpWndLT->SetSize(width, heightL);
        m_pListTablePn->SetSize(wxSize(widthLt, heightL));
        m_pListPn->SetSize(wxSize(width - widthLt, heightL));

        int style = wxLC_REPORT | wxLC_SINGLE_SEL | wxBORDER_SUNKEN;
        m_pListTable = new wxListCtrl(m_pListTablePn, CTRL_PROJECT_LIST_TABLE, wxDefaultPosition, wxSize(widthLt + 2, heightL), style | wxLC_EDIT_LABELS);
        //                m_pListTable->SetSize(wxSize(widthLt+2,heightL));
        //                m_pListTable->Refresh();
        UpdateListTable();


        if(m_pList != NULL) {
            m_pList->ClearAll();
            m_pList->Destroy();
        }

        m_pList = new wxListCtrl(m_pListPn, CTRL_PROJECT_INFO_LIST, wxDefaultPosition, wxSize(width - widthLt - 12, heightL), style);
        //                m_pList->SetSize(wxSize(width-widthLt-12,heightL));
        //                m_pList->Refresh();
    } else {
        int style = wxLC_REPORT | wxLC_SINGLE_SEL | wxBORDER_SUNKEN;
        if(m_pList != NULL) {
            m_pList->ClearAll();
            m_pList->Destroy();
        }
        m_pSpWndLT->SetSize(width, heightL);
        widthLt = m_pListTablePn->GetClientSize().GetWidth();
        m_pListTablePn->SetSize(wxSize(widthLt, heightL));
        m_pListPn->SetSize(wxSize(width - widthLt, heightL));

        m_pList = new wxListCtrl(m_pSpWndLT, CTRL_PROJECT_INFO_LIST, wxDefaultPosition, wxSize(width, heightL), style);
        //                m_pList->SetSize(wxSize(width,heightL));
        //                m_pList->Refresh();
    }

    InitColumn();
    UpdateList();

    // if(m_pHtmlInfo != NULL)
    //     m_pHtmlInfo->Destroy();
    // m_pHtmlInfo = new PInfoHtmlWin(m_pHtmlPan, this, CTRL_PRJINFO_ID_HTML_INFO, wxPoint(0, 35), wxSize(width, heightHtml));
    m_pHtmlInfo->SetSize(wxSize(width, heightHtml));
    m_pHtmlInfo->Refresh();
    UpdateInfoHtml();
}
void SimuPrjInfoView::OnWndLtLcSizeChange(wxSplitterEvent &event)
{   //resize the child windows to suit the new splitter window
    int widthLt = event.GetSashPosition(), //the event.GetSashPosition() return the y cord base on the panel.
        widthLc = m_pSpWndLT->GetClientSize().GetWidth() - event.GetSashPosition() - 5,
        height = m_pSpWndLT->GetClientSize().GetHeight();

    if(m_pListTable != NULL) {
        m_pListTable->ClearAll();
        m_pListTable->Destroy();
    }
    int style = wxLC_REPORT | wxLC_SINGLE_SEL | wxBORDER_SUNKEN;
    m_pListTable = new wxListCtrl(m_pListTablePn, CTRL_PROJECT_LIST_TABLE, wxDefaultPosition, wxSize(widthLt, height), style | wxLC_EDIT_LABELS);
    if(m_pList != NULL) {
        m_pList->ClearAll();
        m_pList->Destroy();
        //     delete m_pList;
    }
    UpdateListTable();
    m_pList = new wxListCtrl(m_pListPn, CTRL_PROJECT_INFO_LIST, wxDefaultPosition, wxSize(widthLc - 3, height), style);
    InitColumn();
    UpdateList();
}
void SimuPrjInfoView::OnNewList(wxCommandEvent &event)
{
    if(m_isShowLt == false)
        return;
    int count = m_pListTable->GetItemCount();
    wxString ltItem = wxT("New List");
    m_pListTable->InsertItem(count, ltItem);
    m_pListTable->SetItemData(count, count);
    m_pListTable->EditLabel(count);
}
void SimuPrjInfoView::OnRClickColumn(wxListEvent &event)
{
    if(event.GetId() == CTRL_PROJECT_INFO_LIST)
    {}
    //wxMessageBox(wxT("On Right Clicked"));
}
void SimuPrjInfoView::OnRClkItem(wxListEvent &event)
{
    wxPoint pos = event.GetPoint();

    if(event.GetId() == CTRL_PROJECT_INFO_LIST)
    {
        m_lcIndex = event.GetData() - 1; //hurukun 0418
        wxMenu menu;
        if(m_ltIndex != 0) {
            menu.Append(POPUPMENU_ID_LC_ADD, wxT("Add To List ..."), wxT("Add item to the list."));
            menu.Append(POPUPMENU_ID_LC_DELETE, wxT("Remove "), wxT("Delete item from the list."));
            //          menu.AppendSeparator();
        }
        menu.Append(POPUPMENU_ID_LC_EXPORT, wxT("Export To List"), wxT("To form a new list"));
        if(m_isShowLt == false)
            menu.Enable(POPUPMENU_ID_LC_EXPORT, false);
        //    menu.Append(POPUPMENU_ID_LC_OPEN, wxT("Open"),wxT("open item in the list."));
        if(m_lcItem.GetCount() == 0) {
            menu.Enable(POPUPMENU_ID_LC_EXPORT, false);
            menu.Enable(POPUPMENU_ID_LC_DELETE, false);
            //         menu.Enable(POPUPMENU_ID_LC_OPEN,false);
        }

        if (menu.GetMenuItemCount() != 0) {
            pos.x = pos.x + m_pListTablePn->GetClientSize().GetWidth() + 15;
            pos.y = pos.y + 50 + 20;
            this->PopupMenu(&menu, pos);
        }
    } else if(event.GetId() == CTRL_PROJECT_LIST_TABLE)
    {
        m_ltIndex = event.GetData(); //hurukun 0418
        wxMenu menu;
        if(m_ltIndex == 0)
        {
            //         menu.Append(POPUPMENU_ID_LT_SETPATH, wxT("Set Path"),wxT("Set path for display."));
        } else {
            menu.Append(POPUPMENU_ID_LT_RENAME, wxT("Rename"), wxT("Rename List."));
            menu.Append(POPUPMENU_ID_LT_ADD, wxT("New List "), wxT("New List"));
            menu.Append(POPUPMENU_ID_LT_DELETE, wxT("Delete "), wxT("Delete list."));
        }

        if(m_pListTable->GetItemCount() <= 1) {
            menu.Enable(POPUPMENU_ID_LC_DELETE, false);
        }

        if (menu.GetMenuItemCount() != 0) {
            pos.x = pos.x + 5;
            pos.y = pos.y + 50 + 10;
            this->PopupMenu(&menu, pos);
        }
    }
}
void        SimuPrjInfoView::OnResize(wxSizeEvent &event)
{
    m_clientSize = GetParent()->GetClientSize();
    wxSize   listSize(m_clientSize.GetWidth(), int(m_clientSize.GetHeight() * 0.6));
    m_pSpWnd->SetClientSize(m_clientSize);
    m_pSpWnd->Refresh();
    int heightL = listSize.GetHeight(), //the event.GetSashPosition() return the y cord base on the panel.
        width = m_pSpWnd->GetClientSize().GetWidth();
    //heightHtml=m_pSpWnd->GetClientSize().GetHeight()-listSize.GetHeight()-5;

    int widthLt = 0;

    if(m_isShowLt == true) {
        if(m_pListTable != NULL) {
            //           m_pListTable->ClearAll();
            //           m_pListTable->Destroy();
            //                        m_pListTable=NULL;
            widthLt = width > 200 ? 200 : (int)(width * 0.5);
        }
        m_pSpWndLT->SetSize(width, heightL);
        m_pSpWndLT->Refresh();
        m_pListTablePn->SetSize(wxSize(widthLt, heightL));
        m_pListTablePn->Refresh();
        m_pListPn->SetSize(wxSize(width - widthLt, heightL));
        m_pListPn->Refresh();

        //int style=wxLC_REPORT|wxLC_SINGLE_SEL|wxBORDER_SUNKEN;
        //        m_pListTable=new wxListCtrl(m_pListTablePn,CTRL_PROJECT_LIST_TABLE,wxDefaultPosition,wxSize(widthLt+2,heightL),style|wxLC_EDIT_LABELS);


        //        if(m_pList!=NULL){
        //            m_pList->ClearAll();
        //            m_pList->Destroy();
        //                        m_pList=NULL;
        //        }

        //        m_pList=new wxListCtrl(m_pListPn,CTRL_PROJECT_INFO_LIST,wxDefaultPosition,wxSize(width-widthLt-12,heightL),style);

    } else {
        //int style=wxLC_REPORT|wxLC_SINGLE_SEL|wxBORDER_SUNKEN;
        //       if(m_pList!=NULL){
        //            m_pList->ClearAll();
        //            m_pList->Destroy();
        //                        m_pList=NULL;
        //       }
        m_pSpWndLT->SetSize(width, heightL);
        m_pSpWndLT->Refresh();
        widthLt = m_pSpWndLT->GetClientSize().GetWidth();
        m_pListTablePn->SetSize(wxSize(widthLt, heightL));
        m_pListTablePn->Refresh();
        m_pListPn->SetSize(wxSize(width - widthLt, heightL));
        m_pListPn->Refresh();

        //        m_pList=new wxListCtrl(m_pSpWndLT,CTRL_PROJECT_INFO_LIST,wxDefaultPosition,wxSize(width,heightL),style);
    }



    //    if(m_pHtmlInfo!=NULL){
    //        m_pHtmlInfo->Destroy();
    //                m_pHtmlInfo=NULL;
    //        }
    //    m_pHtmlInfo= new PInfoHtmlWin(m_pHtmlPan,this, CTRL_PRJINFO_ID_HTML_INFO, wxPoint(0,35), wxSize(width, heightHtml));


    if(m_pHtmlInfo)
    {
        wxSplitterEvent splitEvent(wxEVT_COMMAND_SPLITTER_SASH_POS_CHANGING);
        m_pSpWnd->SetSashPosition(listSize.GetHeight());
        splitEvent.SetSashPosition(m_pSpWnd->GetSashPosition());
        OnWndLtHtSizeChange(splitEvent);
    }
}
void SimuPrjInfoView::OnShowLT(wxCommandEvent &event)
{
    wxSize  szWnd = m_pSpWndLT->GetClientSize();
    int         height = szWnd.GetHeight(),
                width = szWnd.GetWidth();
    if(m_isShowLt == true)
    {
        int style = wxLC_REPORT | wxLC_SINGLE_SEL | wxBORDER_SUNKEN;
        if(m_pListTablePn != NULL) {
            //            m_pListTable->ClearAll();
            //            m_pListTable->Destroy();m_pListTable=NULL;
            //        m_pListTable->Show(false);
            m_pListTablePn->Show(false);
        }
        if(m_pListPn != NULL) {
            m_pListPn->Show(false);
        }

        if(m_pList != NULL) {
            m_pList->ClearAll();
            m_pList->Destroy();
            //     delete m_pList;
        }
        //       m_pList=new wxListCtrl(m_pSpWndLT,CTRL_PROJECT_INFO_LIST,wxPoint(0,0),szWnd,style);
        m_pList = new wxListCtrl(m_pSpWndLT, CTRL_PROJECT_INFO_LIST, wxPoint(0, 0), szWnd, style);
        //        m_pList->Show(true);
        InitColumn();
        UpdateList();
        m_isShowLt = false;
        m_pShowLt->SetLabel(wxT("Show List"));
    } else {
        if(m_pListTablePn != NULL) {
            m_pListTablePn->Show(true);
        }
        if(m_pListPn != NULL) {
            m_pListPn->Show(true);
        }
        int widthLt = 0;
        if(m_pListTable != NULL) {
            m_pListTable->ClearAll();
            m_pListTable->Destroy();
            widthLt = m_pListTablePn->GetClientSize().GetWidth();
        }

        int style = wxLC_REPORT | wxLC_SINGLE_SEL | wxBORDER_SUNKEN;
        m_pListTable = new wxListCtrl(m_pListTablePn, CTRL_PROJECT_LIST_TABLE, wxDefaultPosition, wxSize(widthLt, height), style | wxLC_EDIT_LABELS);
        UpdateListTable();


        //      int style=wxLC_REPORT|wxLC_SINGLE_SEL|wxBORDER_SUNKEN;
        if(m_pList != NULL) {
            m_pList->ClearAll();
            m_pList->Destroy();
            //     delete m_pList;
        }
        UpdateListTable();
        m_pList = new wxListCtrl(m_pListPn, CTRL_PROJECT_INFO_LIST, wxPoint(0, 0), wxSize(width - widthLt - 15, height), style);
        InitColumn();
        UpdateList();
        m_isShowLt = true;
        m_pShowLt->SetLabel(wxT("Hide List"));
    }
}
void SimuPrjInfoView::OnSelectItem(wxListEvent &event)//choose a item;
{
    int index = event.GetData();
    if(event.GetId() == CTRL_PROJECT_INFO_LIST)
    {
        m_lcIndex = index;
        UpdateInfoHtml();
    } else if(event.GetId() == CTRL_PROJECT_LIST_TABLE)
    {
        m_ltIndex = index;
        m_lcIndex = 0;
        UpdateList();
        UpdateInfoHtml();
        m_lcTmpItem = m_lcItem;
        if(m_pAddToList != NULL) {
            if(m_ltIndex == 0) { //point to work directory
                m_pAddToList->Enable(false);
            } else {
                m_pAddToList->Enable(true);
            }
        }
    }
}
void SimuPrjInfoView::OnSetPathForDisp(wxCommandEvent &event)
{

}
void SimuPrjInfoView::OnTxtSearch(wxCommandEvent &event)//get the info by the given dir
{
    wxString value = wxEmptyString;
    if(m_pSearchText != NULL)
        value = m_pSearchText->GetValue();
    m_lcItem = m_lcTmpItem;
    if(value.IsEmpty())
        UpdateList();
    else
        OnBtnSearch(event);
}
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://
//                        DlgExpList
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://

int CTRL_JEXPLIST_ID_COMB_FILENAME = wxNewId();
int CTRL_JEXPLIST_ID_BT_OK = wxNewId();
int CTRL_JEXPLIST_ID_BT_CANCLE = wxNewId();

BEGIN_EVENT_TABLE(DlgExpList, wxDialog)
    EVT_COMBOBOX(CTRL_JEXPLIST_ID_COMB_FILENAME, DlgExpList::OnSelection)
    EVT_BUTTON(wxID_ANY, DlgExpList::OnButton)
END_EVENT_TABLE()

DlgExpList::DlgExpList(wxWindow *parent, wxArrayString &fileNames, wxWindowID id, const wxString &title, const wxPoint &pos, const wxSize &size,
                       long style)
{
    wxDialog::Create(parent, id, title, pos, wxSize(250, 160), style);
    m_fileName = wxEmptyString;
    m_fileNames = fileNames;
    CreateUI();
    Init();
}
DlgExpList::~DlgExpList()
{}
void DlgExpList::CreateUI()
{
    wxBoxSizer *bs = new  wxBoxSizer(wxVERTICAL);

    wxStaticText   *label = new    wxStaticText(this, wxID_ANY, wxT("Choose a list name:"), wxPoint(10, 10), wxSize(200, 25));
    bs->Add(label, 1, wxEXPAND);

    m_pCbExpFile = new wxComboBox(this, CTRL_JEXPLIST_ID_COMB_FILENAME, wxEmptyString, wxPoint(10, 40), wxSize(200, 25), m_fileNames, wxCB_DROPDOWN);

    wxBoxSizer *bsBt = new  wxBoxSizer(wxHORIZONTAL);
    wxButton  *bt = new    wxButton(this, CTRL_JEXPLIST_ID_BT_OK, wxT("Ok"), wxPoint(80, 80), wxSize(60, 25));
    bsBt->Add(bt, 1, wxEXPAND);
    bt = new    wxButton(this, CTRL_JEXPLIST_ID_BT_CANCLE, wxT("Cancel"), wxPoint(150, 80), wxSize(60, 25));
    bsBt->Add(bt, 1, wxEXPAND);
    bs->Add(bsBt, 1, wxEXPAND);
}
void DlgExpList::Init()
{

}
wxString    DlgExpList::GetExpFile()
{
    return m_fileName;
}
///////////////////////////////////////////////////////////////
void DlgExpList::OnButton(wxCommandEvent &event)
{
    int id = event.GetId();
    if(id == CTRL_JEXPLIST_ID_BT_OK)
    {
        EndModal(wxID_OK);
    } else {
        EndModal(wxID_CANCEL);
    }
}
void DlgExpList::OnSelection(wxCommandEvent &event)
{
    m_fileName = m_pCbExpFile->GetValue();
}
