/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

//---------------------------------------------------------------------------
//
// Name:        simuprjapp.cpp
// Author:      xychen
// Created:     2008-11-21 18:17:56
// Description:
//
//---------------------------------------------------------------------------

#include "simuprjapp.h"

#include <wx/filename.h>
#include <wx/stdpaths.h>

#include "envutil.h"
#include "fileutil.h"
#include "tools.h"

#include "manager.h"
#include "simuprjfrm.h"
#include "envsettings.h"


IMPLEMENT_APP(SimuPrjApp)

wxLogNull AD;

bool SimuPrjApp::OnInit() {
    EnvUtil::InitEnv();
    SetAppName(wxT("MolGUI"));
    wxString baseDir = wxEmptyString;
    baseDir = wxFileName(wxStandardPaths::Get().GetExecutablePath()).GetPath(wxPATH_GET_VOLUME|wxPATH_GET_SEPARATOR);
    EnvUtil::InitEnvPaths(baseDir);

    if(argc > 1) {
        if(wxString(argv[1]).Cmp(wxT("clearsetting")) == 0) {
            FileUtil::RemoveDirectory(EnvUtil::GetUserDataDir(), true);
            return false;
        }
    }

    if(!EnvUtil::CheckLicense()) {
        return false;
    };

    //
    SimuPrjFrm *frame = new SimuPrjFrm(NULL);
    Manager::Get()->SetAppFrame(frame);
    frame->CreateGUI();
    SetTopWindow(frame);
    Tools::SetAppFrame(frame);
    frame->Show();
    frame->AfterCreation();
    
    wxString checkMolcas = EnvUtil::GetMolcasDir(EnvSettingsData::Get()->GetMolcasPath());
    //wxString checkPymolcas = EnvUtil::
    if(checkMolcas.IsEmpty())
    {
        wxMessageDialog dlg(frame,wxT("Unable to locate MOLCAS driver.\nYou won't be able to run local jobs.\n\nDo you want to configure MolGUI?"),
                            wxT("Warning!"),
                            wxYES_NO | wxNO_DEFAULT | wxCENTRE | wxICON_EXCLAMATION
                            );
        if(dlg.ShowModal() == wxID_YES)
        {
            EnvSettings envsetDlg(frame);
            if(envsetDlg.ShowModal()==wxID_OK)
            {
            }
        }
    }

    return true;
}

int SimuPrjApp::OnExit() {
    return 0;
}
