/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/filename.h>
#include <wx/dir.h>
#include <wx/msgdlg.h>
#include <wx/treebase.h>
#include <wx/treectrl.h>

#include "fileutil.h"
#include "stringutil.h"
#include "xmlfile.h"
#include "envutil.h"
#include "tools.h"
#include "simuevents.h"
#include "simuproject.h"
#include "manager.h"
#include "projectmanager.h"
#include "simufiles.h"
#include "qchemfiles.h"
#include "molcasfiles.h"
#include "viewmanager.h"
#include "molview.h"
#include "simuprjdef.h"
#include "dlgoutput.h"
#include "mingutil.h"

#define Key_BEGIN "$BEGIN"
#define Key_END "$END"
#define Key_COOR "COOR"
#define MAX_ATOMS 500
#define STRINGSIZE 30
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                        class JobInfo
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static int currProjectId = 0;

JobInfo::JobInfo(wxString fileName)
{
    m_fileName = fileName;
    m_isDirty = false;
}

JobInfo::~JobInfo() {
}

void JobInfo::SetDirty(bool isDirty)
{
    m_isDirty = isDirty;
}

bool JobInfo::IsDirty(void) const
{
    return m_isDirty;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//       class JobNodeData
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
JobNodeData::JobNodeData()
{
    m_head = NULL;
}
JobNodeData::~JobNodeData()
{
    if(m_head != NULL)
    {
        Node *tmp = m_head;
        m_head = m_head->next;
        while(m_head != NULL) {
            delete tmp;
            tmp = m_head;
            m_head = m_head->next;
        }
        delete tmp;
        m_head = NULL;
    }
}
//**********************************************************************************8
bool JobNodeData::Add(wxTreeItemId id, wxString jobName)
{
    if(jobName.IsEmpty() || !id.IsOk())
        return false;
    Node   *nNode = new Node(id, jobName);
    if(m_head == NULL)
        m_head = nNode;
    else {
        Node *tmp = m_head;
        while(tmp->next != NULL)
            tmp = tmp->next;
        tmp->next = nNode;
    }
    return true;
}
bool JobNodeData::Del(wxString jobName)//the jobname in a project is unique.
{
    Node *par = m_head, *chd = NULL;
    if(jobName.IsSameAs(m_head->name))
    {
        m_head = m_head->next;
        delete par;
    } else {
        chd = m_head->next;
        while(chd != NULL) {
            if(chd->name.IsSameAs(jobName))
            {
                par->next = chd->next;
                delete chd;
                break;
            }
            chd = chd->next;
        }
    }

    return true;
}
wxTreeItemId JobNodeData::GetId(wxString jobName)
{
    wxTreeItemId    res;
    if(m_head == NULL)
        return res;
    Node *tmp = m_head;
    if(jobName.IsSameAs(m_head->name))
    {
        res = m_head->id;
    } else {
        tmp = m_head->next;
        while(tmp != NULL) {
            if(tmp->name.IsSameAs(jobName))
            {
                res = tmp->id;
                break;
            }
            tmp = tmp->next;
        }
    }
    return res;
}
bool JobNodeData::Rename(const wxString &oldName, const wxString &newName)
{
    wxTreeItemId    res;
    if(m_head == NULL)
        return false;
    Node *tmp = m_head;
    if(oldName.IsSameAs(m_head->name))
    {
        m_head->name = newName;
        return true;
    } else {
        tmp = m_head->next;
        while(tmp != NULL) {
            if(tmp->name.IsSameAs(oldName))
            {
                tmp->name = newName;
                return true;
            }
            tmp = tmp->next;
        }
    }
    return false;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                class SimuProject
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SimuProject::SimuProject(const wxString &fullPathName, wxTreeItemId parentId)
{
    //open project
    m_isDirty = 1;
    m_projectId = currProjectId++;
    m_type = PROJECT_MOLCAS;//PROJECT_UNKNOWN;
    m_title = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
    m_projectDesc = wxEmptyString;
    m_isLoaded = false;
    m_isChild = false;
    m_pJobInfo = NULL;
    m_prjFilesArray.Empty();
    m_mopacFiles.Empty();
    //        wxMessageBox(_T("Simuproject:")+fullPathName);
    //        m_warnNewMolecule=true;

    if (!fullPathName.IsEmpty() && wxFileExists(fullPathName)) {
        // existing project
        m_fullPathName = fullPathName;
        Load(parentId);
    }
}
SimuProject::SimuProject(ProjectType projectType, const wxString &title, const wxString &fullPathName)
{
    m_isDirty = 1;
    m_projectId = currProjectId++;
    m_type = projectType;
    m_title = title;
    m_projectDesc = wxEmptyString;
    // new project
    m_isLoaded = false;
    m_isChild = false;
    m_projectNode = 0;
    m_fullPathName = fullPathName;
    m_pJobInfo = NULL;
    //        m_fileList.Empty();
    m_prjFilesArray.Empty();
    m_mopacFiles.Empty();
    //        m_warnNewMolecule=true;
}

SimuProject::~SimuProject()
{
    m_prjFilesArray.Clear();
    m_mopacFiles.Clear();
    if(m_pJobInfo != NULL)
        delete m_pJobInfo;
    m_pJobInfo = NULL;
}
//***********************************************************************************************************
void SimuProject::AddMopacNode(wxString title)
{
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL) return;
    ProjectTreeItemData *itemData = NULL;
    if(!m_mopacNode.IsOk())
    {
        itemData = new ProjectTreeItemData(this, wxEmptyString, ITEM_MOPACDIR);
        m_mopacNode = pTree->AppendItem(m_projectNode, wxT("Mopac"), TREE_IMAGE_JOB_RESULTS, TREE_IMAGE_JOB_RESULTS, itemData);
    } else {
        wxString    tmp = wxEmptyString;
        for(unsigned i = 0; i < m_mopacFiles.GetCount(); i++) {
            tmp = m_mopacFiles[i];
            if(title.IsSameAs(tmp))
                return;
        }
    }
    itemData = new ProjectTreeItemData(this, GetMopacOutFilePath(title), ITEM_MOPACOUTPUT);
    pTree->AppendItem(m_mopacNode, title, TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
    m_mopacFiles.Add(title);
}
void SimuProject::AddMopacNode()
{
    m_mopacFiles = FileUtil::GetFileBySuffix(m_fullPathName.BeforeLast(platform::PathSep().GetChar(0)), wxPRJ_FILE_MOPAC_SUFFIX);
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL) return;
    wxString tmp = wxEmptyString;
    ProjectTreeItemData *itemData = NULL;
    if(m_mopacFiles.IsEmpty())
        return;
    if(!m_mopacNode.IsOk())
    {
        itemData = new ProjectTreeItemData(this, wxEmptyString, ITEM_MOPACDIR);
        m_mopacNode = pTree->AppendItem(m_projectNode, wxT("Mopac"), TREE_IMAGE_JOB_RESULTS, TREE_IMAGE_JOB_RESULTS, itemData);
    }
    for(unsigned i = 0; i < m_mopacFiles.GetCount(); i++) {
        tmp = m_mopacFiles[i];
        tmp = tmp.AfterFirst(wxT('.')).BeforeLast(wxT('.'));
        m_mopacFiles[i] = tmp;
        itemData = new ProjectTreeItemData(this, GetMopacOutFilePath(tmp), ITEM_MOPACOUTPUT);
        pTree->AppendItem(m_mopacNode, tmp, TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
    }
}
bool SimuProject::AttachView(MolView *pView)
{
    if(pView == NULL)
        return false;
    pView->SetProjectId(m_projectId);
    pView->SetTitle(m_title);
    wxString tmp = FileUtil::ChangeFileExt(m_fullPathName, wxMSD_FILE_SUFFIX);
    pView->SetFileName(tmp);
    return true;
}

void SimuProject::Refresh(bool expand)
{
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(!pTree) return;
    bool isExpanded = pTree->IsExpanded(GetProjectNode());
    pTree->DeleteChildren(GetProjectNode());
    BuildTree(pTree);
    if(expand || isExpanded)
        pTree->Expand(GetProjectNode());
}

void SimuProject::BuildTree(wxTreeCtrl *pTree)
{
    // add the project's root item
    if(pTree == NULL)
        return;
    ProjectTreeItemData *itemData = new ProjectTreeItemData(this, wxEmptyString, ITEM_MOLECULE);
    pTree->AppendItem(m_projectNode, wxT("Molecule"), TREE_IMAGE_MOLECULE, TREE_IMAGE_MOLECULE, itemData);
    itemData = new ProjectTreeItemData(this, wxEmptyString, ITEM_NEW_JOB);
    pTree->AppendItem(m_projectNode, wxT("New Job"), TREE_IMAGE_JOB_INFO, TREE_IMAGE_JOB_INFO, itemData);

    AddMopacNode();
    for(unsigned i = 0; i < m_prjFilesArray.GetCount(); i++) {
        BuildResultTree(m_prjFilesArray[i]->GetName());
    }
}
void SimuProject::BuildResultTree(wxString jobName, bool isExpandJob)
{
    wxString inputFile = GetFiles(jobName)->GetInputFileName();
    // wxMessageBox(inputFile);
    //m_fullPathName.BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+jobName+platform::PathSep()+jobName+wxMINPUT_FILE_SUFFIX;
    if(!wxFileExists(inputFile))
        return;

    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    wxTreeItemId jobResultNode = m_jobNode.GetId(jobName);
    ProjectTreeItemData *itemData = NULL;
    //        bool        isExpandJob=false;
    if(!jobResultNode.IsOk())//the job node do not exists
    {
        itemData = new ProjectTreeItemData(this, jobName, ITEM_JOB_RESULT);
        jobResultNode = pTree->AppendItem(m_projectNode, jobName, TREE_IMAGE_JOB_RESULTS, TREE_IMAGE_JOB_RESULTS, itemData);
        m_jobNode.Add(jobResultNode, jobName);
        //                isExpandJob=true;
    } else
        pTree->DeleteChildren(jobResultNode);

    itemData = new ProjectTreeItemData(this, jobName, ITEM_JOB_INFO);
    pTree->AppendItem(jobResultNode, wxT("Job Setting"), TREE_IMAGE_JOB_INFO, TREE_IMAGE_JOB_INFO, itemData);

    //   wxString outputFile=inputFile.BeforeLast(wxT('.'))+wxT(".status");//+GetOutfileSuffix();
    //   if(!wxFileExists(outputFile))
    //       return;

    GetFiles(jobName)->BuildResultTree(pTree, jobResultNode);
    if(isExpandJob == true)
        pTree->Expand(jobResultNode);
}
void    SimuProject::Close(bool isAskForSave, bool default_save)
{
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    bool        isToSave = default_save;
    if(isAskForSave == true && IsDirty())
    {
        wxMessageDialog mdlg(NULL, wxT("Do you want to save the project: ") + m_title + wxT("?"), wxT("Warning"), wxYES_NO );
        if(mdlg.ShowModal() == wxID_YES)
        {
            Save();
        } else {
            isToSave = false;
        }
    }
    Manager::Get()->GetViewManager()->CloseViewBPID(m_projectId, false, isToSave);

    pTree->DeleteChildren(GetProjectNode());
    pTree->Delete(GetProjectNode());
}
void SimuProject::CloseView(bool isAskForSave, bool default_save)
{
    bool        isToSave = default_save;
    if(isAskForSave == true && IsDirty())
    {
        wxMessageDialog mdlg(NULL, wxT("Do you want to save the view of project: ") + m_title + wxT("?"), wxT("Warning"), wxYES_NO );
        if(mdlg.ShowModal() == wxID_YES)
        {
            Save();
        } else {
            isToSave = false;
        }
    }
    Manager::Get()->GetViewManager()->CloseViewBPID(m_projectId, false, isToSave);
}
void SimuProject::DelJob(wxString jobName)
{
    m_jobNode.Del(jobName);
    RemoveFiles(jobName);
    wxString    jobDir = GetResultDir(jobName);
    FileUtil::RemoveDirectory(jobDir);
}
void        SimuProject::DelJobOuterFile(wxString jobName, wxString fileName)
{
    //wxMessageBox(jobName);
    //wxMessageBox(fileName);
    wxString        outfileDir = m_fullPathName.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + jobName;
    outfileDir = outfileDir + platform::PathSep() + wxRESULT_OUTER_FILE_DIR;
    if(fileName.IsSameAs(wxT("ALL"))) { //delete all
        FileUtil::RemoveDirectory(outfileDir);
        return;
    } else        {
        wxString        outerFile = outfileDir + platform::PathSep() + fileName;
        //wxMessageBox(outerFile);
        if(wxFileExists(outerFile))
            wxRemoveFile(outerFile);
    }
}
void SimuProject::DelMopacJob(wxString title)
{
    wxString tmp = wxEmptyString, dir = m_fullPathName.BeforeLast(wxT('.'));
    if(title.IsSameAs(wxT("ALL"))) {
        if(m_mopacFiles.IsEmpty())
            return;
        for(unsigned i = 0; i < m_mopacFiles.GetCount(); i++) {
            tmp = dir + wxT(".") + m_mopacFiles[i] + wxPRJ_FILE_MOPAC_SUFFIX;
            if(wxFileExists(tmp))
                wxRemoveFile(tmp);
        }
        m_mopacNode = 0;
        m_mopacFiles.Empty();
    } else {
        wxString mopacFile = GetMopacOutFilePath(title);
        if(wxFileExists(mopacFile))
            wxRemoveFile(mopacFile);
        for(unsigned i = 0; i < m_mopacFiles.GetCount(); i++) {
            tmp = m_mopacFiles[i];
            if(title.IsSameAs(tmp))
                m_mopacFiles.RemoveAt(i);
        }
    }
}
wxArrayString        SimuProject::GetJobs(void)
{
    wxArrayString        jobs;
    if(m_fullPathName.IsEmpty() || !wxFileExists(m_fullPathName))
        return jobs;
    XmlFile project = XmlFile(GetXmlFileRoot(), m_fullPathName);

    jobs = project.GetArrayString(PRJ_XML_ELEM_JOBS);
    if(jobs.GetCount() == 0)
        jobs = project.GetArrayString("results");
    return jobs;
}
wxString SimuProject::GetMopacOutFilePath(wxString fileName)
{
    return (m_fullPathName.BeforeLast(wxT('.')) + wxT(".") + fileName + wxPRJ_FILE_MOPAC_SUFFIX);
}
wxString  SimuProject::GetNewFileName(wxString nTitle)
{
    wxString prefix = m_fullPathName.BeforeFirst(wxCHAR_FN_SEP);
    if(prefix.IsSameAs(m_fullPathName)) {
        prefix = m_fullPathName.BeforeLast(platform::PathSep().GetChar(0)).BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + nTitle;
    } else {
        prefix = prefix.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + nTitle;
        prefix = prefix + wxFN_SEP + m_fullPathName.AfterFirst(wxCHAR_FN_SEP);
        prefix = prefix.BeforeLast(platform::PathSep().GetChar(0));
    }

    return (prefix + platform::PathSep() + nTitle + wxPRJ_FILE_SUFFIX);
}
wxString SimuProject::GetPathWithSep(void) const
{
    return wxFileName(m_fullPathName).GetPathWithSep();
}
ProjectType SimuProject::GetType(void) const
{
    return m_type;
}
wxString SimuProject::GetProjectType(ProjectType type)
{
    switch(type) {
    case PROJECT_SIMUCAL:
        return wxT("SimuCal");
    case PROJECT_MOLCAS:
        return wxT("Molcas");
    case PROJECT_GAUSSIAN:
        return wxT("Gaussian");
    case PROJECT_QCHEM:
        return wxT("QChem");
    case PROJECT_TERACHEM:
        return wxT("TeraChem");
    default:
        break;
    }
    return wxT("SimuCal");
}
ProjectType SimuProject::GetProjectType(const wxString &strType)
{
    ProjectType type = PROJECT_UNKNOWN;
    if(strType.IsSameAs(wxT("SimuCal"))) {
        type = PROJECT_SIMUCAL;
    } else if(strType.IsSameAs(wxT("Molcas"))) {
        type = PROJECT_MOLCAS;
    } else if(strType.IsSameAs(wxT("Gaussian"))) {
        type = PROJECT_GAUSSIAN;
    } else if(strType.IsSameAs(wxT("QChem"))) {
        type = PROJECT_QCHEM;
    } else if(strType.IsSameAs(wxT("TeraChem"))) {
        type = PROJECT_TERACHEM;
    }

    return type;
}
wxString SimuProject::GetOutfileSuffix()
{
    switch(m_type) {
    case PROJECT_MOLCAS:
        return wxMOUTPUT_FILE_SUFFIX;
    case PROJECT_QCHEM:
        return wxQOUTPUT_FILE_SUFFIX;
    default:
        break;
    }
    return wxT("unknown");
}
wxString SimuProject::GetTreeLabel(void)
{
    wxString label = wxEmptyString;
    label = GetTitle() + wxT("(") + GetProjectType(m_type) + wxT(")");
    return label;
}
JobInfo *SimuProject::GetJobInfo(void) const
{
    return m_pJobInfo;
}
wxString SimuProject::GetResultDir(wxString resultName) const
{
    return GetPathWithSep() + resultName;
}
wxString SimuProject::GetResultFileName(wxString resultName) const
{
    wxString tempName = resultName;
    tempName = GetResultDir(resultName) + platform::PathSep() + tempName;
    return tempName;
}
AbstractProjectFiles *SimuProject::GetFiles(wxString resultName)
{
    unsigned i = 0;
    for(i = 0; i < m_prjFilesArray.GetCount(); i++) {
        if(m_prjFilesArray[i]->GetName().Cmp(resultName) == 0) {
            return m_prjFilesArray[i];
        }
    }
    AbstractProjectFiles *projectFiles = NULL;

    switch(GetType()) {
    case PROJECT_SIMUCAL:
        projectFiles = new SimuFiles(this, resultName);
        break;
    case PROJECT_MOLCAS:
        projectFiles = new MolcasFiles(this, resultName);
        break;
    case PROJECT_QCHEM:
        projectFiles = new QChemFiles(this, resultName);
        break;
    default:
        break;
    }
    if(projectFiles) {
        m_prjFilesArray.Add(projectFiles);
    }
    return projectFiles;
}
void    SimuProject::RemoveFiles(wxString resultName)
{
    for(unsigned i = 0; i < m_prjFilesArray.GetCount(); i++) {
        if(m_prjFilesArray[i]->GetName().Cmp(resultName) == 0) {
            m_prjFilesArray.RemoveAt(i);
        }
    }
}
MolView  *SimuProject::GetView()
{
    return (MolView *)Manager::Get()->GetViewManager()->FindView(m_projectId);
}
wxString SimuProject::GetXmlFileRoot()
{
    return wxT("SimuProject");
}
bool SimuProject::HasMolecule()
{
    wxString        dataFile = FileUtil::ChangeFileExt(m_fullPathName, wxMSD_FILE_SUFFIX);

    if(!::wxFileExists(dataFile))
        return false;

    return true;
}
void        SimuProject::Import()
{
    wxString             projectName;

    //        projectName=m_fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
    wxMessageDialog mdlg(NULL, wxT("The file will be imported to the project: ") + m_title + wxT(" ."), wxT("Warning"), wxYES_NO | wxICON_INFORMATION);
    if(mdlg.ShowModal() != wxID_YES)
        return;

    wxString fileName = wxEmptyString, suffix = wxEmptyString;
    do {
        fileName = Tools::GetFilePathFromUser(Manager::Get()->GetAppWindow(), wxT("Import Molecule File"), wxT("XYZ Files (*.xyz)|*.xyz|Q-Chem Molecule File (*.in)|*.in|pdb Files (*.pdb)|*.pdb|sim Files (*.sim)|*.sim|All Files (*.*)|*.*"), wxFD_OPEN);//wangyuanlong   12/01
        suffix = fileName.AfterLast(wxT('.'));
        if(fileName.IsEmpty())  return;
        if(!suffix.IsSameAs(wxT("log")) && !suffix.IsSameAs(wxT("in")) && !suffix.IsSameAs(wxT("xyz")) && !suffix.IsSameAs(wxT("pdb")) && !suffix.IsSameAs(wxT("sim"))) { //wangyuanlong  12/01
            
            wxMessageBox(wxT("Unrecognized file type."), wxT("Warning"));
            continue;
        } else
            break;
    } while(!fileName.IsEmpty());
    // wxMessageBox(fileName);
    // std::cout << "SimuProject::Import() fileName " << fileName << std::endl;
    if(!StringUtil::IsEmptyString(fileName))
    {
        // std::cout << "!IsEmptyString before"  << std::endl;
        ProjectGroup   *pPG = Manager::Get()->GetProjectManager()->GetActiveProjectGroup();
        // std::cout << "!IsEmptyString" << pPG->GetName() << std::endl;
        if(pPG != NULL)
            pPG->Save();
        Manager::Get()->GetProjectManager()->GetTree()->SelectItem(m_projectNode);
        AbstractView *pView = Manager::Get()->GetViewManager()->FindView(m_projectId);
        // std::cout << "!pPG GetTitle" << pView->GetTitle() << std::endl;
        if(pView != NULL) { //delete the old structure;
            ((MolView *)pView)->GetData()->GetActiveCtrlInst()->Empty();
            // std::cout << "!pPG !pView GetTitle" << pView->GetTitle() << std::endl;
            //wxMessageBox(fileName);
        }
        if(fileName.AfterLast(wxT('.')).IsSameAs(wxT("pdb"))) { //change pdb file to xyz file;
            wxString        srcFile = fileName, tmpStr = wxEmptyString;
            fileName = FileUtil::ChangeFileExt(m_fullPathName, wxT(".xyz"));
            tmpStr = EnvUtil::GetWorkDir() + platform::PathSep() + wxT("tmp.xyz");
            //wxMessageBox(fileName);
            //wxMessageBox(tmpStr);
            wxMessageDialog mconvdlg(NULL, wxT("Would you like to add the hydrogen atoms?"), wxT("Warning"), wxYES_NO | wxICON_INFORMATION);
            if(mconvdlg.ShowModal() == wxID_YES) {
                XYZFile::ChangePDBFile(srcFile, tmpStr, fileName);
            } else {
                XYZFile::ChangePDBFile(srcFile, fileName, tmpStr);
            }
        }
        //wxString    msg=wxString::Format(wxT("%d"),GetProjectId());
        //wxMessageBox(msg);
        //        CloseView(false);
        // std::cout << "ShowSelectedItemView before"  << std::endl;
        Manager::Get()->GetProjectManager()->ShowSelectedItemView(fileName);
        // std::cout << "ShowSelectedItemView"  << std::endl;
        Save();
        // std::cout << "ShowSelectedItemView Save"  << std::endl;
        Manager::Get()->GetViewManager()->SaveViews(NULL);  //save the import Q-Chem file to the project
        SetDirty(false);
        Refresh(true);
    }
}
bool SimuProject::IsDirty() //dirty if the molecule is modified;
{
    AbstractView *pView = Manager::Get()->GetViewManager()->FindView(m_projectId);
    CtrlInst *pCtrlInst = NULL;
    if(pView != NULL && pView->GetType() == VIEW_GL_BUILD)
        pCtrlInst = ((MolView *)pView)->GetData()->GetCtrlInstByID(m_projectId);
    if(pCtrlInst != NULL && pCtrlInst->IsDirty()) {
        if(m_isDirty == 0)
            m_isDirty++;
    }
    return (m_isDirty > 0);
}
bool  SimuProject::IsPGChild(wxString pgFileName)
{   //to check if the cur prj is a child of the pgFileName pg.
    wxString    val = ProjectGroup::GetXmlFileRoot();
    XmlFile xmlproject = XmlFile(val, pgFileName);
    wxArrayString        astrProjects = xmlproject.GetArrayString(PG_XML_ELEM_PROJECTS);

    int count = astrProjects.GetCount();
    for(int i = 1; i < count; i = i + 2) {
        if(astrProjects[i].GetChar(0) == wxT('.')) //change relative path to absolute path.
            astrProjects[i] = pgFileName.BeforeLast(platform::PathSep().GetChar(0)) + astrProjects[i].AfterFirst(wxT('.'));
        if(astrProjects[i].IsSameAs(m_fullPathName))
            return true;
    }
    return false;
}
void SimuProject::Load(wxTreeItemId parentId)
{
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;
    ProjectTreeItemData        *itemData = new ProjectTreeItemData(this, m_title, ITEM_PROJECT); //???????????
    if(itemData == NULL)
        return;
    m_projectNode = pTree->AppendItem(parentId, m_title, 1, 1, itemData);

    BuildTree(pTree);
    //    pTree->Expand(parentId);
    pTree->Expand(m_projectNode);
    LoadJobs();

    SetDirty(false);
}
void  SimuProject::LoadJobs()
{
    if(m_projectNode.IsOk() == false)
        return;
    m_isLoaded = true;
    if(!wxFileExists(m_fullPathName))
        return;
    XmlFile project = XmlFile(GetXmlFileRoot(), m_fullPathName);
    wxString projectType = project.GetString(PRJ_XML_ELEM_TYPE);
    if(projectType.IsEmpty())
        projectType = project.GetString("type");
    m_type = GetProjectType(projectType);
    wxArrayString resultNames = project.GetArrayString(PRJ_XML_ELEM_JOBS);
    if(resultNames.GetCount() == 0)
        resultNames = project.GetArrayString("results");
    wxString fullFileName = wxEmptyString;
    unsigned int i = 0;
    for(i = 0; i < resultNames.GetCount(); i++) {
        fullFileName = GetFiles(resultNames[i])->GetInputFileName();
        //GetResultFileName(resultNames[i]);
        //        fullFileName+=wxMINPUT_FILE_SUFFIX;
        if(!wxFileExists(fullFileName)) {
            resultNames.RemoveAt(i);
            i--;
        }
    }
    for( i = 0; i < resultNames.GetCount(); i++) {
        GetFiles(resultNames[i]);
        BuildResultTree(resultNames[i]);
    }
}
void SimuProject::NotifyPlugins(wxEventType type)
{
    /*SimuEvent event(type);
    event.SetProject(this);
    Manager::Get()->ProcessEvent(event);
    */
}
bool SimuProject::RemoveResultDir(wxString resultName)
{
    FileUtil::RemoveDirectory(GetResultDir(resultName), true);
    for(unsigned i = 0; i < m_prjFilesArray.GetCount(); i++) {
        if(m_prjFilesArray[i]->GetName().Cmp(resultName) == 0) {
            AbstractProjectFiles *projectFiles = m_prjFilesArray[i];
            delete projectFiles;
            m_prjFilesArray.RemoveAt(i);
            break;
        }
    }
    Save();
    return true;
}
bool SimuProject::Rename(const wxString &nTitle)
{   //do not change the dir
    wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
    wxString outString = StringUtil::CheckString(nTitle);
    //        if(!outString.IsSameAs(nTitle))
    //    {
    //        wxMessageBox(wxT("Only the following character is legal: '0-9','a-z','A-Z','_'."));
    //        pTree->SetItemText(m_projectNode,m_title);
    //       return false;
    //   }
    wxString nFullPath = GetNewFileName(outString);
    //wxMessageBox(nFullPath);
    if(wxFileExists(nFullPath))
    {
        return false;
    }
    /*        {
            wxMessageDialog mdlg(NULL, wxT("The directory with the same name exists, would you like to overwrite it?"),wxT("Warning"),wxYES_NO);
            if(mdlg.ShowModal()!=wxID_YES){
                pTree->SetItemText(m_projectNode,m_title);
                return false;
            }
           if(Manager::Get()->GetProjectManager()->IsOpen(nFullPath,false)!=NULL){// for safety, do not delete automatically.
                wxMessageBox(wxT("Sorry, please close the project or project group with the same name at first."),wxT("Warnig"));
                pTree->SetItemText(m_projectNode,m_title);
                return false;
           }
           FileUtil::RemoveDirectory(nFullPath.BeforeLast(platform::PathSep().GetChar(0)));
        }*/
    pTree->SetItemText(m_projectNode, outString);
    if(!wxDirExists(nFullPath.BeforeLast(platform::PathSep().GetChar(0))))
        FileUtil::CreateDirectory(nFullPath.BeforeLast(platform::PathSep().GetChar(0)));
    FileUtil::CopyDirectory(m_fullPathName.BeforeLast(platform::PathSep().GetChar(0)), nFullPath.BeforeLast(platform::PathSep().GetChar(0)));

    wxString    oldPDir = m_fullPathName.BeforeLast(platform::PathSep().GetChar(0));
    //wxMessageBox(oldPDir);
    if(wxDirExists(oldPDir))
        FileUtil::RemoveDirectory(oldPDir);
    m_fullPathName = nFullPath;
    m_title = outString;
    Manager::Get()->GetViewManager()->RenameView(m_projectId, outString);

    return true;
}
bool  SimuProject::RenameJob(const wxString &jobName, const wxString &nName)
{
    //wxMessageBox(jobName);
    //wxMessageBox(nName);
    if(jobName.IsSameAs(nName))
        return true;
    unsigned i = 0;
    for(i = 0; i < m_prjFilesArray.GetCount(); i++) {
        if(m_prjFilesArray[i]->GetName().IsSameAs(jobName)) {
            m_prjFilesArray[i]->SetName(nName);
            break;
        }
    }
    wxString        dir = m_fullPathName.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep();
    //wxMessageBox(dir+jobName);
    //wxMessageBox(dir+nName);
    FileUtil::CopyDirectory(dir + jobName, dir + nName);
    FileUtil::RemoveDirectory(dir + jobName);

    wxString        outFileDir = MingUtil::GetJobOutputDir(dir + nName);

    if(!outFileDir.IsSameAs(dir + nName)) { //rename output files in the specified output dir
        //wxMessageBox(outFileDir);
        wxString        fileName, fileSuffix, oldFileName;
        wxDir   oldDir(outFileDir);
        bool cont = oldDir.GetFirst(&fileName, wxEmptyString, wxDIR_DEFAULT);
        while ( cont ) {
            //wxMessageBox(fileName);
            if(fileName.IsSameAs(wxT(".")) || fileName.IsSameAs(wxT(".."))) {
                cont = oldDir.GetNext(&fileName);
                continue;
            }
            fileSuffix = FileUtil::GetSuffix(fileName);
            if(fileSuffix.IsEmpty()) { //invalid file or xmldump.xml
                cont = oldDir.GetNext(&fileName);
                continue;
            }
            oldFileName = outFileDir + platform::PathSep() + fileName;
            fileName = fileName.Left(fileName.Len() - fileSuffix.Len() - 1);
            //wxMessageBox(fileName);
            if(!fileName.IsSameAs(jobName)) {
                cont = oldDir.GetNext(&fileName);
                continue;
            }
            //wxMessageBox(oldFileName);
            fileName = nName + wxT(".") + fileSuffix;
            fileName = outFileDir + platform::PathSep() + fileName;
            //wxMessageBox(fileName);
            wxRenameFile(oldFileName, fileName);

            cont = oldDir.GetNext(&fileName);
        }
    }
    //        FileUtil::RemoveDirectory(dir+nName);
    m_jobNode.Rename(jobName, nName);
    BuildResultTree(nName, true);
    Save();
    return true;
}
bool SimuProject::Save(void)
{
    //        MolView * view=NULL;c++
    //        if(Manager::Get()->GetViewManager()!=NULL)
    //                view=(MolView*)Manager::Get()->GetViewManager()->FindView(m_projectId);
    //        if(IsDirty()==true&&view!=NULL&&view->GetData()->GetActiveCtrlInst()->IsDirty()&&(m_prjFilesArray.GetCount()>0||m_mopacFiles.GetCount()>0))//has jobs
    //        {
    //        m_warnNewMolecule=false;
    //                wxMessageDialog mdlg(NULL,wxT("Some jobs based on the former molecule have been detected, and to save the new project will cause the lost of these jobs. Are you sure to save the projcet?"),wxT("Warning"),wxYES_NO);
    //                if(mdlg.ShowModal()==wxID_YES)
    //                {//hurukun 0426    maybe the old jobs should be deleted.
    //                        wxString        strTmp;
    //                        for(int i = 0; i < m_prjFilesArray.GetCount(); i++) {//delete job files
    //                                strTmp=m_prjFilesArray[i]->GetName();
    //                                strTmp=m_fullPathName.BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+strTmp;
    //                                FileUtil::RemoveDirectory(strTmp);
    //                        }
    /*                        wxTreeCtrl        *pTree=Manager::Get()->GetProjectManager()->GetTree();
                            unsigned int childitemcount=pTree->GetChildrenCount(m_projectNode,false);

                            DelMopacJob(wxT("ALL"));
                            if(m_mopacNode.IsOk())
                                    pTree->Delete(m_mopacNode);

                            wxTreeItemIdValue  cookie;
                            wxTreeItemId*        treeNodeTmp=new wxTreeItemId[childitemcount-2];

                            treeNodeTmp[0]=pTree->GetFirstChild(m_projectNode,cookie);
                            treeNodeTmp[0]=pTree->GetNextChild(m_projectNode,cookie);
                            for(int i=0;i<childitemcount-2;i++){
                                    treeNodeTmp[i]=pTree->GetNextChild(m_projectNode,cookie);
                            }
                            for(int i=0;i<childitemcount-2;i++){
                                    pTree->SelectItem(treeNodeTmp[i]);
                                    Manager::Get()->GetProjectManager()->DoPopDelete();
                            }
                            delete[] treeNodeTmp;
                            treeNodeTmp=NULL;
    */        //                pTree->DeleteChildren(m_projectNode);

    //                ProjectTreeItemData * itemData = new ProjectTreeItemData(this, wxEmptyString, ITEM_MOLECULE);
    //                pTree->AppendItem(m_projectNode, wxT("Molecule"), TREE_IMAGE_MOLECULE, TREE_IMAGE_MOLECULE, itemData);
    //                itemData = new ProjectTreeItemData(this,wxEmptyString, ITEM_NEW_JOB);
    //                pTree->AppendItem(m_projectNode, wxT("New Job"), TREE_IMAGE_JOB_INFO, TREE_IMAGE_JOB_INFO, itemData);
    //                m_prjFilesArray.Clear();
    //        m_warnNewMolecule=true;
    //                }else{
    //                        wxMessageBox(wxT("You can save the current molecule to a new project by \"Save As\", and \"reload\" the old one."),wxT("Warning"));
    //                        return false;
    //                }
    //        }
    //  wxMessageBox(m_fullPathName);
    if(!wxDirExists(m_fullPathName.BeforeLast(platform::PathSep().GetChar(0))))
        FileUtil::CreateDirectory(m_fullPathName.BeforeLast(platform::PathSep().GetChar(0)));
    SaveProjectFile();//save the .sip file;
    Manager::Get()->GetViewManager()->SaveView(m_projectId);//save the data file

    SetDirty(false);

    return true;
}
bool SimuProject::SaveProjectFile(void)
{
    wxString    xmlRoot = GetXmlFileRoot();

    wxString    createTime = wxEmptyString, lastModify = wxEmptyString;
    wxDateTime now = wxDateTime::Now();
    lastModify = now.FormatISODate(); //+now.FormatISOTime();
    //wxMessageBox(m_fullPathName);
    if(!wxFileExists(m_fullPathName))
        createTime = lastModify;
    XmlFile xmlProject = XmlFile(xmlRoot,  m_fullPathName);
    if(createTime.IsEmpty())
        createTime = xmlProject.GetString(PRJ_XML_ELEM_CREATEDATE);
    if(m_projectDesc.IsEmpty())
        m_projectDesc = wxT("A common Project");
    wxString projectType = GetProjectType(m_type);
    xmlProject.BuildElement(PRJ_XML_ELEM_TYPE, projectType);
    xmlProject.BuildElement(PRJ_XML_ELEM_DESCRIPTION, m_projectDesc);
    xmlProject.BuildElement(PRJ_XML_ELEM_CREATEDATE, createTime);
    xmlProject.BuildElement(PRJ_XML_ELEM_LASTMODIFY, lastModify);

    wxArrayString resultNames;
    for(unsigned i = 0; i < m_prjFilesArray.GetCount(); i++) {
        resultNames.Add(m_prjFilesArray[i]->GetName());
    }
    xmlProject.BuildElement("Jobs", resultNames);

    xmlProject.Save();//save the .sip file;
    return true;
}
//begin : hurukun : 9/11/2009
bool SimuProject::SaveAs(const wxString &fullPathName, bool isToLoad)//save the nolecule data file, job info will be ignored.
{
    wxString        dir = fullPathName.BeforeLast(platform::PathSep().GetChar(0));
    if(wxFileExists(fullPathName))//overwrite option.
    {
        wxMessageDialog mdlg(Manager::Get()->GetAppWindow(), wxT("The project file exists, do you want to overwrite it?"), wxT("Warning"), wxYES_NO);
        if(mdlg.ShowModal() != wxID_YES)
            return true;
        FileUtil::RemoveDirectory(dir);
    }

    //        SimuProject        oldprj(*this);
    if(!FileUtil::CreateDirectory(dir))
        return false;

    wxString    oldFullFillName = m_fullPathName;
    wxString    oldTitle = m_title;
    wxString    title = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
    //copy project file(.sip,.sim,.xyz) to a new dir with new name;
    FileUtil::CopyDirectory(oldFullFillName.BeforeLast(platform::PathSep().GetChar(0)), dir, wxDIR_FILES);
    if(m_isChild == false) //do not belong to a project group, replace the old with the new project.
    {
        m_fullPathName = fullPathName;
        m_title = title;
        // save the view data files: *.sim,*.xyz
        Manager::Get()->GetViewManager()->SaveViewAs(m_projectId, m_fullPathName);

        wxTreeCtrl *pTree = Manager::Get()->GetProjectManager()->GetTree();
        //update the name of the new saved one in the tree panel.
        pTree->SetItemText(m_projectNode, m_title);

        //delete job item if it exists.
        wxTreeItemId sasRoot = pTree->GetSelection();
        ProjectTreeItemData *item = sasRoot.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(sasRoot) : NULL;
        if(item == NULL) return false;

        sasRoot = item->GetProject()->GetProjectNode();
        pTree->DeleteChildren(sasRoot);
        ProjectTreeItemData        *itemData = new ProjectTreeItemData(this, wxEmptyString, ITEM_MOLECULE);
        pTree->AppendItem(sasRoot, wxT("Molecule"), TREE_IMAGE_MOLECULE, TREE_IMAGE_MOLECULE, itemData);
        itemData = new ProjectTreeItemData(this, wxEmptyString, ITEM_JOB_INFO);
        pTree->AppendItem(sasRoot, wxT("Job Settings"), TREE_IMAGE_JOB_INFO, TREE_IMAGE_JOB_INFO, itemData);
        // Save the project description file: *.sip and all others
        if(!Save())
            return false;
    } else { //if the project belong to a group, update the view file if opened.
        wxString title = fullPathName.AfterLast(platform::PathSep().GetChar(0)).BeforeLast(wxT('.'));
        //     MolView* pView=(MolView*)Manager::Get()->GetViewManager()->FindView(m_projectId);
        Manager::Get()->GetViewManager()->SaveViewAs(m_projectId, fullPathName);
        //      if(pView!=NULL)
        //          pView->Close();
    }
    if(isToLoad == true)
    {
        SimuProject *pProject = new SimuProject(m_type, title, fullPathName);
        Manager::Get()->GetProjectManager()->AddProject(pProject);
        wxTreeItemId rootId = Manager::Get()->GetProjectManager()->GetTreeRoot();
        pProject->Load(rootId);
        //add node to the wxTreeCtrl
    }
    return true;
}
void SimuProject::SetDirty(bool modified)
{
    AbstractView *pView = Manager::Get()->GetViewManager()->FindView(m_projectId);
    CtrlInst *pCtrlInst = NULL;
    if(pView != NULL && pView->GetType() == VIEW_GL_BUILD)
        pCtrlInst = ((MolView *)pView)->GetData()->GetCtrlInstByID(m_projectId);
    if(pCtrlInst != NULL)
        pCtrlInst->SetDirty(modified);
    if(modified == true && m_isDirty == 0)
        m_isDirty++;
    else if(modified == false)
        m_isDirty = 0;
}
void SimuProject::SetFileName(wxString fullPathName)
{
    m_fullPathName = fullPathName;
    AbstractView *pView = Manager::Get()->GetViewManager()->FindView(m_projectId);
    if(pView != NULL) {
        pView->SetFileName(m_fullPathName);
    }
}
void SimuProject::SetTitle(const wxString &title)
{
    m_title = title;
    AbstractView *pView = Manager::Get()->GetViewManager()->FindView(m_projectId);
    if(pView != NULL) {
        pView->SetTitle(m_title);
    }
    SetDirty(true);
    if(title != GetTitle()) {
        NotifyPlugins(EVT_SIMU_PROJECT_RENAMED);
    }
}
//end   : hurukun : 9/11/2009
void SimuProject::SetJobInfo(JobInfo *jobInfo)
{
    m_pJobInfo = jobInfo;
}
void SimuProject::ShowResultItem(ProjectTreeItemData *item)
{
    GetFiles(item->GetTitle())->ShowResultItem(item);
}
void SimuProject::UpdateViewByXyz()
{   //.sim+.xyz->.tmp->.sim
    wxString    xyzFile = FileUtil::ChangeFileExt(m_fullPathName, wxMCD_FILE_SUFFIX);
    if(!wxFileExists(xyzFile))
        return;
    Manager::Get()->GetProjectManager()->SetActiveProject(this);
    Manager::Get()->GetProjectManager()->GetTree()->SelectItem(m_projectNode);

    MolView *pView = (MolView *)(Manager::Get()->GetViewManager()->FindView(GetProjectId())) ;
    if(pView) {
        CtrlInst *pCtrlInst = pView->GetData()->GetActiveCtrlInst();
        pCtrlInst->Empty();
        pCtrlInst->SetDirty(false);
    }

    Manager::Get()->GetProjectManager()->ShowSelectedItemView(xyzFile);
    Manager::Get()->GetViewManager()->SaveViews(NULL);
}
