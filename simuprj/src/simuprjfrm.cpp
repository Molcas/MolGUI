/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/
#include "simuprjfrm.h"

#include "appglobals.h"
#include "configmanager.h"
#include "envutil.h"
#include "stringutil.h"
#include "tools.h"
#include "abstractview.h"

#include "manager.h"
#include "projectmanager.h"
#include "simuprjdef.h"
#include "starthereview.h"
#include "viewmanager.h"

#include "surfacepnl.h"
#include "vibrationpnl.h"

#include "auisharedwidgets.h"
#include "molrender.h"
#include "resource.h"
#include "uiprops.h"

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// SimuPrjFrm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++

SimuPrjFrm::SimuPrjFrm(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
        : wxFrame(parent, id, title, position, size, style), m_auiManager(this) {

        m_auiManager.SetFlags(wxAUI_MGR_DEFAULT | wxAUI_MGR_ALLOW_ACTIVE_PANE | wxAUI_MGR_TRANSPARENT_DRAG);
        m_pTbProject = NULL;
        m_pTbJob = NULL;
        m_pRecentProjectsMenu = NULL;
        m_pRecentProjectGroupsMenu=NULL;
        //begin : hurukun 05/11/2009
        m_helpCtrl = NULL;
        //end   : hurukun 05/11/2009
        CreateGUIControls();
        m_molData = NULL; //

}

SimuPrjFrm::~SimuPrjFrm() {
//        MolRender::FreeLabelFont();
        m_auiManager.UnInit();
        if(m_helpCtrl)
        delete m_helpCtrl;
    m_helpCtrl=NULL;
}

void SimuPrjFrm::CreateGUIControls() {
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start
    ////GUI Items Creation End

    wxBitmap bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK+ wxT("app.png"), wxBITMAP_TYPE_PNG);
    wxIcon frameIcon;
    frameIcon.CopyFromBitmap(bmp);
    this->SetIcon(frameIcon);

    CreateMenuBar();
    CreateToolBar();
    CreateStatusBar(3, wxST_SIZEGRIP);
    int widths[3] = {-1, -3, 150};
    SetStatusWidths(3, widths);
    Centre();
}

void SimuPrjFrm::CreateGUI(void) {
        InitWindowState();

    wxSize clientsize = GetClientSize();
        // project manager
    m_auiManager.AddPane(Manager::Get()->GetProjectManager()->GetTree(), wxAuiPaneInfo().
                                                        Name(wxT("ManagementPane")).Caption(wxT("Project Management")).
                                                        BestSize(wxSize(150, clientsize.GetHeight())).MinSize(wxSize(200,100)).Left().Layer(1).CloseButton(false)); //SONG add the CloseButton(false) choose, because when close the Project manger panel, it disappear forever
        // display manager
    m_auiManager.AddPane(Manager::Get()->GetViewManager()->GetNotebook(), wxAuiPaneInfo().
                            Name(wxT("ViewPane")).Caption(wxT("View Management")).
                                                        BestSize(wxSize(400, clientsize.GetHeight())).MinSize(wxSize(600,100)).Center().Layer(1).CloseButton(false));
        //
        AuiSharedWidgets::Get()->SetAuiManager(&m_auiManager);
        AuiSharedWidgets::Get()->BuildMenus(GetMenuBar());
        AuiSharedWidgets::Get()->BuildToolBars(this, false);
        AuiSharedWidgets::Get()->BuildWidgets(this);
        AuiSharedWidgets::Get()->EnableMenuTool(false);
        //
        SurfacePnl* pPnlSurface = new SurfacePnl(this, wxID_ANY, wxPoint(0,0), wxSize(290,600));
        VibrationPnl* pPnlVibration = new VibrationPnl(this, wxID_ANY, wxPoint(0,0), wxSize(340,600));
           AuiSharedWidgets::Get()->AddWindow(STR_PNL_SURFACE, wxT("Surface Panel"), pPnlSurface, 290, 600);
           AuiSharedWidgets::Get()->AddWindow(STR_PNL_VIBRATION, wxT("Vibration Panel"), pPnlVibration, 340, 600);
        //m_auiManager.Update();
    DoUpdateLayout();
    //
    Manager::Get()->GetViewManager()->ShowStartHereView(true);
    //
}

void SimuPrjFrm::DoUpdateLayout(void) {
    m_auiManager.Update();
}

void SimuPrjFrm::CreateMenuBar(void) {
        wxMenu *fileMenu = new wxMenu;
        wxMenu *fileAddMenu = new wxMenu;
        fileMenu->Append(MENU_ID_FILE_NEW, wxT("New ..."));
        fileMenu->Append(MENU_ID_FILE_ADD, wxT("Add ..."),fileAddMenu);
        fileAddMenu->Append(MENU_ID_FILE_ADDPROJECT, wxT("New Project"));
        fileAddMenu->Append(MENU_ID_FILE_ADDLEVEL, wxT("New Group"));
        fileAddMenu->Append(MENU_ID_FILE_EXISTITEM, wxT("Exist Item"));

        fileMenu->Append(MENU_ID_FILE_OPEN, wxT("Open ..."));

        m_pRecentProjectsMenu = new wxMenu;
        fileMenu->Append(MENU_ID_FILE_RECENTPROJECTS, wxT("&Recent Projects"), m_pRecentProjectsMenu);
        m_pRecentProjectsMenu->AppendSeparator();
        m_pRecentProjectsMenu->Append(MENU_ID_FILE_RECENTPROJECTS_CLEARHISTORY, wxT("&Clear Histroy"));

                m_pRecentProjectGroupsMenu = new wxMenu;
        fileMenu->Append(MENU_ID_FILE_RECENTPROJECTGROUPS, wxT("&Recent Project Groups"), m_pRecentProjectGroupsMenu);
        m_pRecentProjectGroupsMenu->AppendSeparator();
        m_pRecentProjectGroupsMenu->Append(MENU_ID_FILE_RECENTPROJECTGROUPS_CLEARHISTORY, wxT("&Clear Histroy"));

        fileMenu->Append(MENU_ID_FILE_PROJECT_INFO_LIST,wxT("Project Infomation List"));
        fileMenu->AppendSeparator();
        fileMenu->Append(MENU_ID_FILE_COPY, wxT("Copy"));
        fileMenu->Append(MENU_ID_FILE_PASTE, wxT("Paste"));
        fileMenu->AppendSeparator();
        fileMenu->Append(MENU_ID_FILE_SAVE, wxT("Save"));
        fileMenu->Append(MENU_ID_FILE_SAVEAS, wxT("Save As ..."));
        fileMenu->Append(MENU_ID_FILE_SAVEALL, wxT("Save All ..."));

        fileMenu->AppendSeparator();
        fileMenu->Append(MENU_ID_FILE_DELETE, wxT("Delete"));
//        fileMenu->Append(MENU_ID_FILE_REMOVEALL, wxT("Remove All "));

        fileMenu->AppendSeparator();
        fileMenu->Append(MENU_ID_FILE_CLOSE, wxT("Close "));
        fileMenu->Append(MENU_ID_FILE_CLOSEALLVIEW, wxT("Close All View"));
        fileMenu->Append(MENU_ID_FILE_CLOSEALL, wxT("Close All "));

        fileMenu->AppendSeparator();
        fileMenu->Append(MENU_ID_FILE_IMPORTPROJECT, wxT("Import File..."));

        fileMenu->AppendSeparator();
        fileMenu->Append(wxID_EXIT, wxT("E&xit"));

        fileMenu->Enable(MENU_ID_FILE_ADD,false);
        fileMenu->Enable(MENU_ID_FILE_SAVE,false);
        fileMenu->Enable(MENU_ID_FILE_SAVEAS,false);
        fileMenu->Enable(MENU_ID_FILE_SAVEALL,false);
        fileMenu->Enable(MENU_ID_FILE_CLOSE,false);
        fileMenu->Enable(MENU_ID_FILE_CLOSEALL,false);
//        fileMenu->Enable(MENU_ID_FILE_CLOSEALLVIEW,false);
//        fileMenu->Enable(MENU_ID_FILE_REMOVE,false);
//        fileMenu->Enable(MENU_ID_FILE_REMOVEALL,false);
//        fileMenu->Enable(MENU_ID_FILE_DELETE,false);
//--------------------------

        wxMenu *jobMenu = new wxMenu;
    jobMenu->Append(MENU_ID_JOB_NEW, wxT("&New"));
//        jobMenu->Enable(MENU_ID_JOB_NEW,false);
    jobMenu->AppendSeparator();
    jobMenu->Append(MENU_ID_JOB_SETTING, wxT("&Setting"));
//        jobMenu->Enable(MENU_ID_JOB_SETTING,false);
    jobMenu->Append(MENU_ID_JOB_SUBMIT, wxT("&Submit"));
//        jobMenu->Enable(MENU_ID_JOB_SUBMIT,false);
    jobMenu->Append(MENU_ID_JOB_MONITOR, wxT("&Monitor"));
//        jobMenu->Enable(MENU_ID_JOB_MONITOR,false);

        wxMenu *settingMenu = new wxMenu;
        settingMenu->Append(MENU_ID_EDIT_ENVSETTING, wxT("Environment Settings..."));
        settingMenu->Append(MENU_ID_EDIT_COLRSETTING, wxT("Color Settings..."));
        settingMenu->Append(MENU_ID_SETTING_HOSTS,wxT("Hosts Settings..."));
        settingMenu->Append(MENU_ID_SETTING_MASTER,wxT("Master Settings..."));

    wxMenu *helpMenu = new wxMenu;
           helpMenu->Append(wxID_HELP, wxT("&") + appglobals::appName + wxT(" Help\tF1"));
    helpMenu->Append(wxID_ABOUT, wxT("&About..."));

        wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, wxT("&Project"));
        menuBar->Append(jobMenu, wxT("&Job"));
        menuBar->Append(settingMenu, wxT("&Setting"));
           menuBar->Append(helpMenu, wxT("&Help"));

        SetMenuBar(menuBar);
}

void SimuPrjFrm::CreateToolBar(void) {
    wxBitmap bmp;

    long style = GetUIProps()->menuProps.isToolbarStyleText | GetUIProps()->menuProps.isToolbarStyleFlat;

    m_pTbProject = new wxToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);

    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("new.png"), wxBITMAP_TYPE_PNG);
    //m_pTbProject->AddTool(TOOL_ID_PRJ_NEW, wxT("New"), bmp, wxT("New Project"));
    m_pTbProject->AddTool(TOOL_ID_PRJ_NEW, wxEmptyString, bmp, wxT("New Project/Project Group"));

    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("open.png"), wxBITMAP_TYPE_PNG);
    //m_pTbProject->AddTool(TOOL_ID_PRJ_OPEN, wxT("Open"), bmp, wxT("Open File"));
    m_pTbProject->AddTool(TOOL_ID_PRJ_OPEN, wxEmptyString, bmp, wxT("Open File"));

    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("save.png"), wxBITMAP_TYPE_PNG);
    //m_pTbProject->AddTool(TOOL_ID_PRJ_SAVE, wxT("Save"), bmp, wxT("Save Selected Item"));
    m_pTbProject->AddTool(TOOL_ID_PRJ_SAVE, wxEmptyString, bmp, wxT("Save Selected Item"));
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("saveall.png"), wxBITMAP_TYPE_PNG);
    //m_pTbProject->AddTool(TOOL_ID_PRJ_SAVEALL, wxT("Save All"), bmp, wxT("Save All Items"));
    m_pTbProject->AddTool(TOOL_ID_PRJ_SAVEALL, wxEmptyString, bmp, wxT("Save All Items"));

    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("close.png"), wxBITMAP_TYPE_PNG);
    //m_pTbProject->AddTool(TOOL_ID_PRJ_CLOSE, wxT("Close"), bmp, wxT("Close Selected Item"));
    m_pTbProject->AddTool(TOOL_ID_PRJ_CLOSE, wxEmptyString, bmp, wxT("Close Selected Item"));
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("Pinfolist.png"), wxBITMAP_TYPE_PNG);
    //m_pTbProject->AddTool(TOOL_ID_W_INFO_LIST, wxT("Projects"), bmp, wxT("Projects Information List"));
    m_pTbProject->AddTool(TOOL_ID_W_INFO_LIST, wxEmptyString, bmp, wxT("Projects Information List"));
    m_pTbProject->Realize();

    m_auiManager.AddPane(m_pTbProject, wxAuiPaneInfo().Name(STR_TB_PROJECT).Caption(wxT("Project Tools")).
                        ToolbarPane().Top().Row(0).LeftDockable(false).Position(0).RightDockable(false));

    m_pTbJob = new wxToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);


    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("builder.png"), wxBITMAP_TYPE_PNG);
    //m_pTbJob->AddTool(TOOL_ID_JOB_SUBMIT, wxT("Submit"), bmp, wxT("Sumbit Job"));
    m_pTbJob->AddTool(TOOL_ID_SHOW_MOLECULE, wxEmptyString, bmp, wxT("Show Molecule"));

    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("submit.png"), wxBITMAP_TYPE_PNG);
    //m_pTbJob->AddTool(TOOL_ID_JOB_SUBMIT, wxT("Submit"), bmp, wxT("Sumbit Job"));
    m_pTbJob->AddTool(TOOL_ID_JOB_SUBMIT, wxEmptyString, bmp, wxT("Submit Job"));
#ifndef __SIMU_Q__
    m_combBCmdLine=new wxComboBox(m_pTbJob,TOOL_ID_COMBB_JOB_CMD_LINE,wxEmptyString,wxDefaultPosition,wxSize(200,25),0,NULL,wxCB_DROPDOWN);
    m_pTbJob->AddControl(m_combBCmdLine);
#else
    m_combBCmdLine=NULL;
#endif
    m_pTbJob->AddSeparator();
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("monitor.png"), wxBITMAP_TYPE_PNG);
    //m_pTbJob->AddTool(TOOL_ID_JOB_MONITOR, wxT("Monitor"), bmp, wxT("Monitor Job"));
    m_pTbJob->AddTool(TOOL_ID_JOB_MONITOR, wxEmptyString, bmp, wxT("Monitor Job"));
    m_pTbJob->Realize();

    m_auiManager.AddPane(m_pTbJob, wxAuiPaneInfo().Name(STR_TB_JOB).Caption(wxT("Job Tools")).
                        ToolbarPane().Top().Row(0).LeftDockable(false).Position(1).RightDockable(false));
    //---------add by XIA-----------
    m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVE,false);
    m_pTbProject->EnableTool(TOOL_ID_PRJ_SAVEALL,false);
    m_pTbProject->EnableTool(TOOL_ID_PRJ_CLOSE,false);
    m_pTbProject->EnableTool(TOOL_ID_W_INFO_LIST,true);
}

void SimuPrjFrm::AfterCreation(void) {
        UpdateRecentProjects(wxEmptyString);
        UpdateRecentPrjGrps(wxEmptyString);
}
wxString        SimuPrjFrm::GetCmdLine(){
        if(m_combBCmdLine==NULL)
                return wxEmptyString;
        else
                return m_combBCmdLine->GetValue();
}
void SimuPrjFrm::InitWindowState(void) {
    int posWin[] = {100, 100, 1100, 900};
    // load window size and position
    wxArrayString posArray = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetArrayString("winPosition");
        if(posArray.GetCount() < 4) {
        }else {
            long value = 0;
                for(unsigned i = 0; i < 4; i++) {
                        if(posArray[i].ToLong(&value)) {
                                posWin[i] = (int)value;
                        }
                }
        }
    // maximize if needed
    Maximize(Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetBool("winMaximized", false));
    // set size and position
    SetSize(wxRect(posWin[0], posWin[1], posWin[2], posWin[3]));
        this->SetMinSize(wxSize(800,600));
}

void SimuPrjFrm::SaveWindowState(void) {
        wxArrayString posArray;
        posArray.Add(wxString::Format(wxT("%d"), GetPosition().x));
        posArray.Add(wxString::Format(wxT("%d"), GetPosition().y));
        posArray.Add(wxString::Format(wxT("%d"), GetSize().x));
        posArray.Add(wxString::Format(wxT("%d"), GetSize().y));
        Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->BuildElement("winPosition", posArray);
        Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->BuildElement("winMaximized", IsMaximized());
}

void SimuPrjFrm::UpdateRecentProjects(const wxString& fileName, bool isClearHis, bool isRemove) {
        wxArrayString projHis = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetArrayString("RecentProjects");
    Tools::UpdateRecentFilesMenu(m_pRecentProjectsMenu, MENU_ID_PROJECT0, fileName, projHis, isClearHis, isRemove);
    Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->BuildElement("RecentProjects", projHis);
        AbstractView* startHereView = Manager::Get()->GetViewManager()->GetView(global_startHereTitle);
    if (startHereView) ((StartHereView*)startHereView)->InitView();
}
void SimuPrjFrm::UpdateRecentPrjGrps(const wxString& fileName, bool isClearHis, bool isRemove) {
        wxArrayString prjGrpHis = Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->GetArrayString("RecentProjectGroups");
    Tools::UpdateRecentFilesMenu(m_pRecentProjectGroupsMenu, MENU_ID_PROJECTGROUP0, fileName, prjGrpHis, isClearHis, isRemove);
    Manager::Get()->GetConfigManager()->GetConfig(appglobals::appName)->BuildElement("RecentProjectGroups", prjGrpHis);
        AbstractView* startHereView = Manager::Get()->GetViewManager()->GetView(global_startHereTitle);
    if (startHereView) ((StartHereView*)startHereView)->InitView();
}
void SimuPrjFrm::RefreshGraphics(void) {
//    m_pGLCanvas->Init();
    m_pGLCanvas->Draw();
}

MolViewData* SimuPrjFrm::GetMolData(void) const {
        return m_molData;
}
