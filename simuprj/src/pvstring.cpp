/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

/****************************************************************************/
/*                                                                          */
/* General string operations                                                */
/*                                                                          */
/****************************************************************************/
#include "pvstring.h"

/* See desctription in header file */

int
 MyString::myNext (char *in, char c, int *shift, int *len)
{
  char *ptr;
  int tmp;
  tmp=strlen(in);
  if (*shift >= tmp)
    return -1;
  ptr = strchr (in + *shift, c);

  if (ptr == NULL)
    ptr = strchr (in + *shift, '\n');
  if (ptr == NULL)
    {
      *len = strlen (in) - *shift;
    }
  else
    {
      *len = ptr - in - *shift;
    }
  if (*len < 1)
    return -1;
  return 0;
}

int
MyString::mytoken (char *in, char c, char *out, char *innew)
{
  int i;
  int flag = 0, il = 0;
  for (i = 0; in[i] != 0; i++)
    {
      if (flag == 0)
        {
          out[i] = in[i];
          if (in[i] == c)
            {
              flag = 1;
              out[i] = 0;
              il = i;
            }
        }
      else
        {
          innew[i - il - 1] = in[i];
        }
    }
  innew[i - il - 1] = 0;
  return flag;
}


int
MyString::mycut (char *in, char c, char *out)
{
  int i = 0;
  while (in[i] != 0 && in[i] != c)
    {
      out[i] = in[i];
      i++;
    }
  out[i] = 0;
  return 1;
}


int
MyString::myterminate (char *in, char *term)
{
  int i, j;
  int l = strlen (term);
  for (i = 0; in[i] != 0; i++)
    for (j = 0; j < l; j++)
      if (in[i] == term[j])
        {
          in[i] = 0;
          return 1;
        }
  return 1;
}



char *
MyString::myexchange (char *in, char a, char b)
{
  int i;
  for (i = 0; in[i] != 0; i++)
    {
      if (in[i] == a)
        in[i] = b;
    }
  return in;
}

int
MyString::myfgets (char *str, int len, FILE * inp)
{
  char temp[80];
  char *ptr;
  if (fgets (str, len - 1, inp) == NULL)
    return 0;
  if ((ptr = strchr (str, '\n')) != NULL)
    {
      *ptr = 0;
      return 1;
    }

  str[len - 1] = 0;
  if (fgets (temp, 80, inp) == NULL)
    return 1;
  while ((strchr (temp, '\n')) == NULL)
    if (fgets (temp, 80, inp) == NULL)
      break;

  return 1;
}

int
MyString::mysubdelete (char *str, char *token, char c)
{
  char *ptr, *ptr1;
  ptr = strstr (str, token);
  if (ptr == NULL)
    return 0;
  ptr1 = strchr (ptr, c);
  if (ptr1 == NULL)
    {
      *ptr = 0;
      return 1;
    }
  strcpy (ptr, ptr1 + 1);
  return 1;
}

int
MyString::mysubstitute (char *in, char *out, char *token, char *ins)
{
  char *ptr;
  ptr = strstr (in, token);
  if (ptr == NULL)
    return 0;
  strncpy (out, in, ptr - in);
  out[ptr - in] = 0;
  strcat (out, ins);
  strcat (out, ptr + strlen (token));
  return 1;
}

int
MyString::mylastchar (char *str, char c)
{
  if (str[strlen (str) - 1] == c)
    return 1;
  else
    return 0;
}

char *
MyString::strcatc (char *str, char c)
{
  int i;
  i = strlen (str);
  str[i] = c;
  str[i + 1] = 0;
  return str;
}

char *
MyString::strcatl (char *str, char c)
{
  int i;
  i = strlen (str);
  if (str[i] - 1 == c)
    return str;
  str[i] = c;
  str[i + 1] = 0;
  return str;
}

int
MyString::mylastvchar (char *str, char c)
{
  int i;
  int j;
  j = strlen (str) - 1;
  for (i = j; i > 0; i--)
    {
      if (str[i] != '\n' && str[i] != ' ')
        break;
    }
  if (str[i] == c)
    return 1;
  else
    return 0;
}


int
MyString::mylastdel (char *str, char c)
{
  int i;
  i = strlen (str) - 1;
  if (str[i] == c)
    str[i] = 0;
  return 0;
}

char *
MyString::chop (char *str)
{
  int i;
  i = strlen (str) - 1;
  str[i] = 0;
  return str;
}

char *
MyString::chomp (char *str)
{
  int i;
  i = strlen (str) - 1;
  if (str[i] == '\n')
    str[i] = 0;
  return str;
}


char *
MyString::mydiet (char *in)
{
  int i, j = 0, l;
  char *temp, *ptr;
  //int ln=strlen(in);
  if (!strlen (in))
    return in;

  temp = (char *) malloc ((strlen (in) + 2) * sizeof (char));
  if (temp == NULL)
    {
      fprintf (stderr, "Memory is over\n");
      return NULL;
    }
  if ((ptr = strchr (in, '\n')) != NULL)
    *ptr = ' ';
  l = strlen (in) - 1;
  for (i = 0; i < l; i++)
    {
      if (in[i] == ' ' && in[i + 1] == ' ')
        continue;
      temp[j++] = in[i];
    }
  temp[j++] = in[i];

  temp[j] = 0;
  if (temp[0] == ' ')
    {
      strcpy (temp, temp + 1);
      j--;
    }

  if (j > 0)
    if (temp[j - 1] == ' ')
      temp[j - 1] = 0;

  strcpy (in, temp);

  free (temp);
  return in;
}

char *
MyString::mydietspace (char *in)
{
  int i, j = 0, l, k;
  char *temp;
  if (!strlen (in))
    return in;
  temp = (char *) malloc ((strlen (in) + 2) * sizeof (char));
  if (temp == NULL)
    {
      fprintf (stderr, "Memory is over\n");
      return NULL;
    }
  l = strlen (in) - 1;
  for (i = 0; i < l; i++)
    {
      if (in[i] == ' ' && in[i + 1] == ' ')
        continue;
      temp[j++] = in[i];
    }
  temp[j++] = in[i];

  temp[j] = 0;
  if (temp[0] == ' ')
    {
      for(k=0; k<j; k++)
      {
      temp[k]=temp[k+1];
     /*
      strcpy (temp, temp + 1);
      */
      }
      j--;
    }

  if (j > 0)
    if (temp[j - 1] == ' ')
      temp[j - 1] = 0;
  if (j > 1)
    if (temp[j - 1] == '\n' && temp[j - 2] == ' ')
      temp[j - 1] = '\n';
  strcpy (in, temp);

  free (temp);
  return in;
}


char *MyString::strupr1 (char *str)
{
/* simple substitution of strupr function */
  int i;
  unsigned int c;

  for (i = 0; str[i] != 0; i++)
    if (islower (c = *(str + i)))
      *(str + i) = toupper (c);
  return str;

}

char *MyString::strlwr1 (char *str)
{
/* simple substitution of strlwr function */
  int i;
  unsigned int c;

  for (i = 0; str[i] != 0; i++)
    if (isupper (c = *(str + i)))
      *(str + i) = tolower (c);
  return str;

}

int
MyString::mysubtranc (char *in, char *token, char *ins)
{
  char *ptr;
  int l_ins;
  int l_token;
  int l = 0;
 unsigned  int i;
  l_ins = strlen (ins);
  l_token = strlen (token);
  if (l_token < l_ins)
    {
      fprintf (stderr, "Wrong usage of mysubtranc\n");
      return 0;
    }
  while ((ptr = strstr (in + l, token)) != NULL)
    {
      strncpy (ptr, ins, l_ins);
      for(i=0; i<strlen (ptr + l_token); i++)
       {
         *(ptr+l_ins+i)=*(ptr+l_token+i);
       }
/*
      strncpy (ptr + l_ins, ptr + l_token, strlen (ptr + l_token));
*/
      ptr[l_ins + strlen (ptr + l_token)] = 0;
      l = l_token + 1;
    }
  return 1;
}


/*
int itoa(int i, char* s)
{
char c;
int j,k=0;
  if(i<=0) return -1;
while(i>0)
 {
  j=i-i/10*10;
  i=i/10;
  c=char(j+int('0'));
  s[k++]=c;
if(k>sizeof(s)) return -1;
  }
s[k]=0;
 for(j=0; j<k/2; j++)
 {
 c=s[j];
 s[j]=s[k-1-j];
 s[k-1-j]=c;
 }
 return 0;
}
*/


int
MyString::mycount (char *in, char c)
{
  int i = 0, j = 0;
  while (in[i])
    {
      if (in[i++] == c)
        j++;
    }
  return j;
}

int
MyString::mycount2 (char *in, char c, int n)
{
  int i = 0, j = 0;
  while (in[i])
    {
      if (i == n)
        break;
      if (in[i++] == c)
        j++;

    }
  return j;
}

int
MyString::mycounts (char *in, char *sub, char c)
{
  int i = 0, j = 0, l;
  char *ptr;
  ptr=strstr(in,sub);
  if(ptr==NULL) return 0;
  l=ptr-in;
  while (i<l)
    {
      if (in[i++] == c)
        j++;

    }
  return j;
}

int
MyString::mycounts_next (char *in, char *sub, char c, char *next, int len)
{
  int i = 0, j = 0, l;
  char *ptr;
  ptr=strstr(in,sub);
  if(ptr==NULL) return 0;

  l=ptr-in;

  strncpy(next,ptr,len);
  next[len]=0;
  ptr=strchr(next,c);
  if(ptr) *ptr=0;
  while (i<l)
    {
      if (in[i++] == c)
        j++;

    }
  return j;
}

#include <stdlib.h>

void *
MyString::xmalloc (size_t size)
{

  register void *value = malloc (size);
  if (size == 0)
    {
      fprintf (stderr, "malloc call with zero size!\n");
      exit (-1);
    }
  if (value == 0)
    {
      fprintf (stderr, "Virtual memory exhausted\n");
      exit (-1);
    }
  return value;
}

char *
MyString::myextradiet (char *s, char c)
{
  int i, j = 0, l;
  char *s1;
  if (s[0] == 0)
    return s;
  if ((s1 = (char *) xmalloc ((strlen (s) + 1) * sizeof (char))) == NULL)
    return NULL;
  strcpy (s1, s);
  l = strlen (s);
  for (i = 0; i < l; i++)
    {
      if (s1[i] != c)
        {
          s[j] = s1[i];
          j++;
        }
    }
  s[j] = 0;
  free (s1);

  return s;
}

int
MyString::mycutsub (char *in, char c)
{
  char *ptr;
  ptr = strchr (in, c);
  if (ptr != NULL)
    strcpy (in, ptr + 1);
  return 0;
}

void MyString::msU_MSG_ArgS(char *str)
{
puts(str);
return        ;

}

void MyString::msU_MSG_Display (int confirm, char *type, int *status)
{
  return;
}

void MyString::msU_INT_ReportProgress (int ind)
{
   return;
}

int MyString::msU_INT_RequestInterrupt (void)
{

   return 0;
}

int MyString::msS_STR_TrimToken(char *line, char *token)
{
char *ptr;
int i;
ptr=strstr(line,token);
if(ptr!=NULL)
   {
   i=strlen(token);
   strcpy(line,ptr+i);
   }

 return 0;


}
void *MyString::msS_MEM_Alloc(size_t size)
{
   return (void *) xmalloc(size);

}

int MyString::msU_INT_ReportStatus(char *line, int i, int *status)
{
  return 0;
}
int MyString::echo(const char* s)
{
 return printf("Echo: %s\n",s);
}

int MyString::die(const char* s)
{
 return printf("Die: %s\n",s);
}

int MyString::SayError(const char* msg)
{
 //printf("Error: %s\nerrno=%d\n",msg,errno);
printf("Error: %s\n",msg);
 return 2;
}

/*
int MyString::DumpArray(const char* out_name, int nx, int ny, int nz, double data[])
{
 FILE* f;
 int ix,iy,iz;

 if ((f=fopen(out_name,"w"))==0) {
  return 0;
 }

 for (iz = 0; iz < nz; iz++) {
  for (iy = 0; iy < ny; iy++) {
   for (ix = 0; ix < nx; ix++) {
    int idx = iz + nz*(iy + ny*ix);
    fprintf(f,"%12.5E\n",data[idx]);
   }
   fprintf(f,"\n");
  }
 }
 fclose(f);
 return 1;
}

int MyString::DumpIso(const char* out_name, int nx, int ny, int nz, double data[],double lvl)
{
 FILE* f;
 int ix,iy,iz;
 double delta=1E-3;

 if ((f=fopen(out_name,"w"))==0) {
  return 0; }

 for (iz = 0; iz < nz; iz++) {
  for (iy = 0; iy < ny; iy++) {
   for (ix = 0; ix < nx; ix++) {
    int idx = iz + nz*(iy + ny*ix);
    if (fabs(data[idx]-lvl)<=delta) {
     fprintf(f,"%d\t%d\t%d\t%12.5E\n",ix,iy,iz,data[idx]);
    }
   }
  }
  fprintf(f,"\n");
 }
 fclose(f);
 return 1;
}
*/
