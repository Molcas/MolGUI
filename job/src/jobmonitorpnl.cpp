/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/notebook.h>
#include <wx/filename.h>

#include "jobmonitorpnl.h"
//#include "ui/fileeditdlg.h"
//#include "ui/outputfilelistdlg.h"
#include "tools.h"
#include "manager.h"
#include "projectmanager.h"
#include "xmlfile.h"
#include "envutil.h"
#include "sshclient.h"
#include "stringutil.h"
#include "downloadthread.h"
#include <wx/arrimpl.cpp>
#include <wx/intl.h>
#include "dlgmasterkey.h"
#include "dlglogin.h"
#include "dlgfilelist.h"
#include "simuproject.h"

#define NB_PNL_LOCAL_RUNNING_JOBS 0
#define NB_PNL_COMPLETED_JOBS 1
#define NB_PNL_REMOTE_RUNNING_JOBS 2

int CTRL_ID_LB_LOCAL_RUNNING = wxNewId();
int CTRL_ID_LB_REMOTE_RUNNING = wxNewId();
int CTRL_ID_LB_TERMINATED = wxNewId();
int CTRL_ID_LB_KILLED = wxNewId();
int CTRL_ID_NB_LIST = wxNewId();
int TIMER_ID_IDLE = wxNewId();
int TIMER_ID_REMOTEMONITOR = wxNewId();
int CTRL_ID_REMOTE_STATUS = wxNewId();
int CTRL_ID_REMOTE_DOWNLOAD = wxNewId();
int CTRL_ID_REMOTE_KILL = wxNewId();
int CTRL_ID_GRID_JOBLIST = wxNewId();
int CTRL_ID_REMOTE_COMMAND = wxNewId();
int CTRL_ID_RADIOBOX_FREQ = wxNewId();
int POPUPMENU_JOBLIST_REMOVE = wxNewId();
int CTRL_ID_TXT_RMCMD = wxNewId();

BEGIN_EVENT_TABLE(JobMonitorPnl, wxPanel)
    EVT_LISTBOX(CTRL_ID_LB_LOCAL_RUNNING, JobMonitorPnl::OnLbLocalRunningItemClicked)
    //        EVT_LISTBOX(CTRL_ID_LB_REMOTE_RUNNING, JobMonitorPnl::OnLbRemoteRunningItemClicked)
    EVT_LISTBOX(CTRL_ID_LB_TERMINATED, JobMonitorPnl::OnLbTerminatedItemClicked)
    EVT_LISTBOX_DCLICK(CTRL_ID_LB_TERMINATED, JobMonitorPnl::OnLbTerminatedItemDClicked)
    EVT_NOTEBOOK_PAGE_CHANGED(CTRL_ID_NB_LIST, JobMonitorPnl::OnNbList)
    EVT_TIMER(TIMER_ID_IDLE, JobMonitorPnl::OnTimerIdle)
    EVT_TIMER(TIMER_ID_REMOTEMONITOR, JobMonitorPnl::OnTimerRemoteMonitor)
    EVT_BUTTON(CTRL_ID_REMOTE_STATUS, JobMonitorPnl::OnBtnStatus)
    EVT_BUTTON(CTRL_ID_REMOTE_DOWNLOAD, JobMonitorPnl::OnBtnDownload)
    EVT_BUTTON(CTRL_ID_REMOTE_KILL, JobMonitorPnl::OnRemoveJob)
    EVT_BUTTON(CTRL_ID_REMOTE_COMMAND, JobMonitorPnl::OnBtnRemoteCommand)
    EVT_GRID_CMD_CELL_LEFT_CLICK(CTRL_ID_GRID_JOBLIST, JobMonitorPnl::OnJobSelect)
    EVT_GRID_CMD_CELL_RIGHT_CLICK(CTRL_ID_GRID_JOBLIST, JobMonitorPnl::OnPopupMenu)
    EVT_RADIOBOX(CTRL_ID_RADIOBOX_FREQ, JobMonitorPnl::OnFreqSelect)
    EVT_MENU(POPUPMENU_JOBLIST_REMOVE, JobMonitorPnl::OnRemoveJob)
    EVT_MESSAGE_CHANGED(ID_MESSAGE_CHANGE, JobMonitorPnl::OnMessageUpdate)
    EVT_TEXT_ENTER(CTRL_ID_TXT_RMCMD, JobMonitorPnl::OnBtnRemoteCommand)
END_EVENT_TABLE()

static const wxChar *errorText[] = {
    wxT(""), // no error
    wxT("signal not supported"),
    wxT("permission denied"),
    wxT("no such process"),
    wxT("unspecified error"),
};

JobMonitorPnl::JobMonitorPnl(GenericDlg *parent, wxWindowID id) : GenericPnl(parent, id), timerIdleWakeUp(this, TIMER_ID_IDLE) , timerRemoteMonitor(this, TIMER_ID_REMOTEMONITOR) {
    pLbLocalRunning = NULL;
    //        pLbRemoteRunning = NULL;
    pLbTerminated = NULL;
    //pLbKilled = NULL;
    pPnlOutput = NULL;
    procDataHash.clear();
    //threadDataHash.clear();
    pNbList = NULL;
    //selectedProcessId = 0;
    CreateControls();
    m_connected = false;
    m_seljob = -1;
    m_sshclient = new SSHClient();
    m_httppost = NULL;
    m_newjobs = 0;
    m_oldjobs = 0;
    m_busy = false;
    SetBtnStatus(0);
    //   GetRemoteJobInfoList();

}

JobMonitorPnl::~JobMonitorPnl() {
    delete m_sshclient;
    m_sshclient = NULL;
    m_httppost = NULL;
    //delete m_httppost;
}


void JobMonitorPnl::OnActivate(wxActivateEvent &event) {
    //wxMessageBox(wxT("JobMonitorPnl Activate"));
    //fprintf(stderr,"OnActivate");
    //GetRemoteJobInfoList();
}

void JobMonitorPnl::OnFreqSelect(wxCommandEvent &event)
{
    int cursel = 0;
    cursel = m_rBoxFreq->GetSelection();
    switch(cursel) {
    case 0:
        timerRemoteMonitor.Stop();
        break;
    case 1:
        timerRemoteMonitor.Start(60000);
        break;
    case 2:
        timerRemoteMonitor.Start(900000);
        break;
    case 3:
        timerRemoteMonitor.Start(1800000);
        break;
    }
}

void JobMonitorPnl::SetBtnStatus(int status)
{
    switch(status) {
    case 0:
        m_btnstatus->Enable(false);
        m_btndisplayoutput->Enable(false);
        m_btnkill->Enable(false);
        m_btnremotecommand->Enable(false);
        if(m_seljob >= 0)
            if(m_remotejobinfoarray[m_seljob].protocal.Cmp(wxT("HTTP")) != 0)
                m_btnremotecommand->Enable(true);
        break;
    case 1:
        if(m_seljob >= 0)
        {
            if(m_remotejobinfoarray[m_seljob].protocal.Cmp(wxT("HTTP")) == 0)
            {
                m_btnstatus->Enable(true);
                m_btndisplayoutput->Enable(true);
                m_btnkill->Enable(true);
                m_btnremotecommand->Enable(false);
            } else {
                m_btnstatus->Enable(true);
                m_btndisplayoutput->Enable(true);
                m_btnkill->Enable(true);
                m_btnremotecommand->Enable(true);

                for(unsigned i = 0; i < m_downloadcheckarray.GetCount(); i++) {
                    if(m_downloadcheckarray[i].inputfile.Cmp(m_remotejobinfoarray[m_seljob].inputfile) == 0) {
                        m_btnstatus->Enable(false);
                        m_btndisplayoutput->Enable(false);
                        m_btnkill->Enable(false);
                        m_btnremotecommand->Enable(false);
                    }
                }

            }
        }

        break;
    }
}

void JobMonitorPnl::OnJobSelect(wxGridEvent &event)
{
    if(m_busy)return;
    SetBtnStatus(0);
    int oldjob = m_seljob;
    m_seljob = event.GetRow();
    if((unsigned int)m_seljob < m_remotejobinfoarray.GetCount()) {
        m_gridjoblist->SelectRow(m_seljob, false);
        m_connected = false;
        SetBtnStatus(1);
    } else {
        m_seljob = oldjob;
    }
}

void JobMonitorPnl::CreateControls(void) {
    wxBoxSizer *bsToft = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(bsToft);

    pNbList = new wxNotebook(this, CTRL_ID_NB_LIST, wxDefaultPosition, wxSize(200, 260));
    wxPanel *pPnlListLocalRunning = new wxPanel(pNbList, wxID_ANY);
    wxBoxSizer *bsListLocalRunning = new wxBoxSizer(wxVERTICAL);
    pPnlListLocalRunning->SetSizer(bsListLocalRunning);
    wxPanel *pPnlListRemoteRunning = new wxPanel(pNbList, wxID_ANY);
    wxBoxSizer *bsListRemoteRunning = new wxBoxSizer(wxVERTICAL);
    pPnlListRemoteRunning->SetSizer(bsListRemoteRunning);
    wxPanel *pPnlListTerminated = new wxPanel(pNbList, wxID_ANY);
    wxBoxSizer *bsListTerminated = new wxBoxSizer(wxVERTICAL);
    pPnlListTerminated->SetSizer(bsListTerminated);
    //wxPanel* pPnlListKilled = new wxPanel(pNbList, wxID_ANY);
    //wxBoxSizer* bsListKilled  = new wxBoxSizer(wxVERTICAL);
    //pPnlListKilled ->SetSizer(bsListKilled );

    pLbLocalRunning = new wxListBox(pPnlListLocalRunning, CTRL_ID_LB_LOCAL_RUNNING);
    bsListLocalRunning->Add(pLbLocalRunning, 1, wxEXPAND | wxALL, 2);
    //    bsListLocalRunning->Add(pPnlOutput, 1, wxEXPAND|wxALL, 2);
    //        pLbRemoteRunning = new wxListBox(pPnlListRemoteRunning, CTRL_ID_LB_REMOTE_RUNNING);
    //        bsListRemoteRunning->Add(pLbRemoteRunning, 1, wxEXPAND|wxALL, 2);
    m_gridjoblist = new wxGrid( pPnlListRemoteRunning, CTRL_ID_GRID_JOBLIST, wxDefaultPosition, wxDefaultSize, 0 );

    // Grid
    m_gridjoblist->CreateGrid( 4, 5 );
    m_gridjoblist->EnableEditing( false );
    m_gridjoblist->EnableGridLines( true );
    m_gridjoblist->EnableDragGridSize( false );
    m_gridjoblist->SetMargins( 0, 0 );

    // Columns
    m_gridjoblist->SetColSize( 0, 100 );
    m_gridjoblist->SetColSize( 1, 140 );
    m_gridjoblist->SetColSize( 2, 150 );
    m_gridjoblist->SetColSize( 3, 90 );
    m_gridjoblist->SetColSize( 4, 180 );
    m_gridjoblist->EnableDragColMove( false );
    m_gridjoblist->EnableDragColSize( true );
    m_gridjoblist->SetColLabelSize( 30 );
    m_gridjoblist->SetColLabelValue( 0, wxT("Job") );
    m_gridjoblist->SetColLabelValue( 1, wxT("Host") );
    m_gridjoblist->SetColLabelValue( 2, wxT("Submit Date") );
    m_gridjoblist->SetColLabelValue( 3, wxT("Status") );
    m_gridjoblist->SetColLabelValue( 4, wxT("Status Detail") );
    m_gridjoblist->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );

    // Rows
    m_gridjoblist->EnableDragRowSize( true );
    m_gridjoblist->SetRowLabelSize( 0 );
    m_gridjoblist->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
    m_gridjoblist->SetSelectionMode(wxGrid::wxGridSelectRows);
    // Label Appearance

    // Cell Defaults
    m_gridjoblist->SetDefaultCellAlignment( wxALIGN_CENTRE, wxALIGN_TOP );
    m_gridjoblist->SetMinSize( wxSize( 660, 140 ) );

    //bsListRemoteRunning->Add( m_gridjoblist, 0, wxALIGN_CENTER_HORIZONTAL, 2 );

    wxString __wxRadioBoxChoices_1[4] =
    {
        _("Do Nothing"),
        _("Every Minute"),
        _("15 Minutes"),
        _("30 Minutes")
    };
    m_rBoxFreq  = new wxRadioBox(pPnlListRemoteRunning, CTRL_ID_RADIOBOX_FREQ, _("Update Frequency"), wxPoint(-1, -1), wxSize(130, 160), 4, __wxRadioBoxChoices_1, 1, wxRA_SPECIFY_COLS, wxDefaultValidator, _T("rBoxFreq"));
    wxBoxSizer *bsBoxFreq = new wxBoxSizer(wxHORIZONTAL);
    bsBoxFreq->Add(m_gridjoblist, 0, 2);
    bsBoxFreq->Add(m_rBoxFreq, 0, 2);
    bsListRemoteRunning->Add(bsBoxFreq, 0, wxALIGN_CENTER_HORIZONTAL, 2);
    wxBoxSizer *bsRemoteBtn;
    bsRemoteBtn = new wxBoxSizer( wxHORIZONTAL );

    m_btnstatus = new wxButton( pPnlListRemoteRunning, CTRL_ID_REMOTE_STATUS, wxT("Status Detail"), wxDefaultPosition, wxDefaultSize, 0 );
    bsRemoteBtn->Add( m_btnstatus, 0, wxALL, 5 );

    m_btndisplayoutput = new wxButton( pPnlListRemoteRunning, CTRL_ID_REMOTE_DOWNLOAD, wxT("Download Files"), wxDefaultPosition, wxDefaultSize, 0 );
    bsRemoteBtn->Add( m_btndisplayoutput, 0, wxALL, 5 );

    m_btnkill = new wxButton( pPnlListRemoteRunning, CTRL_ID_REMOTE_KILL, wxT("Remove Job"), wxDefaultPosition, wxDefaultSize, 0 );

    bsRemoteBtn->Add( m_btnkill, 0, wxALL, 5 );
    m_btnremotecommand = new wxButton( pPnlListRemoteRunning, CTRL_ID_REMOTE_COMMAND, wxT("Remote Command"), wxDefaultPosition, wxDefaultSize, 0);

    bsRemoteBtn->Add( m_btnremotecommand, 0, wxALL, 5 );
    m_txtremotecommand = new wxTextCtrl( pPnlListRemoteRunning, CTRL_ID_TXT_RMCMD, wxEmptyString, wxDefaultPosition, wxSize(200, -1), wxTE_PROCESS_ENTER  );
    bsRemoteBtn->Add(m_txtremotecommand, 1, wxALL, 5);

    bsListRemoteRunning->Add( bsRemoteBtn, 0, wxALIGN_CENTER_HORIZONTAL | wxFIXED_MINSIZE, 5 );



    pLbTerminated = new wxListBox(pPnlListTerminated, CTRL_ID_LB_TERMINATED);
    bsListTerminated->Add(pLbTerminated, 1, wxEXPAND | wxALL, 2);
    //pLbKilled = new wxListBox(pPnlListKilled, CTRL_ID_LB_KILLED, wxDefaultPosition, wxSize(300,200));
    //bsListKilled->Add(pLbKilled, 1, wxEXPAND|wxALL, 2);

    pNbList->AddPage(pPnlListLocalRunning, wxT("Local Running Jobs"), true);
    pNbList->AddPage(pPnlListTerminated, wxT("Completed Local Jobs"), false);
    pNbList->AddPage(pPnlListRemoteRunning, wxT("Remote Running Jobs"), false);
    //pNbList->AddPage(pPnlListKilled, wxT("Killed Jobs"), false);

    pPnlOutput = new ProcessOutputPanel(this, wxID_ANY);

    bsToft->Add(pNbList, 0, wxEXPAND | wxALL, 2);
    bsToft->Add(pPnlOutput, 0, wxEXPAND | wxALL, 2);
}

bool JobMonitorPnl::OnBtnClose(void) {
    //GetMingMainDialog()->ShowMainUI(true);
    SaveRemoteJobInfo();
    return true;
}

bool JobMonitorPnl::OnBtnKill(void) {
    wxString strProcessId = wxEmptyString;
    ProcessData *procData = GetSelectedItem(pLbLocalRunning);
    //        ProcessData* procData =GetProcessData();
    if(!procData) {
        Tools::ShowStatus(wxT("Please select a running job."), 1);
    } else {
        strProcessId = wxString::Format(wxT("%ld::"), procData->processId);
        wxKillError rc = wxProcess::Kill(procData->processId, (wxSignal)wxSIGKILL, wxKILL_CHILDREN);
        if (rc == wxKILL_OK) {
            Tools::ShowStatus(wxString::Format(wxT("Process %ld killed."), procData->processId), 1);
            pLbLocalRunning->Delete(pLbLocalRunning->GetSelection());
            //pLbKilled->Append(procData->GetInputFile());
            //                        if (pLbLocalRunning->IsEmpty() && timerIdleWakeUp.IsRunning()){
            //                                timerIdleWakeUp.Stop();
            //                        }
            procData->KillProcess();
            Manager::Get()->GetProjectManager()->DoJobTerminated(procData->processId, procData->GetJobRunningData(), 3);
        } else {
            Tools::ShowStatus(wxString::Format(wxT("Failed to kill process %ld: %s"), procData->processId, errorText[rc]), 1);
        }
    }
    return false;
}

void JobMonitorPnl::OnBtnEdit(wxCommandEvent &WXUNUSED(event)) {
    ProcessData *procData = GetSelectedItem(pLbTerminated);
    if(procData) {
        /*FileEditDlg fileEditDlg(this, wxID_ANY);
        fileEditDlg.SetNeedSubmitBtn(true);
        fileEditDlg.SetNeedSaveBtn(true);
        fileEditDlg.CreateControls();
        fileEditDlg.ReadFile(procData->GetInputFile());
        if(fileEditDlg.ShowModal() == wxID_APPLY) {
                int index = pLbTerminated->GetSelection();
                if(index != wxNOT_FOUND) {
                        pLbTerminated->Deselect(index);
                        pLbTerminated->Delete(index);
                        procDataHash.erase(procData->GetInputFile());
                        //delete procData;
                }
                ExecLocalJob(procData->GetInputFile(), procData->GetProjectName(), procData->GetOutputDir());
        }*/
    }
}

void JobMonitorPnl::OnBtnShow(wxCommandEvent &WXUNUSED(event)) {
    /*ProcessData* procData = GetSelectedItem(pLbTerminated);
    if(procData) {
            OutputFileListDlg fileListDlg(this, wxID_ANY);
            fileListDlg.SetOutputFileDir(procData->GetOutputDir(), procData->GetProjectName());
            fileListDlg.ShowModal();
    }else {
            GetMingMainDialog()->ShowStatus(wxT("Please select a terminated job."));
    }*/
}

void JobMonitorPnl::ExecLocalJob(JobRunningData *pJobData) {
    if(pNbList->GetSelection() != NB_PNL_LOCAL_RUNNING_JOBS) {
        pNbList->SetSelection(NB_PNL_LOCAL_RUNNING_JOBS);
    }

    ProcessData *procData = new ProcessData(pJobData);
    //m_procData=procData;
    //Tools::ShowError(procData->GetCommand());
    if(procData->InitData() && procData->RunProcess(this)) {
        // wxMessageBox(this->m_procData->outputFile);
        if (pLbLocalRunning->IsEmpty()) {
            // we want to start getting the timer events to ensure that a
            // steady stream of idle events comes in -- otherwise we
            // wouldn't be able to poll the child process input
            timerIdleWakeUp.Start(500);
        }
        pPnlOutput->Clear();
        pLbLocalRunning->Append(wxString::Format(wxT("%ld::"), procData->processId) + procData->GetCommand());
        pLbLocalRunning->SetSelection(pLbLocalRunning->GetCount() - 1);
        procDataHash[procData->GetCommand()] = procData;
        if(pNbList->GetSelection() == NB_PNL_LOCAL_RUNNING_JOBS) {
            EnableButton(BTN_KILL, pLbLocalRunning->GetSelection() != wxNOT_FOUND);
        }
    }
}

void JobMonitorPnl::ExecRemoteJob(JobRunningData *pJobData) {
    if(pNbList->GetSelection() != NB_PNL_REMOTE_RUNNING_JOBS) {
        pNbList->SetSelection(NB_PNL_REMOTE_RUNNING_JOBS);
    }

    RemoteThreadData *threadData = new RemoteThreadData(pJobData);
    if(threadData->RunThread()) {
        threadDataHash[threadData->GetInputFile()] = threadData;
        //if(!timerRemoteMonitor.IsRunning())timerRemoteMonitor.Start(10000);
    }
}

void JobMonitorPnl::AppendOutput(wxString &msg) {
    pPnlOutput->AppendOutput(msg);
}

void JobMonitorPnl::AppendError(wxString &msg) {
    pPnlOutput->AppendError(msg);
}

void JobMonitorPnl::OnProcessTerminated(ProcessData *procData, int status) {
    wxString processId = wxString::Format(wxT("%ld::"), procData->processId);
    wxArrayString aryProcess = pLbLocalRunning->GetStrings();
    for(unsigned i = 0; i < aryProcess.GetCount(); i++) {
        if(aryProcess[i].Find(processId) == 0) {
            pLbLocalRunning->Delete(i);
            if(pLbTerminated->FindString(procData->GetCommand()) == wxNOT_FOUND) {
                pLbTerminated->Append(procData->GetCommand());
            }
            pLbTerminated->SetSelection(pLbTerminated->FindString(procData->GetCommand()));
            if(pLbLocalRunning->IsEmpty() && IsShown() && pNbList->GetSelection() != NB_PNL_COMPLETED_JOBS) {
                // when there are no any running jobs, show completed jobs automatically
                pNbList->SetSelection(NB_PNL_COMPLETED_JOBS);
            }
            Manager::Get()->GetProjectManager()->DoJobTerminated(procData->processId, procData->GetJobRunningData(), status);
            break;
        }
    }
    if (pLbLocalRunning->IsEmpty()) {
        //        timerIdleWakeUp.Stop();
    }
}
//*original code noted by xhy
void JobMonitorPnl::OnTimerIdle(wxTimerEvent &WXUNUSED(event)) {
    wxWakeUpIdle();
    //OnSshDownFinished();
    //fprintf(stderr,"OnTimerIdle");
    ProcessData *procData = GetSelectedItem(pLbLocalRunning);
    //wxMessageBox(wxT("hello"));
    if(procData) {
        procData->DumpOutput(false);
    }
}
//*/
/*add by xia
  ProcessData* JobMonitorPnl::GetProcessData(void) {
        return m_procData;
}

void JobMonitorPnl::OnTimerIdle(wxTimerEvent& WXUNUSED(event)) {
        wxWakeUpIdle();
        ProcessData* procData =GetProcessData();
//        ProcessData* procData = GetSelectedItem(pLbLocalRunning);
        if(procData) {
                procData->DumpOutput(false);

        }
}

//end
*/
void JobMonitorPnl::OnLbLocalRunningItemClicked(wxCommandEvent &WXUNUSED(event)) {
    EnableButton(BTN_KILL, pLbLocalRunning->GetSelection() != wxNOT_FOUND);
}

void JobMonitorPnl::OnLbTerminatedItemClicked(wxCommandEvent &WXUNUSED(event)) {
    /*wxWindow* win = FindWindow(CTRL_ID_BTN_SHOW);
    if(win) {
            win->Enable(pLbTerminated->GetSelection() != wxNOT_FOUND);
    }
    win = FindWindow(CTRL_ID_BTN_EDIT);
    if(win) {
            win->Enable(pLbTerminated->GetSelection() != wxNOT_FOUND);
    }*/
    ShowTerminatedResult();
}

void JobMonitorPnl::OnLbTerminatedItemDClicked(wxCommandEvent &event) {
    OnBtnShow(event);
}

void JobMonitorPnl::OnNbList(wxNotebookEvent &event) {
    /*wxWindow* win = FindWindow(CTRL_ID_BTN_SHOW);
    if(win) {
            win->Enable(event.GetSelection() == NB_PNL_COMPLETED_JOBS && pLbTerminated->GetSelection() != wxNOT_FOUND);
    }
    win = FindWindow(CTRL_ID_BTN_EDIT);
    if(win) {
            win->Enable(event.GetSelection() == NB_PNL_COMPLETED_JOBS && pLbTerminated->GetSelection() != wxNOT_FOUND);
    }*/
    EnableButton(BTN_KILL, (event.GetSelection() == NB_PNL_LOCAL_RUNNING_JOBS && pLbLocalRunning->GetSelection() != wxNOT_FOUND));
    //|| (event.GetSelection() == NB_PNL_REMOTE_RUNNING_JOBS));  // && pLbRemoteRunning->GetSelection() != wxNOT_FOUND
    if(event.GetSelection() == NB_PNL_LOCAL_RUNNING_JOBS) {
        if(!timerIdleWakeUp.IsRunning()) {
            timerIdleWakeUp.Start(500);
        }
        //ShowTerminatedResult();
    } else if(event.GetSelection() == NB_PNL_REMOTE_RUNNING_JOBS) {
        GetRemoteJobInfoList();
    } else {
        if (pLbLocalRunning->IsEmpty()) {
            //                        if(timerIdleWakeUp.IsRunning()) {
            //                        timerIdleWakeUp.Stop();
            //        }
        } else {
            if(!timerIdleWakeUp.IsRunning()) {
                timerIdleWakeUp.Start(500);
            }
        }
    }
}

bool JobMonitorPnl::IsClosable(void) {
    if (!pLbLocalRunning->IsEmpty()) { //|| !pLbRemoteRunning->IsEmpty()){
        Tools::ShowStatus(wxT("Some jobs are still running."), 1);
        return false;
    }
    return true;
}

void JobMonitorPnl::ShowTerminatedResult(void) {
    ProcessData *procData = GetSelectedItem(pLbTerminated);
    if(procData) {
        pPnlOutput->LoadFiles(procData->outputFile, procData->errorFile);
    }
}

ProcessData *JobMonitorPnl::GetSelectedItem(wxListBox *listBox) {
    if(listBox->GetSelection() == wxNOT_FOUND)
        return NULL;

    wxString inputFile = listBox->GetString(listBox->GetSelection());
    if(listBox == pLbLocalRunning) {
        int pos = inputFile.Find(wxT("::"));
        if(pos > 0) {
            inputFile = inputFile.SubString(pos + 2, inputFile.Len());
        }
    }
    ProcessDataHash::iterator ite = procDataHash.begin();
    while(ite != procDataHash.end()) {
        //ite->first;
        wxString tmpstr = ite->first;
        //wxMessageBox(tmpstr);
        if(tmpstr.Cmp(inputFile) == 0) {
            return  ite->second;
        }
        ite++;
    }
    return NULL;
}

bool JobMonitorPnl::OnButton(long style) {
    if(style == BTN_KILL) {
        return OnBtnKill();
    } else if(style == BTN_CLOSE) {
        return OnBtnClose();
    }
    return true;
}

void JobMonitorPnl::ExecJob(JobRunningData *pJobData)
{
    long remotejob = 0;
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
    if(wxFileExists(fileName)) {
        wxString strLine;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        strLine = tis.ReadLine();
        strLine.ToLong(&remotejob);
    }

    if(remotejob == 1)
    {
        ExecRemoteJob(pJobData);
    }
    else
    {
        ExecLocalJob(pJobData);
    }

}

void JobMonitorPnl::OnBtnRemoteCommand(wxCommandEvent &event) {
    if (m_seljob == -1)return;
    if(m_busy)return;
    m_busy = true;
    SetBtnStatus(0);
    //    wxMessageBox(m_remotejobinfoarray[m_seljob].fullname);
    //    wxMessageBox(m_remotejobinfoarray[m_seljob].username);
    //    wxMessageBox(m_remotejobinfoarray[m_seljob].password);
    char *input = NULL;

    wxString strerr = wxT("Remote Command Execute Error!");

    if(!m_connected) {
        EnvUtil::SetMasterId(m_remotejobinfoarray[m_seljob].masterid);
        if(m_remotejobinfoarray[m_seljob].masterkey0.IsEmpty()) {

            while(EnvUtil::GetMasterKey().IsEmpty() || m_remotejobinfoarray[m_seljob].masterkey.Cmp(EnvUtil::GetMasterKeyMD5()) != 0) {
                DlgMasterKey *pdlgmasterkey = new DlgMasterKey(this);
                pdlgmasterkey->SetHostname(m_remotejobinfoarray[m_seljob].masterid);
                if(pdlgmasterkey->ShowModal() == wxID_CANCEL)
                {
                    SetBtnStatus(1);
                    m_busy = false;
                    return;
                }
                pdlgmasterkey = NULL;
            }

            m_remotejobinfoarray[m_seljob].masterkey0 = EnvUtil::GetMasterKey();
        }

        m_sshclient->SetMasterkey(m_remotejobinfoarray[m_seljob].masterkey0);
        //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[m_seljob].fullname);
        m_sshclient->SetHostname(m_remotejobinfoarray[m_seljob].fullname);
        //fprintf(stderr, m_machineinfoarray[selmachine].username);
        //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[m_seljob].username);

        m_sshclient->SetUsername(m_remotejobinfoarray[m_seljob].username);
        //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[m_seljob].password);

        //fprintf(stderr, m_machineinfoarray[selmachine].password);
        m_sshclient->SetPassword(m_remotejobinfoarray[m_seljob].password);
        while(m_sshclient->ConnectToHost() != 1) {
            DlgLogin *dlglogin = new DlgLogin(this);
            dlglogin->SetInfo(m_sshclient);
            dlglogin->ShowModal();
            dlglogin = NULL;
            if(!m_sshclient->GetRetry()) {
                wxMessageBox(wxT("Connect Failed!"));
                SetBtnStatus(1);
                m_busy = false;
                return;
            } else {
                m_remotejobinfoarray[m_seljob].masterkey = m_sshclient->GetMasterkey();
            }
        }

        m_connected = true;
    }
    wxString remoteDir = wxEmptyString;
    if(!(m_remotejobinfoarray[m_seljob].remoteworkbase.Trim()).IsEmpty()) {
        remoteDir += m_remotejobinfoarray[m_seljob].remoteworkbase.Trim() + wxT("/") + m_remotejobinfoarray[m_seljob].ticket;
    } else {
        remoteDir += m_remotejobinfoarray[m_seljob].ticket;
    }
    StringUtil::String2CharPointer(&input, wxT("cd ")+remoteDir+wxT("; ") + m_txtremotecommand->GetValue() + wxT(" 1>exec.log 2>&1" ));
    if(m_sshclient->RemoteExec(input, strlen(input), NULL) == 1) {
        wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("exec.log");
        if(wxFileExists(fileName))wxRemoveFile(fileName);
        // StringUtil::String2CharPointer(&input, fileName);
        wxString remoteFile = remoteDir + wxT("/exec.log");
        if(m_sshclient->Download(fileName.fn_str(), remoteFile.fn_str()) == -1)
        {
            AppendOutput(strerr);
        }
        else
        {
            strerr = wxEmptyString;
            pPnlOutput->LoadFiles(fileName, strerr);
        }
    } else {
        AppendOutput(strerr);
    }
    SetBtnStatus(1);
    m_busy = false;
    return;

}

void JobMonitorPnl::OnBtnStatus(wxCommandEvent &event) {
    DoCheckStatus(m_seljob);
}

void JobMonitorPnl::DoCheckStatus(int currow) {
    wxString msg = wxEmptyString;
    if (currow == -1)return;
    if(m_remotejobinfoarray[currow].protocal.Cmp(wxT("HTTP")) == 0) {
        if(m_httppost->IsBusy() >= 0)return;
        m_httppost->SetBusy(currow);
        SetBtnStatus(0);
        m_httppost->SetServer(m_remotejobinfoarray[currow].fullname);
        m_httppost->SetId(m_remotejobinfoarray[currow].username);
        m_httppost->SetTicket(m_remotejobinfoarray[currow].scriptfile);
        m_httppost->Check();
        m_remotejobinfoarray[currow].status = m_httppost->GetStatus();
        msg = m_remotejobinfoarray[currow].jobname + wxT(" status: ") + m_remotejobinfoarray[currow].status;
        m_gridjoblist->SetCellValue(currow, 3, m_remotejobinfoarray[currow].status);
        m_httppost->SetBusy(-1);
    } else {
        if(m_busy)return;
        m_busy = true;
        SetBtnStatus(0);

        //char *input=NULL;
        char outstr[128];
        char *remotepath = NULL;
        char *jobname = NULL;

        if(!m_connected && m_seljob >= 0 && m_seljob < (int)m_remotejobinfoarray.GetCount() ) {
            EnvUtil::SetMasterId(m_remotejobinfoarray[m_seljob].masterid);
            if(m_remotejobinfoarray[m_seljob].masterkey0.IsEmpty()) {

                while(EnvUtil::GetMasterKey().IsEmpty() || m_remotejobinfoarray[m_seljob].masterkey.Cmp(EnvUtil::GetMasterKeyMD5()) != 0) {
                    DlgMasterKey *pdlgmasterkey = new DlgMasterKey(this);
                    pdlgmasterkey->SetHostname(m_remotejobinfoarray[m_seljob].masterid);
                    if(pdlgmasterkey->ShowModal() == wxID_CANCEL)
                    {
                        SetBtnStatus(1);
                        m_busy = false;
                        return;
                    }
                    pdlgmasterkey = NULL;
                }

                m_remotejobinfoarray[m_seljob].masterkey0 = EnvUtil::GetMasterKey();
            }

            m_sshclient->SetMasterkey(m_remotejobinfoarray[m_seljob].masterkey0);
            m_sshclient->SetHostname(m_remotejobinfoarray[currow].fullname);
            //fprintf(stderr, m_machineinfoarray[selmachine].username);
            //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[currow].username);

            m_sshclient->SetUsername(m_remotejobinfoarray[currow].username);
            //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[currow].password);

            //fprintf(stderr, m_machineinfoarray[selmachine].password);
            m_sshclient->SetPassword(m_remotejobinfoarray[currow].password);
            // wxMessageBox(wxT("Begin Connecting"));

            while(m_sshclient->ConnectToHost() != 1) {
                DlgLogin *dlglogin = new DlgLogin(this);
                dlglogin->SetInfo(m_sshclient);
                dlglogin->ShowModal();
                dlglogin = NULL;
                if(!m_sshclient->GetRetry()) {
                    wxMessageBox(wxT("Connect Failed!"));
                    SetBtnStatus(1);
                    m_busy = false;
                    return;
                } else {
                    m_remotejobinfoarray[currow].masterkey = m_sshclient->GetMasterkey();
                }
            }
            m_connected = true;
        }

        wxString strremotedir = wxEmptyString;
        if(!(m_remotejobinfoarray[currow].remoteworkbase.Trim()).IsEmpty()) {
            strremotedir = m_remotejobinfoarray[currow].remoteworkbase.Trim() + wxT("/") + m_remotejobinfoarray[currow].ticket;
        } else {
            strremotedir = m_remotejobinfoarray[currow].ticket;
        }
        if(m_remotejobinfoarray[currow].projecttype.Cmp(wxT("Molcas")) == 0) {
            StringUtil::String2CharPointer(&remotepath, strremotedir);
            StringUtil::String2CharPointer(&jobname, m_remotejobinfoarray[currow].jobname);
            memset(outstr, 0x0, sizeof(outstr));
            strcat(outstr, "ls ");
            strcat(outstr, remotepath);
            wxArrayString arystrout = m_sshclient->GetOutputInfo(outstr);
            if(arystrout.GetCount() > 0 && arystrout[0].Find(wxT("No such file or directory")) != wxNOT_FOUND)
            {
                wxString status = wxT("Not Found");
                m_remotejobinfoarray[currow].status = status;
                msg = m_remotejobinfoarray[currow].jobname + wxT(" status: ") + status;
                pPnlOutput->AppendOutput(msg);
                m_gridjoblist->SetCellValue(currow, 3, status);
                m_gridjoblist->SetCellValue(currow, 4, arystrout[0]);
                m_busy = false;
                SetBtnStatus(1);
                return;
            }
            strcat(outstr, "/");
            strcat(outstr, jobname);
            strcat(outstr, ".status");
            arystrout = m_sshclient->GetOutputInfo(outstr);
            if(arystrout.GetCount() > 0 && arystrout[0].Find(wxT("No such file or directory")) == wxNOT_FOUND)
            {
                memset(outstr, 0x0, sizeof(outstr));
                strcat(outstr, "tail ");
                strcat(outstr, remotepath);
                strcat(outstr, "/");
                strcat(outstr, jobname);
                strcat(outstr, ".status");
                wxArrayString arystrout = m_sshclient->GetOutputInfo(outstr);
                //for(unsigned int i=0;i<arystrout.GetCount();i++)
                msg = m_remotejobinfoarray[currow].jobname + wxT(" status: ") + arystrout[0];
                pPnlOutput->AppendOutput(msg);

                m_gridjoblist->SetCellValue(currow, 4, arystrout[0]);
                if(arystrout[0].Find(wxT("Happy landing")) != wxNOT_FOUND) {
                    m_remotejobinfoarray[currow].status = wxT("Completed");

                } else {
                    m_remotejobinfoarray[currow].status = wxT("Running");
                }
                m_gridjoblist->SetCellValue(currow, 3, m_remotejobinfoarray[currow].status);
            }
            else
            {
                memset(outstr, 0x0, sizeof(outstr));
                strcat(outstr, "tail ~/simu.log");
                wxArrayString arystrout = m_sshclient->GetOutputInfo(outstr);
                if(arystrout.GetCount() > 0) {
                    m_remotejobinfoarray[currow].status = wxT("Queue Wait");
                    m_gridjoblist->SetCellValue(currow, 3, m_remotejobinfoarray[currow].status);
                    msg = m_remotejobinfoarray[currow].jobname + wxT(" status: ");
                    pPnlOutput->AppendOutput(msg);
                    for(unsigned int i = 0; i < arystrout.GetCount(); i++) {
                        pPnlOutput->AppendOutput(arystrout[i]);
                        msg = msg + wxT(" ") + arystrout[i];
                    }
                } else {
                    m_gridjoblist->SetCellValue(currow, 3, wxT("Submit Failed"));
                    msg = msg + wxT("Submit Failed");
                }
            }
        }
        else if(m_remotejobinfoarray[currow].projecttype.Cmp(wxT("QChem")) == 0)
        {
            StringUtil::String2CharPointer(&remotepath, strremotedir);
            StringUtil::String2CharPointer(&jobname, m_remotejobinfoarray[currow].jobname);
            memset(outstr, 0x0, sizeof(outstr));
            strcat(outstr, "ls ");
            strcat(outstr, remotepath);
            strcat(outstr, "/");
            strcat(outstr, jobname);
            strcat(outstr, ".out");
            wxArrayString arystrout = m_sshclient->GetOutputInfo(outstr);
            //    wxMessageBox(arystrout[0]);
            if(arystrout.GetCount() > 0 && arystrout[0].Find(wxT("No such file or directory")) == wxNOT_FOUND)
            {
                memset(outstr, 0x0, sizeof(outstr));
                strcat(outstr, "tail ");
                strcat(outstr, remotepath);
                strcat(outstr, "/");
                strcat(outstr, jobname);
                strcat(outstr, ".out");
                wxArrayString arystrout = m_sshclient->GetOutputInfo(outstr);
                //for(unsigned int i=0;i<arystrout.GetCount();i++)
                msg = m_remotejobinfoarray[currow].jobname + wxT(" status: ") + arystrout[0];
                pPnlOutput->AppendOutput(msg);
                bool bstatus = false;
                for(unsigned int i = 0; i < arystrout.GetCount(); i++) {
                    if(arystrout[i].Find(wxT("Have a nice day")) != wxNOT_FOUND || arystrout[i].Find(wxT("Q-Chem fatal error")) != wxNOT_FOUND) {
                        m_remotejobinfoarray[currow].status = wxT("Completed");
                        msg = msg + wxT("Completed");
                        bstatus = true;
                        break;
                    }
                }
                if(!bstatus) {
                    m_remotejobinfoarray[currow].status = wxT("Running");
                    msg = msg + wxT("Running");
                }
                m_gridjoblist->SetCellValue(currow, 3, m_remotejobinfoarray[currow].status);

            } else
            {
                memset(outstr, 0x0, sizeof(outstr));
                strcat(outstr, "tail ~/simu.log");
                wxArrayString arystrout = m_sshclient->GetOutputInfo(outstr);
                if(arystrout.GetCount() > 0) {
                    m_remotejobinfoarray[currow].status = wxT("Queue Wait");
                    m_gridjoblist->SetCellValue(currow, 3, m_remotejobinfoarray[currow].status);
                    wxString msg = m_remotejobinfoarray[currow].jobname + wxT(" status: ");
                    AppendOutput(msg);
                    for(unsigned int i = 0; i < arystrout.GetCount(); i++) {
                        pPnlOutput->AppendOutput(arystrout[i]);
                        msg = msg + wxT(" ") + arystrout[i];
                    }
                } else {
                    m_gridjoblist->SetCellValue(currow, 3, wxT("Submit Failed"));
                    msg = msg + wxT("Submit Failed");
                }
            }

        } else if(m_remotejobinfoarray[currow].projecttype.Cmp(wxT("TeraChem")) == 0)
        {

        }
    }
    Tools::ShowStatus(msg, 1);
    m_busy = false;
    SetBtnStatus(1);
    return;
}

void JobMonitorPnl::OnBtnDownload(wxCommandEvent &event) {
    //Manager::Get()->GetProjectManager()->DoRemoteJobTerminated(m_remotejobinfoarray[m_seljob].projectfile,m_remotejobinfoarray[m_seljob].jobpath,1);
    DownloadResult(m_seljob);
}

void JobMonitorPnl::OnBtnRemoteKill(wxCommandEvent &event) {

}

void JobMonitorPnl::GetRemoteJobInfoList(void)
{
#ifdef __DEBUG__
    fprintf(stderr, "GetRemoteJobInfoList!\n");
#endif
    m_remotejobinfoarray.Empty();
    RemoteJobInfo rjobinfo = RemoteJobInfo();
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("remotejob.lst");
    JobFile::LoadFile(fileName, m_remotejobinfoarray);

    //    wxString strout=wxString::Format("%d",m_gridjoblist->GetNumberRows());
    //   wxMessageBox(strout);
    //    strout=wxString::Format("%d",m_remotejobinfoarray.GetCount());
    //   wxMessageBox(strout);
    m_gridjoblist->ClearGrid();
    m_oldjobs = m_remotejobinfoarray.GetCount();
    int dr = m_oldjobs - m_gridjoblist->GetNumberRows();
    if(dr > 0)m_gridjoblist->AppendRows(dr, true);
    for(unsigned int i = 0; i < m_remotejobinfoarray.GetCount(); i++)
    {
        m_gridjoblist->SetCellValue(i, 0, m_remotejobinfoarray[i].jobname);
        m_gridjoblist->SetCellValue(i, 1, m_remotejobinfoarray[i].masterid + wxT(": ") + m_remotejobinfoarray[i].shortname);
        m_gridjoblist->SetCellValue(i, 2, m_remotejobinfoarray[i].submittime);
        m_gridjoblist->SetCellValue(i, 3, m_remotejobinfoarray[i].status);
    }
    pNbList->Enable(true);
}

void JobMonitorPnl::SaveRemoteJobInfo(void) {
#ifdef __DEBUG__
    fprintf(stderr, "JobMonitorPnl::saveremotejobinfo");
#endif
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("remotejob.lst");
    JobFile::SaveFile(fileName, m_remotejobinfoarray);
}

void JobMonitorPnl::DownloadResult(int currow) {
    if (currow == -1)return;
    if(m_remotejobinfoarray[currow].protocal.Cmp(wxT("HTTP")) == 0) {
        if(m_httppost->IsBusy() >= 0)return;
        m_httppost = new HttpPost();
        m_httppost->SetServer(m_remotejobinfoarray[currow].fullname);
        m_httppost->SetId(m_remotejobinfoarray[currow].username);
        m_httppost->SetTicket(m_remotejobinfoarray[currow].scriptfile);
        m_httppost->Check();
        wxString strstatus = m_httppost->GetStatus();
        m_gridjoblist->SetCellValue(currow, 3, strstatus);
        if (strstatus.Cmp(wxT("Finished")) == 0) {
            m_httppost->SetBusy(currow);
            m_httppost->SetJobmonitorpnl(this);
            m_httppost->GetDownList(m_remotejobinfoarray[currow].jobpath + platform::PathSep());

            m_arydownload.Empty();
            if (m_aryrfiles.GetCount() <= 0) {
                wxMessageBox(wxT("Can't get file list!"));
                SetBtnStatus(1);
                m_busy = false;
                return;
            }
            DlgFileList *dlgfilelist = new DlgFileList(Manager::Get()->GetAppWindow());
            dlgfilelist->SetFiles(m_aryrfiles);
            dlgfilelist->SetJobMonitor(this);
            dlgfilelist->CreateGUIControls();
            if(dlgfilelist->ShowModal() == wxID_CANCEL) {
                SetBtnStatus(1);
                m_httppost->SetBusy(-1);
                return;
            }
            wxArrayString arylfiles, aryrfiles;
            for(unsigned int i = 0; i < m_arydownload.GetCount(); i++)
            {
                wxString localfile = m_arylfiles[m_arydownload[i]];
                wxString remotefile = m_aryrfiles[m_arydownload[i]];
                arylfiles.Add(localfile);
                aryrfiles.Add(remotefile);
            }
            m_httppost->SetAryDownlist(arylfiles, aryrfiles);
            m_httppost->DownLoad();
        }
    } else {
        if(m_busy)return;
        m_busy = true;
        SetBtnStatus(0);

        //char *input=NULL;
        char outstr[128];
        char *remotepath = NULL;

        if(!m_connected && m_seljob >= 0) {
            EnvUtil::SetMasterId(m_remotejobinfoarray[m_seljob].masterid);
            if(m_remotejobinfoarray[m_seljob].masterkey0.IsEmpty()) {

                while(EnvUtil::GetMasterKey().IsEmpty() || m_remotejobinfoarray[m_seljob].masterkey.Cmp(EnvUtil::GetMasterKeyMD5()) != 0) {
                    DlgMasterKey *pdlgmasterkey = new DlgMasterKey(this);
                    pdlgmasterkey->SetHostname(m_remotejobinfoarray[m_seljob].masterid);
                    if(pdlgmasterkey->ShowModal() == wxID_CANCEL)
                    {
                        SetBtnStatus(1);
                        m_busy = false;
                        return;
                    }
                    pdlgmasterkey = NULL;
                }

                m_remotejobinfoarray[m_seljob].masterkey0 = EnvUtil::GetMasterKey();
            }

            m_sshclient->SetMasterkey(m_remotejobinfoarray[m_seljob].masterkey0);
            //wxMessageBox(wxT("connecting"));
            //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[currow].fullname);
            //m_sshclient->SetMasterkey(m_remotejobinfoarray[currow].masterkey);
            m_sshclient->SetHostname(m_remotejobinfoarray[currow].fullname);
            //fprintf(stderr, m_machineinfoarray[selmachine].username);
            //   StringUtil::String2CharPointer(&input,m_remotejobinfoarray[currow].username);

            m_sshclient->SetUsername(m_remotejobinfoarray[currow].username);
            //  StringUtil::String2CharPointer(&input,m_remotejobinfoarray[currow].password);

            //fprintf(stderr, m_machineinfoarray[selmachine].password);
            m_sshclient->SetPassword(m_remotejobinfoarray[currow].password);
            // wxMessageBox(wxT("Begin Connecting"));

            while(m_sshclient->ConnectToHost() != 1) {
                DlgLogin *dlglogin = new DlgLogin(this);
                dlglogin->SetInfo(m_sshclient);
                dlglogin->ShowModal();
                dlglogin = NULL;
                if(!m_sshclient->GetRetry()) {
                    wxMessageBox(wxT("Connect Failed!"));
                    SetBtnStatus(1);
                    m_busy = false;
                    return;
                } else {
                    m_remotejobinfoarray[currow].masterkey = m_sshclient->GetMasterkey();
                }
            }
            m_connected = true;
        }

        wxString strremotedir = wxEmptyString;
        if(!(m_remotejobinfoarray[currow].remoteworkbase.Trim()).IsEmpty()) {
            strremotedir = m_remotejobinfoarray[currow].remoteworkbase.Trim() + wxT("/") + m_remotejobinfoarray[currow].ticket;
        } else {
            strremotedir = m_remotejobinfoarray[currow].ticket;
        }

        StringUtil::String2CharPointer(&remotepath, strremotedir);
        memset(outstr, 0x0, sizeof(outstr));
        strcat(outstr, "ls -Rl ");
        strcat(outstr, remotepath);
        wxArrayString fileList = m_sshclient->GetOutputInfo(outstr);

        /* Exemple of output:
        RemoteJobs/remJob320141014144316:
        total 16
        -rw-rw-r-- 1 alextek alextek 1077 okt 14 14:43 MyProject.xyz
        drwxr-xr-x 2 alextek alextek 4096 okt 14 14:43 remJob3
        -rw-rw-r-- 1 alextek alextek   56 okt 14 14:43 remJob3.input
        -rw-rw-r-- 1 alextek alextek   15 okt 14 14:43 remJob3.status

        RemoteJobs/remJob320141014144316/remJob3:
        total 16124
        -rw-rw-r-- 1 alextek alextek 16005653 okt 14 14:43 remJob3.grid
        -rw-rw-r-- 1 alextek alextek    61598 okt 14 14:43 remJob3.GssOrb
        -rw-rw-r-- 1 alextek alextek   133876 okt 14 14:43 remJob3.guessorb.molden
        -rw-rw-r-- 1 alextek alextek    91153 okt 14 14:43 remJob3.LprOrb
        -rw-rw-r-- 1 alextek alextek   134464 okt 14 14:43 remJob3.scf.molden
        -rw-rw-r-- 1 alextek alextek    61597 okt 14 14:43 remJob3.ScfOrb
        -rw-rw-r-- 1 alextek alextek     7505 okt 14 14:43 xmldump
        */
       
        wxArrayString arystrout;
        wxString directory = wxEmptyString;
        // Filter files
        // Add directories if needed as libssh2_scp_rcv doesn't read directories
        for(unsigned int i = 0; i < fileList.GetCount(); i++) {
            // Discard input files
            if( fileList[i].EndsWith(wxT(".input")))
                continue;
            // Discard input files
            else if(fileList[i].EndsWith(wxT(".in")))
                continue;
            // Discard empty lines
            else if(fileList[i].IsEmpty())
                continue;
            // Discard total lines
            else if(fileList[i].StartsWith(wxT("total")))
                continue;
            // Manage directories
            else if(fileList[i].EndsWith(wxT(":")))
            {
                // Discard the remote working base directory
                // Treat only sub-directories
                if(fileList[i].BeforeLast(':').Cmp(strremotedir) != 0)
                {
                    wxFileName dirname = fileList[i].BeforeLast(':');
                    directory = dirname.GetName()+wxT("/");
                }
                continue;
            }
            // Discard non-regular files (like directories)
            else if( !fileList[i].StartsWith(wxT("-")) )
                continue;
            wxString fileName = fileList[i].AfterLast(' ');
            arystrout.Add(directory+fileName);
        }
        if (arystrout.GetCount() <= 0)
        {
            wxMessageBox(wxT("No files available"),wxT("Remote Job Error"),wxOK|wxICON_ERROR);
            SetBtnStatus(1);
            m_busy = false;
            return;
        }
        wxArrayString aryinfo;
        aryinfo.Add(m_remotejobinfoarray[currow].fullname);
        aryinfo.Add(m_remotejobinfoarray[currow].username);
        aryinfo.Add(m_remotejobinfoarray[currow].password);

        m_arydownload.Empty();
        DlgFileList *dlgfilelist = new DlgFileList(this);
        dlgfilelist->SetFiles(arystrout);
        dlgfilelist->SetJobMonitor(this);
        dlgfilelist->CreateGUIControls();
        if(dlgfilelist->ShowModal() == wxID_CANCEL) {
            SetBtnStatus(1);
            m_busy = false;
            return;
        }
        m_arylfiles.Empty();
        m_aryrfiles.Empty();
        for(unsigned int i = 0; i < m_arydownload.GetCount(); i++)
        {
            wxString localfile = m_remotejobinfoarray[currow].jobpath + platform::PathSep() + arystrout[m_arydownload[i]];
            wxString remotefile = strremotedir + wxT("/") + arystrout[m_arydownload[i]];
            m_arylfiles.Add(localfile);
            m_aryrfiles.Add(remotefile);
        }

        SSHClient *sshclient = new SSHClient();
        EnvUtil::SetMasterId(m_remotejobinfoarray[currow].masterid);
        sshclient->SetMasterkey(m_remotejobinfoarray[currow].masterkey0);
        sshclient->SetHostname(m_remotejobinfoarray[currow].fullname);
        //fprintf(stderr, m_machineinfoarray[selmachine].username);
        //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[currow].username);

        sshclient->SetUsername(m_remotejobinfoarray[currow].username);
        //StringUtil::String2CharPointer(&input,m_remotejobinfoarray[currow].password);

        //fprintf(stderr, m_machineinfoarray[selmachine].password);
        sshclient->SetPassword(m_remotejobinfoarray[currow].password);
        // wxMessageBox(wxT("Begin Connecting"));

        while(sshclient->ConnectToHost() != 1) {
            DlgLogin *dlglogin = new DlgLogin(this);
            dlglogin->SetInfo(sshclient);
            dlglogin->ShowModal();
            dlglogin = NULL;
            if(!sshclient->GetRetry()) {
                wxMessageBox(wxT("Connect Failed!"));
                m_busy = false;
                SetBtnStatus(1);
                return;
            } else {
                m_remotejobinfoarray[currow].masterkey0 = sshclient->GetMasterkey();
            }
        }

        DownloadThreadData *threadData = new DownloadThreadData(aryinfo, m_arylfiles, m_aryrfiles, this, sshclient); //m_sshclient,arylfiles,aryrfiles);
        if(threadData->RunThread()) {
            wxString msg = wxT("Download Start......!");
            pPnlOutput->AppendOutput(msg);
            DownloadCheck downloadcheck;
            downloadcheck.checkfile = m_arylfiles[m_arylfiles.GetCount() - 1];
            downloadcheck.inputfile = m_remotejobinfoarray[currow].inputfile;
            m_downloadcheckarray.Add(downloadcheck);
            //if(!timerRemoteMonitor.IsRunning())timerRemoteMonitor.Start(10000);
        }
    }
    m_busy = false;
    SetBtnStatus(1);
    return;
}

void JobMonitorPnl::OnTimerRemoteMonitor(wxTimerEvent &WXUNUSED(event)) {
    wxWakeUpIdle();
#ifdef __DEBUG__
    fprintf(stderr, "TimerRemoteMonitor\n");
#endif
    /*        if(m_newjobs>0){
                wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("remotejob.lst");
                XmlFile remotejob(wxT("remotejob-information"),fileName);
                XmlElementList * root=remotejob.GetRoot();
                int dj=root->elem.subElemCount-m_oldjobs;
                if(dj>0){
                GetRemoteJobInfoList();
                    m_newjobs=m_newjobs-dj;
            }
         }
    */
    for(unsigned k = 0; k < m_remotejobinfoarray.GetCount(); k++) {
        DoCheckStatus(k);
    }

    //OnHttpDownFinished();

}

void JobMonitorPnl::OnSshDownFinished() {
    for(unsigned i = 0; i < m_downloadcheckarray.GetCount(); i++) {
        if(wxFileExists(m_downloadcheckarray[i].checkfile)) {
            for(unsigned j = 0; j < m_remotejobinfoarray.GetCount(); j++) {
                if(m_downloadcheckarray[i].inputfile.Cmp(m_remotejobinfoarray[j].inputfile) == 0) {
                    //  wxMessageBox(m_remotejobinfoarray[j].inputfile);
                    Manager::Get()->GetProjectManager()->DoRemoteJobTerminated(m_remotejobinfoarray[j].projectfile, m_remotejobinfoarray[j].jobpath, 0);
                    m_downloadcheckarray.RemoveAt(i);
                    //DoRemoveJob(j);
                }
            }
        }
    }
    return;
}

void JobMonitorPnl::OnHttpDownFinished() {
    if(m_httppost->IsBusy() >= 0) {
        if(m_httppost->IsDownLoadFinished()) {
            Manager::Get()->GetProjectManager()->DoRemoteJobTerminated(m_remotejobinfoarray[m_httppost->IsBusy()].projectfile, m_remotejobinfoarray[m_httppost->IsBusy()].jobpath, 0);
            //  DoRemoveJob(m_httppost->IsBusy());
            m_httppost->SetBusy(-1);
        }
    }
    return;
}

void JobMonitorPnl::DoRemoveJob(unsigned int currow) {
    if(currow == (unsigned int) - 1)return;
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("remotejob.lst");
    m_remotejobinfoarray.RemoveAt(currow);
    SaveRemoteJobInfo();
    GetRemoteJobInfoList();
    m_seljob = -1;
}

void JobMonitorPnl::OnPopupMenu(wxGridEvent &event)
{
    int oldjob = m_seljob;
    m_seljob = event.GetRow();
    if((unsigned int)m_seljob < m_remotejobinfoarray.GetCount()) {
        m_gridjoblist->SelectRow(m_seljob, false);
        m_connected = false;
        SetBtnStatus(1);
        wxMenu menu;
        menu.Append(POPUPMENU_JOBLIST_REMOVE, wxT("Remove"), wxT("Remove Selected Job."));
        PopupMenu(&menu);
    } else {
        m_seljob = oldjob;
    }
}

void JobMonitorPnl::OnRemoveJob(wxCommandEvent &event)
{
    DoRemoveJob(m_seljob);
}

void JobMonitorPnl::SetArrayDownload(wxArrayInt arydownload) {
    m_arydownload = arydownload;
}

void JobMonitorPnl::SetAryDownlist(wxArrayString lfiles, wxArrayString rfiles) {
    m_arylfiles = lfiles;
    m_aryrfiles = rfiles;
}

void JobMonitorPnl::OnMessageUpdate(wxMessageUpdateEvent &event) {
    wxString msg = event.GetString(); //arymsg[arymsg.GetCount()-1];
    if(msg.Cmp(wxT("Http Error! Please check the url or server status! The job is abort!")) == 0)
        wxMessageBox(msg);
    else
        AppendOutput(msg);
    if(msg.Cmp(wxT("All files download finished!")) == 0)
        OnSshDownFinished();
    if(msg.Cmp(wxT("All files download from web!")) == 0)
        OnHttpDownFinished();
    Tools::ShowStatus(msg);
}
