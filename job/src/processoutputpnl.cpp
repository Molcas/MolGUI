/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "processoutputpnl.h"
#include <wx/notebook.h>

ProcessOutputPanel::ProcessOutputPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name) {
        pTcOutput = NULL;
        pTcError = NULL;
        CreateControls();
}

void ProcessOutputPanel::CreateControls(void) {
        wxBoxSizer* bsToft = new wxBoxSizer(wxVERTICAL);
        this->SetSizer(bsToft);

        wxNotebook* pNbResult = new wxNotebook(this, wxID_ANY);
        wxPanel* pPnlOutput = new wxPanel(pNbResult, wxID_ANY);
        wxBoxSizer* bsOutput = new wxBoxSizer(wxVERTICAL);
        pPnlOutput->SetSizer(bsOutput);
        wxPanel* pPnlError = new wxPanel(pNbResult, wxID_ANY);
        wxBoxSizer* bsError = new wxBoxSizer(wxVERTICAL);
        pPnlError->SetSizer(bsError);

        pTcOutput = new wxTextCtrl(pPnlOutput, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(800,320), wxTE_MULTILINE|wxTE_READONLY|wxHSCROLL);
        bsOutput->Add(pTcOutput, 1, wxEXPAND|wxALL, 2);
        pTcError = new wxTextCtrl(pPnlError, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY|wxHSCROLL);
        bsError->Add(pTcError, 1, wxEXPAND|wxALL, 2);

        pNbResult->AddPage(pPnlOutput, wxT("Output"), true);
        pNbResult->AddPage(pPnlError, wxT("Error"), false);

        bsToft->Add(pNbResult, 1, wxEXPAND|wxALL, 2);
        //
        wxFont font;
        font.SetNativeFontInfoUserDesc(wxT("Monospace"));
        font.SetPointSize(10);
        if(font.Ok()) {
                pTcOutput->SetFont(font);
                pTcError->SetFont(font);
        }

}

void ProcessOutputPanel::AppendOutput(wxString& msg) {
        AppendMessage(pTcOutput, msg);
}

void ProcessOutputPanel::AppendError(wxString& msg){
        AppendMessage(pTcError, msg);
}

void ProcessOutputPanel::AppendMessage(wxTextCtrl* pTcLog, wxString& msg){
        pTcLog->AppendText(msg + wxT("\n"));
}

void ProcessOutputPanel::Clear(void) {
        pTcOutput->Clear();
        pTcError->Clear();
}

void ProcessOutputPanel::LoadFiles(wxString& outputFile, wxString& errorFile) {
        if(wxFileExists(outputFile)) {
                pTcOutput->LoadFile(outputFile);
                pTcOutput->ShowPosition(pTcOutput->GetLastPosition());
        }
        if(wxFileExists(errorFile)) {
                pTcError->LoadFile(errorFile);
                pTcError->ShowPosition(pTcError->GetLastPosition());
        }
}
