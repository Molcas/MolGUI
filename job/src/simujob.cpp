/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "simujob.h"

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>
#include <wx/filename.h>

#include "fileutil.h"
#include "stringutil.h"
#include "tools.h"
#include "simufiles.h"

const wxString FILE_BEGIN_JOB_CONTROL=wxT("$BEGIN JOBCONTROL");
const wxString FILE_END_JOB_CONTROL=wxT("$END JOBCONTROL");
const wxString FILE_BEGIN_SURFACE=wxT("$BEGIN SURFACE");
const wxString FILE_END_SURFACE=wxT("$END SURFACE");
const wxString FILE_BEGIN_TITLE=wxT("$BEGIN TITLE");
const wxString FILE_END_TITLE=wxT("$END TITLE");

SimuJob::SimuJob(wxString fileName) : JobInfo(fileName) {
}

SimuJob::~SimuJob() {
}

StringHash& SimuJob::GetJobControl(void) {
        return m_jobControlHash;
}

void SimuJob::SetJobControl(StringHash& jobControl) {
        if(!Tools::IsHashSame(m_jobControlHash, jobControl)){
                m_jobControlHash.clear();
                m_jobControlHash = jobControl;
                SetDirty(true);
        }
}


bool SimuJob::CanCalcSurface()
{
        return m_jobSurfaceHash.size()>0;
}


wxString SimuJob::GetCalcParamFileName(ParamType type) {
////        wxString fileName = wxEmptyString;
////        switch(type)
////        {
////        case PARAM_ARC:
////                fileName = m_simuProject->GetPathWithSep() + wxT("arc.info");
////                return FileUtil::NormalizeFileName(fileName);
////        default:
////                return wxEmptyString;
////        }
        return wxEmptyString;
}


void SimuJob::ResetResultFlags()
{
        ////isShowSurface=false;
}

StringHash& SimuJob::GetJobSurfaceSettings()
{
        return m_jobSurfaceHash;
}

wxString SimuJob::GetJobTitle()
{
        return m_jobTitle;
}

void SimuJob::SetJobTitle(wxString jobTitle)
{
        if(m_jobTitle.Cmp(jobTitle)!=0){
                m_jobTitle=jobTitle;
                SetDirty(true);
        }
}

void SimuJob::SetJobSurfaceSettings(StringHash &surfaceSettings)
{
        if(Tools::IsHashSame(m_jobSurfaceHash,surfaceSettings)==false){
                m_jobSurfaceHash.clear();
                m_jobSurfaceHash=surfaceSettings;
                SetDirty(true);
        }
}

bool SimuJob::LoadFile(void) {
    wxString strLine, value;
        wxFileInputStream fis(m_fileName);
        wxTextInputStream tis(fis);

        StringHash& jobControl=GetJobControl();
        StringHash& jobSurface=GetJobSurfaceSettings();
        wxArrayString tokens;

        strLine=tis.ReadLine();
        if(FILE_BEGIN_TITLE.Cmp(strLine)==0)
        {
                value=wxEmptyString;
                while(FILE_END_TITLE.Cmp(strLine=tis.ReadLine())!=0)
                {
                        if(value.IsEmpty())
                                value=strLine.Trim().Trim(false);
                        else
                                value+=wxT("\n")+strLine.Trim().Trim(false);
                }
                SetJobTitle(value);
                strLine=tis.ReadLine();
        }
        if(FILE_BEGIN_JOB_CONTROL.Cmp(strLine)==0)
        {
                //load job gen setting;
                while(FILE_END_JOB_CONTROL.Cmp(strLine=tis.ReadLine())!=0)
                {
                        strLine=strLine.Trim().Trim(false);
            wxStringTokenizer tzk(strLine,wxT("="));
                        value=tzk.GetNextToken();
                        jobControl[value]=tzk.GetNextToken();
                }
                strLine = tis.ReadLine();
        }
        if(FILE_BEGIN_SURFACE.Cmp(strLine)==0)
        {
                while(FILE_END_SURFACE.Cmp(strLine=tis.ReadLine())!=0)
                {
                        strLine=strLine.Trim().Trim(false);
            wxStringTokenizer tzk(strLine,wxT("="));
                        value=tzk.GetNextToken();
                        jobSurface[value]=tzk.GetNextToken();
                }
                strLine = tis.ReadLine();
        }
        return true;
}

bool SimuJob::SaveFile(void) {
    wxFileOutputStream fos(m_fileName);
    wxTextOutputStream tos(fos);

        SaveJobControl(tos);
        SaveJobSurface(tos);
        return true;
}

bool SimuJob::SaveJobControl(wxTextOutputStream &tos) {
        StringHash& jobControl = GetJobControl();

        tos << FILE_BEGIN_TITLE << endl;
        tos << wxT("    ") << GetJobTitle()<<endl;
        tos << FILE_END_TITLE << endl;

        tos << FILE_BEGIN_JOB_CONTROL << endl;
        wxArrayString array=FormatJobControl(jobControl);
        for(unsigned i=0;i<array.GetCount();i++)
                tos<<array[i]<<endl;
        tos << FILE_END_JOB_CONTROL << endl;

        return true;
}

bool SimuJob::SaveJobSurface(wxTextOutputStream &tos) {
        StringHash& jobSurface = GetJobSurfaceSettings();
        StringHash& jobControl = GetJobControl();
        wxString method = wxEmptyString, calcType = wxEmptyString;
        if(jobControl.find(wxT("Method"))!=jobControl.end())
        {
                method=jobControl.find(wxT("Method"))->second;
        }
        if(!method.IsEmpty())
        {
                calcType=wxT("calc_type=");
                if(method.Cmp(wxT("SE"))==0)
                {
                        calcType+=wxT("se");
                }
                else
                {
                        calcType+=wxT("ab_initio");
                }
        }
        wxArrayString array=FormatJobSurface(jobSurface);
        if(array.GetCount()>0)
        {
                tos << FILE_BEGIN_SURFACE << endl;
                for(unsigned i=0;i<array.GetCount();i++)
                        tos << wxT("    ") << array[i] << endl;

                if(StringUtil::IsEmptyString(calcType)==false)
                        tos << wxT("    ") << calcType << endl;
                tos << FILE_END_SURFACE << endl;
        }
////        wxString dir = pCtrlInst->GetFileName();
////        dir = wxFileName(dir).GetPath();
////        SaveFileList(tos,pSimuFiles->GetOutputFileList(),dir);
        return true;
}

wxArrayString SimuJob::FormatJobControl(StringHash &jobControl)
{
        wxArrayString array;
        wxString keys[13]={        wxT("JobType"),
                                                wxT("Method"),
                                                wxT("SEMod"),
                                                wxT("BasisSet"),
                                                wxT("DFTFunc"),
                                                wxT("Charge"),
                                                wxT("State"),
                                                wxT("Guess"),
                                                wxT("SCF_MAX"),
                                                wxT("SCF_CRT"),
                                                wxT("OPT_MAX"),
                                                wxT("OPT_CRT"),
                                                wxT("KeyWords")};
        wxString chKeys[4]={wxT("LOCSCF"),
                                                wxT("PRT_VECTS"),
                                                wxT("PRT_BONDS"),
                                                wxT("PRT_ITER")};
        int i;
        for(i=0 ; i<13 ; i++)        {
                if(jobControl.find(keys[i])!=jobControl.end()){
                        array.Add(wxT("    ")+keys[i]+wxT("=")+jobControl.find(keys[i])->second);
                }
        }
        for(i=0; i<4 ; i++){
                if(jobControl.find(chKeys[i])!=jobControl.end()){
                        array.Add(wxT("    ")+chKeys[i]);
                }
        }
        return array;
}

wxArrayString SimuJob::FormatJobSurface(StringHash &surfaceSet)
{
        wxArrayString array;
        wxString gridString[4]={wxT("Format"),wxT("Density"),wxT("Resolution"),wxT("Number")};
        if(surfaceSet.find(gridString[0])!= surfaceSet.end())
        {
                for(int j=0;j<4;j++)
                {
                        if(surfaceSet.find(gridString[j])!= surfaceSet.end()){
                                array.Add(gridString[j]+wxT("=")+surfaceSet.find(gridString[j])->second);
                        }
                }
        }
        return array;
}

ExecType SimuJob::GetExecType(void) {
        StringHash& set= GetJobControl();
        if(set.find(wxT("Method"))->second.Cmp(wxT("HF"))==0 || set.find(wxT("Method"))->second.Cmp(wxT("MP2"))==0) {
                return EXEC_ABIN;
        }else if(set.find(wxT("Method"))->second.Cmp(wxT("SE"))==0) {
                return EXEC_SEMI;
        }
        return EXEC_NONE;
}
