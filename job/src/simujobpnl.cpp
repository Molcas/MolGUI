/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "simujobpnl.h"

#include "stringutil.h"
#include "tools.h"
#include "simujob.h"
#include "projectgroup.h"
enum JobGenDlgCtrlID{
        ID_CTRL_JOBTYPE_CHOICE_JOBTYPE=101,//job type
        ID_CTRL_JOBTYPE_TEXT_SCF_MAX,//SCF MAX
        ID_CTRL_JOBTYPE_TEXT_SCF_CRT,//SCF CRT
        ID_CTRL_JOBTYPE_TEXT_OPT_MAX,//OPT MAX
        ID_CTRL_JOBTYPE_TEXT_OPT_CRT,//OPT CRT
        ID_CTRL_JOBTYPE_CHECK_ORBITAL,//Orbitals
        ID_CTRL_JOBTYPE_CHECK_CHARGE,//Charge
//        ID_CTRL_JOBTYPE_CHECK_DENSITY,//Density
        ID_CTRL_JOBTYPE_CHECK_ITERATION,//Iteration
        //ID_CTRL_JOBTYPE_CHECK_OPT,//Optimization


        ID_CTRL_METHOD_CHOICE_METHOD=201, //connect with "Method Choice"
        ID_CTRL_METHOD_CHOICE_BASIS_SET,     //connect with "Basis Set"
        ID_CTRL_METHOD_CHOICE_DFT_FUNC,     //connect with "DFT Function"
        ID_CTRL_METHOD_CHOICE_MODEL,  //connect with "Model"
        ID_CTRL_METHOD_TEXT_TOTAL_CHARGE,//total charge
        ID_CTRL_METHOD_SPIN_TOTAL_CHARGE,//spin button
        ID_CTRL_METHOD_CHECK_LOCALSCF,//LocalSCF
        ID_CTRL_METHOD_CHOICE_STATE,//STATE

        ID_CTRL_TITLE_TEXT_TITLE=301,//title

        ID_CTRL_GUESS_CHOICE_SCF_GUESS=401,//SCF GUESS
        ID_CTRL_GUESS_TEXT_OCCU_ALPHA,//OCCUPIED ALPHA
        ID_CTRL_GUESS_TEXT_OCCU_BETA,//OCCUPIED BETA
        ID_CTRL_GUESS_TEXT_ALTER_ALPHA,//ALTER ALPHA
        ID_CTRL_GUESS_TEXT_ALTER_BETA,//ALTER BETA

        ID_CTRL_SURFACE_CHECK_SURFACE=501,//GRID IT
        ID_CTRL_SURFACE_CHECK_DENSITY,//GRID IT
        ID_CTRL_SURFACE_RADIO_FILE_FORMAT,//GRID FILE FORMAT:BINARY OR ASCII
        ID_CTRL_SURFACE_RADIO_RESOLUTION,//RESOLUTION: NORMAL OR HIGH
        ID_CTRL_SURFACE_TEXT_ORBIT_NUM,//ORBITALS NUMBERS
        ID_CTRL_SURFACE_SPINBTN_ORBIT_NUM,//ORBITALS NUMBERS

        ID_CTRL_TEXT_KEYWORDS,//more keywords

        ID_ANY
};

enum NotePage{
        PageJobType,
        PageMethod,
        //PageTitle,
        PageGuess,
        PageProperty,
        PageSurface,
        PageSolvation,
        PageAny
};
enum JobTypes{
        JobTypeSP,
        JobTypeGrad,
        JobTypeGM,
        JobTypeTS,
        JobTypeVIB,
        JobTypeOFreq,
        JobTypeIRC,
        JobTypeMax
};
enum MethodType{
        MethodSE,
        MethodHF,
        MethodMP2,
        MethodDFT,
        MethodMax
};
/////////////////////////////////////////////////////////////////////
//static vars
static const int GuessChoiceNum=4;
static const wxString GuessChoices[GuessChoiceNum]={
        wxT("Core"),
        wxT("Huckel"),
        wxT("READ"),
        wxT("ALTER")
};
//static const int InitHessionChoiceNum=4;
//static const wxString InitHessionChoices[InitHessionChoiceNum]={
//        wxT("Core"),
//        wxT("Huckel"),
//        wxT("READ"),
//        wxT("ALTER")
//};
static const int JobTypeChoiceNum=7;
static const wxString JobTypeChoices[JobTypeChoiceNum]={
        wxT("Single Point Energy"),
        wxT("Gradients"),
        wxT("Geometry Optimization"),
        wxT("Transition State"),
        wxT("Vibrational Frequency"),
        wxT("Opt-Freq"),
        wxT("Intrinic Reactive Coordinates")
};
static const int MethodBasisSetChoiceNum=8;
static const wxString MethodBasisSetChoices[MethodBasisSetChoiceNum]={
        wxT("STO-3G"),//1
        wxT("3-21G(*)"),//2
        wxT("6-31G*"),//5
        wxT("6-31G**"),//6
        wxT("6-31+G*"),//6
        wxT("6-311G*"),//5
        wxT("6-311+G**"),//5
    wxT("General")//17
};
static const int MethodChoiceNum=3;
static const wxString MethodChoices[MethodChoiceNum]={
        wxT("Semi-Empirical"), //Add the choice of Semi-Empirical in Method
        wxT("Hartree-Fock"),
//        wxT("DFT"),
        wxT("MP2")
//        wxT("CASSCF"),
//        wxT("MBPT2"),
//        wxT("CASPT2"),
//        wxT("RASSI"),
//        wxT("CASVB"),
};
static const int MethodDftChoiceNum=12;
static const wxString MethodDftChoices[MethodDftChoiceNum]={
        wxT("B3LYP"),//1
        wxT("LSDA"),//2
        wxT("LDA"),//3
        wxT("SVWN"),//4
        wxT("LSDA5"),//5
        wxT("LDA5,SVWN5"),//6
        wxT("HFB"),//7
        wxT("HFS"),//8
        wxT("BLYP"),//9
        //wxT("B3LYP"),
        wxT("B3LYP5"),//10
        wxT("TLYP"),//11
        wxT("XPBE")//12
};
static const int MethodModelChoiceNum=6;
static const wxString MethodModelChoices[MethodModelChoiceNum]={
        wxT("MNDO"),
        wxT("AM1"),
        wxT("PM3"),
        wxT("RM1"),
        wxT("PM6")
        wxT("PM7")
};
static const int MethodStateChoiceNum=6;
static const wxString MethodStateChoices[MethodStateChoiceNum]={
        wxT("Singlet"),
        wxT("Doublet"),
        wxT("Triplet"),
        wxT("Quantet"),
        wxT("Quintet"),
        wxT("Sextet")
};
static const int SurfaceFileFormatChoiceNum=2;
static const wxString SurfaceFileFormatChoices[SurfaceFileFormatChoiceNum]={
        wxT("Binary"),
        wxT("ASCII")
};
static const int SurfaceResolutionChoiceNum=3;
static const wxString SurfaceResolutionChoices[SurfaceResolutionChoiceNum]={
        wxT("Low"),
        wxT("Medium"),
        wxT("High")
};

BEGIN_EVENT_TABLE(SimuJobPnl, wxPanel)
        //EVT_TEXT(ID_CTRL_TITLE_TEXT_TITLE,JobGenerateDlg::OnTitleChanged)
        EVT_CHOICE(ID_CTRL_JOBTYPE_CHOICE_JOBTYPE,SimuJobPnl::OnJobTypeSelChanged)

        EVT_CHOICE(ID_CTRL_GUESS_CHOICE_SCF_GUESS,SimuJobPnl::OnGuessSelChanged)

//        EVT_TEXT(ID_CTRL_METHOD_TEXT_TOTAL_CHARGE,SimuJobPnl::OnTotalChargeChanged)
//        EVT_TEXT(ID_CTRL_METHOD_TEXT_MULTI,SimuJobPnl::OnMultiChanged)
        EVT_CHOICE(ID_CTRL_METHOD_CHOICE_METHOD,SimuJobPnl::OnMethodSelChanged)
        EVT_CHOICE(ID_CTRL_METHOD_CHOICE_DFT_FUNC,SimuJobPnl::OnDFTFuncChanged)
        EVT_CHOICE(ID_CTRL_METHOD_CHOICE_MODEL,SimuJobPnl::OnModelChanged)
        EVT_CHOICE(ID_CTRL_METHOD_CHOICE_BASIS_SET,SimuJobPnl::OnBasisSetChanged)

        EVT_CHECKBOX(ID_CTRL_SURFACE_CHECK_SURFACE,SimuJobPnl::OnSurfaceCheck)
        EVT_SPIN(ID_CTRL_SURFACE_SPINBTN_ORBIT_NUM, SimuJobPnl::OnSpinButton)

        EVT_CHOICE(ID_CTRL_METHOD_CHOICE_STATE,SimuJobPnl::OnMethodStateChanged)
        EVT_SPIN(ID_CTRL_METHOD_SPIN_TOTAL_CHARGE, SimuJobPnl::OnTotalChargeSpinChanged)
END_EVENT_TABLE()

SimuJobPnl::SimuJobPnl(GenericDlg* parent, ProjectTreeItemData* treeItem, wxWindowID id,bool isNewjob) : GenericPnl(parent, id) {
        m_pTreeItem = treeItem;
        CreateGUIControls();
        SetDefaults();
        m_isNewJob=isNewjob;
        SimuJob* pSimuJob = static_cast<SimuJob*>(m_pTreeItem->GetProject()->GetJobInfo());
        if(!pSimuJob && wxFileExists(m_pTreeItem->GetFileName())) {
                pSimuJob = new SimuJob(m_pTreeItem->GetFileName());
                m_pTreeItem->GetProject()->SetJobInfo(pSimuJob);
                pSimuJob->LoadFile();
    }

        if(pSimuJob) {
                wxString jobTitle = pSimuJob->GetJobTitle();
                StringHash jobHash = pSimuJob->GetJobControl();
                StringHash jobSurface = pSimuJob->GetJobSurfaceSettings();

                SetJobTitle(jobTitle);
                if(jobHash.size()>0){
                        SetJobControl(jobHash);
                }else{
                        jobHash.clear();
                        jobHash = GetDefaultJobControl();
                        SetJobControl(jobHash);
                }
                if(jobSurface.size()>0) {
                        SetSurfaceSettings(jobSurface);
        }
        }
}

SimuJobPnl::~SimuJobPnl() {
}

void SimuJobPnl::CreateGUIControls(void) {
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        this->SetSizer(bsTop);
        wxFlexGridSizer* fgsLabel=new wxFlexGridSizer(3, 2, 0, 0);
        fgsLabel->AddGrowableCol(1);

        wxStaticText*         stText=new wxStaticText(this, wxID_ANY,wxT(" Title:"),wxDefaultPosition,
                                                                        wxDefaultSize, wxALIGN_LEFT);
        tcTitle = new wxTextCtrl(this, ID_CTRL_TITLE_TEXT_TITLE,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(400, 25));
        wxStaticText* stJobType=new wxStaticText(this, wxID_ANY,wxT(" Job Type:"),wxDefaultPosition,
                                                                        wxDefaultSize, wxALIGN_LEFT);
        stJobTypeDis=new wxStaticText(this,wxID_ANY,wxEmptyString,wxDefaultPosition,
                                                                        wxDefaultSize, wxALIGN_LEFT);
        wxStaticText* stCharge=new wxStaticText(this, wxID_ANY,wxT(" Charge/Mult:"),wxDefaultPosition,
                                                                        wxDefaultSize, wxALIGN_LEFT);
        stCharMultDis=new wxStaticText(this,wxID_ANY,wxEmptyString,wxDefaultPosition,
                                                                        wxDefaultSize, wxALIGN_LEFT);

        fgsLabel->Add(stText                , 5, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL , 5);
        fgsLabel->Add(tcTitle                , 5, wxALIGN_LEFT|wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL , 5);
        fgsLabel->Add(stJobType                , 5, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL , 5);
        fgsLabel->Add(stJobTypeDis        , 5, wxALIGN_LEFT|wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL , 5);
        fgsLabel->Add(stCharge                , 5, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL , 5);
        fgsLabel->Add(stCharMultDis        , 5, wxALIGN_LEFT|wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL , 5);

        //wxNotebook* pNotebook = new wxNotebook(this, wxID_ANY);
        pNotebook = new wxNotebook(this, wxID_ANY);
        wxPanel *pPnlJobType = CreateJobTypePage(pNotebook);
        wxPanel *pPnlMethod = CreateMethodPage(pNotebook);
        //wxPanel *pPnlTitle = CreateTitlePage(pNotebook);
        wxPanel *pPnlGuess = CreateGuessPage(pNotebook);
        wxPanel *pPnlProperty = CreatePropertyPage(pNotebook);
        wxPanel *pPnlSurface = CreateSurfacePage(pNotebook);
        wxPanel *pPnlSolvation = CreateSolvationPage(pNotebook);
        pNotebook->AddPage(pPnlJobType        , wxT("Job Type")        , true);
        pNotebook->AddPage(pPnlMethod        , wxT("Method")                , false);
        //pNotebook->AddPage(pPnlTitle, wxT("Title"), false);
        pNotebook->AddPage(pPnlGuess        , wxT("Guess")                , false);
        pNotebook->AddPage(pPnlProperty        , wxT("Property")        , false);
        pNotebook->AddPage(pPnlSurface        , wxT("Surface")        , false);
        pNotebook->AddPage(pPnlSolvation, wxT("Solvation")        , false);

        wxBoxSizer* bsKey = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* stKey = new wxStaticText(this, wxID_ANY,wxT("More Keywords:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        wxTextCtrl* tcKey = new wxTextCtrl(this, ID_CTRL_TEXT_KEYWORDS,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(60, 25),wxTE_PROCESS_ENTER);
        bsKey->Add(stKey,0,wxALL|wxALIGN_CENTER,5);
        bsKey->Add(tcKey,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);

        wxFlexGridSizer *pOper = new wxFlexGridSizer(1,3,0,0);
        pOper->AddGrowableCol(0);
        pOper->AddGrowableCol(1);
        pOper->AddGrowableCol(3);
/*
        wxButton* btnSubmit=new wxButton(this, wxID_OK, wxT("Submit"), wxPoint(10, 10), wxDefaultSize);
        wxButton* btnReset=new wxButton(this, wxID_CANCEL, wxT("Reset"), wxPoint(10, 10), wxDefaultSize);
        wxButton* btnClose=new wxButton(this, wxID_CLOSE, wxT("Close"), wxPoint(10, 10), wxDefaultSize);
        pOper->Add(btnSubmit        , 5,wxALIGN_CENTER | wxALL, 5);
        pOper->Add(btnReset                , 5,wxALIGN_CENTER | wxALL, 5);
        pOper->Add(btnClose                , 5,wxALIGN_CENTER | wxALL, 5);
*/
        bsTop->Add(fgsLabel        ,0, wxALL, 5);
        bsTop->Add(pNotebook,1, wxALIGN_CENTER |wxEXPAND|wxALL, 5);
        bsTop->Add(bsKey        ,0,wxALIGN_RIGHT |wxALL|wxEXPAND,0);
        bsTop->Add(pOper        ,0,wxALIGN_RIGHT |wxALL,0);
        this->Layout();
        SetMinSize(wxSize(500, 450));
}

long SimuJobPnl::GetButtonStyle(void) {
        return BTN_SAVE | BTN_RESET | BTN_SUBMIT | BTN_CLOSE;
}

bool SimuJobPnl::OnButton(long style) {
        if(style == BTN_CLOSE) {
                return OnBtnClose();
        }else if(style == BTN_SAVE) {
                return OnBtnSave();
        }else if(style == BTN_RESET) {
                return OnBtnReset();
        }else if(style == BTN_SUBMIT) {
                return OnBtnSubmit();
        }
        return true;
}

/*
create a sizer to input text or value
display like:
  scf max : _________________
parent[in]: the parent window
textID[in]: the id of text control
display[in]: the text to display ,such as "scf max"
disSize[in]: the size of display text
textSize[in]: the size of text control
*/
wxBoxSizer* SimuJobPnl::CreateTextInput(wxWindow *parent, wxWindowID textID, wxString display, wxSize disSize, wxSize textSize)
{
        wxBoxSizer* bsTextInput = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* stDisplay = new wxStaticText(parent, wxID_ANY,display,wxDefaultPosition, disSize, wxALIGN_LEFT);
        wxTextCtrl* textInput = new wxTextCtrl(parent, textID,wxEmptyString,wxDefaultPosition, textSize);
        bsTextInput->Add(stDisplay,0,wxALL|wxALIGN_CENTER,5);
        bsTextInput->Add(textInput,0,wxALL|wxALIGN_CENTER,5);
        return bsTextInput;
}

wxPanel*  SimuJobPnl::CreateJobTypePage(wxBookCtrlBase *parent)
{
        //create panel
        wxPanel* pPnlRet = new wxPanel(parent);
        wxSize stSize(60,20);
        //create top sizer
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        //set the panel sizer
        pPnlRet->SetSizer(bsTop);
        //job type choice
        //create the sizer of job type choice
        wxBoxSizer* bsJobType= new wxBoxSizer(wxHORIZONTAL);
        //create the description of job type choice
        wxStaticText* stJobType = new wxStaticText(pPnlRet, wxID_ANY,wxT("Job Type:"),
                                                                                                wxDefaultPosition, stSize, wxALIGN_LEFT);
        //create the job type choice
        wxChoice* choiceJobType= new wxChoice(pPnlRet,ID_CTRL_JOBTYPE_CHOICE_JOBTYPE,wxDefaultPosition, wxDefaultSize,JobTypeChoiceNum,JobTypeChoices);
        //add the description to the sizer
        bsJobType->Add(stJobType,0,wxALL|wxALIGN_CENTER|wxALIGN_CENTRE_VERTICAL,5);
        //add the job type choice to the sizer
        bsJobType->Add(choiceJobType,0,wxALL|wxALIGN_CENTER,5);
        //create the sizer to show the display details
        wxBoxSizer* bsDisplay = new wxBoxSizer(wxHORIZONTAL);
        //create static box sizer to display text input
        wxStaticBox* sbText = new wxStaticBox(pPnlRet,wxID_ANY, wxEmptyString);
        sbsJobTypeText = new wxStaticBoxSizer(sbText,wxVERTICAL);
        //scf max
        wxBoxSizer* bsScfMax=CreateTextInput(pPnlRet,ID_CTRL_JOBTYPE_TEXT_SCF_MAX,wxT("SCF MAX:"),stSize);
        //scf crt
        wxBoxSizer* bsScfCrt=CreateTextInput(pPnlRet,ID_CTRL_JOBTYPE_TEXT_SCF_CRT,wxT("SCF CRT:"),stSize);
        //OPT max
        wxBoxSizer* bsOptMax=CreateTextInput(pPnlRet,ID_CTRL_JOBTYPE_TEXT_OPT_MAX,wxT("OPT MAX:"),stSize);
        //OPT crt
        wxBoxSizer* bsOptCrt=CreateTextInput(pPnlRet,ID_CTRL_JOBTYPE_TEXT_OPT_CRT,wxT("OPT CRT:"),stSize);
        //add to static box sizer
        sbsJobTypeText->Add(bsScfMax,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbsJobTypeText->Add(bsScfCrt,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbsJobTypeText->Add(bsOptMax,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbsJobTypeText->Add(bsOptCrt,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        //create static box sizer to display output
        wxStaticBox* sbOutput = new wxStaticBox(pPnlRet,wxID_ANY, wxT("Output"));
        sbsJobTypeOutput = new wxStaticBoxSizer(sbOutput,wxVERTICAL);
        //create checkbox  of Orbitals
        wxCheckBox* checkOrbitals = new wxCheckBox(pPnlRet, ID_CTRL_JOBTYPE_CHECK_ORBITAL,wxT("Orbitals and Energies"), wxDefaultPosition, wxDefaultSize);
        //create checkbox  of Charge
        wxCheckBox* checkCharge = new wxCheckBox(pPnlRet, ID_CTRL_JOBTYPE_CHECK_CHARGE,wxT("Atomic Charge and Bond Order"), wxDefaultPosition, wxDefaultSize);
        //create checkbox  of Density
//        wxCheckBox* checkDensity = new wxCheckBox(pPnlRet, ID_CTRL_JOBTYPE_CHECK_DENSITY,wxT("Density Matrix"), wxDefaultPosition, wxDefaultSize);
        //create checkbox  of Iteration
        wxCheckBox* checkIteration = new wxCheckBox(pPnlRet, ID_CTRL_JOBTYPE_CHECK_ITERATION,wxT("Iteration"), wxDefaultPosition, wxDefaultSize);
        //create checkbox  of Optimization
        //wxCheckBox* checkOpt = new wxCheckBox(pPnlRet, ID_CTRL_JOBTYPE_CHECK_OPT,wxT("Optimization"), wxDefaultPosition, wxDefaultSize);
        //add to static box sizer
        sbsJobTypeOutput->Add(checkOrbitals,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbsJobTypeOutput->Add(checkCharge,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
//        sbsJobTypeOutput->Add(checkDensity,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbsJobTypeOutput->Add(checkIteration,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        //sbsJobTypeOutput->Add(checkOpt,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        //add sizer to display
        bsDisplay->Add(sbsJobTypeText,0,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        bsDisplay->Add(sbsJobTypeOutput,0,wxALL|wxEXPAND|wxALIGN_CENTER,5);

        bsTop->Add(bsJobType, 0, wxALL, 5);
        bsTop->Add(bsDisplay,0,wxALL|wxEXPAND,5);
        return pPnlRet;
}

wxPanel*  SimuJobPnl::CreateMethodPage(wxBookCtrlBase *parent)
{
        wxPanel* pPnlRet = new wxPanel(parent);
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        pPnlRet->SetSizer(bsTop);

        wxBoxSizer* bsList1 = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText0 = new wxStaticText(pPnlRet, wxID_ANY,wxT("Method:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        bsList1->Add(sText0,0,wxALL|wxALIGN_CENTER,5);

        wxChoice* choice1= new wxChoice(pPnlRet,ID_CTRL_METHOD_CHOICE_METHOD,wxDefaultPosition, wxDefaultSize,MethodChoiceNum,MethodChoices);
        bsList1->Add(choice1,0,wxALL|wxALIGN_CENTER,5);

        wxBoxSizer* bsList4 = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText1 = new wxStaticText(pPnlRet, wxID_ANY,wxT("Basis Set:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        bsList4->Add(sText1,0,wxALL|wxALIGN_LEFT,5);
        wxChoice* choice2= new wxChoice(pPnlRet,ID_CTRL_METHOD_CHOICE_BASIS_SET,wxDefaultPosition, wxDefaultSize,MethodBasisSetChoiceNum,MethodBasisSetChoices);
        bsList4->Add(choice2,0,wxALL|wxEXPAND|wxALIGN_LEFT,5);

        wxBoxSizer* bsList2 = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText2 = new wxStaticText(pPnlRet, wxID_ANY,wxT("DFT Functional:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        bsList2->Add(sText2,0,wxALL|wxALIGN_CENTER,5);
        wxChoice* choice3= new wxChoice(pPnlRet,ID_CTRL_METHOD_CHOICE_DFT_FUNC,wxDefaultPosition, wxDefaultSize,MethodDftChoiceNum,MethodDftChoices);
        bsList2->Add(choice3,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);

        //add Model choice selection
        wxBoxSizer* bsList3 = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText6 = new wxStaticText(pPnlRet, wxID_ANY,wxT("Model:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        bsList3->Add(sText6,0,wxALL|wxALIGN_CENTER,5);
        wxChoice* choice4= new wxChoice(pPnlRet,ID_CTRL_METHOD_CHOICE_MODEL,wxDefaultPosition, wxDefaultSize,MethodModelChoiceNum,MethodModelChoices);
        bsList3->Add(choice4,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);

        //wxBoxSizer* bsList3 = new wxBoxSizer(wxHORIZONTAL);
        wxBoxSizer* bsDetail = new wxBoxSizer(wxHORIZONTAL);
        // Create static box and static box sizer
        wxStaticBox* sBox = new wxStaticBox(pPnlRet,wxID_ANY, wxEmptyString);
        wxStaticBoxSizer* sbSizer = new wxStaticBoxSizer(sBox,wxVERTICAL);
        wxBoxSizer* bsTotalCharge = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText3 = new wxStaticText(pPnlRet, wxID_ANY,wxT("Total Charge:"),
                                                                                                wxDefaultPosition, wxSize(80,25), wxALIGN_LEFT);

        wxTextCtrl* textCtrl1 = new wxTextCtrl(pPnlRet, ID_CTRL_METHOD_TEXT_TOTAL_CHARGE,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(60, 25));
        wxSpinButton* psbTotalCharge= new wxSpinButton(pPnlRet, ID_CTRL_METHOD_SPIN_TOTAL_CHARGE, wxDefaultPosition, wxSize(20, 25), wxSP_VERTICAL);
        psbTotalCharge->SetRange(-30, 30);
        psbTotalCharge->SetValue(0);




        bsTotalCharge->Add(sText3,0,wxALL|wxALIGN_CENTER,5);
        bsTotalCharge->Add(textCtrl1,0,wxALL|wxALIGN_CENTER,0);
        bsTotalCharge->Add(psbTotalCharge,0,wxALL|wxALIGN_CENTER,0);

        wxBoxSizer* bsState = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText4 = new wxStaticText(pPnlRet, wxID_ANY,wxT("State:"),
                                                                                                wxDefaultPosition, wxSize(80,25), wxALIGN_LEFT);
        wxChoice* choiceState= new wxChoice(pPnlRet,ID_CTRL_METHOD_CHOICE_STATE,wxDefaultPosition, wxDefaultSize,MethodStateChoiceNum,MethodStateChoices);


        bsState->Add(sText4,0,wxALL|wxALIGN_CENTER,5);
        bsState->Add(choiceState,1,wxALL|wxEXPAND|wxALIGN_CENTER,0);

        sbSizer->Add(bsTotalCharge,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbSizer->Add(bsState,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);

        //wxStaticBox* sBoxSet = new wxStaticBox(pPnlRet,wxID_ANY, wxEmptyString);
        //wxStaticBoxSizer* sbSizerSet = new wxStaticBoxSizer(sBoxSet,wxVERTICAL);

        wxCheckBox* checkLocScf = new wxCheckBox(pPnlRet, ID_CTRL_METHOD_CHECK_LOCALSCF,wxT("LocalSCF"), wxDefaultPosition, wxDefaultSize);
        //sbSizerSet->Add(checkLocScf,1,wxALL|wxEXPAND|wxALIGN_CENTER,10);

        bsDetail->Add(sbSizer,0,wxALL|wxALIGN_LEFT|wxALIGN_CENTER_HORIZONTAL ,5);
        bsDetail->Add(checkLocScf,0,wxALL|wxALIGN_LEFT ,25);

        bsTop->Add(bsList1,0,wxALL|wxEXPAND,5);
        bsTop->Add(bsList4,0,wxALL|wxEXPAND,5);  //basis set
        bsTop->Add(bsList2,0,wxALL/*|wxEXPAND*/,5);
        bsTop->Show(bsList2,false);
        bsTop->Add(bsList3,0,wxALL/*|wxEXPAND*/,5); //Model
        bsTop->Show(bsList3,false);
        bsTop->Add(bsDetail,0,wxALL/*|wxEXPAND*/,5);
        return pPnlRet;
}

wxPanel*  SimuJobPnl::CreateGuessPage(wxBookCtrlBase *parent)
{
        wxPanel* pPnlRet = new wxPanel(parent);
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        pPnlRet->SetSizer(bsTop);
        wxSize stSize(40,20);

        wxBoxSizer* bsList1 = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText1 = new wxStaticText(pPnlRet, wxID_ANY,wxT("SCF Guess:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        bsList1->Add(sText1,0,wxALL|wxALIGN_CENTER,5);
        wxChoice* choice1= new wxChoice(pPnlRet,ID_CTRL_GUESS_CHOICE_SCF_GUESS,wxDefaultPosition, wxDefaultSize,GuessChoiceNum,GuessChoices);
        bsList1->Add(choice1,0,wxALL|wxALIGN_CENTER,5);

        wxStaticBox* sBox1 = new wxStaticBox(pPnlRet,wxID_ANY, wxT("Occupied:"));
        sbsGuessOccup = new wxStaticBoxSizer(sBox1,wxVERTICAL);

        wxBoxSizer* bsList2 = new wxBoxSizer(wxHORIZONTAL);

        stGuessOccuAlpha = new wxStaticText(pPnlRet, wxID_ANY,wxT("ALPHA:"),
                                                                                                wxDefaultPosition, stSize, wxALIGN_LEFT);


        wxTextCtrl* textCtrl1 = new wxTextCtrl(pPnlRet, ID_CTRL_GUESS_TEXT_OCCU_ALPHA,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(60, 25));

        bsList2->Add(stGuessOccuAlpha,0,wxALL|wxALIGN_CENTER,5);
        bsList2->Add(textCtrl1,0,wxALL|wxALIGN_CENTER,5);

        wxBoxSizer* bsList3 = new wxBoxSizer(wxHORIZONTAL);
        wxStaticText* sText3 = new wxStaticText(pPnlRet, wxID_ANY,wxT("BETA:"),
                                                                                                wxDefaultPosition, stSize, wxALIGN_LEFT);

        wxTextCtrl* textCtrl2 = new wxTextCtrl(pPnlRet, ID_CTRL_GUESS_TEXT_OCCU_BETA,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(60, 25));

        bsList3->Add(sText3,0,wxALL|wxALIGN_CENTER,5);
        bsList3->Add(textCtrl2,0,wxALL|wxALIGN_CENTER,5);

        sbsGuessOccup->Add(bsList2,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbsGuessOccup->Add(bsList3,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);

        wxStaticBox* sBox2 = new wxStaticBox(pPnlRet,wxID_ANY, wxT("ALTER:"));
        sbsGuessAlter = new wxStaticBoxSizer(sBox2,wxVERTICAL);

        wxBoxSizer* bsList4 = new wxBoxSizer(wxHORIZONTAL);

        stGuessAlteAlpha = new wxStaticText(pPnlRet, wxID_ANY,wxT("ALPHA:"),
                                                                                                wxDefaultPosition, stSize, wxALIGN_LEFT);

        wxTextCtrl* textCtrl3 = new wxTextCtrl(pPnlRet, ID_CTRL_GUESS_TEXT_ALTER_ALPHA,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(60, 25));

        bsList4->Add(stGuessAlteAlpha,0,wxALL|wxALIGN_CENTER,5);
        bsList4->Add(textCtrl3,0,wxALL|wxALIGN_CENTER,5);

        wxBoxSizer* bsList5 = new wxBoxSizer(wxHORIZONTAL);

        wxStaticText* sText5 = new wxStaticText(pPnlRet, wxID_ANY,wxT("BETA:"),
                                                                                                wxDefaultPosition, stSize, wxALIGN_LEFT);

        wxTextCtrl* textCtrl4 = new wxTextCtrl(pPnlRet, ID_CTRL_GUESS_TEXT_ALTER_BETA,wxEmptyString,
                                                                                        wxDefaultPosition, wxSize(60, 25));

        bsList5->Add(sText5,0,wxALL|wxALIGN_CENTER,5);
        bsList5->Add(textCtrl4,0,wxALL|wxALIGN_CENTER,5);

        sbsGuessAlter->Add(bsList4,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);
        sbsGuessAlter->Add(bsList5,1,wxALL|wxEXPAND|wxALIGN_CENTER,5);

        bsTop->Add(bsList1,0,wxALL|wxEXPAND,5);
        bsTop->Add(sbsGuessOccup,0,wxALL,5);
        bsTop->Add(sbsGuessAlter,0,wxALL,5);
        return pPnlRet;
}

wxPanel*  SimuJobPnl::CreatePropertyPage(wxBookCtrlBase *parent)
{
        wxPanel* pPnlRet = new wxPanel(parent);
        return pPnlRet;
}

wxPanel*  SimuJobPnl::CreateSurfacePage(wxBookCtrlBase *parent)
{
        wxPanel* pPnlRet = new wxPanel(parent);
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);

        wxBoxSizer* bsCheck = new wxBoxSizer(wxHORIZONTAL);
        wxCheckBox* chkSurface = new wxCheckBox(pPnlRet, ID_CTRL_SURFACE_CHECK_SURFACE,wxT("Surface"),
                                                                                        wxDefaultPosition, wxDefaultSize);
        bsCheck->Add(chkSurface,0,wxALL|wxALIGN_LEFT,5);

        bsSurface = new wxBoxSizer(wxVERTICAL);
        wxCheckBox* checkDesity = new wxCheckBox(pPnlRet, ID_CTRL_SURFACE_CHECK_DENSITY,wxT("Density"),
                                                                                        wxDefaultPosition, wxDefaultSize);
        wxRadioBox* radioFileFormat = new wxRadioBox(pPnlRet, ID_CTRL_SURFACE_RADIO_FILE_FORMAT,wxT("File Format:"),
                                                                                wxDefaultPosition, wxDefaultSize,SurfaceFileFormatChoiceNum,SurfaceFileFormatChoices,SurfaceFileFormatChoiceNum, wxRA_SPECIFY_COLS);
        wxRadioBox* radioResolution = new wxRadioBox(pPnlRet, ID_CTRL_SURFACE_RADIO_RESOLUTION,wxT("Resolution"),
                                                                                wxDefaultPosition, wxDefaultSize,SurfaceResolutionChoiceNum,SurfaceResolutionChoices,SurfaceResolutionChoiceNum, wxRA_SPECIFY_COLS);


        wxStaticBox* sbOrbital = new wxStaticBox(pPnlRet,wxID_ANY, wxT("Orbitals"));
        wxStaticBoxSizer* sbsNum= new wxStaticBoxSizer(sbOrbital,wxHORIZONTAL);
        wxStaticText* stNum = new wxStaticText(pPnlRet, wxID_ANY,wxT("Numbers:"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
        wxTextCtrl* tcNum = new wxTextCtrl(pPnlRet, ID_CTRL_SURFACE_TEXT_ORBIT_NUM,wxT("1"),
                                                                                        wxDefaultPosition, wxDefaultSize,wxTE_READONLY /*|wxTE_PROCESS_ENTER*/);
        wxSpinButton* pSbNum = new wxSpinButton(pPnlRet, ID_CTRL_SURFACE_SPINBTN_ORBIT_NUM , wxDefaultPosition, wxSize(20,25), wxSP_VERTICAL);
        pSbNum->SetRange(0, 500);
        pSbNum->SetValue(1);

        sbsNum->Add(stNum,0,wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_LEFT,2);
        sbsNum->Add(tcNum,0,wxALIGN_CENTER_VERTICAL |wxALL, 0);
        sbsNum->Add(pSbNum,0,wxALIGN_CENTER_VERTICAL |wxALL, 0);

        bsSurface->Add(checkDesity,0,wxALL|wxALIGN_LEFT,5);
        bsSurface->Add(radioFileFormat,0,wxALL|wxALIGN_LEFT,5);
        bsSurface->Add(radioResolution,0,wxALL|wxALIGN_LEFT,5);
        bsSurface->Add(sbsNum,0,wxALL|wxALIGN_LEFT,5);

        bsTop->Add(bsCheck,0,wxALL|wxALIGN_LEFT,5);
        bsTop->Add(bsSurface,0,wxALL|wxALIGN_LEFT,5);
        bsTop->Show(bsSurface,false);
        pPnlRet->SetSizer(bsTop);

        return pPnlRet;
}

wxPanel*  SimuJobPnl::CreateSolvationPage(wxBookCtrlBase *parent)
{
        wxPanel* pPnlRet = new wxPanel(parent);
        return pPnlRet;
}

bool SimuJobPnl::OnBtnClose(void)
{
/*        bsJobType[0]=NULL;
        bsJobType[1]=NULL;
        bsJobType[2]=NULL;
        bsJobType[3]=NULL;
        bsJobType[4]=NULL;
        bsJobType[5]=NULL;
*///        Destroy();

        /*wxString jobTitle=GetJobTitle();
        StringHash jobHash=GetJobControl();
        StringHash jobSurface=GetSurfaceSettings();
        GetUIData()->GetActiveCtrlInst()->SetJobTitle(jobTitle);
        GetUIData()->GetActiveCtrlInst()->SetJobControl(jobHash);
        GetUIData()->GetActiveCtrlInst()->SetJobSurfaceSettings(jobSurface);
        wxGetApp().GetMainFrame()->SetMenuToolStatusCtrlInst();*/
        return true;
}

void SimuJobPnl::OnJobTypeSelChanged(wxCommandEvent &event)
{
        int i;
        bool bShow[7]={true,true,true,true,true,true,true};
        int sel=event.GetSelection();
        wxString defValues[4]={wxT("120"),wxT("1.0e-5"),wxT("100"),wxT("1.0")};
        wxASSERT_MSG(ID_CTRL_JOBTYPE_CHECK_ITERATION-ID_CTRL_JOBTYPE_TEXT_SCF_MAX+1==7,wxT("Job Type Control ID changed!"));
        wxASSERT_MSG(ID_CTRL_JOBTYPE_TEXT_OPT_CRT-ID_CTRL_JOBTYPE_TEXT_SCF_MAX+1==4,wxT("Job Type Input Control ID changed!"));
        //int sel=((wxChoice*)(FindWindow(event.GetId())))->GetCurrentSelection();
        //ResetControls();
        ResetPageControls(PageJobType);
        SetControlValue(event.GetId(),sel);
        wxSizer* bsTop=pNotebook->GetPage(PageJobType)->GetSizer();
        wxASSERT_MSG(ID_CTRL_JOBTYPE_TEXT_OPT_MAX-ID_CTRL_JOBTYPE_TEXT_SCF_MAX==2,wxT("Job Type Input ID Change1!"));
        wxASSERT_MSG(ID_CTRL_JOBTYPE_TEXT_OPT_CRT-ID_CTRL_JOBTYPE_TEXT_SCF_MAX==3,wxT("Job Type Input ID Change2!"));
    switch(sel)
        {
        case JobTypeGM:
        case JobTypeTS:
        case JobTypeOFreq:
        case JobTypeIRC:
                break;
        case JobTypeSP:
        case JobTypeGrad:
        case JobTypeVIB:
        default:
                bShow[2]=false;
                bShow[3]=false;


                break;
        }
        for(i=0;i<4;i++){
                sbsJobTypeText->Show(GetContainSizer(ID_CTRL_JOBTYPE_TEXT_SCF_MAX+i),bShow[i]);
                if(bShow[i]){
                        SetControlValue(ID_CTRL_JOBTYPE_TEXT_SCF_MAX+i,defValues[i]);
                }
        }
        for(i=4;i<7;i++)
                sbsJobTypeOutput->Show(FindWindow(ID_CTRL_JOBTYPE_TEXT_SCF_MAX+i),bShow[i]);
        ChangeJobTypeDisplay();
        bsTop->Layout();
        Refresh();
}

void SimuJobPnl::OnMethodSelChanged(wxCommandEvent &event)
{
        int sel=event.GetSelection();
        //int sel=((wxChoice*)(FindWindow(event.GetId())))->GetCurrentSelection();
        //ResetControls();
        ResetPageControls(PageMethod);
        SetControlValue(event.GetId(),sel);
        wxSizer* bsTop=pNotebook->GetPage(PageMethod)->GetSizer();
        switch(sel)
        {
        case MethodSE://"SE"
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_BASIS_SET),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_DFT_FUNC),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_MODEL),true);
                GetContainSizer(ID_CTRL_METHOD_CHECK_LOCALSCF)->Show(FindWindow(ID_CTRL_METHOD_CHECK_LOCALSCF),true);
                SetControlValue(ID_CTRL_METHOD_CHOICE_MODEL,wxT("PM6"));
                break;
        case MethodHF://"Hartree-Fock"
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_DFT_FUNC),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_MODEL),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_BASIS_SET),true);
                GetContainSizer(ID_CTRL_METHOD_CHECK_LOCALSCF)->Show(FindWindow(ID_CTRL_METHOD_CHECK_LOCALSCF),false);
                SetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET,wxT("3-21G(*)"));
                break;
        case MethodDFT://"DFT"
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_MODEL),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_BASIS_SET),true);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_DFT_FUNC),true);
                GetContainSizer(ID_CTRL_METHOD_CHECK_LOCALSCF)->Show(FindWindow(ID_CTRL_METHOD_CHECK_LOCALSCF),false);
                SetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET,wxT("6-31G*"));
                SetControlValue(ID_CTRL_METHOD_CHOICE_DFT_FUNC,wxT("B3LYP"));
                break;
        case MethodMP2:
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_DFT_FUNC),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_MODEL),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_BASIS_SET),true);
                GetContainSizer(ID_CTRL_METHOD_CHECK_LOCALSCF)->Show(FindWindow(ID_CTRL_METHOD_CHECK_LOCALSCF),false);
                SetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET,wxT("3-21G(*)"));
                break;
        default:
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_DFT_FUNC),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_MODEL),false);
                bsTop->Show(GetContainSizer(ID_CTRL_METHOD_CHOICE_BASIS_SET),true);
                GetContainSizer(ID_CTRL_METHOD_CHECK_LOCALSCF)->Show(FindWindow(ID_CTRL_METHOD_CHECK_LOCALSCF),false);
                SetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET,wxT("3-21G(*)"));
                break;
        }
        ChangeJobTypeDisplay();
        SetControlValue(ID_CTRL_METHOD_TEXT_TOTAL_CHARGE,wxT("0"));
        SetControlValue(ID_CTRL_METHOD_CHOICE_STATE,wxT("Singlet"));
        SendMessage(ID_CTRL_METHOD_CHOICE_STATE,wxT("Singlet"));
        bsTop->Layout();
        Refresh();
}

bool SimuJobPnl::OnBtnSave(void)
{
        wxString jobTitle=GetJobTitle();
        StringHash jobHash=GetJobControl();
        StringHash jobSurface=GetSurfaceSettings();

        SimuJob* pSimuJob = static_cast<SimuJob*>(m_pTreeItem->GetProject()->GetJobInfo());
        if(!pSimuJob) {
                pSimuJob = new SimuJob(m_pTreeItem->GetFileName());
                m_pTreeItem->GetProject()->SetJobInfo(pSimuJob);
        }
        pSimuJob->SetJobTitle(jobTitle);
        pSimuJob->SetJobControl(jobHash);
        pSimuJob->SetJobSurfaceSettings(jobSurface);
        pSimuJob->SaveFile();
        return true;
}

bool SimuJobPnl::OnBtnSubmit(void)
{
        return OnBtnSave();
}

bool SimuJobPnl::OnBtnReset(void)
{
        ResetControls();
        SetDefaults();
        return false;
}
/*
void SimuJobPnl::OnTotalChargeChanged(wxCommandEvent &event)
{
        wxString str=event.GetString();
        long val;
        str.ToLong(&val);
        if(val%2==0)
        {
                ((wxTextCtrl*)(FindWindow(ID_CTRL_METHOD_TEXT_MULTI)))->SetValue(wxT("1"));
        }
        else
        {
                ((wxTextCtrl*)(FindWindow(ID_CTRL_METHOD_TEXT_MULTI)))->SetValue(wxT("2"));
        }
}
*/

void SimuJobPnl::OnGuessSelChanged(wxCommandEvent &event)
{
        int sel=event.GetSelection();

        //int sel=((wxChoice*)(FindWindow(event.GetId())))->GetCurrentSelection();
        //ResetControls();
        ResetPageControls(PageGuess);
        ((wxChoice*)(FindWindow(event.GetId())))->SetSelection(sel);
        wxSizer* bsTop=pNotebook->GetPage(PageGuess)->GetSizer();
        switch(sel)
        {
        case 0://"Core"
        case 1://"Huckel"
                bsTop->Show(sbsGuessOccup,false);
                bsTop->Show(sbsGuessAlter,false);
                break;
        case 2://"READ"
        case 3://"ALTER"
                bsTop->Show(sbsGuessOccup,true);
                bsTop->Show(sbsGuessAlter,true);
                break;
        default:
                break;
        }

        OnMethodStateChangeCommon(GetControlValueInt(ID_CTRL_METHOD_CHOICE_STATE));
        bsTop->Layout();
        Refresh();
}

void SimuJobPnl::OnMethodStateChanged(wxCommandEvent &event)
{
        int sel=event.GetSelection();
        wxSizer* bsTop=pNotebook->GetPage(PageGuess)->GetSizer();
        wxSizer* bsMethod=pNotebook->GetPage(PageMethod)->GetSizer();
        OnMethodStateChangeCommon(sel);
        ChangChargeStateDisplay();
        bsMethod->Layout();
        bsTop->Layout();
        Refresh();
}
void SimuJobPnl::OnSurfaceCheck(wxCommandEvent &event)
{
        wxSizer* bsTop=pNotebook->GetPage(PageSurface)->GetSizer();
        //wxSizer* container=GetContainSizer(ID_CTRL_SURFACE_CHECK_DENSITY);
        bsTop->Show(bsSurface,GetCheckValue(event.GetId()));
        bsTop->Layout();
        Refresh();
}

void SimuJobPnl::OnDFTFuncChanged(wxCommandEvent &event)
{
        ChangeJobTypeDisplay();
}

void SimuJobPnl::OnModelChanged(wxCommandEvent &event)
{
        ChangeJobTypeDisplay();
}

void SimuJobPnl::OnBasisSetChanged(wxCommandEvent &event)
{
        ChangeJobTypeDisplay();
}
//DEL void SimuJobPnl::OnTitleChanged(wxCommandEvent &event)
//DEL {
//DEL         wxString title=((wxTextCtrl*)(FindWindow(ID_CTRL_TITLE_TEXT_TITLE)))->GetValue();
//DEL         title.Replace(wxT("\n"),wxT(" "));
//DEL         stShow[0]->SetLabel(title);
//DEL }
//////////////////////////////////////////////////////////////
//Job Control
///////////////////////////////////////////////////////////////
StringHash SimuJobPnl::GetDefaultJobControl()
{
        StringHash jobCon;
        jobCon[wxT("JobType")]        =wxT("SP");
        jobCon[wxT("Method")]        =wxT("SE");
        jobCon[wxT("SEMod")]        =wxT("AM1");
        jobCon[wxT("Charge")]        =wxT("0");
        jobCon[wxT("State")]        =wxT("Singlet");
        jobCon[wxT("Guess")]        =wxT("Huckel");
        jobCon[wxT("SCF_MAX")]        =wxT("120");
        jobCon[wxT("SCF_CRT")]        =wxT("1.0e-5");
        return jobCon;
}
StringHash SimuJobPnl::GetJobControl(void)
{
        StringHash jobCon;
        wxString keywords,value;
        int sel=GetControlValueInt(ID_CTRL_JOBTYPE_CHOICE_JOBTYPE);
        bool bShow[4]={true,true,true,true};
        bool checked;
        wxString jobTypes[JobTypeMax]={ wxT("SP"),
                                                                        wxT("GRADIENTS"),
                                                                        wxT("OPT"),
                                                                        wxT("TS"),
                                                                        wxT("FREQ"),
                                                                        wxT("OPT-FREQ"),
                                                                        wxT("IRC")};
        wxString keys[4]={        wxT("SCF_MAX"),
                                                wxT("SCF_CRT"),
                                                wxT("OPT_MAX"),
                                                wxT("OPT_CRT")};
        wxString chKeys[3]={wxT("PRT_VECTS"),
                                                wxT("PRT_BONDS"),
                                                wxT("PRT_ITER")};
        int i;
        switch(sel)
        {
        case JobTypeGM:
        case JobTypeTS:
        case JobTypeOFreq:
        case JobTypeIRC:
                break;
        case JobTypeSP:
        case JobTypeGrad:
        case JobTypeVIB:
        default:
                bShow[2]=false;
                bShow[3]=false;
                break;
        }
        jobCon[wxT("JobType")]                =jobTypes[sel];
        for(i=0; i<4 ; i++)
                if(bShow[i])
                        jobCon[keys[i] ]        =GetControlValue(ID_CTRL_JOBTYPE_TEXT_SCF_MAX+i);
        sel=GetControlValueInt(ID_CTRL_METHOD_CHOICE_METHOD);
        //wxMessageBox(GetControlValue(ID_CTRL_METHOD_CHOICE_METHOD,Choice));
        switch(sel)  //selection of "Method"
        {
        case MethodSE: //add the choice of SE
                jobCon[wxT("Method")]                =wxT("SE");
                jobCon[wxT("SEMod")]                =GetControlValue(ID_CTRL_METHOD_CHOICE_MODEL);
            checked=GetCheckValue(ID_CTRL_METHOD_CHECK_LOCALSCF);
            if(checked){
                    jobCon[wxT("LOCSCF")]=wxEmptyString;
            }
                break;
        case MethodHF://"Hartree-Fock"
                jobCon[wxT("Method")]                =wxT("HF");
                jobCon[wxT("BasisSet")]                =GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
                break;
        case MethodDFT:
                jobCon[wxT("Method")]                =wxT("DFT");
                jobCon[wxT("BasisSet")]                        =GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
                jobCon[wxT("DFTFunc")]                =GetControlValue(ID_CTRL_METHOD_CHOICE_DFT_FUNC);
                break;
        case MethodMP2:
                jobCon[wxT("Method")]                =GetControlValue(ID_CTRL_METHOD_CHOICE_METHOD);
                jobCon[wxT("BasisSet")]                =GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
                break;
        default:
                jobCon[wxT("Method")]                =GetControlValue(ID_CTRL_METHOD_CHOICE_METHOD);
                jobCon[wxT("BasisSet")]                =GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
                break;
        }
        jobCon[wxT("Charge")]        =GetControlValue(ID_CTRL_METHOD_TEXT_TOTAL_CHARGE);
        jobCon[wxT("State")]        =GetControlValue(ID_CTRL_METHOD_CHOICE_STATE);
        jobCon[wxT("Guess")]        =GetControlValue(ID_CTRL_GUESS_CHOICE_SCF_GUESS);
        jobCon[wxT("KeyWords")]        =GetControlValue(ID_CTRL_TEXT_KEYWORDS);;
        //wxMessageBox(keywords);
        for(i=0 ; i<3 ;i ++){
                checked=GetCheckValue(ID_CTRL_JOBTYPE_CHECK_ORBITAL+i);
                if(checked){
                        jobCon[chKeys[i]]=wxEmptyString;
                }
        }
        return jobCon;
}
void SimuJobPnl::SetJobControl(StringHash &jobControl)
{
        int i,sel=-1;
        wxString keywords,value;
        bool bShow[4]={true,true,true,true};
        wxString jobTypes[JobTypeMax]={ wxT("SP"),
                                                                        wxT("GRADIENTS"),
                                                                        wxT("OPT"),
                                                                        wxT("TS"),
                                                                        wxT("FREQ"),
                                                                        wxT("OPT-FREQ"),
                                                                        wxT("IRC")};
        wxString keys[4]={        wxT("SCF_MAX"),
                                                wxT("SCF_CRT"),
                                                wxT("OPT_MAX"),
                                                wxT("OPT_CRT")};
        wxString chKeys[3]={wxT("PRT_VECTS"),
                                                wxT("PRT_BONDS"),
                                                wxT("PRT_ITER")};
        if(jobControl.find(wxT("JobType"))!=jobControl.end()){
                for(i=0;i<JobTypeMax;i++){
                        if(jobTypes[i].Cmp(jobControl.find(wxT("JobType"))->second)==0)        {
                                SetControlValue(ID_CTRL_JOBTYPE_CHOICE_JOBTYPE,jobTypes[i]);
                                SendMessage(ID_CTRL_JOBTYPE_CHOICE_JOBTYPE,jobTypes[i]);
                                sel=i;
                                break;
                        }
                }
        }
        switch(sel)
        {
        case JobTypeGM:
        case JobTypeTS:
        case JobTypeOFreq:
        case JobTypeIRC:
                break;
        case JobTypeSP:
        case JobTypeGrad:
        case JobTypeVIB:
        default:
                bShow[2]=false;
                bShow[3]=false;
                break;
        }
        for(i=0; i<4 ; i++){
                if(bShow[i]){
                        if(jobControl.find(keys[i])!=jobControl.end()){
                                SetControlValue(ID_CTRL_JOBTYPE_TEXT_SCF_MAX+i,jobControl.find(keys[i])->second);
                        }
                }
        }
        if(jobControl.find(wxT("Method"))!=jobControl.end()){
                if(jobControl.find(wxT("Method"))->second.Cmp(wxT("HF"))==0){
                        SetControlValue(ID_CTRL_METHOD_CHOICE_METHOD,MethodHF);
                        SendMessage(ID_CTRL_METHOD_CHOICE_METHOD,MethodHF);
                }else if(jobControl.find(wxT("Method"))->second.Cmp(wxT("SE"))==0){
                        SetControlValue(ID_CTRL_METHOD_CHOICE_METHOD,MethodSE);
                        SendMessage(ID_CTRL_METHOD_CHOICE_METHOD,MethodSE);
                }else{
                        SetControlValue(ID_CTRL_METHOD_CHOICE_METHOD,jobControl.find(wxT("Method"))->second);
                        SendMessage(ID_CTRL_METHOD_CHOICE_METHOD,
                                                                        jobControl.find(wxT("Method"))->second);
                }
        }
        if(jobControl.find(wxT("BasisSet"))!=jobControl.end())        {
                SetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET,jobControl.find(wxT("BasisSet"))->second);
                SendMessage(ID_CTRL_METHOD_CHOICE_BASIS_SET,
                                                                jobControl.find(wxT("BasisSet"))->second);
        }
        if(jobControl.find(wxT("SEMod"))!=jobControl.end())
                SetControlValue(ID_CTRL_METHOD_CHOICE_MODEL,jobControl.find(wxT("SEMod"))->second);
                //jobCon[wxT("SEMod")]=((wxChoice*)(FindWindow(ID_CTRL_METHOD_CHOICE_MODEL)))->GetStringSelection();
        if(jobControl.find(wxT("Charge"))!=jobControl.end()){
                SetControlValue(ID_CTRL_METHOD_TEXT_TOTAL_CHARGE,jobControl.find(wxT("Charge"))->second);
                SetControlValue(ID_CTRL_METHOD_SPIN_TOTAL_CHARGE,jobControl.find(wxT("Charge"))->second);
        }
        if(jobControl.find(wxT("State"))!=jobControl.end()){
                SetControlValue(ID_CTRL_METHOD_CHOICE_STATE,jobControl.find(wxT("State"))->second);
                SendMessage(ID_CTRL_METHOD_CHOICE_STATE,jobControl.find(wxT("State"))->second);
        }
        if(jobControl.find(wxT("Guess"))!=jobControl.end())        {
                SetControlValue(ID_CTRL_GUESS_CHOICE_SCF_GUESS,jobControl.find(wxT("Guess"))->second);
                SendMessage(ID_CTRL_GUESS_CHOICE_SCF_GUESS,
                                                                jobControl.find(wxT("Guess"))->second);
        }
        if(jobControl.find(wxT("KeyWords"))!=jobControl.end()){
                wxString keywords=jobControl.find(wxT("KeyWords"))->second;
                keywords.Replace(wxT(","),wxT(" "));
                SetControlValue(ID_CTRL_TEXT_KEYWORDS,keywords);
        }

        for(i=0 ; i<3 ;i ++){
                if(jobControl.find(chKeys[i])!=jobControl.end()){
                        SetControlValue(ID_CTRL_JOBTYPE_CHECK_ORBITAL+i,true);
                }
        }
        if(jobControl.find(wxT("LOCSCF"))!=jobControl.end()){
                SetControlValue(ID_CTRL_METHOD_CHECK_LOCALSCF,true);
        }
        Refresh();
}


void SimuJobPnl::SetDefaults()
{
        SendMessage(ID_CTRL_JOBTYPE_CHOICE_JOBTYPE,0);
        SendMessage(ID_CTRL_METHOD_CHOICE_METHOD,0);
        //SendMessage(ID_CTRL_METHOD_CHOICE_BASIS_SET,1);
        SendMessage(ID_CTRL_GUESS_CHOICE_SCF_GUESS,1);
        SetControlValue(ID_CTRL_JOBTYPE_CHOICE_JOBTYPE,0);
        SetControlValue(ID_CTRL_METHOD_CHOICE_METHOD,0);
        //SetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET,1);
        SetControlValue(ID_CTRL_GUESS_CHOICE_SCF_GUESS,1);
        SetControlValue(ID_CTRL_METHOD_TEXT_TOTAL_CHARGE,wxT("0"));
        SetControlValue(ID_CTRL_METHOD_CHOICE_STATE,wxT("Singlet"));
        SetControlValue(ID_CTRL_SURFACE_CHECK_SURFACE,false);
        SendMessage(ID_CTRL_SURFACE_CHECK_SURFACE,false);
}
////////////////////////////////////////////////////////////////
//GUI Control
////////////////////////////////////////////////////////////////
void SimuJobPnl::ChangeJobTypeDisplay()
{
        wxString jobType=wxEmptyString;
        wxString jobTypes[JobTypeMax]={ wxT("SP/"),
                                                                        wxT("Gradients/"),
                                                                        wxT("OPT/"),
                                                                        wxT("TS/"),
                                                                        wxT("Freq/"),
                                                                        wxT("Opt-Freq/"),
                                                                        wxT("IRC/")};
        int sel=GetControlValueInt(ID_CTRL_JOBTYPE_CHOICE_JOBTYPE);
        jobType=jobTypes[sel];
        sel=GetControlValueInt(ID_CTRL_METHOD_CHOICE_METHOD);
        switch(sel)
        {
        case MethodSE:
                jobType=wxT("SE/")+GetControlValue(ID_CTRL_METHOD_CHOICE_MODEL);
                break;
        case MethodHF://"Hartree-Fock"
                jobType=wxT("HF/")+GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
                break;
        case MethodDFT:
                jobType=GetControlValue(ID_CTRL_METHOD_CHOICE_DFT_FUNC)+wxT("/")+GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
                break;
        default:
                jobType=GetControlValue(ID_CTRL_METHOD_CHOICE_METHOD)+wxT("/")+GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
                break;
        }
//        jobType=GetControlValue(ID_CTRL_METHOD_CHOICE_METHOD)+wxT("/")+GetControlValue(ID_CTRL_METHOD_CHOICE_BASIS_SET);
        stJobTypeDis->SetLabel(jobType);
}

void SimuJobPnl::ResetPageControls(int pageIndex)
{
        long id,start,end;
        switch(pageIndex)
        {
        case PageJobType:
                start=ID_CTRL_JOBTYPE_CHOICE_JOBTYPE;
                end=ID_CTRL_JOBTYPE_CHECK_ITERATION;
                break;
        case PageMethod:
                start=ID_CTRL_METHOD_CHOICE_METHOD;
                end=ID_CTRL_METHOD_CHOICE_STATE;
                break;
        case PageGuess:
                start=ID_CTRL_GUESS_CHOICE_SCF_GUESS;
                end=ID_CTRL_GUESS_TEXT_ALTER_BETA;
                break;
        case PageSurface:
                start=ID_CTRL_SURFACE_CHECK_SURFACE;
                end=ID_CTRL_SURFACE_SPINBTN_ORBIT_NUM;
                break;
        default:
                return;
        }
        for(id=start; id<=end; id++)
                ResetControl(id);
}
void SimuJobPnl::ResetControls()
{
        ResetPageControls(PageJobType);
        ResetPageControls(PageMethod);
        ResetPageControls(PageGuess);
        ResetPageControls(PageSurface);
        ResetControl(ID_CTRL_TITLE_TEXT_TITLE);
}

void SimuJobPnl::OnMethodStateChangeCommon(long value)
{
        int sel=GetControlValueInt(ID_CTRL_GUESS_CHOICE_SCF_GUESS);
        if(sel==0 || sel ==1)
                return;
        switch(value)
        {
        case 1:
                sbsGuessOccup->Show(GetContainSizer(ID_CTRL_GUESS_TEXT_OCCU_BETA),false);
                sbsGuessAlter->Show(GetContainSizer(ID_CTRL_GUESS_TEXT_ALTER_BETA),false);
                GetContainSizer(ID_CTRL_GUESS_TEXT_OCCU_ALPHA)->Show(stGuessOccuAlpha,false);
                GetContainSizer(ID_CTRL_GUESS_TEXT_ALTER_ALPHA)->Show(stGuessAlteAlpha,false);
                break;
        case 2:
                sbsGuessOccup->Show(GetContainSizer(ID_CTRL_GUESS_TEXT_OCCU_BETA),true);
                sbsGuessAlter->Show(GetContainSizer(ID_CTRL_GUESS_TEXT_ALTER_BETA),true);
                GetContainSizer(ID_CTRL_GUESS_TEXT_OCCU_ALPHA)->Show(stGuessOccuAlpha,true);
                GetContainSizer(ID_CTRL_GUESS_TEXT_ALTER_ALPHA)->Show(stGuessAlteAlpha,true);
                break;
        default:
                break;
        }
}

void SimuJobPnl::ChangChargeStateDisplay()
{
        wxString chargeMulti=GetControlValue(ID_CTRL_METHOD_TEXT_TOTAL_CHARGE);
        chargeMulti+=wxT("/")+wxString::Format(wxT("%d"),GetControlValueInt(ID_CTRL_METHOD_CHOICE_STATE)+1);
        stCharMultDis->SetLabel(chargeMulti);
}

wxString   SimuJobPnl::GetControlValue(long id)
{
        wxString type=GetControlType(id);
        wxString value=wxEmptyString;
        if(type.IsEmpty())
                return wxEmptyString;
        type.MakeUpper();
        if(type.Cmp(wxT("WXTEXTCTRL"))==0){
                value=((wxTextCtrl*)(FindWindow(id)))->GetValue();
        }else if(type.Cmp(wxT("WXCHECKBOX"))==0){
                if(((wxCheckBox*)(FindWindow(id)))->IsChecked())
                        value=wxT("TRUE");
                else
                        value=wxT("FALSE");
        }else if(type.Cmp(wxT("WXCHOICE"))==0){
                value=((wxChoice*)(FindWindow(id)))->GetStringSelection();
        }else if(type.Cmp(wxT("WXRADIOBOX"))==0){
                value=((wxRadioBox*)(FindWindow(id)))->GetStringSelection();
        }
        return value;
}

int SimuJobPnl::GetControlValueInt( long id)
{
        wxString type=GetControlType(id);
        int value=wxNOT_FOUND;
        long lvalue;
        if(type.IsEmpty())
                return wxNOT_FOUND;
        type.MakeUpper();
        if(type.Cmp(wxT("WXCHECKBOX"))==0){
                value=((wxCheckBox*)(FindWindow(id)))->GetValue();
        }else if(type.Cmp(wxT("WXCHOICE"))==0){
                value=((wxChoice*)(FindWindow(id)))->GetCurrentSelection();
        }else if(type.Cmp(wxT("WXTEXTCTRL"))==0){
                ((wxTextCtrl*)(FindWindow(id)))->GetValue().ToLong(&lvalue);
                value=(int)lvalue;
        }else if(type.Cmp(wxT("WXRADIOBOX"))==0){
                value=((wxRadioBox*)(FindWindow(id)))->GetSelection();
        }else if(type.Cmp(wxT("WXSPINBUTTON"))==0){
                value=((wxSpinButton*)(FindWindow(id)))->GetValue();
        }
        return value;
}

void SimuJobPnl::SetControlValue( long id, wxString value)
{
        wxString type=GetControlType(id);
        if(type.IsEmpty())
                return;
        long lvalue;
        type.MakeUpper();
        if(type.Cmp(wxT("WXTEXTCTRL"))==0){
                ((wxTextCtrl*)(FindWindow(id)))->SetValue(value);
        }else if(type.Cmp(wxT("WXCHECKBOX"))==0){
                if(value.Cmp(wxT("TRUE"))==0)
                        ((wxCheckBox*)(FindWindow(id)))->SetValue(true);
                else
                        ((wxCheckBox*)(FindWindow(id)))->SetValue(false);
        }else if(type.Cmp(wxT("WXCHOICE"))==0){
                ((wxChoice*)(FindWindow(id)))->SetStringSelection(value);
        }else if(type.Cmp(wxT("WXRADIOBOX"))==0){
                ((wxRadioBox*)(FindWindow(id)))->SetStringSelection(value);
        }else if(type.Cmp(wxT("WXSPINBUTTON"))==0){
                value.ToLong(&lvalue);
                ((wxSpinButton*)(FindWindow(id)))->SetValue((int)lvalue);
        }
}

void SimuJobPnl::SetControlValue( long id, int value)
{
        wxString type=GetControlType(id);
        if(type.IsEmpty())
                return;
        type.MakeUpper();
        if(type.Cmp(wxT("WXTEXTCTRL"))==0){
                ((wxTextCtrl*)(FindWindow(id)))->SetValue(wxString::Format(wxT("%d"),value));
        }else if(type.Cmp(wxT("WXCHECKBOX"))==0){
                ((wxCheckBox*)(FindWindow(id)))->SetValue(value!=0);
        }else if(type.Cmp(wxT("WXCHOICE"))==0){
                ((wxChoice*)(FindWindow(id)))->SetSelection(value);
        }else if(type.Cmp(wxT("WXRADIOBOX"))==0){
                ((wxRadioBox*)(FindWindow(id)))->SetSelection(value);
        }else if(type.Cmp(wxT("WXSPINBUTTON"))==0){
                ((wxSpinButton*)(FindWindow(id)))->SetValue(value);
        }
}

wxString SimuJobPnl::GetControlType( long id)
{
        if(FindWindow(id)==NULL)
                return wxEmptyString;
        return FindWindow(id)->GetClassInfo()->GetClassName();
}

wxSizer* SimuJobPnl::GetContainSizer( long id)
{
        if(FindWindow(id)==NULL)
                return NULL;
        return FindWindow(id)->GetContainingSizer();
}

bool SimuJobPnl::GetCheckValue(long id)
{
        wxString type=GetControlType(id).Upper();
        wxASSERT_MSG(type.Cmp(wxT("WXCHECKBOX"))==0,wxT("Control Type is not CheckBox!"));
        return ((wxCheckBox*)(FindWindow(id)))->IsChecked();
}

void SimuJobPnl::ResetControl(long id)
{
        wxString type=GetControlType(id);
        if(type.IsEmpty())
                return;
        type.MakeUpper();
        if(type.Cmp(wxT("WXTEXTCTRL"))==0){
                ((wxTextCtrl*)(FindWindow(id)))->Clear();
        }else if(type.Cmp(wxT("WXCHECKBOX"))==0){
                ((wxCheckBox*)(FindWindow(id)))->SetValue(false);
        }else if(type.Cmp(wxT("WXCHOICE"))==0){
                ((wxChoice*)(FindWindow(id)))->SetSelection(wxNOT_FOUND);
        }else if(type.Cmp(wxT("WXRADIOBOX"))==0){
                ((wxRadioBox*)(FindWindow(id)))->SetSelection(0);
        }else if(type.Cmp(wxT("WXSPINBUTTON"))==0){
                ((wxSpinButton*)(FindWindow(id)))->SetValue(0);
        }
}

void SimuJobPnl::OnSpinButton(wxSpinEvent &event)
{
        SetControlValue(ID_CTRL_SURFACE_TEXT_ORBIT_NUM,event.GetInt());
}

void SimuJobPnl::SendMessage(long id, int value)
{
        wxString type=GetControlType(id);
        if(type.IsEmpty())
                return;
        type.MakeUpper();
        if(type.Cmp(wxT("WXCHOICE"))==0){
                wxCommandEvent choEvent(wxEVT_COMMAND_CHOICE_SELECTED,id);
                choEvent.SetInt(value);
                GetEventHandler()->ProcessEvent(choEvent);
        }else if(type.Cmp(wxT("WXCHECKBOX"))==0){
                wxCommandEvent chkEvent(wxEVT_COMMAND_CHECKBOX_CLICKED,id);
                chkEvent.SetInt(value);
                GetEventHandler()->ProcessEvent(chkEvent);
/*        }else if(type.Cmp(wxT("WXTEXTCTRL"))==0){

        }else if(type.Cmp(wxT("WXRADIOBOX"))==0){
*/}else{
                wxMessageBox(type);
        }
}

int SimuJobPnl::GetChoiceItemIndex(long id, wxString value)
{
        wxString type=GetControlType(id);
        if(type.IsEmpty())
                return wxNOT_FOUND;
        type.MakeUpper();
        wxASSERT_MSG(type.Cmp(wxT("WXCHOICE"))==0,wxT("Control Type is not Choice!"));
        return ((wxChoice*)(FindWindow(id) ) )->FindString(value);
}

void SimuJobPnl::SendMessage(long id, wxString value)
{
        wxString type=GetControlType(id);
        if(type.IsEmpty())
                return;
        type.MakeUpper();
        if(type.Cmp(wxT("WXCHOICE"))==0){
                wxCommandEvent choEvent(wxEVT_COMMAND_CHOICE_SELECTED,id);
                //choEvent.SetInt(GetChoiceItemIndex(id,value));
                choEvent.SetString(value);
        GetEventHandler()->ProcessEvent(choEvent);
        }else if(type.Cmp(wxT("WXTEXTCTRL"))==0){
                wxCommandEvent txtEvent(wxEVT_COMMAND_TEXT_UPDATED,id);
                GetEventHandler()->ProcessEvent(txtEvent);
/*        }else if(type.Cmp(wxT("WXCHECKBOX"))==0){

        }else if(type.Cmp(wxT("WXRADIOBOX"))==0){
*/}else{
                wxMessageBox(type);
        }
}

StringHash SimuJobPnl::GetSurfaceSettings()
{
        StringHash surfaceSet;
        bool checked;
        bool isCheckSurface        =GetCheckValue(ID_CTRL_SURFACE_CHECK_SURFACE);
        if(isCheckSurface)
        {
                surfaceSet[wxT("Format")]                =GetControlValue(ID_CTRL_SURFACE_RADIO_FILE_FORMAT);
                checked=GetCheckValue(ID_CTRL_SURFACE_CHECK_DENSITY);
                surfaceSet[wxT("Density")]                =checked ? wxT("yes") :wxT("no");
                surfaceSet[wxT("Resolution")]        =GetControlValue(ID_CTRL_SURFACE_RADIO_RESOLUTION);
                surfaceSet[wxT("Number")]                =GetControlValue(ID_CTRL_SURFACE_TEXT_ORBIT_NUM);
        }
        return surfaceSet;
}

wxString SimuJobPnl::GetJobTitle()
{
        return tcTitle->GetValue();
}

void SimuJobPnl::SetJobTitle(wxString title)
{
        tcTitle->SetValue(title);
}

void SimuJobPnl::SetSurfaceSettings(StringHash &sufaceSet)
{
        bool checked;
        wxString value;
        if(sufaceSet.size()>0)
        {
                SetControlValue(ID_CTRL_SURFACE_CHECK_SURFACE,true);
                SendMessage(ID_CTRL_SURFACE_CHECK_SURFACE,(int)true);
                StringHash::iterator i=sufaceSet.find(wxT("Format"));
                if(i!=sufaceSet.end())
                SetControlValue(ID_CTRL_SURFACE_RADIO_FILE_FORMAT,sufaceSet.find(wxT("Format"))->second);
                value=sufaceSet.find(wxT("Density"))->second.Upper();
                if(value.Cmp(wxT("YES"))==0)
                        checked=true;
                else
                        checked=false;
                SetControlValue(ID_CTRL_SURFACE_CHECK_DENSITY,checked);
                SetControlValue(ID_CTRL_SURFACE_RADIO_RESOLUTION,StringUtil::UpperFirstChar(sufaceSet.find(wxT("Resolution"))->second));
                SetControlValue(ID_CTRL_SURFACE_TEXT_ORBIT_NUM,sufaceSet.find(wxT("Number"))->second);
                SetControlValue(ID_CTRL_SURFACE_SPINBTN_ORBIT_NUM,sufaceSet.find(wxT("Number"))->second);
        }
}

void SimuJobPnl::OnTotalChargeSpinChanged(wxSpinEvent &event)
{
        SetControlValue(ID_CTRL_METHOD_TEXT_TOTAL_CHARGE,event.GetInt());
}
