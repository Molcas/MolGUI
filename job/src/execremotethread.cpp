/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "execremotethread.h"
#include "tools.h"
#include "sshclient.h"
#include "xmlfile.h"
#include "envutil.h"
#include "stringutil.h"
#include <wx/filename.h>
#include <wx/arrimpl.cpp>
#include "dlgmasterkey.h"
#include "manager.h"
#include "httppost.h"
#include "dlglogin.h"
#include <wx/textfile.h>
#include <wx/tokenzr.h>
#include "messageevent.h"
#include "simuproject.h"

RemoteThreadData::RemoteThreadData(JobRunningData *pJobData) {
    m_JobData = pJobData;
    m_remoteThread = NULL;
    threadId = -1;
    pfLog = NULL;
    m_jobinfo = NULL;
    m_sshclient = NULL;
}

RemoteThreadData::~RemoteThreadData() {
    if(pfLog) fclose(pfLog);
}

bool RemoteThreadData::RunThread(void) {
    m_jobinfo = new RemoteJobInfo();
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
    if(wxFileExists(fileName)) {
        wxString strLine;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        strLine = tis.ReadLine();
        //strLine.ToLong(&remotejob);
        strLine = tis.ReadLine();
        //m_fileName;
        strLine = tis.ReadLine();
        m_jobinfo->fullname = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->shortname = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->username = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->password = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->command = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->scriptfile = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->protocal = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->submitway = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->remoteworkbase = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->masterid = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->masterkey = strLine;
        strLine = tis.ReadLine();
        m_jobinfo->machineid = strLine;

    }


    m_jobinfo->jobname = GetJobName();
    //m_jobinfo->password=EnvUtil::GetDecipherCode(m_jobinfo->password);
    m_jobinfo->inputfile = GetInputFile();
    m_jobinfo->submittime = wxNow();
    m_jobinfo->status = wxT("Queue Wait");
    m_jobinfo->ticket = GetJobName() + StringUtil::GetCurTime(1);
    //wxMessageBox(m_jobinfo->ticket);
    wxFileName finputfile(m_jobinfo->inputfile);
    m_jobinfo->jobpath = finputfile.GetPath(wxPATH_GET_VOLUME , wxPATH_NATIVE);
    m_jobinfo->projectfile = GetProjectFile();
    m_jobinfo->projecttype = GetProjectType();
    m_sshclient = NULL;
    if(m_jobinfo->protocal.Cmp(wxT("HTTP")) != 0) {

        m_sshclient = new SSHClient();

        //char * input=NULL;
        //StringUtil::String2CharPointer(&input,m_jobinfo->fullname);
        //fprintf(stderr, input);
        EnvUtil::SetMasterId(m_jobinfo->masterid);
        while(EnvUtil::GetMasterKey().IsEmpty() || m_jobinfo->masterkey.Cmp(EnvUtil::GetMasterKeyMD5()) != 0) {
            DlgMasterKey *pdlgmasterkey = new DlgMasterKey(Manager::Get()->GetAppWindow());
            pdlgmasterkey->SetHostname(m_jobinfo->masterid);
            if(pdlgmasterkey->ShowModal() == wxID_CANCEL)return -1;
            //wxMessageBox(m_machineinfoarray[selmachine].masterkey);
            //wxMessageBox(EnvUtil::GetMasterKeyMD5());
            pdlgmasterkey = NULL;

        }

        //m_sshclient->SetMasterkey(m_jobinfo->masterkey);
        m_sshclient->SetHostname(m_jobinfo->fullname);
        //StringUtil::String2CharPointer(&input,m_jobinfo->username);
        //fprintf(stderr, input);
        m_sshclient->SetUsername(m_jobinfo->username);
        //StringUtil::String2CharPointer(&input,m_jobinfo->password);
        //fprintf(stderr, input);
        m_sshclient->SetPassword(m_jobinfo->password);

        while(m_sshclient->ConnectToHost() != 1) {
            DlgLogin *dlglogin = new DlgLogin(Manager::Get()->GetAppWindow());
            dlglogin->SetInfo(m_sshclient);
            dlglogin->ShowModal();
            dlglogin = NULL;
            if(!m_sshclient->GetRetry()) {
                wxMessageBox(wxT("Connect failed......"));
                return false;
            } else {
                m_jobinfo->masterkey = m_sshclient->GetMasterkey();
            }
        }
    }
    JobMonitorPnl *pjobmonitorpnl = Manager::Get()->GetMonitorPnl();
    m_remoteThread = new ExecRemoteThread(this, m_sshclient, m_jobinfo, pjobmonitorpnl);
    threadId = m_remoteThread->GetId();
    if(m_remoteThread->Create() != wxTHREAD_NO_ERROR) {
        Tools::ShowError(wxT("Sorry, cannot create thread to submit job!"));
        return false;
    }
    if(m_remoteThread->Run() != wxTHREAD_NO_ERROR) {
        Tools::ShowError(wxT("Sorry, cannot start thread to submit job!"));
        return false;
    }
    return true;

}

bool RemoteThreadData::KillThread(void) {
    return true;
}

/*
size_t RemoteThreadWrite(void *buffer, size_t size, size_t nmemb, void *stream) {
        RemoteThreadData* threadData = (RemoteThreadData*)stream;
        if(threadData) {
                if(threadData->pfLog == NULL) {
                        threadData->pfLog = fopen("test1.log", "wa");
                        if(threadData->pfLog == NULL) {
                                return size * nmemb;                //cannot open log file to write
                        }
                }
                printf("\n====================buffer size: %d\n", size * nmemb);
                fwrite(buffer, size, nmemb, stdout);
                size_t iSize = fwrite(buffer, size, nmemb, threadData->pfLog);
                fflush(threadData->pfLog);
                return iSize;
        }
        return size * nmemb;
}

*/

ExecRemoteThread::ExecRemoteThread(RemoteThreadData *threadData, SSHClient *sshclient, RemoteJobInfo *jobinfo, JobMonitorPnl *pjobmonitorpnl) : wxThread(wxTHREAD_JOINABLE) {
    m_threadData = threadData;
    m_status = REMOTE_INIT;
    m_sshclient = sshclient;
    m_jobinfo = jobinfo;
    m_pJobMonitorPnl = pjobmonitorpnl;
    /*
        while(EnvUtil::GetMasterKey().IsEmpty()){
        DlgMasterKey *pdlgmasterkey=new DlgMasterKey(Manager::Get()->GetAppWindow());
        if(pdlgmasterkey->ShowModal()==wxID_CANCEL)return;
        pdlgmasterkey=NULL;
        }

    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
    XmlFile jobinfo(wxT("job-information"),fileName);
    m_jobinfo->jobname=m_threadData->GetJobName();
    m_jobinfo->fullname=jobinfo.GetString("HostName");
    m_jobinfo->username=jobinfo.GetString("UserName");
    m_jobinfo->password=jobinfo.GetString("PassWord");
    //wxMessageBox(m_jobinfo->password);
    m_jobinfo->password=EnvUtil::GetDecipherCode(m_jobinfo->password);
    //wxMessageBox(m_jobinfo->password);
    m_jobinfo->command=jobinfo.GetString("Command");
    m_jobinfo->inputfile=m_threadData->GetInputFile();
    m_jobinfo->submittime=wxNow();
    m_jobinfo->status=wxT("In Queue");
    m_jobinfo->ticket=m_threadData->GetJobName()+StringUtil::GetCurTime(1);
    //wxMessageBox(m_jobinfo->ticket);
    m_jobinfo->scriptfile=jobinfo.GetString("ScriptFile");
    m_jobinfo->protocal=jobinfo.GetString("Protocal");
    m_jobinfo->submitway=jobinfo.GetString("SubmitWay");
    wxFileName finputfile(m_jobinfo->inputfile);
    m_jobinfo->jobpath=finputfile.GetPath(wxPATH_GET_VOLUME ,wxPATH_NATIVE);
    m_jobinfo->projectfile=m_threadData->GetProjectFile();
    //wxMessageBox(m_jobinfo->jobpath);
    //        m_pCurl = NULL;
    */
}

wxArrayString ExecRemoteThread::PrepareInputFile(wxString inputfile)
{
    wxArrayString strarray;
    wxArrayString strreturn;
    //wxFileName finputfile(inputfile);
    wxFileName *pxyzfile;
    wxString tmpinputfile = inputfile + wxT(".tmp");
    wxString bakinputfile = inputfile + wxT(".bak");
    if(!wxFileExists(bakinputfile))
        wxCopyFile(inputfile, bakinputfile, true);
    else
        wxCopyFile(bakinputfile, inputfile, true);

    //wxCopyFile(inputfile,tmpinputfile,true);

    wxFileInputStream fis(inputfile);
    wxTextInputStream tis(fis);
    wxString strLine;
    wxString xyzfile;
    bool isfile = false;
#ifdef __DEBUG__
    fprintf(stderr, "PrepareInputFile");
#endif
    while(!fis.Eof()) {
        strLine = tis.ReadLine();
        strLine.Trim();
        strLine.Trim(false);
        //wxMessageBox(strLine);
        if(isfile) {
            isfile = false;
            xyzfile = strLine;
            pxyzfile = new wxFileName(xyzfile);
            strLine = pxyzfile->GetFullName();
            strreturn.Add(xyzfile);
        } else if(strLine.Cmp(wxT("LOAD")) == 0 || strLine.Cmp(wxT("FILELOAD")) == 0 || strLine.Cmp(wxT("FILE")) == 0 || strLine.Cmp(wxT("HEXT")) == 0 || strLine.Cmp(wxT("POTE")) == 0 || strLine.Cmp(wxT("OBSE")) == 0) {
            isfile = true;
        } else if(strLine.Left(5).Cmp(wxT("COORD")) == 0) {
            xyzfile = strLine.Right(strLine.Len() - 6);
            pxyzfile = new wxFileName(xyzfile);
            strLine = wxT("COORD=") + pxyzfile->GetFullName();
            strreturn.Add(xyzfile);
        } else if(strLine.Left(1).Cmp(wxT("/")) == 0 && strLine.Left(2).Cmp(wxT("/*")) != 0 ) {
            xyzfile = strLine;
            pxyzfile = new wxFileName(xyzfile);
            strLine = pxyzfile->GetFullName();
            strreturn.Add(xyzfile);
        } else {
            int pos = 0;
            pos = StringUtil::StringMatch(strLine, wxT(">include "));
            if(pos >= 0) {
                wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                xyzfile = strArr[0];
                pxyzfile = new wxFileName(xyzfile);
                strLine = wxT(">>>> include ") + pxyzfile->GetFullName();
                strreturn.Add(xyzfile);
            } else {
                pos = StringUtil::StringMatch(strLine, wxT(">copy "));
                if(pos >= 0) {
                    wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                    xyzfile = strArr[0];
                    pxyzfile = new wxFileName(xyzfile);
                    strLine = wxT(">>>>>copy ") + pxyzfile->GetFullName();
                    strreturn.Add(xyzfile);
                } else {
                    pos = StringUtil::StringMatch(strLine, wxT(">link -force "));
                    if(pos >= 0) {
                        wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                        xyzfile = strArr[0];
                        pxyzfile = new wxFileName(xyzfile);
                        strLine = wxT(">>>>>link -force ") + pxyzfile->GetFullName();
                        strreturn.Add(xyzfile);
                    } else {
                        pos = StringUtil::StringMatch(strLine, wxT(">link force "));
                        if(pos >= 0) {
                            wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                            xyzfile = strArr[0];
                            pxyzfile = new wxFileName(xyzfile);
                            strLine = wxT(">>>>>link force ") + pxyzfile->GetFullName();
                            strreturn.Add(xyzfile);
                        } else {
                            pos = StringUtil::StringMatch(strLine, wxT(">link "));
                            if(pos >= 0) {
                                wxArrayString strArr = wxStringTokenize(strLine.Right(strLine.Len() - pos), wxT(" \t\n\r"), wxTOKEN_STRTOK);
                                xyzfile = strArr[0];
                                pxyzfile = new wxFileName(xyzfile);
                                strLine = wxT(">>>>>link ") + pxyzfile->GetFullName();
                                strreturn.Add(xyzfile);
                            }
                        }
                    }
                }
            }
        }
        strarray.Add(strLine);
    }
    wxFileOutputStream fos(tmpinputfile);
    wxTextOutputStream tos(fos);
    //wxMessageBox(wxT("output"));
    for(unsigned int i = 0; i < strarray.GetCount(); i++) {
        tos << strarray[i] << endl;
    }
    return strreturn;
}

void *ExecRemoteThread::Entry() {
#ifdef __DEBUG__
    fprintf(stderr, "ExecRemoteThread::Entry!"); //wxMessageBox(wxT("Entry"));
#endif
    if(!m_jobinfo->fullname || m_jobinfo->fullname.IsNull() || m_jobinfo->fullname.IsEmpty())return NULL;
    if(m_jobinfo->protocal.Cmp(wxT("HTTP")) == 0) {
        HttpPost *post = new HttpPost(m_jobinfo->fullname, m_jobinfo->username);
        wxArrayString upfilelist = PrepareInputFile(m_jobinfo->inputfile);
        wxString tmpinputfile = m_jobinfo->inputfile + wxT(".tmp");
        post->AddFile(tmpinputfile);
        for(unsigned int i = 0; i < upfilelist.GetCount(); i++)post->AddFile(upfilelist[i]);
        if(post->Post()) {
            m_jobinfo->scriptfile = post->FindTicket();
            //    wxMessageBox(m_jobinfo->scriptfile);
            if(m_jobinfo->scriptfile.IsEmpty()) {
#ifdef __DEBUG__
                fprintf(stderr, "Can't find ticket!");
#endif
                return NULL;
            } else
                SaveRemoteJobInfo();
        } else {
            wxString msg = wxT("Http Error!Please check the url or server status!The job is abort!");
            wxMessageUpdateEvent event(wxEVT_COMMAND_MESSAGE_CHANGED, ID_MESSAGE_CHANGE );
            event.SetString(msg); // that's all
            wxPostEvent( m_pJobMonitorPnl, event );
            //        fprintf(stderr,"Http Error!Please check the url or server status!The job is abort!");
            return NULL;
        }
    } else {

        //wxMessageBox(wxT("PrepareInputFile"));
        char *input = NULL;
        char outstr[128];
        memset(outstr, 0x0, sizeof(outstr));
        wxString tmpinputfile = m_jobinfo->inputfile + wxT(".tmp");
        StringUtil::String2CharPointer(&input, tmpinputfile);
        char *remotefile = NULL;
        wxArrayString upfilelist = PrepareInputFile(m_jobinfo->inputfile);
        wxFileName *finputfile = new wxFileName(m_jobinfo->inputfile);
        wxFileName *fscriptfile = new wxFileName(m_jobinfo->scriptfile);
        char *remotedir = NULL;
        wxString strremotedir = wxEmptyString;
        if(!m_jobinfo->remoteworkbase.IsNull() && !(m_jobinfo->remoteworkbase.Trim()).IsEmpty()) {
            strremotedir = m_jobinfo->remoteworkbase.Trim() + wxT("/") + m_jobinfo->ticket;
            StringUtil::String2CharPointer(&remotedir, m_jobinfo->remoteworkbase);
            strcat(outstr, "mkdir ");
            strcat(outstr, remotedir);
            if(m_sshclient->RemoteExec(outstr, strlen(outstr), NULL) != 1) {
#ifdef __DEBUG__
                fprintf(stderr, "create folder error!");
#endif
                return NULL;
            }

        } else {
            strremotedir = m_jobinfo->ticket;
        }
        memset(outstr, 0x0, sizeof(outstr));
        StringUtil::String2CharPointer(&remotedir, strremotedir);
        strcat(outstr, "mkdir ");
        strcat(outstr, remotedir);
        if(m_sshclient->RemoteExec(outstr, strlen(outstr), NULL) != 1) {
#ifdef __DEBUG__
            fprintf(stderr, "create folder error!");
#endif
            return NULL;
        }

        StringUtil::String2CharPointer(&remotefile, strremotedir + wxT("/") + finputfile->GetFullName());
        //wxMessageBox(wxT(remotefile));
#ifdef __DEBUG__
        fprintf(stderr, "%s", input);
#endif
        wxThread::Sleep(100);
        if(m_sshclient->Upload(input, remotefile) == -1)fprintf(stderr, "upload failed!");

        if(m_jobinfo->submitway.Cmp(wxT("One command with variables")) != 0) {
            StringUtil::String2CharPointer(&input, m_jobinfo->scriptfile);
            StringUtil::String2CharPointer(&remotefile, strremotedir + wxT("/") + fscriptfile->GetFullName());
#ifdef __DEBUG__
            fprintf(stderr, "Script file:");
            fprintf(stderr, "%s", input);
            fprintf(stderr, "%s", remotefile);
#endif
            wxThread::Sleep(100);
            if(m_sshclient->Upload(input, remotefile) == -1)fprintf(stderr, "upload failed!");
        }

        for (unsigned int i = 0; i < upfilelist.GetCount(); i++)
        {
            StringUtil::String2CharPointer(&input, upfilelist[i]);
            wxFileName finputfile1(upfilelist[i]);
            StringUtil::String2CharPointer(&remotefile, strremotedir + wxT("/") + finputfile1.GetFullName());
            wxThread::Sleep(100);
            if(m_sshclient->Upload(input, remotefile) == -1)fprintf(stderr, "upload failed!"); //wxMessageBox(wxT("open failed!"));
        }
        /*
        memset(outstr,0x0,sizeof(outstr));
        strcat(outstr,"cd ");
        strcat(outstr,remotedir);
        if(sshclient->RemoteExec(outstr,strlen(outstr),NULL)!=1){
            fprintf(stderr,"enter job folder error!");
            return NULL;
        }
        */
        wxString command = wxEmptyString;
        if(m_jobinfo->submitway.Cmp(wxT("One command with variables")) == 0) {
            command = m_jobinfo->command;
            command.Replace(wxT("$inputfile"), finputfile->GetName());
            command = command + wxT(">~/simu.log"); //+wxT(" g038.q>~/simu.log");//
        } else {
            StringUtil::String2CharPointer(&input, fscriptfile->GetFullName());
            memset(outstr, 0x0, sizeof(outstr));
            strcat(outstr, "chmod 555 ");
            strcat(outstr, input);
            if(m_sshclient->RemoteExec(outstr, strlen(outstr), remotedir) != 1) {
                fprintf(stderr, "chmod command error!");
                return NULL;
            }
            command = wxT("./") + fscriptfile->GetFullName() + wxT(">") + finputfile->GetName() + wxT(".output");
        }
        //wxMessageBox(command);
        StringUtil::String2CharPointer(&input, command);
#ifdef __DEBUG__
        fprintf(stderr, "%s", input);
#endif
        //fprintf(stderr,"%d",strlen(input));
        SaveRemoteJobInfo();
        if(m_sshclient->RemoteExec(input, strlen(input), remotedir) == 1)
        {
            wxArrayString strary = m_sshclient->GetOutputInfo("tail ~/simu.log");
            if(strary.GetCount() > 0)fprintf(stderr, "Submit Sucess!!"); //wxMessageBox(strary[0]);
        } else {
#ifdef __DEBUG__
            fprintf(stderr, "Submit error!!"); //wxMessageBox(wxT("exec error!"));
#endif
        }
#ifdef __DEBUG__
        fprintf(stderr, "exit!"); //wxMessageBox(wxT("Exit"));
#endif
    }
    return NULL;
}

void ExecRemoteThread::SaveRemoteJobInfo(void)
{
#ifdef __DEBUG__
    fprintf(stderr, "execremotethread::saveremotejobinfo");
#endif
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("remotejob.lst");
    RemoteJobInfoArray remotejobinfoarray;
    JobFile::LoadFile(fileName, remotejobinfoarray);
    RemoteJobInfo rjobinfo = RemoteJobInfo();

    rjobinfo.jobname = m_jobinfo->jobname;
    rjobinfo.fullname = m_jobinfo->fullname;
    rjobinfo.shortname = m_jobinfo->shortname;
    rjobinfo.username = m_jobinfo->username;
    rjobinfo.password = m_jobinfo->password;
    rjobinfo.protocal = m_jobinfo->protocal;
    rjobinfo.scriptfile = m_jobinfo->scriptfile;
    rjobinfo.ticket = m_jobinfo->ticket;
    rjobinfo.submittime = m_jobinfo->submittime;
    rjobinfo.status = m_jobinfo->status;
    rjobinfo.inputfile = m_jobinfo->inputfile;
    rjobinfo.jobpath = m_jobinfo->jobpath;
    rjobinfo.projectfile = m_jobinfo->projectfile;
    rjobinfo.projecttype = m_jobinfo->projecttype;
    rjobinfo.remoteworkbase = m_jobinfo->remoteworkbase;
    if(m_jobinfo->protocal.Cmp(wxT("HTTP")) != 0) {
        rjobinfo.masterid = EnvUtil::GetMasterId();
        rjobinfo.masterkey = EnvUtil::GetMasterKeyMD5();
    } else {
        rjobinfo.masterid = wxT("no use");
        rjobinfo.masterkey = wxT("no use");
    }
    rjobinfo.machineid = m_jobinfo->machineid;
    remotejobinfoarray.Add(rjobinfo);
    JobFile::SaveFile(fileName, remotejobinfoarray);
#ifdef __DEBUG__
    fprintf(stderr, "remotejoblist.save");
#endif
    //fprintf(stderr,jobname);
    //  wxMessageBox(wxT("Your Job has submit"));
    Manager::Get()->GetMonitorPnl()->GetRemoteJobInfoList();

}

void ExecRemoteThread::OnExit() {
    /*        if(m_pCurl) {
                    curl_easy_cleanup(m_pCurl);
            }
    */
}

bool ExecRemoteThread::KillThread(void) {
    return true;
}

wxString RemoteThreadData::GetInputFile()
{
    return m_JobData->GetInputFileName();
}

wxString RemoteThreadData::GetJobName() {
    return m_JobData->GetJobName();
}

wxString RemoteThreadData::GetProjectFile()
{
    return m_JobData->GetProjectFile();
}

wxString RemoteThreadData::GetProjectType()
{
    return SimuProject::GetProjectType(m_JobData->GetProjectType());
}


RemoteJobInfo::RemoteJobInfo()
{
    jobname = wxEmptyString;
    fullname = wxEmptyString;
    shortname = wxEmptyString;
    username = wxEmptyString;
    password = wxEmptyString;
    protocal = wxEmptyString;
    scriptfile = wxEmptyString;
    ticket = wxEmptyString;
    submittime = wxEmptyString;
    status = wxEmptyString;
    jobpath = wxEmptyString;
    inputfile = wxEmptyString;
    projectfile = wxEmptyString;
    submitway = wxEmptyString;
    command = wxEmptyString;
    masterkey = wxEmptyString;
    remoteworkbase = wxEmptyString;
    masterid = wxEmptyString;
    masterkey0 = wxEmptyString;
    machineid = wxEmptyString;
    projecttype = wxEmptyString;
    //m_sshclient=NULL;
    //m_connected=false;
}


DownloadCheck::DownloadCheck()
{
    checkfile = wxEmptyString;
    inputfile = wxEmptyString;
}

WX_DEFINE_OBJARRAY(RemoteJobInfoArray);
WX_DEFINE_OBJARRAY(DownloadCheckArray);

void JobFile::LoadFile(const wxString &strLoadedFileName, RemoteJobInfoArray &remotejobinfoarray)
{
    wxTextFile jobfile(strLoadedFileName);
    if(!jobfile.Exists()) {
#ifdef __DEBUG__
        fprintf(stderr, "job file no exist!");
#endif
        return;
    }
    jobfile.Open();
    if(jobfile.GetLineCount() == 0) {
#ifdef __DEBUG__
        fprintf(stderr, "job file is empty!");
#endif
        return;
    }
    remotejobinfoarray.Clear();
    wxString strLine;
    strLine = jobfile.GetFirstLine();

    while(FILE_BEGIN_JOBS.Cmp(strLine) != 0)strLine = jobfile.GetNextLine();
    while( FILE_END_JOBS.Cmp(strLine) != 0)
    {   //IRS BEGIN
        RemoteJobInfo minfo = RemoteJobInfo();
        int i = 0;
        while(FILE_BEGIN_JOB.Cmp(strLine) != 0)strLine = jobfile.GetNextLine();
        while( FILE_END_JOB.Cmp(strLine) != 0)
        {
            strLine = jobfile.GetNextLine();
            switch(i) {
            case 0:
                minfo.jobname = strLine;
                //wxMessageBox(minfo.machineid);
                break;
            case 1:
                minfo.fullname = strLine;
                break;
            case 2:
                minfo.shortname = strLine;
                break;
            case 3:
                minfo.username = strLine;
                //        wxMessageBox(minfo.shortname);
                break;
            case 4:
                //minfo.password=EnvUtil::GetDecipherCode(strLine);
                minfo.password = strLine;
                break;
            case 5:
                minfo.protocal = strLine;
                break;
            case 6:
                minfo.scriptfile = strLine;
                break;
            case 7:
                minfo.ticket = strLine;
                break;
            case 8:
                minfo.submittime = strLine;
                break;
            case 9:
                minfo.status = strLine;
                break;
            case 10:
                minfo.inputfile = strLine;
                break;
            case 11:
                minfo.jobpath = strLine;
                break;
            case 12:
                minfo.projectfile = strLine;
                break;
            case 13:
                minfo.remoteworkbase = strLine;
                break;
            case 14:
                minfo.masterid = strLine;
                break;
            case 15:
                minfo.masterkey = strLine;
                break;
            case 16:
                minfo.machineid = strLine;
                break;
            case 17:
                minfo.projecttype = strLine;
            };
            i++;
        }//IR END
        remotejobinfoarray.Add(minfo);
        strLine = jobfile.GetNextLine();
    }
    jobfile.Close();
}

void JobFile::SaveFile(const wxString fileName, RemoteJobInfoArray remotejobinfoarray) {
    unsigned int i;

    unsigned int totaljobs = remotejobinfoarray.GetCount();
    if (totaljobs <= 0) {
#ifdef __DEBUG__
        fprintf(stderr, "No job to save!");
#endif
        if(wxFileExists(fileName))wxRemoveFile(fileName);
        return;
    }

    wxTextFile jobfile(fileName);
    if(jobfile.Exists()) {
        jobfile.Open();
        jobfile.Clear();
    } else
        jobfile.Create();


    jobfile.AddLine(FILE_BEGIN_JOBS, wxTextFileType_Unix);

    for(i = 0; i < totaljobs; i++) {
        RemoteJobInfo minfo = remotejobinfoarray[i];
        jobfile.AddLine( FILE_BEGIN_JOB, wxTextFileType_Unix);
        jobfile.AddLine(minfo.jobname, wxTextFileType_Unix);
        jobfile.AddLine(minfo.fullname, wxTextFileType_Unix);
        jobfile.AddLine(minfo.shortname, wxTextFileType_Unix);
        jobfile.AddLine(minfo.username, wxTextFileType_Unix);
        jobfile.AddLine(minfo.password, wxTextFileType_Unix);
        jobfile.AddLine(minfo.protocal, wxTextFileType_Unix);
        jobfile.AddLine(minfo.scriptfile, wxTextFileType_Unix);
        jobfile.AddLine(minfo.ticket, wxTextFileType_Unix);
        jobfile.AddLine(minfo.submittime, wxTextFileType_Unix);
        jobfile.AddLine(minfo.status, wxTextFileType_Unix);
        jobfile.AddLine(minfo.inputfile, wxTextFileType_Unix);
        jobfile.AddLine(minfo.jobpath, wxTextFileType_Unix);
        jobfile.AddLine(minfo.projectfile, wxTextFileType_Unix);
        jobfile.AddLine(minfo.remoteworkbase, wxTextFileType_Unix);
        jobfile.AddLine(minfo.masterid, wxTextFileType_Unix);
        jobfile.AddLine(minfo.masterkey, wxTextFileType_Unix);
        jobfile.AddLine(minfo.machineid, wxTextFileType_Unix);
        jobfile.AddLine(minfo.projecttype, wxTextFileType_Unix);
        jobfile.AddLine( FILE_END_JOB , wxTextFileType_Unix);
    }
    jobfile.AddLine( FILE_END_JOBS , wxTextFileType_Unix);
    jobfile.Write(wxTextFileType_Unix);
    jobfile.Close();
}
