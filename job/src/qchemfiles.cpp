/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "qchemfiles.h"

#include <wx/filename.h>
#include <wx/tokenzr.h>

#include "fileutil.h"
#include "stringutil.h"
#include "envutil.h"
#include "tools.h"

#include "renderingdata.h"
#include "molfile.h"
#include "xyzfile.h"

#include "simuproject.h"
#include "envsettings.h"
#include "manager.h"
#include "execprocess.h"
#include "qchemlogreading.h"

#include <wx/filename.h>
#include <wx/dir.h>
#include <stdio.h>
#include "dlgsummary.h"
#include "dlgoutput.h"
#include "molglcanvas.h"


#define Q_BUFFERSIZE 80
#define STRINGSIZE 30
#define NA_SIZE 6
#define COOD_SIZE 5
#define SIZE_FOUR 4
#define SIZE_16 16



QChemFiles::QChemFiles(SimuProject* simuProject, wxString resultDir) : AbstractProjectFiles(simuProject, resultDir) {
}

QChemFiles::~QChemFiles() {

}

bool QChemFiles::CreateInputFile(void) {
        wxString molFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_MOLECULE);
//        wxString jobFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_JOB_INFO);
        wxString quifile=m_pSimuProject->GetResultFileName(m_name) + wxT(".xyz");
        wxString quiproc=wxEmptyString;
        wxString execOutputDir=wxEmptyString;
        if(wxFileExists(molFile)) {
                RenderingData tmpData(NULL);
                MolFile::LoadFile(molFile, &tmpData);
                XYZFile::SaveCoord(quifile, tmpData.GetAtomBondArray(), false);
                if(!wxFileExists(quifile)){
                        Tools::ShowError(quifile + wxT("  does not exist!"));
                        return false;
                }
                wxString execDir = EnvSettingsData::Get()->GetChemSoftDir(PROJECT_QCHEM);
                //wxMessageBox(execDir);
        wxString conspath=execDir+platform::PathSep();
                execDir += platform::PathSep() + EnvUtil::GetProgramName(wxT("qui"));

                //wxMessageBox(execDir);
                if(!wxFileExists(execDir)) {
                        Tools::ShowError(execDir + wxT("  does not exist!"));
                        return false;
                }
                execOutputDir = m_pSimuProject->GetResultFileName(m_name) + wxT(".in");
        if(!wxFileExists(execOutputDir))
            SaveQuiCons(conspath);

                //wxMessageBox(execOutputDir);
                quiproc=execDir+wxT(" \"")+quifile+wxT("\" \"")+execOutputDir+wxT("\"");
                //wxMessageBox(quiproc);
        //        long processId = wxExecute(quiproc, wxEXEC_SYNC);
                wxExecute(quiproc, wxEXEC_SYNC);
                // std::cout << "QChemFiles::CreateInputFile: " << quiproc << ";" << std::endl;
                if(wxFileExists(quifile))
                    wxRemoveFile(quifile);
            wxString fileName=conspath + wxT("aaa.click");
        if(wxFileExists(fileName)){
            wxString strLine;
            long lok;
            wxFileInputStream fis(fileName);
            wxTextInputStream tis(fis);
            strLine=tis.ReadLine();
            strLine.ToLong(&lok);
                    if(lok==1)
                              return true;
                    else
                              return false;
        }
        return false;
        }
        return false;
}

wxString QChemFiles::GetExecPara(void) {
        wxString qchempara=wxEmptyString;
        wxString rcfile= EnvSettingsData::Get()->GetChemSoftDir(PROJECT_QCHEM);
        rcfile += platform::PathSep() + wxT("qchem.rc");
        qchempara=wxT("\"")+m_pSimuProject->GetResultFileName(m_name) + wxT(".in\" \"")+m_pSimuProject->GetResultFileName(m_name)+wxT(".out\" \"")+rcfile+wxT("\"");
        return qchempara;
}

wxString QChemFiles::GetExecCommand(void){
        wxString execProcName = wxEmptyString;
        if(platform::windows) {
                execProcName= wxT("qc.bat");
        }else{
                execProcName= wxT("qc.sh");
        }
        return execProcName;
}
/*
void QChemFiles::BuildResultTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode) {
        ProjectTreeItemData* itemData = NULL;
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_SUMMARY);
        pTree->AppendItem(jobResultNode, wxT("Summary"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        if(HasSurfaceData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_DENSITY);
                pTree->AppendItem(jobResultNode, wxT("Density"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        }

        if(HasVibrationData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_VIBRATION);
                pTree->AppendItem(jobResultNode, wxT("Vibration"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
                InitVibrationData();
        }
}
*/
bool QChemFiles::PostJobTerminated(bool isFinal) {
        wxString outFile = GetOutputFileName();
//        wxMessageBox(outFile);
        if(!wxFileExists(outFile)) {
                return true;        // when the job file does not exist, terminate the processing
        }
        GetSummary();
        if(!HasSummary()) //SONG add it, if the first line of sum file is 1, then won't do the surface Cal.
        {
                Tools::ShowError(wxT("Sorry, Q-Chem Job Failed!"));
                return true;
        }
/*        if(HasSummary())
        {
                wxMessageBox("Hello Hassummary is true");
                return true;
        }*/
        GetXYZ();

        if(!isFinal){
                ConvertVib();
        if (wxNO == wxMessageBox(wxString(wxT("Do you want to calculate surfaces?")),
                                                                         wxString(wxT("Calculate Setting")), wxYES_NO  | wxICON_QUESTION)) {
                                SetNeedSurface(false);
                }else{
                                SetNeedSurface(true);
                }
                if(GetNeedSurface()){
                        ConvertDens();
                        return DoCalcSurface();
                }
        }

//        if(GetNeedSurface())SaveSurfaces();
//        Tools::ShowStatus(wxT("Job Finished!"));
        return true;
}

bool QChemFiles::ConvertVib()
{
        FILE *fpin, *fpout;
        char buffer[Q_BUFFERSIZE];
        char string1[STRINGSIZE];
        char string2[STRINGSIZE];
        char string3[STRINGSIZE];
        char string4[STRINGSIZE];
        char string_f1[STRINGSIZE];
        char string_f2[STRINGSIZE];
        char string_f3[STRINGSIZE];
        char string_i1[STRINGSIZE];
        char string_i2[STRINGSIZE];
        char string_i3[STRINGSIZE];
        char *string_Stand = const_cast<char*>("Standard");
        char *string_Nucle =const_cast<char*>( "Nuclear");
        char *string_Orien = const_cast<char*>("Orientation");
        char *string_Mode =const_cast<char*>( "Mode:");
        char *string_Begin_IRS =const_cast<char*>( "$BEGIN IRS");
        char *string_End_IRS =const_cast<char*>( "$END IRS");
        char *string_Begin_IR = const_cast<char*>("$BEGIN IR");
        char *string_End_IR = const_cast<char*>("$END IR");
        int index, i;
        int tempI;
        char stringES[3];
        int NofAtoms=0;
        int count=0;

        struct vib_part {
                char vx[STRINGSIZE];
                char vy[STRINGSIZE];
                char vz[STRINGSIZE];
        } *vib1, *vib2;

//    wxMessageBox(GetOutputFileName());
//    wxMessageBox(GetVibOutputFileName());
        char *charOutputName=NULL,*charVibName=NULL;
    StringUtil::String2CharPointer(&charOutputName, GetOutputFileName());
        StringUtil::String2CharPointer(&charVibName, GetVibOutputFileName());
        if ((fpin = fopen(charOutputName, "r")) == NULL) {
                printf( "Can't open input file %s. \n", charOutputName);
                return false;
        }

        if ((fpout = fopen(charVibName, "w")) == NULL) {
                printf( "Can't open output file %s. \n", charVibName);
                return false;
        }


// Get number of atoms
        while (fgets(buffer, Q_BUFFERSIZE, fpin) != NULL) {
                sscanf(buffer, "%s %s %s", string1,  string2,  string3);
                if (!strcmp(string1, string_Stand) &&
                        !strcmp(string2, string_Nucle) &&
                        !strcmp(string3, string_Orien) ) {
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        index = 0;
                        while (1) {
                                if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                                if (buffer[1] == 45 && buffer[2] == 45)
                                        break;
                                index++;
                        }
                        NofAtoms = index;
                        fprintf(fpout, "%d\n", NofAtoms);
                        fprintf(fpout, "#Generated with SimuGUI from Q-Chem output\n");
                        rewind(fpin);
                        break;
                }
        }
//
        vib1 = (struct vib_part *) malloc(sizeof(struct vib_part)*NofAtoms +1);
        vib2 = (struct vib_part *) malloc(sizeof(struct vib_part)*NofAtoms +1);

        while (fgets(buffer, Q_BUFFERSIZE, fpin) != NULL) {
// Get coordinate
                sscanf(buffer, "%s %s %s %s", string1,  string2,  string3, string4);
                if (!strcmp(string1, string_Stand) &&
                        !strcmp(string2, string_Nucle) &&
                        !strcmp(string3, string_Orien) ) {
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        for (i=0; i<NofAtoms; i++) {
                                if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                                sscanf(buffer, "%d %2s %s %s %s", &tempI, stringES, string1,  string2,  string3);
                                fprintf(fpout, "%2s %12s %12s %12s\n", stringES, string1,  string2,  string3);
                        }
                        fprintf(fpout, " \n");
                        fprintf(fpout, "%s\n", string_Begin_IRS);
                }
// Get vib info
                if (!strcmp(string1,string_Mode) ) {
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        string_f1[0] = '\0';
                        string_f2[0] = '\0';
                        string_f3[0] = '\0';
                        sscanf(buffer, "%s %s %s %s", string1, string_f1,  string_f2, string_f3);
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        string_i1[0] = '\0';
                        string_i2[0] = '\0';
                        string_i3[0] = '\0';
                        sscanf(buffer, "%s %s %s %s %s", string1, string1, string_i1,  string_i2, string_i3);
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                        fprintf(fpout, "%s\n", string_Begin_IR);
                        fprintf(fpout, "%s %s\n", string_f1, string_i1);
                        fprintf(fpout, "A \n");
                        for (i=0; i<NofAtoms; i++) {
                                if(fgets(buffer, Q_BUFFERSIZE, fpin) == NULL) return false;
                                sscanf(buffer, "%s %s %s %s %s %s %s %s %s %s",
                                                                  stringES, string1,  string2,  string3,
                                                                  vib1[i].vx,  vib1[i].vy,  vib1[i].vz,
                                                                  vib2[i].vx,  vib2[i].vy,  vib2[i].vz);
                                fprintf(fpout, " %7s %7s %7s\n", string1,  string2,  string3);
                        }
                        fprintf(fpout, "%s\n", string_End_IR);
//                        fprintf(fpout, " \n");
                        count++;
// second
                        if (string_f2[0] != '\0') {
                                fprintf(fpout, "%s\n", string_Begin_IR);
                                fprintf(fpout, "%s %s\n", string_f2, string_i2);
                                fprintf(fpout, "A \n");
                                for (i=0; i<NofAtoms; i++) {
                                        fprintf(fpout, " %7s %7s %7s\n", vib1[i].vx,  vib1[i].vy,  vib1[i].vz);
                                }
                                fprintf(fpout, "%s\n", string_End_IR);
//                                fprintf(fpout, " \n");
                                count++;
                        }
// third
                        if (string_f3[0] != '\0') {
                                fprintf(fpout, "%s\n", string_Begin_IR);
                                fprintf(fpout, "%s %s\n", string_f3, string_i3);
                                fprintf(fpout, "A \n");
                                for (i=0; i<NofAtoms; i++) {
                                        fprintf(fpout, " %7s %7s %7s\n", vib2[i].vx,  vib2[i].vy,  vib2[i].vz);
                                }
                                fprintf(fpout, "%s\n", string_End_IR);
                                count++;
                        }
                }
        }

        fprintf(fpout, "%s\n", string_End_IRS);

        free ( (void *) vib1);
        free ( (void *) vib2);

        fclose (fpout);
        fclose (fpin);

        if (count==0)
        {
            if(wxFileExists(GetVibOutputFileName()))
                wxRemoveFile(GetVibOutputFileName());
        }
        return true;
}

wxString QChemFiles::GetInputFileName()
{
        return m_pSimuProject->GetResultFileName(m_name) + wxT(".in");
}

wxString QChemFiles::GetOutputFileName()
{
        return m_pSimuProject->GetResultFileName(m_name) + wxT(".out");
}

wxString QChemFiles::GetSurfaceFileName()
{
        return m_pSimuProject->GetResultDir(m_name) + platform::PathSep() + wxT("surface.info");
}


bool QChemFiles::ConvertDens()
{
        wxString execDir = EnvSettingsData::Get()->GetChemSoftDir(PROJECT_QCHEM);
        execDir += platform::PathSep() + EnvUtil::GetProgramName(wxT("surfaceinfo"));
        if(!wxFileExists(execDir)) {
                Tools::ShowError(execDir + wxT("  does not exist!"));
                return false;
        }
        wxString fchkFile=m_pSimuProject->GetResultDir(m_name)+platform::PathSep()+wxT("Test.FChk");
        wxString surfacefile=GetSurfaceFileName();
        wxString quiproc=execDir+wxT(" ")+fchkFile+wxT(" ")+surfacefile;
//        long processId = wxExecute(quiproc, wxEXEC_SYNC);
        wxExecute(quiproc, wxEXEC_SYNC);
        // std::cout << "QChemFiles::ConvertDens: " << quiproc << ";" << std::endl;
        //wxCopyFile(jobFile, m_pSimuProject->GetResultFileName(m_name) + wxT(".input"));

        return true;
}

/*
wxArrayString QChemFiles::GetSurfaceDataFiles()
{
        wxArrayString fileArray;
        wxString fileName = m_pSimuProject->GetResultFileName(m_name);;
        if(wxFileExists(fileName + wxT(".dens"))){
                fileArray.Add(fileName + wxT(".dens"));
        }else {
                if(wxFileExists(fileName + wxT("_a.dens")) ){
                        fileArray.Add(fileName + wxT("_a.dens"));
                }
                if(wxFileExists(fileName + wxT("_b.dens")) ){
                        fileArray.Add(fileName + wxT("_b.dens"));
                }
        }
        return fileArray;
}
*/


bool QChemFiles::GetSummary()
{
        wxString execDir = EnvSettingsData::Get()->GetChemSoftDir(PROJECT_QCHEM);
        execDir += platform::PathSep() + EnvUtil::GetProgramName(wxT("getsum"));
        if(!wxFileExists(execDir)) {
                Tools::ShowError(execDir + wxT("  does not exist!"));
                return false;
        }
        wxString infile = GetOutputFileName();
        wxString sumfile=GetSummaryFileName();
        wxString quiproc=execDir+wxT(" \"")+infile+wxT("\" \"")+sumfile+wxT("\"");
        // std::cout << "QChemFiles::GetSummary: " << quiproc << ";" << std::endl;
//        long processId = wxExecute(quiproc, wxEXEC_SYNC);
        wxExecute(quiproc, wxEXEC_SYNC);
        //wxCopyFile(jobFile, m_pSimuProject->GetResultFileName(m_name) + wxT(".input"));

        return true;
}


/*    for(i = 0; i < atomNumber; i++) {
        strLine = tis.ReadLine();       // read one line
        wxStringTokenizer tzk(strLine);
        AtomBond atomBond = AtomBond();
        atomSymbol = tzk.GetNextToken();
        if(atomSymbol.ToLong(&lAtomIndex)) {
                        atomId = (int)lAtomIndex;
                }else {
                atomId = GetElemId(atomSymbol);
                }
        tzk.GetNextToken().ToDouble(&x);
        tzk.GetNextToken().ToDouble(&y);
        tzk.GetNextToken().ToDouble(&z);
        atomBond.atom.elemId = (int)atomId;
        if(atomBond.atom.elemId == ELEM_H) {
            atomBond.atom.elemId = (ElemId)DEFAULT_HYDROGEN_ID;
        }
        atomBond.atom.pos.x = x;
        atomBond.atom.pos.y = y;
        atomBond.atom.pos.z = z;
        appendAtomBondArray.Add(atomBond);
    }
*/

void QChemFiles::GetXYZ()
{
wxString inputDir= GetOutputFileName();
wxString xyzDir=m_pSimuProject->GetResultFileName(m_name) + wxT(".Opt.xyz");
char * inputDirchar = NULL;
char * xyzDirchar = NULL;
StringUtil::String2CharPointer(&inputDirchar,inputDir);  //convert from wxString to char*
StringUtil::String2CharPointer(&xyzDirchar,xyzDir);
QchemLogReading (inputDirchar, xyzDirchar);
return;
}

void QChemFiles::BuildResultTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode){
        ProjectTreeItemData* itemData = NULL;
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_INPUT);
        pTree->AppendItem(jobResultNode, wxT("Input"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);

        if(HasSummary()){
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_SUMMARY);
        pTree->AppendItem(jobResultNode, wxT("Summary"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        }
//        else //SONG add it
//        {
//                Tools::ShowError(wxT("Sorry, Q-Chem Job Failed!"));
//        }
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_OUTPUT);
        pTree->AppendItem(jobResultNode, wxT("Output"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);

//        if(!HasSummary()) //SONG add it
//                return ;

        if(HasSurfaceData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_DENSITY);
                pTree->AppendItem(jobResultNode, wxT("Density"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        }
        if(HasVibrationData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_FREQUENCE);
                pTree->AppendItem(jobResultNode, wxT("Vibration"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
                InitVibrationData();
        }

}

void QChemFiles::ShowResultItem(ProjectTreeItemData *item)
{
        ProjectTreeItemType itemType=item->GetType();
        wxString jtitle =wxEmptyString;
        jtitle=jtitle+item->GetTitle();
        //wxMessageBox(wxT("QChemFiles::ShowResultItem:")+jtitle);
            if(itemType==ITEM_MOPACOUTPUT){
            wxString outfile=m_pSimuProject->GetMopacOutFilePath(jtitle);
            DlgOutput* pdlg=new DlgOutput(Manager::Get()->GetAppWindow(),wxID_ANY);
//wxMessageBox(outfile);
            pdlg->AppendFile(outfile,wxT("Mopac OutputFile"));
            pdlg->Show(true);
            return;
            }else if(itemType == ITEM_JOB_RESULT_SUMMARY)
                {
          //  wxMessageBox(wxT("before new dlg"));
                        DlgSummary *dlgsummary=new DlgSummary(Manager::Get()->GetAppWindow(),wxID_ANY);
                //        wxMessageBox(wxT("before getsummaryfile"));
            wxString sumfile=m_pSimuProject->GetFiles(jtitle)->GetSummaryFileName();
                //        wxMessageBox(sumfile);
                        dlgsummary->AppendSummary(sumfile);
                        dlgsummary->ShowModal();
//                        free(dlgsummary);
            dlgsummary=NULL;
                }
                else if(itemType== ITEM_JOB_RESULT_INPUT)
                {
                        DlgOutput *dlgOutput=new DlgOutput(Manager::Get()->GetAppWindow(),wxID_ANY);
                        wxString infile=m_pSimuProject->GetFiles(jtitle)->GetInputFileName();
                //        wxMessageBox(infile);
                        dlgOutput->AppendFile(infile,wxT("InPut"));
                        dlgOutput->Show(true);
                }
                else if(itemType == ITEM_JOB_RESULT_OUTPUT)
                {
                        DlgOutput *dlgOutput=new DlgOutput(Manager::Get()->GetAppWindow(),wxID_ANY);
                        wxString outfile=m_pSimuProject->GetFiles(jtitle)->GetOutputFileName();
                //        wxMessageBox(outfile);
                        dlgOutput->AppendFile(outfile,wxT("OutPut"));
                        dlgOutput->Show(true);
                }else{
                                Manager::Get()->GetViewManager()->ShowMolView(item, wxEmptyString, true);//show                return;
        }
}

bool QChemFiles::HasInput()
{
        wxString tmpFileName= m_pSimuProject->GetResultFileName(m_name)+wxT(".in");
        if(wxFileExists(tmpFileName))
                return true;
        else
                return false;

}
bool QChemFiles::HasOutput()
{
        wxString tmpFileName= m_pSimuProject->GetResultFileName(m_name)+wxT(".output");
        if(wxFileExists(tmpFileName))
                return true;
        else
                return false;

}

bool QChemFiles::HasError_Info(){
        wxString tmpFileName= m_pSimuProject->GetResultFileName(m_name)+wxT(".error");
        if(wxFileExists(tmpFileName))
                return true;
        else
                return false;
}

bool QChemFiles::HasOptimization(){
        wxString tmpFileName= m_pSimuProject->GetResultFileName(m_name)+wxT(".geo.molden");
        if(wxFileExists(tmpFileName))
                return true;
        else
                return false;

}
bool QChemFiles::HasGssOrb()
{
    wxString tmpFileName= m_pSimuProject->GetResultFileName(m_name)+wxT(".GssOrb");
        if(wxFileExists(tmpFileName))
                return true;
        else
                return false;
}
bool QChemFiles::HasScfOrb()
{
        wxString tmpFileName= m_pSimuProject->GetResultFileName(m_name)+wxT(".ScfOrb");
        if(wxFileExists(tmpFileName))
                return true;
        else
                return false;
}

bool QChemFiles::HasDump()
{
        wxString tmpFileName= m_pSimuProject->GetResultFileName(m_name)+wxT(".xmldump.xml");
    if(wxFileExists(tmpFileName))
                return true;
        tmpFileName=tmpFileName.BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+wxT("xmldump.xml");
        if(wxFileExists(tmpFileName)){
            wxRenameFile(tmpFileName,m_pSimuProject->GetResultFileName(m_name)+wxT(".xmldump.xml"));
                return true;
        }
        return false;
}


void QChemFiles::SaveQuiCons(wxString QuiConsPath) {                //QuiConsPath  specify the path file of a.cons . should be end of '/'

        Manager::Get()->GetProjectManager()->GetActiveProject()->Save();

        wxString name;

        name =Manager::Get()->GetProjectManager()->GetActiveProject()->GetResultDir(name);

        name.RemoveLast();

        wxString simName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".sim");

        ifstream in(simName.ToAscii());

        vector<string> vec;

        string s;

        while(getline(in, s)) {

                if (0 == s.compare("$BEGIN CONSTRAINTS")) {

                        while (getline(in, s)){

                                if (0 != s.compare("$END CONSTRAINTS")) {

                                        string::size_type pos=0;

                                        if( (pos=s.find("L", pos)) != string::npos){
                                                 s.erase(pos, 1);
                                                 s.insert(pos, "stre");
                                        }
                                        pos=0;
                                        if( (pos=s.find("A", pos)) != string::npos){
                                                 s.erase(pos, 1);
                                                 s.insert(pos, "bend");
                                        }
                                        pos=0;
                                        if( (pos=s.find("D", pos)) != string::npos){
                                                 s.erase(pos, 1);
                                                 s.insert(pos, "tors");
                                        }

                                        vec.push_back(s);

                                } else {
                                        break;
                                }

                        }
                }
        }
        in.close();

        QuiConsPath = QuiConsPath + wxT("a.cons");

        ofstream out( QuiConsPath.ToAscii() );
        for (unsigned int i=0; i<vec.size(); ++i) {
                out << vec[i] << endl;
        }
        out.close();
}
