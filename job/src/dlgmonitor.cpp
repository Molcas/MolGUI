/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "dlgmonitor.h"

///////////////////////////////////////////////////////////////////////////

RMonitorPanel::RMonitorPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name)
{
        this->SetSizeHints( wxDefaultSize, wxDefaultSize );

        wxBoxSizer* bSizer1;
        bSizer1 = new wxBoxSizer( wxVERTICAL );

        m_gridjoblist = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

        // Grid
        m_gridjoblist->CreateGrid( 5, 4 );
        m_gridjoblist->EnableEditing( false );
        m_gridjoblist->EnableGridLines( true );
        m_gridjoblist->EnableDragGridSize( false );
        m_gridjoblist->SetMargins( 0, 0 );

        // Columns
        m_gridjoblist->SetColSize( 0, 139 );
        m_gridjoblist->SetColSize( 1, 110 );
        m_gridjoblist->SetColSize( 2, 149 );
        m_gridjoblist->SetColSize( 3, 94 );
        m_gridjoblist->EnableDragColMove( false );
        m_gridjoblist->EnableDragColSize( true );
        m_gridjoblist->SetColLabelSize( 30 );
        m_gridjoblist->SetColLabelValue( 0, wxT("Job") );
        m_gridjoblist->SetColLabelValue( 1, wxT("Host") );
        m_gridjoblist->SetColLabelValue( 2, wxT("Submit Date") );
        m_gridjoblist->SetColLabelValue( 3, wxT("Status") );
        m_gridjoblist->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );

        // Rows
        m_gridjoblist->EnableDragRowSize( true );
        m_gridjoblist->SetRowLabelSize( 0 );
        m_gridjoblist->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );

        // Label Appearance

        // Cell Defaults
        m_gridjoblist->SetDefaultCellAlignment( wxALIGN_CENTRE, wxALIGN_TOP );
        m_gridjoblist->SetMinSize( wxSize( 500,-1 ) );

        bSizer1->Add( m_gridjoblist, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        wxBoxSizer* bSizer2;
        bSizer2 = new wxBoxSizer( wxHORIZONTAL );

        m_btnstatus = new wxButton( this, wxID_ANY, wxT("Status Detail"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnstatus, 0, wxALIGN_RIGHT|wxALL, 5 );

        m_btndisplayoutput = new wxButton( this, wxID_ANY, wxT("Display Output"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btndisplayoutput, 0, wxALIGN_RIGHT|wxALL, 5 );

        m_btnkill = new wxButton( this, wxID_ANY, wxT("Kill"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnkill, 0, wxALIGN_RIGHT|wxALL, 5 );

        bSizer1->Add( bSizer2, 0, wxALIGN_CENTER_HORIZONTAL|wxFIXED_MINSIZE, 5 );

        m_txtshow = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,240 ), wxTE_MULTILINE|wxTE_READONLY|wxHSCROLL );

        bSizer1->Add( m_txtshow, 1, wxEXPAND|wxALL, 5 );

        wxBoxSizer* bSizer3;
        bSizer3 = new wxBoxSizer( wxHORIZONTAL );

        m_btnok = new wxButton( this, wxID_ANY, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer3->Add( m_btnok, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        m_btncancel = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer3->Add( m_btncancel, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        bSizer1->Add( bSizer3, 1, wxALIGN_CENTER_HORIZONTAL, 5 );

        this->SetSizer( bSizer1 );
        this->Layout();
}
