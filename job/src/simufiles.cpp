/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "simufiles.h"

#include <wx/filename.h>

#include "fileutil.h"
#include "stringutil.h"
#include "envutil.h"
#include "tools.h"

#include "renderingdata.h"
#include "molfile.h"

#include "simujob.h"
#include "outputanalyse.h"
#include "gmfile.h"
#include "inputfile.h"

// #include "envsettings.h"
#include "manager.h"
#include "execprocess.h"

#include <wx/dir.h>

SimuFiles::SimuFiles(SimuProject* simuProject, wxString resultDir) : AbstractProjectFiles(simuProject, resultDir) {
}

SimuFiles::~SimuFiles() {

}

bool SimuFiles::CreateInputFile(void) {
        wxString molFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_MOLECULE);
        wxString jobFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_JOB_INFO);
        if(wxFileExists(molFile) && wxFileExists(jobFile)) {
                SimuJob simJob(jobFile);
                simJob.LoadFile();
                if(simJob.GetExecType() == EXEC_NONE) {
                        return false;
                }
//                if(simJob.GetExecType() == EXEC_ABIN) {
//                        RenderingData tmpData(NULL);
//                        MolFile::LoadFile(molFile, &tmpData);
//                        INPUTFile::SaveFileAI(GetInputFileName(simJob.GetExecType()), &tmpData, &simJob);
//                }else if(simJob.GetExecType() == EXEC_SEMI) {

//                }
                //always create gm file because it will be used as the input file to surface calculation
                GMFile::SaveFile(GetInputFileName(), molFile, jobFile, NULL, false);
                return true;
        }

        return false;
}
/*
void SimuFiles::InitVibrationData(void) {
        ExecType execTypes[2] = {EXEC_SEMI, EXEC_ABIN};
        wxString fileName;
        for(int i = 0; i < 2; i++) {
                fileName = GetOutputFileName(execTypes[i]);
                if(wxFileExists(fileName)) {
                        OutputAnalyse::LoadOutputFile(fileName, execTypes[i]);
                        if(OutputAnalyse::CheckSuccess()) {
                                OutputAnalyse::AnalyseVibrationData(this);
                                wxString vibname=GetVibOutputFileName();
                                //wxMessageBox(vibname);
                                if(wxFileExists(vibname))
                                {
                                        //AddOutputFile(vibname,true);
                                }
                                else
                                {
                                        if(OutputAnalyse::SaveVibrationData(vibname))
                                        {
                                                //AddOutputFile(vibname,true);
                                        }
                                }
                        }
                        break;
                }
        }
}
*/
/*void SimuFiles::ResetOutputList(bool saveChange)
{
        StringSet::iterator it;
        wxString filename;
        for(it=m_outputFileSet.begin() ; it!=m_outputFileSet.end(); ++it)
        {
                filename=*it;
                wxRemoveFile(filename);
        }
        m_outputFileSet.clear();
        m_vibArray.Clear();
////        if(m_pSurfaceData)
////        {
////                delete m_pSurfaceData;
////                m_pSurfaceData=NULL;
////        }
////        if(saveChange)
////        {
////                SaveFile(GetFileName(), false);
////        }
}*/

/*
bool SimuFiles::HasSurfaceData()
{
        wxArrayString fileList=GetSurfaceDataFiles();
        for(unsigned i = 0; i < fileList.GetCount(); i++) {
                if(wxFileExists(fileList[i])) return true;
        }
        return false;

}
*/
wxString SimuFiles::GetInputFileName(void) {
                return m_pSimuProject->GetResultFileName(m_name) + wxT(".gm");
}

wxString SimuFiles::GetOutputFileName(void) {
                return m_pSimuProject->GetResultFileName(m_name) + wxT(".out");
}

wxString SimuFiles::GetSurfaceFileName(void){
                return m_pSimuProject->GetResultDir(m_name) + platform::PathSep() + wxT("surface.info");
}
/*
wxArrayString SimuFiles::GetSurfaceDataFiles(void) {
        wxArrayString fileArray;
        wxString fileName = GetOutputFileName(EXEC_SURFACE);
        if(wxFileExists(fileName + wxT(".dens"))){
                fileArray.Add(fileName + wxT(".dens"));
        }else {
                if(wxFileExists(fileName + wxT("_a.dens")) ){
                        fileArray.Add(fileName + wxT("_a.dens"));
                }
                if(wxFileExists(fileName + wxT("_b.dens")) ){
                        fileArray.Add(fileName + wxT("_b.dens"));
                }
        }
        return fileArray;
}
*/
wxString SimuFiles::GetCalcResultFileName(ExecType type) {
        switch(type)
        {
        case EXEC_SEMI:
                return m_pSimuProject->GetResultFileName(m_name) + wxT("_SE_RESULT.log");
        case EXEC_ABIN:
                return m_pSimuProject->GetResultFileName(m_name) + wxT("_AI_RESULT.log");
        case EXEC_SURFACE:
                return m_pSimuProject->GetResultFileName(m_name) + wxT("_SF_RESULT.log");
        default:
                return wxEmptyString;
        }
}

wxString SimuFiles::GetCalcParamFileName(ParamType type) {
        switch(type)
        {
        case PARAM_ARC:
                return m_pSimuProject->GetResultDir(m_name) + platform::PathSep() + wxT("arc.info");;
        default:
                return wxEmptyString;
        }
}

wxString SimuFiles::GetExecLogName(void) {
        wxString fileName = wxEmptyString;
        ExecType type = GetExecType();
        if(type == EXEC_ABIN) {
                fileName = wxT("AI_EXEC");
        }else if(type == EXEC_ABIN) {
                fileName = wxT("SE_EXEC");
        }else {
                fileName = wxT("SIMU_EXEC");
        }
        return m_pSimuProject->GetResultDir(m_name) + platform::PathSep() + fileName;
}

wxString SimuFiles::GetExecCommand(void) {
        wxString execProcName = wxEmptyString;
        ExecType type = GetExecType();

        if(type == EXEC_ABIN) {
                execProcName = wxT("ai") + platform::PathSep() + EnvUtil::GetProgramName(wxT("SimuPac_AI"));
        }else if(type == EXEC_SEMI) {
                execProcName = wxT("se") + platform::PathSep() + EnvUtil::GetProgramName(wxT("SimuCal_SE"));
        }
        return execProcName;
}

wxString SimuFiles::GetExecPara(void) {
        wxString execPara = wxEmptyString;
        ExecType type = GetExecType();

        wxString inputFileName = GetInputFileName();
        wxString outputFileName = GetOutputFileName();
        wxString surfaceFileName = GetSurfaceFileName();
        wxString resultPath=m_pSimuProject->GetResultDir(m_name);// + platform::PathSep();
        if(wxFileExists(outputFileName)) wxRemoveFile(outputFileName);
        if(wxFileExists(surfaceFileName)) wxRemoveFile(surfaceFileName);
        RemoveSurfaceFiles(resultPath);
        if(type == EXEC_ABIN) {
                execPara = wxT("\"") + inputFileName + wxT("\" \"") + outputFileName + wxT("\"");
                execPara += wxT(" \"") + surfaceFileName + wxT("\"");
        }else if(type == EXEC_SEMI) {
                if(wxFileExists(GetCalcParamFileName(PARAM_ARC))) 
                        wxRemoveFile(GetCalcParamFileName(PARAM_ARC) );
                if(wxFileExists(GetCalcResultFileName(EXEC_SEMI))) 
                        wxRemoveFile(GetCalcResultFileName(EXEC_SEMI));
                execPara = wxT("\"") + inputFileName + wxT("\" \"") + outputFileName + wxT("\"");
                execPara += wxT(" \"") + GetCalcParamFileName(PARAM_ARC) + wxT("\"");
                execPara += wxT(" \"") + surfaceFileName + wxT("\"");
                execPara += wxT(" \"") + GetCalcResultFileName(EXEC_SEMI) + wxT("\"");
        }
        return execPara;
}

ExecType SimuFiles::GetExecType(void) {
        wxString jobFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_JOB_INFO);
        if(wxFileExists(jobFile)) {
                SimuJob simJob(jobFile);
                simJob.LoadFile();
                return simJob.GetExecType();
        }
        return EXEC_NONE;
}

/*
void SimuFiles::BuildResultTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode) {
        ProjectTreeItemData* itemData = NULL;
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_SUMMARY);
        pTree->AppendItem(jobResultNode, wxT("Summary"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        if(HasSurfaceData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_DENSITY);
                pTree->AppendItem(jobResultNode, wxT("Density"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        }
        if(HasVibrationData()) {
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_VIBRATION);
                pTree->AppendItem(jobResultNode, wxT("Vibration"), TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
                InitVibrationData();
        }
}
*/
bool SimuFiles::PostJobTerminated(bool isFinal) {
        wxString jobFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_JOB_INFO);
        if(!wxFileExists(jobFile)) {
                return true;        // when the job file does not exist, terminate the processing
        }
        SimuJob simJob(jobFile);
        simJob.LoadFile();
        if(!isFinal) {
                if(simJob.CanCalcSurface()) {
                        return DoCalcSurface();
                }
        }
        //else {
                OutputAnalyse::ClearData();
                OutputAnalyse::LoadOutputFile(GetOutputFileName(), simJob.GetExecType());
                if(OutputAnalyse::CheckSuccess()) {
                        OutputAnalyse::SaveOutputMolecule(m_pSimuProject->GetResultFileName(m_name) + wxT(".dens.mol"));
                        wxString vibname = GetVibOutputFileName();
                        if(wxFileExists(vibname)) wxRemoveFile(vibname);
                        OutputAnalyse::SaveVibrationData(vibname);
                }
        //}
        return true;
}

/*
bool SimuFiles::DoCalcSurface(void) {
        wxString inputFileName = GetInputFileName(EXEC_SURFACE);
        wxString outputFileName = GetOutputFileName(EXEC_SURFACE);
        if(!wxFileExists(inputFileName)){
                Tools::ShowError(inputFileName + wxT("  does not exist!"));
                return false;
        }
        wxString execDir = EnvSettingsData::Get()->GetChemSoftDir(m_pSimuProject->GetType());
        execDir += platform::PathSep() + wxT("surface") + platform::PathSep() + EnvUtil::GetProgramName(wxT("density"));
        if(!wxFileExists(execDir)) {
                Tools::ShowError(execDir + wxT("  does not exist!"));
                return false;
        }
        wxString execPara = wxT(" \"")+ GetInputFileName(EXEC_SEMI) + wxT("\" \"") + inputFileName + wxT("\" \"") + outputFileName + wxT("\"");
        wxString execOutputDir = m_pSimuProject->GetResultDir(m_name);
        wxString projectName = m_pSimuProject->GetTitle();

        JobRunningData* pJobData = new JobRunningData(m_pSimuProject->GetProjectId(), m_pSimuProject->GetType(), m_pSimuProject->GetTitle());
        pJobData->SetProjectFile(m_pSimuProject->GetFileName());
        pJobData->SetOutputDir(execOutputDir);
        pJobData->SetCommand(execDir, execPara);
        pJobData->SetFinal(true);
        Manager::Get()->GetMonitorPnl()->ExecLocalJob(pJobData);
        return false;
}
*/

void SimuFiles::RemoveSurfaceFiles(wxString nmask)
{
wxDir dir(nmask);
        if ( !dir.IsOpened() )
        {
        return;
        }
wxString fname;
wxString fspec = wxT("*.srf");
int flags = wxDIR_FILES;
bool conts = dir.GetFirst(&fname, fspec, flags);
while(conts){
        fname=nmask  + platform::PathSep()+fname;
        if(wxFileExists(fname)) wxRemoveFile(fname);
        conts = dir.GetNext(&fname);
}
}

void SimuFiles::ShowResultItem(ProjectTreeItemData *item)
{
return;
}
