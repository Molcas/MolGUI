/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsummary.cpp: implementation of the DlgSummary class.
//
//////////////////////////////////////////////////////////////////////

#include "dlgtemplate.h"
#include <wx/textfile.h>
////////////////////////////////////////////////////////////////////
//event tabel
int CTRL_ID_OK = wxNewId();

////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgTemplate, wxDialog)
        EVT_BUTTON(CTRL_ID_OK, DlgTemplate::DoOk)
END_EVENT_TABLE()
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DlgTemplate::DlgTemplate(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos,
            const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style)
{
        CreateGUIControls();
}

DlgTemplate::~DlgTemplate()
{

}

void DlgTemplate::CreateGUIControls()
{
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        SetSizer(bsTop);
        lbcaption= new wxStaticText(this, wxID_ANY,wxT("Submit Script Template"),
                                                                                                wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

        m_txtcontent = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_WORDWRAP );
        m_txtcontent->SetMinSize( wxSize( 500,400 ) );
        m_txtcontent->SetToolTip(wxT("The script will upload to host,all files(input file,xyz file and so on) in same folder.it would be executed after all files are ready.Use $inputfile where you need input file's name(without extension)."));
        bsTop->Add(lbcaption,0,wxALL|wxALIGN_CENTER,5);
        bsTop->Add(m_txtcontent,0,wxALL|wxALIGN_CENTER,5);

        wxButton*        btnOK=new wxButton(this, CTRL_ID_OK, wxT("OK"), wxPoint(10, 10), wxDefaultSize);

        bsTop->Add(btnOK,0,wxALL|wxALIGN_CENTER,5);
        Layout();
        bsTop->SetSizeHints(this);
        Refresh();
}

void DlgTemplate::DoOk(wxCommandEvent &event)
{
    SaveTemplate(m_templatefile);
        if(IsModal())
        {
                EndModal(wxID_OK);
        }
        else
        {
                SetReturnCode(wxID_OK);
                this->Show(false);
        }
}

void DlgTemplate::SaveTemplate(wxString templatefile)
{
    wxTextFile scriptfile(templatefile);
    if(scriptfile.Exists()){
        scriptfile.Open();
        scriptfile.Clear();
    }else
        scriptfile.Create();
    int i;
    for(i=0;i<m_txtcontent->GetNumberOfLines();i++){
        scriptfile.AddLine(m_txtcontent->GetLineText(i),wxTextFileType_Unix);
    }
    scriptfile.Write(wxTextFileType_Unix);
    scriptfile.Close();
}

void DlgTemplate::AppendTemplate(wxString templatefile)
{
    m_templatefile=templatefile;
        if (!wxFileExists(m_templatefile))return;
        wxString strLine;
    wxTextFile scriptfile(m_templatefile);
    scriptfile.Open();
    for(strLine=scriptfile.GetFirstLine();!scriptfile.Eof();strLine=scriptfile.GetNextLine()){
                strLine.Trim();
                strLine.Trim(false);
                if(!strLine.IsEmpty()){
            m_txtcontent->AppendText(strLine);
            m_txtcontent->AppendText(wxT("\n"));
        }
        }
        Refresh();
}
