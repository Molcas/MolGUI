/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/filename.h>
#include <wx/tokenzr.h>
#include "inputfile.h"
#include "datatype.h"
//#include "molcascommons.h"

static const wxString FILE_GENERAL_SPACE=wxT(" ");
static const wxString AI_FILE_CART_END_FLAG=wxT("ENDCART");
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#ifdef __BP_CONSTRAIN_DATA_H__
void INPUTFile::SaveFile(const wxString &fileName, AtomBondArray &array, StringHash &setting, ConstrainData &constrainData)
{
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);

        SaveExport(tos);
        SaveOutput(tos,array,setting);
        SaveSettings(tos,setting);
        SaveJobType(tos,array,setting,constrainData);
        //wxString tempStr;
        SaveFormat(tos,fileName,setting);
}
#endif

void INPUTFile::SaveExport(wxTextOutputStream &tos)
{
        tos<<wxT(">> export Project=test")<<endl;
        //tos<<wxT(">> export WorkDir=")<<MOLCAS_M2MSI_WORK_DIR<<endl;
}

void INPUTFile::SaveOutputSE(wxTextOutputStream &tos, AtomBondArray &array, StringHash &setting)
{
        unsigned int i;
        int elemId;
        for(i=0;i<array.GetCount();i++)
        {
                if(array[i].atom.elemId == DEFAULT_HYDROGEN_ID){
            elemId = (int)ELEM_H;
        }else{
            elemId = array[i].atom.elemId ;
        }
                //only for
                if(elemId >0 )
                        tos<<ELEM_NAMES[elemId-1]
                                <<FILE_GENERAL_SPACE;
                else
                        tos<<wxT("X")
                                <<FILE_GENERAL_SPACE;
                tos<<wxString::Format(wxT("%-f"),array[i].atom.pos.x)
                        <<FILE_GENERAL_SPACE
                        <<wxT("1")
                        <<FILE_GENERAL_SPACE
                        <<wxString::Format(wxT("%-f"),array[i].atom.pos.y)
                        <<FILE_GENERAL_SPACE
                        <<wxT("1")
                        <<FILE_GENERAL_SPACE
                        <<wxString::Format(wxT("%-f"),array[i].atom.pos.z)
                        <<FILE_GENERAL_SPACE
                        <<wxT("1")
                        <<FILE_GENERAL_SPACE
                        <<endl;
        }
}
#ifdef __BP_CONSTRAIN_DATA_H__

void INPUTFile::SaveOutput(wxTextOutputStream &tos, AtomBondArray &array, StringHash &setting)
{
        unsigned int i,j,label;
        int elem[ELEMENT_COUNT];
        ElementInfo* info=NULL;
        for(i=0;i<ELEMENT_COUNT;i++)
                elem[i]=0;
        for(i=0;i<array.GetCount();i++)
        {
                if(array[i].atom.atomId>0)
                {
                        elem[array[i].atom.atomId]++;
                }
                else if(array[i].atom.atomId==-1)
                {
                        elem[1]++;
                }
        }
        //tos<<wxT(">> export HomeDir=/home/liufenglai/molcas ")<<endl;
        tos<<MOLCAS_FILE_OUTPUT_BEGIN<<endl;
        if(setting.find(wxT("Title"))!=setting.end())
        {
                if(!setting.find(wxT("Title"))->second.IsEmpty())
                {
                        tos<<MOLCAS_FILE_GENERAL_TITLE<<endl;
                        tos<<setting.find(wxT("Title"))->second<<endl;
                }
        }
        for(i=ELEMENT_COUNT-1;i>0;i--)
        {
                if(elem[i]>0)
                {
                        info=GetElementInfo((AtomId)i);
                        tos<<MOLCAS_FILE_GENERAL_BASIS_SET_BEGIN<<endl;
                        tos<<info->name
                                <<MOLCAS_FILE_GENERAL_POINT
                                <<setting.find(wxT("BasisSet"))->second
                                <<MOLCAS_FILE_GENERAL_POINT
                                <<MOLCAS_FILE_GENERAL_POINT
                                <<MOLCAS_FILE_GENERAL_POINT
                                <<MOLCAS_FILE_GENERAL_POINT
                                <<MOLCAS_FILE_GENERAL_POINT
                                <<endl;

  j=0;
                        label=1;
                        while(j<array.GetCount())
                        {
                                if(abs(array[j].atom.atomId)==(int)i)
                                {
                                        tos<<info->name
                                                <<wxString::Format(wxT("%d"),label)
                                                <<FILE_GENERAL_SPACE
                                                <<wxString::Format(wxT("%f"),array[j].atom.posX)
                                                <<FILE_GENERAL_SPACE
                                                <<wxString::Format(wxT("%f"),array[j].atom.posY)
                                                <<FILE_GENERAL_SPACE
                                                <<wxString::Format(wxT("%f"),array[j].atom.posZ)
                                                <<endl;
                                                //<<FILE_GENERAL_SPACE
                                                //<<MOLCAS_FILE_POSITION_MEASURE
                                        label++;
                                }
                                j++;
                        }
                        tos<<MOLCAS_FILE_METHOD_BASIS_SET_END<<endl;
                }
        }
        tos<<MOLCAS_FILE_GENERAL_END<<endl;
}
#endif

void INPUTFile::SaveSESettings(wxTextOutputStream &tos, StringHash &setting)
{
        wxString jobType=setting.find(wxT("JobType"))->second;
        wxString keywords=wxEmptyString;
        if(setting.find(wxT("KeyWords"))!=setting.end())
        {
                keywords=setting.find(wxT("KeyWords"))->second;
        }
        //wxMessageBox(keywords);
        if(!jobType.IsEmpty()){
                if(jobType.Cmp(wxT("SP"))==0)
                        jobType=wxT("1SCF");
                else if(jobType.Cmp(wxT("TS"))==0)
                        jobType=wxT("TS");
                else if(jobType.Cmp(wxT("FREQ"))==0)
                        jobType=wxT("FREQ");
                else
                        jobType=wxEmptyString;
        }
        if(setting.find(wxT("SEMod"))!=setting.end())  //add SEModel
        {
                tos<<setting.find(wxT("SEMod"))->second<<FILE_GENERAL_SPACE;
        }
        tos        <<jobType
                <<FILE_GENERAL_SPACE
                <<wxT("charge=")
                <<setting.find(wxT("Charge"))->second
                <<FILE_GENERAL_SPACE;
        wxString multi;
        long value;
        multi=setting.find(wxT("Multi"))->second;
        multi.ToLong(&value);
        switch(value)
        {
        case 2:
                multi=wxT("doublet");
                break;
        case 3:
                multi=wxT("triplet");
                break;
        default:
                multi=wxEmptyString;
                break;
        }
        tos<<multi<<FILE_GENERAL_SPACE;
        if(setting.find(wxT("Format"))!=setting.end())  //add SEModel
        {
                tos <<wxT("calc_surface")
                        <<FILE_GENERAL_SPACE;
        }
        if(setting.find(wxT("OptInteration"))!=setting.end())  //add SEModel
        {
                tos <<wxT("opt=")
                        <<setting.find(wxT("OptInteration"))->second.Trim()
                        <<FILE_GENERAL_SPACE;
        }
        if(!keywords.IsEmpty())
        {
                keywords.Replace(wxT(","),wxT(" "));
                tos <<wxT("debug")
                        <<FILE_GENERAL_SPACE
                        <<keywords
                        <<FILE_GENERAL_SPACE;
        }
        tos<<endl;
        if(setting.find(wxT("Title"))!=setting.end())
        {
                if(!setting.find(wxT("Title"))->second.IsEmpty())
                {
                        tos<<setting.find(wxT("Title"))->second;
                }
        }
        tos<<endl;
        tos<<endl;
}
#ifdef __BP_CONSTRAIN_DATA_H__

void INPUTFile::SaveSettings(wxTextOutputStream &tos, StringHash &setting)
{
    long lv;
        tos<<MOLCAS_FILE_SETTING_BEGIN<<endl;
        if(setting.find(wxT("Title"))!=setting.end())
        {
                if(!setting.find(wxT("Title"))->second.IsEmpty())
                {
                        tos<<MOLCAS_FILE_GENERAL_TITLE<<endl;
                        tos<<setting.find(wxT("Title"))->second<<endl;
                }
        }
        if(setting.find(wxT("DFTFunc"))!=setting.end())
        {
                tos<<MOLCAS_FILE_METHOD_DFT<<endl;
                tos<<setting.find(wxT("DFTFunc"))->second<<endl;
        }


        if(setting.find(wxT("Charge"))!=setting.end())
        {
                tos<<MOLCAS_FILE_UHF_FLAG<<endl;
                tos<<MOLCAS_FILE_ZSPIN_FLAG<<endl;
                //ZSPIN
                setting.find(wxT("Multi"))->second.ToLong(&lv);
                lv--;
                tos<<wxString::Format(wxT("%ld"),lv)<<endl;
                tos<<MOLCAS_FILE_CHARGE_FLAG<<endl;
                tos<<setting.find(wxT("Charge"))->second<<endl;
        }
        tos<<MOLCAS_FILE_GENERAL_END<<endl;
}

void INPUTFile::SaveGeometryOptimization(wxTextOutputStream &tos, AtomBondArray &array,StringHash &setting, ConstrainData &constrainData)
{
        tos<<MOLCAS_FILE_ALASKA_FLAG<<endl;
        tos<<MOLCAS_FILE_GENERAL_END<<endl;
        tos<<MOLCAS_FILE_SLAPAF_FLAG<<endl;
        //CONSTRAIN
        SaveConstraints(tos,array,constrainData,true);

        tos<<MOLCAS_FILE_ITERATION_FLAG<<endl;
        tos<<setting.find(wxT("OptInteration"))->second<<endl;
        tos<<MOLCAS_FILE_THRESHOLD_FLAG<<endl;
        tos<<setting.find(wxT("ECharge"))->second
                <<FILE_GENERAL_SPACE
                <<setting.find(wxT("Grad"))->second
                <<endl;
        tos<<MOLCAS_FILE_GENERAL_END<<endl;
}
void INPUTFile::SaveConstraints(wxTextOutputStream &tos, AtomBondArray &array, ConstrainData &constrainData,bool useFlag)
{
        wxArrayString lenArray=constrainData.GetConstrainLengthArray(array,true);
        wxArrayString angArray=constrainData.GetConstrainAngleArray(array,true);
        wxArrayString dihArray=constrainData.GetConstrainDihedralArray(array,true);
        wxArrayString tokens;
        unsigned int i,j;
        if(lenArray.GetCount()>0||angArray.GetCount()>0||dihArray.GetCount()>0)
        {
                if(useFlag)
                        tos << MOLCAS_FILE_CONSTRAIN_BEGIN <<endl;
                for(i=0;i< lenArray.GetCount();i++)
                {
                        tokens=wxStringTokenize(lenArray[i],wxT("\t"),wxTOKEN_STRTOK);
                        tos << MOLCAS_FILE_CONSTRAIN_BOND_PREFIX
                                << wxString::Format(wxT("%d"),i+1)
                                << MOLCAS_FILE_CONSTRAIN_OPERATOR
                                << MOLCAS_FILE_CONSTRAIN_BOND_FLAG;
                        for(j=0;j<2;j++)
                                tos <<wxT(" ")<<tokens[j];
                        tos << endl;
                }
                for(i=0;i< angArray.GetCount();i++)
                {
                        tokens=wxStringTokenize(angArray[i],wxT("\t"),wxTOKEN_STRTOK);
                        tos << MOLCAS_FILE_CONSTRAIN_ANGLE_PREFIX
                                << wxString::Format(wxT("%d"),i+1)
                                << MOLCAS_FILE_CONSTRAIN_OPERATOR
                                << MOLCAS_FILE_CONSTRAIN_ANGLE_FLAG;
                        for(j=0;j<3;j++)
                                tos <<wxT(" ")<<tokens[j];
                        tos <<endl;
                }
                for(i=0;i< dihArray.GetCount(); i++)
                {
                        tokens=wxStringTokenize(dihArray[i],wxT("\t"),wxTOKEN_STRTOK);
                        tos << MOLCAS_FILE_CONSTRAIN_DIHEDRAL_PREFIX
                                << wxString::Format(wxT("%d"),i+1)
                                << MOLCAS_FILE_CONSTRAIN_OPERATOR
                                << MOLCAS_FILE_CONSTRAIN_DIHEDRAL_FLAG;
                        for(j=0;j<4;j++)
                                tos << wxT(" ")<<tokens[j];
                        tos << endl;
                }
                tos << MOLCAS_FILE_CONSTRAIN_VALUE << endl;
                for(i=0;i< lenArray.GetCount();i++)
                {
                        tokens=wxStringTokenize(lenArray[i],wxT("\t"),wxTOKEN_STRTOK);
                        tos << MOLCAS_FILE_CONSTRAIN_BOND_PREFIX
                                << wxString::Format(wxT("%d"),i+1)
                                << MOLCAS_FILE_CONSTRAIN_OPERATOR
                                << tokens[2]
                                << wxT(" ")
                                << MOLCAS_FILE_CONSTRAIN_BOND_MEASURE
                                << endl;
                }
                for(i=0;i< angArray.GetCount();i++)
                {
                        tokens=wxStringTokenize(angArray[i],wxT("\t"),wxTOKEN_STRTOK);
                        tos << MOLCAS_FILE_CONSTRAIN_ANGLE_PREFIX
                                << wxString::Format(wxT("%d"),i+1)
                                << MOLCAS_FILE_CONSTRAIN_OPERATOR
                                << tokens[3]
                                << wxT(" ")
                                << MOLCAS_FILE_CONSTRAIN_ANGLE_MEASURE
                                << endl;
                }
                for(i=0;i< dihArray.GetCount(); i++)
                {
                        tokens=wxStringTokenize(dihArray[i],wxT("\t"),wxTOKEN_STRTOK);
                        tos << MOLCAS_FILE_CONSTRAIN_DIHEDRAL_PREFIX
                                << wxString::Format(wxT("%d"),i+1)
                                << MOLCAS_FILE_CONSTRAIN_OPERATOR
                                << tokens[4]
                                << wxT(" ")
                                << MOLCAS_FILE_CONSTRAIN_DIHEDRAL_MEASURE
                                << endl;
                }
                if(useFlag)
                        tos << MOLCAS_FILE_CONSTRAIN_END <<endl;
        }
}
void INPUTFile::SaveTransitionState(wxTextOutputStream &tos, AtomBondArray &array, StringHash &setting, ConstrainData &constrainData)
{
        tos<<MOLCAS_FILE_ALASKA_FLAG<<endl;
        tos<<MOLCAS_FILE_GENERAL_END<<endl;
        tos<<MOLCAS_FILE_SLAPAF_FLAG<<endl;
        //CONSTRAIN
        SaveConstraints(tos,array,constrainData,true);

        tos<<MOLCAS_FILE_JOB_TYPE_TRANSITION_STATE<<endl;
        tos<<MOLCAS_FILE_ITERATION_FLAG<<endl;
        tos<<setting.find(wxT("OptInteration"))->second<<endl;
        tos<<MOLCAS_FILE_THRESHOLD_FLAG<<endl;
        tos<<setting.find(wxT("ECharge"))->second
                <<FILE_GENERAL_SPACE
                <<setting.find(wxT("Grad"))->second
                <<endl;
        tos<<MOLCAS_FILE_GENERAL_END<<endl;
}
void INPUTFile::SaveFormat(wxTextOutputStream &tos,wxString filename,StringHash &setting)
{
        wxFileName fname(filename);
        if(setting.find(wxT("Format"))!=setting.end())
        {
                tos<<MOLCAS_FILE_GRID_IT_FLAG<<endl;
                if(setting.find(wxT("Format"))->second.MakeUpper().Cmp(wxT("ASCII"))==0)
                        tos<<setting.find(wxT("Format"))->second<<endl;
                tos<<wxT("name")<<endl;
                tos<<fname.GetName()<<endl;
                        //tempStr=setting.find(wxT("FileName"))->second;
                tos<<MOLCAS_FILE_GENERAL_END<<endl;
                /*tos<<endl;
                //tos<<MOLCAS_FILE_GRID_IT_EXTEND<<endl;
                tempStr.Replace(wxT(".M2Msi"),wxEmptyString);
                tempStr.MakeUpper();
                tos<<wxT("!cp ")
                        <<wxT("$WorkDir/molcas_tmp.")
                        <<tempStr
                        <<wxT(".M2Msi   $HomeDir")
                        <<endl;
                */
        }
}

void INPUTFile::SaveJobType(wxTextOutputStream &tos, AtomBondArray &array, StringHash &setting, ConstrainData &constrainData)
{
        unsigned int i;
        wxString jobType[4]={wxT("SP"),        wxT("OPT"),        wxT("TS"),wxT("FREQ")};
        for(i=0;i<4;i++)
        {
                if(jobType[i].Cmp(setting.find(wxT("JobType"))->second)==0)
                        break;
        }
        switch(i)
        {
        case 0://Single Point Energy
                break;
        case 1://Geometry Optimization
                SaveGeometryOptimization(tos,array,setting,constrainData);
                break;
        case 2://Transition State
                SaveTransitionState(tos,array,setting,constrainData);
                break;
        default:
                break;
        }
}
void INPUTFile::SaveConstrainFile(const wxString &fileName, AtomBondArray &array, ConstrainData &constrainData)
{
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);

        SaveConstraints(tos,array,constrainData,false);
}

#endif

void INPUTFile::SaveFileSE(const wxString &fileName, RenderingData* pData, SimuJob* pJob)
{
        if(pData == NULL || pData->GetAtomCount()<=0)
                return;
        StringHash &setting=pJob->GetJobControl();
        AtomBondArray &array=pData->GetAtomBondArray();
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);

        //SaveExportSE(tos);
        SaveSESettings(tos,setting);
        SaveOutputSE(tos,array,setting);
}

void INPUTFile::SaveFileAI(const wxString &fileName, RenderingData* pData, SimuJob* pJob)
{
        if(pData == NULL || pData->GetAtomCount()<=0)
                return;
        unsigned int i;
        int elemId;
        StringHash &setting=pJob->GetJobControl();
        //StringHash &surface=pCtrlInst->GetJobSurfaceSettings();
        AtomBondArray &array=pData->GetAtomBondArray();
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        wxString jobType=setting.find(wxT("JobType"))->second;
        wxString keywords=wxEmptyString;
        if(setting.find(wxT("Method"))!=setting.end())
        {
                wxString method=setting.find(wxT("Method"))->second;
                tos<< method << FILE_GENERAL_SPACE;
        }
        if(pJob->CanCalcSurface())  //add SEModel
        {
                tos <<wxT("calc_surface")
                        <<FILE_GENERAL_SPACE;
        }
        if(setting.find(wxT("BasisSet"))!=setting.end())
        {
                tos<< setting.find(wxT("BasisSet"))->second << FILE_GENERAL_SPACE;
        }
        if(!jobType.IsEmpty()){
                if(jobType.Cmp(wxT("SP"))==0)
                        jobType=wxEmptyString;
                else if(jobType.Cmp(wxT("OPT"))==0)
                        jobType=wxT("OPT");
                else if(jobType.Cmp(wxT("TS"))==0)
                        jobType=wxT("TSOPT");
                else if(jobType.Cmp(wxT("FREQ"))==0)
                        jobType=wxT("FREQ");
                tos<< jobType<< FILE_GENERAL_SPACE;
        }

        if(setting.find(wxT("KeyWords"))!=setting.end())
        {
                keywords=setting.find(wxT("KeyWords"))->second;
                if(!keywords.IsEmpty())
                {
                        keywords.Replace(wxT(","),wxT(" "));
                        tos <<keywords<<FILE_GENERAL_SPACE;
                }
        }
        //wxMessageBox(keywords);
        if(setting.find(wxT("SEMod"))!=setting.end())  //add SEModel
        {
                tos<<setting.find(wxT("SEMod"))->second<<FILE_GENERAL_SPACE;
        }
        tos<<wxT("printlev=3")<<endl;
        tos<<pJob->GetJobTitle()<<endl;
        wxString multi,charge;
        long value;
        charge=setting.find(wxT("Charge"))->second;
        charge.ToLong(&value);
        charge=wxString::Format(wxT("%ld"),value);
        multi=setting.find(wxT("Multi"))->second;
        multi.ToLong(&value);
        multi=wxString::Format(wxT("%ld"),value);
        tos<<charge<<FILE_GENERAL_SPACE<<multi<<FILE_GENERAL_SPACE<<endl;
        for(i=0;i<array.GetCount();i++)
        {
                if(array[i].atom.elemId == DEFAULT_HYDROGEN_ID){
            elemId = (int)ELEM_H;
        }else{
            elemId = array[i].atom.elemId ;
        }
                if(elemId>0)
                        tos <<FILE_GENERAL_SPACE
                                <<ELEM_NAMES[elemId-1];
                else
                        tos <<FILE_GENERAL_SPACE
                                <<wxT("X");
                tos<<FILE_GENERAL_SPACE
                        <<wxString::Format(wxT("%-f"),array[i].atom.pos.x)
                        <<FILE_GENERAL_SPACE
                        <<wxString::Format(wxT("%-f"),array[i].atom.pos.y)
                        <<FILE_GENERAL_SPACE
                        <<wxString::Format(wxT("%-f"),array[i].atom.pos.z)
                        <<endl;
        }
        tos<< AI_FILE_CART_END_FLAG <<endl;
}
