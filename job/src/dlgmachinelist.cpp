/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "dlgmachinelist.h"
#include "envutil.h"
#include "xmlfile.h"
#include "stringutil.h"
#include <wx/arrimpl.cpp>
#include "dlgmasterkey.h"
#include "dlgmasterlogin.h"
#include "dlgnewmachine.h"
#include "dlglogin.h"
#include <wx/textfile.h>


enum{
        CTRL_ID_BTN_NEW=1000,
        CTRL_ID_BTN_EDITHOST,
        CTRL_ID_BTN_REMOVEHOST,
      CTRL_ID_BTN_STATUS,
        CTRL_ID_BTN_DISKUSAGE,
        CTRL_ID_BTN_PROCESSES,
        CTRL_ID_GRID_MACHINELIST
};

int POPUPMENU_MACHINELIST_NEW = wxNewId();
int POPUPMENU_MACHINELIST_EDIT = wxNewId();
int POPUPMENU_MACHINELIST_REMOVE = wxNewId();

///////////////////////////////////////////////////////////////////////////
bool DlgMachineList::GetMachineList(void)
{
    m_machineinfoarray.Empty();
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
        MachineFile::LoadFile(fileName,m_machineinfoarray);
        int i;
        m_gridmachinelist->ClearGrid();
        m_oldhosts=m_machineinfoarray.GetCount();
        int dr=m_oldhosts-m_gridmachinelist->GetNumberRows();
        if(dr>0)m_gridmachinelist->AppendRows(dr,true);

                        for(i=0;i<(int)m_machineinfoarray.GetCount();i++)
                {
                        m_gridmachinelist->SetCellValue(i,0,m_machineinfoarray[i].masterid);
                        m_gridmachinelist->SetCellValue(i,1,m_machineinfoarray[i].shortname);
        //                m_gridmachinelist->SetCellValue(i,1,m_machineinfoarray[i].qsystem);
                        m_gridmachinelist->SetCellValue(i,2,m_machineinfoarray[i].protocal);
                        m_gridmachinelist->SetCellValue(i,3,m_machineinfoarray[i].interprocessor);
                        m_gridmachinelist->SetCellValue(i,4,m_machineinfoarray[i].nodes);
                }

        return true;

}

DlgMachineList::DlgMachineList( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
//        this->SetSizeHints( wxDefaultSize, wxDefaultSize );

        wxBoxSizer* bSizer1;
        bSizer1 = new wxBoxSizer( wxVERTICAL );

        m_gridmachinelist = new wxGrid( this, CTRL_ID_GRID_MACHINELIST, wxDefaultPosition, wxDefaultSize, 0 );

        // Grid
        m_gridmachinelist->CreateGrid( 5, 5);
        m_gridmachinelist->EnableEditing( false );
        m_gridmachinelist->EnableGridLines( true );
        m_gridmachinelist->EnableDragGridSize( false );
        m_gridmachinelist->SetMargins( 0, 0 );

        // Columns
        m_gridmachinelist->SetColSize( 0, 80 );
        m_gridmachinelist->SetColSize( 1, 150 );
        m_gridmachinelist->SetColSize( 2, 110 );
        m_gridmachinelist->SetColSize( 3, 80 );
        m_gridmachinelist->SetColSize( 4, 80 );
        m_gridmachinelist->EnableDragColMove( false );
        m_gridmachinelist->EnableDragColSize( true );
        m_gridmachinelist->SetColLabelSize( 30 );
        m_gridmachinelist->SetColLabelValue( 0, wxT("MasterID") );
        m_gridmachinelist->SetColLabelValue( 1, wxT("Host") );
        //m_gridmachinelist->SetColLabelValue( 1, wxT("Queue System") );
        m_gridmachinelist->SetColLabelValue( 2, wxT("Protocol") );
        m_gridmachinelist->SetColLabelValue( 3, wxT("Processors") );
        m_gridmachinelist->SetColLabelValue( 4, wxT("Nodes") );
        m_gridmachinelist->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );

        // Rows
        m_gridmachinelist->EnableDragRowSize( true );
        m_gridmachinelist->SetRowLabelSize( 0 );
        m_gridmachinelist->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );

        // Label Appearance

        // Cell Defaults
        m_gridmachinelist->SetDefaultCellAlignment( wxALIGN_CENTRE, wxALIGN_TOP );
        m_gridmachinelist->SetMinSize( wxSize( 500,180 ) );
        //GetMachineList();
        bSizer1->Add( m_gridmachinelist, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );
        m_gridmachinelist->SetSelectionMode(wxGrid::wxGridSelectRows);
        wxBoxSizer* bSizer2;
        bSizer2 = new wxBoxSizer( wxHORIZONTAL );
        m_btnnew = new wxButton( this, CTRL_ID_BTN_NEW, wxT("New Host"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnnew, 0, wxALL, 5 );

        m_btnedit = new wxButton( this, CTRL_ID_BTN_EDITHOST, wxT("Edit Host"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnedit, 0, wxALL, 5 );

        m_btnremove = new wxButton( this, CTRL_ID_BTN_REMOVEHOST, wxT("Remove Host"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnremove, 0, wxALL, 5 );

        m_btnstatus = new wxButton( this, CTRL_ID_BTN_STATUS, wxT("Status"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnstatus, 0, wxALL, 5 );

        m_btndiskusage = new wxButton( this, CTRL_ID_BTN_DISKUSAGE, wxT("Disk Usage"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btndiskusage, 0, wxALL, 5 );

        m_btnprocesses = new wxButton( this, CTRL_ID_BTN_PROCESSES, wxT("Processes"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnprocesses, 0, wxALL, 5 );

        bSizer1->Add( bSizer2, 0, wxFIXED_MINSIZE, 5 );

        m_txtshow = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_WORDWRAP );
        m_txtshow->SetMinSize( wxSize( 500,240 ) );

        bSizer1->Add( m_txtshow, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_TOP|wxALL, 5 );

        wxBoxSizer* bSizer3;
        bSizer3 = new wxBoxSizer( wxHORIZONTAL );

        m_btnok = new wxButton( this, wxID_OK, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer3->Add( m_btnok, 0, wxALL, 5 );

        m_btncancel = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer3->Add( m_btncancel, 0, wxALL, 5 );

        bSizer1->Add( bSizer3, 1, 5 );

        this->SetSizer( bSizer1 );
        bSizer1->SetSizeHints(this);
        selmachine=-1;
        m_connected=false;
    m_sshclient=new SSHClient();
    m_busy=false;
//    m_txtshow->AppendText(m_machineinfoarray[0].shortname);
 //   m_txtshow->AppendText(m_machineinfoarray[0].username);
 //   m_txtshow->AppendText(m_machineinfoarray[0].password);
//        this->Layout();
}

BEGIN_EVENT_TABLE(DlgMachineList, wxDialog)
        EVT_BUTTON(wxID_OK, DlgMachineList::DoOk)
        EVT_BUTTON(wxID_CANCEL,DlgMachineList::DoCancel)
        EVT_BUTTON(CTRL_ID_BTN_NEW,DlgMachineList::OnNew)
        EVT_BUTTON(CTRL_ID_BTN_STATUS,DlgMachineList::DoStatus)
        EVT_BUTTON(CTRL_ID_BTN_DISKUSAGE,DlgMachineList::OnDiskUsage)
        EVT_BUTTON(CTRL_ID_BTN_PROCESSES,DlgMachineList::DoProcesses)
        EVT_GRID_CMD_CELL_LEFT_CLICK(CTRL_ID_GRID_MACHINELIST,DlgMachineList::OnMachineSelect)
        EVT_GRID_CMD_CELL_RIGHT_CLICK(CTRL_ID_GRID_MACHINELIST,DlgMachineList::OnPopupMenu)
    EVT_CLOSE(DlgMachineList::OnClose)
        EVT_MENU(POPUPMENU_MACHINELIST_NEW, DlgMachineList::OnNew)
        EVT_MENU(POPUPMENU_MACHINELIST_EDIT, DlgMachineList::OnEdit)
        EVT_BUTTON(CTRL_ID_BTN_EDITHOST,DlgMachineList::OnEdit)
        EVT_BUTTON(CTRL_ID_BTN_REMOVEHOST,DlgMachineList::OnRemove)
        EVT_MENU(POPUPMENU_MACHINELIST_REMOVE, DlgMachineList::OnRemove)
END_EVENT_TABLE()

void DlgMachineList::OnNew(wxCommandEvent&event)
{
    MasterInfoArray masterinfoarray;
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("master.conf");
    MasterFile::LoadFile(fileName,masterinfoarray);
    unsigned int total=masterinfoarray.GetCount();
    if(total==0){
        wxString msg=wxT("You have to set at least one MasterID first to protect your hosts.\n\r");
        msg=msg+wxT("Setting->Master Setting");
        wxMessageBox(msg);
        return;
    }

    DlgMasterLogin *masterlogin=new DlgMasterLogin(this,wxID_ANY,wxT("Master Login"));
    if(masterlogin->ShowModal()==wxID_CANCEL)return;
    delete masterlogin;
    DlgNewMachine *newmachine=new DlgNewMachine(this,wxID_ANY,wxT("New Host"));
    if(newmachine->ShowModal()==wxID_CANCEL)return;
    delete newmachine;
    GetMachineList();
}
void DlgMachineList::OnEdit(wxCommandEvent&event)
{
    if (selmachine==-1)return;

    DlgNewMachine *newmachine=new DlgNewMachine(this,wxID_ANY,wxT("Host Information Edit"));
    if(m_machineinfoarray[selmachine].protocal.Cmp(wxT("HTTP"))!=0){
            EnvUtil::SetMasterId(m_machineinfoarray[selmachine].masterid);
            while(EnvUtil::GetMasterKey().IsEmpty()||m_machineinfoarray[selmachine].masterkey.Cmp(EnvUtil::GetMasterKeyMD5())!=0){
            DlgMasterKey *pdlgmasterkey=new DlgMasterKey(this);
            pdlgmasterkey->SetHostname(m_machineinfoarray[selmachine].masterid);
            if(pdlgmasterkey->ShowModal()==wxID_CANCEL)return;
            //wxMessageBox(m_machineinfoarray[selmachine].masterkey);
            //wxMessageBox(EnvUtil::GetMasterKeyMD5());

            pdlgmasterkey=NULL;
            }
    }

    newmachine->SetInfo(m_machineinfoarray[selmachine]);
    newmachine->ShowModal();

    GetMachineList();
}

void DlgMachineList::OnRemove(wxCommandEvent&event)
{
    if (selmachine==-1)return;

    m_machineinfoarray.RemoveAt(selmachine);
        wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
        MachineFile::SaveFile(fileName,m_machineinfoarray);
        int i;
        for(i=0;i<(int)m_machineinfoarray.GetCount();i++)
    {
        m_gridmachinelist->SetCellValue(i,0,wxEmptyString);
        m_gridmachinelist->SetCellValue(i,1,wxEmptyString);
        m_gridmachinelist->SetCellValue(i,2,wxEmptyString);
        m_gridmachinelist->SetCellValue(i,3,wxEmptyString);
        m_gridmachinelist->SetCellValue(i,4,wxEmptyString);
    }

    GetMachineList();
}
void DlgMachineList::OnPopupMenu(wxGridEvent& event)
{
    selmachine=event.GetRow();
    if((unsigned int)selmachine<m_machineinfoarray.GetCount()){
    m_gridmachinelist->SelectRow(selmachine,false);
    m_connected=false;
    SetBtnStatus(1);
    }else{
    selmachine=-1;
    SetBtnStatus(0);
    }
    wxMenu menu;
    menu.Append(POPUPMENU_MACHINELIST_NEW, wxT("New"),wxT("New Machine."));
    menu.Append(POPUPMENU_MACHINELIST_EDIT, wxT("Edit"),wxT("Edit Selected Machine."));
    menu.Append(POPUPMENU_MACHINELIST_REMOVE, wxT("Remove"),wxT("Remove Selected Machine."));
    PopupMenu(&menu);
}

void DlgMachineList::SetBtnStatus(int status)
{
switch(status){
case 0:
        m_btnnew->Enable(true);
        m_btnedit->Enable(false);
        m_btnremove->Enable(false);
        m_btnstatus->Enable(false);
                m_btndiskusage->Enable(false);
                m_btnprocesses->Enable(false);
        break;
case 1:
        m_btnedit->Enable(true);
        m_btnremove->Enable(true);

if(m_machineinfoarray[selmachine].protocal.Cmp(wxT("HTTP"))==0)
    {
        m_btnstatus->Enable(false);
                m_btndiskusage->Enable(false);
                m_btnprocesses->Enable(false);
    }else{
        m_btnstatus->Enable(true);
                m_btndiskusage->Enable(true);
                m_btnprocesses->Enable(true);

    }
break;
}
}

void DlgMachineList::OnMachineSelect(wxGridEvent& event)
{
//    int oldmachine=selmachine;
    selmachine=event.GetRow();
    if((unsigned int)selmachine<m_machineinfoarray.GetCount()){
    m_gridmachinelist->SelectRow(selmachine,false);
    m_connected=false;
    SetBtnStatus(1);
    }else{
    SetBtnStatus(0);
    selmachine=-1;
    }

}
void DlgMachineList::DoStatus(wxCommandEvent&event)
{
    if (selmachine==-1)return;

    const char *input=NULL;
    //m_txtshow->AppendText(wxString::Format("%d",selmachine));
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].shortname);
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].username);
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].password);
    if(!m_connected)
    {
    if(m_machineinfoarray[selmachine].protocal.Cmp(wxT("HTTP"))!=0){
            EnvUtil::SetMasterId(m_machineinfoarray[selmachine].masterid);
            while(EnvUtil::GetMasterKey().IsEmpty()||m_machineinfoarray[selmachine].masterkey.Cmp(EnvUtil::GetMasterKeyMD5())!=0){
            DlgMasterKey *pdlgmasterkey=new DlgMasterKey(this);
            pdlgmasterkey->SetHostname(m_machineinfoarray[selmachine].masterid);
            if(pdlgmasterkey->ShowModal()==wxID_CANCEL)return;
            //wxMessageBox(m_machineinfoarray[selmachine].masterkey);
            //wxMessageBox(EnvUtil::GetMasterKeyMD5());
            pdlgmasterkey=NULL;
            }
    }


    //StringUtil::String2CharPointer(&input,m_machineinfoarray[selmachine].fullname);
//  m_sshclient->SetMasterkey(m_machineinfoarray[selmachine].masterkey);
    m_sshclient->SetHostname(m_machineinfoarray[selmachine].fullname);

    //fprintf(stderr, m_machineinfoarray[selmachine].username);
   // StringUtil::String2CharPointer(&input,m_machineinfoarray[selmachine].username);

    m_sshclient->SetUsername(m_machineinfoarray[selmachine].username);
  //  StringUtil::String2CharPointer(&input,m_machineinfoarray[selmachine].password);

    //fprintf(stderr, m_machineinfoarray[selmachine].password);
    m_sshclient->SetPassword(m_machineinfoarray[selmachine].password);
while(m_sshclient->ConnectToHost()!=1){
        DlgLogin *dlglogin=new DlgLogin(this);
        dlglogin->SetInfo(m_sshclient);
        dlglogin->ShowModal();
        dlglogin=NULL;
        if(!m_sshclient->GetRetry()){
            wxMessageBox(wxT("Connect failed......"));
            return;
        }else{
            m_machineinfoarray[selmachine].masterkey=m_sshclient->GetMasterkey();
        }
    }
    m_connected=true;
    }
    //if(sshclient->Upload(input,"test.in")==-1)wxMessageBox(wxT("open failed!"));
   // sprintf(input,"%s","uptime");
    input="uptime";
    //fprintf(stderr,input);
    wxArrayString arystrout=m_sshclient->GetOutputInfo(input);
    for(unsigned int i=0;i<arystrout.GetCount();i++)
    {
        m_txtshow->AppendText(arystrout[i]);
        m_txtshow->AppendText(wxT("\n"));
    }
  return;
}
void DlgMachineList::OnDiskUsage(wxCommandEvent&event)
{
    DoDiskUsage();
}

int DlgMachineList::DoDiskUsage()
{
    if (selmachine==-1)return -1;
    const char * input=NULL;
    //m_txtshow->AppendText(wxString::Format("%d",selmachine));
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].shortname);
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].username);
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].password);
    if(!m_connected)
    {
    if(m_machineinfoarray[selmachine].protocal.Cmp(wxT("HTTP"))!=0){
            EnvUtil::SetMasterId(m_machineinfoarray[selmachine].masterid);
            while(EnvUtil::GetMasterKey().IsEmpty()||m_machineinfoarray[selmachine].masterkey.Cmp(EnvUtil::GetMasterKeyMD5())!=0){
            DlgMasterKey *pdlgmasterkey=new DlgMasterKey(this);
            pdlgmasterkey->SetHostname(m_machineinfoarray[selmachine].masterid);
            if(pdlgmasterkey->ShowModal()==wxID_CANCEL)return -1;
            //wxMessageBox(m_machineinfoarray[selmachine].masterkey);
            //wxMessageBox(EnvUtil::GetMasterKeyMD5());
            pdlgmasterkey=NULL;
            }
    }

//    m_sshclient->SetMasterkey(m_machineinfoarray[selmachine].masterkey);
    m_sshclient->SetHostname(m_machineinfoarray[selmachine].fullname);

    //fprintf(stderr, m_machineinfoarray[selmachine].username);
   // StringUtil::String2CharPointer(&input,m_machineinfoarray[selmachine].username);

    m_sshclient->SetUsername(m_machineinfoarray[selmachine].username);
  //  StringUtil::String2CharPointer(&input,m_machineinfoarray[selmachine].password);

    //fprintf(stderr, m_machineinfoarray[selmachine].password);
    m_sshclient->SetPassword(m_machineinfoarray[selmachine].password);
while(m_sshclient->ConnectToHost()!=1){
        DlgLogin *dlglogin=new DlgLogin(this);
        dlglogin->SetInfo(m_sshclient);
        dlglogin->ShowModal();
        dlglogin=NULL;
        if(!m_sshclient->GetRetry()){
            wxMessageBox(wxT("Connect failed......"));
            return -1;
        }else{
            m_machineinfoarray[selmachine].masterkey=m_sshclient->GetMasterkey();
        }
    }
    m_connected=true;
    }
    //if(sshclient->Upload(input,"test.in")==-1)wxMessageBox(wxT("open failed!"));
   // sprintf(input,"%s","df");
    input="df";
    //fprintf(stderr,input);
    wxArrayString arystrout=m_sshclient->GetOutputInfo(input);
    for(unsigned int i=0;i<arystrout.GetCount();i++)
    {
        m_txtshow->AppendText(arystrout[i]);
        m_txtshow->AppendText(wxT("\n"));
    }
  return 0;
}

void DlgMachineList::DoProcesses(wxCommandEvent&event)
{
    if (selmachine==-1)return;

    const char * input=NULL;
    //m_txtshow->AppendText(wxString::Format("%d",selmachine));
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].shortname);
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].username);
    //m_txtshow->AppendText(m_machineinfoarray[selmachine].password);
    if(!m_connected)
    {
    if(m_machineinfoarray[selmachine].protocal.Cmp(wxT("HTTP"))!=0){
            EnvUtil::SetMasterId(m_machineinfoarray[selmachine].masterid);
            while(EnvUtil::GetMasterKey().IsEmpty()||m_machineinfoarray[selmachine].masterkey.Cmp(EnvUtil::GetMasterKeyMD5())!=0){
            DlgMasterKey *pdlgmasterkey=new DlgMasterKey(this);
            pdlgmasterkey->SetHostname(m_machineinfoarray[selmachine].masterid);
            if(pdlgmasterkey->ShowModal()==wxID_CANCEL)return;
            //wxMessageBox(m_machineinfoarray[selmachine].masterkey);
            //wxMessageBox(EnvUtil::GetMasterKeyMD5());
            pdlgmasterkey=NULL;
            }
    }

//    m_sshclient->SetMasterkey(m_machineinfoarray[selmachine].masterkey);
    m_sshclient->SetHostname(m_machineinfoarray[selmachine].fullname);

    //fprintf(stderr, m_machineinfoarray[selmachine].username);
   // StringUtil::String2CharPointer(&input,m_machineinfoarray[selmachine].username);

    m_sshclient->SetUsername(m_machineinfoarray[selmachine].username);
  //  StringUtil::String2CharPointer(&input,m_machineinfoarray[selmachine].password);

    //fprintf(stderr, m_machineinfoarray[selmachine].password);
    m_sshclient->SetPassword(m_machineinfoarray[selmachine].password);
while(m_sshclient->ConnectToHost()!=1){
        DlgLogin *dlglogin=new DlgLogin(this);
        dlglogin->SetInfo(m_sshclient);
        dlglogin->ShowModal();
        dlglogin=NULL;
        if(!m_sshclient->GetRetry()){
            wxMessageBox(wxT("Connect failed......"));
            return;
        }else{
            m_machineinfoarray[selmachine].masterkey=m_sshclient->GetMasterkey();
        }
    }
    m_connected=true;
    }
    //if(sshclient->Upload(input,"test.in")==-1)wxMessageBox(wxT("open failed!"));
    //sprintf(input,"%s","ps");
    input="ps";
    //fprintf(stderr,input);
    wxArrayString arystrout=m_sshclient->GetOutputInfo(input);
    for(unsigned int i=0;i<arystrout.GetCount();i++)
    {
        m_txtshow->AppendText(arystrout[i]);
        m_txtshow->AppendText(wxT("\n"));
    }

  return;
}

void DlgMachineList::DoOk(wxCommandEvent& event)
{

    this->Close();
}

void DlgMachineList::DoCancel(wxCommandEvent&event)
{
    this->Close();
}

void DlgMachineList::OnClose(wxCloseEvent& event)
{
    if(IsModal())
        {
                EndModal(wxID_OK);
        }
        else
        {
                SetReturnCode(wxID_OK);
                this->Show(false);
        }
}

DlgMachineList::~DlgMachineList()
{
m_machineinfoarray.Clear();
}

MachineInfo::MachineInfo()
{
    machineid=wxEmptyString;
    fullname=wxEmptyString;
    shortname=wxEmptyString;
    interprocessor=wxEmptyString;
    nodes=wxEmptyString;
    username=wxEmptyString;
    password=wxEmptyString;
    protocal=wxEmptyString;
    submitway=wxEmptyString;
    remotecmd=wxEmptyString;
    remoteworkbase=wxEmptyString;
    scriptfile=wxEmptyString;
    masterkey=wxEmptyString;
    masterid=wxEmptyString;
}

WX_DEFINE_OBJARRAY(MachineInfoArray);

void MachineFile::LoadFile(const wxString& strLoadedFileName, MachineInfoArray& machineinfoarray)
{
    wxTextFile machinefile(strLoadedFileName);
    if(!machinefile.Exists()){
#ifdef __DEBUG__
        fprintf(stderr,"machine file no exist!");
#endif
        return;}
    machinefile.Open();
    if(machinefile.GetLineCount()==0){
#ifdef __DEBUG__
        fprintf(stderr,"machine file is empty!");
#endif
        return;}
    machineinfoarray.Clear();
        wxString strLine;
    strLine=machinefile.GetFirstLine();
         while(FILE_BEGIN_MACHINEINFOS.Cmp(strLine) != 0)strLine=machinefile.GetNextLine();
        while( FILE_END_MACHINEINFOS.Cmp(strLine) != 0 && !machinefile.Eof())
        {//IRS BEGIN
                MachineInfo minfo = MachineInfo();
                int i=0;
                while(FILE_BEGIN_MACHINE.Cmp(strLine) != 0)strLine=machinefile.GetNextLine();
                while( (FILE_END_MACHINE.Cmp(strLine) != 0) && (!machinefile.Eof()))
                {
        strLine=machinefile.GetNextLine();
        switch(i){
                case 0:
                minfo.machineid=strLine;
                //wxMessageBox(minfo.machineid);
                break;
                case 1:
                minfo.fullname=strLine;
                break;
                case 2:
                minfo.shortname=strLine;
        //        wxMessageBox(minfo.shortname);
                break;
                case 3:
                minfo.interprocessor=strLine;
                break;
                case 4:
                minfo.nodes=strLine;
                break;
                case 5:
                minfo.username=strLine;
                break;
                case 6:
                minfo.password=strLine;
//                minfo.password=EnvUtil::GetDecipherCode(strLine);
                break;
                case 7:
                minfo.protocal=strLine;
                break;
                case 8:
                minfo.submitway=strLine;
                break;
                case 9:
                minfo.remotecmd=strLine;
                break;
                case 10:
                minfo.scriptfile=strLine;
                break;
                case 11:
                minfo.remoteworkbase=strLine;
                break;
                case 12:
                minfo.masterid=strLine;
                case 13:
                minfo.masterkey=strLine;
                };
                i++;
        }//IR END
                machineinfoarray.Add(minfo);
                strLine=machinefile.GetNextLine();
    }
    machinefile.Close();
}

void MachineFile::SaveFile(const wxString fileName,MachineInfoArray machineinfoarray) {
    unsigned int i;

    unsigned int totalhosts=machineinfoarray.GetCount();

    if (totalhosts<=0){
#ifdef __DEBUG__
    fprintf(stderr,"No host's infomation to save!");
#endif
    if(wxFileExists(fileName))wxRemoveFile(fileName);
    return;
    }

    wxTextFile machinefile(fileName);
    if(machinefile.Exists()){
        machinefile.Open();
        machinefile.Clear();
    }else
        machinefile.Create();
    machinefile.AddLine(FILE_BEGIN_MACHINEINFOS,wxTextFileType_Unix);

    for(i = 0; i < totalhosts; i++) {
        MachineInfo minfo=machineinfoarray[i];
                machinefile.AddLine(FILE_BEGIN_MACHINE,wxTextFileType_Unix);
                machinefile.AddLine(minfo.machineid,wxTextFileType_Unix);
                machinefile.AddLine(minfo.fullname,wxTextFileType_Unix);
                machinefile.AddLine(minfo.shortname,wxTextFileType_Unix);
                machinefile.AddLine(minfo.interprocessor,wxTextFileType_Unix);
                machinefile.AddLine(minfo.nodes,wxTextFileType_Unix);
                machinefile.AddLine(minfo.username,wxTextFileType_Unix);
                //machinefile.AddLine(EnvUtil::GetEncryptCode(minfo.password),wxTextFileType_Unix);
                machinefile.AddLine(minfo.password,wxTextFileType_Unix);
                machinefile.AddLine(minfo.protocal,wxTextFileType_Unix);
                machinefile.AddLine(minfo.submitway,wxTextFileType_Unix);
                machinefile.AddLine(minfo.remotecmd,wxTextFileType_Unix);
                machinefile.AddLine(minfo.scriptfile,wxTextFileType_Unix);
                machinefile.AddLine(minfo.remoteworkbase,wxTextFileType_Unix);
                machinefile.AddLine(minfo.masterid,wxTextFileType_Unix);
                machinefile.AddLine(minfo.masterkey,wxTextFileType_Unix);
                machinefile.AddLine(FILE_END_MACHINE,wxTextFileType_Unix);
    }
        machinefile.AddLine(FILE_END_MACHINEINFOS,wxTextFileType_Unix);
        machinefile.Write(wxTextFileType_Unix);
        machinefile.Close();
}
