/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "dlgmasterlist.h"
#include "envutil.h"
#include "stringutil.h"
#include <wx/arrimpl.cpp>
#include "dlgnewmaster.h"
#include "dlgmasterkey.h"
#include <wx/textfile.h>
#include "dlgmachinelist.h"

enum{
        CTRL_ID_BTN_NEW=1000,
        CTRL_ID_BTN_EDITHOST,
        CTRL_ID_BTN_REMOVEHOST,
        CTRL_ID_LIST_MASTERLIST
};


///////////////////////////////////////////////////////////////////////////
bool DlgMasterList::GetMasterList(void)
{
    m_masterinfoarray.Empty();
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("master.conf");
        MasterFile::LoadFile(fileName,m_masterinfoarray);
        int i;
        m_listmasterlist->Clear();
        for(i=0;i<(int)m_masterinfoarray.GetCount();i++)
                        m_listmasterlist->Append(m_masterinfoarray[i].masterid);

        return true;

}

DlgMasterList::DlgMasterList( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
//        this->SetSizeHints( wxDefaultSize, wxDefaultSize );

        wxBoxSizer* bSizer1;
        bSizer1 = new wxBoxSizer( wxHORIZONTAL );

        m_listmasterlist = new wxListBox( this, CTRL_ID_LIST_MASTERLIST, wxDefaultPosition, wxDefaultSize, 0 );
        m_listmasterlist->SetMinSize( wxSize( 200,280 ) );
        //GetmasterList();
    bSizer1->Add( m_listmasterlist, 0, wxALL, 5 );
        wxBoxSizer* bSizer2;
        bSizer2 = new wxBoxSizer( wxVERTICAL );
        m_btnnew = new wxButton( this, CTRL_ID_BTN_NEW, wxT("New"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnnew, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        m_btnedit = new wxButton( this, CTRL_ID_BTN_EDITHOST, wxT("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnedit, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        m_btnremove = new wxButton( this, CTRL_ID_BTN_REMOVEHOST, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnremove, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        m_btnok = new wxButton( this, wxID_OK, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btnok, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        m_btncancel = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer2->Add( m_btncancel, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        bSizer1->Add( bSizer2, 0, wxFIXED_MINSIZE, 5 );

         this->SetSizer( bSizer1 );
        bSizer1->SetSizeHints(this);
        selmaster=-1;
        GetMasterList();
        SetBtnStatus(0);
//        this->Layout();
}

BEGIN_EVENT_TABLE(DlgMasterList, wxDialog)
        EVT_BUTTON(wxID_OK, DlgMasterList::DoOk)
        EVT_BUTTON(wxID_CANCEL,DlgMasterList::DoCancel)
        EVT_BUTTON(CTRL_ID_BTN_NEW,DlgMasterList::OnNew)
        EVT_LISTBOX(CTRL_ID_LIST_MASTERLIST,DlgMasterList::OnMasterSelect)
    EVT_CLOSE(DlgMasterList::OnClose)
        EVT_BUTTON(CTRL_ID_BTN_EDITHOST,DlgMasterList::OnEdit)
        EVT_BUTTON(CTRL_ID_BTN_REMOVEHOST,DlgMasterList::OnRemove)
END_EVENT_TABLE()

void DlgMasterList::OnNew(wxCommandEvent&event)
{
    DlgNewMaster *newmaster=new DlgNewMaster(this,wxID_ANY,wxT("Master Setting"));
    newmaster->ShowModal();
    delete newmaster;
    GetMasterList();
}
void DlgMasterList::OnEdit(wxCommandEvent&event)
{
    if (selmaster==-1)return;
    EnvUtil::SetMasterId(m_masterinfoarray[selmaster].masterid);
    while(EnvUtil::GetMasterKey().IsEmpty()||m_masterinfoarray[selmaster].masterkey.Cmp(EnvUtil::GetMasterKeyMD5())!=0){
        DlgMasterKey *pdlgmasterkey=new DlgMasterKey(this);
        pdlgmasterkey->SetHostname(m_masterinfoarray[selmaster].masterid);
        if(pdlgmasterkey->ShowModal()==wxID_CANCEL)return;
        pdlgmasterkey=NULL;
    }

    DlgNewMaster *newmaster=new DlgNewMaster(this,wxID_ANY,wxT("Master Setting"));
    newmaster->SetInfo(m_masterinfoarray[selmaster]);
    newmaster->ShowModal();
    GetMasterList();
}
void DlgMasterList::OnRemove(wxCommandEvent&event)
{
    if (selmaster==-1)return;

        wxMessageDialog dialog(this, wxT("All the hosts under this master ID will be deleted. Do you persist?"), wxT("MING"), wxYES_DEFAULT | wxYES_NO | wxICON_QUESTION);
        if (dialog.ShowModal() == wxID_NO)return;

        MachineInfoArray machineinfoarray;
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
        MachineFile::LoadFile(fileName,machineinfoarray);
    for(unsigned int i=0;i<machineinfoarray.GetCount();i++){
        if(m_masterinfoarray[selmaster].masterid.Cmp(machineinfoarray[i].masterid)==0)
            machineinfoarray.RemoveAt(i);
    }
           MachineFile::SaveFile(fileName,machineinfoarray);

    m_masterinfoarray.RemoveAt(selmaster);
        fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("master.conf");
        MasterFile::SaveFile(fileName,m_masterinfoarray);
    GetMasterList();

}

void DlgMasterList::SetBtnStatus(int status)
{
switch(status){
case 0:
        m_btnnew->Enable(true);
        m_btnedit->Enable(false);
        m_btnremove->Enable(false);
        break;
case 1:
        m_btnedit->Enable(true);
        m_btnremove->Enable(true);
        break;
}
}

void DlgMasterList::OnMasterSelect(wxCommandEvent& event)
{
//    int oldmaster=selmaster;
    selmaster=m_listmasterlist->GetSelection();
    if((unsigned int)selmaster<m_masterinfoarray.GetCount()){
    SetBtnStatus(1);
    }else{
    SetBtnStatus(0);
    selmaster=-1;
    }

}
void DlgMasterList::DoOk(wxCommandEvent& event)
{

    this->Close();
}

void DlgMasterList::DoCancel(wxCommandEvent&event)
{
    this->Close();
}

void DlgMasterList::OnClose(wxCloseEvent& event)
{
    if(IsModal())
        {
                EndModal(wxID_OK);
        }
        else
        {
                SetReturnCode(wxID_OK);
                this->Show(false);
        }
}

DlgMasterList::~DlgMasterList()
{
m_masterinfoarray.Clear();
}

MasterInfo::MasterInfo()
{
    masterid=wxEmptyString;
    masterkey=wxEmptyString;
}

WX_DEFINE_OBJARRAY(MasterInfoArray);

void MasterFile::LoadFile(const wxString& strLoadedFileName, MasterInfoArray& masterinfoarray)
{
    wxTextFile masterfile(strLoadedFileName);
    if(!masterfile.Exists()){
#ifdef __DEBUG__
        fprintf(stderr,"master file no exist!");
#endif
        return;}
    masterfile.Open();
    if(masterfile.GetLineCount()==0){
#ifdef __DEBUG__
        fprintf(stderr,"master file is empty!");
#endif
        return;}
    masterinfoarray.Clear();
        wxString strLine;
    strLine=masterfile.GetFirstLine();
         while(FILE_BEGIN_MASTERINFOS.Cmp(strLine) != 0)strLine=masterfile.GetNextLine();
        while( FILE_END_MASTERINFOS.Cmp(strLine) != 0 && !masterfile.Eof())
        {//IRS BEGIN
                MasterInfo minfo = MasterInfo();
                int i=0;
                while(FILE_BEGIN_MASTER.Cmp(strLine) != 0)strLine=masterfile.GetNextLine();
                while( (FILE_END_MASTER.Cmp(strLine) != 0) && (!masterfile.Eof()))
                {
        strLine=masterfile.GetNextLine();
        switch(i){
                case 0:
                minfo.masterid=strLine;
                //wxMessageBox(minfo.masterid);
                break;
                case 1:
                minfo.masterkey=strLine;
                break;
                };
                i++;
        }//IR END
                masterinfoarray.Add(minfo);
                strLine=masterfile.GetNextLine();
    }
    masterfile.Close();
}

void MasterFile::SaveFile(const wxString fileName,MasterInfoArray masterinfoarray) {
    unsigned int i;

    unsigned int totalhosts=masterinfoarray.GetCount();

    if (totalhosts<=0){
#ifdef __DEBUG__
    fprintf(stderr,"No master infomation to save!");
#endif
    if(wxFileExists(fileName))wxRemoveFile(fileName);
    return;
    }

    wxTextFile masterfile(fileName);
    if(masterfile.Exists()){
        masterfile.Open();
        masterfile.Clear();
    }else
        masterfile.Create();
    masterfile.AddLine(FILE_BEGIN_MASTERINFOS,wxTextFileType_Unix);

    for(i = 0; i < totalhosts; i++) {
        MasterInfo minfo=masterinfoarray[i];
                masterfile.AddLine(FILE_BEGIN_MASTER,wxTextFileType_Unix);
                masterfile.AddLine(minfo.masterid,wxTextFileType_Unix);
                masterfile.AddLine(minfo.masterkey,wxTextFileType_Unix);
                masterfile.AddLine(FILE_END_MASTER,wxTextFileType_Unix);
    }
        masterfile.AddLine(FILE_END_MASTERINFOS,wxTextFileType_Unix);
        masterfile.Write(wxTextFileType_Unix);
        masterfile.Close();
}
