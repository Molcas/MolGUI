/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "qchemlogreading.h"
#define FAIL   -1
#define SUCCEED 0


/*******************************************************************
 * attention:
 * in the testing, we found that the function of feof can not
 * reach the exact tail of file. while reading the end of file,
 * actually it still return with status of 0; while another reading
 * of lines will make the feof function < 0. so we change the
 * function of ReadingData to make the fgets to read until NULL
 * meets.
********************************************************************/


/********************************************************************
 * used to fetch the position where we have the key words
*********************************************************************/
static int FindKey (FILE * input, const char * key);



/********************************************************************
 * get atom numbers in the log file. if nothing captured, return FAIL
 * however, if in batch mode; then it's only check the first part
 * of data and neglecting the other parts
*********************************************************************/
static int GetNumbers (FILE * input);


/********************************************************************
 * read in the atom data
 * the principle is: we do not care what the method it used in this
 * log file. we just read in the data enclosed by the key words.
 * there at least have one of them. we just read in each one,
 * and pick the last one of them
 * if encounter any failure, then return FAIL; else return SUCCEED
*********************************************************************/
static int ReadingData (FILE * input, int number, XyzData * data);


/********************************************************************
 * writing the result into the xyz file
*********************************************************************/
static void WritingXYZFile (XyzData * data, FILE * resultFile, int number);



static int FindKey (FILE * input, const char * key) {

  char data[MAXCHARINLINE];

  while (! feof (input)) {

    if (fgets(data, MAXCHARINLINE, input) == NULL ) {
      return FAIL;
    } else {

      /* get the key */
      if (strstr (data, key) == NULL) {
        continue;
      } else {
        return SUCCEED;
      }

    }
  }

  return FAIL;

}



static int GetNumbers (FILE * input) {

  int number;                        /* the value returned */
  char data[MAXCHARINLINE];


  number = 0;
  rewind(input);

  /* search the key of xyzKey */
  if (FindKey (input, "Standard Nuclear Orientation") == FAIL) {
      return FAIL;
  }

  /* get the key of "---" */
  if (FindKey (input, "---") == FAIL) {
    return FAIL;
  }

  /* step into reading the block data */
  while (! feof (input)) {

    if (fgets(data, MAXCHARINLINE, input) == NULL ) {
      return FAIL;
    } else {

      if (strstr (data, "---") != NULL) {
        break;
      } else {
        number++;
      }

    }

  }

  return number;


}


static int ReadingData (FILE * input, int number, XyzData * data) {

  char lineData[MAXCHARINLINE];
  int i, j;
  int batchJob;


  batchJob = 0;
  rewind(input);

  while (! feof (input)) {

    if (fgets(lineData, MAXCHARINLINE, input) == NULL ) {
      break;
    }

    /* here we only read the first job data */
    if (strstr (lineData, "Welcome to Q-Chem") != NULL) {
      batchJob++;
    }
    if (batchJob > 1) {
      break;
    }

    /* search the key area */
    if (strstr (lineData, "Standard Nuclear Orientation") == NULL) {
      continue;
    } else {

      /* there are two lines are no use, so read them and abadon the data */
      if (fgets(lineData, MAXCHARINLINE, input) == NULL ) {
        return FAIL;
      }
      if (fgets(lineData, MAXCHARINLINE, input) == NULL ) {
        return FAIL;
      }

      for (i=0; i<number; i++) {
        if (fgets(lineData, MAXCHARINLINE, input) == NULL ) {
          return FAIL;
        } else {
          sscanf (lineData, "%d%s%lf%lf%lf", &j, data[i].symbol, &data[i].x,
                  &data[i].y, &data[i].z);
        }
      }

    }

  }

  return SUCCEED;

}



static void WritingXYZFile (XyzData * data, FILE * result, int number) {

  int i;

  /* writing the number out */
  fprintf (result, "%d\n", number);

  /* writing the comment line */
  fprintf (result, "%s\n", "#generated by the simugui program");


  /* writing the data */
  for (i=0; i< number; i++) {
    fprintf (result, "%s%12.6f%12.6f%12.6f\n", data[i].symbol, data[i].x,
             data[i].y, data[i].z);
  }

}




LogReadingInfo QchemLogReading (char * log, char * xyzFile) {


  FILE * input;
  FILE * out;
  int number;
  XyzData * data;

  /* open the input file and output file, testing them */
  if ((input = fopen(log, "r")) == NULL) {
    return CAN_NOT_OPEN_LOG_FILE;
  }


  /* get the number of atoms */
  number = GetNumbers (input);
  if (number == FAIL) {
    return ATOM_NUMBER_READING_ERROR;
  }


  /* distribute the memory */
  if ((data = (XyzData *) malloc(sizeof(XyzData)*(number))) == NULL) {
    return MEMORY_FAILED;
  }


  /* read in data */
  if (ReadingData (input, number, data) == FAIL) {
    fclose (input);
    return XYZ_DATA_READING_ERROR;
  }
  fclose (input);


  /* writing the result into the xyz file */
  if ((out = fopen(xyzFile, "w")) == NULL) {
    return CAN_NOT_CREATE_XYZ_FILE;
  }else {
    WritingXYZFile (data, out, number);
        fclose(out);
  }


  if (data != NULL) {
    free(data);
  }

  return SUCCESS;

}
