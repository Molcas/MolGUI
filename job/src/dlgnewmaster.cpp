/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "dlgnewmaster.h"
#include "envutil.h"
#include "stringutil.h"


///////////////////////////////////////////////////////////////////////////

DlgNewMaster::DlgNewMaster( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{

//        this->SetSizeHints( wxDefaultSize, wxDefaultSize );

        wxBoxSizer* bSizer2;
    wxGridSizer* gSizer2;

        bSizer2 = new wxBoxSizer( wxVERTICAL );

        gSizer2 = new wxGridSizer( 3, 2, 0, 0 );

        m_lbmasterid = new wxStaticText( this, wxID_ANY, wxT("*Master ID:"), wxDefaultPosition, wxDefaultSize, 0 );
        gSizer2->Add( m_lbmasterid, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

        m_txtmasterid = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxPoint( -1,-1 ), wxSize( -1,-1 ), 0 );
        m_txtmasterid->SetMinSize( wxSize( 130,-1 ) );
        m_txtmasterid->SetToolTip(wxT("Set an ID for management of hosts safely."));

        gSizer2->Add( m_txtmasterid, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

        m_lbmasterkey1 = new wxStaticText( this, wxID_ANY, wxT("*Master Key:"), wxDefaultPosition, wxDefaultSize, 0 );
        gSizer2->Add( m_lbmasterkey1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

        m_txtmasterkey1= new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
        m_txtmasterkey1->SetMinSize( wxSize( 130,-1 ) );
    m_txtmasterkey1->SetToolTip(wxT("Managed by master ID, to encrypt password of hosts for safety."));
        gSizer2->Add( m_txtmasterkey1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

        m_lbmasterkey2 = new wxStaticText( this, wxID_ANY, wxT("*Retype Master Key:"), wxDefaultPosition, wxDefaultSize, 0 );
        gSizer2->Add( m_lbmasterkey2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

        m_txtmasterkey2= new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
        m_txtmasterkey2->SetMinSize( wxSize( 130,-1 ) );
    m_txtmasterkey2->SetToolTip(wxT("Managed by master ID, to encrypt password of hosts for safety."));
        gSizer2->Add( m_txtmasterkey2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

        bSizer2->Add( gSizer2, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        wxBoxSizer* bSizer3;
        bSizer3 = new wxBoxSizer( wxHORIZONTAL );
        m_btnOk = new wxButton( this, wxID_OK, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer3->Add( m_btnOk, 0, wxALL, 5 );

        m_btnCancel = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
        bSizer3->Add( m_btnCancel, 0, wxALL, 5 );

        bSizer2->Add( bSizer3, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

        this->SetSizer( bSizer2 );
//        this->SetSize(wxSize(360,260));
    bSizer2->SetSizeHints(this);
  //  bSizer2->Show(1,FALSE);
//        bSizer2->Layout();
        m_info=NULL;
        m_txtmasterid->SetFocus();

}

BEGIN_EVENT_TABLE(DlgNewMaster, wxDialog)
        EVT_BUTTON(wxID_OK, DlgNewMaster::DoOk)
        EVT_BUTTON(wxID_CANCEL,DlgNewMaster::DoCancel)
    EVT_CLOSE(DlgNewMaster::OnClose)
END_EVENT_TABLE()

void DlgNewMaster::DoOk(wxCommandEvent& event)
{
    if((m_txtmasterid->GetValue()).IsNull()||(m_txtmasterid->GetValue().Trim()).IsEmpty()){
        wxMessageBox(wxT("please input the Master ID!"));
        m_txtmasterid->SetFocus();
        return; }
    if((m_txtmasterkey1->GetValue()).IsNull()||(m_txtmasterkey1->GetValue().Trim()).IsEmpty()){
        wxMessageBox(wxT("please input Master Key!"));
        m_txtmasterkey1->SetFocus();
        return; }

    if((m_txtmasterkey2->GetValue()).IsNull()||(m_txtmasterkey2->GetValue().Trim()).IsEmpty()){
        wxMessageBox(wxT("please retype the masterkey!"));
        m_txtmasterkey2->SetFocus();
        return; }

    if((m_txtmasterkey1->GetValue()).Cmp(m_txtmasterkey2->GetValue())!=0){
        wxMessageBox(wxT("MasterKeys are different!"));
        m_txtmasterkey1->SetFocus();
        return; }

    if(!SaveInfo())return;
        if(IsModal())
        {
                EndModal(wxID_OK);
        }
        else
        {
                SetReturnCode(wxID_OK);
                this->Show(false);
        }
}

void DlgNewMaster::OnClose(wxCloseEvent &event)
{
        if(IsModal())
        {
                EndModal(wxID_CANCEL);
        }
        else
        {
                SetReturnCode(wxID_CANCEL);
                this->Show(false);
        }
}


bool DlgNewMaster::SaveInfo()
{
    bool m_newmaster=true;
    int i;
    MasterInfoArray masterinfoarray;
    MasterInfo minfo=MasterInfo();
    minfo.masterid= m_txtmasterid->GetValue();
    minfo.masterkey=m_txtmasterkey1->GetValue();
    EnvUtil::SetMasterId(minfo.masterid);
    EnvUtil::SetMasterKey(minfo.masterkey);
    minfo.masterkey=EnvUtil::GetMasterKeyMD5();

    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("master.conf");
        MasterFile::LoadFile(fileName,masterinfoarray);
        int totalhost=masterinfoarray.GetCount();
        if(m_info!=NULL){
        m_newmaster=false;
    }else{
        i=0;
        while(i<totalhost&&minfo.masterid.Cmp(masterinfoarray[i].masterid)!=0)i++;
        if(i<totalhost){
            wxMessageBox(wxT("This master ID already exists!"));
            m_txtmasterid->SetFocus();
            return false;
        }
    }



    if(m_newmaster){
    masterinfoarray.Add(minfo);
    }else{
    i=0;
    while(minfo.masterid.Cmp(masterinfoarray[i].masterid)!=0)i++;
    if(i>=totalhost){
        wxMessageBox(wxT("Edit master info error!"));
        return false;
    }else{
        masterinfoarray[i].masterid=minfo.masterid;
        masterinfoarray[i].masterkey=minfo.masterkey;
        }
    }
    MasterFile::SaveFile(fileName,masterinfoarray);
    return true;
}


void DlgNewMaster::DoCancel(wxCommandEvent& event)
{
this->Close();
}

DlgNewMaster::~DlgNewMaster()
{
}

void DlgNewMaster::SetInfo(MasterInfo minfo)
{
   m_txtmasterid->SetValue(minfo.masterid);
   m_txtmasterkey1->SetFocus();
//   m_txtmasterkey1->SetValue(minfo.masterkey);
 //  m_txtmasterkey2->SetValue(minfo.masterkey);
   m_info=&minfo;
};
