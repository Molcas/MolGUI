/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>
#include <wx/filename.h>

#include "gmfile.h"

#include "tools.h"
#include "stringutil.h"
#include "fileutil.h"

#include "commons.h"
#include "molfile.h"

const wxString FILE_BEGIN_RESULTINFO=wxT("$BEGIN RESULTINFO");
const wxString FILE_END_RESULTINFO=wxT("$END RESULTINFO");
const wxString FILE_FILE_FLAG=wxT("FILE");

bool GMFile::IsGMFile(const wxString& fileName) {
    return true;
}

void GMFile::LoadFile(const wxString &strLoadedFileName, CtrlInst *pCtrlInst, SimuJob* pSimuJob, SimuFiles* pSimuFiles)
{
        int i;
    wxString strLine, value;
    ElemInfo* eleInfoOption = NULL;
        if(pCtrlInst==NULL)
                return;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
        AtomBondArray& appendAtomBondArray=pCtrlInst->GetRenderingData()->GetAtomBondArray();
        int* atomLabels=pCtrlInst->GetRenderingData()->GetAtomLabels();
        StringHash& jobControl=pSimuJob->GetJobControl();
        StringHash& jobSurface=pSimuJob->GetJobSurfaceSettings();
        ConstrainData& constrainData=pCtrlInst->GetRenderingData()->GetConstrainData();
        ChemLigandArray& ligandArray=pCtrlInst->GetRenderingData()->GetLigandArray();
        wxArrayString tokens;

        int atomCountBase = appendAtomBondArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;
        strLine=tis.ReadLine();
        if(FILE_BEGIN_TITLE.Cmp(strLine)==0)
        {
                value=wxEmptyString;
                while(FILE_END_TITLE.Cmp(strLine=tis.ReadLine())!=0)
                {
                        if(value.IsEmpty())
                                value=strLine.Trim().Trim(false);
                        else
                                value+=wxT("\n")+strLine.Trim().Trim(false);
                }
                pSimuJob->SetJobTitle(value);
                strLine=tis.ReadLine();
        }
        if(FILE_BEGIN_JOB_CONTROL.Cmp(strLine)==0)
        {
                //load job gen setting;
                while(FILE_END_JOB_CONTROL.Cmp(strLine=tis.ReadLine())!=0)
                {
            wxStringTokenizer tzk(strLine,wxT(" \t="));
                        value=tzk.GetNextToken();
                        //wxMessageBox(value);
                        jobControl[value]=tzk.GetNextToken();
                }
                strLine = tis.ReadLine();
        }
        if(FILE_BEGIN_COOR.Cmp(strLine) == 0) {
        //load atom info
        long atomId;
        double x, y, z;
        while( FILE_END_COOR.Cmp(strLine = tis.ReadLine()) != 0) {
            wxStringTokenizer tzk(strLine);
            AtomBond atomBond = AtomBond();
            tzk.GetNextToken().ToLong(&atomId);
            tzk.GetNextToken().ToDouble(&x);
            tzk.GetNextToken().ToDouble(&y);
            tzk.GetNextToken().ToDouble(&z);
            atomBond.atom.elemId = (int)atomId;
            atomBond.atom.pos.x = x;
            atomBond.atom.pos.y = y;
            atomBond.atom.pos.z = z;
            appendAtomBondArray.Add(atomBond);
        }
                strLine = tis.ReadLine();
    }

    if(FILE_BEGIN_BOND.Cmp(strLine) == 0) {
        //load bond info
        int atom1, atom2;
        short bondType;
        long atom1Long, atom2Long, bondTypeLong;
        while(FILE_END_BOND.Cmp(strLine = tis.ReadLine()) != 0) {
            wxStringTokenizer tzk(strLine);
            tzk.GetNextToken().ToLong(&atom1Long);
            tzk.GetNextToken().ToLong(&atom2Long);
            tzk.GetNextToken().ToLong(&bondTypeLong);
            atom1 = (int)atom1Long;
            atom2 = (int)atom2Long;
            bondType = (short)bondTypeLong;

            atom1--;
            atom2--;
            atom1 += atomCountBase;
            atom2 += atomCountBase;

            appendAtomBondArray[atom1].bonds[appendAtomBondArray[atom1].bondCount].atomIndex = atom2;
            appendAtomBondArray[atom1].bonds[appendAtomBondArray[atom1].bondCount].bondType = bondType;

            appendAtomBondArray[atom2].bonds[appendAtomBondArray[atom2].bondCount].atomIndex = atom1;
            appendAtomBondArray[atom2].bonds[appendAtomBondArray[atom2].bondCount].bondType = bondType;

            appendAtomBondArray[atom1].bondCount++;
            appendAtomBondArray[atom2].bondCount++;
        }
                strLine = tis.ReadLine();
    }

    if(FILE_BEGIN_TYPE.Cmp(strLine) == 0) {
        int type;
        long typeLong;
        i = atomCountBase;
        while(FILE_END_TYPE.Cmp(strLine = tis.ReadLine()) != 0) {
            strLine.ToLong(&typeLong);
            type = (int)typeLong;
            if( type == DEFAULT_HYDROGEN_ID) {
                appendAtomBondArray[i].atom.elemId = type;
            }
            eleInfoOption = GetElemInfo((ElemId)appendAtomBondArray[i].atom.elemId);
            appendAtomBondArray[i].atom.SetDispRadius(eleInfoOption->dispRadius);
                        appendAtomBondArray[i].atom.label = ++atomLabels[ELEM_PREFIX + appendAtomBondArray[i].atom.elemId];
                        i++;
        }
                strLine = tis.ReadLine();
    }
        ChemLigand ligand;
    if(FILE_BEGIN_LIGAND.Cmp(strLine) == 0) {
        long indexLong;
                int atomIndex;
        while(FILE_END_LIGAND.Cmp(strLine = tis.ReadLine()) != 0) {
                        tokens=wxStringTokenize(strLine,wxT(" \t\r\n"),wxTOKEN_STRTOK);
                        tokens[0].ToLong(&indexLong);
                        atomIndex=(int)indexLong;
                        atomIndex--;
                        atomIndex+=atomCountBase;
                        ligand.dummyAtomId=appendAtomBondArray[atomIndex].atom.uniqueId;
                        for(i=1 ; i <(int) tokens.GetCount() ; i++)
                        {
                                tokens[i].ToLong(&indexLong);
                                atomIndex=(int)indexLong;
                                atomIndex--;
                                atomIndex+=atomCountBase;
                                ligand.linkedAtomIds.Add(appendAtomBondArray[atomIndex].atom.uniqueId);
                        }
                        ligandArray.Add(ligand);
        }
                strLine = tis.ReadLine();
    }

        ConsElem ce;
        long indexLong;
        double valueDouble;
        wxString consFlags[3]={        FILE_CONSTRAIN_LENGTH_FLAG,
                                                                                        FILE_CONSTRAIN_ANGLE_FLAG,
                                                                                        FILE_CONSTRAIN_DIHEDRAL_FLAG};
        ConsType consTypes[3]={CONS_BOND,CONS_ANGLE,CONS_DIHEDRAL};
        ConsType consType=CONS_NULL;
        if(FILE_BEGIN_CONSTRAIN.Cmp(strLine)==0)
        {
                while(FILE_END_CONSTRAIN.Cmp(strLine=tis.ReadLine())!=0)
                {
                        //wxMessageBox(strLine);
                        tokens=wxStringTokenize(strLine,wxT(" \t\r\n"),wxTOKEN_STRTOK);
                        //DisplayArrayString(tokens);
                        //wxMessageBox(tokens[0]);
                        consType=CONS_NULL;
                        for(i=0; i <3 ;i ++)
                        {
                                if(consFlags[i].Cmp(tokens[0])==0)
                                {
                                        consType=consTypes[i];
                                        break;
                                }
                        }
                        if(consType==CONS_NULL)
                                continue;
                        if((int)tokens.GetCount()<consType+2)
                                continue;
                        ce=ConsElem();
                        for(i=0;i<consType;i++)
                        {
                                //wxMessageBox(tokens[i+1]);
                                tokens[i+1].ToLong(&indexLong);
                                indexLong--;
                                ce.elemInfos.Add(appendAtomBondArray[indexLong].atom.uniqueId);
                        }
                        //wxMessageBox(tokens[tokens.GetCount()-1]);
                        tokens[tokens.GetCount()-1].ToDouble(&valueDouble);
                        ce.value=valueDouble;
                        constrainData.AppendConstrainData(ce);
                }
                strLine = tis.ReadLine();
        }
        if(FILE_BEGIN_SURFACE.Cmp(strLine)==0)
        {
                while(FILE_END_SURFACE.Cmp(strLine=tis.ReadLine())!=0)
                {
            wxStringTokenizer tzk(strLine,wxT(" ="));
                        value=tzk.GetNextToken();
                        //wxMessageBox(value);
                        jobSurface[value]=tzk.GetNextToken();
                }
                strLine = tis.ReadLine();
        }
        /*wxString restLine;
        wxString path=FileUtil::GetPath(strLoadedFileName);
        if(FILE_BEGIN_RESULTINFO.Cmp(strLine)==0)
        {
        while( FILE_END_RESULTINFO.Cmp(strLine = tis.ReadLine()) != 0)
                {
                        strLine.Trim().Trim(false);
                        restLine=wxEmptyString;
                        if(strLine.StartsWith(FILE_FILE_FLAG+wxT("=\""),&restLine))
                        {
                                restLine.Replace(wxT("\""),wxEmptyString);
                                if(!restLine.IsEmpty())
                                {
                                        if(restLine.StartsWith(path))
                                                pSimuFiles->AddOutputFile(restLine,true);
                                        else
                                                pSimuFiles->AddOutputFile(FileUtil::NormalizeFileName(path+wxFileName::GetPathSeparator()+restLine),true);
                                }
                        }
                }
        }*/
}


void GMFile::SaveFile(const wxString &fileName, const wxString& molFileName, const wxString& jobFileName, SimuFiles* pSimuFiles, bool useTab)
{
        unsigned i = 0;
        if(!wxFileExists(molFileName)) return;

    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);

        wxArrayString jobData = FileUtil::LoadStringLineFile(jobFileName);
        for(i = 0; i < jobData.GetCount(); i++) {
                if(jobData[i].Cmp(FILE_BEGIN_SURFACE) == 0) {
                        break;
                }
                tos << jobData[i] << endl;
        }
        //pSimuJob->SaveJobControl(tos);
        //MolFile::SaveFile(tos, pCtrlInst, useTab);
        wxArrayString molData = FileUtil::LoadStringLineFile(molFileName);
        FileUtil::OutputStringLineFile(tos, molData);
        //pSimuJob->SaveJobSurface(tos);
        for(; i < jobData.GetCount(); i++) {
                tos << jobData[i] << endl;
        }

////        wxString dir = pCtrlInst->GetFileName();
////        dir = wxFileName(dir).GetPath();
////        SaveFileList(tos,pSimuFiles->GetOutputFileList(),dir);
}


void GMFile::SaveFileList(wxTextOutputStream &tos, StringSet  &fileList,wxString dir)
{
        tos << FILE_BEGIN_RESULTINFO << endl;
        for(StringSet::iterator it=fileList.begin(); it != fileList.end(); ++it)
        {
        wxString key = *it;
                wxString rest=wxEmptyString;
                if(key.StartsWith(dir,&rest))
                        key=rest.AfterLast(wxFileName::GetPathSeparator());
                tos << wxT("    ")
                        << FILE_FILE_FLAG
                        << wxT("=\"")
                        << key
                        << wxT("\"")
                        << endl;
        }
        tos << FILE_END_RESULTINFO << endl;
}
