/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "execprocess.h"

#include "envutil.h"
#include "fileutil.h"
#include "stringutil.h"
#include "tools.h"

#include <wx/filename.h>

JobRunningData::JobRunningData(int projectId, ProjectType type, wxString projectTitle) {
        m_isTerminated = false;
        m_projectId = projectId;
        m_type = type;
        m_projectTitle = projectTitle;
        m_strStartTime = wxEmptyString;
        m_isFinal = true;
}

int JobRunningData::GetProjectId(void) const {
        return m_projectId;
}

ProjectType JobRunningData::GetProjectType(void) const {
        return m_type;
}

wxString JobRunningData::GetProjectTitle(void) const {
        return m_projectTitle;
}

void JobRunningData::SetProjectFile(wxString projectFile) {
        m_projectFileName = projectFile;
}

wxString JobRunningData::GetProjectFile(void) const {
        return m_projectFileName;
}

void JobRunningData::SetOutputDir(wxString outputDir) {
        m_outputDir = outputDir;
}

wxString JobRunningData::GetOutputDir(void) const {
        return m_outputDir;
}

void JobRunningData::SetCommand(wxString command, wxString para) {
        m_command = command;
        m_para = para;
}

wxString JobRunningData::GetCommand(void) const {
        return m_command;
}

wxString JobRunningData::GetPara(void) const {
        return m_para;
}
void JobRunningData::SetJobName(wxString jobName)
{
    m_jobName=jobName;
}
wxString JobRunningData::GetJobName(void)const
{
        return m_jobName;
}
void JobRunningData::SetInputFileName(wxString inputfile){
        m_inputfilename=inputfile;
}

wxString JobRunningData::GetInputFileName(void) const{
        return m_inputfilename;
}
bool JobRunningData::IsTerminated(void) const {
        return m_isTerminated;
}

void JobRunningData::SetStartTimeStr(wxString startTime) {
        m_strStartTime = startTime;
}

void JobRunningData::SetStartTime(wxLongLong startTime) {
        m_startTime = startTime;
}

void JobRunningData::SetEndTime(wxLongLong endTime) {
        m_isTerminated = true;
        m_endTime = endTime;
}

wxString JobRunningData::GetStartTime(void) const {
        return m_strStartTime;
}

wxString JobRunningData::GetRunningTime(void) const {
        if(m_isTerminated) {
                return StringUtil::FormatTime(m_endTime - m_startTime);
        }
        return wxEmptyString;
}

wxLongLong JobRunningData::GetEndTime(void) const {
        return m_endTime;
}

bool JobRunningData::IsFinal(void) const {
        return m_isFinal;
}

void JobRunningData::SetFinal(bool isFinal) {
        m_isFinal = isFinal;
}

ExecPipedProcess::ExecPipedProcess(IProcessListener* listener, ProcessData *procData): wxProcess(){
        m_listener = listener;
        m_procData = procData;
        pTisOutput = NULL;
        pTosOutput = NULL;
        pFfoutOutput = NULL;
        pTisError = NULL;
        pTosError = NULL;
        pFfoutError = NULL;
        Redirect();
}

ExecPipedProcess::~ExecPipedProcess() {
        Release();
}

void ExecPipedProcess::Release(void) {
        if(pTisOutput) {
                m_procData->processId = 0;
                delete pTisOutput;
                pFfoutOutput->Close();
                delete pTosOutput;
                delete pFfoutOutput;
                delete pTisError;
                pFfoutError->Close();
                delete pTosError;
                delete pFfoutError;
        }
}

bool ExecPipedProcess::DumpOutput(bool isAll){
        bool hasInput = false;
        wxString line = wxEmptyString;
        int k = 0;
        if (IsInputAvailable()){
                if(pTisOutput == NULL) {
                        pTisOutput = new wxTextInputStream(*GetInputStream());
                        if(wxFileName::FileExists(m_procData->outputFile)) {
                                wxRemoveFile(m_procData->outputFile);
                        }
                        pFfoutOutput = new wxFileOutputStream(m_procData->outputFile);
                        pTosOutput = new wxTextOutputStream(*pFfoutOutput);
                }
                while(IsInputAvailable() && !GetInputStream()->Eof() && (isAll || k < 10)) {
                        line = wxEmptyString;
                        line << pTisOutput->ReadLine();
                        pTosOutput->WriteString(line+wxT("\n"));
                        m_listener->AppendOutput(line);
                        k++;
                        hasInput = true;
                }
/*  debug code add by bao
                if(isAll)
                {
                        wxString format=wxEmptyString;
                        format=wxT("%d %d %d");
                        wxMessageBox(wxString::Format(format,IsInputAvailable() , !GetInputStream()->Eof() , (isAll || k < 10)));
                }*/
        }
        k = 0;
        if (IsErrorAvailable()){
                if(pTisError == NULL) {
                        pTisError = new wxTextInputStream(*GetErrorStream());
                        if(wxFileName::FileExists(m_procData->errorFile)) {
                                wxRemoveFile(m_procData->errorFile);
                        }
                        pFfoutError = new wxFileOutputStream(m_procData->errorFile);
                        pTosError = new wxTextOutputStream(*pFfoutError);
                }
                while(IsErrorAvailable() && !GetErrorStream()->Eof() && (isAll || k < 5)) {
                        line = wxEmptyString;
                        line << pTisError->ReadLine();
                        pTosError->WriteString(line+wxT("\n"));
                        m_listener->AppendError(line);
                        k++;
                        hasInput = true;
                }
        }
        return hasInput;
}

void ExecPipedProcess::OnTerminate(int pid, int status){
        // show the rest of the output
        DumpOutput(true);
        m_procData->GetJobRunningData()->SetEndTime(wxGetLocalTimeMillis());
        m_listener-> OnProcessTerminated(m_procData, status);
        // we're not needed any more
        //delete this;
}


//////////////////////////////////////////////////////
ProcessData::ProcessData(JobRunningData* pJobData) {
        m_pJobData = pJobData;
        m_pipedProcess = NULL;
        outputFile = wxEmptyString;
        errorFile = wxEmptyString;
        isValid = false;
        if(m_pJobData->GetOutputDir().IsEmpty()) {
                m_pJobData->SetOutputDir(EnvUtil::GetWorkDir());
        }
}

wxString ProcessData::GetShellCmd(void) const {
        wxString procData=m_pJobData->GetCommand() + wxT(" ") + m_pJobData->GetPara();
        return procData;
}

wxString ProcessData::GetCommand(void) const{
        wxString procCmd=m_pJobData->GetCommand();
        return procCmd;
}
JobRunningData* ProcessData::GetJobRunningData(void) {
        return m_pJobData;
}

bool ProcessData::InitData(void) {
        ////wxString tempFileNameFull = FileUtil::NormalizeFileName(m_inputFile);
        ////if(!tempFileNameFull.IsEmpty()) {
        if(!m_pJobData->GetPara().IsEmpty()) {
                ////m_inputFile = tempFileNameFull;
                ////if(m_projectName.IsEmpty()) {
                ////        m_projectName = wxFileName(tempFileNameFull).GetName();
                ////}
                outputFile = m_pJobData->GetOutputDir() + platform::PathSep() + m_pJobData->GetJobName() + wxT(".output");
                errorFile = m_pJobData->GetOutputDir() + platform::PathSep() + m_pJobData->GetJobName() + wxT(".error");
                isValid = true;
        }
        return isValid;
}

bool ProcessData::RunProcess(IProcessListener *listener) {
        m_pipedProcess = new ExecPipedProcess(listener, this);
        m_pJobData->SetStartTime(wxGetLocalTimeMillis());
        m_pJobData->SetStartTimeStr(wxNow());
        processId = wxExecute(GetShellCmd(),wxEXEC_ASYNC| wxEXEC_MAKE_GROUP_LEADER, m_pipedProcess);
        // std::cout << "RunProcess: " << processId << ";" << std::endl;
        bool started = true;
        if (!processId) {
                //wxLogError(_T("Execution of '%s' failed."), cmd.c_str());
                delete m_pipedProcess;
                m_pipedProcess = NULL;
        }
        return started;
}

bool ProcessData::DumpOutput(bool isAll) {
        return m_pipedProcess->DumpOutput(isAll);
}

void ProcessData::KillProcess(void) {
        processId = 0;
        //m_pipedProcess->Release();
        //delete m_pipedProcess;
        //m_pipedProcess = NULL;
}
