/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "downloadthread.h"
#include "tools.h"
#include "xmlfile.h"
#include "envutil.h"
#include "stringutil.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>
#include <wx/filename.h>
#include <wx/arrimpl.cpp>
#include "dlglogin.h"
#include "manager.h"
#include "messageevent.h"

DownloadThreadData::DownloadThreadData(wxArrayString sshinfo, wxArrayString lfiles, wxArrayString rfiles, JobMonitorPnl *pJobMonitorPnl, SSHClient *sshclient) {
    m_sshinfo = sshinfo;
    m_lfiles = lfiles;
    m_rfiles = rfiles;
    m_downloadThread = NULL;
    threadId = -1;
    pfLog = NULL;
    m_pJobMonitorPnl = pJobMonitorPnl;
    m_sshclient = sshclient;
}

DownloadThreadData::~DownloadThreadData() {
    if(pfLog) fclose(pfLog);
}

bool DownloadThreadData::RunThread(void) {
    m_downloadThread = new DownloadThread(this, m_sshclient);
    threadId = m_downloadThread->GetId();
    if(m_downloadThread->Create() != wxTHREAD_NO_ERROR) {
        Tools::ShowError(wxT("Sorry, cannot create thread to download files!"));
        return false;
    }
    if(m_downloadThread->Run() != wxTHREAD_NO_ERROR) {
        Tools::ShowError(wxT("Sorry, cannot start thread to download files!"));
        return false;
    }
    return true;
}

bool DownloadThreadData::KillThread(void) {
    return true;
}


DownloadThread::DownloadThread(DownloadThreadData *threadData, SSHClient *sshclient) : wxThread(wxTHREAD_JOINABLE) {
    m_threadData = threadData;
    m_status = DOWNLOAD_INIT;
    m_sshclient = sshclient;
}

void *DownloadThread::Entry() {

#ifdef __DEBUG__
    fprintf(stderr, "entry!"); //wxMessageBox(wxT("Entry"));
#endif
    //char * input=NULL;
    //StringUtil::String2CharPointer(&input,m_threadData->m_sshinfo[0]);
    //fprintf(stderr, input);
    /*
    m_sshclient->SetHostname(m_threadData->m_sshinfo[0]);
    StringUtil::String2CharPointer(&input,m_threadData->m_sshinfo[1]);
    fprintf(stderr, input);
    m_sshclient->SetUsername(input);
    StringUtil::String2CharPointer(&input,m_threadData->m_sshinfo[2]);
    fprintf(stderr, input);
    sshclient->SetPassword(input);
    wxThread::Sleep(100);

    while(sshclient->ConnectToHost()!=1){
            DlgLogin *dlglogin=new DlgLogin(Manager::Get()->GetAppWindow());
            dlglogin->SetInfo(sshclient);
            dlglogin->ShowModal();
            dlglogin=NULL;
            if(!sshclient->GetRetry()){
                wxMessageBox(wxT("Connect failed......"));
                return NULL;
            }
        }
    */
    for(unsigned int i = 0; i < m_threadData->m_lfiles.GetCount(); i++) {
        wxThread::Sleep(100);
        char *lfile = NULL, *rfile = NULL;
        StringUtil::String2CharPointer(&lfile, m_threadData->m_lfiles[i]);
        StringUtil::String2CharPointer(&rfile, m_threadData->m_rfiles[i]);
#ifdef __DEBUG__
        fprintf(stderr, "%s", lfile);
        fprintf(stderr, "%s", rfile);
#endif
        wxString msg = wxT("Start Download:") + m_threadData->m_lfiles[i];
        wxMessageUpdateEvent event(wxEVT_COMMAND_MESSAGE_CHANGED, ID_MESSAGE_CHANGE );
        event.SetString(msg); // that's all
        wxPostEvent( m_threadData->m_pJobMonitorPnl, event );

        //  m_threadData->m_pJobMonitorPnl->pPnlOutput->AppendOutput(msg);
        msg = wxEmptyString;
        if(m_sshclient->Download(lfile, rfile) == 1) {
            msg = wxT("Download Finished");
        } else {
            msg = wxT("Download Failed: ") + m_threadData->m_lfiles[i];
        }
        event.SetString(msg); // that's all
        wxPostEvent( m_threadData->m_pJobMonitorPnl, event );
        wxThread::Sleep(1000);
    }
    // m_threadData->m_pJobMonitorPnl->OnSshDownFinished();
    // fprintf(stderr,"all files download finished!");
    wxString msg = wxT("All files download finished!");
    wxMessageUpdateEvent event(wxEVT_COMMAND_MESSAGE_CHANGED, ID_MESSAGE_CHANGE );
    event.SetString(msg); // that's all
    wxPostEvent( m_threadData->m_pJobMonitorPnl, event );
    wxThread::Sleep(1000);
    //    m_threadData->m_pJobMonitorPnl->pPnlOutput->AppendOutput(msg);
    return NULL;
}


void DownloadThread::OnExit() {
    /*        if(m_pCurl) {
                    curl_easy_cleanup(m_pCurl);
            }
    */
}

bool DownloadThread::KillThread(void) {
    return true;
}

//WX_DEFINE_OBJARRAY(RemoteJobInfoArray);
