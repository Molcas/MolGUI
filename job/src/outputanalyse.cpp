/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "outputanalyse.h"

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/filename.h>
#include <wx/dir.h>
#include <wx/tokenzr.h>

#include "stringutil.h"
#include "molfile.h"

const wxString SEMI_OUT_SUCCESS_FLAG=wxT("finished successfully!");
const wxString SEMI_OUT_COOR_FLAG=wxT("*** Optimized geometry ***");
const wxString SEMI_OUT_ENERGY_FLAG=wxT("FINAL HEAT OF FORMATION");
const wxString SEMI_OUT_ENERGY_FLAG2=wxT("HEAT OF FORMATION");
const wxString SEMI_OUT_RMS_GRADIENT_NORM_FLAG=wxT("GRAD.:");
const wxString SEMI_OUT_HOMO_LUMO_POS_FLAG=wxT("NO. OF FILLED LEVELS");
const wxString SEMI_OUT_HOMO_LUMO_FLAG=wxT("EIGENVALUES");
const wxString SEMI_OUT_DIPOLE_MOMENT_CALC_FLAG=wxT("NET ATOMIC CHARGES AND DIPOLE CONTRIBUTIONS");
const wxString SEMI_OUT_DIPOLE_MOMENT_FLAG=wxT("DIPOLE");
const wxString SEMI_OUT_CPU_TIME_FLAG=wxT("TOTAL CPU TIME:");
//const wxString ABIN_OUT_COOR_FLAG=wxT("Cartesian Coordinates (Angstroms)");
//const wxString ABIN_OUT_VIBRATION_FLAG=wxT("Normal Modes and Vibrational Frequencies");
//const wxString ABIN_OUT_DEVIDE_FLAG=wxT("** Archive file written to unit 12 **");
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
wxArrayString OutputAnalyse::data;
ExecType OutputAnalyse::type;

void OutputAnalyse::LoadOutputFile(const wxString& outputFileName,ExecType execType)
{
        wxString strLine;
        wxFileInputStream fis(outputFileName);
        wxTextInputStream tis(fis);
        type=execType;
        data.Clear();
        while(!fis.Eof())
        {
                strLine = tis.ReadLine();
                strLine.Trim();
                strLine.Trim(false);
                //if(!strLine.IsEmpty())
                data.Add(strLine);
        }
        //DisplayArrayString(data);
}

bool OutputAnalyse::CheckSuccess()
{
//        switch(type)
//        {
//        case EXEC_SEMI:
                return CheckSemiSuccess();
//                break;
//        case EXEC_ABIN:
//                return data.GetCount()>10;
//        default:

//                break;
//        }
//        return false;
}

void OutputAnalyse::SaveOutputMolecule(wxString fileName)
{
        AtomBondArray chemArray;
//        if(type==EXEC_SEMI)
//        {
                GetSemiOutputMolecule(chemArray);
//        }else if(type==EXEC_ABIN)
//        {
//                GetAbinOutputMolecule(chemArray);
//        }
        if(chemArray.GetCount() > 0) {
                MolFile::SaveAtomBondArray(fileName, chemArray, false);
        }
}

bool OutputAnalyse::CheckSemiSuccess()
{
        if(data.GetCount()<=0)
                return false;
        for(unsigned i=data.GetCount()-5;i< data.GetCount() ; i++)
                if(data[i].Find(SEMI_OUT_SUCCESS_FLAG)!=wxNOT_FOUND)
                        return true;
        return false;
}

void OutputAnalyse::GetSemiOutputMolecule(AtomBondArray& chemArray)
{
        int i,j,line=data.GetCount();
        wxString delim=wxT(" \t\n\r");
        wxArrayString temp;
    ElemInfo* eleInfoOption = NULL;
        int elemId;
        double dvalue;
        for(i=data.GetCount()-1;i>=0;i--)
        {
                if(data[i].Find(SEMI_OUT_COOR_FLAG)!=wxNOT_FOUND)
                {
                        line=i+4;
                        break;
                }
        }
        if(line==(int)data.GetCount())
    {
        return;
    }
        for(i=0,j=-1;j<(int)chemArray.GetCount();i++)
        {
                if(data[line+i].IsEmpty())
                        break;
                temp=wxStringTokenize(data[line+i],delim,wxTOKEN_STRTOK);
                do
                {
                        j++;
                        if(j==(int)chemArray.GetCount())
                        {
                                //DisplayArrayString(temp);
                                break;
                        }
                        if(chemArray[j].atom.elemId == DEFAULT_HYDROGEN_ID) {
                                elemId = (int)ELEM_H;
                        }else{
                                elemId = chemArray[j].atom.elemId ;
                        }
                        eleInfoOption = GetElemInfo((ElemId)elemId);
                }while(eleInfoOption->name.Cmp(temp[1])!=0 );
                if(j==(int)chemArray.GetCount())
                        break;
                temp[2].ToDouble(&dvalue);
                chemArray[j].atom.pos.x=dvalue;
                temp[3].ToDouble(&dvalue);
                chemArray[j].atom.pos.y=dvalue;
                temp[4].ToDouble(&dvalue);
                chemArray[j].atom.pos.z=dvalue;
        }
}

wxString OutputAnalyse::GetSemiEnergy(const wxArrayString& semiData)
{
        int i,line=semiData.GetCount();
        wxString delim=wxT(" =\t\n\r");
        wxArrayString temp;
        for(i=(int)semiData.GetCount()-1;i>=0;i--)
        {
                if(semiData[i].Find(SEMI_OUT_ENERGY_FLAG)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line!=(int)semiData.GetCount())
        {
                temp=wxStringTokenize(semiData[line],delim,wxTOKEN_STRTOK );
                //DisplayArrayString(temp);
                //wxMessageBox(semiData[line]+wxT("\n")+temp[4]);
                return temp[4];
        }
        else
        {
                for(i=(int)semiData.GetCount()-1;i>=0;i--)
                {
                        if(semiData[i].Find(SEMI_OUT_ENERGY_FLAG2)!=wxNOT_FOUND)
                        {
                                line=i;
                                break;
                        }
                }
                if(line!=(int)semiData.GetCount())
                {
                        temp=wxStringTokenize(semiData[line],delim,wxTOKEN_STRTOK );
                        //DisplayArrayString(temp);
                        //wxMessageBox(semiData[line]+wxT("\n")+temp[4]);
                        return temp[3];
                }
                return wxEmptyString;
        }
}

wxString OutputAnalyse::GetSemiRMSGradientNorm(const wxArrayString& semiData)
{
        int i,line=semiData.GetCount();
        wxString delim=wxT(" \t\n\r");
        //wxArrayString temp;
        for(i=(int)semiData.GetCount()-1;i>=0;i--)
        {
                if(semiData[i].Find(SEMI_OUT_RMS_GRADIENT_NORM_FLAG)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line!=(int)semiData.GetCount())
        {
                //temp=wxStringTokenize(semiData[line],delim,wxTOKEN_STRTOK);
                double dvalue;
                wxString rest;
                rest=semiData[i].Right(semiData[i].Len()-semiData[i].Find(SEMI_OUT_RMS_GRADIENT_NORM_FLAG)-SEMI_OUT_RMS_GRADIENT_NORM_FLAG.Len());
                rest.ToDouble(&dvalue);
                //DisplayArrayString(temp);
                ///wxMessageBox(semiData[line]+wxT("\n")+temp[4]);
                return wxString::Format(wxT("%.4f"),dvalue);
        }
        else
        {
                return wxEmptyString;
        }
}

wxString OutputAnalyse::GetSemiHOMO(const wxArrayString& semiData)
{
        int i,line=semiData.GetCount(),offset=2;
        wxString delim=wxT(" =\t\n\r");
        wxArrayString temp,values;
        for(i=(int)semiData.GetCount()-1;i>=0;i--)
        {
                if(semiData[i].Find(SEMI_OUT_HOMO_LUMO_POS_FLAG)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line!=(int)semiData.GetCount())
        {
                temp=wxStringTokenize(semiData[line],delim,wxTOKEN_STRTOK);
                long lvalue;
                temp[temp.GetCount()-1].ToLong(&lvalue);
                //wxMessageBox(wxString::Format(wxT("%d"),lvalue));
                for(i=line;i< (int)semiData.GetCount();i++)
                {
                        if(semiData[i].Find(SEMI_OUT_HOMO_LUMO_FLAG)!=wxNOT_FOUND)
                        {
                                line=i;
                                break;
                        }

                }
                for(i=line+offset;i< (int)semiData.GetCount();i++)
                {
                        if(!StringUtil::IsDigital(semiData[i]))
                                break;
                        temp.Clear();
                        temp=wxStringTokenize(semiData[i],delim,wxTOKEN_STRTOK);
                        //DisplayArrayString(temp);
                        for(unsigned j=0;j<temp.GetCount();j++)
                                values.Add(temp[j]);
                }
                //DisplayArrayString(values);
                //wxMessageBox(values[lvalue-1]);
                return values[lvalue-1];
        }
        else
        {
                return wxEmptyString;
        }
}

wxString OutputAnalyse::GetSemiLUMO(const wxArrayString& semiData)
{
        int i,line=semiData.GetCount(),offset=2;
        wxString delim=wxT(" =\t\n\r");
        wxArrayString temp,values;
        for(i=(int)semiData.GetCount()-1;i>=0;i--)
        {
                if(semiData[i].Find(SEMI_OUT_HOMO_LUMO_POS_FLAG)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line!=(int)semiData.GetCount())
        {
                temp=wxStringTokenize(semiData[line],delim,wxTOKEN_STRTOK);
                long lvalue;
                temp[temp.GetCount()-1].ToLong(&lvalue);
                //wxMessageBox(wxString::Format(wxT("%d"),lvalue));
                for(i=line;i< (int)semiData.GetCount();i++)
                {
                        if(semiData[i].Find(SEMI_OUT_HOMO_LUMO_FLAG)!=wxNOT_FOUND)
                        {
                                line=i;
                                break;
                        }

                }
                for(i=line+offset;i< (int)semiData.GetCount();i++)
                {
                        if(!StringUtil::IsDigital(semiData[i]))
                                break;
                        temp.Clear();
                        temp=wxStringTokenize(semiData[i],delim,wxTOKEN_STRTOK);
                        //DisplayArrayString(temp);
                        for(unsigned j=0;j<temp.GetCount();j++)
                                values.Add(temp[j]);
                }
                //DisplayArrayString(values);
                //wxMessageBox(values[lvalue]);
                return values[lvalue];
        }
        else
        {
                return wxEmptyString;
        }
}

wxString OutputAnalyse::GetSemiDipoleMoment(const wxArrayString& semiData)
{
        int i,line=-1;
        wxString delim=wxT(" \t\n\r");
        wxArrayString temp;
        for(i=0;i<(int)semiData.GetCount();i++)
        {
                if(semiData[i].Find(SEMI_OUT_DIPOLE_MOMENT_CALC_FLAG)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line!=-1)
        {
                for(i=line+1;i<(int)semiData.GetCount();i++)
                {
                        if(semiData[i].Find(SEMI_OUT_DIPOLE_MOMENT_FLAG)!=wxNOT_FOUND)
                        {
                                temp=wxStringTokenize(semiData[i+3],delim,wxTOKEN_STRTOK);
                                //DisplayArrayString(temp);
                                //wxMessageBox(semiData[i]+wxT("\n")+semiData[i+3]+wxT("\n")+temp[4]);
                                return temp[4];
                        }
                }
                return wxEmptyString;
        }
        else
        {
                return wxEmptyString;
        }
}


wxString OutputAnalyse::GetSemiCPUTime(const wxArrayString& semiData)
{
        int i,line=semiData.GetCount();
        wxString delim=wxT(" \t\n\r");
        wxArrayString temp;
        for(i=(int)semiData.GetCount()-1;i>=0;i--)
        {
                if(semiData[i].Find(SEMI_OUT_CPU_TIME_FLAG)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line!=(int)semiData.GetCount())
        {
                temp=wxStringTokenize(semiData[line],delim,wxTOKEN_STRTOK);
                //DisplayArrayString(temp);
                //wxMessageBox(semiData[line]+wxT("\n")+temp[4]);
                return temp[3];
        }
        else
        {
                return wxEmptyString;
        }
}

StringHash OutputAnalyse::GetSemiSummaryData()
{
        StringHash hash;
        //unsigned i,line=data.GetCount();
        wxString delim=wxT(" =\t\n\r");
        //wxArrayString temp;
        wxString value=wxEmptyString;
        if(CheckSemiSuccess())
        {
                value=GetSemiEnergy(data);
                //wxMessageBox(value);
                if(!value.IsEmpty())
                        hash[wxT("Energy")]=value;
                ///////////////////////
                value=GetSemiRMSGradientNorm(data);
                //wxMessageBox(value);
                if(!value.IsEmpty())
                        hash[wxT("RMSGradientNorm")]=value;
                /////////////////////////////////////
                value=GetSemiHOMO(data);
                //wxMessageBox(value);
                if(!value.IsEmpty())
                        hash[wxT("HOMO")]=value;
                /////////////////////////////////
                value=GetSemiLUMO(data);
                //wxMessageBox(value);
                if(!value.IsEmpty())
                        hash[wxT("LUMO")]=value;
                /////////////////////////////////////
                value=GetSemiDipoleMoment(data);
                //wxMessageBox(value);
                if(!value.IsEmpty())
                        hash[wxT("DipoleMoment")]=value;
                value=GetSemiCPUTime(data);
                if(!value.IsEmpty())
                        hash[wxT("CPUTime")]=value;
        }
        return hash;
}


Vibration OutputAnalyse::GetSemiVibrationData()
{
        Vibration vibration;
        VibrationItem item;
        wxArrayString lines;
        int i,line=data.GetCount();
        for(i=0;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(SEMI_OUT_VIBRATION_ORIENTATION_COORDINATE_FLAG)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line==(int)data.GetCount())
                return vibration;
        lines.Clear();
        for(i=line+2;i<(int)data.GetCount();i++)
        {
                if(data[i].IsEmpty())
                {
                        ConstructSemiVibrationOrientCoors(vibration.OrientCoors,lines);
                        break;
                }
                lines.Add(data[i]);
        }
        wxIntArray ins;
        for(i=0        ;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(SEMI_OUT_VIBRATION_NORMAL_COORDINATE_FLAG)!=wxNOT_FOUND)
                {
                        break;
                }
        }
        for( ;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(SEMI_OUT_VIBRATION_NORMAL_COORDINATE_FLAG)!=wxNOT_FOUND)
                {
                        ins.Add(i);
                        continue;
                }
                if(data[i].Find(SEMI_OUT_VIBRATION_COORDINATE_FLAG)!=wxNOT_FOUND)
                {
                        ins.Add(i);
                        continue;
                }
                if(data[i].Find(SEMI_OUT_END_FLAG)!=wxNOT_FOUND)
                {
                        ins.Add(i);
                        break;
                }
        }
        lines.Clear();
        for(i=ins[0]+1;i<ins[ins.GetCount()-1];i++)
        {
                if(!data[i].IsEmpty())
                lines.Add(data[i]);
        }
        ConstructSemiCoodinateAnalysis(vibration.NormalCoors,lines);
        return vibration;
}

bool OutputAnalyse::SaveVibrationData(const wxString &fileName)
{
        int i,line=data.GetCount();
        wxString strSearch;
        strSearch=SEMI_OUT_VIBRATION_NORMAL_COORDINATE_FLAG;
        for(i=0;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(strSearch)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line==(int)data.GetCount())
                return false;
        Vibration vibInfo;
        vibInfo=GetSemiVibrationData();
        VibFile::SaveFile(fileName,vibInfo);
        return true;
}
bool OutputAnalyse::AnalyseVibrationData(SimuFiles *pSimuFiles)
{
        int i,line=data.GetCount();
        VibPropertyDisItemArray disArray;
        VibPropertyDisItem disItem;
        wxString strSearch;
//        switch(type)
//        {
//        case SEMI_OUT:
//                strSearch=SEMI_OUT_VIBRATION_NORMAL_COORDINATE_FLAG;
//                break;
//        case ABIN_OUT:
//                strSearch=ABIN_OUT_VIBRATION_FLAG;
//                break;
//        default:
                strSearch=SEMI_OUT_VIBRATION_NORMAL_COORDINATE_FLAG;
//                break;
//        }
        for(i=0;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(strSearch)!=wxNOT_FOUND)
                {
                        line=i;
                        break;
                }
        }
        if(line==(int)data.GetCount())
                return false;
        Vibration vibInfo;
//        switch(type)
//        {
//        case SEMI_OUT:
//                vibInfo=GetSemiVibrationData();
//                break;
//        case ABIN_OUT:
//                vibInfo=GetAbinVibrationData();
//                break;
//        default:
                vibInfo=GetSemiVibrationData();
//                break;
//        }

        disArray.Clear();
        for(i=0;i<(int)vibInfo.NormalCoors.GetCount();i++)
        {
                disItem=VibPropertyDisItem();
                disItem.Frequency=vibInfo.NormalCoors[i].Freq;
                disItem.Symmetry=vibInfo.NormalCoors[i].type;
                disArray.Add(disItem);
        }
        pSimuFiles->ResetVibrationData(disArray);
        return true;
    }

void OutputAnalyse::ClearData()
{
        data.Clear();
        type=EXEC_NONE;
}

/*
void OutputAnalyse::GetAbinOutputMolecule(AtomBondArray& chemArray)
{
        int i,j,line=data.GetCount();
        wxString delim=wxT(" \t\n\r");
        wxArrayString temp;
    ElemInfo* eleInfoOption = NULL;
        int elemId;
        double dvalue;
        for(i=data.GetCount()-1;i>=0;i--)
        {
                if(data[i].Find(ABIN_OUT_COOR_FLAG)!=wxNOT_FOUND)
                {
                        line=i+4;
                        break;
                }
        }
        if(line==(int)data.GetCount())
    {
        return;
    }
        //while(!StringUtil::CanConvertToNumber(data[i])|| data[line].IsEmpty())
        //        line++;
        for(i=0,j=-1;i<(int)chemArray.GetCount();i++)
        {
                if(data[line+i].IsEmpty())
                        break;
                temp=wxStringTokenize(data[line+i],delim,wxTOKEN_STRTOK);
                //if(temp.GetCount()!=5)
                //        break;
                //DisplayArrayString(temp);
                do
                {
                        j++;
                        if(chemArray[j].atom.elemId == DEFAULT_HYDROGEN_ID) {
                                elemId = (int)ELEM_H;
                        }else{
                                elemId = chemArray[j].atom.elemId ;
                        }
                        eleInfoOption = GetElemInfo((ElemId)elemId);
                }while(eleInfoOption->name.Cmp(temp[0])!=0 );
                if(j==(int)chemArray.GetCount())
                        break;
                temp[2].ToDouble(&dvalue);
                chemArray[j].atom.pos.x=dvalue;
                temp[3].ToDouble(&dvalue);
                chemArray[j].atom.pos.y=dvalue;
                temp[4].ToDouble(&dvalue);
                chemArray[j].atom.pos.z=dvalue;
        }

}


Vibration OutputAnalyse::GetAbinVibrationData()
{
        Vibration vibration;
        VibrationItem item;
        wxArrayString lines;
        wxString strSearch,strEnd;
        int i,line=data.GetCount();
        strSearch=ABIN_OUT_VIBRATION_FLAG;
        strEnd=ABIN_OUT_DEVIDE_FLAG;
        for(i=0;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(strSearch)!=wxNOT_FOUND)
                {
                        line=i+1;
                        break;
                }
        }
        if(line==(int)data.GetCount())
                return vibration;
        while(data[line].IsEmpty())
                line++;
        for(i=line;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(strEnd)!=wxNOT_FOUND){
                        break;
                }
                if(data[i-1].IsEmpty() && !StringUtil::IsDigital(data[i])){
                        break;
                }
                lines.Add(data[i]);
        }
        ConstructAbinCoodinateAnalysis(vibration.NormalCoors,lines);
        for(i=0;i<(int)data.GetCount();i++)
        {
                if(data[i].Find(ABIN_OUT_COOR_FLAG)!=wxNOT_FOUND)
                {
                        line=i+4;
                        break;
                }
        }
        lines.Clear();
        for(i=line; i< (int)data.GetCount(); i++)
        {
                if(data[i].IsEmpty())
                        break;
                lines.Add(data[i]);
        }
        ConstructAbinVibrationCoors(vibration.OrientCoors,lines);
        return vibration;
}
*/
