/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "dlgnewmachine.h"
#include "envutil.h"
#include "xmlfile.h"
#include "stringutil.h"
#include "dlgtemplate.h"
#include "sshclient.h"
#include "httppost.h"

int CTRL_ID_BTNTEMPLET = wxNewId();
int CTRL_RB_PROTOCAL = wxNewId();
int CTRL_RB_SUBMITWAY = wxNewId();
int CTRL_ID_BTNCHECK = wxNewId();

///////////////////////////////////////////////////////////////////////////

DlgNewMachine::DlgNewMachine( wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &pos, const wxSize &size, long style ) : wxDialog( parent, id, title, pos, size, style )
{

    //        this->SetSizeHints( wxDefaultSize, wxDefaultSize );


    bSizer2 = new wxBoxSizer( wxVERTICAL );

    wxBoxSizer *bSizer0 = new wxBoxSizer(wxHORIZONTAL);
    wxString m_rbprotocolChoices[] = { wxT("Open"), wxT("One Way SSH"), wxT("Two Ways SSH"), wxT("HTTP") };
    int m_rbprotocolNChoices = sizeof( m_rbprotocolChoices ) / sizeof( wxString );
    m_rbprotocol = new wxRadioBox( this, CTRL_RB_PROTOCAL, wxT("Communication Protocol"), wxDefaultPosition, wxDefaultSize, m_rbprotocolNChoices, m_rbprotocolChoices, 4, wxRA_SPECIFY_COLS );
    m_rbprotocol->SetSelection( 1 );
    //m_rbprotocol->Enable(false);
    bSizer0->Add( m_rbprotocol, 0, wxALL, 5 );
    bSizer2->Add(bSizer0);

    gSizer2 = new wxGridSizer( 4, 4, 0, 0 );

    m_lbfullname = new wxStaticText( this, wxID_ANY, wxT("*Full Host Address:"), wxDefaultPosition, wxDefaultSize, 0 );
    gSizer2->Add( m_lbfullname, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtfullname = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxPoint( -1, -1 ), wxSize( -1, -1 ), 0 );
    m_txtfullname->SetMinSize( wxSize( 130, -1 ) );
    m_txtfullname->SetToolTip(wxT("Host address, domain name or IP address."));

    gSizer2->Add( m_txtfullname, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_staticText7 = new wxStaticText( this, wxID_ANY, wxT("*Short Name:"), wxDefaultPosition, wxDefaultSize, 0 );
    gSizer2->Add( m_staticText7, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtshortname = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1, -1 ), 0 );
    m_txtshortname->SetMinSize( wxSize( 130, -1 ) );
    m_txtshortname->SetToolTip(wxT("The name you set, showing in the hosts list. Must begin with letter: a~z or A~Z!"));
    gSizer2->Add( m_txtshortname, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_staticText9 = new wxStaticText( this, wxID_ANY, wxT("Processors:"), wxDefaultPosition, wxDefaultSize, 0 );

    gSizer2->Add( m_staticText9, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtprocessors = new wxTextCtrl( this, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, 0 );
    m_txtprocessors->SetMinSize( wxSize( 130, -1 ) );
    m_txtprocessors->SetToolTip(wxT("Processors"));
    gSizer2->Add( m_txtprocessors, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("Nodes:"), wxDefaultPosition, wxDefaultSize, 0 );

    gSizer2->Add( m_staticText10, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtnodes = new wxTextCtrl( this, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, 0 );
    m_txtnodes->SetMinSize( wxSize( 130, -1 ) );
    m_txtnodes->SetToolTip(wxT("Total nodes of host"));
    gSizer2->Add( m_txtnodes, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_staticText11 = new wxStaticText( this, wxID_ANY, wxT("User Name:"), wxDefaultPosition, wxDefaultSize, 0 );

    gSizer2->Add( m_staticText11, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtusername = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    m_txtusername->SetMinSize( wxSize( 130, -1 ) );
    m_txtusername->SetToolTip(wxT("Login name for the user, keep it empty if you do not want to login automatically"));
    gSizer2->Add( m_txtusername, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_staticText12 = new wxStaticText( this, wxID_ANY, wxT("Password:"), wxDefaultPosition, wxDefaultSize, 0 );

    gSizer2->Add( m_staticText12, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtpassword1 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
    m_txtpassword1->SetMinSize( wxSize( 130, -1 ) );
    m_txtpassword1->SetToolTip(wxT("Password for the user"));

    gSizer2->Add( m_txtpassword1, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    // Empty StaticText as filler
    gSizer2->Add(new wxStaticText(this,wxID_ANY,wxT("")), 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    gSizer2->Add(new wxStaticText(this,wxID_ANY,wxT("")), 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

    m_staticText18 = new wxStaticText( this, wxID_ANY, wxT("Retype Password:"), wxDefaultPosition, wxDefaultSize, 0 );

    gSizer2->Add( m_staticText18, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtpassword2 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
    m_txtpassword2->SetMinSize( wxSize( 130, -1 ) );
    m_txtpassword2->SetToolTip(wxT("Retype password!"));

    gSizer2->Add( m_txtpassword2, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    bSizer2->Add( gSizer2, 0, wxALL, 5 );

    gSizer3 = new wxGridSizer( 3, 2, 0, 0 );

    m_staticText16 = new wxStaticText( this, wxID_ANY, wxT("*Server URL:"), wxDefaultPosition, wxDefaultSize, 0 );

    gSizer3->Add( m_staticText16, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txturladdress = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize );
    m_txturladdress->SetMinSize( wxSize( 240, -1 ) );
    gSizer3->Add( m_txturladdress, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_staticText17 = new wxStaticText( this, wxID_ANY, wxT("*User ID:"), wxDefaultPosition, wxDefaultSize, 0 );

    gSizer3->Add( m_staticText17, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtuserid = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize );
    m_txtuserid->SetMinSize( wxSize( 240, -1 ) );
    gSizer3->Add( m_txtuserid, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    bSizer2->Add( gSizer3, 0, wxALL, 5 );
    //

    wxBoxSizer *bSizer5;
    bSizer5 = new wxBoxSizer( wxHORIZONTAL );

    //m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Submit Way:"), wxDefaultPosition, wxDefaultSize, 0 );

    //m_staticText13->SetMinSize( wxSize( 120,-1 ) );

    //bSizer5->Add( m_staticText13, 0, wxALL, 5 );

    wxString m_rbsubmitwayChoices[] = { wxT("One command with variables"), wxT("My own script") };
    int m_rbsubmitwayNChoices = sizeof( m_rbsubmitwayChoices ) / sizeof( wxString );
    m_rbsubmitway = new wxRadioBox( this, CTRL_RB_SUBMITWAY, wxT("Set submit template:"), wxDefaultPosition, wxDefaultSize, m_rbsubmitwayNChoices, m_rbsubmitwayChoices, 2, wxRA_SPECIFY_COLS );
    m_rbsubmitway->SetSelection( 0);
    bSizer5->Add( m_rbsubmitway, 0, wxALL, 5 );

    m_btnScript = new wxButton( this, CTRL_ID_BTNTEMPLET, wxT("Script Template"), wxDefaultPosition, wxDefaultSize, 0 );
    m_btnScript->SetMinSize(wxSize(110, -1));
    bSizer5->Add( m_btnScript, 0, wxALL, 5 );

    bSizer2->Add( bSizer5, 0, wxALL, 5 );

    wxBoxSizer *bSizer7;
    bSizer7 = new wxBoxSizer( wxHORIZONTAL );

    m_staticText14 = new wxStaticText( this, wxID_ANY, wxT("Command:"), wxDefaultPosition, wxDefaultSize, 0 );

    m_staticText14->SetMinSize( wxSize( 120, -1 ) );

    bSizer7->Add( m_staticText14, 0, wxALL, 5 );

    m_txtremotecmd = new wxTextCtrl( this, wxID_ANY, wxT("pymolcas $inputfile.input"), wxDefaultPosition, wxDefaultSize, 0 );
    m_txtremotecmd->SetMinSize( wxSize( 400, -1 ) );
    m_txtremotecmd->SetToolTip(wxT("This command is used to submit job to this host, use variable $inputfile to replace the input file's name (without extension)"));
    bSizer7->Add( m_txtremotecmd, 0, wxALL, 5 );

    bSizer2->Add( bSizer7, 0, wxALL, 5 );

    wxBoxSizer *bSizer8;
    bSizer8 = new wxBoxSizer( wxHORIZONTAL );

    m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Remote workspace:"), wxDefaultPosition, wxDefaultSize, 0 );
    m_staticText15->SetMinSize( wxSize( 120, -1 ) );

    bSizer8->Add( m_staticText15, 0, wxALL, 5 );

    m_txtremoteworkbase = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    m_txtremoteworkbase->SetMinSize( wxSize( 400, -1 ) );

    bSizer8->Add( m_txtremoteworkbase, 0, wxALL, 5 );

    //        m_txtmachineid = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    //        m_txtmachineid->SetMinSize( wxSize( 400,-1 ) );

    //        bSizer8->Add(m_txtmachineid,0,wxALL,5);

    bSizer2->Add( bSizer8, 0, wxALL, 5 );

    wxBoxSizer *bSizer9;
    bSizer9 = new wxBoxSizer( wxHORIZONTAL );

    m_btnOk = new wxButton( this, wxID_OK, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0 );
    bSizer9->Add( m_btnOk, 0, wxALL, 5 );

    m_btnCancel = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
    bSizer9->Add( m_btnCancel, 0, wxALL, 5 );
    m_btnCheck = new wxButton( this, CTRL_ID_BTNCHECK, wxT("Check"), wxDefaultPosition, wxDefaultSize, 0 );
    bSizer9->Add( m_btnCheck, 0, wxALL, 5 );

    bSizer2->Add( bSizer9, 0, wxALL, 5 );

    this->SetSizer( bSizer2 );
    //        this->SetSize(wxSize(600,400));
    bSizer2->SetSizeHints(this);
    bSizer2->Show(2, FALSE);
    bSizer2->Layout();
    m_info = NULL;
    m_scriptfile = wxEmptyString;
    m_btnScript->Enable(false);


}

BEGIN_EVENT_TABLE(DlgNewMachine, wxDialog)
    EVT_BUTTON(wxID_OK, DlgNewMachine::DoOk)
    EVT_BUTTON(wxID_CANCEL, DlgNewMachine::DoCancel)
    EVT_BUTTON(CTRL_ID_BTNTEMPLET, DlgNewMachine::OnScriptTemplate)
    EVT_BUTTON(CTRL_ID_BTNCHECK, DlgNewMachine::OnCheck)
    EVT_RADIOBOX(CTRL_RB_PROTOCAL, DlgNewMachine::OnProtocalChange)
    EVT_RADIOBOX(CTRL_RB_SUBMITWAY, DlgNewMachine::OnSubmitWayChange)

    EVT_CLOSE(DlgNewMachine::OnClose)
END_EVENT_TABLE()

void DlgNewMachine::OnCheck(wxCommandEvent &event) {
    if(m_rbprotocol->GetSelection() != 3) {
        if(m_rbsubmitway->GetSelection() == 0 ) {
            if((m_txtfullname->GetValue()).IsNull() || (m_txtfullname->GetValue().Trim()).IsEmpty()) {
                wxMessageBox(wxT("please input the address of your host!"));
                m_txtfullname->SetFocus();
                return;
            }
            if((m_txtshortname->GetValue()).IsNull() || (m_txtfullname->GetValue().Trim()).IsEmpty()) {
                wxMessageBox(wxT("please input the nick name of your host!"));
                m_txtshortname->SetFocus();
                return;
            }

            if(!(m_txtpassword1->GetValue()).IsNull() || !(m_txtpassword1->GetValue().Trim()).IsEmpty()) {
                if(!(m_txtpassword2->GetValue()).IsNull() || !(m_txtpassword2->GetValue().Trim()).IsEmpty()) {
                    if((m_txtpassword1->GetValue()).Cmp(m_txtpassword2->GetValue()) != 0) {
                        wxMessageBox(wxT("Passwords are different!"));
                        m_txtpassword1->SetFocus();
                        return;
                    }
                }
            }

            SSHClient *sshclient = new SSHClient();
            sshclient->SetHostname(m_txtfullname->GetValue());
            sshclient->SetUsername(m_txtusername->GetValue());
            sshclient->SetPassword(EnvUtil::GetEncryptCode(m_txtpassword1->GetValue()));
            if(sshclient->ConnectToHost() != 1) {
                wxMessageBox(wxT("Connect failed......"));
            } else {
                wxMessageBox(wxT("Success Connected!"));
            }
        }
    } else {
        //wxMessageBox(wxT("http"));
        HttpPost *post = new HttpPost(m_txturladdress->GetValue(), m_txtuserid->GetValue());
        if(post->Post()) {
            wxMessageBox(wxT("Success Connected!"));
        } else {
            wxString msg = wxT("Http Error! Please check the url or server status!");
            wxMessageBox(msg);
        }
    }

}

void DlgNewMachine::Disp1()
{
    bSizer2->Show((size_t) 0, TRUE);
    bSizer2->Show(1, FALSE);
    bSizer2->Show(2, TRUE);
    bSizer2->Show(3, FALSE);
    bSizer2->Show(4, FALSE);
    bSizer2->Show(5, FALSE);
    bSizer2->Layout();

}

void DlgNewMachine::Disp2()
{
    bSizer2->Show((size_t) 0, TRUE);
    bSizer2->Show(1, TRUE);
    bSizer2->Show(2, FALSE);
    bSizer2->Show(3, TRUE);
    bSizer2->Show(4, TRUE);
    bSizer2->Show(5, TRUE);

    bSizer2->Layout();
}

void DlgNewMachine::OnProtocalChange(wxCommandEvent &event)
{
    if(m_rbprotocol->GetSelection() == 3 )
        Disp1();
    else
        Disp2();
}

void DlgNewMachine::OnSubmitWayChange(wxCommandEvent &event)
{
    if(m_rbsubmitway->GetSelection() == 1 ) {
        m_btnScript->Enable(true);
        m_txtremotecmd->Enable(false);
    } else {
        m_btnScript->Enable(false);
        m_txtremotecmd->Enable(true);
        m_txtremotecmd->SetFocus();
    }
}

void DlgNewMachine::OnScriptTemplate(wxCommandEvent &event)
{
    if(m_scriptfile.IsEmpty()) {
        int i = 0;
        m_scriptfile = wxString::Format(wxT("machine%d.tlp"), i);
        while(wxFileExists(EnvUtil::GetUserDataDir() + platform::PathSep() + m_scriptfile)) {
            i++;
            m_scriptfile = wxString::Format(wxT("machine%d.tlp"), i);
        }
    }
    wxString scriptfile = EnvUtil::GetUserDataDir() + platform::PathSep() + m_scriptfile;
    DlgTemplate *dlgtemplate = new DlgTemplate(this);
    dlgtemplate->AppendTemplate(scriptfile);
    dlgtemplate->ShowModal();
}
void DlgNewMachine::DoOk(wxCommandEvent &event)
{
    if(m_rbprotocol->GetSelection() != 3)
        if(m_rbsubmitway->GetSelection() == 0 ) {
            if((m_txtfullname->GetValue()).IsNull() || (m_txtfullname->GetValue().Trim()).IsEmpty()) {
                wxMessageBox(wxT("please input the address of your host!"));
                m_txtfullname->SetFocus();
                return;
            }
            if((m_txtshortname->GetValue()).IsNull() || (m_txtfullname->GetValue().Trim()).IsEmpty()) {
                wxMessageBox(wxT("please input the nick name of your host!"));
                m_txtshortname->SetFocus();
                return;
            }

            if(!(m_txtpassword1->GetValue()).IsNull() || !(m_txtpassword1->GetValue().Trim()).IsEmpty()) {
                if(!(m_txtpassword2->GetValue()).IsNull() || !(m_txtpassword2->GetValue().Trim()).IsEmpty()) {
                    if((m_txtpassword1->GetValue()).Cmp(m_txtpassword2->GetValue()) != 0) {
                        wxMessageBox(wxT("Passwords are different!"));
                        m_txtpassword1->SetFocus();
                        return;
                    }
                }
            }

            wxString tmpcmd = m_txtremotecmd->GetValue();
            if(tmpcmd.Find(wxT("$inputfile")) == wxNOT_FOUND) {
                wxMessageBox(wxT("please use $inputfile to replace inputfile's name (without extension)"));
                return;
            }
        }

    SaveInfo();
    if(IsModal())
    {
        EndModal(wxID_OK);
    }
    else
    {
        SetReturnCode(wxID_OK);
        this->Show(false);
    }
}

void DlgNewMachine::OnClose(wxCloseEvent &event)
{
    if(IsModal())
    {
        EndModal(wxID_CANCEL);
    }
    else
    {
        SetReturnCode(wxID_CANCEL);
        this->Show(false);
    }
}


void DlgNewMachine::SaveInfo()
{
    bool m_newmachine = true;
    MachineInfoArray machineinfoarray;
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
    MachineFile::LoadFile(fileName, machineinfoarray);
    // std::cout <<  "SaveInfo: " << fileName << std::endl;
    int hostnumber = machineinfoarray.GetCount();
    int totalhost = hostnumber;
    if(hostnumber < 0)hostnumber = 0;
    wxString strhostname = wxEmptyString;
    // std::cout <<  "SaveInfo: totalhost " << totalhost << std::endl;
    if(m_info != NULL) {
        // std::cout <<  "SaveInfo: prev m_info " << m_info->machineid << std::endl;
        strhostname = m_info->machineid;
        // std::cout <<  "SaveInfo: m_info " << strhostname << std::endl;
        m_newmachine = false;
    } else {
        // std::cout <<  "SaveInfo: else m_info " << totalhost << std::endl;
        strhostname = wxString::Format(wxT("machine%d"), hostnumber);
        while(hostnumber > 0 && strhostname.Cmp(machineinfoarray[totalhost - 1].machineid) <= 0) {
            hostnumber++;
            strhostname = wxString::Format(wxT("machine%d"), hostnumber);
        }
    }
    // std::cout <<  "SaveInfo: prev strhostname " << strhostname << std::endl;
    MachineInfo minfo = MachineInfo();
    minfo.machineid = strhostname;
    // std::cout <<  "SaveInfo: strhostname " << strhostname << std::endl;
    if(m_rbprotocol->GetSelection() == 3)
    {
        minfo.fullname = m_txturladdress->GetValue();
        minfo.shortname = m_txturladdress->GetValue();
    } else {
        minfo.fullname = m_txtfullname->GetValue();
        minfo.shortname = m_txtshortname->GetValue();
    }
    minfo.interprocessor = m_txtprocessors->GetValue();
    minfo.nodes = m_txtnodes->GetValue();
    if(m_rbprotocol->GetSelection() == 3) {
        minfo.username = m_txtuserid->GetValue();
        //if(minfo.masterkey.IsEmpty())minfo.masterkey=wxT("12345678");
        //EnvUtil::SetMasterKey(minfo.masterkey);
        minfo.password = wxT("no use");
    } else {
        minfo.username = m_txtusername->GetValue();
        //if(minfo.masterkey.IsEmpty())minfo.masterkey=wxT("12345678");
        //EnvUtil::SetMasterKey(minfo.masterkey);
        minfo.password = m_txtpassword1->GetValue();
        minfo.password = EnvUtil::GetEncryptCode(minfo.password);
    }
    minfo.masterkey = EnvUtil::GetMasterKeyMD5();
    minfo.masterid = EnvUtil::GetMasterId();
    minfo.protocal = m_rbprotocol->GetStringSelection();
    minfo.submitway = m_rbsubmitway->GetStringSelection();
    minfo.remotecmd = m_txtremotecmd->GetValue();
    if(m_scriptfile.IsEmpty()) {
        int i = 0;
        m_scriptfile = wxString::Format(wxT("machine%d.tlp"), i);
        while(wxFileExists(EnvUtil::GetUserDataDir() + platform::PathSep() + m_scriptfile)) {
            i++;
            m_scriptfile = wxString::Format(wxT("machine%d.tlp"), i);
        }
    }
    minfo.scriptfile = m_scriptfile;
    // std::cout <<  "SaveInfo: m_scriptfile " << m_scriptfile << std::endl;
    minfo.remoteworkbase = m_txtremoteworkbase->GetValue();
    // std::cout <<  "SaveInfo: minfo.remoteworkbase " << minfo.remoteworkbase << std::endl;
    if(m_newmachine) {
        // std::cout <<  "SaveInfo: prev m_newmachine " << m_scriptfile << std::endl;
        machineinfoarray.Add(minfo);
        // std::cout <<  "SaveInfo: m_newmachine " << m_scriptfile << std::endl;
    } else {
        int i = 0;
        // std::cout <<  "SaveInfo: else prev m_newmachine " << m_scriptfile << std::endl;
        // std::cout <<  "machineinfoarray[i].machineid " << machineinfoarray[i].machineids << std::endl;
        while(minfo.machineid.Cmp(machineinfoarray[i].machineid) != 0){
            i++;
            // std::cout <<  "SaveInfo: else prev m_newmachine i " << i << " minfo " << minfo.machineid.Cmp(machineinfoarray[i].machineid) << std::endl;
        }
        // std::cout <<  "SaveInfo: else m_newmachine " << i << std::endl;
        if(i >= hostnumber){
            wxMessageBox(wxT("Edit machine info error!"));
            // std::cout <<  "SaveInfo: hostnumber " << hostnumber << std::endl;
        } else {
            machineinfoarray[i].fullname = minfo.fullname;
            machineinfoarray[i].shortname = minfo.shortname;
            machineinfoarray[i].interprocessor = minfo.interprocessor;
            machineinfoarray[i].nodes = minfo.nodes;
            machineinfoarray[i].username = minfo.username;
            machineinfoarray[i].password = minfo.password;
            machineinfoarray[i].protocal = minfo.protocal;
            machineinfoarray[i].submitway = minfo.submitway;
            machineinfoarray[i].remotecmd = minfo.remotecmd;
            machineinfoarray[i].scriptfile = minfo.scriptfile;
            machineinfoarray[i].remoteworkbase = minfo.remoteworkbase;
            machineinfoarray[i].masterid = minfo.masterid;
            machineinfoarray[i].masterkey = minfo.masterkey;
            // std::cout <<  "SaveInfo: else hostnumber " << hostnumber << std::endl;
        }

    }
    // std::cout <<  "SaveInfo: final" << m_scriptfile << std::endl;
    MachineFile::SaveFile(fileName, machineinfoarray);
}


void DlgNewMachine::DoCancel(wxCommandEvent &event)
{
    this->Close();
}

DlgNewMachine::~DlgNewMachine()
{
}

void DlgNewMachine::SetInfo(MachineInfo minfo)
{
    m_rbprotocol->SetStringSelection(minfo.protocal);
    if(m_rbprotocol->GetSelection() == 3)
    {
        m_txturladdress->SetValue(minfo.fullname);
        m_txtuserid->SetValue(minfo.username);
        Disp1();
    } else {
        m_txtfullname->SetValue(minfo.fullname);
        m_txtshortname->SetValue(minfo.shortname);
        m_scriptfile = minfo.scriptfile;
        m_txtprocessors->SetValue(minfo.interprocessor);
        m_txtnodes->SetValue(minfo.nodes);
        m_txtusername->SetValue(minfo.username);
        //EnvUtil::SetMasterKey(minfo.masterkey);
        m_txtpassword1->SetValue(EnvUtil::GetDecipherCode( minfo.password));
        m_txtpassword2->SetValue(EnvUtil::GetDecipherCode( minfo.password));
        // m_rbprotocol->SetStringSelection(minfo.protocal);
        m_rbsubmitway->SetStringSelection(minfo.submitway);
        m_txtremotecmd->SetValue(minfo.remotecmd);
        m_txtremoteworkbase->SetValue(minfo.remoteworkbase);
        Disp2();
        if(m_rbsubmitway->GetSelection() == 1 ) {
            m_btnScript->Enable(true);
            m_txtremotecmd->Enable(false);
        } else {
            m_btnScript->Enable(false);
            m_txtremotecmd->Enable(true);
            m_txtremotecmd->SetFocus();
        }

    }
    //   m_txtmachineid->SetValue(minfo.scriptfile);
    m_info = &minfo;
};
