/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __DLGMASTERLIST__H
#define __DLGMASTERLIST__H
//#include "datatype.h"
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/font.h>
#include <wx/gdicmn.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/listbox.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////
const wxString FILE_BEGIN_MASTERINFOS = wxT("$BEGIN MASTERINFOS");
const wxString FILE_END_MASTERINFOS = wxT("$END MASTERINFOS");
const wxString FILE_BEGIN_MASTER = wxT("$BEGIN MASTER");
const wxString FILE_END_MASTER = wxT("$END MASTER");

class MasterInfo
{
public:
MasterInfo();
public:
wxString masterid;
wxString masterkey;
};

WX_DECLARE_OBJARRAY(MasterInfo, MasterInfoArray);

class MasterFile{
public:
static void SaveFile(const wxString fileName,MasterInfoArray masterinfoarray);
static void LoadFile(const wxString& strLoadedFileName, MasterInfoArray& masterinfoarray);
};
///////////////////////////////////////////////////////////////////////////////
/// Class DlgMasterList
///////////////////////////////////////////////////////////////////////////////
class DlgMasterList : public wxDialog
{
        private:
                DECLARE_EVENT_TABLE();
                void DoOk(wxCommandEvent& event);
                void DoCancel(wxCommandEvent&event);
            void OnClose(wxCloseEvent& event);
               void OnNew(wxCommandEvent&event);
               void OnEdit(wxCommandEvent&event);
               void OnRemove(wxCommandEvent&event);
            void OnMasterSelect(wxCommandEvent& event);
            void SetBtnStatus(int status);
            int selmaster;
                MasterInfoArray m_masterinfoarray;
        protected:
                wxListBox* m_listmasterlist;
                wxButton* m_btnnew;
                wxButton* m_btnedit;
                wxButton* m_btnremove;
                wxButton* m_btnok;
                wxButton* m_btncancel;

        public:
                DlgMasterList( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 596,400 ), long style = wxDEFAULT_DIALOG_STYLE );
                ~DlgMasterList();
            bool GetMasterList(void);
};

#endif
