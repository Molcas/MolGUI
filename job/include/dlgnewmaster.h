/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __DLGNEWMASTER__H
#define __DLGNEWMASTER__H

#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include "dlgmasterlist.h"
///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class DlgNewmaster
///////////////////////////////////////////////////////////////////////////////
class DlgNewMaster : public wxDialog
{
        private:
                DECLARE_EVENT_TABLE();
                void DoOk(wxCommandEvent& event);
                void DoCancel(wxCommandEvent&event);
            void OnClose(wxCloseEvent& event);
            bool SaveInfo(void);
            MasterInfo* m_info;

        protected:
                wxStaticText* m_lbmasterid;
                wxTextCtrl* m_txtmasterid;
                wxStaticText* m_lbmasterkey1;
                wxTextCtrl* m_txtmasterkey1;
                wxStaticText* m_lbmasterkey2;
                wxTextCtrl* m_txtmasterkey2;
                wxButton * m_btnOk;
                wxButton * m_btnCancel;
        public:
                DlgNewMaster( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );
                ~DlgNewMaster();
                void SetInfo(MasterInfo minfo);

};

#endif //__noname__
