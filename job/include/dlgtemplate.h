/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __JOB_DLGTEMPLATE_H__
#define __JOB_DLGTEMPLATE_H__

#include "wx.h"

class DlgTemplate : public wxDialog
{
    DECLARE_EVENT_TABLE()
public:
        void AppendTemplate(wxString templatefile);
        void SaveTemplate(wxString templatefile);
        void DoOk(wxCommandEvent &event);
        //begin : hurukun : 19/11/2009
        DlgTemplate(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Submit Script Template"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
//            long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER );
            long style = wxDEFAULT_DIALOG_STYLE);
        //end   : hurukun : 19/11/2009
        virtual ~DlgTemplate();
private:
        wxStaticText* lbcaption;
        wxTextCtrl* m_txtcontent;
        wxString m_templatefile;
private:
        void CreateGUIControls(void);
};

#endif
