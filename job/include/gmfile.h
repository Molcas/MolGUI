/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_SIMUCAL_GMFILE_H__
#define __BP_SIMUCAL_GMFILE_H__

#include "wx.h"
#include "ctrlinst.h"
#include "renderingdata.h"
#include "simujob.h"
#include "simufiles.h"

class GMFile {
public:
        static void SaveFile(const wxString& fileName, const wxString& molFileName, const wxString& jobFileName, SimuFiles* pSimuFiles, bool useTab);
        static void LoadFile(const wxString& strLoadedFileName, CtrlInst* pCtrlInst, SimuJob* pSimuJob, SimuFiles* pSimuFiles);
        static bool IsGMFile(const wxString& fileName);

protected:
        static void SaveFileList(wxTextOutputStream &tos,StringSet& fileList,wxString dir);
};
#endif
