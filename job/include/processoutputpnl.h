/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MONITOR_PROCESSOUTPUTPNL_H__
#define __BP_MONITOR_PROCESSOUTPUTPNL_H__

#include "wx.h"
#include "execprocess.h"

class ProcessOutputPanel : public wxPanel {
        public:
                ProcessOutputPanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = wxT("processoutputpanel"));

                void AppendOutput(wxString& msg);
                void AppendError(wxString& msg);

                void Clear(void);
                void LoadFiles(wxString& outputFile, wxString& errorFile);

        private:
                void CreateControls(void);
                void AppendMessage(wxTextCtrl* pTcLog, wxString& msg);

        private:
                wxTextCtrl* pTcOutput;
                wxTextCtrl* pTcError;
};

#endif
