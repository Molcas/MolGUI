/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_QCHEM_QCHEMFILES_H__
#define __BP_QCHEM_QCHEMFILES_H__

#include "wx.h"

#include "projectfiles.h"
#include "renderingdata.h"
#include "projectgroup.h"

class QChemFiles : public AbstractProjectFiles {
        public:
                void ShowResultItem(ProjectTreeItemData *item);
                void GetXYZ();
                bool GetSummary();

//                wxArrayString GetSurfaceDataFiles();
                bool ConvertDens();
//                wxString GetVibOutputFileName();
                wxString GetSurfaceFileName();
                bool ConvertVib();
                QChemFiles(SimuProject* simuProject, wxString resultDir);
                ~QChemFiles();
//                virtual bool HasSummary(void);
                virtual bool CreateInputFile(void);
            virtual wxString GetExecCommand(void);
            virtual wxString GetExecPara(void);
//            virtual wxString GetExecLogName(void);
                virtual wxString GetOutputFileName();
                virtual wxString GetInputFileName();
//                virtual wxString GetSummaryFileName();
            virtual bool PostJobTerminated(bool isFinal);

            virtual void BuildResultTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode);
                virtual bool HasOutput();
                //virtual bool HasEnergy_Statistics();
                virtual bool HasError_Info();
                //virtual int  HasDensity();
                virtual bool HasOptimization();
                //virtual bool HasFrequence();
                virtual bool HasGssOrb();
                virtual bool HasInput();
                virtual bool HasScfOrb();
                virtual bool HasDump();
                void SaveQuiCons(wxString QuiConsPath);                        //add ma   save cons data for qui

        private:

};
#endif
