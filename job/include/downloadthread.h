/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DOWNLOAD_THREAD_H__
#define __BP_DOWNLOAD_THREAD_H__

#include "wx.h"
#include "sshclient.h"
#include "jobmonitorpnl.h"
//#include "execprocess.h"
//#include <curl/curl.h>

typedef enum _DownloadThreadStatus {
        DOWNLOAD_INIT,
        DOWNLOAD_WAITING,
        DOWNLOAD_RECEIVING_RESULTS
}DownloadThreadStatus;


class DownloadThreadData;

class DownloadThread : public wxThread {
        public:
                DownloadThread(DownloadThreadData* threadData,SSHClient *sshclient);
                // thread execution start here
                virtual void* Entry();
                virtual void OnExit();

                bool KillThread(void);

        private:
                DownloadThreadData* m_threadData;
                DownloadThreadStatus m_status;
                SSHClient *m_sshclient;
//CURL *m_pCurl;
};


class DownloadThreadData {
        public:
                DownloadThreadData(wxArrayString sshinfo,wxArrayString lfiles,wxArrayString rfiles,JobMonitorPnl* pJobMonitorPnl,SSHClient *sshclient);
                ~DownloadThreadData();
                bool RunThread(void);
                bool KillThread(void);

        public:
                long threadId;
                FILE* pfLog;
                wxArrayString m_sshinfo,m_lfiles,m_rfiles;
        JobMonitorPnl * m_pJobMonitorPnl;

        private:
                DownloadThread* m_downloadThread;
                SSHClient *m_sshclient;

};

//WX_DECLARE_STRING_HASH_MAP(DownloadThreadData*, DownloadThreadDataHash);

#endif
