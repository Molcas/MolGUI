/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __DLGNEWMACHINE__
#define __DLGNEWMACHINE__

#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/radiobox.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include "dlgmachinelist.h"
///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class DlgNewMachine
///////////////////////////////////////////////////////////////////////////////
class DlgNewMachine : public wxDialog
{
        private:
                DECLARE_EVENT_TABLE();
                void DoOk(wxCommandEvent& event);
                void DoCancel(wxCommandEvent&event);
            void OnClose(wxCloseEvent& event);
            void SaveInfo(void);
            MachineInfo* m_info;
            wxString m_masterid;
            wxString m_masterkey;
            wxString m_scriptfile;
                 void OnScriptTemplate(wxCommandEvent& event);
                 void OnProtocalChange(wxCommandEvent& event);
                 void OnSubmitWayChange(wxCommandEvent& event);
            void OnCheck(wxCommandEvent &event);
                 void Disp1();
                 void Disp2();

        protected:
                wxStaticText* m_lbfullname;
                wxTextCtrl* m_txtmachineid;
                wxTextCtrl* m_txtfullname;
                wxStaticText* m_staticText7;
                wxTextCtrl* m_txtshortname;
                wxStaticText* m_staticText9;
                wxTextCtrl* m_txtprocessors;
                wxStaticText* m_staticText10;
                wxTextCtrl* m_txtnodes;
                wxStaticText* m_staticText11;
                wxTextCtrl* m_txtusername;
                wxStaticText* m_staticText12;
                wxTextCtrl* m_txtpassword1;
                wxTextCtrl* m_txtpassword2;
                wxRadioBox* m_rbprotocol;
        //        wxButton* m_btnTestConnect;
//                wxStaticText* m_staticText13;
                wxRadioBox* m_rbsubmitway;
                wxButton* m_btnScript;
                wxStaticText* m_staticText14;
                wxTextCtrl* m_txtremotecmd;
                wxStaticText* m_staticText15;
                wxTextCtrl* m_txtremoteworkbase;
                wxButton* m_btnOk;
                wxButton* m_btnCancel;
                wxButton* m_btnCheck;
                wxTextCtrl* m_txturladdress;
                wxStaticText* m_staticText16;
                wxTextCtrl* m_txtuserid;
                wxStaticText* m_staticText17;
                wxStaticText* m_staticText18;

                wxBoxSizer* bSizer2;
        wxGridSizer* gSizer2;
                wxGridSizer* gSizer3;
        public:
                DlgNewMachine( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );
                ~DlgNewMachine();
                void SetInfo(MachineInfo minfo);

};

#endif //__noname__
