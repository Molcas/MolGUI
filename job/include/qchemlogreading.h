/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_QCHEM_QCHEMLOGREADING_H__
#define __BP_QCHEM_QCHEMLOGREADING_H__


#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define  MAXCHARINLINE  100

typedef struct _xyzData {
  char symbol[4];
  double x;
  double y;
  double z;
} XyzData;


typedef enum _logReadingInfo {
  CAN_NOT_OPEN_LOG_FILE = 1,
  CAN_NOT_CREATE_XYZ_FILE,
  ATOM_NUMBER_READING_ERROR,
  MEMORY_FAILED,
  XYZ_DATA_READING_ERROR,
  SUCCESS
} LogReadingInfo;


/*************************************************************************
 * target : get the final xyz coord from the qchem log file
 * input: inputDir: the qchem log file
 * output:  xyzDir: the created xyz file
**************************************************************************/
LogReadingInfo QchemLogReading (char * inputDir, char * xyzDir);

#endif
