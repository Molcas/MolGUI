/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_SIMUCAL_SIMUJOBPNL_H__
#define __BP_SIMUCAL_SIMUJOBPNL_H__

#include "genericdlg.h"

#include <wx/notebook.h>
#include <wx/spinctrl.h>

#include "datatype.h"
#include "simuproject.h"

class SimuJobPnl : public GenericPnl {
public:
        SimuJobPnl(GenericDlg* parent, ProjectTreeItemData* treeItem, wxWindowID id = wxID_ANY,bool isNewjob=true);
        virtual ~SimuJobPnl();

        virtual long GetButtonStyle(void);
        virtual bool OnButton(long style);

        StringHash GetJobControl(void);
        StringHash GetSurfaceSettings(void);
        void SetJobControl(StringHash& jobControl);
        void SetSurfaceSettings(StringHash& sufaceSet);
        void SetJobTitle(wxString title);
        wxString GetJobTitle(void);

        static wxString GetDIISFlag(void) {return wxT("DIIS");}
        static wxString GetDAMPFlag(void) {return wxT("DAMP");}
        static StringHash GetDefaultJobControl(void);

protected:
        void SendMessage(long id, wxString value);
        int GetChoiceItemIndex(long id,wxString value);
        //GUI contol utils
        wxSizer* GetContainSizer(long id);
        wxString GetControlType(long id);
        void SetControlValue(long id,int value);
        void SetControlValue(long id,wxString value);
        wxString GetControlValue(long id);
        int GetControlValueInt(long id);
        bool GetCheckValue(long id);
        void ResetControl( long id);
        //Process Event
        void SendMessage(long id,int value);

private:
        DECLARE_EVENT_TABLE();
        //
        bool OnBtnClose(void);
        bool OnBtnSubmit(void);
        bool OnBtnReset(void);
        bool OnBtnSave(void);
        //GUI Control
//        void ChangChargeMultiDisplay(void);
        void ChangeJobTypeDisplay(void);
        void SetDefaults(void);
        void ResetControls(void);
        void ResetPageControls(int pageIndex);
//        void OnMultiChangeComman(long value);
        //GUI Create
        void CreateGUIControls(void);
        wxPanel* CreateSolvationPage(wxBookCtrlBase *parent);
        wxPanel* CreateSurfacePage(wxBookCtrlBase *parent);
        wxPanel* CreatePropertyPage(wxBookCtrlBase *parent);
        wxPanel* CreateGuessPage(wxBookCtrlBase *parent);
        wxPanel* CreateMethodPage(wxBookCtrlBase *parent);
        wxPanel* CreateJobTypePage(wxBookCtrlBase *parent);
        wxBoxSizer* CreateTextInput(wxWindow* parent,wxWindowID textID,wxString display,wxSize disSize=wxDefaultSize,wxSize textSize=wxDefaultSize);
        //Event handler
        void OnDFTFuncChanged(wxCommandEvent &event);
        void OnModelChanged(wxCommandEvent &event); //change the display of the name
        void OnBasisSetChanged(wxCommandEvent &event); //change the basisset name of the display
        void OnMethodSelChanged(wxCommandEvent &event);
        void OnSurfaceCheck(wxCommandEvent &event);
        void OnJobTypeSelChanged(wxCommandEvent& event);
//        void OnTotalChargeChanged(wxCommandEvent &event);
        void OnGuessSelChanged(wxCommandEvent &event);
//        void OnMultiChanged(wxCommandEvent &event);
        void OnSpinButton(wxSpinEvent& event);
        void OnTotalChargeSpinChanged(wxSpinEvent& event);
           void ChangChargeStateDisplay(void);
        void OnMethodStateChangeCommon(long value);
        void OnMethodStateChanged(wxCommandEvent &event);

private:
        ProjectTreeItemData* m_pTreeItem;
//        wxStaticText* stShow[2];
        wxNotebook* pNotebook;
//        wxStaticBoxSizer* sbSizer[2];
//        wxStaticText* stGuess[2];
        wxBoxSizer* bsSurfaceResolution;
        wxTextCtrl* tcTitle;
        wxBoxSizer* bsSurface;
        wxStaticBoxSizer* sbsJobTypeText;
        wxStaticBoxSizer* sbsJobTypeOutput;
        wxStaticText* stJobTypeDis;
        wxStaticText* stCharMultDis;
        wxStaticText* stGuessOccuAlpha;
        wxStaticText* stGuessAlteAlpha;
        wxStaticBoxSizer* sbsGuessOccup;
        wxStaticBoxSizer* sbsGuessAlter;
        bool m_isNewJob;
};

#endif
