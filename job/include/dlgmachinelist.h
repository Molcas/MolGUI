/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 29 2008)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __DLGMACHINELIST__
#define __DLGMACHINELIST__
//#include "datatype.h"
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/font.h>
#include <wx/grid.h>
#include <wx/gdicmn.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/dialog.h>
#include "sshclient.h"
///////////////////////////////////////////////////////////////////////////
const wxString FILE_BEGIN_MACHINEINFOS = wxT("$BEGIN MACHINEINFOS");
const wxString FILE_END_MACHINEINFOS = wxT("$END MACHINEINFOS");
const wxString FILE_BEGIN_MACHINE = wxT("$BEGIN MACHINE");
const wxString FILE_END_MACHINE = wxT("$END MACHINE");

class MachineInfo
{
public:
MachineInfo();
public:
wxString machineid;
wxString fullname;
wxString shortname;
wxString interprocessor;
wxString nodes;
wxString username;
wxString password;
wxString protocal;
wxString submitway;
wxString remotecmd;
wxString remoteworkbase;
wxString scriptfile;
wxString masterkey;
wxString masterid;
};

WX_DECLARE_OBJARRAY(MachineInfo, MachineInfoArray);

class MachineFile{
public:
static void SaveFile(const wxString fileName,MachineInfoArray machineinfoarray);
static void LoadFile(const wxString& strLoadedFileName, MachineInfoArray& machineinfoarray);
};
///////////////////////////////////////////////////////////////////////////////
/// Class DlgMachineList
///////////////////////////////////////////////////////////////////////////////
class DlgMachineList : public wxDialog
{
        private:
                DECLARE_EVENT_TABLE();
                void DoOk(wxCommandEvent& event);
                void DoCancel(wxCommandEvent&event);
            void OnClose(wxCloseEvent& event);
        void DoStatus(wxCommandEvent&event);
            void OnDiskUsage(wxCommandEvent&event);
               void OnNew(wxCommandEvent&event);
               void OnEdit(wxCommandEvent&event);
               void OnRemove(wxCommandEvent&event);
            int DoDiskUsage(void);
            void DoProcesses(wxCommandEvent&event);
            void OnMachineSelect(wxGridEvent& event);
            void OnPopupMenu(wxGridEvent& event);
            void SetBtnStatus(int status);
            int selmachine;
                bool m_connected;
                SSHClient *m_sshclient;
                MachineInfoArray m_machineinfoarray;
                bool m_busy;
        int m_oldhosts;
        protected:
                wxGrid* m_gridmachinelist;
                wxButton* m_btnnew;
                wxButton* m_btnedit;
                wxButton* m_btnremove;
                wxButton* m_btnstatus;
                wxButton* m_btndiskusage;
                wxButton* m_btnprocesses;
                wxTextCtrl* m_txtshow;
                wxButton* m_btnok;
                wxButton* m_btncancel;

        public:
                DlgMachineList( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 596,535 ), long style = wxDEFAULT_DIALOG_STYLE );
                ~DlgMachineList();
            bool GetMachineList(void);
};

#endif
