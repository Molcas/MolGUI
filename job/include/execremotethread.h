/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#ifndef __BP_MONITOR_EXECREMOTETHREAD_H__
#define __BP_MONITOR_EXECREMOTETHREAD_H__

#include "wx.h"
#include "execprocess.h"
#include "sshclient.h"

//#include <curl/curl.h>
const wxString FILE_BEGIN_JOBS = wxT("$BEGIN JOBS");
const wxString FILE_END_JOBS = wxT("$END JOBS");
const wxString FILE_BEGIN_JOB = wxT("$BEGIN JOB");
const wxString FILE_END_JOB = wxT("$END JOB");

class JobMonitorPnl;
class RemoteJobInfo
{
public:
wxString jobname;
wxString fullname;
wxString shortname;
wxString username;
wxString password;
wxString protocal;
wxString scriptfile;
wxString ticket;
wxString submittime;
wxString status;
wxString inputfile;
wxString jobpath;
wxString projectfile;
wxString submitway;
wxString command;
wxString masterkey;
wxString remoteworkbase;
wxString masterid;
wxString masterkey0;
wxString machineid;
wxString projecttype;
//SSHClient *m_sshclient;
//bool m_connected;
RemoteJobInfo();
};

class DownloadCheck{
public:
DownloadCheck();
wxString checkfile;
wxString inputfile;
};

WX_DECLARE_OBJARRAY(RemoteJobInfo, RemoteJobInfoArray);
WX_DECLARE_OBJARRAY(DownloadCheck, DownloadCheckArray);

class JobFile{
public:
static void LoadFile(const wxString& strLoadedFileName, RemoteJobInfoArray& remotejobinfoarray);
static void SaveFile(const wxString fileName,RemoteJobInfoArray remotejobinfoarray);
};


typedef enum _RemoteThreadStatus {
        REMOTE_INIT,
        REMOTE_WAITING,
        REMOTE_RECEIVING_RESULTS
}RemoteThreadStatus;

class RemoteThreadData;

class ExecRemoteThread : public wxThread {
        public:
//#ifdef __LIBSSH2__
                ExecRemoteThread(RemoteThreadData* threadData,SSHClient *sshclient,RemoteJobInfo* jobinfo,JobMonitorPnl * pjobmonitorpnl);
//#endif
                // thread execution start here
                virtual void* Entry();
                virtual void OnExit();

                bool KillThread(void);

        private:
                RemoteThreadData* m_threadData;
                RemoteThreadStatus m_status;
                RemoteJobInfo* m_jobinfo;
        void SaveRemoteJobInfo(void);
        wxArrayString PrepareInputFile(wxString inputfile);
        SSHClient* m_sshclient;
        JobMonitorPnl * m_pJobMonitorPnl;
};


class RemoteThreadData {
        public:
                RemoteThreadData(JobRunningData* pJobData);
                ~RemoteThreadData();
                bool RunThread(void);
                bool KillThread(void);

        public:
                wxString GetInputFile(void);
                wxString GetJobName(void);
                wxString GetProjectFile(void);
        wxString GetProjectType(void);
                long threadId;
                FILE* pfLog;

        private:
                JobRunningData* m_JobData;
                ExecRemoteThread* m_remoteThread;
                RemoteJobInfo* m_jobinfo;
        SSHClient* m_sshclient;


};

WX_DECLARE_STRING_HASH_MAP(RemoteThreadData*, RemoteThreadDataHash);

#endif
