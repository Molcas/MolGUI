/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_SIMUCAL_SIMUFILES_H__
#define __BP_SIMUCAL_SIMUFILES_H__

#include "wx.h"

#include "projectfiles.h"
#include "simujob.h"

class SimuFiles : public AbstractProjectFiles {
        public:
                void ShowResultItem(ProjectTreeItemData *item);
                SimuFiles(SimuProject* simuProject, wxString resultDir);
                ~SimuFiles();

                virtual bool CreateInputFile(void);
            virtual wxString GetExecCommand(void);
            virtual wxString GetExecPara(void);
            virtual wxString GetExecLogName(void);
            virtual bool PostJobTerminated(bool isFinal);
                virtual wxString GetOutputFileName(void);
                virtual wxString GetInputFileName(void);
 //           virtual void BuildResultTree(wxTreeCtrl* pTree, wxTreeItemId& jobResultNode);

 //       wxString GetVibOutputFileName(void);

 //       bool HasSurfaceData(void);
//                wxArrayString GetSurfaceDataFiles(void);
                wxString GetCalcResultFileName(ExecType type);
        wxString GetCalcParamFileName(ParamType type);
        wxString GetSurfaceFileName(void);
//                bool HasVibrationData(void);
//                virtual void InitVibrationData(void);
        void RemoveSurfaceFiles(wxString nmask);
        private:
                ExecType GetExecType(void);
//                bool DoCalcSurface(void);

};

#endif
