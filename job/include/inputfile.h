/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_SIMUCAL_INPUTFILE_H__
#define __BP_SIMUCAL_INPUTFILE_H__

#include "wx.h"
#include "chematom.h"
#include "renderingdata.h"
#include "simujob.h"

class INPUTFile
{
public:
        static void SaveFileAI(const wxString& fileName, RenderingData* pData, SimuJob* pJob);
        static void SaveFileSE(const wxString &fileName, RenderingData* pData, SimuJob* pJob);
#ifdef __BP_CONSTRAIN_DATA_H__
        static void SaveFile(const wxString &fileName, AtomBondArray &array, StringHash &setting,ConstrainData& constrainData);
        static void SaveConstrainFile(const wxString& fileName,AtomBondArray &array,ConstrainData& constrainData);
#endif
protected:
        static void SaveExport(wxTextOutputStream& tos);
        static void SaveOutputSE(wxTextOutputStream &tos, AtomBondArray &array, StringHash &setting);
        static void SaveSESettings(wxTextOutputStream &tos, StringHash &setting);
#ifdef __BP_CONSTRAIN_DATA_H__
        static void SaveFormat(wxTextOutputStream& tos,wxString filename,StringHash &setting);
        static void SaveSettings(wxTextOutputStream& tos,StringHash &setting);
        static void SaveOutput(wxTextOutputStream& tos,AtomBondArray &array, StringHash &setting);
        static void SaveJobType(wxTextOutputStream& tos,AtomBondArray &array, StringHash &setting,ConstrainData& constrainData);
        static void SaveTransitionState(wxTextOutputStream& tos,AtomBondArray &array,StringHash &setting,ConstrainData& constrainData);
        static void SaveConstraints(wxTextOutputStream& tos,AtomBondArray &array,ConstrainData& constrainData,bool useFlag);
        static void SaveGeometryOptimization(wxTextOutputStream& tos,AtomBondArray &array,StringHash &setting,ConstrainData& constrainData);
#endif
};

#endif // !defined(__BP_INPUTFILE_H__)
