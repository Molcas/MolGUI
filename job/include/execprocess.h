/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MONITOR_EXECPROCESS_H__
#define __BP_MONITOR_EXECPROCESS_H__

#include <wx/process.h>
#include <wx/txtstrm.h>
#include <wx/wfstream.h>

#include "wx.h"
#include "simuproject.h"

class ProcessData;
class IProcessListener;

class JobRunningData {
        public:
                JobRunningData(int projectId, ProjectType type, wxString projectTitle);
                int GetProjectId(void) const;
                ProjectType GetProjectType(void) const;
                wxString GetProjectTitle(void) const;
                void SetProjectFile(wxString projectFile);
                wxString GetProjectFile(void) const;
                void SetJobName(wxString jobName);
                wxString GetJobName(void)const;
                void SetOutputDir(wxString outputDir);
                wxString GetOutputDir(void) const;
                void SetCommand(wxString command, wxString para);
                wxString GetCommand(void) const;
                wxString GetPara(void) const;
                wxString GetInputFileName(void) const;
                void SetInputFileName(wxString inputfile);
                bool IsTerminated(void) const;
                void SetStartTimeStr(wxString startTime);
                void SetStartTime(wxLongLong startTime);
                void SetEndTime(wxLongLong endTime);
                wxLongLong GetEndTime(void) const;
                wxString GetStartTime(void) const;
                wxString GetRunningTime(void) const;

                bool IsFinal(void) const;
                void SetFinal(bool isFinal);

        private:
                bool m_isTerminated;
                int m_projectId;
                ProjectType m_type;
                wxString m_projectTitle;
                wxString m_projectFileName;
                wxString m_outputDir;
                wxString m_inputfilename;
                wxString m_jobName;
        wxString m_command;
        wxString m_para;
        wxString m_strStartTime;
        wxLongLong m_startTime;
        wxLongLong m_endTime;
        bool m_isFinal;
};

/**
 * This is the handler for process termination events
 * A specialization of ExecProcess for redirecting the output
 */
class ExecPipedProcess : public wxProcess {
        public:
                ExecPipedProcess(IProcessListener* listener, ProcessData *procData);
                ~ExecPipedProcess();
                /**
                 * instead of overriding this virtual function we might as well process the
             * event from it in the frame class - this might be more convenient in some
             * cases*/
                virtual void OnTerminate(int pid, int status);
                bool DumpOutput(bool isAll);
                void Release(void);

        private:
                IProcessListener *m_listener;
                ProcessData *m_procData;
                wxTextInputStream* pTisOutput;
                wxFileOutputStream* pFfoutOutput;
                wxTextOutputStream* pTosOutput;
                wxTextInputStream* pTisError;
                wxFileOutputStream* pFfoutError;
                wxTextOutputStream* pTosError;
};

class ProcessData {
        public:
                ProcessData(JobRunningData* pJobData);
                ~ProcessData();
                bool InitData(void);
                JobRunningData* GetJobRunningData(void);
                bool RunProcess(IProcessListener *listener);
                void KillProcess(void);
                bool DumpOutput(bool isAll);
                wxString GetShellCmd(void) const;
                wxString GetCommand(void) const;

        public:
                long processId;
                wxString outputFile;
                wxString errorFile;
                bool isValid;

        private:
                JobRunningData* m_pJobData;
                ExecPipedProcess* m_pipedProcess;
};

class IProcessListener {
        public:
                virtual ~IProcessListener() {}
                virtual void AppendOutput(wxString& msg) = 0;
                virtual void AppendError(wxString& msg) = 0;
                virtual void OnProcessTerminated(ProcessData* procData, int status) = 0;
};

//WX_DECLARE_HASH_MAP(int, ExecPipedProcess*, wxIntegerHash, wxIntegerEqual, PipedProcessHash);
WX_DECLARE_STRING_HASH_MAP(ProcessData*, ProcessDataHash);
#endif
