/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_SIMUCAL_OUTPUTANALYSE_H__
#define __BP_SIMUCAL_OUTPUTANALYSE_H__

#include "wx.h"
#include "chematom.h"
#include "commons.h"
#include "vibration.h"
#include "ctrlinst.h"
#include "simufiles.h"

class OutputAnalyse
{
public:
        static void ClearData(void);
        static bool AnalyseVibrationData(SimuFiles *pSimuFiles);
        static bool SaveVibrationData(const wxString& fileName);
        static StringHash GetSemiSummaryData(void);
        static void SaveOutputMolecule(wxString fileName);
        static bool CheckSuccess(void);
        static void LoadOutputFile(const wxString& outputFileName,ExecType execType);
private:
        static Vibration GetSemiVibrationData(void);
//        static Vibration GetAbinVibrationData(void);
//        static void GetAbinOutputMolecule(AtomBondArray& chemArray);
        static wxString GetSemiCPUTime(const wxArrayString& semiData);
        static wxString GetSemiDipoleMoment(const wxArrayString& semiData);
        static wxString GetSemiLUMO(const wxArrayString& semiData);
        static wxString GetSemiHOMO(const wxArrayString& semiData);
        static wxString GetSemiRMSGradientNorm(const wxArrayString& semiData);
        static wxString GetSemiEnergy(const wxArrayString& semiData);
        static void GetSemiOutputMolecule(AtomBondArray& chemArray);
        static bool CheckSemiSuccess();
        static wxArrayString data;
        static ExecType type;
};

#endif // !defined(__MOLGUI_OUTPUTANALYSE_H__)
