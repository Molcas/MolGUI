/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MONITOR_JOBMONITORPNL_H__
#define __BP_MONITOR_JOBMONITORPNL_H__

#include "wx.h"
//#include "datatype.h"
#include <wx/bookctrl.h>
#include "genericdlg.h"
#include "execprocess.h"
#include "execremotethread.h"
#include "processoutputpnl.h"
#include <wx/grid.h>
#include <wx/textctrl.h>
#include "sshclient.h"
#include "httppost.h"
#include <messageevent.h>

class JobMonitorPnl : public GenericPnl, public IProcessListener {
private:
    DECLARE_EVENT_TABLE();

public:
    void ExecJob(JobRunningData *pJobData);
    JobMonitorPnl(GenericDlg *parent, wxWindowID id = wxID_ANY);
    virtual ~JobMonitorPnl();
    virtual long GetButtonStyle(void) { return BTN_KILL | BTN_CLOSE; }
    virtual bool OnButton(long style);
    void GetRemoteJobInfoList(void);
    void ExecLocalJob(JobRunningData *pJobData);
    void ExecRemoteJob(JobRunningData *pJobData);
    bool IsClosable(void);
    //add by xia
    ProcessData *GetProcessData(void);
    //end
    ProcessOutputPanel *pPnlOutput;

    virtual void AppendOutput(wxString &msg);
    virtual void AppendError(wxString &msg);
    virtual void OnProcessTerminated(ProcessData *procData, int status);
    void OnPopupMenu(wxGridEvent &event);
    void OnHttpDownFinished();
    void OnSshDownFinished();
    void SetArrayDownload(wxArrayInt arydownload);
    void SetAryDownlist(wxArrayString lfiles, wxArrayString rfiles);
private:
    void CreateControls(void);
    bool OnBtnClose(void);
    bool OnBtnKill(void);

    void OnBtnStatus(wxCommandEvent &event);
    void OnBtnDownload(wxCommandEvent &event);
    void OnBtnRemoteKill(wxCommandEvent &event);
    void GetRemoteJobList();

    void OnBtnEdit(wxCommandEvent &event);
    void OnBtnShow(wxCommandEvent &event);
    void OnTimerIdle(wxTimerEvent &event);
    void OnTimerRemoteMonitor(wxTimerEvent &event);
    void OnLbLocalRunningItemClicked(wxCommandEvent &event);
    //                void OnLbRemoteRunningItemClicked(wxCommandEvent& event);
    void OnLbTerminatedItemClicked(wxCommandEvent &event);
    void OnLbTerminatedItemDClicked(wxCommandEvent &event);
    void OnNbList(wxNotebookEvent &event);
    void OnJobSelect(wxGridEvent &event);
    void OnBtnRemoteCommand(wxCommandEvent &event);
    void OnFreqSelect(wxCommandEvent &event);
    void ShowTerminatedResult(void);
    ProcessData *GetSelectedItem(wxListBox *listBox);
    void SaveRemoteJobInfo(void);
    void DownloadResult(int currow);
    void DoCheckStatus(int currow);
    void DoRemoveJob(unsigned int currow);
    void OnRemoveJob(wxCommandEvent &event);
    void SetBtnStatus(int status);
    void OnActivate(wxActivateEvent &event);
    void OnMessageUpdate(wxMessageUpdateEvent &event);
private:
    //add by xia
    ProcessData *m_procData;
    //end
    wxNotebook *pNbList;
    wxListBox *pLbLocalRunning;
    //                wxListBox* pLbRemoteRunning;
    wxListBox *pLbTerminated;
    //wxListBox* pLbKilled;
    // the idle event wake up timer
    wxTimer timerIdleWakeUp;
    wxTimer timerRemoteMonitor;
    ProcessDataHash procDataHash;
    RemoteThreadDataHash threadDataHash;
    //long selectedProcessId;
    wxGrid *m_gridjoblist;
    wxRadioBox *m_rBoxFreq;
    wxButton *m_btnstatus;
    wxButton *m_btndisplayoutput;
    wxButton *m_btnkill;
    wxButton *m_btnremotecommand;
    RemoteJobInfoArray m_remotejobinfoarray;
    int m_seljob;
    SSHClient *m_sshclient;
    HttpPost *m_httppost;
    bool m_connected;
    wxTextCtrl *m_txtremotecommand;
    int m_newjobs, m_oldjobs;
    bool m_busy;
    DownloadCheckArray m_downloadcheckarray;
    wxArrayString m_arylfiles, m_aryrfiles;
    wxArrayInt m_arydownload;
    //        wxArrayString m_arychkfiles,m_aryinputfiles;
};

#endif
