/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_SIMUCAL_SIMUJOB_H__
#define __BP_SIMUCAL_SIMUJOB_H__

#include "wx.h"
#include "datatype.h"
#include "simuproject.h"

extern const wxString FILE_BEGIN_JOB_CONTROL;
extern const wxString FILE_END_JOB_CONTROL;
extern const wxString FILE_BEGIN_SURFACE;
extern const wxString FILE_END_SURFACE;
extern const wxString FILE_BEGIN_TITLE;
extern const wxString FILE_END_TITLE;

typedef enum _execType {
        EXEC_SEMI,
        EXEC_SURFACE,
        EXEC_ABIN,
        EXEC_NONE//NO CALC
}ExecType;

typedef enum _paramType{
        PARAM_ARC,
        PARAM_NONE
}ParamType;

/**
 * SimuJob represents a simu job.
 */
class SimuJob : public JobInfo {

public:
        SimuJob(wxString fileName);
        ~SimuJob();
        virtual bool LoadFile(void);
        virtual bool SaveFile(void);

        ///job control
        wxString GetJobTitle(void);
        StringHash& GetJobControl(void);
        StringHash& GetJobSurfaceSettings(void);
        void SetJobTitle(wxString jobTitle);
        void SetJobControl(StringHash& jobControl);
        void SetJobSurfaceSettings(StringHash& surfaceSettings);

        bool CanCalcSurface(void);

        wxString GetCalcParamFileName(ParamType type);
        void ResetResultFlags();

        bool SaveJobControl(wxTextOutputStream &tos);
        bool SaveJobSurface(wxTextOutputStream &tos);


        ExecType GetExecType(void);

private:
        wxArrayString FormatJobSurface(StringHash& surfaceSet);
        wxArrayString FormatJobControl(StringHash& jobControl);

private:
        wxString                                m_jobTitle;                                        //save the job title
        StringHash                                m_jobControlHash;                        // save the job controls
        StringHash                                m_jobSurfaceHash;                        //save the surface set of job
};

#endif
