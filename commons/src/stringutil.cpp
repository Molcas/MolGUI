/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "stringutil.h"
#include "envutil.h"

bool StringUtil::IsEmptyString(wxString &str) {
    if(wxEmptyString == str || str.IsEmpty() || str.Trim().Len() <= 0) {
        return true;
    }
    return false;
}

wxString StringUtil::CharPointer2String(const char *str) {
    if(str != NULL) {
        return wxString(str, wxConvUTF8);
    } else {
        return wxEmptyString;
    }
}

void StringUtil::String2CharPointer(char **charStr, wxString str) {
    const char *value = NULL;
    *charStr = NULL;
    if(str == wxEmptyString || str.IsEmpty()) {
    } else {
        wxString trimStr = str.Trim(true);
        trimStr = trimStr.Trim(false);
        if(!trimStr.IsEmpty()) {
            wxCharBuffer charBuf = str.mb_str(wxConvUTF8);
            value = (const char *) charBuf;
            StringCopy(charStr, value, strlen(value));
        }
    }
}

void StringUtil::String2UnSignedCharPointer(unsigned char **charStr, wxString str) {
    *charStr = NULL;
    char *tchar = NULL;
    String2CharPointer(&tchar, str);
    *charStr = reinterpret_cast<unsigned char *>(tchar);
}


void StringUtil::StringCopy(char **dest, const char *src, int srclen) {
    //apply for one more space and add '\0'
    *dest = (char *)malloc(srclen + 1);
    if( NULL == *dest) {
        printf("cannot allocate memory\n");
        exit(1);
    }
    (*dest)[0] = 0;                                        // it should be set '\0' as to use strncat
    strncat(*dest, src, srclen);        //cannot use strncpy as it may be wrong
}

int StringUtil::ToInt(wxString value) {
    int intValue = -1;
    long longValue = 0;
    if(value.ToLong(&longValue)) {
        intValue = (int)longValue;
    }
    return intValue;
}

wxString StringUtil::FormatBool(bool value) {
    return (value ? wxT("TRUE") : wxT("FALSE"));
}

bool StringUtil::IsDigital(const wxString &src) {
    wxString tempStr = src;
    wxString strNumber = wxT("1234567890");
    wxString strSymbol = wxT("+-");
    tempStr = tempStr.Trim().Left(1);
    if(strNumber.Find(tempStr) == wxNOT_FOUND && strSymbol.Find(tempStr) == wxNOT_FOUND)
        return false;
    if(strSymbol.Find(tempStr) != wxNOT_FOUND) {
        tempStr = src;
        tempStr = tempStr.Trim().Mid(1, 1);
        if(strNumber.Find(tempStr) == wxNOT_FOUND)
            return false;
    }
    return true;
}

wxString StringUtil::CheckInput(wxString input, bool addPathSep)
{
    wxString output = wxEmptyString, tmp = wxEmptyString;
    int strLen = input.Len();
    wxChar  chTmp;
    //        int spDetectorBeg=-1,spDetectorEnd=spDetectorBeg;
    for(int i = 0; i < strLen; i++) {
        chTmp = input.GetChar(i); //deal with the ith char
        /*                if(chTmp==wxT(' ')){//check the space in the string
                                if(spDetectorBeg==-1)
                                        spDetectorBeg=i;
                                spDetectorEnd=i;
                                continue;
                        }
                        if(spDetectorEnd>=spDetectorBeg&&spDetectorBeg>-1)
                        {
                                if(spDetectorBeg==0)//the string can not begin with space
                                {
                                        strLen=strLen-(spDetectorEnd+1);
                                        input=input.Right(strLen);
                                        i=0;
                                }
                                spDetectorBeg==-1;
                                spDetectorEnd=spDetectorBeg;
                        }
         */       if(addPathSep == true) //the input string contain pathsep
            if(chTmp == platform::PathSep().GetChar(0))
                continue;
        if(chTmp == wxT('@') || chTmp == 47 || chTmp == 92 || chTmp == 58 || chTmp == 42 || chTmp == 63 || chTmp == 22 || chTmp == 60 || chTmp == 62 || chTmp == 124 || chTmp == 32 || chTmp == 38) //illegal char
        {
            if(i != 0) {
                tmp = input.Left(i);
                output = output + tmp;
            }
            if(strLen == (i + 1)) {
                input = wxEmptyString;
                break;
            }
            input = input.Right(strLen - i - 1);
            strLen = input.Len();
            i = -1;
        }
    }
    /*        if(spDetectorBeg==0){
                    output=wxEmptyString;
            }else{
    */        output = output + input;
    //        }
    return output;
}
wxString StringUtil::CheckString(wxString input, bool addPathSep)
{
    wxString output = wxEmptyString, tmp = wxEmptyString;
    int strLen = input.Len();
    wxChar  chTmp;
    int spDetectorBeg = -1, spDetectorEnd = spDetectorBeg;
    for(int i = 0; i < strLen; i++) {
        chTmp = input.GetChar(i); //deal with the ith char
        if(chTmp == wxT(' ')) { //check the space in the string
            if(spDetectorBeg == -1)
                spDetectorBeg = i;
            spDetectorEnd = i;
            continue;
        }
        if(spDetectorEnd >= spDetectorBeg && spDetectorBeg > -1)
        {
            if(spDetectorBeg == 0) //the string can not begin with space
            {
                strLen = strLen - (spDetectorEnd + 1);
                input = input.Right(strLen);
                i = 0;
            }
            spDetectorBeg = -1;
            spDetectorEnd = spDetectorBeg;
        }
        if(addPathSep == true) //the input string contain pathsep
            if(chTmp == platform::PathSep().GetChar(0))
                continue;
        if(chTmp == wxT('@') || chTmp == 47 || chTmp == 92 || chTmp == 58 || chTmp == 42 || chTmp == 63 || chTmp == 22 || chTmp == 60 || chTmp == 62 || chTmp == 124 || chTmp == 32 || chTmp == 38) //illegal char
        {
            if(i != 0) {
                tmp = input.Left(i);
                output = output + tmp;
            }
            if(strLen == (i + 1)) {
                input = wxEmptyString;
                break;
            }
            input = input.Right(strLen - i - 1);
            strLen = input.Len();
            i = -1;
        }
    }
    if(spDetectorBeg == 0) {
        output = wxEmptyString;
    } else if(spDetectorEnd == strLen - 1) { //the string can not end with space
        input = input.Left(spDetectorBeg);
        output = output + input;
    } else {
        output = output + input;
    }
    return output;
}
wxString StringUtil::FormatValue(long value, int width, wxChar emptyChar) {
    if(width < 0)
        return wxString::Format(wxT("%ld"), value);
    wxString str = wxString::Format(wxT("%ld"), value);
    while((int)str.Len() < width) {
        str = emptyChar + str;
    }
    return str;
}

wxString StringUtil::FormatValue(int value, int width, wxChar emptyChar) {
    if(width < 0)
        return wxString::Format(wxT("%d"), value);
    wxString str = wxString::Format(wxT("%d"), value);
    while((int)str.Len() < width) {
        str = emptyChar + str;
    }
    return str;
}

wxString StringUtil::UpperFirstChar(const wxString &src) {
    return src.Left(1).Upper() + src.Right(src.Len() - 1).Lower();
}


wxString StringUtil::FormatTime(wxLongLong longTime) {
    long diffTime = longTime.ToLong();
    long millsec, seconds, minutes, hours;
    hours = diffTime / 3600000;
    minutes = diffTime / 60000 - hours * 60;
    seconds = diffTime / 1000 - hours * 3600 - minutes * 60;
    millsec = diffTime % 1000;
    return wxString::Format(wxT("%ld:"), hours) + FormatValue(minutes, 2) + wxT(":") + FormatValue(seconds, 2) + wxT(":") + FormatValue(millsec, 3);
}

bool StringUtil::StartsWith(wxString str, wxString prefix, bool ignoreCase) {
    bool isStartWith = false;
    int index = -1;
    if (ignoreCase) {
        str = str.Lower();
        prefix = prefix.Lower();
    }
    if (str.Len() >= prefix.Len()) {
        index = str.Find(prefix);
        if (index != wxNOT_FOUND && index == 0) {
            isStartWith = true;
        }
    }
    return isStartWith;
}

bool StringUtil::EndsWith(wxString str, wxString suffix, bool ignoreCase) {
    //bool isEndWith = false;
    //int index = -1;
    if (ignoreCase) {
        str = str.Lower();
        suffix = suffix.Lower();
    }
    return str.EndsWith(suffix);
    /* wrong implemented
    if (str.Len() >= suffix.Len()) {
            index = str.Find(suffix, true);
            wxLogError(wxString::Format(wxT("index %d"), index));
            if (index != wxNOT_FOUND && index + suffix.Len() == str.Len()) {
                    isEndWith = true;
            }
    }
    return isEndWith;*/
}

int StringUtil::SubStringIndex(char *str, const char *subString, int fromIndex) {
    int index = -1;
    int i = 0, j = 0;
    int hasFound = 0;

    for(i = fromIndex; i < (int)strlen(str); i++) {
        hasFound = 1;
        j = 0;
        index = i;
        while(i < (int)strlen(str) && j < (int)strlen(subString)) {
            if(str[i++] != subString[j++]) {
                hasFound = 0;
                i--;
                break;
            }
        }
        if(hasFound) {
            return index;
        }
    }
    return -1;
}

wxString StringUtil::GetCurTime(int flag)
{
    wxString curtime = wxEmptyString;
    char outstr[128];
    char str[100];

    time_t t;
    struct tm *area;
    t = time(NULL);
    area = localtime(&t);

    memset(outstr, 0x0, sizeof(outstr));

    if(flag == 1)
    {
        sprintf(str, "%4d", 1900 + area->tm_year);
        strcat(outstr, str);
        sprintf(str, "%02d", area->tm_mon + 1);
        strcat(outstr, str);
        sprintf(str, "%02d", area->tm_mday);
        strcat(outstr, str);
        sprintf(str, "%02d", area->tm_hour);
        strcat(outstr, str);
        sprintf(str, "%02d", area->tm_min);
        strcat(outstr, str);
        sprintf(str, "%02d", area->tm_sec);
        strcat(outstr, str);
    }
    else if(flag == 3)
    {
        sprintf(str, "%4d", 1900 + area->tm_year);
        strcat(outstr, str);
        sprintf(str, "%02d", area->tm_mon + 1);
        strcat(outstr, str);
        sprintf(str, "%02d ", area->tm_mday);
        strcat(outstr, str);
    }
    else
    {
        sprintf(str, "%4d-", 1900 + area->tm_year);
        strcat(outstr, str);
        sprintf(str, "%02d-", area->tm_mon + 1);
        strcat(outstr, str);
        sprintf(str, "%02d ", area->tm_mday);
        strcat(outstr, str);
        sprintf(str, "%02d:", area->tm_hour);
        strcat(outstr, str);
        sprintf(str, "%02d:", area->tm_min);
        strcat(outstr, str);
        sprintf(str, "%02d", area->tm_sec);
        strcat(outstr, str);
    }
    curtime = CharPointer2String(outstr);
    return curtime;
}
bool StringUtil::IncSufNumber(wxString &string)
{
    string = string + wxT("1");
    return true;
}

int StringUtil::StringMatch(wxString strsrc, wxString strmatch)
{
    int i, j, len1, len2;
    bool cflag = false;
    len1 = strsrc.Length();
    len2 = strmatch.Length();
    j = 0;
    for(i = 0; i < len1 && j < len2; i++) {
        if(strsrc.GetChar(i) != strmatch.GetChar(j)) {
            if(cflag) {
                j++; i--; cflag = false;
            } else if(strsrc.GetChar(i) != ' ')
                return -1;
        }
        else
        {
            cflag = true;
        }

    }
    if(j >= len2)
        return i;
    else
        return -1;
}

bool StringUtil::Contains(const wxString &where, const wxString &what, bool ignoreCase)
{
    wxString str1 = where;
    wxString str2 = what;

    if(ignoreCase)
    {
        str1.MakeUpper();
        str2.MakeUpper();
    }
    if(str1.Find(str2) == wxNOT_FOUND)
        return false;
    return true;
}

wxArrayString StringUtil::Split(const wxString& str, const wxString& delims, wxStringTokenizerMode mode)
{
    wxArrayString array;
    wxStringTokenizer tkzr(str,delims,mode);
    while(tkzr.HasMoreTokens())
    {
        array.Add(tkzr.GetNextToken());
    }
    return array;
}
