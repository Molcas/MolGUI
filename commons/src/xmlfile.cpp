/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "xmlfile.h"

#include "envutil.h"
#include "stringutil.h"
#include "tools.h"
#include "cstringutil.h"

XmlFile::XmlFile(const wxString& rootName, const wxString& filePath) {
        m_filePath = filePath;
//        wxMessageBox(m_filePath);
        char* capp = NULL;
        if(wxFileExists(m_filePath)) {
                StringUtil::String2CharPointer(&capp, filePath);
    //    fprintf(stderr,capp);
                m_rootElementList = ParseXmlDocument(capp, false);
        }else {
                StringUtil::String2CharPointer(&capp, rootName);
                m_rootElementList = CreateDocument(capp);
        }
//        if(capp) free(capp);
}

XmlFile::~XmlFile() {
        FreeXmlElementList(m_rootElementList);
}
void XmlFile::BuildElement(const char* elemName, wxArrayString& arrayString)
{
    if(elemName==NULL)
        return;
        XmlElement* elem = GetSubElement(GetRootElement(), elemName);
        XmlElement* strelem = NULL;
        if(elem == NULL) {
                elem = AddElement(GetRootElement());
                StringCopy(&elem->name, elemName, strlen(elemName));
        }else {
                //clear its contents, attributes, and subelements
                FreeXmlElement(elem);
        }

        /*int count = 0;
        XmlElement** elems = GetSubElements(GetRootElement(), elemName, &count);
        XmlElement* elem = NULL;
        XmlElement* strelem = NULL;
        if(count <= 0) {
                elem = AddElement(GetRootElement());
                StringCopy(&elem->name, elemName, strlen(elemName));
        }else {
                elem = elems[0];
                //clear its contents, attributes, and subelements
                FreeXmlElement(elem);
        }*/
        for(unsigned i = 0; i < arrayString.GetCount(); i++) {
                strelem = AddElement(elem);
                StringCopy(&strelem->name, "string", strlen("string"));
                StringUtil::String2CharPointer(&strelem->content, arrayString[i]);
        }
}
void XmlFile::BuildElement(const char* elemName, wxString& value)
{
    if(elemName==NULL)
        return;
        XmlElement* elem = GetSubElement(GetRootElement(), elemName);
        if(elem == NULL) {
                elem = AddElement(GetRootElement());
                StringCopy(&elem->name, elemName, strlen(elemName));
        }else {
                FreeXmlElement(elem);
        }
        char* cvalue = NULL;
        StringUtil::String2CharPointer(&cvalue, value);
        AddAttribute(elem, "value", cvalue);
        free(cvalue);
}
void XmlFile::BuildElement(const char* elemName, bool isTrue)
{
        wxString value;
        if(isTrue) {
                value = wxT("true");
        }else{
                value = wxT("false");
        }
        BuildElement(elemName, value);
}
wxArrayString XmlFile::GetArrayString(const char* elemName)
{
        int count = 0;
        wxArrayString arrayString;
    if(elemName==NULL)
        return arrayString;
        XmlElement** elems = GetSubElements(GetRootElement(), elemName, &count);
        if(count > 0) {
                XmlElement** strelems = GetSubElements(elems[0], "string", &count);
                for(int i = 0; i < count; i++) {
                        //begin : hurukun : 10/11/2009
                        //if(::wxFileExists(StringUtil::CharPointer2String(strelems[i]->content)))
                        //end   : hurukun : 10/11/2009
                            arrayString.Add(StringUtil::CharPointer2String(strelems[i]->content));
                }
        }

        return arrayString;
}
XmlElement* XmlFile::GetRootElement(void)
{
    return &m_rootElementList->elem;
}
wxString XmlFile::GetString(const char* elemName)
{
        wxString value = wxEmptyString;
        XmlElement* elem = GetSubElement(GetRootElement(), elemName);
        if(elem != NULL) {
                char* cvalue = GetAttributeString(elem, "value");
                value = StringUtil::CharPointer2String(cvalue);
        }
        return value;
}
bool XmlFile::GetBool(const char* elemName, bool defaultValue)
{
        bool value = defaultValue;
        wxString strValue = GetString(elemName);
        if(!StringUtil::IsEmptyString(strValue)) {
                value = strValue.Cmp(wxT("true")) == 0;
        }
        return value;
}
XmlElementList * XmlFile::GetRoot()
{
return m_rootElementList;
}
void XmlFile::Save(void)
{
        FILE* fp = NULL;
        char* cfile = NULL;
        StringUtil::String2CharPointer(&cfile, m_filePath);
        fp = fopen(cfile, "w");
#ifdef __DEBUG_XML__
        fprintf(stderr,"%s",cfile);
        if(fp==NULL){
            fprintf(stderr,"XmlFile::Save Error!");
        }
        else{
        fprintf(stderr,"xmlfile::save Open Success!");
        }
#endif
        fprintf(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
        DumpXmlElementList(fp, m_rootElementList, 0);
        fclose(fp);
        free(cfile);
}
