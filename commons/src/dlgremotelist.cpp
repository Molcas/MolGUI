/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsshinfo.cpp: implementation of the DlgSSHInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "dlgremotelist.h"
#include "stringutil.h"
#include "envutil.h"
#include "dlgmachinelist.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/filename.h>
#include "dlgtemplate.h"

int CTRL_ID_COMB_HOSTNAME_LIST=wxNewId();
int CTRL_ID_BTN_NEWHOST=wxNewId();

DlgRemoteList::DlgRemoteList(wxWindow* parent, wxWindowID id, const wxString& title,
            const wxPoint& pos , const wxSize& size,long style)
{
        wxDialog::Create(parent, id, title, pos, wxSize(400,260), style);
        m_inputfile=wxEmptyString;
        CreateGUIControls();
}
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgRemoteList, wxDialog)
        EVT_BUTTON(wxID_OK, DlgRemoteList::DoOk)
        EVT_BUTTON(wxID_CANCEL,DlgRemoteList::DoCancel)
    EVT_CLOSE(DlgRemoteList::OnClose)
        EVT_BUTTON(CTRL_ID_BTN_NEWHOST,DlgRemoteList::OnNewHost)
END_EVENT_TABLE()

DlgRemoteList::~DlgRemoteList()
{
}

void DlgRemoteList::DoCancel(wxCommandEvent&event)
{
    this->Close();
}

void DlgRemoteList::CreateGUIControls()
{
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        SetSizer(bsTop);
        wxStaticText* lbtitle = new wxStaticText( this, wxID_STATIC,
                wxT("Please choose the host you want to submit: ") ,
                wxPoint(20,5), wxSize(260,20), 0 );
        m_combHostname=new wxComboBox(this,CTRL_ID_COMB_HOSTNAME_LIST,wxEmptyString,wxDefaultPosition,wxDefaultSize);
        m_combHostname->SetWindowStyle(wxCB_DROPDOWN);
        wxButton *btnNewHostname=new wxButton(this,CTRL_ID_BTN_NEWHOST,wxT("Manage Hosts"),wxDefaultPosition,wxDefaultSize);
        wxBoxSizer *bsBottom=new wxBoxSizer(wxHORIZONTAL);
        bsTop->Add(lbtitle,0,wxALL|wxALIGN_CENTER,5);
        bsBottom->Add(m_combHostname,0,wxALL|wxALIGN_CENTER,5);
        bsBottom->Add(btnNewHostname,0,wxALL|wxALIGN_CENTER,5);
        bsTop->Add(bsBottom,0,wxALL|wxALIGN_CENTER,5);
        wxButton* btnok = new wxButton( this, wxID_OK, wxT("&OK"),wxDefaultPosition, wxDefaultSize);
        wxButton* btncancel=new wxButton(this,wxID_CANCEL,wxT("&Cancel"),wxDefaultPosition,wxDefaultSize);
        wxBoxSizer *bsBtn=new wxBoxSizer(wxHORIZONTAL);

        bsBtn->Add(btnok,0,wxALL|wxALIGN_CENTER,5);
        bsBtn->Add(btncancel,0,wxALL|wxALIGN_CENTER,5);
        bsTop->Add(bsBtn,0,wxALL|wxALIGN_CENTER,5);
        bsTop->SetSizeHints(this);
        RefreshHostList();

}

void DlgRemoteList::DoOk(wxCommandEvent& event)
{
     if(m_combHostname->GetSelection()>0) {
          if(SaveJobInfo()&&IsModal())
               EndModal(wxID_OK);
          else if(IsModal())
               EndModal(wxID_CANCEL);
     }
     if(IsModal())
          EndModal(wxID_OK);
}

void DlgRemoteList::OnClose(wxCloseEvent &event)
{
        if(IsModal())
        {
                EndModal(wxID_CANCEL);
        }
        else
        {
                SetReturnCode(wxID_CANCEL);
                this->Show(false);
        }
}


void DlgRemoteList::SetInputFileName(wxString inputfile)
{
    m_inputfile=inputfile;
}


void DlgRemoteList::ReplaceScript(wxString scriptfile)
{
        if (!wxFileExists(scriptfile))return;
        wxString strLine;
        wxString strContent=wxEmptyString;
        wxFileInputStream fis(scriptfile);
        wxTextInputStream tis(fis);
        while(!fis.Eof())
        {
                strLine = tis.ReadLine();
                strLine.Trim();
                strLine.Trim(false);
                if(!strLine.IsEmpty()){
            strContent=strContent+strLine;
            strContent=strContent+wxT("\n");
        }
        }

    wxFileName finputfile(m_inputfile);
    strContent.Replace(wxT("$inputfile"),finputfile.GetName());
    wxFileOutputStream fos(scriptfile);
    wxTextOutputStream tos(fos);
    tos<<strContent<<endl;



}

void DlgRemoteList::RefreshHostList()
{
    MachineInfoArray machineinfoarray;
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
        MachineFile::LoadFile(fileName,machineinfoarray);
        int totalhost=machineinfoarray.GetCount();

        wxArrayString hostnameary;
        hostnameary.Add(wxT("Local"));
        for(int i=0;i<totalhost;i++)
        {
                        m_hostarray.Add(machineinfoarray[i].machineid);
                        //wxMessageBox(machineinfoarray[i].machineid);
                        hostnameary.Add(machineinfoarray[i].masterid+wxT(": ")+ machineinfoarray[i].shortname);
                        //wxMessageBox(machineinfoarray[i].shortname);
        }
        m_combHostname->Clear();
        m_combHostname->Append(hostnameary);
        m_combHostname->SetSelection(0);
}


void DlgRemoteList::OnNewHost(wxCommandEvent &event)
{
//        DlgSSHInfo *sshinfo=new DlgSSHInfo(this);
//        sshinfo->ShowModal();
        DlgMachineList * machinelist=new DlgMachineList(this,wxID_ANY,wxT("Hosts List"));
        if(!machinelist->GetMachineList())return;
    machinelist->ShowModal();
        RefreshHostList();
}

void DlgRemoteList::SaveSelectHost(){
        if(m_combHostname->GetSelection()>0){
        wxString fileName=m_inputfile+wxT(".host");
        if(wxFileExists(fileName))wxRemoveFile(fileName);
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
    tos << m_hostarray[m_combHostname->GetSelection()-1] << endl;
        }
}
void DlgRemoteList::LoadSelectHost(){
    unsigned int i;
    wxString hostname=wxEmptyString;
        wxString fileName=m_inputfile+wxT(".host");
        if(!wxFileExists(fileName))return;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        hostname=tis.ReadLine();
    if(hostname.IsNull()||hostname.IsEmpty())return;
    for(i=0;i<m_hostarray.GetCount();i++){
        if(hostname.Cmp(m_hostarray[i])==0){
            m_combHostname->SetSelection(i+1);
            return;
        }
    }
    m_combHostname->SetSelection(0);
}

bool DlgRemoteList::SaveJobInfo()
{
    MachineInfo minfo=MachineInfo();

    MachineInfoArray machineinfoarray;
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
        MachineFile::LoadFile(fileName,machineinfoarray);
        int totalhost=machineinfoarray.GetCount();

        wxString hostname=m_hostarray[m_combHostname->GetSelection()-1];
        wxString scriptOld,scriptNew;
        for(int i=0;i<totalhost;i++)
        {
                if(hostname.Cmp(machineinfoarray[i].machineid)==0){
        minfo.machineid=machineinfoarray[i].machineid;
                minfo.fullname=machineinfoarray[i].fullname;
                minfo.shortname=machineinfoarray[i].shortname;
                minfo.interprocessor=machineinfoarray[i].interprocessor;
                minfo.nodes=machineinfoarray[i].nodes;
                minfo.username=machineinfoarray[i].username;
                minfo.password=machineinfoarray[i].password;
                minfo.protocal=machineinfoarray[i].protocal;
                minfo.submitway=machineinfoarray[i].submitway;
                minfo.masterid=machineinfoarray[i].masterid;
                minfo.masterkey=machineinfoarray[i].masterkey;
                if(minfo.submitway.Cmp(wxT("One command with variables"))==0)
            minfo.remotecmd=machineinfoarray[i].remotecmd;
        else
            minfo.remotecmd=wxT("no use");
                minfo.remoteworkbase=machineinfoarray[i].remoteworkbase;
                if(minfo.submitway.Cmp(wxT("One command with variables"))==0)
            minfo.scriptfile=wxT("no use");
        else{
            minfo.scriptfile=GetFilePath()+machineinfoarray[i].scriptfile;

            if(minfo.protocal.Cmp(wxT("HTTP"))!=0){
            scriptNew=minfo.scriptfile;
            scriptOld=EnvUtil::GetUserDataDir() + platform::PathSep() + machineinfoarray[i].scriptfile;

            if(wxFileExists(scriptOld)){
                wxCopyFile(scriptOld,scriptNew,true);
            }else{
                wxMessageBox(wxT("Script file remote machine is not found!"));
                return false;
            }
            }
            break;
            }
                }
        }

        fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
        //if(wxFileExists(fileName))wxRemoveFile(fileName);
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);

    tos << wxT("1") << endl;
    tos << m_inputfile << endl;
    tos << minfo.fullname << endl;
    tos << minfo.shortname << endl;
    tos << minfo.username << endl;
    tos << minfo.password << endl;
    tos << minfo.remotecmd << endl;
    tos << minfo.scriptfile << endl;
    tos << minfo.protocal << endl;
    tos << minfo.submitway << endl;
    tos << minfo.remoteworkbase << endl;
    tos << minfo.masterid << endl;
    tos << minfo.masterkey << endl;
    tos << minfo.machineid << endl;
    if(minfo.protocal.Cmp(wxT("HTTP"))!=0){
        if(minfo.submitway.Cmp(wxT("One command with variables"))!=0){
            DlgTemplate *dlgtemplate=new DlgTemplate(this);
            dlgtemplate->AppendTemplate(minfo.scriptfile);
            if(dlgtemplate->ShowModal()==wxID_CANCEL)return false;
            ReplaceScript(minfo.scriptfile);

#if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_))
wxString exeproc=EnvUtil::GetPrgRootDir()+wxT("\\modules\\dos2unix.exe \"")+minfo.scriptfile+wxT("\"");
//wxMessageBox(exeproc);
wxExecute(exeproc, wxEXEC_SYNC);
#endif

    }
        }
        return true;
}

wxString DlgRemoteList::GetFilePath(void) {
        if (m_inputfile.IsEmpty()) {
                return wxT("./");
        }
        return wxFileName(m_inputfile).GetPathWithSep();
}
