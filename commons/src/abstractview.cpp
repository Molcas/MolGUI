/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "abstractview.h"
#include "molvieweventhandler.h"
#include "stringutil.h"
#include "fileutil.h"
#include "tools.h"

//int MENU_ID_STATUS_SHOW_INFO = wxNewId();

BEGIN_EVENT_TABLE(AbstractView, wxPanel)
END_EVENT_TABLE()

AbstractView::AbstractView(wxWindow* parent, wxEvtHandler* evtHandler, ViewType viewType, long style)
        : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style) {
        m_viewId = -1;
        m_title =  wxEmptyString;
        m_fileName = wxEmptyString;
        m_viewType = viewType;
        m_pMenuBar = NULL;
        m_pEvtHandler = evtHandler;
        m_prjTitle=wxEmptyString;
}

AbstractView::~AbstractView() {
}

bool AbstractView::Close(bool isAskForSave,bool default_save)
{
        if(m_viewId==PRJINFO_PID)
                return true;
    return false;
}

void AbstractView::SetFileName(const wxString& fileName){
        m_fileName = FileUtil::NormalizeFileName(fileName);
}

void AbstractView::SetMenuBar(wxMenuBar* pMenuBar) {
        m_pMenuBar = pMenuBar;
}

wxMenuBar* AbstractView::GetMenuBar(void) const {
        return m_pMenuBar;
}

void AbstractView::EnableMenu(int menuId, bool enabled) {
        Tools::EnableMenu(GetMenuBar(), menuId, enabled);
}

wxString AbstractView::GetViewTitle(wxString prefix) {//const {
        if(m_viewType == VIEW_START_HERE) {
                return m_title;
        }
        if(m_viewType == VIEW_PROJECTINFO) {
                return m_title;
        }
        wxString title = prefix;
        m_prjTitle=title;
        title +=  wxT("::");
        switch(m_viewType) {
                case VIEW_UNKNOWN:
                case VIEW_START_HERE:
                        break;
                case VIEW_GL_BUILD:
                        title += wxT("Molecule");
                        break;
                case VIEW_GL_DISPLAY_SURFACE:
                        title += wxT("Density");
                        break;
                case VIEW_GL_DISPLAY_VIBRATION:
                        title += wxT("Vibration");
                        break;
                case VIEW_GL_PREVIEW:
                        break;
                default:
                        break;
        }
        return title;
}

/*void AbstractView::ShowStatus(wxString msg, int pos) {
        wxCommandEvent evt(wxEVT_COMMAND_MENU_SELECTED, MENU_ID_STATUS_SHOW_INFO);
    evt.SetString(msg);
    evt.SetInt(pos);
    m_pEvtHandler->ProcessEvent(evt);
}*/
