/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "envutil.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>

#include <wx/fs_zip.h>
#include <wx/stdpaths.h>
//begin : hurukun : 3/12/2009
#include "envsettings.h"
//end   : hurukun : 3/12/2009
#include "manager.h"
#include "configmanager.h"
#include "appglobals.h"
#if defined (__LINUX__)
#include <wx/utils.h>
#include <wx/socket.h>
#endif

#include "resource.h"
#include "fileutil.h"
#include "stringutil.h"
#include "md5.h"

#define         MAX_MOPAC_PATH 100

namespace platform {
wxString PathSep(void) {
#if defined (__WINDOWS__)
    return wxString(wxT("\\"));
#else
    return wxString(wxT("/"));
#endif
}
}

EnvUtil *EnvUtil::s_instance = NULL;

EnvUtil *EnvUtil::GetInstance(void) {
    if (s_instance == NULL)
        s_instance = new EnvUtil();
    return s_instance;
}

EnvUtil::EnvUtil(void) {
    m_strWorkDir = wxEmptyString;
    m_strBaseDir = wxEmptyString;
    m_strTmpDataDir = wxEmptyString;
    m_strProjectDir = wxEmptyString;
    m_strMolcasDir = wxEmptyString;
    m_strMolcasGVDir = wxEmptyString;
    m_strMasterKey = wxEmptyString;
    m_strMasterId = wxEmptyString;
    m_projectType = wxEmptyString;
    m_platform = wxEmptyString;
}

void EnvUtil::InitEnv(void) {return GetInstance()->InitEnvImpl();}
void EnvUtil::InitEnvPaths(wxString baseDir) {return GetInstance()->InitEnvPathsImpl(baseDir);}
wxString EnvUtil::GetWorkDir(void) {return GetInstance()->GetWorkDirImpl();}
wxString EnvUtil::GetBaseDir(void) {return GetInstance()->GetBaseDirImpl();}
wxString EnvUtil::GetDefWorkPath(void) {return GetInstance()->GetDefWorkPathImpl();}
wxString EnvUtil::GetUserDataDir(void) {return GetInstance()->GetUserDataDirImpl();}
wxString EnvUtil::GetLogDir(void) {return GetInstance()->GetLogDirImpl();}
wxString EnvUtil::GetHistoryDir(void) {return GetInstance()->GetHistoryDirImpl();}
wxString EnvUtil::GetPlatform(void) {return GetInstance()->GetPlatformImpl();}
void EnvUtil::SetProjectDir(const wxString dir) {return GetInstance()->SetProjectDirImpl(dir);}
wxString EnvUtil::GetProjectDir(void) {return GetInstance()->GetProjectDirImpl();}
wxString EnvUtil::GetProgramName(wxString progName) {return GetInstance()->GetProgramNameImpl(progName);}
wxString EnvUtil::GetPrgRootDir(void) {return GetInstance()->GetPrgRootDirImpl();}
wxString EnvUtil::GetPrgResDir(void) {return GetInstance()->GetPrgResDirImpl();}
wxArrayString EnvUtil::GetHWInfo(bool first) {return GetInstance()->GetHWInfoImpl(first);}
wxString EnvUtil::GetLicenseCode(wxString mainten, wxString expdate, wxString hwinfo) {return GetInstance()->GetLicenseCodeImpl(mainten, expdate, hwinfo);}
bool EnvUtil::CheckLicense(void) {return GetInstance()->CheckLicenseImpl();}
void EnvUtil::WriteLicenseCode(bool first) {return GetInstance()->WriteLicenseCodeImpl(first);}
wxString EnvUtil::GetMolcasGVDir(void) {return GetInstance()->GetMolcasGVDirImpl();}
wxString EnvUtil::GetPegamoidDir(void) {return GetInstance()->GetPegamoidDirImpl();}
wxString EnvUtil::GetOthersoftDir(void) {return GetInstance()->GetOthersoftDirImpl();}
wxString EnvUtil::GetMolcasDir(const wxString& molcasCmd) {return GetInstance()->GetMolcasDirImpl(molcasCmd);}
// molcasrc related code:
// wxString EnvUtil::GetMolcasRCFile(void) {return GetInstance()->GetMolcasRCFileImpl();}
wxString EnvUtil::GetMoldenPath(void) {return GetInstance()->GetMoldenPathImpl();}
wxString EnvUtil::GetMopacDir(void) {return GetInstance()->GetMopacDirImpl();}
wxString EnvUtil::GetMasterKey(void) {return GetInstance()->GetMasterKeyImpl();}
wxString EnvUtil::GetMD5String(wxString str) {return GetInstance()->GetMD5StringImpl(str);}
wxString EnvUtil::GetMasterKeyMD5() {return GetInstance()->GetMasterKeyMD5Impl();}
void EnvUtil::SetMasterKey(wxString masterkey) {return GetInstance()->SetMasterKeyImpl(masterkey);}
void EnvUtil::SetMasterId(wxString masterid) {return GetInstance()->SetMasterIdImpl(masterid);}
wxString EnvUtil::GetMasterId(void) {return GetInstance()->GetMasterIdImpl();}
void EnvUtil::SetProjectType(wxString prjtype) {return GetInstance()->SetProjectTypeImpl(prjtype);}
wxString EnvUtil::GetProjectType(void) {return GetInstance()->GetProjectTypeImpl();}
wxString EnvUtil::GetEncryptCode(wxString str) {return GetInstance()->GetEncryptCodeImpl(str);}
wxString EnvUtil::GetDecipherCode(wxString str) {return GetInstance()->GetDecipherCodeImpl(str);}

void EnvUtil::InitEnvImpl(void) {
    setlocale(LC_ALL, "C");
    wxInitAllImageHandlers();
    wxFileSystem::AddHandler(new wxZipFSHandler);
}

//void EnvUtil::InitTempPaths(wxString baseDir) {
//        m_strTmpDataDir = wxStandardPaths::Get().GetUserDataDir();
//        m_strBaseDir = baseDir;
////        wxMessageBox(m_strTmpDataDir);
//        FileUtil::CreateDirectory(GetUserDataDir());
//        FileUtil::CreateDirectory(GetWorkDir());
//        FileUtil::CreateDirectory(GetHistoryDir());
//        FileUtil::CreateDirectory(GetLogDir());
//}
void EnvUtil::InitEnvPathsImpl(wxString baseDir)
{
    wxSetWorkingDirectory(baseDir);
    m_strTmpDataDir = wxStandardPaths::Get().GetUserDataDir();//tempDataDir
    m_strBaseDir = baseDir;//bin dir
    m_strProjectDir = EnvSettingsData::Get()->GetDefWorkPath();
    
    FileUtil::CreateDirectory(GetUserDataDir());
    FileUtil::CreateDirectory(GetWorkDir());
    FileUtil::CreateDirectory(GetHistoryDir());
    FileUtil::CreateDirectory(GetLogDir());
}
wxString EnvUtil::GetWorkDirImpl(void)
{
    return GetUserDataDir() + platform::PathSep() + wxT("work");
}

/**
 * As on Windows, wxGetCwd() will return different dir strings when operating file open dialog
 * so we save the work dir.
 */
wxString EnvUtil::GetBaseDirImpl(void)
{
    if(StringUtil::IsEmptyString(m_strBaseDir)) {
        if(StringUtil::IsEmptyString(m_strWorkDir)) {
            m_strWorkDir = wxGetCwd();
        }
        m_strBaseDir = m_strWorkDir;
    }
    return m_strBaseDir;
}
wxString EnvUtil::GetDefWorkPathImpl(void)
{
    return EnvSettingsData::Get()->GetDefWorkPath();
}
wxString EnvUtil::GetUserDataDirImpl(void)
{
    return m_strTmpDataDir;
}

wxString EnvUtil::GetLogDirImpl(void)
{
    return GetUserDataDir() + platform::PathSep() + wxT("logs");
}

wxString EnvUtil::GetHistoryDirImpl(void)
{
    return GetUserDataDir() + platform::PathSep() + wxT("his");
}

wxString EnvUtil::GetPlatformImpl(void)
{
    if (m_platform.IsEmpty()) {
        wxString symbolsFile = GetBaseDir() + platform::PathSep() + wxT("Symbols");
        if(!wxFileExists(symbolsFile)) {
            return m_platform;
        }
        wxFileInputStream fis(symbolsFile);
        wxTextInputStream tis(fis);
        wxString tmpStr;
        int k, klow, khigh;
        while(fis.IsOk() && !fis.Eof()) {
            tmpStr = tis.ReadLine();
            k = tmpStr.Find(wxT("PLATFORM"));
            if (k != wxNOT_FOUND) {
                klow = tmpStr.Find(wxT('\''), false);
                khigh = tmpStr.Find(wxT('\''), true);
                m_platform = tmpStr.Mid(klow + 1, khigh - klow - 1);
                return m_platform;
            }
        }
    }
    return m_platform;
}

void EnvUtil::SetProjectDirImpl(const wxString dir)
{
    m_strProjectDir = dir;
}
wxString EnvUtil::GetProjectDirImpl(void)
{
    //   if(m_strProjectDir.IsEmpty())
    m_strProjectDir = EnvSettingsData::Get()->GetDefWorkPath();
    return m_strProjectDir;
}
wxString EnvUtil::GetProgramNameImpl(wxString progName)
{
    if(platform::windows) {
        return progName + wxT(".exe");
    } else {
        return progName;
    }
}
wxString EnvUtil::GetPrgRootDirImpl(void)
{
#ifdef __MOLCAS__
    wxString path = GetBaseDir() + platform::PathSep() + wxT("gui");
#else
    wxString path = GetBaseDir(); //.BeforeLast(platform::pathSep.GetChar(0))+wxT("/trunk");
#endif
    return path;
}

wxString EnvUtil::GetPrgResDirImpl(void)
{
    // wxString resDir = wxStandardPaths::Get().GetResourcesDir();
// #if defined(__LINUX__)
    wxString resDir = GetBaseDir() + platform::PathSep() + wxT("share/MolGUI");
// #endif
    return resDir;
}

wxArrayString EnvUtil::GetHWInfoImpl(bool first)
{
    wxString computerName =::wxGetFullHostName();
    wxArrayString hwinfo;
    wxStringTokenizer tzk0(computerName, wxT("."));
    computerName = tzk0.GetNextToken();
    hwinfo.Add(computerName);
    wxString hwinfofile = wxT("hwinfo.tmp");
    wxString exeproc = wxEmptyString;
    wxString oldipaddr = wxEmptyString;
#if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_))
    hwinfofile = m_strBaseDir + wxT("\\hwinfo.tmp");
    exeproc = m_strBaseDir + wxT("\\modules\\qc2.bat \"") + hwinfofile + wxT("\"");
#elif defined (__MAC__)
    //const identifier id = platform_macosx;
    hwinfofile = wxT("hwinfo.tmp");
    exeproc = m_strBaseDir + wxT("SimuPack.app/Contents/MacOS/modules/qc2.sh");
#elif defined (__LINUX__)
    //long processid;
    hwinfofile = wxT("hwinfo.tmp");
    exeproc = m_strBaseDir + wxT("/modules/qc2.sh");
    //processid=wxExecute(exeproc, wxEXEC_SYNC);
#endif
    wxExecute(exeproc, wxEXEC_SYNC);
    // std::cout << "EnvUtil::GetHWInfoImpl: " << exeproc << std::endl;

    if(!wxFileExists(hwinfofile))
        oldipaddr = wxT("");
    else
    {
        wxFileInputStream fis(hwinfofile);
        wxTextInputStream tis(fis);
        wxString strLine = tis.ReadLine();
        strLine = tis.ReadLine();
        wxStringTokenizer tzk(strLine);
        oldipaddr = tzk.GetNextToken();
        oldipaddr = tzk.GetNextToken();
#if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_))
        while(tzk.HasMoreTokens()) {
            oldipaddr = tzk.GetNextToken();
        }
#endif
    }
    hwinfo.Add(oldipaddr);
    //wxRemoveFile(hwinfofile);
    return hwinfo;
}

wxString EnvUtil::GetLicenseCodeImpl(wxString mainten, wxString expdate, wxString hwinfo)
{
    wxString licensecode = wxEmptyString;
    wxChar ch1, ch2;
    int i = 0, ipos = 0, maintenlen = 0, expdatelen = 0, hwinfolen = 0;
    maintenlen = mainten.Len();
    expdatelen = expdate.Len();
    hwinfolen = hwinfo.Len();
    for (i = 0; i < maintenlen; i++) {
        if(ipos < hwinfolen) {
            ch1 = mainten.GetChar(i);
            ch2 = hwinfo.GetChar(i);
            licensecode.Append(char(abs(ch2 - ch1) + 25), 1);
            ipos = i;
        }
    }
    for (i = 0; i < expdatelen && ipos < hwinfolen; i++) {
        if(ipos < hwinfolen) {
            ch1 = expdate.GetChar(i);
            ch2 = hwinfo.GetChar(ipos);
            licensecode.Append(char(abs(ch2 - ch1) + 25), 1);
            ipos++;
        }
    }
    for(i = 0; i < hwinfolen - ipos; i++) {
        ch2 = hwinfo.GetChar(ipos + i);
        licensecode.Append(char(ch2 + 5), 1);

    }
    return licensecode;
}

bool EnvUtil::CheckLicenseImpl(void)
{
    return true; // checking temporarily disabled
    wxString licensefile;
    licensefile = m_strBaseDir + platform::PathSep() + wxT("license.dat");
    if(!wxFileExists(licensefile)) {
        WriteLicenseCode(true);
        wxMessageBox(wxT("Please send me the file 'license.key' in application folder, I'll send you license.dat"));
        return false;
    }
    wxFileInputStream fis(licensefile);
    wxTextInputStream tis(fis);
    wxString maintenance = tis.ReadLine();
    //wxMessageBox(maintenance);
    wxString expdate = tis.ReadLine();
    //        wxMessageBox(expdate);
    wxString regkey = tis.ReadLine();
    //        wxMessageBox(regkey);
    wxArrayString hwinfo = GetHWInfo(false);
    wxString hwinfo0 = hwinfo[0] + wxT("*") + hwinfo[1];
    wxString licensecode = GetLicenseCode(maintenance, expdate, hwinfo0);
    wxString md5code = GetMD5String(expdate);
    wxDateTime dtoday = wxDateTime::Today();
    wxString stoday = dtoday.FormatISODate();
    if(licensecode.Cmp(regkey) == 0 || md5code.Cmp(regkey) == 0) {
        if(stoday.Cmp(expdate) >= 0) {
            wxMessageBox(wxT("Your license is expired!"));
            return false;
        }
        return true;
    }
    else {
        //      wxMessageBox(regkey+wxT(",")+licensecode);
        wxMessageBox(wxT("Your license is invaild!"));
        return false;
    }
}

void EnvUtil::WriteLicenseCodeImpl(bool first) {
    wxString maintenance = wxT("STANDARD");
    wxString expdate = wxT("2010-09-30");
    wxArrayString hwinfo = GetHWInfo(first);
    wxString hwinfo0 = hwinfo[0] + wxT("*") + hwinfo[1];
    wxString licensecode;
    if(!first)
        licensecode = GetLicenseCode(maintenance, expdate, hwinfo0);
    else
        licensecode = hwinfo0;
    wxString licensefile;
    if(!first)
        licensefile = m_strBaseDir + platform::PathSep() + wxT("license.dat");
    else
        licensefile = m_strBaseDir + platform::PathSep() + wxT("license.key");
    wxFileOutputStream fos(licensefile);
    wxTextOutputStream tos(fos);
    tos << maintenance << endl;
    tos << expdate << endl;
    tos << licensecode << endl;
    fos.Close();
}

wxString EnvUtil::GetMolcasGVDirImpl(void) {//not function
    // m_strMolcasGVDir = EnvSettingsData::Get()->GetMolGVPath();
    m_strMolcasGVDir = EnvSettingsData::Get()->GetPegaPath();

    if(m_strMolcasGVDir.IsEmpty() || !wxFileExists(m_strMolcasGVDir))
        m_strMolcasGVDir = wxEmptyString;

    return m_strMolcasGVDir;
}

wxString EnvUtil::GetPegamoidDirImpl(void) {//not function
    // m_strMolcasGVDir = EnvSettingsData::Get()->GetMolGVPath();
    m_strPegamoidDir = EnvSettingsData::Get()->GetPegaPath();

    if(m_strPegamoidDir.IsEmpty() || !wxFileExists(m_strPegamoidDir))
        m_strPegamoidDir = wxEmptyString;

    return m_strPegamoidDir;
}

wxString EnvUtil::GetOthersoftDirImpl(void) {//not function
    // m_strMolcasGVDir = EnvSettingsData::Get()->GetMolGVPath();
    m_strOthersoftDir = EnvSettingsData::Get()->GetOthersoftPath();

    if(m_strOthersoftDir.IsEmpty() || !wxFileExists(m_strOthersoftDir))
        m_strOthersoftDir = wxEmptyString;

    return m_strOthersoftDir;
}

wxString EnvUtil::GetMolcasDirImpl(const wxString& molcasCmd) {
#ifdef __WINDOWS__
    return wxEmptyString;
#endif
    wxString cmd = molcasCmd;
    if(cmd.IsSameAs(m_strMolcasDir) && !m_strMolcasDir.IsEmpty())
        return m_strMolcasDir;

    cmd = cmd + " --version";

    if (m_strMolcasDir.IsEmpty() || !cmd.IsSameAs(m_strMolcasDir)) {
        if(cmd.IsEmpty())
            cmd = wxT("pymolcas --version");
        wxString molcas = wxT("python driver version = py");
        wxArrayString output, errors;
        
        wxExecute(cmd, output, errors,wxEXEC_ASYNC);
        // std::cout << "EnvUtil::GetMolcasDirImpl: " << cmd << std::endl;
        unsigned i, j;
        int index = wxNOT_FOUND;
        if (errors.Count() > 0) {
            wxString        err_msg = errors[0].AfterFirst(' ');
            err_msg = errors[0].BeforeFirst(' ') + wxT(" ") + err_msg.BeforeFirst(' ');
            //wxMessageBox(err_msg);
            if(!err_msg.IsSameAs(wxT("License is")))
                return wxEmptyString;
            //        wxMessageBox(errors[0]);
            //        wxMessageBox(wxT("Sorry, cannot find MOLCAS!"));
            //        return wxEmptyString;
        }
        for (i = 0; i < output.Count(); i++) {
            //wxLogError(output[i]);
            index = output[i].Find(molcas);
            if (index != wxNOT_FOUND) {
                molcas = output[i].Mid(index + molcas.Len());
                j = 0;
                while (j < molcas.Len() && molcas[j] == wxChar(' ')) {
                    j++;
                }
                if (j != 0) molcas = molcas.Mid(j);
                j = molcas.Len() - 1;
                while (j >= 0 && molcas[j] == wxChar(' ')) {
                    j--;
                }
                if (j != molcas.Len() - 1) molcas = molcas.Mid(0, j + 1);
                m_strMolcasDir = molcas;
                break;
            }
        }
    }
    return m_strMolcasDir;
}

wxString EnvUtil::GetMoldenPathImpl(void)
{
    return EnvSettingsData::Get()->GetMoldenPath();
}
//begin ; hurukun : 10/11/2009
wxString EnvUtil::GetMopacDirImpl(void) {
    wxString mopacpath = EnvSettingsData::Get()->GetMopacPath();

    if(mopacpath.IsEmpty())
    {
        //        ::wxMessageBox(wxT("Can't get MOPAC path!"),wxT("warning"));
        return wxEmptyString;
    }
    if(!::wxFileExists(mopacpath))
        return wxEmptyString;
    else
        return mopacpath;

    wxArrayString output, errors;
    wxExecute(mopacpath, output, errors, wxEXEC_ASYNC);
    // std::cout << "EnvUtil::GetMopacDirImpl: " << mopacpath << std::endl;
    if (errors.Count() > 0) {
        //        ::wxMessageBox(wxT("Can't execute MOPAC with the given path!"));
        return wxEmptyString;
    }
    return mopacpath;
}

//end   : hurukun : 10/11/2009
wxString EnvUtil::GetMasterKeyImpl(void) {
    return m_strMasterKey;
}

wxString EnvUtil::GetMD5StringImpl(wxString str) {
    char *input = NULL;
    StringUtil::String2CharPointer(&input, str);
    md5_state_t state;
    md5_byte_t digest[16];
    char hex_output[16 * 2 + 1];
    int di;

    md5_init(&state);
    md5_append(&state, (const md5_byte_t *)input, strlen(input));
    md5_finish(&state, digest);
    for (di = 0; di < 16; ++di)
        sprintf(hex_output + di * 2, "%02x", digest[di]);
    //puts(hex_output);

    wxString md5str = StringUtil::CharPointer2String(hex_output);
    return md5str;
}

wxString EnvUtil::GetMasterKeyMD5Impl(void) {
    char *input = NULL;
    wxString strmd5 = m_strMasterId + m_strMasterKey;
    StringUtil::String2CharPointer(&input, strmd5);
    md5_state_t state;
    md5_byte_t digest[16];
    char hex_output[16 * 2 + 1];
    int di;

    md5_init(&state);
    md5_append(&state, (const md5_byte_t *)input, strlen(input));
    md5_finish(&state, digest);
    for (di = 0; di < 16; ++di)
        sprintf(hex_output + di * 2, "%02x", digest[di]);
    //puts(hex_output);
    wxString returnstr = StringUtil::CharPointer2String(hex_output);
    returnstr = returnstr.MakeUpper();
    return returnstr;
}

void EnvUtil::SetMasterKeyImpl(wxString masterkey) {
    m_strMasterKey = masterkey;
}
void EnvUtil::SetMasterIdImpl(wxString masterid) {
    m_strMasterId = masterid;
}

wxString EnvUtil::GetMasterIdImpl(void) {
    return m_strMasterId;
}

void EnvUtil::SetProjectTypeImpl(wxString prjtype) {
    m_projectType = prjtype;
}

wxString EnvUtil::GetProjectTypeImpl(void) {
    return m_projectType;
}


wxString EnvUtil::GetEncryptCodeImpl(wxString str) {
    /*   IceKey *ik=new IceKey(0);
       unsigned char *key1=NULL;
       StringUtil::String2UnSignedCharPointer(&key1,m_strMasterKey);
       ik->set(key1);
       fprintf(stderr,(char*)key1);
       unsigned char *str1;
       StringUtil::String2UnSignedCharPointer(&str1,str);
       unsigned char out[50];
       memset(out,NULL,50);
       ik->encrypt(str1,out);
       fprintf(stderr,(char*)out);
       fprintf(stderr,"end");
       //char tmpstr[128];
       //memset(tmpstr,NULL,128);
       int i=0;

       for(i=0;i<str.Length();++i)returncode.Append((wxChar)out[i]);;
       //char *tmp=(char*)out;

           //StringUtil::CharPointer2String(tmp);
           //wxMessageBox(returncode);
    */
    wxString returncode = wxEmptyString;
    wxString masterkey = GetMasterKeyMD5();
    wxChar ch1, ch2, ch0;
    wxString tmpstr = wxEmptyString;
    int i = 0, ipos = 0, strlen = 0, masterkeylen = 0;
    strlen = str.Len();
    masterkeylen = masterkey.Len();
    for (i = 0; i < strlen; i++) {
        if(i < masterkeylen) {
            ch1 = str.GetChar(i);
            ch2 = masterkey.GetChar(ipos);
            ch0 = ch1 ^ ch2;
            tmpstr = wxString::Format(wxT("%02x"), ch0);
            returncode.Append(tmpstr);
            ipos++;
        } else {
            ch1 = str.GetChar(i);
            ch2 = masterkey.GetChar(ipos);
            ch0 = ch1 ^ ch2;
            tmpstr = wxString::Format(wxT("%02x"), ch0);
            returncode.Append(tmpstr);
            ipos--;
        }
    }

    return returncode;
}

wxString EnvUtil::GetDecipherCodeImpl(wxString str) {
    /*    IceKey *ik=new IceKey(0);
        unsigned char *key1=NULL;
        StringUtil::String2UnSignedCharPointer(&key1,m_strMasterKey);
        ik->set(key1);
        fprintf(stderr,(char*)key1);
        unsigned char *str1;
       // wxMessageBox(str);
        StringUtil::String2UnSignedCharPointer(&str1,str);
        unsigned char out[50];
        memset(out,NULL,50);
        ik->decrypt(str1,out);
        //fprintf(stderr,(char*)out);
        fprintf(stderr,"end");
            wxString returncode=wxEmptyString;
        returncode=StringUtil::CharPointer2String((char*)out);
    */
    wxString returncode = wxEmptyString;
    wxString masterkey = GetMasterKeyMD5();
    wxString strtmp = wxEmptyString;
    wxChar ch1, ch2;
    int i = 0, ipos = 0, strlen = 0, masterkeylen = 0;
    strlen = str.Len();
    masterkeylen = masterkey.Len();
    for (i = 0; i < strlen; i += 2) {
        if(i < masterkeylen) {
            strtmp = str.Mid(i, 2);
            long ltmp;
            strtmp.ToLong(&ltmp, 16);
            //strtmp=wxString::Format(wxT("%c"),ltmp);

            ch1 = ltmp; //strtmp.GetChar(0);
            ch2 = masterkey.GetChar(ipos);
            returncode.Append(char(ch1 ^ ch2), 1);
            ipos++;
        } else {
            ch1 = str.GetChar(i);
            ch2 = masterkey.GetChar(ipos);
            returncode.Append(char(ch1 ^ ch2), 1);
            ipos--;
        }
    }

    return returncode;

}
