/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "fileutil.h"

#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/textfile.h>
#include <wx/tokenzr.h>
#include <wx/txtstrm.h>
#include <wx/wfstream.h>

#include "envutil.h"
#include "stringutil.h"
#include "tools.h"
#include "resource.h"
#include "simuprjdef.h"

#include <wx/arrimpl.cpp>

wxString FileUtil::AppendFileExt(wxString fileName, int filterIndex, const wxString aryExts[])
{
    wxString appendedFile = wxEmptyString;
    if(!StringUtil::IsEmptyString(fileName)) {
        appendedFile = fileName;
        if(!(fileName.Lower().EndsWith(aryExts[filterIndex]))) {
            appendedFile += aryExts[filterIndex];
        }
    }
    return appendedFile;
}
wxString FileUtil::BuildFullPath(const wxString &homeDir, const wxString &dirName, const wxString &fileName)
{
    if(fileName.IsEmpty()) {
        return homeDir + platform::PathSep() + dirName;
    } else {
        return homeDir + platform::PathSep() + dirName + platform::PathSep() + fileName;
    }
    return wxEmptyString;
}
wxString FileUtil::ChangeFileExt(wxString fileName, wxString ext)
{
    wxString suffix = GetSuffix(fileName);
    fileName = fileName.Left(fileName.Len() - suffix.Len() - 1);
    //wxMessageBox(fileName);
    fileName = fileName + ext;
    return fileName;
    //        wxFileName fName(NormalizeFileName(fileName));
    //        return NormalizeFileName(fName.GetPath() + platform::PathSep() + fName.GetName()+ ext);
}
bool        FileUtil::CopyDirectory(wxString oldPath, wxString newPath, int flag) //flag=wxDIR_DEFAULT
{
    if(!wxDirExists(oldPath))
        return false;
    if(!wxDirExists(newPath))
        if(!FileUtil::CreateDirectory(newPath))
            return false;
    wxString tmpOld, tmpNew, filename;

    wxDir   oldDir(oldPath);
    bool cont = oldDir.GetFirst(&filename, wxEmptyString, flag);
    while ( cont ) {
        if(filename.IsSameAs(wxT(".")) || filename.IsSameAs(wxT(".."))) {
            cont = oldDir.GetNext(&filename);
            continue;
        }
        tmpOld = oldPath + platform::PathSep() + filename;
        if(wxFileExists(tmpOld)) { //file has the same name with part of the folder
            wxString tmpFileName, fileSuffix;
            fileSuffix = GetSuffix(filename);
            int        fileNameLen = filename.Len();
            tmpFileName = filename.Left(fileNameLen - fileSuffix.Len() - 1);
            if(tmpFileName.IsSameAs(oldPath.AfterLast(platform::PathSep().GetChar(0)).BeforeFirst(wxCHAR_FN_SEP)))//change the file name if it related to the folder name.
            {
                tmpFileName = newPath.AfterLast(platform::PathSep().GetChar(0));
                tmpFileName = tmpFileName.BeforeFirst(wxCHAR_FN_SEP); //@
            }
            tmpNew = newPath + platform::PathSep() + tmpFileName + wxT(".") + fileSuffix;
            wxCopyFile(tmpOld, tmpNew);
        } 
        else if(wxDirExists(tmpOld))
        {
            wxFileName fullFileName(newPath);
            filename = fullFileName.GetName();
            tmpNew = newPath + platform::PathSep() + filename;
            FileUtil::CopyDirectory(tmpOld, tmpNew);
        }

        cont = oldDir.GetNext(&filename);
    }
    return true;
}
bool        FileUtil::CreateDirectory(wxString dirPath)
{
    wxString path = NormalizeFileName(dirPath);
    wxString currDir = wxEmptyString;
    wxString token = wxEmptyString;

    if(wxFileName::DirExists(path)) {
        return true;
    }

    path.Replace(wxT("\\"), wxT("/"));
    wxStringTokenizer tkz(path, wxT("/"));

    bool isFirst = true;
    while (tkz.HasMoreTokens()) {
        token = tkz.GetNextToken();
        currDir += token + wxT("/");

        if(isFirst) {
            isFirst = false;
        } else {
            if(!wxFileName::DirExists(currDir)) {
                if(!wxMkdir(currDir, 0755)) {
                    return false;
                }
            }
        }
    }
    return true;
}
wxString FileUtil::CheckFile(const wxString &dirName, const wxString &fileName, bool needReport)
{

    wxString fileFullName = BuildFullPath(wxT(""), dirName, fileName);

    if(!wxFileName::FileExists(fileFullName)) {
        if(needReport) {
            Tools::ShowError(wxT("Please check file: ") + fileFullName);
        }
        return wxEmptyString;
    }
    return fileFullName;
}
wxString FileUtil::GetDirectoryName(wxString dirPath)
{
    wxString path = NormalizeFileName(dirPath);
    wxString token = wxEmptyString;

    path.Replace(wxT("\\"), wxT("/"));
    wxStringTokenizer tkz(path, wxT("/"));

    while (tkz.HasMoreTokens()) {
        token = tkz.GetNextToken();
    }
    return token;
}

wxString FileUtil::GetMolcasFileType(wxString &fileName)
{
    wxFileName fn(fileName);
    wxString fullName = fn.GetFullName();
    wxString suffix = fn.GetExt();
    if(suffix.IsNumber())
    {
        suffix = fullName.BeforeLast('.').AfterLast('.');
    }
    return suffix;
}

wxString FileUtil::GetSuffix(wxString &fileName)
{
    //wxMessageBox(fileName);
    wxString        suffix = wxEmptyString;
    if(fileName.Right(8).IsSameAs(wxT(".Opt.xyz"))) {
        return wxT("Opt.xyz");
    }
    if(fileName.Right(10).IsSameAs(wxT(".geo.molden"))) {
        return wxT("geo.molden");
    }
    if(fileName.Len() > 12 && fileName.Right(12).IsSameAs(wxT(".xmldump.xml"))) {
        return wxT("xmldump.xml");
    }

    if(fileName.Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX) ||
            fileName.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX) ||
            fileName.Right(MSD_FILE_SUFFIX_LEN).IsSameAs(wxMSD_FILE_SUFFIX) ||
            fileName.Right(MCD_FILE_SUFFIX_LEN).IsSameAs(wxMCD_FILE_SUFFIX) ||
            fileName.Right(MINPUT_FILE_SUFFIX_LEN).IsSameAs(wxMINPUT_FILE_SUFFIX) ||
            fileName.Right(MOUTPUT_FILE_SUFFIX_LEN).IsSameAs(wxMOUTPUT_FILE_SUFFIX) ||
            fileName.Right(ERROR_FILE_SUFFIX_LEN).IsSameAs(wxERROR_FILE_SUFFIX)) {
        return fileName.AfterLast(wxT('.'));
        //        }else if(fileName.Right(DUMP_FILE_SUFFIX_LEN).IsSameAs(wxDUMP_FILE_SUFFIX )){
        //                return fileName.Right(DUMP_FILE_SUFFIX_LEN-1);
    } else if(fileName.AfterLast(wxT('.')).IsSameAs(wxT("RunFile")) ||
              fileName.AfterLast(wxT('.')).IsSameAs(wxT("xml")) ||
              fileName.AfterLast(wxT('.')).IsSameAs(wxT("log")) ||
              fileName.AfterLast(wxT('.')).IsSameAs(wxT("err")) ||
              fileName.AfterLast(wxT('.')).IsSameAs(wxT("status"))) {
        return fileName.AfterLast(wxT('.'));
    }

    wxString        dbFilePath = EnvUtil::GetPrgResDir();
#ifdef __MOLCAS
    dbFilePath = dbFilePath + wxMOLCAS_SUFFIX_FILE_PATH;
#else
    dbFilePath = dbFilePath + wxTRUNK_SUFFIX_FILE_PATH;
#endif
    //wxMessageBox(dbFilePath);
    wxFileInputStream        ifo(dbFilePath);//open the file for reading
    if(!ifo.IsOk())//fail to open the file
        return suffix;
    wxTextInputStream        iftext(ifo);
    wxString        tmpStr;
    while(true) {
        iftext >> tmpStr;
        if(tmpStr.IsEmpty())
            break;
        tmpStr = tmpStr.AfterLast(wxCHAR_SAVEDB_PRE).AfterFirst(wxT('.'));
        //wxMessageBox(tmpStr);
        unsigned int sufLen = tmpStr.Len();
        if(sufLen == 0 || fileName.Len() < sufLen)
            continue;
        if(fileName.Right(sufLen).IsSameAs(tmpStr)) {
            suffix = tmpStr;
            break;
        }
    }
    //wxMessageBox(suffix);
    return suffix;
}
wxArrayString FileUtil::GetFileDispType(wxString fileName)
{
    //wxMessageBox(fileName);
    wxArrayString        dispType;
    if(fileName.Right(PG_FILE_SUFFIX_LEN).IsSameAs(wxPG_FILE_SUFFIX) ||
            fileName.Right(PRJ_FILE_SUFFIX_LEN).IsSameAs(wxPRJ_FILE_SUFFIX) ||
            fileName.Right(MSD_FILE_SUFFIX_LEN).IsSameAs(wxMSD_FILE_SUFFIX) ||
            fileName.Right(MCD_FILE_SUFFIX_LEN).IsSameAs(wxMCD_FILE_SUFFIX) ||
            fileName.Right(MINPUT_FILE_SUFFIX_LEN).IsSameAs(wxMINPUT_FILE_SUFFIX) ||
            fileName.Right(MOUTPUT_FILE_SUFFIX_LEN).IsSameAs(wxMOUTPUT_FILE_SUFFIX) ||
            fileName.Right(ERROR_FILE_SUFFIX_LEN).IsSameAs(wxERROR_FILE_SUFFIX) ||
            fileName.Right(DUMP_FILE_SUFFIX_LEN).IsSameAs(wxDUMP_FILE_SUFFIX )) {

        dispType.Add(wxDISP_TYPE_TEXT);
        return dispType;
    }

    wxString        dbFilePath = EnvUtil::GetPrgResDir();
#ifdef __MOLCAS
    dbFilePath = dbFilePath + wxMOLCAS_SUFFIX_FILE_PATH;
#else
    dbFilePath = dbFilePath + wxTRUNK_SUFFIX_FILE_PATH;
#endif
    //wxMessageBox(dbFilePath);
    wxFileInputStream        ifo(dbFilePath);//open the file for reading
    if(!ifo.IsOk())//fail to open the file
        return dispType;
    wxTextInputStream        iftext(ifo);
    wxString        tmpStr, tmpCurType;
    while(true) {
        iftext >> tmpStr;
        if(tmpStr.IsEmpty())
            break;
        if(tmpStr.IsSameAs(wxDISP_TYPE_GV)) {
            tmpCurType = wxDISP_TYPE_GV;
            continue;
        } else if(tmpStr.IsSameAs(wxDISP_TYPE_TEXT)) {
            tmpCurType = wxDISP_TYPE_TEXT;
            continue;
        }
        //wxMessageBox(tmpStr);
        tmpStr = tmpStr.AfterLast(wxCHAR_SAVEDB_PRE).AfterFirst(wxT('.'));
        unsigned int sufLen = tmpStr.Len();
        if(sufLen == 0 || fileName.Len() < sufLen)
            continue;
        if(fileName.Right(sufLen).IsSameAs(tmpStr)) {
            dispType.Add(tmpCurType);
        }
    }
    return dispType;
}
wxArrayString FileUtil::GetFileBySuffix(wxString dir, wxString suffix)
{
    wxArrayString fileList;
    fileList.Clear();

    if(!wxDirExists(dir))
        return fileList;

    wxString suf = suffix;
    int sufLen = suf.Len();

    wxString filename = wxEmptyString;
    wxDir   theDir(dir);
    int flag = wxDIR_FILES;
    bool cont = theDir.GetFirst(&filename, wxEmptyString, flag);
    while ( cont ) {
        if(filename.IsSameAs(wxT(".")) || filename.IsSameAs(wxT("..")))
            continue;
        if(filename.Right(sufLen).IsSameAs(suf))//check sip file
            fileList.Add(filename);

        cont = theDir.GetNext(&filename);
    }
    return fileList;
}
wxString FileUtil::GetPath(wxString fileName)
{
    wxFileName fName(NormalizeFileName(fileName));
    return fName.GetPath();
}
bool        FileUtil::IsRelativePath(wxString path)
{
    if (path.Find(wxChar(':')) == wxNOT_FOUND && !(StringUtil::StartsWith(path, wxT("/"), true))) {
        return true;
    }
    return false;
}
bool        FileUtil::IsEmptyDirectory(wxString fileDir)
{
    if(!wxDir::Exists(fileDir))
        return true;
    wxDir dir(fileDir);
    wxString fileName;
    return !dir.GetFirst(&fileName, wxT("*"), wxDIR_FILES | wxDIR_DIRS);
}
wxArrayString FileUtil::LoadStringLineFile(const wxString &fileName)
{
    wxArrayString lines;
    wxTextFile txtFile;
    if(txtFile.Open(fileName)) {
        for(unsigned int i = 0; i < txtFile.GetLineCount(); i++) {
            lines.Add(txtFile.GetLine(i));
        }
        txtFile.Close();
    }
    return lines;
}
wxString FileUtil::LoadStringLineFileStr(const wxString &fileName)
{
    wxString strFile = wxEmptyString;
    wxTextFile txtFile;
    if (txtFile.Open(fileName)) {
        for (unsigned int i = 0; i < txtFile.GetLineCount(); i++) {
            strFile += txtFile.GetLine(i) + wxT("\n");
        }
        txtFile.Close();
    }
    return strFile;
}
void        FileUtil::ListDirectoryFiles(wxString fileDir, bool isRecursive, bool isOnlyName, wxArrayString &fileArrays)
{
    if(!wxDir::Exists(fileDir))
        return;
    wxDir dir(fileDir);
    wxString fileName;
    bool cont = dir.GetFirst(&fileName, wxT("*"), wxDIR_FILES | wxDIR_DIRS);
    while(cont) {
        fileName = fileDir + wxFileName::GetPathSeparator() + fileName;
        if(isRecursive && wxFileName::DirExists(fileName)) {
            ListDirectoryFiles(fileName, isRecursive, isOnlyName, fileArrays);
        } else if(wxFileName::FileExists(fileName)) {
            if(isOnlyName) {
                fileArrays.Add(wxFileName(fileName).GetName());
            } else {
                fileArrays.Add(fileName);
            }
        }
        cont = dir.GetNext(&fileName);
    }
}
wxString FileUtil::NormalizeFileName(const wxString fileName)
{
    wxFileName fName(fileName);
    fName.Normalize(wxPATH_NORM_LONG | wxPATH_NORM_DOTS | wxPATH_NORM_TILDE | wxPATH_NORM_ABSOLUTE);
    return fName.GetFullPath();
}
void        FileUtil::OutputStringLineFile(const wxString &fileName, wxArrayString values)
{
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
    OutputStringLineFile(tos, values);
}
void        FileUtil::OutputStringLineFile(wxTextOutputStream &tos, wxArrayString values)
{
    for(unsigned i = 0; i < values.GetCount(); i++) {
        tos << values[i] << endl;
    }
}
bool        FileUtil::RemoveDirectory(wxString fileDir, bool removeSelf)
{
    if(!wxDir::Exists(fileDir))
        return true;
    {
        wxDir dir(fileDir);
        wxString fileName;
        bool cont = dir.GetFirst(&fileName, wxT("*"), wxDIR_FILES | wxDIR_DIRS);
        while(cont) {
            fileName = fileDir + wxFileName::GetPathSeparator() + fileName;
            if(wxFileName::DirExists(fileName)) {
                if(RemoveDirectory(fileName, true)) {
                    if(removeSelf) {
                        if(wxDir::Exists(fileName))
                            wxRmdir(fileName);
                    }
                } else {
                    return false;
                }
            } else if(wxFileName::FileExists(fileName)) {
                if(!wxRemoveFile(fileName)) {
                    return false;
                }
            }
            cont = dir.GetNext(&fileName);
        }
    }
    if(removeSelf && wxDir::Exists(fileDir)) {
        wxRmdir(fileDir);
    }
    return true;
}
//.........................................................................//
wxString FileUtil::GetElemFromFile(const wxString &fileName, const wxString &elemName) 
{
    StringHash elems = ReadElemsFromFile(fileName);
     
    return elems[elemName];
}
StringHash FileUtil::ReadElemsFromFile(const wxString &fileName)
{
    StringHash elems;
    wxFileInputStream ifs(fileName);//open the file for reading
    if(!ifs.IsOk()) 
    { 
        //fail to open the file
        wxMessageBox(wxT("Fail to open the file.") + fileName);
        return elems;
    }
    wxTextInputStream iftext(ifs);
    wxString buffer = iftext.ReadLine();
    while(!buffer.IsEmpty()) {
        wxArrayString key_value = StringUtil::Split(buffer,wxT("="));
        // 0 is the key, 1 is the value
        if(key_value.GetCount() == 2)
        {
            elems[key_value[0]] = key_value[1];
        }
        buffer = iftext.ReadLine();
    }
    return elems;
}
bool FileUtil::WriteElemsToFile(const wxString &fileName, const StringHash &elems)
{
    wxFileOutputStream ofs(fileName);//open the file for writing
    if(!ofs.IsOk()) { //fail to open the file
        wxMessageBox(wxT("Fail to open the file.") + fileName);
        return false;
    }
    wxTextOutputStream oftext(ofs);
    StringHash::const_iterator it;
    for(it = elems.begin(); it != elems.end(); it++) {
        oftext << it->first << wxT("=") << it->second << endl;
    }
    return true;
}
