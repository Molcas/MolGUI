/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

/*
 * HttpPost.cpp
 *
 *  Created on: 2010-8-27
 *      Author: allan
 */

#include "httppost.h"
#include "jobmonitorpnl.h"
#include "stringutil.h"
#include <curl/curl.h>
#include "envutil.h"
#include "dlgfilelist.h"
#include "manager.h"
#include "messageevent.h"

char *postbuf;
char *checkbuf;

DownLoadThread::DownLoadThread(HttpPost *p) {
        post = p;
}

void * DownLoadThread::Entry()  {
        post->DoDownLoad();
        return 0;
}




void process_post(void *buffer, size_t size, size_t nmemb, void *user_p) {
        postbuf = (char *) buffer;
        FILE *fptr = (FILE*) user_p;
        fwrite(buffer, size, nmemb, fptr);
}

void process_check(void *buffer, size_t size, size_t nmemb, void *user_p) {
        checkbuf = (char *) buffer;
        FILE *fptr = (FILE*) user_p;
        fwrite(buffer, size, nmemb, fptr);
}

HttpPost::HttpPost(wxString server, wxString id, wxString ticket ) {

        this->server = std::string(server.mb_str());
        this->id = std::string(id.mb_str());
        this->ticket = std::string(ticket.mb_str());
        this->m_pJobMonitorPnl=NULL;
    m_busy=-1;
        thread = new DownLoadThread(this);
        thread->Create();
}

void HttpPost::AddFile(wxString file) {
    char * cfile;
    StringUtil::String2CharPointer(&cfile,file);

        filesUpload.push_back(cfile);
}

void HttpPost::SetBusy(int isbusy) {
m_busy=isbusy;
}

int HttpPost::IsBusy() {
    return m_busy;
}
void HttpPost::SetJobmonitorpnl(JobMonitorPnl * pJobMonitorPnl)
{
        m_pJobMonitorPnl=pJobMonitorPnl;
}
bool HttpPost::Post() {

        CURL *curl = NULL;
        CURLcode res;

        FILE * flagfp = fopen("flag", "w");
        filesUpload.push_back("flag");
        fclose(flagfp);

        struct curl_httppost *post = NULL;
        struct curl_httppost *last = NULL;
        //struct curl_slist *headerlist = NULL;

        if (curl_formadd(&post, &last, CURLFORM_COPYNAME, "ID",
                        CURLFORM_COPYCONTENTS, id.c_str(), CURLFORM_END) != 0) {
                fprintf(stderr, "ID error.\n");
                curl_formfree(post);
                return false;
        }

        for (unsigned i=0; i<filesUpload.size(); ++i) {
                char c = '0' + i;
                string f = "fileInput";
                f += c;
                if (curl_formadd(&post, &last, CURLFORM_COPYNAME, f.c_str(),
                                CURLFORM_FILE, filesUpload[i].c_str(), CURLFORM_END) != 0) {
                        fprintf(stderr, "curl_formadd error.\n");
                        curl_formfree(post);
                        return false;
                }
        }

        /* Fill in the submit field too, even if this is rarely needed */
        curl_formadd(&post, &last, CURLFORM_COPYNAME, "submit",
                        CURLFORM_COPYCONTENTS, "Submit!", CURLFORM_END);

        //curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();
        if (curl == NULL) {
                fprintf(stderr, "curl_easy_init() error.\n");
                curl_formfree(post);
                return false;
        }

        string posturl = server + "cgi-bin/molcas_new_ticket.plx";

        curl_easy_setopt(curl, CURLOPT_HEADER, 0);
        curl_easy_setopt(curl, CURLOPT_URL, posturl.c_str() ); /*Set URL*/
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

        int timeout = 5;

        curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 1);


        FILE *fp;

        if ((fp = fopen("temppost", "w")) == NULL)
                return false;

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &process_post);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);

        if (res != CURLE_OK) {
                fprintf(stderr, "curl_easy_perform[%d] error.\n", res);
                curl_formfree(post);
                return false;
        }

        curl_easy_cleanup(curl);

        curl_formfree(post);

        wxRemoveFile(wxT("flag"));

        /////////////////////
        return true;
}

wxString HttpPost::FindTicket() {

        SetPostData(postbuf);

        string s(postdata);
    wxString strticket=wxEmptyString;
        if (string::npos != s.find("job=")) {
                ticket = s.substr(s.find("job=") + 4, s.find_first_of('>', s.find("job="))-(s.find("job=")+4));
            wxString tmpstr(ticket.c_str(), wxConvUTF8);
        strticket=tmpstr;
       }
        return strticket;
}

void HttpPost::SetServer(wxString server) {
        this->server = std::string(server.mb_str());
}


void HttpPost::SetId(wxString id) {
        this->id = std::string(id.mb_str());
}


void HttpPost::SetTicket(wxString ticket) {
        this->ticket = std::string(ticket.mb_str());
}

void HttpPost::Check() {

        if (ticket.empty()) {
                FindTicket();
        }

        CURL *curl;

        curl = curl_easy_init();

//        string s = "&submit=Submit!&ticket=" + ticket;

        if (curl)        {

//                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, s.c_str()); //

                string checkurl = server + "cgi-bin/molcas_check_ticket.plx?job=" + ticket;
        curl_easy_setopt(curl,CURLOPT_FRESH_CONNECT,1);
                curl_easy_setopt(curl, CURLOPT_URL, checkurl.c_str()); //

                FILE *fp;

                fp = fopen("tempcheck", "w");

                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &process_check);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

                curl_easy_perform(curl);

                curl_easy_cleanup(curl);

        }
}

wxString HttpPost::GetStatus() {

        Check();

        SetCheckData(checkbuf);

        string s(checkdata);
    wxString returnstr;
        if (string::npos != s.find("your job is waiting to start")) {
                returnstr=wxT("Queue Wait");
        } else if (string::npos != s.find("your job is running. Come back later")){
                returnstr=wxT("Running");
        } else {
                returnstr=wxT("Finished");
        }
    return returnstr;
}


void HttpPost::DownLoad(void) {
        if (GetStatus().Cmp(wxT("Finished"))!=0) {
                return ;
        }

        thread->Run();
}

void HttpPost::GetDownList(wxString path) {
    downpath = std::string(path.mb_str());
        string s(checkdata);
        string name;

        string lfile;
        string rfile;
        int pos = 0;
    m_arylfiles.Empty();
    m_aryrfiles.Empty();
//    wxString sf0(s.c_str(), wxConvUTF8);
//        m_pJobMonitorPnl->pPnlOutput->AppendOutput(sf0);
        while (string::npos != s.find("<a href=/", pos)) {
        //        wxMessageBox(wxT("begin"));
        pos = s.find("<a href=/", pos) + 9;
                name = s.substr(pos, s.find(">", pos)-pos);

                if (name.substr(name.length()-4,4) == "flag") {
                        continue;
                }
        //        wxMessageBox(wxT("after flag"));
        rfile=server + name;
        wxString sf1(rfile.c_str(), wxConvUTF8);
        if((sf1.Right(6)).Cmp(wxT(".input"))==0)continue;
        m_aryrfiles.Add(sf1);

//        m_pJobMonitorPnl->pPnlOutput->AppendOutput(sf1);
     //   name=name.substr(name.find_last_of(platform::PathSep().GetChar(0))+1, name.size() - name.find_last_of(platform::PathSep().GetChar(0)));
        name=name.substr(name.find_last_of('/')+1, name.size() - name.find_last_of('/'));
//        m_pJobMonitorPnl->pPnlOutput->AppendOutput(sf2);
                lfile=downpath + name;
                wxString sf2(lfile.c_str(), wxConvUTF8);
                m_arylfiles.Add(sf2);
        }

    m_pJobMonitorPnl->SetAryDownlist(m_arylfiles,m_aryrfiles);
}

void HttpPost::DoDownLoad() {
    string tmpstr;
    tmpstr=downpath + "flag";
    wxString tmp1(tmpstr.c_str(), wxConvUTF8);
        m_arylfiles.Add(tmp1);
        tmpstr=server + "flag";
    wxString tmp2(tmpstr.c_str(), wxConvUTF8);
        m_aryrfiles.Add(tmp2);

        for (unsigned i=0; i<m_arylfiles.GetCount(); ++i) {
        string name=std::string(m_arylfiles[i].mb_str());
        string file=std::string(m_aryrfiles[i].mb_str());
    wxMessageUpdateEvent event(wxEVT_COMMAND_MESSAGE_CHANGED, ID_MESSAGE_CHANGE );
    event.SetString(m_arylfiles[i]); // that's all
    wxPostEvent( m_pJobMonitorPnl, event );

  //      m_pJobMonitorPnl->pPnlOutput->AppendOutput(m_arylfiles[i]);
    //    wxMessageBox(sf3);
                GetRemoteFile(name, file);
        //        wxMessageBox(wxT("after savefile"));
        }
        wxString msg=wxT("All files download from web!");
    wxMessageUpdateEvent event(wxEVT_COMMAND_MESSAGE_CHANGED, ID_MESSAGE_CHANGE );
    event.SetString(msg); // that's all
    wxPostEvent( m_pJobMonitorPnl, event );
        //filesDownLoad = names;
}

bool HttpPost::IsDownLoadFinished() {
        string s = downpath + "flag";
    wxString sf(s.c_str(), wxConvUTF8);
        if (wxFileExists(sf)){
                return true;
        }
        return false;
}

bool HttpPost::IsUploadFinished() {
        if (!wxFileExists(wxT("flag"))){
                return false;
        }
        return true;
}


#include <stdio.h>
#include <string.h>
#ifndef WIN32
#include <sys/time.h>
#endif
#include <stdlib.h>
#include <errno.h>

#include <curl/curl.h>

#include <string>
#include <vector>
using namespace std;

enum fcurl_type_e {
        CFTYPE_NONE = 0, CFTYPE_FILE = 1, CFTYPE_CURL = 2
};

struct fcurl_data {
        enum fcurl_type_e type; /* type of handle */
        union {
                CURL *curl;
                FILE *file;
        } handle; /* handle */

        char *buffer; /* buffer to store cached data*/
        int buffer_len; /* currently allocated buffers length */
        int buffer_pos; /* end of data in buffer*/
        int still_running; /* Is background url fetch still in progress */
};

typedef struct fcurl_data URL_FILE;

/* exported functions */
URL_FILE *url_fopen(const char *url, const char *operation);
int url_fclose(URL_FILE *file);
int url_feof(URL_FILE *file);
char * url_fgets(char *ptr, int size, URL_FILE *file);

/* we use a global one for convenience */
CURLM *multi_handle;

/* curl calls this routine to get more data */
static size_t write_callback(char *buffer, size_t size, size_t nitems,
                void *userp) {
        char *newbuff;
        unsigned int rembuff;

        URL_FILE *url = (URL_FILE *) userp;
        size *= nitems;

        rembuff = url->buffer_len - url->buffer_pos; /* remaining space in buffer */

        if (size > rembuff) {
                /* not enough space in buffer */
                newbuff = (char*)realloc(url->buffer, url->buffer_len + (size - rembuff));
                if (newbuff == NULL) {
                        fprintf(stderr,"callback buffer grow failed\n");
                        size = rembuff;
                } else {
                        /* realloc suceeded increase buffer size*/
                        url->buffer_len += size - rembuff;
                        url->buffer = newbuff;
                }
        }

        memcpy(&url->buffer[url->buffer_pos], buffer, size);
        url->buffer_pos += size;

        return size;
}

/* use to attempt to fill the read buffer up to requested number of bytes */
static int fill_buffer(URL_FILE *file, int want, int waittime) {
        fd_set fdread;
        fd_set fdwrite;
        fd_set fdexcep;
        struct timeval timeout;
        int rc;

        /* only attempt to fill buffer if transactions still running and buffer
         * doesnt exceed required size already
         */
        if ((!file->still_running) || (file->buffer_pos > want))
                return 0;

        /* attempt to fill buffer */
        do {
                int maxfd = -1;
                long curl_timeo = -1;

                FD_ZERO(&fdread);
                FD_ZERO(&fdwrite);
                FD_ZERO(&fdexcep);

                /* set a suitable timeout to fail on */
                timeout.tv_sec = 60; /* 1 minute */
                timeout.tv_usec = 0;

                curl_multi_timeout(multi_handle, &curl_timeo);
                if (curl_timeo >= 0) {
                        timeout.tv_sec = curl_timeo / 1000;
                        if (timeout.tv_sec > 1)
                                timeout.tv_sec = 1;
                        else
                                timeout.tv_usec = (curl_timeo % 1000) * 1000;
                }

                /* get file descriptors from the transfers */
                curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);

                /* In a real-world program you OF COURSE check the return code of the
                 function calls.  On success, the value of maxfd is guaranteed to be
                 greater or equal than -1.  We call select(maxfd + 1, ...), specially
                 in case of (maxfd == -1), we call select(0, ...), which is basically
                 equal to sleep. */

                rc = select(maxfd + 1, &fdread, &fdwrite, &fdexcep, &timeout);

                switch (rc) {
                case -1:
                        /* select error */
                        break;

                case 0:
                        break;

                default:
                        /* timeout or readable/writable sockets */
                        /* note we *could* be more efficient and not wait for
                         * CURLM_CALL_MULTI_PERFORM to clear here and check it on re-entry
                         * but that gets messy */
                        while (curl_multi_perform(multi_handle, &file->still_running)
                                        == CURLM_CALL_MULTI_PERFORM)
                                ;

                        break;
                }
        } while (file->still_running && (file->buffer_pos < want));
        return 1;
}

/* use to remove want bytes from the front of a files buffer */
static int use_buffer(URL_FILE *file, int want) {
        /* sort out buffer */
        if ((file->buffer_pos - want) <= 0) {
                /* ditch buffer - write will recreate */
                if (file->buffer)
                        free(file->buffer);

                file->buffer = NULL;
                file->buffer_pos = 0;
                file->buffer_len = 0;
        } else {
                /* move rest down make it available for later */
                memmove(file->buffer, &file->buffer[want], (file->buffer_pos - want));

                file->buffer_pos -= want;
        }
        return 0;
}

URL_FILE *
url_fopen(const char *url, const char *operation) {
        /* this code could check for URLs or types in the 'url' and
         basicly use the real fopen() for standard files */

        URL_FILE *file;
        (void) operation;

        file = (URL_FILE*)malloc(sizeof(URL_FILE));
        if (!file)
                return NULL;

        memset(file, 0, sizeof(URL_FILE));

        if ((file->handle.file = fopen(url, operation))) {
                file->type = CFTYPE_FILE; /* marked as URL */
        } else {
                file->type = CFTYPE_CURL; /* marked as URL */
                file->handle.curl = curl_easy_init();

                curl_easy_setopt(file->handle.curl, CURLOPT_URL, url);
                curl_easy_setopt(file->handle.curl, CURLOPT_WRITEDATA, file);
                curl_easy_setopt(file->handle.curl, CURLOPT_VERBOSE, 0L);
                curl_easy_setopt(file->handle.curl, CURLOPT_WRITEFUNCTION, write_callback);

                if (!multi_handle)
                        multi_handle = curl_multi_init();

                curl_multi_add_handle(multi_handle, file->handle.curl);

                /* lets start the fetch */
                while (curl_multi_perform(multi_handle, &file->still_running)
                                == CURLM_CALL_MULTI_PERFORM)
                        ;

                if ((file->buffer_pos == 0) && (!file->still_running)) {
                        /* if still_running is 0 now, we should return NULL */

                        /* make sure the easy handle is not in the multi handle anymore */
                        curl_multi_remove_handle(multi_handle, file->handle.curl);

                        /* cleanup */
                        curl_easy_cleanup(file->handle.curl);

                        free(file);

                        file = NULL;
                }
        }
        return file;
}

int url_fclose(URL_FILE *file) {
        int ret = 0;/* default is good return */

        switch (file->type) {
        case CFTYPE_FILE:
                ret = fclose(file->handle.file); /* passthrough */
                break;

        case CFTYPE_CURL:
                /* make sure the easy handle is not in the multi handle anymore */
                curl_multi_remove_handle(multi_handle, file->handle.curl);

                /* cleanup */
                curl_easy_cleanup(file->handle.curl);
                break;

        default: /* unknown or supported type - oh dear */
                ret = EOF;
                errno = EBADF;
                break;

        }

        if (file->buffer)
                free(file->buffer);/* free any allocated buffer space */

        free(file);

        return ret;
}

int url_feof(URL_FILE *file) {
        int ret = 0;

        switch (file->type) {
        case CFTYPE_FILE:
                ret = feof(file->handle.file);
                break;

        case CFTYPE_CURL:
                if ((file->buffer_pos == 0) && (!file->still_running))
                        ret = 1;
                break;
        default: /* unknown or supported type - oh dear */
                ret = -1;
                errno = EBADF;
                break;
        }
        return ret;
}

char *
url_fgets(char *ptr, int size, URL_FILE *file) {
        int want = size - 1;/* always need to leave room for zero termination */
        int loop;

        switch (file->type) {
        case CFTYPE_FILE:
                ptr = fgets(ptr, size, file->handle.file);
                break;

        case CFTYPE_CURL:
                fill_buffer(file, want, 1);

                /* check if theres data in the buffer - if not fill either errored or
                 * EOF */
                if (!file->buffer_pos)
                        return NULL;

                /* ensure only available data is considered */
                if (file->buffer_pos < want)
                        want = file->buffer_pos;

                /*buffer contains data */
                /* look for newline or eof */
                for (loop = 0; loop < want; loop++) {
                        if (file->buffer[loop] == '\n') {
                                want = loop + 1;/* include newline */
                                break;
                        }
                }

                /* xfer data to caller */
                memcpy(ptr, file->buffer, want);
                ptr[want] = 0;/* allways null terminate */

                use_buffer(file, want);

                /*printf("(fgets) return %d bytes %d left\n", want,file->buffer_pos);*/
                break;

        default: /* unknown or supported type - oh dear */
                ptr = NULL;
                errno = EBADF;
                break;
        }

        return ptr;/*success */
}


/* Small main program to retrive from a url using fgets and fread saving the
 * output to two test files (note the fgets method will corrupt binary files if
 * they contain 0 chars */



int HttpPost::GetRemoteFile(string file, string url) {
        URL_FILE *handle;
        FILE *outf;

        char buffer[256];

        /* copy from url line by line with fgets */



        outf = fopen(file.c_str(), "w+");
        if (!outf) {
                perror("couldn't open fgets output file\n");
                return 1;
        }

        handle = url_fopen(url.c_str(), "r");
        if (!handle) {
                printf("couldn't url_fopen() %s\n", url.c_str());
                fclose(outf);
                return 2;
        }

        while (!url_feof(handle)) {
                url_fgets(buffer, sizeof(buffer), handle);
                fwrite(buffer, 1, strlen(buffer), outf);
        }

        url_fclose(handle);

        fclose(outf);

        return 0;/* all done */
}

void HttpPost::SetAryDownlist(wxArrayString lfiles,wxArrayString rfiles){
            m_arylfiles=lfiles;
        m_aryrfiles=rfiles;
}
