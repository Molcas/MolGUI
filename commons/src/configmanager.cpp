/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "configmanager.h"

#include "envutil.h"

static ConfigManager* managerInstance = NULL;

ConfigManager::ConfigManager() {
}

ConfigManager::~ConfigManager() {
        ConfigMap::iterator it;
    for(it = m_configMap.begin(); it != m_configMap.end(); it++) {
                it->second->Save();
        delete it->second;
    }
    m_configMap.clear();
}

ConfigManager* ConfigManager::Get(void) {
        if(!managerInstance)
                managerInstance = new ConfigManager;
    return managerInstance;
}

void ConfigManager::Free(void) {
    delete managerInstance;
    managerInstance = NULL;
}

XmlFile* ConfigManager::GetConfig(const wxString& appName) {
        wxCriticalSectionLocker locker(m_cs);
    ConfigMap::iterator it = m_configMap.find(appName);
    if(it != m_configMap.end())
        return it->second;
    XmlFile* config = new XmlFile(appName, EnvUtil::GetUserDataDir() + platform::PathSep() + appName + wxT(".conf"));
    m_configMap[appName] = config;
    return config;
}
