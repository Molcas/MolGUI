/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "genericdlg.h"

#include <wx/statline.h>

const int CTRL_ID_BTN_SUBMIT = wxNewId();
const int CTRL_ID_BTN_KILL = wxNewId();
const int CTRL_ID_BTN_SHOW = wxNewId();

#define BTN_COUNT 10

static const long btnStyles[BTN_COUNT] = {
        BTN_YES,
        BTN_OK,
        BTN_SAVE,
        BTN_NO,
        BTN_CANCEL,
        BTN_RESET,
        BTN_SUBMIT,
        BTN_KILL,
        BTN_SHOW,
        BTN_CLOSE
};

static const int btnIds[BTN_COUNT] = {
        wxID_YES,
        wxID_OK,
        wxID_SAVE,
        wxID_NO,
        wxID_CANCEL,
        wxID_RESET,
        CTRL_ID_BTN_SUBMIT,
        CTRL_ID_BTN_KILL,
        CTRL_ID_BTN_SHOW,
        wxID_CLOSE
};

GenericPnl::GenericPnl(wxWindow* parent, wxWindowID id) : wxPanel(parent, id) {
        m_pParent = parent;
}

GenericPnl::~GenericPnl() {
}

wxArrayString& GenericPnl::GetAdditionalBtnLabels(void) {
        return m_additionalBtnLabels;
}

wxIntArray& GenericPnl::GetAdditionalBtnIds(void) {
        return m_additionalBtnIds;
}

void GenericPnl::EnableButton(int id, bool isEnable) {
        GenericDlg* dlg = static_cast<GenericDlg*>(m_pParent);
        if(dlg) dlg->EnableButton(id, isEnable);
}

BEGIN_EVENT_TABLE(GenericDlg, wxDialog)
    EVT_BUTTON(wxID_ANY, GenericDlg::OnDialogButton)
    EVT_CLOSE(GenericDlg::OnClose)
END_EVENT_TABLE()

GenericDlg::GenericDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style) {
    SetFocus();
}

GenericDlg::~GenericDlg() {
}

void GenericDlg::SetGenericPanel(GenericPnl* panel) {
        m_genericPnl = panel;
    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
    wxButton * btn = NULL;

        wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
        wxArrayString& addtionalArray = panel->GetAdditionalBtnLabels();
        wxIntArray& idArray =  panel->GetAdditionalBtnIds();
        for(unsigned j = 0; j < addtionalArray.GetCount(); j++) {
            btn = new wxButton(this, idArray[j], addtionalArray[j], wxDefaultPosition, wxSize(100,30));
                bsBottom->Add(btn, 0, wxALL, 2);
        }

        bsBottom->AddStretchSpacer(1);

        long buttonStyle = m_genericPnl->GetButtonStyle();
        wxString label;
        for(int i = 0; i < BTN_COUNT; i++) {
                if(buttonStyle & btnStyles[i]) {
                        if(btnStyles[i] == BTN_RESET) {
                                label = wxT("Reset");
                        }else if(btnStyles[i] == BTN_SUBMIT) {
                                label = wxT("Submit");
                        }else if(btnStyles[i] == BTN_KILL) {
                                label = wxT("Kill");
                        }else if(btnStyles[i] == BTN_SHOW) {
                                label = wxT("Show");
                        }else {
                                label = wxEmptyString;
                        }
                        btn = new wxButton(this, btnIds[i], label, wxDefaultPosition, wxSize(100,30));
                        bsBottom->Add(btn, 0, wxALL, 2);
                }
        }

    bsTop->Add(panel, 1, wxEXPAND|wxALL, 4);
    bsTop->AddSpacer(3);
    bsTop->Add(bsBottom, 0, wxEXPAND | wxALL, 2);

    this->SetSizer(bsTop);
    Fit();
    Centre();
}

void GenericDlg::OnDialogButton(wxCommandEvent& event) {
        long id = 0;
        switch(event.GetId()) {
                case wxID_YES:
                        id = BTN_YES;
                        break;
                case wxID_OK:
                        id = BTN_OK;
                        break;
                case wxID_SAVE:
                        id = BTN_SAVE;
                        break;
                case wxID_NO:
                        id = BTN_NO;
                        break;
                case wxID_CANCEL:
                        id = BTN_CANCEL;
                        break;
                case wxID_RESET:
                        id = BTN_RESET;
                        break;
                case wxID_CLOSE:
                        id = BTN_CLOSE;
                        break;
                default:
                        if(event.GetId() == CTRL_ID_BTN_SUBMIT) {
                                id = BTN_SUBMIT;
                        }else if(event.GetId()==CTRL_ID_BTN_KILL){
                            id=BTN_KILL;
                        }else{
                                id = event.GetId();
                        }
                        break;
        }
        if(m_genericPnl->OnButton(id)) {
                    HideDialog(id);
        }
}

void GenericDlg::OnClose(wxCloseEvent& event){
        if(m_genericPnl->OnButton(BTN_CLOSE)) {
            HideDialog(BTN_CLOSE);
        }
}

void GenericDlg::HideDialog(int returnCode) {
    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
}

void GenericDlg::EnableButton(int buttonStyle, bool isEnable) {
        bool found = false;
        wxWindow* win = NULL;
        for(int i = 0; i < BTN_COUNT; i++) {
                if(buttonStyle & btnStyles[i]) {
                        win = FindWindow(btnIds[i]);
                        if(win) {
                                win->Enable(isEnable);
                                found = true;
                                break;
                        }
                }
        }
        if(!found) {
                win = FindWindow(buttonStyle);
                if(win) {
                        win->Enable(isEnable);
                }
        }
}
