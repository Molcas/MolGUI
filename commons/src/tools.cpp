/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "tools.h"
#include "appglobals.h"
#include "envutil.h"
#include "configmanager.h"
#include "fileutil.h"
#include "stringutil.h"

#include <wx/filename.h>
#include <wx/tokenzr.h>

static wxFrame* m_pFrameInst = NULL;

void Tools::ShowError(const wxString& message, bool isSafe) {
        if(platform::windows && isSafe) {
                wxSafeShowMessage(wxT("Error"), message);
        }else {
            //wxMessageBox(message, wxT("Error"), wxOK | wxICON_ERROR);
                wxLogError(message);
            //exit(0);
        }
}

void Tools::ShowInfo(const wxString& message) {
        wxMessageBox(message, wxT("Info"), wxOK | wxICON_INFORMATION);
        //wxLogMessage(message);
}

void Tools::ShowWarning(const wxString& message) {
        wxMessageBox(message, wxT("Warning"), wxOK | wxICON_WARNING);
}

void Tools::ShowStatus(const wxString& message, int pos) {
        if(m_pFrameInst) {
                m_pFrameInst->GetStatusBar()->SetStatusText(message, pos);
        }
}

void Tools::SetAppFrame(wxFrame* pFrame) {
        m_pFrameInst = pFrame;
}

wxFrame* Tools::GetAppFrame(void) {
        return m_pFrameInst;
}

bool Tools::ExecModule(wxString execProcName, wxString outputResultFileName, wxString errorResultFileName) {
    wxArrayString output, errors;
    int code = wxExecute(execProcName, output, errors, wxEXEC_SYNC);
    // std::cout << "Tools::ExecModule: " << execProcName << std::endl;
    FileUtil::OutputStringLineFile(outputResultFileName, output);
    FileUtil::OutputStringLineFile(errorResultFileName, errors);

    if (code < 0 || errors.GetCount() > 0 ) {
        return false;
    } else {
        return true;
    }
}

void Tools::EnableMenu(wxMenuBar* pMenuBar, int menuId, bool enabled) {
        if(pMenuBar != NULL) {
                if(pMenuBar->FindItem(menuId) && pMenuBar->IsEnabled(menuId) != enabled) {
                pMenuBar->Enable(menuId, enabled);
                }
    }
}

void Tools::CheckMenu(wxMenuBar* pMenuBar, int id, const bool check) {
        wxMenuItem* menuItem = pMenuBar->FindItem(id);
        if(menuItem) {
                pMenuBar->Check(id, check);
        }
}

bool Tools::IsMenuChecked(wxMenuBar* pMenuBar, int id) {
        wxMenuItem* menuItem = pMenuBar->FindItem(id);
        if(menuItem) {
                return pMenuBar->IsChecked(id);
        }
        return false;
}

wxString Tools::GetFilePathFromUser(wxWindow* parent, wxString title, wxString filter, long style) {
        wxString lastVisitDir = ConfigManager::Get()->GetConfig(appglobals::appName)->GetString("lastVisitDir");
        if(lastVisitDir == wxEmptyString) {
                lastVisitDir = EnvUtil::GetBaseDir();
        }
        //begin : hurukun : 13/11/2009
    //wxFileDialog dialog(parent, title, lastVisitDir, wxEmptyString, filter, style);
        wxFileDialog dialog(parent, title, lastVisitDir.BeforeLast(platform::PathSep().GetChar(0)), wxEmptyString, filter, style);
        //end   : hurukun : 13/11/2009
    if (dialog.ShowModal() == wxID_OK) {
        wxString fileName = dialog.GetPath();
        if(style & wxFD_SAVE) {
                        if(StringUtil::IsEmptyString(fileName)) {
                                ShowError(wxT("Please enter a valid file name."));
                        }else if(fileName.Find(wxT(".")) == wxNOT_FOUND) {
                                wxStringTokenizer tkz(filter, wxT("|"));
                                int i = -1;
                                while (tkz.HasMoreTokens()) {
                                        wxString token = tkz.GetNextToken();
                                        i++;
                                        if(i == dialog.GetFilterIndex() * 2 + 1 && token.StartsWith(wxT("*."))) {
                                                wxLogError(token);
                                        }
                                }
                        }
                }
                wxString fullPath = FileUtil::NormalizeFileName(fileName);
        wxString visitDir = wxFileName(fullPath).GetPath(wxPATH_GET_VOLUME);
        if(!visitDir.IsSameAs(lastVisitDir, platform::windows == false)) {
                        ConfigManager::Get()->GetConfig(appglobals::appName)->BuildElement("lastVisitDir", visitDir);
                }
                return fileName;
        }
        return wxEmptyString;
}



void Tools::UpdateRecentFilesMenu(wxMenu* pMenu, int startId, const wxString& fileName,
                                                                        wxArrayString& arrayFileHis, bool isClearHis, bool isRemove) {
    int i = 0, preCount = 0, j;
    wxString updateFileName = fileName;

        preCount = arrayFileHis.GetCount();
        if (isClearHis) {
        arrayFileHis.Empty();
    }else {
            if (!StringUtil::IsEmptyString(updateFileName)) {
                for (i = 0; i < preCount; i++) {
                    if (arrayFileHis[i].IsSameAs(updateFileName, platform::windows == false)) {
                        arrayFileHis.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        if (!isClearHis) {
            if (!isRemove && !StringUtil::IsEmptyString(updateFileName)) {
                    arrayFileHis.Add(updateFileName);
                    while (arrayFileHis.GetCount() > (unsigned)10) {
                        arrayFileHis.RemoveAt(0);
                    }
                }
        }

        if (!StringUtil::IsEmptyString(updateFileName) || isClearHis) {
            while (preCount > 0) {
                pMenu->Delete(startId + preCount - 1);
                preCount--;
            }
        }
    j = 0;
    for (i = arrayFileHis.GetCount() - 1; i >= 0; i--, j++) {
        pMenu->Insert(j, startId + j, wxString::Format(wxT("%d "), j+1) + arrayFileHis[i]);
    }
}


bool Tools::IsHashSame(const StringHash& hash1,const StringHash& hash2)
{
        StringHash::const_iterator  cit;
        wxString key,value;
        if(hash1.size()!=hash2.size()){
                return false;
        }
        for(cit=hash1.begin(); cit !=hash1.end(); ++cit){
                key=cit->first;
                value=cit->second;
                if(hash2.find(key)==hash2.end()){
                        return false;
                }
                if(value.Cmp(hash2.find(key)->second)!=0){
                        return false;
                }
        }
        return true;
}
