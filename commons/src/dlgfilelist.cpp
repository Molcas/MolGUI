/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsshinfo.cpp: implementation of the DlgSSHInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "dlgfilelist.h"
#include "stringutil.h"
#include "envutil.h"
const int CTRL_ID_TXT_MASTERKEY = wxNewId();
const int CTRL_ID_BTN_SELECTALL = wxNewId();

DlgFileList::DlgFileList(wxWindow *parent, wxWindowID id, const wxString &title,
                         const wxPoint &pos , const wxSize &size, long style)
{
    wxDialog::Create(parent, id, title, pos, wxSize(400, 180), style);
    //CreateGUIControls();
    initflag = false;
}
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgFileList, wxDialog)
    EVT_BUTTON(wxID_OK, DlgFileList::DoOk)
    EVT_BUTTON(wxID_CANCEL, DlgFileList::DoCancel)
    EVT_CLOSE(DlgFileList::OnClose)
    EVT_TEXT_ENTER(CTRL_ID_TXT_MASTERKEY, DlgFileList::DoOk)
    EVT_BUTTON(CTRL_ID_BTN_SELECTALL, DlgFileList::OnSelectAll)
END_EVENT_TABLE()

DlgFileList::~DlgFileList()
{
    m_pJobMonitorPnl = NULL;
}

void DlgFileList::OnSelectAll(wxCommandEvent &event)
{
    for(unsigned int i = 0; i < m_chklist->GetCount(); i++) {
        m_chklist->Check(i, true);
    }
}

void DlgFileList::DoCancel(wxCommandEvent &event)
{
    this->Close();
}

void DlgFileList::SetFiles(wxArrayString rfiles) {
    m_rfiles = rfiles;
}

void DlgFileList::CreateGUIControls()
{

    if(initflag)return;
    initflag = true;
    wxBoxSizer *bsTop = new wxBoxSizer(wxVERTICAL);
    SetSizer(bsTop);
    wxStaticText *lbmasterkey = new wxStaticText( this, wxID_STATIC,
            wxT("Please check the files you want to download:") ,
            wxDefaultPosition, wxDefaultSize);
    wxButton *btnall = new wxButton( this, CTRL_ID_BTN_SELECTALL, wxT("Select All"), wxDefaultPosition, wxDefaultSize);
    m_chklist = new wxCheckListBox(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_rfiles);
    bsTop->Add(lbmasterkey, 0, wxALL | wxEXPAND, 5);
    bsTop->Add(btnall, 0, wxALL | wxALIGN_CENTER, 5);
    bsTop->Add(m_chklist, 0, wxALL | wxEXPAND, 5);
    wxButton *btnok = new wxButton( this, wxID_OK, wxT("&OK"), wxDefaultPosition, wxDefaultSize);
    wxButton *btncancel = new wxButton(this, wxID_CANCEL, wxT("&Cancel"), wxDefaultPosition, wxDefaultSize);
    wxBoxSizer *bsBtn = new wxBoxSizer(wxHORIZONTAL);

    bsBtn->Add(btnok, 0, wxALL | wxALIGN_CENTER, 5);
    bsBtn->Add(btncancel, 0, wxALL | wxALIGN_CENTER, 5);
    bsTop->Add(bsBtn, 0, wxALL | wxALIGN_CENTER);
    bsTop->SetSizeHints(this);
    Fit();
    //m_chklist->SetFocus();
}

void DlgFileList::DoOk(wxCommandEvent &event)
{
    m_arydownload.Empty();
    for(unsigned int i = 0; i < m_chklist->GetCount(); i++) {
        if(m_chklist->IsChecked(i))m_arydownload.Add(i);
    }
    m_pJobMonitorPnl->SetArrayDownload(m_arydownload);

    if(m_arydownload.GetCount() <= 0) {
        wxMessageBox(wxT("Please select at least one file to download!"));
        return;
    }

    if(IsModal())
    {
        EndModal(wxID_OK);
    }
    else
    {
        SetReturnCode(wxID_OK);
        this->Show(false);
    }
}

void DlgFileList::OnClose(wxCloseEvent &event)
{
    //

    if(IsModal())
    {
        EndModal(wxID_CANCEL);
    }
    else
    {
        SetReturnCode(wxID_CANCEL);
        this->Show(false);
    }
}

void DlgFileList::SetJobMonitor(JobMonitorPnl *pjobmonitor) {
    m_pJobMonitorPnl = pjobmonitor;
}
