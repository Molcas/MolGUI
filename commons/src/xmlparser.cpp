/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <stdio.h>
#include <string.h>

#include "expat.h"
#include "xmlparser.h"
#include "cstringutil.h"

#include "tools.h"

#include <iostream>

#define LINE_BUF_SIZE 1024
#define XMLCALL
/**
 * A global variable to keep the root of the xml file
 */
XmlElementList* rootElementList = NULL;
/**
 * The current processed element, add its attributes, content, and its sub element
 */
XmlElement* currElement = NULL;

bool keepContentFormat = true;
/**
 * Called when encountering a start tag.
 * @param userData: the depth of the element;
 * @param name: the name of the element;
 * @param attrs: the array of attributes;
 *                                attrs[0] is attribute name, and attrs[1] is attribute value, and so on
 */
static void XMLCALL StartElement(void *userData, const char *name, const char **attrs) {
        int i;
        int *depthPtr = (int *)userData;
//#define __DEBUG_XML__
#ifdef __DEBUG_XML__
        for (i = 0; i < *depthPtr; i++) {
                putchar('\t');
        }
#endif

        if(NULL == currElement) {
                rootElementList = CreateElementList();
                currElement = &rootElementList->elem;
        }else {
                currElement = AddElement(currElement);
        }
        StringCopy(&currElement->name, name, strlen(name));
#ifdef __DEBUG_XML__
        fprintf(stdout, "===Start Element <%s>\n", currElement->name);
#endif
        i = 0;
        while(attrs[i] != NULL) {
                
#ifdef __DEBUG_XML__
                XmlAttribute* attr = NULL;
                attr = AddAttribute(currElement, attrs[i], attrs[i+1]);
                fprintf(stdout, "%s = %s\n", attr->name, attr->value);
#else
                AddAttribute(currElement, attrs[i], attrs[i+1]);
#endif

                i += 2;
        }

        *depthPtr += 1;
}

/**
 * Called when encountering an end tag.
 * @param userData: the depth of the element;
 * @param name: the name of the element;
 */
static void XMLCALL EndElement(void *userData, const char *name) {
        int *depthPtr = (int *)userData;
        *depthPtr -= 1;

#ifdef __DEBUG_XML__
        for (int i = 0; i < *depthPtr; i++) {
                putchar('\t');
        }
        if(NULL != currElement) {
                fprintf(stdout, "===End Element </%s>\n", currElement->name);
        }
#endif
        currElement = currElement->parent;
}

/**
 * Called when processing characters enclosed in the pair of a tag.
 * @param s: may be '\n'
 * @param len: the lenght of s
 * @note s may not terminated by '\0'
 */
static void XMLCALL CharacterHandler(void *userData, const char *s, int len){
        char* newStr = NULL;
        char* paddedStr = NULL;
        char* trimmedStr = NULL;
        int newStrLen = 1;

        StringCopy(&paddedStr, s, len);
        if(!keepContentFormat) {
                trimmedStr = StringTrim(paddedStr, NULL);
        }else {
                //keep the format of content, but remove blank spaces
                trimmedStr = StringTrim(paddedStr, " \t");
        }

        if(strlen(trimmedStr) != 0) {
                if(NULL != currElement->content) {
                        // why add 2?
                        // one space for '\0'
                        // one space for ' '
                        newStrLen = strlen(currElement->content) + 2;
                }
                newStrLen += strlen(trimmedStr);
                newStr = (char*)malloc(newStrLen);
                if( NULL == newStr) {
                        fprintf(stderr, "cannot allocate memory\n");
                        exit(1);
                }
                newStr[0] = 0;
                if(NULL != currElement->content) {
                        strcpy(newStr, currElement->content);
                        strcpy(newStr + strlen(currElement->content), " ");
                        free(currElement->content);
                }
                //currElement->content = realloc(currElement->content, len);
                strncat(newStr, trimmedStr, strlen(trimmedStr));
                currElement->content = newStr;
        }
        free(paddedStr);
}

XmlElementList* ParseXmlDocument(const char* fileName, bool keepFormat) {
        char buf[LINE_BUF_SIZE];
        int depth = 0;
        FILE* fp = NULL;
        keepContentFormat = keepFormat;

        XML_Parser parser = XML_ParserCreate(NULL);

        XML_SetUserData(parser, &depth);
        XML_SetElementHandler(parser, StartElement, EndElement);
        XML_SetCharacterDataHandler(parser, CharacterHandler);

        fp = fopen(fileName, "r");
        if(fp == NULL) {
                fprintf(stderr, "No such file: %s\n", fileName);
                exit(0);
        }
        rootElementList = NULL;
        currElement = NULL;

        while(!feof(fp)) {
                memset(buf, 0, sizeof(buf));
                if(fgets(buf, LINE_BUF_SIZE, fp))
                {
                    size_t len = strlen(buf);
                    if (XML_Parse(parser, buf, len, 0) == XML_STATUS_ERROR) {
                            fprintf(stderr, "Parse error at line %d: %s\n",  (int)XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
                            if(fp) fclose(fp);
                            exit(1);
                    }
                }
        }
        fclose(fp);
        XML_ParserFree(parser);
        return rootElementList;
}

XmlElementList* CreateDocument(char* rootName) {
        XmlElementList* root = CreateElementList();
        StringCopy(&root->elem.name, rootName, strlen(rootName));
        return root;
}

XmlElementList* CreateElementList(void) {
        XmlElementList* elemList = (XmlElementList*)malloc(sizeof(XmlElementList));
        if( NULL == elemList) {
                fprintf(stderr, "cannot allocate memory\n");
                exit(1);
        }
        elemList->elem.name = NULL;
        elemList->elem.attrCount = 0;
        elemList->elem.attrList = NULL;
        elemList->elem.content = NULL;
        elemList->elem.parent = NULL;
        elemList->elem.subElemCount = 0;
        elemList->elem.subElemList = 0;
        elemList->next = NULL;

        return elemList;
}

XmlAttributeList* CreateAttributeList(void) {
        XmlAttributeList* attrList = (XmlAttributeList*)malloc(sizeof(XmlAttributeList));
        if( NULL == attrList) {
                fprintf(stderr, "cannot allocate memory\n");
                exit(1);
        }
        attrList->next = NULL;
        attrList->attr.name = NULL;
        attrList->attr.value = NULL;
        attrList->attr.elem = NULL;

        return attrList;
}

XmlAttribute* AddAttribute(XmlElement* elem, const char* attrName, const char* attrValue){
        XmlAttributeList* tempList = NULL;
        XmlAttributeList* attrList = CreateAttributeList();

//        if(strcmp(attrName, "MODULE") == 0 && strcmp(attrValue, "GATEWAY") == 0) {
                //printf("TODO:// xmlparser.c\n");
//        }
        StringCopy(&attrList->attr.name, attrName, strlen(attrName));
        StringCopy(&attrList->attr.value, attrValue, strlen(attrValue));
        attrList->attr.elem = elem;

        if(NULL == elem->attrList) {
                elem->attrList = attrList;
        }else{
                tempList = elem->attrList;
                while(NULL != tempList->next) {
                        tempList = tempList->next;
                }
                tempList->next = attrList;
        }
        elem->attrCount++;

        return &attrList->attr;
}

XmlElement* AddElement(XmlElement* parentElem){
        XmlElementList* tempList = NULL;
        XmlElementList* elemList = CreateElementList();

        elemList->elem.parent = parentElem;
        if(NULL == parentElem->subElemList) {
                parentElem->subElemList = elemList;
        }else{
                tempList = parentElem->subElemList;
                while(NULL != tempList->next) {
                        tempList = tempList->next;
                }
                tempList->next = elemList;
        }
        parentElem->subElemCount++;
        
        // std::cout << "addelement " << parentElem->subElemCount << "/" << parentElem->attrList << "/" << parentElem->content << std::endl;

        return &elemList->elem;
}

void FreeXmlElementList(XmlElementList* elemList) {
        XmlElementList* currElem = NULL;
        XmlElementList* nextElem = elemList;
        while(NULL != nextElem) {
                currElem = nextElem;
                nextElem = nextElem->next;
                FreeXmlElement(&currElem->elem);
                if(NULL != currElem->elem.name) {
                        free(currElem->elem.name);
                }
                free(currElem);
        }
}

void FreeXmlElement(XmlElement* elem) {
        XmlAttributeList* currAttr = NULL;
        XmlAttributeList* nextAttr = NULL;
        nextAttr = elem->attrList;
        while(NULL != nextAttr) {
                currAttr = nextAttr;
                nextAttr = nextAttr->next;
                FreeXmlAttribute(&currAttr->attr);
                free(currAttr);
        }
        elem->attrCount = 0;
        elem->attrList = NULL;

        if(NULL != elem->content) {
                free(elem->content);
                elem->content = NULL;
        }
        FreeXmlElementList(elem->subElemList);
        elem->subElemCount = 0;
        elem->subElemList = 0;
}

void FreeXmlAttribute(XmlAttribute* attr) {
        free(attr->name);
        free(attr->value);
}


void DumpXmlElementList(FILE* fp, XmlElementList* elemList, int depth) {
        XmlElementList* nextElem = elemList;
        while(NULL != nextElem) {
                DumpXmlElement(fp, &nextElem->elem, depth);
                nextElem = nextElem->next;
        }
}

void DumpXmlElement(FILE* fp, XmlElement* elem, int depth) {
        int i = 0;
        XmlAttributeList* nextAttr = NULL;
        for(i = 0; i < depth; i++) {
                fprintf(fp, "\t");
        }
        fprintf(fp, "<%s", elem->name);
        nextAttr = elem->attrList;
        while(NULL != nextAttr) {
                DumpXmlAttribute(fp, &nextAttr->attr);
                nextAttr = nextAttr->next;
        }
        if(NULL == elem->content && NULL == elem->subElemList) {
                fprintf(fp, " />\n");
        } else {
                fprintf(fp, ">\n");

                if(NULL != elem->content && strlen(StringRightTrim(elem->content, NULL)) > 0) {
                        for(i = 0; i < depth + 1; i++) {
                                fprintf(fp, "\t");
                        }
                        fprintf(fp, "%s\n", elem->content);
                }
                DumpXmlElementList(fp, elem->subElemList, depth + 1);
                for(i = 0; i < depth; i++) {
                        fprintf(fp, "\t");
                }
                fprintf(fp, "</%s>\n", elem->name);
        }
}

void DumpXmlAttribute(FILE* fp, XmlAttribute* attr) {
        fprintf(fp, " %s=\"%s\"", attr->name, attr->value);
}

XmlElement** GetSubElements(XmlElement* elem, const char* tagName, int* count) {
        int i = 0;
        XmlElement** elems = NULL;
        XmlElementList* currElemList = NULL;

        *count = 0;
        if(NULL == elem) return NULL;
        elems = (XmlElement**)malloc(elem->subElemCount * sizeof(XmlElement*));
        memset(elems, 0, elem->subElemCount * sizeof(XmlElement*));

        currElemList = elem->subElemList;
        while(NULL != currElemList) {
                // std::cout << "elem->subElemList: " << i << " " << currElemList->elem.name << std::endl;
                // std::cout << "tagName: " << tagName << std::endl;
                if(strcmp(tagName, currElemList->elem.name) == 0 || (strcmp(tagName, "KEYWORD") == 0
            && (strcmp(currElemList->elem.name, "SELECT") == 0 || strcmp(currElemList->elem.name, "GROUP") == 0))) {
                        elems[i++] = &currElemList->elem;
                        // std::cout << "currElemList: " << i << " " << currElemList << std::endl;
        
                }
                currElemList = currElemList->next;
                // std::cout << "Elements: " << i << " " << elem.name << std::endl;
                }
        *count = i;
        return elems;
}

XmlElement* GetSubElement(XmlElement* elem, const char* tagName) {
        int count = 0;
        XmlElement** elems = GetSubElements(elem, tagName, &count);
        if(count > 0) {
                return elems[0];
        }
        return NULL;
}

char* GetAttributeString(XmlElement* elem, const char* attrName) {
        XmlAttributeList* currAttrList = NULL;
        if(NULL == elem || NULL == elem->attrList)
                return NULL;

        currAttrList = elem->attrList;
        while(NULL != currAttrList) {
                if(currAttrList->attr.name != NULL && strcmp(attrName, currAttrList->attr.name) == 0) {
                        return currAttrList->attr.value;
                }
                currAttrList = currAttrList->next;
        }
        return NULL;
}

int GetAttributeInt(int* value, XmlElement* elem, const char* attrName) {
        char* strValue = GetAttributeString(elem, attrName);
        *value = 0;
        if(strValue != NULL) {
                *value = atoi(strValue);
                return 1;
        }
        return 0;
}

int GetAttributeDouble(double* value, XmlElement* elem, const char* attrName) {
        char* strValue = GetAttributeString(elem, attrName);
        *value = 0.0f;
        if(strValue != NULL) {
                *value = atof(strValue);
                return 1;
        }
        return 0;
}

char** GetAttributeStringArray(XmlElement* elem, const char* attrName, int* count) {
        char* strValue = GetAttributeString(elem, attrName);
        *count = 0;
        if(strValue != NULL) {
                return StringTokenizer(strValue, ",", count, 0);
        }
        return NULL;
}

int* GetAttributeIntArray(XmlElement* elem, const char* attrName, int* count) {
        int i = 0;
        int* intArray = NULL;
        char** stringArray = GetAttributeStringArray(elem, attrName, count);
        if(NULL != stringArray) {
                intArray = (int*)RequestMemory(intArray, 0, *count, sizeof(int));
                for(i = 0; i < *count; i++) {
                        intArray[i] = atoi(stringArray[i]);
                }
                FreeStringArray(stringArray, *count);
        }else {
                *count = 0;
        }
        return intArray;
}

double* GetAttributeDoubleArray(XmlElement* elem, const char* attrName, int* count) {
        int i = 0;
        double* doubleArray = NULL;
        char** stringArray = GetAttributeStringArray(elem, attrName, count);
        if(NULL != stringArray) {
                doubleArray = (double*)RequestMemory(doubleArray, 0, *count, sizeof(double));
                for(i = 0; i < *count; i++) {
                        doubleArray[i] = atof(stringArray[i]);
                }
                FreeStringArray(stringArray, *count);
        }else {
                *count = 0;
        }
        return doubleArray;
}
