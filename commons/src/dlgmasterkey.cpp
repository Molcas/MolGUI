/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsshinfo.cpp: implementation of the DlgSSHInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "dlgmasterkey.h"
#include "stringutil.h"
#include "envutil.h"
const int CTRL_ID_TXT_MASTERKEY = wxNewId();

DlgMasterKey::DlgMasterKey(wxWindow *parent, wxWindowID id, const wxString &title,
                           const wxPoint &pos , const wxSize &size, long style)
{
    wxDialog::Create(parent, id, title, pos, wxSize(400, 180), style);
    CreateGUIControls();
}
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgMasterKey, wxDialog)
    EVT_BUTTON(wxID_OK, DlgMasterKey::DoOk)
    EVT_BUTTON(wxID_CANCEL, DlgMasterKey::DoCancel)
    EVT_CLOSE(DlgMasterKey::OnClose)
    EVT_TEXT_ENTER(CTRL_ID_TXT_MASTERKEY, DlgMasterKey::DoOk)
END_EVENT_TABLE()

DlgMasterKey::~DlgMasterKey()
{

}

void DlgMasterKey::DoCancel(wxCommandEvent &event)
{
    this->Close();
}

void DlgMasterKey::CreateGUIControls()
{
    wxBoxSizer *bsTop = new wxBoxSizer(wxVERTICAL);
    SetSizer(bsTop);
    lbmasterkey = new wxStaticText( this, wxID_STATIC,
                                    wxT("Please input your masterkey:") ,
                                    wxDefaultPosition, wxDefaultSize, 0 );
    m_pTxtMasterKey = new wxTextCtrl(this, CTRL_ID_TXT_MASTERKEY, wxEmptyString,
                                     wxDefaultPosition, wxSize(200, 20), wxTE_PASSWORD | wxTE_PROCESS_ENTER);
    bsTop->Add(lbmasterkey, 0, wxALL | wxEXPAND, 5);
    bsTop->Add(m_pTxtMasterKey, 0, wxALL | wxEXPAND, 5);
    wxButton *btnok = new wxButton( this, wxID_OK, wxT("&OK"), wxDefaultPosition, wxDefaultSize);
    wxButton *btncancel = new wxButton(this, wxID_CANCEL, wxT("&Cancel"), wxDefaultPosition, wxDefaultSize);
    wxBoxSizer *bsBtn = new wxBoxSizer(wxHORIZONTAL);

    bsBtn->Add(btnok, 0, wxALL | wxALIGN_CENTER, 5);
    bsBtn->Add(btncancel, 0, wxALL | wxALIGN_CENTER, 5);
    bsTop->Add(bsBtn, 0, wxALL | wxALIGN_CENTER);
    bsTop->SetSizeHints(this);
    Fit();
    m_pTxtMasterKey->SetFocus();
}

void DlgMasterKey::DoOk(wxCommandEvent &event)
{
    if((m_pTxtMasterKey->GetValue()).IsNull() || (m_pTxtMasterKey->GetValue().Trim()).IsEmpty()) {
        wxMessageBox(wxT("Please input the masterkey!"));
        return;
    }
    EnvUtil::SetMasterKey(m_pTxtMasterKey->GetValue());
    if(IsModal())
    {
        EndModal(wxID_OK);
    }
    else
    {
        SetReturnCode(wxID_OK);
        this->Show(false);
    }
}

void DlgMasterKey::SetHostname(wxString hostname) {
    wxString msg = wxT("Please input the masterkey for '") + EnvUtil::GetMasterId() + wxT("'");
    lbmasterkey->SetLabel(msg);
    Fit();
}

void DlgMasterKey::OnClose(wxCloseEvent &event)
{
    //

    if(IsModal())
    {
        EndModal(wxID_CANCEL);
    }
    else
    {
        SetReturnCode(wxID_CANCEL);
        this->Show(false);
    }
}
