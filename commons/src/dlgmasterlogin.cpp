/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsshinfo.cpp: implementation of the DlgSSHInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "dlgmasterlogin.h"
#include "stringutil.h"
#include "envutil.h"
#include "dlgmasterlist.h"

const int CTRL_ID_TXT_MASTERKEY = wxNewId();

DlgMasterLogin::DlgMasterLogin(wxWindow *parent, wxWindowID id, const wxString &title,
                               const wxPoint &pos , const wxSize &size, long style)
{
    wxDialog::Create(parent, id, title, pos, wxSize(400, 180), style);
    CreateGUIControls();
    wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("master.conf");
    MasterFile::LoadFile(fileName, masterinfoarray);
    total = masterinfoarray.GetCount();

}
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgMasterLogin, wxDialog)
    EVT_BUTTON(wxID_OK, DlgMasterLogin::DoOk)
    EVT_BUTTON(wxID_CANCEL, DlgMasterLogin::DoCancel)
    EVT_CLOSE(DlgMasterLogin::OnClose)
    EVT_TEXT_ENTER(CTRL_ID_TXT_MASTERKEY, DlgMasterLogin::DoOk)
END_EVENT_TABLE()

DlgMasterLogin::~DlgMasterLogin()
{

}

void DlgMasterLogin::DoCancel(wxCommandEvent &event)
{
    this->Close();
}

void DlgMasterLogin::CreateGUIControls()
{
    wxBoxSizer *bSizer2;
    wxGridSizer *gSizer2;

    bSizer2 = new wxBoxSizer( wxVERTICAL );

    gSizer2 = new wxGridSizer( 3, 2, 0, 0 );


    m_lbmasterid = new wxStaticText( this, wxID_ANY, wxT("*Master Id:"), wxDefaultPosition, wxDefaultSize, 0 );
    gSizer2->Add( m_lbmasterid, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtmasterid = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxPoint( -1, -1 ), wxSize( -1, -1 ), 0 );
    m_txtmasterid->SetMinSize( wxSize( 130, -1 ) );
    m_txtmasterid->SetToolTip(wxT("Set an ID for management of hosts safely."));

    gSizer2->Add( m_txtmasterid, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_lbmasterkey = new wxStaticText( this, wxID_ANY, wxT("*Master Key:"), wxDefaultPosition, wxDefaultSize, 0 );
    gSizer2->Add( m_lbmasterkey, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    m_txtmasterkey = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
    m_txtmasterkey->SetMinSize( wxSize( 130, -1 ) );
    m_txtmasterkey->SetToolTip(wxT("Managed by master ID, to encrypt password of hosts for safety."));
    gSizer2->Add( m_txtmasterkey, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

    bSizer2->Add( gSizer2, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

    bSizer2->Add(CreateButtonSizer(wxOK | wxCANCEL), wxSizerFlags().Right().Border(wxALL, 5));

    this->SetSizer( bSizer2 );
    bSizer2->SetSizeHints(this);
    Fit();
    m_txtmasterid->SetFocus();
}

void DlgMasterLogin::DoOk(wxCommandEvent &event)
{
    if((m_txtmasterid->GetValue()).IsNull() || (m_txtmasterid->GetValue().Trim()).IsEmpty()) {
        wxMessageBox(wxT("please input the Master ID!"));
        m_txtmasterid->SetFocus();
        return;
    }
    if((m_txtmasterkey->GetValue()).IsNull() || (m_txtmasterkey->GetValue().Trim()).IsEmpty()) {
        wxMessageBox(wxT("please input Master Key!"));
        m_txtmasterkey->SetFocus();
        return;
    }

    unsigned int i = 0;
    bool isfind = false;
    for (i = 0; i < total; i++) {
        if(masterinfoarray[i].masterid.Cmp(m_txtmasterid->GetValue()) == 0) {
            isfind = true;
            EnvUtil::SetMasterId(m_txtmasterid->GetValue());
            EnvUtil::SetMasterKey(m_txtmasterkey->GetValue());
            //wxMessageBox(masterinfoarray[i].masterkey);
            //wxMessageBox(EnvUtil::GetMasterKeyMD5());
            if(masterinfoarray[i].masterkey.Cmp(EnvUtil::GetMasterKeyMD5()) == 0) {
                if(IsModal())
                {
                    //wxMessageBox(wxT("equals"));
                    EndModal(wxID_OK);
                }
                else
                {
                    SetReturnCode(wxID_OK);
                    this->Show(false);
                }
                //this->Close();
            } else {
                wxMessageBox(wxT("Master Key Error!"));
                m_txtmasterkey->SetFocus();
                return;
            }
        }
    }
    if(!isfind) {
        wxMessageBox(wxT("This master ID does not exist!"));
        m_txtmasterid->SetFocus();
    }
}


void DlgMasterLogin::OnClose(wxCloseEvent &event)
{
    //

    if(IsModal())
    {
        EndModal(wxID_CANCEL);
    }
    else
    {
        SetReturnCode(wxID_CANCEL);
        this->Show(false);
    }
}
