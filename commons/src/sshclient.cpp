/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "sshclient.h"
#include "fcntl.h"
#include "fileutil.h"
#include "envutil.h"
#include "stringutil.h"
#include "dlglogin.h"
#include "manager.h"
#include "wx/socket.h"
#include <wx/filename.h>
#ifdef _DARWIN_
#include <arpa/inet.h>
#endif

void SSHClient::kbd_callback(const char *name, int name_len,
                             const char *instruction, int instruction_len,
                             int num_prompts,
                             const LIBSSH2_USERAUTH_KBDINT_PROMPT *prompts,
                             LIBSSH2_USERAUTH_KBDINT_RESPONSE *responses,
                             void **abstract)
{
    (void)name;
    (void)name_len;
    (void)instruction;
    (void)instruction_len;
    if (num_prompts == 1) {
        //       responses[0].text = strdup(password);
        //   responses[0].length = strlen(password);
    }
    (void)prompts;
    (void)abstract;
    // std::cout << "kbd_callback " << std::endl;
} /* kbd_callback */

SSHClient::SSHClient()
{
    m_hostname = NULL;
    m_username = NULL;
    m_password = NULL;

    session = NULL;
    channel = NULL;
    m_retry = true;
    m_masterkey = wxEmptyString;

}

void SSHClient::SetHostname(wxString hostname) {
    wxIPV4address addr;
    addr.Hostname(hostname);
    wxString ipAddr = addr.IPAddress();
    StringUtil::String2CharPointer(&m_hostname, ipAddr);

}
void SSHClient::SetRetry(bool retry)
{
    m_retry = retry;
}

bool SSHClient::GetRetry(void)
{
    return m_retry;
}

void SSHClient::SetUsername(wxString username)
{
    char *input = NULL;
    StringUtil::String2CharPointer(&input, username);
    m_username = input;
}
void SSHClient::SetPassword(wxString pwd)
{
    char *input = NULL;
    pwd = EnvUtil::GetDecipherCode(pwd);
    StringUtil::String2CharPointer(&input, pwd);
    m_password = input;
}


void SSHClient::SetMasterkey(wxString masterkey)
{
    m_masterkey = masterkey;
    EnvUtil::SetMasterKey(masterkey);
}

wxString SSHClient::GetMasterkey(void)
{
    return m_masterkey;
}

SSHClient::~SSHClient()
{
    channel = NULL;
    session = NULL;
}

void SSHClient::Shutdown()
{
    int rc;

    if(session)
    {
        while ((rc = libssh2_session_disconnect(session,
                                                "Normal Shutdown, Thank you for playing")) == LIBSSH2_ERROR_EAGAIN);
        libssh2_session_free(session);
    }
#ifdef WIN32
    closesocket(sock);
#else
    close(sock);
#endif
}


int SSHClient::Download(const char *localfile, const char *remotefile)
{
    //    int sock=0;
    struct stat fileinfo;
    int rc;
#if defined(HAVE_IOCTLSOCKET)
    long flag = 1;
#endif
    off_t got = 0;
    int handle = 0;
#ifdef WIN32
    WSADATA wsadata;

    WSAStartup(MAKEWORD(2, 0), &wsadata);
#endif
    /* Request a file via SCP */
    do {
        channel = libssh2_scp_recv(session, remotefile, &fileinfo);

        if (!channel) {
            if(libssh2_session_last_errno(session) != LIBSSH2_ERROR_EAGAIN) {
                char *err_msg;

                libssh2_session_last_error(session, &err_msg, NULL, 0);
                //wxMessageBox(wxT("Unable to open a session"));
                fprintf(stderr, "Unable to open a session%s\n", err_msg);
                return -1;
            }
            else {
                fprintf(stderr, "libssh2_scp_recv() spin\n");
                //wxMessageBox(wxT("libssh2_scp_recv() spin"));
                waitsocket(sock, session);
            }
        }
    } while (!channel);
    /*    channel = libssh2_scp_recv(session, remotefile, &fileinfo);

        if (!channel) {
            wxMessageBox(wxT("Unable to open a session"));
            fprintf(stderr, "Unable to open a session\n");
            return -1;
        }
    */
    wxString strlfile = StringUtil::CharPointer2String(localfile);
    wxFileName fname(strlfile);
    if(!fname.DirExists())wxMkdir(fname.GetPath());
    if(wxFileExists(strlfile))wxRemoveFile(strlfile);
    handle = open(localfile, O_WRONLY | O_TRUNC | O_CREAT, S_IRWXU);
    //fprintf(stderr,"%d",handle);
    if(handle == -1) {
        //wxMessageBox(wxT("create execlog file error!"));
        return -1;
    }

    while(got < fileinfo.st_size) {
        char mem[1024];
        int amount = sizeof(mem);

        if((fileinfo.st_size - got) < amount) {
            amount = fileinfo.st_size - got;
        }

        rc = libssh2_channel_read(channel, mem, amount);
        if(rc > 0) {
            if(write(handle, mem, rc) == -1)
                return -1;

        }
        else {
#ifdef __DEBUG__
            fprintf(stderr, "libssh2_channel_read() failed: %d\n", rc);
#endif
            //wxMessageBox(wxT("libssh2_channel_read() failed!"));
            return -1;
        }
        got += rc;
    }

    libssh2_channel_free(channel);
    channel = NULL;
    close(handle);
#ifdef __DEBUG__
    fprintf(stderr, "all done\n");
#endif
    return 1;

}

long SSHClient::tvdiff(struct timeval newer, struct timeval older)
{
    return (newer.tv_sec - older.tv_sec) * 1000 +
           (newer.tv_usec - older.tv_usec) / 1000;
}


int SSHClient::waitsocket(int socket_fd, LIBSSH2_SESSION *session)
{
    struct timeval timeout;
    int rc;
    fd_set fd;
    fd_set *writefd = NULL;
    fd_set *readfd = NULL;
    int dir;

    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    FD_ZERO(&fd);

    FD_SET(socket_fd, &fd);

    /* now make sure we wait in the correct direction */
    dir = libssh2_session_block_directions(session);

    if(dir & LIBSSH2_SESSION_BLOCK_INBOUND)
        readfd = &fd;

    if(dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)
        writefd = &fd;

    rc = select(socket_fd + 1, readfd, writefd, NULL, &timeout);

    return rc;
}

int SSHClient::ConnectToHost(void)
{
    unsigned long hostaddr;
    int auth_pw = 0;
    struct sockaddr_in sin;
    int rc;
#if defined(HAVE_IOCTLSOCKET)
    long flag = 1;
#endif

#ifdef WIN32
    WSAStartup(MAKEWORD(2, 0), &wsadata);
#endif
    if(!m_hostname) {
        wxMessageBox(wxT("Please set host's name!"));
        return -1;
    }
    hostaddr = inet_addr(m_hostname);

    /* Ultra basic "connect to port 22 on localhost"
     * Your code is responsible for creating the socket establishing the
     * connection
     */
    sock = socket(AF_INET, SOCK_STREAM, 0);

    sin.sin_family = AF_INET;
    sin.sin_port = htons(22);
    sin.sin_addr.s_addr = hostaddr;
    if(!m_username)return -1;
    if(!m_password)return -1;
#ifdef __DEBUG__
    fprintf(stderr, "username:%s ", m_username);
    fprintf(stderr, "password:%s ", m_password);

    fprintf(stderr, "begin connecting %s \n", m_hostname);
#endif
    if (connect(sock, (struct sockaddr *)(&sin),
                sizeof(struct sockaddr_in)) != 0) {
#ifdef __DEBUG__
        fprintf(stderr, "failed to connect!\n");
#endif
        return -1;
    }

    /* Create a session instance
     */
#ifdef __DEBUG__
    fprintf(stderr, "creating session!\n");
#endif
    session = libssh2_session_init();
    if(!session)
        return -1;
#ifdef __DEBUG__
    fprintf(stderr, "set blocking\n");
#endif
    libssh2_session_set_blocking(session, 1);
    /*
        rc      = libssh2_session_startup (session, sock);
        if (rc != 0) {
            fprintf(stderr, "Failed Start the SSH session\n");
            return -1;
        }
        // Authenticate via password
        rc = libssh2_userauth_password(session, m_username, m_password);
        if (rc != 0) {
            fprintf(stderr, "Failed to authenticate\n");
            Shutdown();
            close(sock);
            return -1;
        }
    */
    /* ... start it up. This will trade welcome banners, exchange keys,
     * and setup crypto, compression, and MAC layers
     */
#ifdef __DEBUG__
    fprintf(stderr, "start up session\n");
#endif
    while ((rc = libssh2_session_startup(session, sock))
            == LIBSSH2_ERROR_EAGAIN);
    if(rc) {
#ifdef __DEBUG__
        fprintf(stderr, "Failure establishing SSH session: %d\n", rc);
#endif
        return -1;
    }

    /* At this point we havn't yet authenticated.  The first thing to do
     * is check the hostkey's fingerprint against our known hosts Your app
     * may have it hard coded, may go to a file, may present it to the
     * user, that's your call
     */
#ifdef __DEBUG__
    const char *fingerprint;
    fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_MD5);
    fprintf(stderr, "Fingerprint: ");
    int i;
    for(i = 0; i < 16; i++) {
        fprintf(stderr, "%02X ", (unsigned char)fingerprint[i]);
    }
    fprintf(stderr, "\n");
#endif
    /* check what authentication methods are available */
    char *userauthlist = libssh2_userauth_list(session, m_username, strlen(m_username));

#ifdef __DEBUG__
    printf("Authentication methods: %s\n", userauthlist);
#endif
    if (strstr(userauthlist, "password") != NULL) {
        auth_pw |= 1;
    }
    if (strstr(userauthlist, "keyboard-interactive") != NULL) {
        auth_pw |= 2;
    }
    if (strstr(userauthlist, "publickey") != NULL) {
        auth_pw |= 4;
    }

    if (auth_pw & 1) {
        // We could authenticate via password
        while ((rc = libssh2_userauth_password(session, m_username, m_password)) ==
                LIBSSH2_ERROR_EAGAIN);
        if (rc) {
            printf("Authentication by password failed.\n");
            Shutdown();
            return 2;
        }
    }  else if (auth_pw & 2) {
        /* Or via keyboard-interactive */
        if (libssh2_userauth_keyboard_interactive(session, m_username,

                &kbd_callback) ) {
            printf("\tAuthentication by keyboard-interactive failed!\n");
            Shutdown();
            return 2;
        } else {
            printf("\tAuthentication by keyboard-interactive succeeded.\n");
        }
    } else if (auth_pw & 4) {
        // Or by public key
        while ((rc = libssh2_userauth_publickey_fromfile(session, m_username,
                     "/home/username/.ssh/id_rsa.pub",
                     "/home/username/.ssh/id_rsa",
                     m_password)) == LIBSSH2_ERROR_EAGAIN);
        if (rc) {
#ifdef __DEBUG__
            fprintf(stderr, "\tAuthentication by public key failed\n");
#endif
            Shutdown();
            return 2;
        }
    }
    return 1;
}

int SSHClient::Upload(const char *inputfile, const char *remotefile)
{

    FILE *local;
    int rc;
#if defined(HAVE_IOCTLSOCKET)
    long flag = 1;
#endif
    char mem[1024];
    size_t nread, sent;
    char *ptr;
    struct stat fileinfo;

    local = fopen(inputfile, "rb");
    if (!local) {
        fprintf(stderr, "Can't local file %s\n", inputfile);
        //Shutdown();
        return -1;
    }

    stat(inputfile, &fileinfo);

    /* Request a file via SCP */
    do {
        channel = libssh2_scp_send(session, remotefile, 0x1FF & fileinfo.st_mode,
                                   (unsigned long)fileinfo.st_size);

        if ((!channel) && (libssh2_session_last_errno(session) !=
                           LIBSSH2_ERROR_EAGAIN)) {
            char *err_msg;

            libssh2_session_last_error(session, &err_msg, NULL, 0);
#ifdef __DEBUG__
            fprintf(stderr, "%s\n", err_msg);
#endif
            //Shutdown();
            return -1;
        }
    } while (!channel);

#ifdef __DEBUG__
    fprintf(stderr, "SCP session waiting to send file\n");
#endif
    do {
        nread = fread(mem, 1, sizeof(mem), local);
        if (nread <= 0) {
            /* end of file */
            break;
        }
        ptr = mem;
        sent = 0;

        do {
            /* write the same data over and over, until error or completion */
            rc = libssh2_channel_write(channel, ptr, nread);
            if (LIBSSH2_ERROR_EAGAIN == rc) { /* must loop around */
                continue;
            } else if (rc < 0) {
#ifdef __DEBUG__
                fprintf(stderr, "ERROR %d\n", rc);
#endif
            } else {
                /* rc indicates how many bytes were written this time */
                sent += rc;
            }
        } while (rc > 0 && sent < nread);
        ptr += sent;
        nread -= sent;
    } while (1);

#ifdef __DEBUG__
    fprintf(stderr, "Sending EOF\n");
#endif
    while (libssh2_channel_send_eof(channel) == LIBSSH2_ERROR_EAGAIN);
#ifdef __DEBUG__
    fprintf(stderr, "Waiting for EOF\n");
#endif
    while (libssh2_channel_wait_eof(channel) == LIBSSH2_ERROR_EAGAIN);
#ifdef __DEBUG__
    fprintf(stderr, "Waiting for channel to close\n");
#endif
    while (libssh2_channel_wait_closed(channel) == LIBSSH2_ERROR_EAGAIN);

    libssh2_channel_free(channel);
    channel = NULL;
    return 1;
}

wxArrayString SSHClient::GetOutputInfo(const char *command)
{

    wxString strout = wxEmptyString;
    wxArrayString arystrout;
    char *localname;
    char *rcommand;

    const char *rmlog = "rm -rf exec.log";
    RemoteExec(rmlog, strlen(rmlog), NULL);

    rcommand = (char *)malloc(strlen(command) + 20);
    memset(rcommand, 0x0, sizeof(*rcommand));
    rcommand[0] = 0;
    strncat(rcommand, command, strlen(command));
    strncat(rcommand, " 1>exec.log 2>&1\0", 16);
#ifdef __DEBUG__
    fprintf(stderr, "%s\n", rcommand);
#endif
    if(RemoteExec(rcommand, strlen(rcommand), NULL) == 1) {
        wxString fileName = EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("exec.log");
        if(wxFileExists(fileName))wxRemoveFile(fileName);
        //wxMessageBox(fileName);
        StringUtil::String2CharPointer(&localname, fileName);
        Download(localname, "exec.log");
        arystrout = FileUtil::LoadStringLineFile(fileName);
    } else {
        strout = wxT("Command Execute Failed!");
        arystrout.Add(strout);
    }
    return arystrout;
}

int SSHClient::RemoteExec(const char *command, int len, const char *cpath)
{
    int rc;
    int nfds = 1;
    int bytecount = 0;
    LIBSSH2_POLLFD *fds = NULL;
#ifdef __DEBUG__
    fprintf(stderr, "%s ", command);
    fprintf(stderr, "%d\n", len);
#endif
    /* Open a channel */
    channel  = libssh2_channel_open_session(session);
    if ( channel == NULL ) {
        // wxMessageBox(wxT("Failed to open a new channel"));
        Shutdown();
        close(sock);
        return -1;
    }


    /* Request a PTY */
    rc = libssh2_channel_request_pty( channel, "vt100");
    if (rc != 0) {
        // wxMessageBox(wxT("Failed to request a pty"));
        Shutdown();
        close(sock);
        return -1;
    }
    rc = libssh2_channel_shell(channel);
    if (rc != 0) {
        // wxMessageBox(wxT("libssh2_channel_shell"));
        Shutdown();
        close(sock);
        return -1;
    }
    char buffer[4000];
    unsigned int nsend = 0;

    if(cpath != NULL)
    {
        char cdpath[128];
        memset(cdpath, 0x0, sizeof(cdpath));
        strcat(cdpath, "cd ");
        strcat(cdpath, cpath);
        strcat(cdpath, "\r\n");
        do {
            rc = libssh2_channel_write(channel, cdpath, strlen(cdpath));
            if(rc == LIBSSH2_ERROR_EAGAIN) {
                continue;
            } else if(rc < 0) {
#ifdef __DEBUG__
                fprintf(stderr, "Write Error!\n");
#endif
            } else {
                nsend += rc;
            }
        } while(rc > 0 && nsend < strlen(cdpath));
    }
    nsend = 0;
    do {
        rc = libssh2_channel_write(channel, command, len);
        if(rc == LIBSSH2_ERROR_EAGAIN) {
            continue;
        } else if(rc < 0) {
#ifdef __DEBUG__
            fprintf(stderr, "Write Error!\n");
#endif
        } else {
            nsend += rc;
        }
    } while(rc > 0 && (int)nsend < len);

    if ((fds = (LIBSSH2_POLLFD *)malloc (sizeof (LIBSSH2_POLLFD))) == NULL) {
        return -1;
    }

    fds[0].type = LIBSSH2_POLLFD_CHANNEL;
    fds[0].fd.channel = channel;
    fds[0].events = LIBSSH2_POLLFD_POLLIN;
    fds[0].revents = LIBSSH2_POLLFD_POLLIN;

    rc = libssh2_poll(fds, nfds, 0);
    if (rc > 0) {
        rc = libssh2_channel_read(channel, buffer, sizeof(buffer));
        if(rc > 0) {
            bytecount += rc;
#ifdef __DEBUG__
            fprintf(stdout, "%s", buffer);
            fflush(stdout);
#endif
        }
    }
    nsend = 0;
    do {
        rc = libssh2_channel_write(channel, "\r\nexit\r\n", strlen("\r\nexit\r\n"));
        if(rc == LIBSSH2_ERROR_EAGAIN) {
            continue;
        } else if(rc < 0) {
#ifdef __DEBUG__
            fprintf(stderr, "Write Error!\n");
#endif
        } else {
            nsend += rc;
        }
    } while(rc > 0 && nsend < strlen("\r\nexit\r\n"));
#ifdef __DEBUG__
    fprintf(stderr, "send eof!\n");
#endif
    while(libssh2_channel_send_eof(channel) == LIBSSH2_ERROR_EAGAIN);
#ifdef __DEBUG__
    fprintf(stderr, "wait for eof!\n");
#endif
    while(libssh2_channel_wait_eof(channel) == LIBSSH2_ERROR_EAGAIN);
#ifdef __DEBUG__
    fprintf(stderr, "Wait for close!\n");
#endif
    while(libssh2_channel_wait_closed(channel) == LIBSSH2_ERROR_EAGAIN);
#ifdef __DEBUG__
    fprintf(stderr, "channel close!\n");
#endif
    free (fds);

    if (channel) {
        libssh2_channel_free (channel);
        channel = NULL;
    }
    return 1;
}
