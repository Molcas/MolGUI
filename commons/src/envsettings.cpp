/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
*               2021, Ignacio Fdez. Galván                             *
***********************************************************************/

#include "envsettings.h"
#include <wx/dirdlg.h>
#include <wx/filedlg.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/textfile.h>
#include <wx/filename.h>
#include <wx/textdlg.h> 
#include <wx/msgdlg.h>
#include <wx/statline.h>

#include "fileutil.h"
#include "xmlfile.h"
#include "envutil.h"
#include "stringutil.h"
#include "mingutil.h"
#include "resource.h"
#include "dlgtext.h"

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
static EnvSettingsData *envInstance = NULL;

EnvSettingsData::EnvSettingsData(wxString &fullName, wxString rootName)
{
    m_fullFileName = fullName;
}

EnvSettingsData* EnvSettingsData::Get(void)
{
    if(!envInstance) {
        wxString path = EnvUtil::GetUserDataDir() + wxXML_SETTING_FILE_PATH;
        envInstance = new EnvSettingsData(path, wxXML_SETTING_FILE_ROOT);

        if(!wxFileExists(path)) {
            wxMessageBox(wxT("Please set the environment under menu \"Setting\" at first." ), wxT("Warning"));

            wxString molcasPath = wxEmptyString;

            wxString wstr = wxGetUserHome() + platform::PathSep() + wxT("WorkSpace");
            wxString molcasGVpath = wxEmptyString;
            wxString pegamoidPath = wxEmptyString;
            wxString moldenPath = wxEmptyString;
            wxString othersoftPath = wxEmptyString;
            wxString mopacPath = wxEmptyString;
            wxString mopacLibPath = wxEmptyString;
            wxString platformType = EnvUtil::GetPlatform();

	    wxPathList systemPath = wxPathList();
            systemPath.AddEnvList(wxT("PATH"));
            
            molcasPath = systemPath.FindAbsoluteValidPath(wxT("pymolcas"));
            molcasGVpath = wxT("./xbin") + platform::PathSep() + wxT("gv.exe");
            if(!wxFileExists(molcasGVpath)) {
                molcasGVpath = wxEmptyString;
            }
            pegamoidPath = systemPath.FindAbsoluteValidPath(wxT("pegamoid.py"));
            if(!wxFileExists(pegamoidPath)) {
                pegamoidPath = wxEmptyString;
            }
            moldenPath = systemPath.FindAbsoluteValidPath(wxT("molden"));
            if(!wxFileExists(moldenPath)) {
                moldenPath = wxEmptyString;
            }
            mopacPath = systemPath.FindAbsoluteValidPath(wxT("mopac"));
            if(!wxFileExists(mopacPath)) {
                mopacPath = systemPath.FindAbsoluteValidPath(wxT("MOPAC2016.exe"));
                if(!wxFileExists(mopacPath)) {
                    mopacPath = wxEmptyString;
                }
            }
            mopacLibPath = wxT("xlib");
            if(!wxDirExists(mopacLibPath)) {
                mopacLibPath = wxEmptyString;
            }
            othersoftPath = wxEmptyString;
            wxString configFileName = envInstance->m_fullFileName;
            if(!wxDirExists(configFileName.BeforeLast(platform::PathSep().GetChar(0))))
                FileUtil::CreateDirectory(configFileName.BeforeLast(platform::PathSep().GetChar(0)));
            StringHash envPaths;
            envPaths[MOLCAS_PATH_ELEMENT_NAME]        = molcasPath;
            envPaths[DEFWORK_PATH_ELEMENT_NAME]       = wstr;
            // envPaths[MOLGV_PATH_ELEMENT_NAME]         = molcasGVpath;
            envPaths[PEGAMOID_PATH_ELEMENT_NAME]      = pegamoidPath;
            envPaths[MOLDEN_PATH_ELEMENT_NAME]        = moldenPath;
            envPaths[OTHERSOFT_PATH_ELEMENT_NAME]     = othersoftPath;
            envPaths[MOPAC_PATH_ELEMENT_NAME]         = mopacPath;
            envPaths[MOPAC_LIB_PATH_ELEMENT_NAME]     = mopacLibPath;
            FileUtil::WriteElemsToFile(configFileName, envPaths);
        }

        // Set molden path to the user specified molden path
        // If molden is not specified, set it to gv by default
        // If molden is not specified, set it to pegamoid by default
        wxString moldenPath = envInstance->GetMoldenPath();
        if(moldenPath.IsEmpty())
            // moldenPath = envInstance->GetMolGVPath();
            moldenPath = envInstance->GetPegaPath();
        wxString fileTypesConfigFileName = EnvUtil::GetUserDataDir() + MOLCAS_FILE_TYPES_CONFIG_FILE;
        wxTextFile realFile(fileTypesConfigFileName);
        if(!realFile.Exists())
        {
            realFile.Create();
            // realFile.AddLine(wxT("grid='") + envInstance->GetMolGVPath() + wxT(" $f'"));
            realFile.AddLine(wxT("h5='") + envInstance->GetPegaPath() + wxT(" $f'"));
            realFile.AddLine(wxT("grid='") + envInstance->GetPegaPath() + wxT(" $f'"));
            realFile.AddLine(wxT("molden='") + moldenPath + wxT(" $f'"));
            realFile.Write();
            realFile.Close();
        }
    }
    return envInstance;
}

void EnvSettingsData::Free(void)
{
    if(envInstance != NULL)
        delete envInstance;
    envInstance = NULL;
}

wxString EnvSettingsData::GetElement(const wxString &elemName)
{
    if(m_fullFileName.IsEmpty())
        return wxEmptyString;
    return FileUtil::GetElemFromFile(m_fullFileName, elemName);
}
wxString EnvSettingsData::GetMolcasPath()
{
    wxString str = GetElement(MOLCAS_PATH_ELEMENT_NAME);
    if(str.IsSameAs(wxT("NULL")))
        return wxEmptyString;
    else
        return str;
}
wxString EnvSettingsData::GetMopacPath()
{
    wxString str = GetElement(MOPAC_PATH_ELEMENT_NAME);
    if(str.IsSameAs(wxT("NULL")))
        return wxEmptyString;
    else
        return str;
}
wxString EnvSettingsData::GetMopacLibPath()
{
    return GetElement(MOPAC_LIB_PATH_ELEMENT_NAME);
}

wxString EnvSettingsData::GetDefWorkPath()
{
    wxString str = GetElement(DEFWORK_PATH_ELEMENT_NAME);
    if(str.IsSameAs(wxT("NULL")))
        return wxEmptyString;
    else
        return str;
}

wxString EnvSettingsData::GetMoldenPath()
{
    wxString str = GetElement(MOLDEN_PATH_ELEMENT_NAME);
    if(str.IsSameAs(wxT("NULL")))
        return wxEmptyString;
    else
        return str;
}
// wxString EnvSettingsData::GetMolGVPath()
// {
//     wxString str = GetElement(MOLGV_PATH_ELEMENT_NAME);
//     //wxMessageBox(str);
//     if(str.IsSameAs(wxT("NULL")))
//         return wxEmptyString;
//     else
//         return str;
// }
wxString EnvSettingsData::GetPegaPath()
{
    wxString str = GetElement(PEGAMOID_PATH_ELEMENT_NAME);
    //wxMessageBox(str);
    if(str.IsSameAs(wxT("NULL")))
        return wxEmptyString;
    else
        return str;
}
wxString EnvSettingsData::GetOthersoftPath()
{
    wxString str = GetElement(OTHERSOFT_PATH_ELEMENT_NAME);
    //wxMessageBox(str);
    if(str.IsSameAs(wxT("NULL")))
        return wxEmptyString;
    else
        return str;
}
wxString EnvSettingsData::GetFileTypeCommand(const wxString &type)
{
    if(type.Strip().IsEmpty())
        return wxEmptyString;
    wxString fileTypesConfigFileName = EnvUtil::GetUserDataDir() + MOLCAS_FILE_TYPES_CONFIG_FILE;
    wxString cmd = FileUtil::GetElemFromFile(fileTypesConfigFileName, type);
    cmd.Replace(wxT("'"), wxT(""));
    cmd.Replace(wxT("\""), wxT(""));
    if(cmd.IsSameAs(wxT("NULL")))
        return wxEmptyString;
    return cmd;
}

wxString EnvSettingsData::GetChemSoftDir(ProjectType type)
{
    wxString dir = wxEmptyString;
    switch(type) {
    case PROJECT_SIMUCAL:
        dir = wxT("./simucal");
        dir = FileUtil::NormalizeFileName(dir);
        break;
    case PROJECT_MOLCAS:
#if defined (__WINDOWS__)
        dir = wxT("./molcas");
        dir = FileUtil::NormalizeFileName(dir);
#else
        dir = GetMolcasPath();
        if(dir.IsEmpty())
            dir = wxT("pymolcas");
#endif
        break;
    case PROJECT_GAUSSIAN:
        dir = wxT("gaussian");
        break;
    case PROJECT_QCHEM:
        //                #if defined (__WINDOWS__)
        dir = wxT("./qchem");
        dir = FileUtil::NormalizeFileName(dir);
        //                #else
        //                        dir = wxT("qchem");
        //                #endif
        break;
    case PROJECT_DENS:
        dir = wxT("./modules");
        dir = FileUtil::NormalizeFileName(dir);
        break;
    default:

        break;
    }
    return dir;
}

void EnvSettingsData::Save(const StringHash& envPaths, const StringHash& fileTypes)
{
    if(!wxDirExists(m_fullFileName.BeforeLast(platform::PathSep().GetChar(0))))
        FileUtil::CreateDirectory(m_fullFileName.BeforeLast(platform::PathSep().GetChar(0)));
    FileUtil::WriteElemsToFile(m_fullFileName, envPaths);
    SetFullPath(m_fullFileName);

    wxString fileTypesConfigFileName = EnvUtil::GetUserDataDir() + MOLCAS_FILE_TYPES_CONFIG_FILE;
    FileUtil::WriteElemsToFile(fileTypesConfigFileName, fileTypes);
}

MOPAC_CHECK EnvSettingsData::CheckMopac()
{
    wxString mopacExe = GetMopacPath();
    wxString mopacLibPath = GetMopacLibPath();

    return CheckMopac(mopacExe, mopacLibPath);
}

MOPAC_CHECK EnvSettingsData::CheckMopac(const wxString& mopacExe, const wxString& mopacLibPath)
{
    MOPAC_CHECK retval = MOPAC_ERROR_PATH;

    // mopacExe shouldn't be empty
    // wxExecute doesn't complain if empty so we check here.
    if(mopacExe.Strip().IsEmpty())
        return retval;

    wxString mopacDir = wxFileName(mopacExe).GetPathWithSep();

    SimpleProcess *proc = new SimpleProcess(wxPROCESS_REDIRECT, wxT("\n"));
    proc->Redirect();
    wxSetEnv(wxT("LD_LIBRARY_PATH"), mopacLibPath);
    wxSetEnv(wxT("MOPAC_LICENSE"), "./");
    // wxMessageBox(mopacExe);
    wxString cwd = wxGetCwd();
    wxSetWorkingDirectory(mopacDir);
    wxExecute(mopacExe, wxEXEC_ASYNC, proc);
    // std::cout << "EnvSettingsData::CheckMopac: " << mopacExe << std::endl;
    wxSetWorkingDirectory(cwd);

    // Wait until the process try to output something on err
    // Seems to work even when nothing is written on stderr...
    // except if mopacExe is empty...
    wxMilliSleep(200);
    while( !proc->IsErrorAvailable());

    wxString tmpStr, tmpErr;
    wxInputStream *is = proc->GetInputStream();
    wxInputStream *es = proc->GetErrorStream();
    wxTextInputStream tos(*is);
    wxTextInputStream tes(*es);

    // Read standard output.
    while(proc->IsInputAvailable())
    {
        tmpStr += tos.GetChar();
    }

    // Read standard error
    while(proc->IsErrorAvailable())
    {
        tmpErr += tes.GetChar();
    }
    // std::cout << "Err:" << tmpErr.fn_str() << ";" << std::endl;
    // std::cout << "Err:" << mopacDir.fn_str() << ";" << std::endl;
    // std::cout << "Err:" << wxGetCwd().fn_str() << ";" << std::endl;
    
    // Detect if there is an error
    if(StringUtil::Contains(tmpErr, wxT("libiomp5.so")))
    {
        retval = MOPAC_ERROR_LIB;
    }
    else if(StringUtil::Contains(tmpErr, wxT("execvp")))
    {
        retval = MOPAC_ERROR_PATH;
    } 
    else if(StringUtil::Contains(tmpErr, wxT("license-key")))
    {
        // MOPAC asks to press enter to exit
        wxOutputStream *os = proc->GetOutputStream();
        if(os)
        {
            wxTextOutputStream tos(*os);
            tos.WriteString(wxT("\n"));
            proc->CloseOutput();
        }

        retval = MOPAC_ERROR_PASSWORD;
    }
    else if(StringUtil::Contains(tmpErr, wxT("data-set")))
    {
        // Everything is good !
        // MOPAC asks to press enter to exit
        wxOutputStream *os = proc->GetOutputStream();
        if(os)
        {
            wxTextOutputStream tos(*os);
            tos.WriteString(wxT("\n"));
            proc->CloseOutput();
        }
        retval = MOPAC_OK;
    }
    else
    {
        retval = MOPAC_ERROR_UNKNOWN;
    }
    // I can't delete the process now in case it has not terminated...
    // Possible leak...
    proc->Detach();

    return retval;
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

//....................................................................//
int CTRL_ENVSET_ID_TEXT_MOLCAS_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_MOLCAS_PATH = wxNewId();
int CTRL_ENVSET_ID_TEXT_DEF_WORK_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_DEF_WORK_PATH = wxNewId();
int CTRL_ENVSET_ID_TEXT_MOPAC_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_MOPAC_PATH = wxNewId();
int CTRL_ENVSET_ID_TEXT_MOPAC_LIB_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_MOPAC_LIB_PATH = wxNewId();
int CTRL_ENVSET_ID_TEXT_MOLDEN_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_MOLDEN_PATH = wxNewId();
int CTRL_ENVSET_ID_TEXT_MOLGV_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_MOLGV_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_PEGAMOID_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_OTHERSOFT_PATH = wxNewId();
int CTRL_ENVSET_ID_BTN_DEFAULT_FILE_TYPES = wxNewId();
int CTRL_ENVSET_ID_BTN_MOPAC_LICENSE = wxNewId();

int CTRL_ENVSET_ID_BTN_OK = wxNewId();
int CTRL_ENVSET_ID_BTN_CANCEL = wxNewId();
//....................................................................//
BEGIN_EVENT_TABLE(EnvSettings, wxDialog)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_MOLCAS_PATH, EnvSettings::OnMolcasPath)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_DEF_WORK_PATH, EnvSettings::OnDefWorkPath)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_MOPAC_PATH, EnvSettings::OnMopacPath)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_MOPAC_LIB_PATH, EnvSettings::OnMopacLibPath)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_MOLDEN_PATH, EnvSettings::OnMoldenPath)
    // EVT_BUTTON(CTRL_ENVSET_ID_BTN_MOLGV_PATH, EnvSettings::OnMolGVPath)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_PEGAMOID_PATH, EnvSettings::OnPegaPath)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_OTHERSOFT_PATH, EnvSettings::OnOthersoftPath)

    EVT_BUTTON(CTRL_ENVSET_ID_BTN_DEFAULT_FILE_TYPES, EnvSettings::OnDefaultFileTypes)
    // EVT_BUTTON(CTRL_ENVSET_ID_BTN_CHECK_MOPAC, EnvSettings::OnCheckMopac)

    EVT_BUTTON(wxID_OK, EnvSettings::OnOk)
    EVT_BUTTON(wxID_CANCEL, EnvSettings::OnCancel)

    EVT_TEXT_ENTER(CTRL_ENVSET_ID_TEXT_MOPAC_PATH, EnvSettings::OnCheckMopac)
    EVT_TEXT_ENTER(CTRL_ENVSET_ID_TEXT_MOPAC_LIB_PATH, EnvSettings::OnCheckMopac)
    EVT_BUTTON(CTRL_ENVSET_ID_BTN_MOPAC_LICENSE, EnvSettings::OnMopacLicense)

    EVT_TEXT_ENTER(CTRL_ENVSET_ID_TEXT_MOLCAS_PATH, EnvSettings::OnCheckMolcas)

END_EVENT_TABLE()

//....................................................................//
EnvSettings::EnvSettings(wxWindow *parent, wxWindowID id, 
                         const wxString &title, 
                         const wxPoint &pos,
                         const wxSize &size , 
                         long style): wxDialog(parent, id, title, pos, size, style)
{
    // #ifdef  __WINDOWS__
    // #ifndef __SIMU_Q__
    //     wxDialog::Create(parent, id, title, pos, wxSize(525, 350), style);
    // #else
    //     wxDialog::Create(parent, id, title, pos, wxSize(525, 165), style);
    // #endif
    // #else
    // #ifndef __SIMU_Q__
    //     wxDialog::Create(parent, id, title, pos, wxSize(525, 350), style);
    // #else
    //     wxDialog::Create(parent, id, title, pos, wxSize(525, 145), style);
    // #endif
    // #endif
    // wxDialog::Create(parent, id, title, pos, size, style);
    m_pTcMopacPath = NULL;
    m_pTcMopacLibPath = NULL;
    m_pTcDefWorkPath = NULL;
#ifndef __SIMU_Q__
    m_pTcMoldenPath = NULL;
    // m_pTcMolGVPath = NULL;
    m_pTcPegaPath = NULL;
    m_pTcOthersoftPath = NULL;
    m_pTcFileTypes = NULL;
    m_pLabelMopacCheck = NULL;
#endif
    
    CreateUI();
    InitCompVal();
}

EnvSettings::~EnvSettings()
{
    ;
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
//      Create UI
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
void EnvSettings::CreateUI()
{
    int labelLength = 220;
    int labelHeight = 25;
    int pathButtonLength = 45;
    int pathButtonHeight = 25;
    wxBoxSizer *sizerTop = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizerPaths = new wxBoxSizer(wxVERTICAL);

    // Molcas Check Label
    m_pLabelMolcasCheck = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize);
    wxFont font = m_pLabelMolcasCheck->GetFont();
    font.SetStyle(wxFONTSTYLE_ITALIC );
    m_pLabelMolcasCheck->SetFont(font);
    m_pLabelMolcasCheck->SetForegroundColour(wxColor(200,0,0));
    sizerPaths->Add(m_pLabelMolcasCheck, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 7);

    // Molcas User path
    wxStaticText *label = new wxStaticText(this, wxID_ANY, wxT("Path to OpenMolcas driver:"), wxDefaultPosition, wxSize(labelLength, labelHeight));
    sizerPaths->Add(label, 0, wxALIGN_LEFT | wxTOP | wxLEFT, 5);
    wxBoxSizer *molcasPathSizer = new wxBoxSizer(wxHORIZONTAL);
    
    label = new wxStaticText(this, wxID_ANY, wxT("If pymolcas is not available or to use a specific version"), wxDefaultPosition, wxDefaultSize);
    font = label->GetFont();
    font.MakeSmaller();
    font.SetStyle(wxFONTSTYLE_ITALIC );
    label->SetFont(font);
    sizerPaths->Add(label, 0, wxEXPAND | wxLEFT, 7);

    m_pTcMolcasPath = new wxTextCtrl(this, CTRL_ENVSET_ID_TEXT_MOLCAS_PATH, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
    molcasPathSizer->Add(m_pTcMolcasPath, 2,  wxEXPAND | wxALIGN_LEFT, 5);
    wxButton *button = new wxButton(this, CTRL_ENVSET_ID_BTN_MOLCAS_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    molcasPathSizer->Add(button, 0, wxALIGN_CENTER, 5);
    sizerPaths->Add(molcasPathSizer, 0, wxEXPAND | wxALIGN_LEFT | wxLEFT | wxRIGHT, 7);

    wxStaticLine *hLine = new wxStaticLine(this);
    sizerPaths->Add(hLine, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 7);

    // Input Output (i.e. Workspace)
    label = new wxStaticText(this, wxID_ANY, wxT("Input and Output: "), wxDefaultPosition, wxSize(labelLength, labelHeight));
    sizerPaths->Add(label, 0, wxALIGN_LEFT | wxTOP | wxLEFT, 5);
    wxBoxSizer *ioSizer = new wxBoxSizer(wxHORIZONTAL);
    m_pTcDefWorkPath = new wxTextCtrl(this, CTRL_ENVSET_ID_TEXT_DEF_WORK_PATH, wxEmptyString, wxDefaultPosition, wxDefaultSize);
    ioSizer->Add(m_pTcDefWorkPath, 2,  wxEXPAND | wxALIGN_LEFT, 5);
    button = new wxButton(this, CTRL_ENVSET_ID_BTN_DEF_WORK_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    ioSizer->Add(button, 0, wxALIGN_CENTER, 5);
    sizerPaths->Add(ioSizer, 0, wxEXPAND | wxALIGN_LEFT | wxLEFT | wxRIGHT, 7);

    // Molden
    label = new wxStaticText(this, wxID_ANY, wxT("Molden Location: "), wxDefaultPosition, wxSize(labelLength, labelHeight));
    sizerPaths->Add(label, 0, wxALIGN_LEFT | wxTOP | wxLEFT, 5);
    wxBoxSizer *moldenSizer = new wxBoxSizer(wxHORIZONTAL);
    m_pTcMoldenPath = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    moldenSizer->Add(m_pTcMoldenPath, 2, wxEXPAND | wxALIGN_LEFT, 5);
    button = new wxButton(this, CTRL_ENVSET_ID_BTN_MOLDEN_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    moldenSizer->Add(button, 0, wxALIGN_CENTER, 5);
    sizerPaths->Add(moldenSizer, 0, wxEXPAND | wxALIGN_LEFT | wxLEFT | wxRIGHT, 5);

    // gv
    //label = new wxStaticText(this, wxID_ANY, wxT("Molcas gv Location: "), wxDefaultPosition, wxSize(labelLength, labelHeight));
    //sizerPaths->Add(label, 0, wxALIGN_LEFT | wxTOP | wxLEFT, 5);
    //wxBoxSizer *gvSizer = new wxBoxSizer(wxHORIZONTAL);
    //m_pTcMolGVPath = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    //gvSizer->Add(m_pTcMolGVPath, 2, wxEXPAND | wxALIGN_LEFT, 5);
    //button = new wxButton(this, CTRL_ENVSET_ID_BTN_MOLGV_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    //gvSizer->Add(button, 0, wxALIGN_CENTER, 5);
    //sizerPaths->Add(gvSizer, 0, wxEXPAND | wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 7);

    // Pegamoid
    label = new wxStaticText(this, wxID_ANY, wxT("Pegamoid Location: "), wxDefaultPosition, wxSize(labelLength, labelHeight));
    sizerPaths->Add(label, 0, wxALIGN_LEFT | wxTOP | wxLEFT, 5);
    wxBoxSizer *pegaSizer = new wxBoxSizer(wxHORIZONTAL);
    m_pTcPegaPath = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    pegaSizer->Add(m_pTcPegaPath, 2, wxEXPAND | wxALIGN_LEFT, 5);
    button = new wxButton(this, CTRL_ENVSET_ID_BTN_PEGAMOID_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    pegaSizer->Add(button, 0, wxALIGN_CENTER, 5);
    sizerPaths->Add(pegaSizer, 0, wxEXPAND | wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 7);

    // Othersoft
    label = new wxStaticText(this, wxID_ANY, wxT("Other software Location: "), wxDefaultPosition, wxSize(labelLength, labelHeight));
    sizerPaths->Add(label, 0, wxALIGN_LEFT | wxTOP | wxLEFT, 5);
    wxBoxSizer *othersoftSizer = new wxBoxSizer(wxHORIZONTAL);
    m_pTcOthersoftPath = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
    othersoftSizer->Add(m_pTcOthersoftPath, 2, wxEXPAND | wxALIGN_LEFT, 5);
    button = new wxButton(this, CTRL_ENVSET_ID_BTN_OTHERSOFT_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    othersoftSizer->Add(button, 0, wxALIGN_CENTER, 5);
    sizerPaths->Add(othersoftSizer, 0, wxEXPAND | wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 9);

    // Mopac
    wxStaticBoxSizer *mopacConfSizer = new wxStaticBoxSizer(wxVERTICAL, this, _T("MOPAC Configuration"));

    // Mopac Check Label
    m_pLabelMopacCheck = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize);
    font = m_pLabelMopacCheck->GetFont();
    font.SetStyle(wxFONTSTYLE_ITALIC );
    m_pLabelMopacCheck->SetFont(font);
    m_pLabelMopacCheck->SetForegroundColour(wxColor(200,0,0));
    mopacConfSizer->Add(m_pLabelMopacCheck, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 7);

    // Mopac Exe
    label = new wxStaticText(this, wxID_ANY, wxT("Mopac Location: "), wxDefaultPosition, wxSize(labelLength, labelHeight));
    mopacConfSizer->Add(label, 0, wxALIGN_LEFT | wxTOP, 5); 
    wxBoxSizer *mopacSizer = new wxBoxSizer(wxHORIZONTAL);
    m_pTcMopacPath = new wxTextCtrl(this, CTRL_ENVSET_ID_TEXT_MOPAC_PATH, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
    mopacSizer->Add(m_pTcMopacPath, 1, wxEXPAND | wxALIGN_LEFT, 5);
    button = new wxButton(this, CTRL_ENVSET_ID_BTN_MOPAC_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    mopacSizer->Add(button, 0, wxALIGN_CENTER, 5);
    mopacConfSizer->Add(mopacSizer, 1, wxEXPAND | wxALIGN_LEFT | wxBOTTOM, 5);

    // Mopac Lib
    label = new wxStaticText(this, wxID_ANY, wxT("Mopac Library Directory: "), wxDefaultPosition, wxSize(labelLength, labelHeight));
    mopacConfSizer->Add(label, 0, wxALIGN_LEFT, 5);
    wxBoxSizer *mopacLibSizer = new wxBoxSizer(wxHORIZONTAL);
    m_pTcMopacLibPath = new wxTextCtrl(this, CTRL_ENVSET_ID_TEXT_MOPAC_LIB_PATH, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
    mopacLibSizer->Add(m_pTcMopacLibPath, 2,  wxEXPAND | wxALIGN_LEFT, 5);
    button = new wxButton(this, CTRL_ENVSET_ID_BTN_MOPAC_LIB_PATH, wxT("..."), wxDefaultPosition, wxSize(pathButtonLength, pathButtonHeight));
    mopacLibSizer->Add(button, 0, wxALIGN_CENTER, 5);
    mopacConfSizer->Add(mopacLibSizer, 0, wxEXPAND | wxALIGN_LEFT, 5);

    button = new wxButton(this, CTRL_ENVSET_ID_BTN_MOPAC_LICENSE, wxT("License-Key"));
    mopacConfSizer->Add(button, 0, wxALIGN_LEFT);
    button->Disable();

    sizerPaths->Add(mopacConfSizer, 0, wxEXPAND | wxALL, 5);
    sizerTop->Add(sizerPaths, 0, wxEXPAND|wxALIGN_LEFT, 5);

    // File types
    wxStaticBoxSizer *fileSizer = new wxStaticBoxSizer(wxVERTICAL, this, _T("Commands to open file types"));
    m_pTcFileTypes = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100,100), wxTE_MULTILINE);
    fileSizer->Add(m_pTcFileTypes, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);
    // m_pTcFileTypes2 = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100,100), wxTE_MULTILINE);
    // fileSizer->Add(m_pTcFileTypes2, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);
    button = new wxButton(this, CTRL_ENVSET_ID_BTN_DEFAULT_FILE_TYPES, wxT("Default"), wxDefaultPosition);
    fileSizer->Add(button, 0, wxALIGN_LEFT | wxBOTTOM, 5);
    m_pLabelFileTypesHelp = new wxStaticText(this, wxID_ANY, GetFileTypesHelp(), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
    fileSizer->Add(m_pLabelFileTypesHelp, 0, wxEXPAND);
    sizerTop->Add(fileSizer, 0, wxEXPAND | wxALIGN_LEFT| wxALL,5);
    // sizerTop->Add(100,0);
    
    wxBoxSizer *frameSizer = new wxBoxSizer(wxVERTICAL);
    frameSizer->Add(sizerTop, 0, wxALIGN_LEFT);
    frameSizer->Add(CreateButtonSizer(wxOK | wxCANCEL), wxSizerFlags().Right().Border(wxALL,5));

    SetSizer(frameSizer);
    frameSizer->SetSizeHints(this);
    Fit();
}

wxString EnvSettings::GetFileTypesHelp()
{
    wxString help = wxT("type='cmd $f' ($f will be replaced by the filename)\n");
    help += wxT("\tex: molden='/usr/bin/molden $f'\n");
    help += wxT("The Default button sets the following:\n");
    wxString moldenPath = m_pTcMoldenPath->GetValue();
    if(moldenPath.IsEmpty())
        moldenPath = m_pTcPegaPath->GetValue();
    help += wxT("\th5='") + m_pTcPegaPath->GetValue() + wxT(" $f'\n");
    help += wxT("\tgrid='") + m_pTcPegaPath->GetValue() + wxT(" $f'\n");
    help += wxT("\tmolden='") + moldenPath + wxT(" $f'\n");

    return help;
}

void EnvSettings::InitCompVal()
{
    wxString strTmp;
    strTmp = EnvSettingsData::Get()->GetMolcasPath();
    if(!strTmp.IsSameAs(wxT("NULL")))
        m_pTcMolcasPath->SetValue(strTmp);

    strTmp = EnvSettingsData::Get()->GetDefWorkPath();
    if(!strTmp.IsSameAs(wxT("NULL")))
        m_pTcDefWorkPath->SetValue(strTmp);

    strTmp = EnvSettingsData::Get()->GetMopacPath();
    if(!strTmp.IsSameAs(wxT("NULL")))
        m_pTcMopacPath->SetValue(strTmp);

    m_pTcMopacLibPath->SetValue(EnvSettingsData::Get()->GetMopacLibPath());
    m_pTcMopacLibPath->SetInsertionPointEnd();
#ifndef __SIMU_Q__
    strTmp = EnvSettingsData::Get()->GetMoldenPath();
    if(!strTmp.IsSameAs(wxT("NULL")))
        m_pTcMoldenPath->SetValue(strTmp);
    // std::cout << "molden path: " << m_pTcMoldenPath->GetValue() << std::endl;
    // strTmp = EnvSettingsData::Get()->GetMolGVPath();
    strTmp = EnvSettingsData::Get()->GetPegaPath();
    if(!strTmp.IsSameAs(wxT("NULL")))
        //m_pTcMolGVPath->SetValue(strTmp);
        m_pTcPegaPath->SetValue(strTmp);
    strTmp = EnvSettingsData::Get()->GetOthersoftPath();
    // std::cout << "pega path: " << strTmp << std::endl;
    if(!strTmp.IsSameAs(wxT("NULL")))
        m_pTcOthersoftPath->SetValue(strTmp);

    wxString fileTypesConfigFileName = EnvUtil::GetUserDataDir() + MOLCAS_FILE_TYPES_CONFIG_FILE;
    m_pTcFileTypes->SetValue(FileUtil::LoadStringLineFileStr(fileTypesConfigFileName));
    m_pLabelFileTypesHelp->SetLabel(GetFileTypesHelp());
    Fit();
#endif
    wxCommandEvent e;
    OnCheckMopac(e);
    OnCheckMolcas(e);
}


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
//
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
void    EnvSettings::OnMolcasPath(wxCommandEvent &event)
{
    wxFileDialog fDlg(this);
    if(fDlg.ShowModal() == wxID_OK)
    {
        m_pTcMolcasPath->SetValue(fDlg.GetPath());
    }
    OnCheckMolcas(event);
}
void    EnvSettings::OnDefWorkPath(wxCommandEvent &event)
{
    wxDirDialog dirDlg(this);
    if(dirDlg.ShowModal() == wxID_OK)
        m_pTcDefWorkPath->SetValue(dirDlg.GetPath());
}
void    EnvSettings::OnMopacPath(wxCommandEvent &event)
{
    wxFileDialog   fDlg(this);
    if(fDlg.ShowModal() == wxID_OK)
        m_pTcMopacPath->SetValue(fDlg.GetPath());
    OnCheckMopac(event);
}
void    EnvSettings::OnMopacLibPath(wxCommandEvent &event)
{
    wxDirDialog   fDlg(this);
    if(fDlg.ShowModal() == wxID_OK)
        m_pTcMopacLibPath->SetValue(fDlg.GetPath());
    OnCheckMopac(event);
}

void    EnvSettings::OnMoldenPath(wxCommandEvent &event)
{
#ifndef __SIMU_Q__
    wxFileDialog   fDlg(this);//,wxT("Choose a file"),wxT(""),wxT(""),wxT(""),wxFD_DEFAULT_STYLE|wxFD_FILE_MUST_EXIST);
    if(fDlg.ShowModal() == wxID_OK)
        m_pTcMoldenPath->SetValue(fDlg.GetPath());
    m_pLabelFileTypesHelp->SetLabel(GetFileTypesHelp());
    Fit();
#endif
}
// void    EnvSettings::OnMolGVPath(wxCommandEvent &event)
// {
// #ifndef __SIMU_Q__
//     wxFileDialog   fDlg(this);//,wxT("Choose a file"),wxT(""),wxT(""),wxT(""),wxFD_DEFAULT_STYLE|wxFD_FILE_MUST_EXIST);
//     if(fDlg.ShowModal() == wxID_OK)
//         // m_pTcMolGVPath->SetValue(fDlg.GetPath());
//         m_pTcPegaPath->SetValue(fDlg.GetPath());
//     m_pLabelFileTypesHelp->SetLabel(GetFileTypesHelp());
//     Fit();
// #endif
// }
void    EnvSettings::OnPegaPath(wxCommandEvent &event)
{
#ifndef __SIMU_Q__
    wxFileDialog   fDlg(this);//,wxT("Choose a file"),wxT(""),wxT(""),wxT(""),wxFD_DEFAULT_STYLE|wxFD_FILE_MUST_EXIST);
    if(fDlg.ShowModal() == wxID_OK)
        m_pTcPegaPath->SetValue(fDlg.GetPath());
    m_pLabelFileTypesHelp->SetLabel(GetFileTypesHelp());
    Fit();
#endif
}
void    EnvSettings::OnOthersoftPath(wxCommandEvent &event)
{
#ifndef __SIMU_Q__
    wxFileDialog   fDlg(this);//,wxT("Choose a file"),wxT(""),wxT(""),wxT(""),wxFD_DEFAULT_STYLE|wxFD_FILE_MUST_EXIST);
    if(fDlg.ShowModal() == wxID_OK)
        m_pTcOthersoftPath->SetValue(fDlg.GetPath());
    m_pLabelFileTypesHelp->SetLabel(GetFileTypesHelp());
    Fit();
#endif
}

void EnvSettings::OnDefaultFileTypes(wxCommandEvent &event)
{
    wxString msg = wxT("Setting Default commands will erase all your commands. Do you want to continue?");
    wxMessageDialog warning(this, msg, wxT("Warning"), wxOK | wxCANCEL | wxICON_EXCLAMATION | wxSTAY_ON_TOP);
    if(warning.ShowModal() == wxID_CANCEL)
        return;
    // wxString gvPath = EnvSettingsData::Get()->GetMolGVPath();
    wxString pegaPath = EnvSettingsData::Get()->GetPegaPath();
    wxString moldenPath = EnvSettingsData::Get()->GetMoldenPath();
    if(moldenPath.IsEmpty()) moldenPath = pegaPath;
    wxString def = wxT("h5='") + pegaPath + wxT(" $f'\n");
    def += wxT("grid='") + pegaPath + wxT(" $f'\n");
    def += wxT("molden='") + moldenPath + wxT(" $f'\n");
    m_pTcFileTypes->SetValue(def);
}

void    EnvSettings::OnOk(wxCommandEvent &event)
{
    Save();

    if(IsModal())
        EndModal(1);
}
void    EnvSettings::OnCancel(wxCommandEvent &event)
{
    if(IsModal())
        EndModal(0);
}

void    EnvSettings::OnMopacLicense(wxCommandEvent &event)
{
    wxString pswd = wxGetTextFromUser(wxT("Please enter your MOPAC password"),
                                      wxT("MOPAC License Validation"),
                                      wxEmptyString,
                                      this);

    wxString mopacExe = m_pTcMopacPath->GetValue();
    // mopacExe shouldn't be empty
    // wxExecute doesn't complain if empty so we check here.
    if(mopacExe.Strip().IsEmpty())
        return;
    wxString mopacLibPath = m_pTcMopacLibPath->GetValue();
    wxString mopacDir = wxFileName(mopacExe).GetPath();

    SimpleProcess *proc = new SimpleProcess(wxPROCESS_REDIRECT, wxT("\n"));
    proc->Redirect();
    wxSetEnv(wxT("LD_LIBRARY_PATH"), mopacLibPath);
    wxSetEnv(wxT("MOPAC_LICENSE"), "./");
    // wxMessageBox(mopacExe);
    mopacExe += wxT(" ") + pswd;
    wxString cwd = wxGetCwd();
    wxSetWorkingDirectory(mopacDir);
    wxExecute(mopacExe, wxEXEC_ASYNC, proc);
    // std::cout << "EnvSettingsData::OnMopacLicense: " << mopacExe << std::endl;
    wxSetWorkingDirectory(cwd);

    // Wait until the process try to output something
    // Seems to work even when nothing is written on stderr...
    wxMilliSleep(200);
    while( !proc->IsErrorAvailable());

    wxString tmpStr, tmpErr;
    wxInputStream *is = proc->GetInputStream();
    wxInputStream *es = proc->GetErrorStream();
    wxTextInputStream tis(*is);
    wxTextInputStream tes(*es);
    wxOutputStream *os = proc->GetOutputStream();
    wxTextOutputStream tos(*os);

    // Read standard output.
    while(proc->IsInputAvailable())
    {
        tmpStr += tis.GetChar();
    }

    // Read standard error
    while(proc->IsErrorAvailable())
    {
        tmpErr += tes.GetChar();
    }

    if(StringUtil::Contains(tmpErr, wxT("Password invalid")))
    {
        // MOPAC asks to press enter to continue
        tos.WriteString(wxT("\n"));
        proc->CloseOutput();
        wxMessageBox(wxT("Password invalid"));
        return;
    }
    if(StringUtil::Contains(tmpErr, wxT("License Agreement")))
    {
        // MOPAC asks to press enter to continue
        tos.WriteString(wxT("\n"));
        wxMilliSleep(200);
        while(proc->IsErrorAvailable())
        {
            tmpErr += tes.GetChar();
        }
        #ifdef __DEBUG__
        std::cout << tmpErr.fn_str() << std::endl;
        #endif
        DlgText licenseDialog(this,
                              tmpErr,
                              wxT("Do you accept MOPAC's License Agreement?"),
                              wxDefaultPosition,
                              wxSize(600,550),
                              wxCAPTION | wxRESIZE_BORDER,
                              wxYES | wxNO | wxCENTER | wxNO_DEFAULT,
                              wxHSCROLL | wxTE_MULTILINE | wxTE_READONLY);
        licenseDialog.SetAffirmativeId(wxID_YES);
        licenseDialog.SetEscapeId(wxID_NO);
        if(licenseDialog.ShowModal() == wxID_YES)
        {
            tos.WriteString(wxT("Yes\n"));
            wxMessageBox(wxT("MOPAC License validated"));
        }
        else
        {
            tos.WriteString(wxT("No\n"));
            wxMessageBox(wxT("MOPAC License Validation canceled"),wxEmptyString,wxICON_ERROR|wxOK|wxCENTRE);
        }
    }
    OnCheckMopac(event);
}

void    EnvSettings::OnCheckMopac(wxCommandEvent &event)
{
    wxButton *button = (wxButton*)FindWindow(CTRL_ENVSET_ID_BTN_MOPAC_LICENSE);
    button->Disable();

    MOPAC_CHECK check = EnvSettingsData::Get()->CheckMopac(m_pTcMopacPath->GetValue(), m_pTcMopacLibPath->GetValue());

    switch(check)
    {
    case MOPAC_OK:
        m_pLabelMopacCheck->SetLabel(MOPAC_ERROR_STRING_OK);
        m_pLabelMopacCheck->SetForegroundColour(wxColor(0,200,0));
        break;
    case MOPAC_ERROR_LIB:
        m_pLabelMopacCheck->SetLabel(MOPAC_ERROR_STRING_LIB);
        break;
    case MOPAC_ERROR_PASSWORD:
        m_pLabelMopacCheck->SetLabel(MOPAC_ERROR_STRING_PASSWORD);
        button->Enable();
        break;
    case MOPAC_ERROR_PATH:
        m_pLabelMopacCheck->SetLabel(MOPAC_ERROR_STRING_PATH);
        break;
    default:
        m_pLabelMopacCheck->SetLabel(MOPAC_ERROR_STRING_UNKNOWN);
        break;
    }
}

void    EnvSettings::OnCheckMolcas(wxCommandEvent &event)
{
    wxString check = EnvUtil::GetMolcasDir(m_pTcMolcasPath->GetValue());
   
    if(check.IsEmpty())
        m_pLabelMolcasCheck->SetLabel(MOLCAS_ERROR_STRING_UNAVAILABLE);
    else
        m_pLabelMolcasCheck->SetLabel(MOLCAS_ERROR_STRING_OK);
        m_pLabelMolcasCheck->SetForegroundColour(wxColor(0,200,0));
}

void EnvSettings::Save()
{
    wxString    path = m_pTcMolcasPath->GetValue();
    StringHash fileElems;
    if(!path.IsEmpty()) {
        fileElems[MOLCAS_PATH_ELEMENT_NAME] = path;
    }

    path = m_pTcDefWorkPath->GetValue();
    if(!path.IsEmpty()) {
        fileElems[DEFWORK_PATH_ELEMENT_NAME] = path;
    }

    path = m_pTcMopacPath->GetValue();
    if(path.IsEmpty())
        path = wxT("NULL");
    fileElems[MOPAC_PATH_ELEMENT_NAME] = path;
    fileElems[MOPAC_LIB_PATH_ELEMENT_NAME] = m_pTcMopacLibPath->GetValue();
#ifndef __SIMU_Q__
    path = m_pTcMoldenPath->GetValue();
    if(path.IsEmpty())
        path = wxT("NULL");
    fileElems[MOLDEN_PATH_ELEMENT_NAME] = path;

    // path = m_pTcMolGVPath->GetValue();
    // path = m_pTcPegaPath->GetValue();
    // if(path.IsEmpty())
    //     path = wxT("NULL");
    // fileElems[MOLGV_PATH_ELEMENT_NAME] = path;

    path = m_pTcPegaPath->GetValue();
    if(path.IsEmpty())
        path = wxT("NULL");
    fileElems[PEGAMOID_PATH_ELEMENT_NAME] = path;

    path = m_pTcOthersoftPath->GetValue();
    if(path.IsEmpty())
        path = wxT("NULL");
    fileElems[OTHERSOFT_PATH_ELEMENT_NAME] = path;
#endif
    StringHash fileTypes;
    wxArrayString lines = StringUtil::Split(m_pTcFileTypes->GetValue(),wxT("\r\n"));
    for(unsigned i=0; i<lines.GetCount(); i++)
    {
        wxArrayString typeCmd = StringUtil::Split(lines[i], wxT("="));
        fileTypes[typeCmd[0]] = typeCmd[1];
    }

    EnvSettingsData::Get()->Save(fileElems, fileTypes);

}


void SimpleProcess::OnTerminate(int pid, int status)
{
    // Can't delete the process now in case something remains to be read
}

#include "wx/colordlg.h"
#include "uiprops.h"
#include "molvieweventhandler.h"
int CTRL_COLR_CHOOSE = wxNewId();
int CTRL_COLR_OK = wxNewId();
int CTRL_COLR_CANCEL = wxNewId();
int CTRL_COLR_DEFAULT = wxNewId();
BEGIN_EVENT_TABLE(ColrSettings, wxDialog)
    EVT_BUTTON(CTRL_COLR_CHOOSE, ColrSettings::OnChoose)
    EVT_BUTTON(CTRL_COLR_OK, ColrSettings::OnOk)
    EVT_BUTTON(CTRL_COLR_CANCEL, ColrSettings::OnCancel)
    EVT_BUTTON(CTRL_COLR_DEFAULT, ColrSettings::OnDefault)
END_EVENT_TABLE()

ColrSettings::ColrSettings(wxWindow *parent) : colour(GetUIProps()->openGLProps.mainBgColor.GetColor()) {

    wxDialog::Create(parent, -1, wxT("Color Setting"), wxDefaultPosition, wxSize(270, 150));

    bgBtn = new wxButton(this, CTRL_COLR_CHOOSE, wxT("Background:"));
    bgShow = new wxWindow(this, wxID_ANY);
    bgShow->SetBackgroundColour(colour);

    wxBoxSizer *sz = new wxBoxSizer(wxHORIZONTAL);

    sz->Add(bgBtn, 1, wxEXPAND);
    sz->Add(bgShow, 1, wxEXPAND);

    wxBoxSizer *sz1 = new wxBoxSizer(wxHORIZONTAL);

    sz1->Add(new wxButton(this, CTRL_COLR_DEFAULT, wxT("Default")));
    sz1->Add(new wxButton(this, CTRL_COLR_OK, wxT("Ok")));
    sz1->Add(new wxButton(this, CTRL_COLR_CANCEL, wxT("Cancel")));

    wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

    sizer->Add(sz);
    sizer->AddStretchSpacer(1);
    sizer->Add(sz1);

    wxBoxSizer *sizer1 = new wxBoxSizer(wxVERTICAL);
    sizer1->Add(sizer, 1, wxALL, 10);

    SetSizer(sizer1);


}
ColrSettings::~ColrSettings() {

}
// void Onchoose(wxCommandEvent &event){
//     OnChoose(event)
// }
void ColrSettings::OnChoose (wxCommandEvent &event) {
    colour = wxGetColourFromUser(this, wxColour(colour), wxT("Choose Color"));
    if (colour.IsOk()) {
        bgShow->SetBackgroundColour(colour);
        bgShow->ClearBackground();
    }
}

void ColrSettings::OnOk (wxCommandEvent &event) {

    if(IsModal())
        EndModal(wxID_OK);
}
void ColrSettings::OnCancel (wxCommandEvent &event) {

    if(IsModal())
        EndModal(wxID_CANCEL);
}

void ColrSettings::OnDefault(wxCommandEvent &event) {

    colour.Set(0, 153, 255);

    bgShow->SetBackgroundColour(colour);
    bgShow->ClearBackground();
    //        savecolour(colour);

    //        GetUIProps()->openGLProps.mainBgColor.SetColor(colour);
    //        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();

}
