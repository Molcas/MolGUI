/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsshinfo.cpp: implementation of the DlgSSHInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "dlglogin.h"
#include "stringutil.h"
#include "envutil.h"

DlgLogin::DlgLogin(wxWindow* parent, wxWindowID id, const wxString& title,
            const wxPoint& pos , const wxSize& size,long style)
{
        wxDialog::Create(parent, id, title, pos, wxSize(400,260), style);
        m_psshclient=NULL;
        CreateGUIControls();
}
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgLogin, wxDialog)
        EVT_BUTTON(wxID_OK, DlgLogin::DoOk)
        EVT_BUTTON(wxID_CANCEL,DlgLogin::DoCancel)
    EVT_CLOSE(DlgLogin::OnClose)
END_EVENT_TABLE()

DlgLogin::~DlgLogin()
{
m_psshclient=NULL;
}

void DlgLogin::DoCancel(wxCommandEvent&event)
{
  //  if(m_psshclient!=NULL)m_psshclient->SetRetry(false);
    this->Close();
}

void DlgLogin::CreateGUIControls()
{
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        SetSizer(bsTop);
        wxStaticText* lbtitle = new wxStaticText( this, wxID_STATIC,
                wxT("Please input login information: ") ,
                wxPoint(20,5), wxSize(200,20), 0 );

        wxStaticText* lbmasterkey = new wxStaticText( this, wxID_STATIC,
                wxT("MasterKey:") ,
                wxPoint(20,5), wxSize(200,20), 0 );
        wxStaticText* lbusername = new wxStaticText( this, wxID_STATIC,
                wxT("UserName:") ,
                wxPoint(20,25), wxSize(200,20), 0 );
        wxStaticText* lbpassword = new wxStaticText( this, wxID_STATIC,
                wxT("PassWord:") ,
                wxPoint(20,45), wxSize(200,20), 0 );
        m_pTxtMasterkey =new wxTextCtrl(this,wxID_ANY,wxEmptyString,
                        wxPoint(220,15), wxSize(200, 20),wxTE_PASSWORD);
        m_pTxtUsername= new wxTextCtrl(this,wxID_ANY,wxEmptyString,
                        wxPoint(220,35), wxSize(200, 20));
        m_pTxtPassword= new wxTextCtrl(this,wxID_ANY,wxEmptyString,
                        wxPoint(220,55), wxSize(200, 20),wxTE_PASSWORD);
        bsTop->Add(lbtitle,0,wxALL|wxEXPAND,5);
        bsTop->Add(lbmasterkey,0,wxALL|wxEXPAND,5);
        bsTop->Add(m_pTxtMasterkey,0,wxALL|wxEXPAND,5);
        bsTop->Add(lbusername,0,wxALL|wxEXPAND,5);
        bsTop->Add(m_pTxtUsername,0,wxALL|wxEXPAND,5);
        bsTop->Add(lbpassword,0,wxALL|wxEXPAND,5);
        bsTop->Add(m_pTxtPassword,0,wxALL|wxEXPAND,5);
        wxButton* btnok = new wxButton( this, wxID_OK, wxT("&OK"),wxDefaultPosition, wxDefaultSize);
        wxButton* btncancel=new wxButton(this,wxID_CANCEL,wxT("&Cancel"),wxDefaultPosition,wxDefaultSize);
        wxBoxSizer *bsBtn=new wxBoxSizer(wxHORIZONTAL);

        bsBtn->Add(btnok,0,wxALL|wxALIGN_CENTER,5);
        bsBtn->Add(btncancel,0,wxALL|wxALIGN_CENTER,5);
        bsTop->Add(bsBtn,0,wxALL|wxALIGN_CENTER,5);
        bsTop->SetSizeHints(this);
        m_pTxtMasterkey->SetFocus();
}

void DlgLogin::DoOk(wxCommandEvent& event)
{
    if((m_pTxtMasterkey->GetValue()).IsNull()||(m_pTxtMasterkey->GetValue().Trim()).IsEmpty()){
        wxMessageBox(wxT("Please input the masterkey!"));
        m_pTxtMasterkey->SetFocus();
        return;
    }
    if((m_pTxtUsername->GetValue()).IsNull()||(m_pTxtUsername->GetValue().Trim()).IsEmpty()){
        wxMessageBox(wxT("Please input the username!"));
        m_pTxtUsername->SetFocus();
        return;
    }
    if((m_pTxtPassword->GetValue()).IsNull()||(m_pTxtPassword->GetValue().Trim()).IsEmpty()){
        wxMessageBox(wxT("Please input the password!"));
        m_pTxtPassword->SetFocus();
        return;
    }

//    char * input=NULL;
   // EnvUtil::SetMasterKey(m_pTxtMasterkey->GetValue());
        if(m_psshclient!=NULL){
            m_psshclient->SetMasterkey(m_pTxtMasterkey->GetValue());
  //      StringUtil::String2CharPointer(&input,m_pTxtUsername->GetValue());
        m_psshclient->SetUsername(m_pTxtUsername->GetValue());
  //      StringUtil::String2CharPointer(&input,EnvUtil::GetDecipherCode( m_pTxtPassword->GetValue()));
        m_psshclient->SetPassword(EnvUtil::GetEncryptCode( m_pTxtPassword->GetValue()));
        m_psshclient->SetRetry(true);
        }
    if(IsModal())EndModal(wxID_OK);
}

void DlgLogin::OnClose(wxCloseEvent &event)
{
//
    if(m_psshclient!=NULL)m_psshclient->SetRetry(false);
        if(IsModal())
        {
                EndModal(wxID_CANCEL);
        }
        else
        {
                SetReturnCode(wxID_CANCEL);
                this->Show(false);
        }
}


void DlgLogin::SetInfo(SSHClient *psshclient)
{
    m_psshclient=psshclient;
}
