/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgtext.cpp: implementation of the DlgText class.

#include "dlgtext.h"

DlgText::DlgText(wxWindow* parent, const wxString& message, const wxString& title, const wxPoint& pos, const wxSize& size, long style, long buttonsStyle, long textCtrlStyle) : wxDialog(parent, wxID_ANY, title, pos, size, style)
{
    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
    wxTextCtrl* tCtrl = new wxTextCtrl(this, wxID_ANY, message, wxDefaultPosition, wxDefaultSize, textCtrlStyle);
    bsTop->Add(tCtrl, 1, wxALIGN_TOP|wxEXPAND|wxALL, 5);

    bsTop->Add(CreateButtonSizer(buttonsStyle), wxSizerFlags().Right().Border(wxALL,5));
    SetSizer(bsTop);
}
