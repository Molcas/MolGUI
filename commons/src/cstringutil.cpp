/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cstringutil.h"

int IsEmptyString(const char* str, int len) {
        int i = 0;
        for(i = 0; i < len; i++) {
                if(!(str[i] == ' ' || str[i] == '\n' || str[i] == '\r' || str[i] == '\t'))
                        return 0;
        }
        return 1;
}

char* StringTrim(char *str, const char *trim) {
        return StringRightTrim(StringLeftTrim(str, trim), trim);
}

char* StringRightTrim(char* str, const char* trim) {
        char* end;
        if(!str)
                return NULL;
        if(!trim)
                trim = " \t\n\r";

        end = str + strlen(str);
        while(end-- > str) {
                if(!strchr(trim, *end))
                        return str;
                *end = 0;
        }
        return str;
}

char* StringLeftTrim(char* str, const char* trim) {
        if(!str)
                return NULL;
        if(!trim)
                trim = " \t\n\r";

        while(*str) {
                if(!strchr(trim, *str))
                        return str;
                ++str;
        }
        return str;
}

void StringCopy(char** dest, const char* src, int srclen) {
        //apply for one more space and add '\0'
        *dest = (char*)malloc(srclen + 1);
        if( NULL == *dest) {
                printf("cannot allocate memory\n");
                exit(1);
        }
        (*dest)[0] = 0;                                        // it should be set '\0' as to use strncat
        strncat(*dest, src, srclen);        //cannot use strncpy as it may be wrong
}

/*void StringConcatenate(char** dest, const char* delim, const char* src) {
        int length = 0;
        int beforeSize = 0;
        int afterSize = 0;
        if(NULL == dest) {
                length = strlen(src) + 1;
                beforeSize = 0;
        }else {
                length = strlen(*dest) + strlen(delim) + strlen(src) + 1;
                beforeSize = (int)((srtlen(src) + 1) / 100.0 + 0.5) * 100;
        }
        afterSize = (int)(length / 100.0 + 0.5) * 100;
        if(afterSize > beforeSize) {
                *dest = realloc(dest, afterSize * sizeof(char));
                if( NULL == dest) {
                        printf("cannot allocate memory\n");
                        exit(1);
                }
        }
}*/

/*
char** StringTokenizer(const char* str, const char* delim, int* count) {
        int i = 0;
        char* pch = NULL;
        char** tokens = NULL;
        char* strBuf = NULL;

        *count = 0;
        if(str == NULL || delim == NULL)
                return NULL;
        StringCopy(&strBuf, str, strlen(str));

        pch = strtok(strBuf, delim);
        while(pch != NULL) {
                tokens = (char**)RequestMemory(tokens, i, 5, sizeof(char*));
#ifdef __DEBUG__
                printf("%s\n", pch);
#endif
                pch = StringTrim(pch, NULL);
                StringCopy(&tokens[i], pch, strlen(pch));
                i++;
                pch = strtok(NULL, delim);
        }

        *count = i;
        return tokens;
}
*/

char* StringConcatenator(const char** strs, int count, const char* delim) {
    int currTotalSize = 0;
    int currOccupedSize = 0;
    char* value = NULL;
    int i = 0;
    int nextLen = 0;

    for(i = 0; i < count; i++) {
        if(strs[i] == NULL) continue;
        nextLen = strlen(strs[i]) + 1;
        if(i < count - 1) {
            nextLen+= strlen(delim);
        }
        if(currTotalSize < currOccupedSize + nextLen) {
            value = (char*)realloc(value, (currTotalSize + 100) * sizeof(char));
            if( NULL == value) {
                printf("cannot allocate memory\n");
                return NULL;
                    }
                    currTotalSize += 100;
        }
        strcpy(value, strs[i]);
        if(i < count - 1) {
            strcpy(value, delim);
        }
        currOccupedSize = strlen(value);
    }
    return value;
}

char** StringTokenizer(const char* str, const char* delim, int* count, int needEmptySting) {
        int i = 0, j = 0, tokenCount = 0;
        int hasFound = 0;
        int lastIndex = 0;
        char** tokens = NULL;
        char* tempString = NULL;
        char* trimString = NULL;

        *count = 0;
        if(str == NULL || delim == NULL)
                return NULL;

        for(i = 0; i < (int)strlen(str); i++) {
                hasFound = 1;
                j = 0;
                while(i < (int)strlen(str) && j < (int)strlen(delim)) {
                        if(str[i++] != delim[j++]) {
                                hasFound = 0;
                                i--;
                                break;
                        }
                }
                if(hasFound) {
                        StringCopy(&tempString, &str[lastIndex], i - strlen(delim) - lastIndex);
                        trimString = StringTrim(tempString, NULL);

            if(!needEmptySting && 0 == strlen(trimString)) {
            }else{
                tokens = (char**)RequestMemory(tokens, tokenCount, 3, sizeof(char*));
                StringCopy(&tokens[tokenCount], trimString, strlen(trimString));
                tokenCount++;
            }
                        free(tempString);
                        lastIndex = i;
                        i--;
                }
        }

    //copy the last token
        StringCopy(&tempString, &str[lastIndex], strlen(str) - lastIndex);
        trimString = StringTrim(tempString, NULL);
    if(!needEmptySting && 0 == strlen(trimString)) {
    }else{
            tokens = (char**)RequestMemory(tokens, tokenCount, 3, sizeof(char*));
            StringCopy(&tokens[tokenCount], trimString, strlen(trimString));
            tokenCount++;
    }
    free(tempString);

        *count = tokenCount;
        return tokens;
}

void* RequestMemory(void* pmem, int currOccupiedSize, int incrSize, int typeSize) {
        if(currOccupiedSize % incrSize == 0) {
                pmem = realloc(pmem, (currOccupiedSize + incrSize) * typeSize);
                if( NULL == pmem) {
                        printf("cannot allocate memory\n");
                        exit(1);
                }
        }
        return pmem;
}

void FreeStringArray(char** array, int length) {
        int i = 0;
        for(i = 0; i < length; i++) {
        if(array[i])
                  free(array[i]);
        }
        free(array);
}

int StringCharCount(const char* str, char pattern) {
    int i = 0;
    int count = 0;
    if(str != NULL) {
        for(i = 0; i < (int)strlen(str); i++) {
            if(str[i] == pattern ) {
                // skip
                if(i > 0 && str[i - 1] == '%') {
                    continue;
                }
                count++;
            }
        }
    }
    return count;
}

int SubStringIndex(char* str, const char* subString, int fromIndex) {
    int index = -1;
    int i = 0, j = 0;
    int hasFound = 0;

        for(i = fromIndex; i < (int)strlen(str); i++) {
                hasFound = 1;
                j = 0;
                index = i;
                while(i < (int)strlen(str) && j < (int)strlen(subString)) {
                        if(str[i++] != subString[j++]) {
                                hasFound = 0;
                                i--;
                                break;
                        }
                }
                if(hasFound) {
            return index;
        }
    }
    return -1;
}

int IsCharEqualNoCase(const char char1, const char char2) {
    char capChar1, capChar2;
    capChar1 = char1;
    capChar2 = char2;

    if(capChar1 >= 97 && capChar1 <= 123)
        capChar1 -= 32;
    if(capChar2 >= 97 && capChar2 <= 123)
        capChar2 -= 32;

    if(capChar1 == capChar2) {
        return 1;
    }
    return 0;
}

int IsStringEqualNoCase(const char* str1, const char* str2, int len) {
    int k;
    //char capChar1, capChar2;
           if(str1 == NULL || str2 == NULL)
                return 0;
    if(strlen(str1) < (unsigned)len)
        len = strlen(str1);
    if(strlen(str2) < (unsigned)len)
        len = strlen(str2);

    for(k = 0; k < len; k++) {
        if(!IsCharEqualNoCase(str1[k], str2[k])) {
            return 0;
        }
        /*capChar1 = str1[k];
        if(capChar1 >= 97 && capChar1 <= 123)
            capChar1 -= 32;
        capChar2 = str2[k];
        if(capChar2 >= 97 && capChar2 <= 123)
            capChar2 -= 32;
        if(capChar1 != capChar2) {
            return 0;
        }*/
    }
    return 1;
}
