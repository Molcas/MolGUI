/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "appglobals.h"

namespace appglobals {
        const wxString appVendor                                = wxT("MOLCAS");
        const wxString appName                                        = wxT("MolGUI"); //SONG change SimuPac for SimuPrj for workshop2009

        const wxString appVersion                                = wxT("0.1.0");

        const wxString appUrl                                        = wxT("http://www.glsimu.com/");//xia change http://www.bnu.edu.cn/
        const wxString appContactEmail                        = wxT("jianguo_yu@bnu.edu.cn");//xia change test@test.com

        #if defined (__WINDOWS__)
                const wxString appPlatform                         = wxT("Windows");
        #elif defined (__MACOSX__)
                const wxString appPlatform                         = wxT("Mac OS X");
        #elif defined (__LINUX__ )
                const wxString appPlatform                         = wxT("Linux");
        #else
                const wxString appPlatform                         = wxT("Unknown");
        #endif

        const wxString appBuildTimestamp                 = (wxString(wxT(__DATE__)) + wxT(", ") + wxT(__TIME__) + wxT(" (") + appPlatform + wxT(")"));
};
