/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// dlgsshinfo.cpp: implementation of the DlgSSHInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "dlgsshinfo.h"
#include "envutil.h"
#include "xmlfile.h"
#include "stringutil.h"

DlgSSHInfo::DlgSSHInfo(wxWindow* parent, wxWindowID id, const wxString& title,
            const wxPoint& pos , const wxSize& size,long style)
{
        wxDialog::Create(parent, id, title, pos, wxSize(400,260), style);
        CreateGUIControls();
}
////////////////////////////////////////////////////////////////////
//event tabel
////////////////////////////////////////////////////////////////////
BEGIN_EVENT_TABLE(DlgSSHInfo, wxDialog)
        EVT_BUTTON(wxID_OK, DlgSSHInfo::DoOk)
    EVT_CLOSE(DlgSSHInfo::OnClose)
END_EVENT_TABLE()

DlgSSHInfo::~DlgSSHInfo()
{

}

void DlgSSHInfo::CreateGUIControls()
{
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
        SetSizer(bsTop);
        wxStaticText* lbhostname = new wxStaticText( this, wxID_STATIC,
                wxT("HostName:") ,
                wxPoint(20,5), wxSize(200,20), 0 );
        wxStaticText* lbusername = new wxStaticText( this, wxID_STATIC,
                wxT("UserName:") ,
                wxPoint(20,25), wxSize(200,20), 0 );
        wxStaticText* lbpassword = new wxStaticText( this, wxID_STATIC,
                wxT("PassWord:") ,
                wxPoint(20,45), wxSize(200,20), 0 );

        wxStaticText* lbcommand = new wxStaticText( this, wxID_STATIC,
                wxT("Command:") ,
                wxPoint(20,45), wxSize(200,20), 0 );

        wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("ssh.conf");
        XmlFile sshcfg(wxT("ssh-information"),fileName);
        XmlElement * root=sshcfg.GetRootElement();
        if(root->subElemCount>0){
        char *hostname;
        hostname=root->subElemList[0].elem.name;
        wxArrayString sshinfoary=sshcfg.GetArrayString(hostname);
        m_pTxtHostname= new wxTextCtrl(this,wxID_ANY,sshinfoary[0],
                        wxPoint(220,5), wxSize(200, 20));
        m_pTxtUsername= new wxTextCtrl(this,wxID_ANY,sshinfoary[1],
                        wxPoint(220,25), wxSize(200, 20));
        m_pTxtPassword= new wxTextCtrl(this,wxID_ANY,sshinfoary[2],
                        wxPoint(220,45), wxSize(200, 20));
        m_pTxtCommand= new wxTextCtrl(this,wxID_ANY,sshinfoary[3],
                        wxPoint(220,65), wxSize(200, 20));

        }else{
        m_pTxtHostname= new wxTextCtrl(this,wxID_ANY,wxEmptyString,
                        wxPoint(220,5), wxSize(200, 20));
        m_pTxtUsername= new wxTextCtrl(this,wxID_ANY,wxEmptyString,
                        wxPoint(220,25), wxSize(200, 20));
        m_pTxtPassword= new wxTextCtrl(this,wxID_ANY,wxEmptyString,
                        wxPoint(220,45), wxSize(200, 20));
        m_pTxtCommand= new wxTextCtrl(this,wxID_ANY,wxEmptyString,
                        wxPoint(220,65), wxSize(200, 20));

        }
        bsTop->Add(lbhostname,0,wxALL|wxEXPAND,5);
        bsTop->Add(m_pTxtHostname,0,wxALL|wxEXPAND,5);
        bsTop->Add(lbusername,0,wxALL|wxEXPAND,5);
        bsTop->Add(m_pTxtUsername,0,wxALL|wxEXPAND,5);
        bsTop->Add(lbpassword,0,wxALL|wxEXPAND,5);
        bsTop->Add(m_pTxtPassword,0,wxALL|wxEXPAND,5);
        bsTop->Add(lbcommand,0,wxALL|wxEXPAND,5);
        bsTop->Add(m_pTxtCommand,0,wxALL|wxEXPAND,5);
        wxButton* ok = new wxButton( this, wxID_OK, wxT("&OK"),wxPoint(120,550), wxDefaultSize, 0 );
        bsTop->Add(ok,0,wxALL|wxALIGN_RIGHT,5);
}

void DlgSSHInfo::DoOk(wxCommandEvent& event)
{
SaveInfo();
this->Close();
}

void DlgSSHInfo::OnClose(wxCloseEvent &event)
{
        if(IsModal())
        {
                EndModal(wxID_OK);
        }
        else
        {
                SetReturnCode(wxID_OK);
                this->Show(false);
        }
}


void DlgSSHInfo::SaveInfo()
{
        wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("ssh.conf");
        XmlFile sshcfg(wxT("ssh-information"),fileName);
        XmlElement * root=sshcfg.GetRootElement();
        int hostnumber=root->subElemCount;
        char *hostname;
        if(hostnumber<0)hostnumber=0;
        wxString strhostname=wxString::Format(wxT("host%d"),hostnumber);
        wxArrayString sshinfoary;
        sshinfoary.Add(m_pTxtHostname->GetValue());
        sshinfoary.Add(m_pTxtUsername->GetValue());
        sshinfoary.Add(m_pTxtPassword->GetValue());
        sshinfoary.Add(m_pTxtCommand->GetValue());
//        sshcfg.BuildElement("HostName",m_pTxtHostname->GetValue());
//        sshcfg.BuildElement("UserName",m_pTxtUsername->GetValue());
//        sshcfg.BuildElement("PassWord",m_pTxtPassword->GetValue());
        StringUtil::String2CharPointer(&hostname,strhostname);
        sshcfg.BuildElement(hostname,sshinfoary);
        sshcfg.Save();
}
