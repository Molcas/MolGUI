/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DLGTEXT_H__
#define __BP_DLGTEXT_H__

#include "wx.h"

#define DEFAULT_DLGTEXT_BUTTONS_STYLE  wxOK

/**
 * A dialog with displaying a text in a wxTextCtrl
 * More customizable than wxTextEntryControl
 */
class DlgText : public wxDialog
{
public:
    DlgText(wxWindow* parent, const wxString& message = wxEmptyString, const wxString& title = wxT("Text"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, long buttonsStyle = DEFAULT_DLGTEXT_BUTTONS_STYLE, long textCtrlStyle=0);
};

#endif
