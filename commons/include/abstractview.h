/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_ABSTRACT_VIEW_H__
#define __BP_ABSTRACT_VIEW_H__

#include "wx.h"

//extern int MENU_ID_STATUS_SHOW_INFO;

typedef enum _viewType {
        VIEW_UNKNOWN,
        VIEW_START_HERE,
        VIEW_PROJECTINFO,
        VIEW_GL_BUILD,
        VIEW_GL_DISPLAY,
        VIEW_GL_DISPLAY_SURFACE,
        VIEW_GL_DISPLAY_VIBRATION,
        VIEW_GL_PREVIEW
}ViewType;
enum{
    STARTHERE_PID=-1,
    PRJINFO_PID=-2
};
/**
 * Base class that all "views" should inherit from.
 */
class AbstractView : public wxPanel {
public:
        AbstractView(wxWindow* parent, wxEvtHandler* evtHandler, ViewType viewType, long style = wxTAB_TRAVERSAL);
        virtual ~AbstractView();
public:
                ViewType        GetType(void) {  return m_viewType; }
                virtual void    InitView(void) {}

                int             GetProjectId(void) const { return m_viewId; }
                void            SetProjectId(int projectId) { m_viewId = projectId; }
                wxString                GetProjectTitle(void){return m_prjTitle;};

                // @return The view's filename (if applicable).
         virtual const wxString& GetFileName(void) const { return m_fileName; }//.sim
        // Sets the view's filename.
        virtual void    SetFileName(const wxString& fileName);

        //@return The view's short name.
                const   wxString& GetTitle() const { return m_title; }
                // Set the view's title.
        void    SetTitle(const wxString& newTitle) { m_title = newTitle; }
        /**
         * This is the name displayed on the view's tab.
         * @return The view's display name.
         */
       wxString     GetViewTitle(wxString prefix);// const;
        /**
         * Close this view.
         * The default implementation closes (destroys) the view and returns true.
         * @return True if view closed succesfully
         */
        virtual bool Close(bool isAskForSave=true,bool default_save=false);
        /**
         * Save the editor's contents. The default implementation does nothing and returns true.
         * @return True on success, false otherwise.
                 */
        virtual bool Save() { return true; }
        virtual bool SaveAs(wxString newPath){return true;}
        /**
                 * Undo changes.
                 */
        virtual void Undo(void) {}
        /**
                 * Redo changes.
                 */
        virtual void Redo(void) {}

        void       SetMenuBar(wxMenuBar* pMenuBar);
        wxMenuBar* GetMenuBar(void) const;
        void       EnableMenu(int menuId, bool enabled);
                virtual void  EnableMenuTool(bool isEnable) {}
                /**
         * Update any menu items and tools the view needs on the mainframe when activated.
         */
        virtual void    UpdateMenuTool(void) {}
        virtual void  InitSurfaceOrVib(void){}
        //void ShowStatus(wxString msg, int pos = 0);

        private:
                DECLARE_EVENT_TABLE();

        protected:
                int             m_viewId;//same to the related project's projectId
                wxString        m_title;
        wxString        m_fileName;
        ViewType        m_viewType;
                wxMenuBar*      m_pMenuBar;
        wxEvtHandler*   m_pEvtHandler;
                wxString                m_prjTitle;
};

#endif
