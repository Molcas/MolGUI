/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

//#ifdef __LIBSSH2__
#ifndef SSHCLIENT_H
#define SSHCLIENT_H
//#include "libssh2_config.h"
//#ifdef __LIBSSH2__
#include <libssh2.h>
//#endif

#ifdef _WIN32
# include <winsock2.h>

#endif


#ifdef __LINUX__
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
#include <sys/types.h>
# include <sys/time.h>
# include <unistd.h>

#endif
#ifdef __MAC__
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <sys/types.h>
# include <sys/time.h>
# include <unistd.h>

#endif
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <wx.h>
class SSHClient
{
    public:
        SSHClient();
        virtual ~SSHClient();
        const char * GetHostname() { return m_hostname; }
        void SetHostname(wxString hostname);
        const char * GetUsername() { return m_username; }
        void SetUsername(wxString username);
        const char * GetPassword() { return m_password; }
        void SetPassword(wxString pwd);
        static int waitsocket(int socket_fd, LIBSSH2_SESSION *session);
        static long tvdiff(struct timeval newer, struct timeval older);
        static void kbd_callback(const char *name, int name_len,
                         const char *instruction, int instruction_len,
                         int num_prompts,
                         const LIBSSH2_USERAUTH_KBDINT_PROMPT *prompts,
                         LIBSSH2_USERAUTH_KBDINT_RESPONSE *responses,
                         void **abstract);
        int ConnectToHost(void);
        int Upload(const char* inputfile,const char* remotefile);
        int Download(const char* localfile,const char* remotefile);
        wxArrayString GetOutputInfo(const char* keyword);
        int RemoteExec(const char* command,int len,const char* cpath);
        void Shutdown(void);
        void SetRetry(bool retry);
        bool GetRetry(void);
        void SetMasterkey(wxString masterkey);
        wxString GetMasterkey(void);

    protected:
    private:
        char * m_hostname;
        const char * m_username;
        const char * m_password;
        wxString m_masterkey;
        LIBSSH2_SESSION *session;
        LIBSSH2_CHANNEL *channel;

        int sock;
        bool m_retry;
#ifdef WIN32
    WSADATA wsadata;
#endif


};

#endif // SSHCLIENT_H
//#endif // LIBSSH2_FOUND
