/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_XML_XMLPARSER_H__
#define __BP_XML_XMLPARSER_H__

#include <stdio.h>

struct _xmlElement;
struct _xmlElementList;

typedef struct _xmlAttribute {
        char* name;                                        // Attribute Name
        char* value;                                // Attribute Value
        struct _xmlElement* elem;        // pointer to its element
}XmlAttribute;

typedef struct _xmlAttributeList {
        XmlAttribute attr;                                        //
        struct _xmlAttributeList* next;         // pointer to the next XmlAttribute
}XmlAttributeList;

typedef struct _xmlElement {
        char* name;
        int attrCount;
        XmlAttributeList* attrList;

        char* content;
        int subElemCount;
        struct _xmlElementList* subElemList;
        struct _xmlElement* parent;                        // pointer to its parent element
}XmlElement;

typedef struct _xmlElementList {
        XmlElement elem;
        struct _xmlElementList* next;
}XmlElementList;

XmlElementList* CreateDocument(char* rootName);
/**
 * Parse an XML file into a list XmlElement
 * @param fileName: xml file path
 * @param keepFormat: If true, dont change the format of the content of an element.
 */
XmlElementList* ParseXmlDocument(const char* fileName, bool keepFormat);

/**
 * Free memory.
 */
void FreeXmlElementList(XmlElementList* elemList);
void FreeXmlElement(XmlElement* elem);
void FreeXmlAttribute(XmlAttribute* attr);

XmlElementList* CreateElementList(void);
XmlAttributeList* CreateAttributeList(void);
XmlAttribute* AddAttribute(XmlElement* elem, const char* attrName, const char* attrValue);
XmlElement* AddElement(XmlElement* parentElem);
//void CopyString(char** dest, const char* src);

void DumpXmlElementList(FILE* fp, XmlElementList* elemList, int depth);
void DumpXmlElement(FILE* fp, XmlElement* elem, int depth);
void DumpXmlAttribute(FILE* fp, XmlAttribute* attr);

/**
 * Get sub elements with given tagName.
 * @param elem: the element contains subelements;
 * @param tagName: the tagname of subelement;
 * @param count: (out) the number of subelements;
 * @return arry of pointers to satisfied subelements;
 */
XmlElement** GetSubElements(XmlElement* elem, const char* tagName, int* count);
XmlElement* GetSubElement(XmlElement* elem, const char* tagName);

/**
 * Get the value of the given attribute in the given element.
 */
char* GetAttributeString(XmlElement* elem, const char* attrName);
int GetAttributeInt(int* value, XmlElement* elem, const char* attrName);
int GetAttributeDouble(double* value, XmlElement* elem, const char* attrName);
char** GetAttributeStringArray(XmlElement* elem, const char* attrName, int* count);
int* GetAttributeIntArray(XmlElement* elem, const char* attrName, int* count);
double* GetAttributeDoubleArray(XmlElement* elem, const char* attrName, int* count);

#endif
