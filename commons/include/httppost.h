/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

/*
 * HttpPost.h
 *
 *  Created on: 2010-8-27
 *      Author: allan
 */

#ifndef HTTPPOST_H
#define HTTPPOST_H

//enum State { WAITING, RUNNING, DONE };

#include <wx/wx.h>
#include <wx/thread.h>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
//#include "jobmonitorpnl.h"

using namespace std;

class JobMonitorPnl;
class HttpPost;

class DownLoadThread : public wxThread {
public:
        DownLoadThread(HttpPost * post);
        virtual void * Entry();
private :
        HttpPost * post;
};

class HttpPost {

public :

        HttpPost(wxString server=wxEmptyString, wxString id=wxEmptyString, wxString ticket = wxEmptyString);

        void AddFile(wxString file);
        bool Post();
        wxString FindTicket();
        void SetServer(wxString server);
        void SetId(wxString id);
        void SetTicket(wxString ticket);
        void Check();
        wxString GetStatus();
        void DownLoad(void);
        void DoDownLoad();
        bool IsDownLoadFinished();
        bool IsUploadFinished();
        int IsBusy();
        void SetBusy(int isbusy);
        vector<string> GetDownLoadFiles() {
                return filesDownLoad;
        }

        void SetPostData(char * buf) {
                postdata = buf;
        }
        void SetCheckData(char * buf) {
                checkdata = buf;
        }
        void SetJobmonitorpnl(JobMonitorPnl * pJobMonitorPnl);
    int GetRemoteFile(string file, string url);
    void GetDownList(wxString path);
    void SetAryDownlist(wxArrayString lfiles,wxArrayString rfiles);
private :
        string ticket;
        string server;
        string id;
        string downpath;
        vector<string> filesUpload;
        vector<string> filesDownLoad;
    int m_busy;
        char * postdata;
        char * checkdata;

        DownLoadThread * thread;
        JobMonitorPnl * m_pJobMonitorPnl;
    wxArrayString m_arylfiles,m_aryrfiles;
};




#endif /* HTTPPOST_H */
