/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_XML_CSTRING_UTIL_H__
#define __BP_XML_CSTRING_UTIL_H__

/**
 * @param str: may not be terminated with '\0';
 * @param len: the number of valid char in str;
 * @return: 1 is an empty string
 */
int IsEmptyString(const char* str, int len);
/**
 * @param trim: the chars to be trimmed;
 *                                 If trim=NULL, it equals to trim=" \t\r\n".
 * @example : trim="\t" only tab space will be trimmed.
 */
char* StringTrim(char *str, const char *trim);

/**
 * Trim string from right.
 * @see StringTrim
 */
char* StringRightTrim(char* str, const char* trim);

/**
 * Trim string from left.
 * @see StringTrim
 */
char* StringLeftTrim(char* str, const char* trim);

/**
 * Allocate memory for dest and then copy src string to dest.
 * @param dest: a pointer to char*;
 * @param src: may not be terminated by '\0';
 * @param srclen: the length of source string;
 */
void StringCopy(char** dest, const char* src, int srclen);

/**
 * Concatenate string array with delim
 * @param strs: string array
 * @param count: array length
 * @param delim: delimiter
 */
char* StringConcatenator(const char** strs, int count, const char* delim);
/**
 * @param str: the source string;
 * @param delim: the delimiters;
 * @param count: (out) the number of tokens;
 * @param needEmptyString:
 *      needEmptyString = 1: will keep empty string as a token.
 * @return tokens contained in the source string, seperated by delim.
 */
char** StringTokenizer(const char* str, const char* delim, int* count, int needEmptySting);

/**
 * Free memoey allocated to string array.
 * Firstly free memory of each string, and then free memory of the array.
 * @param length: the length of the array.
 */
void FreeStringArray(char** array, int length);

/**
 * @param pmem: pointer to allocated memory;
 * @param currOccupiedSize: current size that has been used;
 * @param incrSize: if currOccupiedSize % incrSize == 0, then the memory will be enlarged with incrSize;
 * @param typeSize: the size of memory unit;
 */
void* RequestMemory(void* pmem, int currOccupiedSize, int incrSize, int typeSize);

/**
 * Get the count of the given patten in the str.
 */
int StringCharCount(const char* str, char pattern);

/**
 * Returns the index within the src string of the first occurrence of the specified substring,
 * starting at the specified fromIndex.
 */
int SubStringIndex(char* src, const char* subString, int fromIndex);

/**
 * compare two string with given len is equal or not with no case
 */
int IsCharEqualNoCase(const char char1, const char char2);
int IsStringEqualNoCase(const char* str1, const char* str2, int len);

#endif
