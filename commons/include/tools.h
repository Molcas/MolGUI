/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_TOOLS_H__
#define __BP_TOOLS_H__

#include "wx.h"

#include "datatype.h"

class Tools {
public:
        static void ShowError(const wxString& message, bool isSafe = true);
        static void ShowInfo(const wxString& message);
        static void ShowWarning(const wxString& message);
        static void ShowStatus(const wxString& message, int pos = 0);
        static void SetAppFrame(wxFrame* pFrame);
        static wxFrame* GetAppFrame(void);

        static bool ExecModule(wxString execProcName, wxString outputResultFileName, wxString errorResultFileName);

        static void EnableMenu(wxMenuBar* pMenuBar, int menuId, bool enabled);
        static void CheckMenu(wxMenuBar* pMenuBar, int id, const bool check);
        static bool IsMenuChecked(wxMenuBar* pMenuBar, int id);

        static wxString GetFilePathFromUser(wxWindow* parent, wxString title, wxString filter, long style);
        /**
         * Update the menu with the newest filename and history filename.
         * @param pMenu The updated menu
         * @param startId the first menu id
         * @param fileName The newest file, its empty means to initialize the menu with arrayFileHis
         * @param arrayFileHis The recent file history array
         * @param isClearHis True means to clear history
         * @param isRemove True means to remove the specified fileName
         */
        static void UpdateRecentFilesMenu(wxMenu* pMenu, int startId, const wxString& fileName,
                                                                        wxArrayString& arrayFileHis, bool isClearHis, bool isRemove);

        static bool IsHashSame(const StringHash& hash1, const StringHash& hash2);

};

class MathUtil {
public:
    static inline float GetMinValue(float f1, float f2) {
        return (f1 < f2) ? f1 : f2;
    }
    static inline float GetMaxValue(float f1, float f2) {
        return (f1 > f2) ? f1 : f2;
    }
};

#endif
