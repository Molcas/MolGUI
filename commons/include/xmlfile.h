/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_XML_FILE_H__
#define __BP_XML_FILE_H__

#include "wx.h"

#include "xmlparser.h"

class XmlFile {
public:
        XmlElementList * GetRoot();
        XmlFile(const wxString& rootName, const wxString& filePath);
        ~XmlFile();
        void Save(void);

        void BuildElement(const char* elemName, wxArrayString& arrayString);
        wxArrayString GetArrayString(const char* elemName);

        void BuildElement(const char* elemName, wxString& value);
        wxString GetString(const char* elemName);

        void BuildElement(const char* elemName, bool isTrue);
        bool GetBool(const char* elemName, bool defaultValue);

        XmlElement* GetRootElement(void);

private:
        XmlElementList* m_rootElementList;
        wxString m_filePath;
};

#endif
