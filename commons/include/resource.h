/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef  _RESOURCE_H_
#define _RESOURCE_H_
//this file defines the resource's name and path etc.
#define    wxTRUNK                      wxT("/res/")
#define    wxTRUNK_PRJ_IMG_PATH         wxT("/res/prj/images/")
#define    wxTRUNK_MOL_IMG_PATH         wxT("/res/mol/images/")
#define    wxTRUNK_MOL_DAT_PATH         wxT("/res/mol/dat/")
#define    wxTRUNK_MING_PATH            wxT("/res/ming/")
#define    wxTRUNK_MING_DAT_PATH        wxT("/res/ming/data/")
#define    wxTRUNK_MING_BASIS_PATH      wxT("/res/ming/basis/")
#endif
