/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_GENERIC_DLG_H__
#define __BP_GENERIC_DLG_H__

#include "wx.h"
#include "datatype.h"

typedef enum _genericButtonStyle {
        BTN_YES                        = 0x0001,
        BTN_OK                        = 0x0002,
        BTN_SAVE                = 0x0004,
        BTN_NO                        = 0x0008,
        BTN_CANCEL                = 0x0010,
        BTN_RESET                = 0x0020,
        BTN_SUBMIT                = 0x0040,
        BTN_KILL                = 0x0080,
        BTN_SHOW                = 0x0100,
        BTN_CLOSE                = 0x0200,
}GenericButtonStyle;

class GenericDlg;

class GenericPnl : public wxPanel {
public:
        GenericPnl(wxWindow* parent, wxWindowID id = wxID_ANY);
        ~GenericPnl();
        virtual long GetButtonStyle(void) { return BTN_OK | BTN_CANCEL; }
        virtual bool OnButton(long style) { return true; }

        wxArrayString& GetAdditionalBtnLabels(void);
        wxIntArray& GetAdditionalBtnIds(void);

protected:
        void EnableButton(int id, bool isEnable);

protected:
        wxArrayString m_additionalBtnLabels;
        wxIntArray m_additionalBtnIds;
protected:
        wxWindow* m_pParent;

};

class GenericDlg : public wxDialog {

public:
    GenericDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Generic Dialog"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
            long style = wxDEFAULT_DIALOG_STYLE | wxMAXIMIZE_BOX);
    ~GenericDlg();
        void SetGenericPanel(GenericPnl* panel);
    void HideDialog(int returnCode);
        void EnableButton(int id, bool isEnable);

private:
        DECLARE_EVENT_TABLE();
    void OnDialogButton(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

private:
        GenericPnl* m_genericPnl;
};

#endif
