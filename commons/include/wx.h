/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_WX_H__
#define __BP_WX_H__

#ifdef __BORLANDC__
        #pragma hdrstop
#endif

#ifndef WX_PRECOMP
        #include <wx/wx.h>
#else
        #include <wx/wxprec.h>
#endif
#if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_) || defined(WIN32))
#include <winsock2.h>
#include <WTYPES.h>
#include <windows.h>
#pragma warning( disable : 4284 )
#endif


#endif
