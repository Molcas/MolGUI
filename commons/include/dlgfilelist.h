/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DLGFILELIST_H__
#define __BP_DLGFILELIST_H__

#include "wx.h"
#include "wx/checklst.h"
#include "jobmonitorpnl.h"

class DlgFileList: public wxDialog
{
public:
        void DoOk(wxCommandEvent& event);
        void DoCancel(wxCommandEvent& event);
        void OnSelectAll(wxCommandEvent& event);
        void SetFiles(wxArrayString rfiles);
        void SetJobMonitor(JobMonitorPnl* pjobmonitor);
        void CreateGUIControls(void);
        DlgFileList(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Files List"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
        ~DlgFileList();
private:
        DECLARE_EVENT_TABLE();
        JobMonitorPnl * m_pJobMonitorPnl;
        wxArrayString m_rfiles;
        wxCheckListBox * m_chklist;
        wxArrayInt m_arydownload;
private:

        void OnClose(wxCloseEvent& event);
        bool initflag;
};

#endif
