/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_FILE_UTIL_H__
#define __BP_FILE_UTIL_H__
#include <wx/dir.h>
#include "wx.h"
#include "datatype.h"

class FileUtil {
public:
    static wxString AppendFileExt(wxString fileName, int filterIndex, const wxString aryExts[]);
    static wxString BuildFullPath(const wxString &homeDir, const wxString &dirName, const wxString &fileName = wxEmptyString);
    static wxString CheckFile(const wxString &dirName, const wxString &fileName, bool needReport);
    static wxString ChangeFileExt(wxString fileName, wxString ext);
    static bool     CopyDirectory(wxString oldPath, wxString newPath, int flag = wxDIR_DEFAULT);
    static bool     CreateDirectory(wxString dirPath);
    static wxString GetDirectoryName(wxString dirPath);
    static wxArrayString GetFileBySuffix(wxString dir, wxString suffix);
    static wxString GetMolcasFileType(wxString &fileName);
    static wxString GetSuffix(wxString &fileName);
    static wxString GetPath(wxString fileName);
    static wxArrayString GetFileDispType(wxString fileName);
    static bool     IsEmptyDirectory(wxString fileDir);
    static bool     IsRelativePath(wxString path);
    static void     ListDirectoryFiles(wxString fileDir, bool isRecursive, bool isOnlyName, wxArrayString &fileArrays);
    static wxArrayString LoadStringLineFile(const wxString &fileName);
    static wxString LoadStringLineFileStr(const wxString &fileName);
    static wxString NormalizeFileName(const wxString fileName);
    static bool     RemoveDirectory(wxString fileDir, bool removeSelf = true);
    static void     OutputStringLineFile(const wxString &fileName, wxArrayString values);
    static void     OutputStringLineFile(wxTextOutputStream &tos, wxArrayString values);
    //...........................................................................//
    static wxString     GetElemFromFile(const wxString &fileName, const wxString &elemName);
    static StringHash   ReadElemsFromFile(const wxString &fileName);
    static bool         WriteElemsToFile(const wxString &fileName, const StringHash &elems);

};
#endif
