/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DLGLOGIN_H__
#define __BP_DLGLOGIN_H__

#include "wx.h"
#include "sshclient.h"

class DlgLogin: public wxDialog
{

public:
        void SetInfo(SSHClient *psshclient);
        void DoOk(wxCommandEvent& event);
        void DoCancel(wxCommandEvent&event);
        DlgLogin(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Login Information"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
        ~DlgLogin();
private:
        DECLARE_EVENT_TABLE();
    wxTextCtrl* m_pTxtMasterkey;
        wxTextCtrl* m_pTxtUsername;
        wxTextCtrl* m_pTxtPassword;
    SSHClient * m_psshclient;
private:
        void CreateGUIControls(void);
        void OnClose(wxCloseEvent& event);
};

#endif
