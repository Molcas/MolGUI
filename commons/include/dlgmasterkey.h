/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DLGMASTERKEY_H__
#define __BP_DLGMASTERKEY_H__

#include "wx.h"

class DlgMasterKey: public wxDialog
{

public:
        void SetHostname(wxString hostname);
        DlgMasterKey(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Input MasterKey"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
        ~DlgMasterKey();
private:
        DECLARE_EVENT_TABLE();
        wxStaticText* lbmasterkey;
        wxTextCtrl* m_pTxtMasterKey;
private:
        void DoOk(wxCommandEvent& event);
        void DoCancel(wxCommandEvent&event);
        void CreateGUIControls(void);
        void OnClose(wxCloseEvent& event);

};

#endif
