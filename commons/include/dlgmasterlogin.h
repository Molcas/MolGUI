/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DLGMASTERLOGIN_H__
#define __BP_DLGMASTERLOGIN_H__

#include "wx.h"
#include "dlgmasterlist.h"

class DlgMasterLogin: public wxDialog
{

public:
        DlgMasterLogin(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Input MasterKey"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
        ~DlgMasterLogin();
private:
        DECLARE_EVENT_TABLE();
        wxStaticText* m_lbmasterid;
        wxTextCtrl* m_txtmasterid;
        wxStaticText* m_lbmasterkey;
        wxTextCtrl* m_txtmasterkey;
        wxButton * m_btnOk;
        wxButton * m_btnCancel;
private:
        void DoOk(wxCommandEvent& event);
        void DoCancel(wxCommandEvent&event);
        void CreateGUIControls(void);
        void OnClose(wxCloseEvent& event);
    MasterInfoArray masterinfoarray;
    unsigned int total;
};

#endif
