/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_STRING_UTIL_H__
#define __BP_STRING_UTIL_H__

#include <wx.h>
#include <wx/tokenzr.h>

class StringUtil {
public:
    static bool IsEmptyString(wxString &str);
    static wxString CharPointer2String(const char *str);
    static void String2CharPointer(char **charStr, wxString str);
    static void String2UnSignedCharPointer(unsigned char **, wxString str);
    static void StringCopy(char **dest, const char *src, int srclen);
    static int ToInt(wxString value);
    static wxString UpperFirstChar(const wxString &src);
    static bool IsDigital(const wxString &src);
    static wxString FormatBool(bool value);
    static wxString FormatValue(int value, int width = -1, wxChar emptyChar = wxT('0'));
    static wxString FormatValue(long value, int width = -1, wxChar emptyChar = wxT('0'));
    static wxString FormatTime(wxLongLong longTime);
    static wxString GetCurTime(int flag);
    static bool EndsWith(wxString str, wxString suffix, bool ignoreCase);
    static bool StartsWith(wxString str, wxString prefix, bool ignoreCase);
    static int SubStringIndex(char *str, const char *subString, int fromIndex);
    static  wxString CheckInput(wxString input, bool addPathSep = false); //remove illegal char in the input string;
    static  wxString CheckString(wxString input, bool addPathSep = false);
    static bool IncSufNumber(wxString &string);
    static int StringMatch(wxString strsrc, wxString strmatch);
    static bool Contains(const wxString &where, const wxString &what, bool ignoreCase=false);
    static wxArrayString Split(const wxString& str, const wxString& delims = wxT(" \t\r\n"), wxStringTokenizerMode mode = wxTOKEN_DEFAULT);

};

#endif
