/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#if !defined(WX_MESSAGE_EVENT_H)
#define WX_MESSAGE_EVENT_H

#define ID_MESSAGE_CHANGE_EVENT_TYPE (wxID_HIGHEST+100)        // 事件类型id
#define ID_MESSAGE_CHANGE (wxID_HIGHEST+12)        // 事件id

class wxMessageUpdateEvent : public wxNotifyEvent
{
public:
    wxMessageUpdateEvent (wxEventType commandType = wxEVT_NULL,
      int id = 0): wxNotifyEvent(commandType, id)
    {}

    wxMessageUpdateEvent (const wxMessageUpdateEvent& event): wxNotifyEvent(event)
    {}

    virtual wxEvent *Clone() const
                  { return new wxMessageUpdateEvent (*this); }

    virtual ~wxMessageUpdateEvent(){}
DECLARE_DYNAMIC_CLASS(wxMessageUpdateEvent);
};

typedef void (wxEvtHandler::*wxMessageUpdateEventFunction)
                                        (wxMessageUpdateEvent&);


BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_COMMAND_MESSAGE_CHANGED, ID_MESSAGE_CHANGE_EVENT_TYPE)
END_DECLARE_EVENT_TYPES()

#define EVT_MESSAGE_CHANGED(id, fn) DECLARE_EVENT_TABLE_ENTRY( \
wxEVT_COMMAND_MESSAGE_CHANGED, id, -1, (wxObjectEventFunction) (wxEventFunction) \
(wxMessageUpdateEventFunction) & fn,(wxObject *) NULL ),
#endif
