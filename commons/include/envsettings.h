/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#ifndef __BP_PRJ_ENVSETTINGS_H__
#define __BP_PRJ_ENVSETTINGS_H__
#include <wx/dialog.h>
#include <wx/textctrl.h>
#include <wx/process.h>

#include "wx.h"
#include "simuproject.h"
#include "datatype.h"

#define wxXML_SETTING_FILE_ROOT wxT("SimuProject-Setting")
#if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_) || defined(WIN32))
#define wxXML_SETTING_FILE_PATH wxT("\\config\\EnvSetting.xml")
#else
#define wxXML_SETTING_FILE_PATH wxT("/config/EnvSetting.xml")
#endif
#if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_) || defined(WIN32))
#define MOLCAS_FILE_TYPES_CONFIG_FILE wxT("\\config\\FileTypesConfig.xml")
#else
#define MOLCAS_FILE_TYPES_CONFIG_FILE wxT("/config/FileTypesConfig.xml")
#endif
#define MOLCAS_PATH_ELEMENT_NAME         wxT("MolcasPath")
#define MOPAC_PATH_ELEMENT_NAME         wxT("MopacPath")
#define MOPAC_LIB_PATH_ELEMENT_NAME     wxT("MopacLibPath")
#define DEFWORK_PATH_ELEMENT_NAME       wxT("DefWorkPath")
#define MOLDEN_PATH_ELEMENT_NAME   wxT("MoldenPath")
// #define MOLGV_PATH_ELEMENT_NAME    wxT("MolGVPath")
#define PEGAMOID_PATH_ELEMENT_NAME    wxT("PegamoidPath")
#define OTHERSOFT_PATH_ELEMENT_NAME    wxT("OthersoftPath")

#define MOPAC_ERROR_STRING_OK wxT("MOPAC is ready to use")
#define MOPAC_ERROR_STRING_LIB wxT("MOPAC library path is invalid")
#define MOPAC_ERROR_STRING_PATH wxT("MOPAC path is invalid")
#define MOPAC_ERROR_STRING_PASSWORD wxT("MOPAC license is not valid")
#define MOPAC_ERROR_STRING_UNKNOWN wxT("MOPAC error unknown")

#define MOLCAS_ERROR_STRING_OK wxT("OpenMolcas is ready to use")
#define MOLCAS_ERROR_STRING_UNAVAILABLE wxT("OpenMolcas is unavailable")

typedef enum _MOPAC_CHECK{
    MOPAC_OK = 0,
    MOPAC_ERROR_LIB,
    MOPAC_ERROR_PATH,
    MOPAC_ERROR_PASSWORD,
    MOPAC_ERROR_UNKNOWN
}MOPAC_CHECK;


/**
 * Simple wxProcess to accept input
 */
class SimpleProcess : public wxProcess
{
public:
    SimpleProcess(int flags, const wxString & input) : wxProcess(flags)
    {
        m_input = input;
    }

    virtual void OnTerminate(int pid, int status);

private:
    wxString m_input;
};

/**
 * Environment Settings Data management
 *
 * Use global instance through EnvSettings::Get()
 * Create, read and write Environment Settings files
 * Check availability of MOPAC
 */
class EnvSettingsData
{
public:
    static void Free();
    static EnvSettingsData * Get();
    wxString GetDefWorkPath();
    void     SaveDefWorkPath(wxString &defWorkPath);
    wxString GetMolcasPath();
    wxString GetMopacPath();
    wxString GetMopacLibPath();
    wxString GetMoldenPath();
    // wxString GetMolGVPath();
    wxString GetPegaPath();
    wxString GetOthersoftPath();
    wxString GetChemSoftDir(ProjectType type);
    wxString GetFileTypeCommand(const wxString &type);

    void Save(const StringHash& envPaths, const StringHash& fileTypes);

    MOPAC_CHECK CheckMopac();
    MOPAC_CHECK CheckMopac(const wxString& mopacExe, const wxString& mopacLibPath);

private:
    EnvSettingsData(wxString &fullName, wxString rootName);

    wxString GetElement(const wxString &elemName);
    wxString GetFullPath()const {return m_fullFileName;}
    void     SetFullPath(wxString fullPath) {m_fullFileName = fullPath; }
    void     Save();

    wxString m_fullFileName;

};

/**
 * Environement Settings Dialog
 *
 * Interface to access EnvSettingsData
 */
class EnvSettings : public wxDialog {

public:
    EnvSettings(wxWindow *parent, wxWindowID id = wxID_ANY, const wxString &title = wxT("Environment Settings"),
                const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize,
                long style = wxDEFAULT_DIALOG_STYLE );
    ~EnvSettings();

    void    CreateUI();
    void    InitCompVal();


private:
    wxString GetFileTypesHelp();
    void     Save();

    DECLARE_EVENT_TABLE();

    void    OnDefWorkPath(wxCommandEvent &event);
    void    OnMolcasPath(wxCommandEvent &event);
    void    OnMopacPath(wxCommandEvent &event);
    void    OnMopacLibPath(wxCommandEvent &event);
    void    OnMoldenPath(wxCommandEvent &event);
    // void    OnMolGVPath(wxCommandEvent &event);
    void    OnPegaPath(wxCommandEvent &event);
    void    OnOthersoftPath(wxCommandEvent &event);
    void    OnDefaultFileTypes(wxCommandEvent &event);
    void    OnCheckMolcas(wxCommandEvent &event);
    void    OnCheckMopac(wxCommandEvent &event);
    void    OnMopacLicense(wxCommandEvent &event);
    void    OnOk(wxCommandEvent &event);
    void    OnCancel(wxCommandEvent &event);
    
    //.....................................................................//
    /**
     * Additional Directories for the PATH env variable
     */
    wxTextCtrl *m_pTcMolcasPath;

    /**
     * Path to Mopac Executable
     */
    wxTextCtrl *m_pTcMopacPath;

    /**
     * Path to Mopac library directory
     */
    wxTextCtrl *m_pTcMopacLibPath;

    /**
     * Path to User Workspace
     */
    wxTextCtrl *m_pTcDefWorkPath;
#ifndef __SIMU_Q__
    /**
     * Path to Molden executable
     */
    wxTextCtrl *m_pTcMoldenPath;
    /**
     * Path to MOLCAS gv executable
     */
    // wxTextCtrl *m_pTcMolGVPath;
    /**
     * Path to Pegamoid executable
     */
    wxTextCtrl *m_pTcPegaPath;
    /**
     * Path to Othersoft executable
     */
    wxTextCtrl *m_pTcOthersoftPath;
    /**
     * Commands to open file types.
     * $f will be replaced by the file name
     * ex.: molden='/usr/local/bin/molden $f'
     */
    wxTextCtrl *m_pTcFileTypes;
    /**
     * Help for file types commands
     */
    wxStaticText *m_pLabelFileTypesHelp;
    /**
     * Indicate the state of MOLCAS
     */
    wxStaticText *m_pLabelMolcasCheck;
    /**
     * Indicate the state of MOPAC
     */
    wxStaticText *m_pLabelMopacCheck;
#endif
};


class ColrSettings : public wxDialog {
public :

    ColrSettings(wxWindow *parent);
    ~ColrSettings();
    wxColour GetColour() {
        return colour;
    }
    // void OnChooseBC();
// private :
    DECLARE_EVENT_TABLE()

    void OnChoose(wxCommandEvent &event);
    void OnOk(wxCommandEvent &event);
    void OnCancel(wxCommandEvent &event);
    void OnDefault(wxCommandEvent &event);

    wxColour colour;
    wxButton *bgBtn;
    wxWindow *bgShow;

};


#endif
