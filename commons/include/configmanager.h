/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_CONFIG_MANAGER_H__
#define __BP_CONFIG_MANAGER_H__

#include "wx.h"

#include <wx/thread.h>
#include <wx/hashmap.h>

#include "xmlfile.h"
#include "xmlparser.h"

WX_DECLARE_STRING_HASH_MAP(XmlFile*, ConfigMap);

class ConfigManager {
public:
        static ConfigManager* Get(void);
        static void Free(void);

        XmlFile* GetConfig(const wxString& appName);

private:
        ConfigManager();
        ~ConfigManager();

private:
        ConfigMap m_configMap;
        wxCriticalSection m_cs;
};



#endif
