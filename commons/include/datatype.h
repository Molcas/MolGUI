/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DATATYPE_H__
#define __BP_DATATYPE_H__

#include "wx.h"

#include <wx/hashset.h>

#ifndef PI
#define PI 3.141592653589793
#endif

#define RADIAN (PI/180.0)

#ifndef BP_PREC
#define BP_PREC 10e-6
#endif

#define BP_NULL_VALUE -100

WX_DEFINE_ARRAY_INT(int, wxIntArray);
WX_DEFINE_ARRAY_DOUBLE(double, wxDoubleArray);

#ifndef STRING_HASH
#define STRING_HASH
        WX_DECLARE_HASH_MAP(wxString, wxString, wxStringHash, wxStringEqual, StringHash);
#endif

#ifndef STRING_SET
#define STRING_SET
        WX_DECLARE_HASH_SET(wxString, wxStringHash, wxStringEqual, StringSet);
#endif

#ifndef WX_INT_HASH
#define WX_INT_HASH
        WX_DECLARE_HASH_MAP(int, int, wxIntegerHash, wxIntegerEqual, wxIntHash);
#endif

#ifndef __HASH_SET_DECLARE__
#define __HASH_SET_DECLARE__
        #ifdef __WINVC__
                #pragma warning( push )
                #pragma warning( disable : 4284 )
        #endif

WX_DECLARE_HASH_MAP( wxString,                        // type of the keys
                     double,                        // type of the values
                     wxStringHash,                // hasher
                     wxStringEqual,                // key equality predicate
                     StrDHash);                        // name of the class
WX_DECLARE_HASH_MAP( wxString,
                     long,
                     wxStringHash,
                     wxStringEqual,
                     StrLHash);
WX_DECLARE_HASH_MAP( long,
                     wxString,
                     wxIntegerHash,
                     wxIntegerEqual,
                     LngSHash);
WX_DECLARE_HASH_MAP( long,
                     long,
                     wxIntegerHash,
                     wxIntegerEqual,
                     LngLHash);
WX_DECLARE_HASH_SET( long,
                     wxIntegerHash,
                     wxIntegerEqual,
                     wxSetLong);
        #ifdef __WINVC__
                #pragma warning( pop )
        #endif
#endif//__HASH_SET_DECLARE__

WX_DECLARE_HASH_MAP( wxString,
                     wxWindow*,
                     wxStringHash,
                     wxStringEqual,
                     WindowPtrHash);

WX_DEFINE_ARRAY(wxWindow*, WindowPtrArray);

#endif
