/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_ENV_UTIL_H__
#define __BP_ENV_UTIL_H__
#include "wx.h"

namespace platform {
        enum identifier {
            platform_unknown,
            platform_windows,
            platform_linux,
            platform_macosx
        };

        #if(wxUSE_UNICODE)
                const bool unicode = true;
        #else
                const bool unicode = false;
        #endif

        #if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_))
                const identifier id = platform_windows;
        #elif defined (__MAC__)
                const identifier id = platform_macosx;
        #elif defined (__LINUX__)
                const identifier id = platform_linux;
        #else
                const identifier id = platform_unknown;
        #endif

        const bool windows = (id == platform_windows);
        const bool macosx  = (id == platform_macosx);
        const bool oslinux   = (id == platform_linux);

        extern wxString PathSep(void);
};

class EnvUtil {
public:
     static bool CheckLicense(void);
     static void InitEnv(void);
     static void InitEnvPaths(wxString baseDir);
     static wxString GetWorkDir(void);
     static wxString GetDefWorkPath(void);
     static wxString GetBaseDir(void);
     static wxString GetUserDataDir(void);
     static wxString GetLogDir(void);
     static wxString GetHistoryDir(void);
     static wxString GetProgramName(wxString progName);
     static wxString GetPrgRootDir(void);
     static wxString GetPrgResDir(void);
     static wxArrayString GetHWInfo(bool first);
     static wxString GetLicenseCode(wxString mainten,wxString expdate,wxString hwinfo);
     static wxString GetEncryptCode(wxString str);
     static wxString GetDecipherCode(wxString str);
     static void WriteLicenseCode(bool first);
     static wxString GetMolcasDir(const wxString& molcasCmd=wxEmptyString);
     static wxString GetMolcasGVDir(void);
     static wxString GetPegamoidDir(void);
     static wxString GetOthersoftDir(void);
// molcasrc related code:
//   static wxString GetMolcasRCFile(void);
     static wxString GetMoldenPath(void);
     static wxString GetMasterKey(void);
     static wxString GetMasterKeyMD5(void);
     static void SetMasterKey(wxString masterkey);
     static wxString GetMasterId(void);
     static void SetMasterId(wxString masterid);
     static wxString GetProjectType(void);
     static void SetProjectType(wxString prjtype);
     static wxString GetMD5String(wxString str);
     static wxString GetMopacDir(void);
     static void SetProjectDir(const wxString dir);
     static wxString GetProjectDir(void);
     static wxString GetPlatform(void);
     static EnvUtil* GetInstance(void);
private:
     EnvUtil(void);
     bool CheckLicenseImpl(void);
     void InitEnvImpl(void);
     void InitEnvPathsImpl(wxString baseDir);
     wxString GetWorkDirImpl(void);
     wxString GetDefWorkPathImpl(void);
     wxString GetBaseDirImpl(void);
     wxString GetUserDataDirImpl(void);
     wxString GetLogDirImpl(void);
     wxString GetHistoryDirImpl(void);
     wxString GetProgramNameImpl(wxString progName);
     wxString GetPrgRootDirImpl(void);
     wxString GetPrgResDirImpl(void);
     wxArrayString GetHWInfoImpl(bool first);
     wxString GetLicenseCodeImpl(wxString mainten,wxString expdate,wxString hwinfo);
     wxString GetEncryptCodeImpl(wxString str);
     wxString GetDecipherCodeImpl(wxString str);
     void WriteLicenseCodeImpl(bool first);
     wxString GetMolcasDirImpl(const wxString& molcasCmd=wxEmptyString);
     wxString GetMolcasGVDirImpl(void);
     wxString GetPegamoidDirImpl(void);
     wxString GetOthersoftDirImpl(void);
// molcasrc related code:
//   wxString GetMolcasRCFileImpl(void);
     wxString GetMoldenPathImpl(void);
     wxString GetMasterKeyImpl(void);
     wxString GetMasterKeyMD5Impl(void);
     void SetMasterKeyImpl(wxString masterkey);
     wxString GetMasterIdImpl(void);
     void SetMasterIdImpl(wxString masterid);
     wxString GetProjectTypeImpl(void);
     void SetProjectTypeImpl(wxString prjtype);
     wxString GetMD5StringImpl(wxString str);
     wxString GetMopacDirImpl(void);
     wxString GetMopacEnvComImpl();
     void SetProjectDirImpl(const wxString dir);
     wxString GetProjectDirImpl(void);
     wxString GetPlatformImpl(void);
     wxString m_strBaseDir; // installation dir.
     wxString m_strWorkDir;
     wxString m_strTmpDataDir; //temp file directory of the project.
     wxString m_strProjectDir; // save the path-info of the current project, changed when "open","new","save as" a project.
     wxString m_strMolcasDir;
     wxString m_strMolcasGVDir;
     wxString m_strPegamoidDir;
     wxString m_strOthersoftDir;
     wxString m_strMasterKey;
     wxString m_strMasterId;
     wxString m_projectType;
     wxString m_platform;
     static EnvUtil* s_instance;
};

#endif
