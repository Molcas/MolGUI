/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_APP_GLOBALS_H__
#define __BP_APP_GLOBALS_H__

#include "wx.h"

namespace appglobals {
    extern const wxString appVendor;
    extern const wxString appName;
    extern const wxString appVersion;
    extern const wxString appUrl;
    extern const wxString appContactEmail;
    extern const wxString appPlatform;
    extern const wxString appBuildTimestamp;
};

#endif
