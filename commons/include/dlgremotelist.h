/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DLGREMOTELIST_H__
#define __BP_DLGREMOTELIST_H__

#include "wx.h"

class DlgRemoteList: public wxDialog
{

public:
        void SetInputFileName(wxString inputfile);
        wxString GetInputFileName();
        void DoOk(wxCommandEvent& event);
        void DoCancel(wxCommandEvent&event);
        DlgRemoteList(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Select host"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
        ~DlgRemoteList();
        bool SaveJobInfo();
        void LoadSelectHost(void);
        void SaveSelectHost(void);
        void OnNewHost(wxCommandEvent& event);
        void RefreshHostList(void);
    void ReplaceScript(wxString scriptfile);
    wxString GetFilePath();
private:
        DECLARE_EVENT_TABLE();
        wxComboBox *m_combHostname;
        wxArrayString m_hostarray;
    wxString m_inputfile;
private:
        void CreateGUIControls(void);
        void OnClose(wxCloseEvent& event);

};

#endif
