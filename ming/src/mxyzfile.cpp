/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "mxyzfile.h"
#include "cstringutil.h"
#include "keywordconst.h"
#include <iostream>

#define LINE_BUF_SIZE 512
#define VAR_BUF_SIZE 64

const char* AtomSymbols[] = {
        "",
        "H",
        "He",
        "Li",
        "Be",
        "B",
        "C",
        "N",
        "O",
        "F",
        "Ne",
        "Na",
        "Mg",
        "Al",
        "Si",
        "P",
        "S",
        "Cl",
        "Ar",
        "K",
        "Ca",
        "Sc",
        "Ti",
        "V",
        "Cr",
        "Mn",
        "Fe",
        "Co",
        "Ni",
        "Cu",
        "Zn",
        "Ga",
        "Ge",
        "As",
        "Se",
        "Br",
        "Kr",
        "Rb",
        "Sr",
        "Y",
        "Zr",
        "Nb",
        "Mo",
        "Tc",
        "Ru",
        "Rh",
        "Pd",
        "Ag",
        "Cd",
        "In",
        "Sn",
        "Sb",
        "Te",
        "I",
        "Xe",
        "Cs",
        "Ba",
        "La",
        "Ce",
        "Pr",
        "Nd",
        "Pm",
        "Sm",
        "Eu",
        "Gd",
        "Tb",
        "Dy",
        "Ho",
        "Er",
        "Tm",
        "Yb",
        "Lu",
        "Hf",
        "Ta",
        "W",
        "Re",
        "Os",
        "Ir",
        "Pt",
        "Au",
        "Hg",
        "Tl",
        "Pb",
        "Bi",
        "Po",
        "At",
        "Rn",
        "Fr",
        "Ra",
        "Ac",
        "Th",
        "Pa",
        "U",
        "Np",
        "Pu",
        "Am",
        "Cm",
        "Bk",
        "Cf",
        "Es",
        "Fm",
        "Md",
        "No",
        "Lr",
        "Rf",
        "Db",
        "Sg",
        "Bh",
        "Hs",
        "Mt",
        "Uun",
        "Uuu",
        "Uub",
        "X",
        "Uuq",
        NULL
};

int MXYZFile::GetAtomicNumber(const char* atomSymbol) {
    int number = 1;
    do{
        if(strcmp(atomSymbol, AtomSymbols[number]) == 0){
            return number;
        }
    }while(AtomSymbols[++number] != NULL);

    return -1;
}

int MXYZFile::ConvertXYZ2SymmetryInput(const char* inputFileName, const char* outputFileName, double threshould, char*** eleSymbols) {
    FILE* fpInput = NULL;
    FILE* fpOutput = NULL;
    int atomCount = -1;
    int i = 0, j = 0;
    char symbol[20];
    char buf[LINE_BUF_SIZE];
    char * tmp_for_warning=NULL;

    double pos[3];
    char** tmpSymbols = NULL;
    int tmpSymbolCount = 0;
    int isFound = 0;

        fpInput = fopen(inputFileName, "r");
        if(fpInput == NULL) {
                printf("No such file: %s\n", inputFileName);
                return 0;
        }

        fpOutput = fopen(outputFileName, "w");
        if(fpOutput == NULL) {
                printf("Cannot write to file: %s\n", outputFileName);
                return 0;
        }

    fprintf(fpOutput, "%lf\n", threshould);
    tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
    sscanf(buf, "%d", &atomCount);                                //avoid to use fscanf
    tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);       //neglect comment line
    for(i = 0; i < atomCount; i++) {
            tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
            sscanf(buf, "%s %lf %lf %lf", symbol, &pos[0], &pos[1], &pos[2]);
        //fprintf(fpOutput, "%s %d %lf %lf %f\n", symbol, GetAtomicNumber(symbol), pos[0], pos[1], pos[2]);
        fprintf(fpOutput, "%s %lf %lf %f\n", symbol, pos[0], pos[1], pos[2]);
        isFound = 0;
                for(j = 0; j < (int)strlen(symbol); j++) {
                        if(!(toupper(symbol[j]) <= 'Z' && toupper(symbol[j] >= 'A'))) {
                                //remove other characters which are not the name of an elements.
                                symbol[j] = '\0';
                                break;
                        }
                }
        for(j = 0; j < tmpSymbolCount; j++) {
            if(strcmp(tmpSymbols[j], symbol) == 0) {
                isFound = 1;
            }
        }
        if(!isFound) {
            // record down distinct element symbols
            tmpSymbols = (char**)RequestMemory(tmpSymbols, tmpSymbolCount, 10, sizeof(char*));
            StringCopy(&tmpSymbols[tmpSymbolCount], symbol, strlen(symbol));
            tmpSymbolCount++;
        }
    }

    *eleSymbols = tmpSymbols;
        fclose(fpInput);
        fclose(fpOutput);
        if(tmp_for_warning!=NULL)
        tmp_for_warning=NULL;

        return tmpSymbolCount;
}

FILE* MXYZFile::OpenBasisFile(const char* dir, int eleNumber, const char* postfix, const char* mode) {
    char* fileName = NULL;
    FILE* fp = NULL;

        if(eleNumber <= 0) return NULL;
    fileName = (char*)malloc(strlen(dir) + 20);
        if( NULL == fileName) {
                printf("cannot allocate memory\n");
                return NULL;
        }
    fileName[0] = 0;
    //strcpy(fileName, dir);
    strncat(fileName, dir, strlen(dir));
    strncat(fileName, AtomSymbols[eleNumber], strlen(AtomSymbols[eleNumber]));
    strcat(fileName, postfix);
    // std::cout << "Openbasisfile: fileName " << fileName << std::endl;
    fp = fopen(fileName, mode);
        if(fp == NULL) {
                printf("No such file: %s\n", fileName);
        }

    free(fileName);

    return fp;
}

int MXYZFile::GenerateBasisSetFiles(const char* inputDir, const char* outputDir) {
    int number = 1;
    do{
        if(!GenerateBasisSetFile(inputDir, outputDir, number)) {
            return 0;
        }
        number++;
    }while(AtomSymbols[number] != NULL && strlen(AtomSymbols[number]) > 0);

    return 1;
}

int MXYZFile::GenerateBasisSetFile(const char* inputDir, const char* outputDir, int eleNumber) {
    FILE* fpInput = NULL;
    FILE* fpOutput = NULL;
    char buf[LINE_BUF_SIZE];
    int substrIndex = 0;
    int recommendedBasis = 0, otherBasis = 0;
    int recommendedBasisCount = 0, otherBasisCount = 0;
    char** data = NULL;
    int i, tmp;
    char * tmp_for_warning=NULL;

    fpInput = OpenBasisFile(inputDir, eleNumber, ".basis", "r");
    fpOutput = OpenBasisFile(outputDir, eleNumber, ".basis", "w");

        if(fpInput== NULL || fpOutput == NULL) {
                printf("Cannot convert basis set file for element %s from %s to %s\n", AtomSymbols[eleNumber], inputDir, outputDir);
                return 0;
        }

        while(!feof(fpInput)) {
                memset(buf, 0, sizeof(buf));
                tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
                size_t len = strlen(buf);
                if(len == 0) continue;
                if(!recommendedBasis && !otherBasis) {
            substrIndex = SubStringIndex(buf, "Recommended basis sets", 0);
            if(substrIndex >= 0) {
                recommendedBasis = 1;
                fprintf(fpOutput, "#RECOMMENDED\n");
                continue;
            }
        }
        if(!otherBasis) {
            substrIndex = SubStringIndex(buf, "Other basis sets", 0);
            if(substrIndex >= 0) {
                recommendedBasis = 0;
                otherBasis = 1;
                fprintf(fpOutput, "#OTHER\n");
                continue;
            }
        }
        substrIndex = SubStringIndex(buf, AtomSymbols[eleNumber], 0);
        if(substrIndex != 0) continue;
        if(recommendedBasis) {
            // parse recommended basis sets;
            data =  StringTokenizer(buf, " ", &tmp, 0);
            for(i = 0; i < tmp; i++) {
                fprintf(fpOutput, "%s\n", data[i]);
            }
            FreeStringArray(data, tmp);
            recommendedBasisCount++;
        }
        if(otherBasis) {
            // parse other basis sets;
            fprintf(fpOutput, "%s\n", StringTrim(buf, NULL));
            otherBasisCount++;
        }
        }

    fclose(fpInput);
        fclose(fpOutput);
    if(tmp_for_warning!=NULL)
        tmp_for_warning=NULL;
        return 1;
}

char *MXYZFile::TrimBasis(char *buf)
{   int i=0,len=0;
    len=strlen(buf);
        for(i=0;i<len;i++){
                if(buf[i]==' ')
           {  buf[i]='\0';
                                break;
                }

         }

    return buf;
}

char** MXYZFile::GetBasisSet(const char* basisDir, const char* eleSymbol, int* totalStringCount, int* recommendedCount, int* otherCount)
{
    char** basisSets = NULL;
    char buf[LINE_BUF_SIZE];
    int eleNumber = GetAtomicNumber(eleSymbol);
    int tmpRecommended = 0, tmpOther = 0, tmpTotal = 0;
    int isRecommendedBasis = 0;
        unsigned i = 0;
        char * tmp_for_warning=NULL;

    FILE* fpInput = OpenBasisFile(basisDir, eleNumber, ".basis", "r");
    if(fpInput== NULL) {
                printf("Cannot open basis set file for element %s in dir %s\n", AtomSymbols[eleNumber], basisDir);
                return NULL;
        }

    while(!feof(fpInput))
        {
                memset(buf, 0, sizeof(buf));
                tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
//                if(buf[0]!=' '){
                if(SubStringIndex(buf, "not found", 0) >= 0) continue;
//begin : hurukun : 26/11/2009
//#ifdef __MOLCAS__
                if(SubStringIndex(buf, "#RECOMMENDED", 0) >= 0)
//#else
//                if(SubStringIndex(buf, "Recommended", 0) >= 0)
//#endif
//end   : hurukun : 26/11/2009
                {
                            isRecommendedBasis = 1;
                }
//begin : hurukun : 26/11/2009
//#ifdef __MOLCAS__
                else if(SubStringIndex(buf, "#OTHER", 0) >= 0)
//#else
//                else if(SubStringIndex(buf, "Other", 0) >= 0)
//#endif
//end   : hurukun : 26/11/2009
                {
            isRecommendedBasis = 0;
        }
                else if(strlen(buf) > 0)
                {
            // remove the newline char
            StringRightTrim(buf, NULL);
                        if(strlen(buf) <= 0) continue;
            if(isRecommendedBasis)
                        {
                                for(i = 0; i < strlen(buf); i++)
                                {
                                        if(buf[i] == ' ')
                                        {
                                                buf[i] = '\0';
                                                break;
                                        }
                                }
                                tmpRecommended++;
            }
                        else
                        {
                tmpOther++;
            }
            TrimBasis(buf);
                        basisSets = (char**)RequestMemory(basisSets, tmpTotal, 20, sizeof(char*));
                        StringCopy(&basisSets[tmpTotal], buf, strlen(buf));
            tmpTotal++;
                }
    }
    *recommendedCount = tmpRecommended;
    *otherCount = tmpOther;
        *totalStringCount = tmpTotal;

        //wxLogError(wxString::Format("%d", *totalStringCount));
        fclose(fpInput);
        if(tmp_for_warning!=NULL)
        tmp_for_warning=NULL;
        return basisSets;
}
/*
char** MXYZFile::GetSymmetryGenerator(const char* fileName, int* totalCount) {
    char** symGenerators = NULL;
    char buf[LINE_BUF_SIZE];
    int tmpTotal = 0;
    char tempLine[LINE_BUF_SIZE];

        FILE* fpInput = fopen(fileName, "r");
        if(fpInput == NULL) {
                printf("No such file: %s\n", fileName);
                return NULL;
        }

    while(!feof(fpInput)) {
                memset(buf, 0, sizeof(buf));
                fgets(buf, LINE_BUF_SIZE, fpInput);
                if(strlen(buf) > 2 && buf[0] == '+') {
            sscanf(buf + 2, "%s", tempLine);
            symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
            StringCopy(&symGenerators[tmpTotal], tempLine, strlen(tempLine));
            tmpTotal++;

            fgets(buf, LINE_BUF_SIZE, fpInput);
            symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
            StringCopy(&symGenerators[tmpTotal], buf, strlen(buf));
            tmpTotal++;
        }
    }
    *totalCount = tmpTotal/2;

        fclose(fpInput);
        return symGenerators;
}*/
/**
 * FULL: the first one
 * NOSYM: the last one
 * Otherwise: the matched one
 */
int MXYZFile::GetUniqueAtomCount(const char* fileName, const char* symgen) {
    int uniqueAtom = 0;
    char buf[LINE_BUF_SIZE];
    char symName[32];
    char * tmp_for_warning=NULL;
    FILE* fpInput = fopen(fileName, "r");
        if(fpInput == NULL) {
                printf("No such file: %s\n", fileName);
        }else {
        while(!feof(fpInput)) {
                    memset(buf, 0, sizeof(buf));
                    tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
                    if(strlen(buf) > 2 && buf[0] == '+') {
                symName[0] = 0;
                sscanf(&buf[2], "%s", symName);
                tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
                tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
                sscanf(buf, "%d", &uniqueAtom);

                if(IsStringEqualNoCase(ATTR_VALUE_GROUP_FULL, symgen, strlen(ATTR_VALUE_GROUP_FULL))) {
                    break;
                }else if(IsStringEqualNoCase(symName, symgen, strlen(symName))) {
                    break;
                }
            }
        }
        fclose(fpInput);
    }
    if(tmp_for_warning!=NULL)
        tmp_for_warning=NULL;
    return uniqueAtom;
}

char** MXYZFile::GetSymmetryGenerator(const char* fileName, int* totalCount) {
    char** symGenerators = NULL;
    char buf[LINE_BUF_SIZE];
    int tmpTotal = 0;
    char generators[3][VAR_BUF_SIZE];   // generator string
    int generatorAlges[3];             // generator algebra
    char generatorName[VAR_BUF_SIZE];
    char tempGenerator[VAR_BUF_SIZE];
    char tempName[VAR_BUF_SIZE];
    int generatorCount = 0;
    int i = 0, tempNumber, tempResult;
    unsigned k;
    char * tmp_for_warning=NULL;

    symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
    StringCopy(&symGenerators[tmpTotal], ATTR_VALUE_GROUP_FULL, strlen(ATTR_VALUE_GROUP_FULL));
    tmpTotal++;
    StringCopy(&symGenerators[tmpTotal], ATTR_VALUE_GROUP_NOSYM, strlen(ATTR_VALUE_GROUP_NOSYM));
    tmpTotal++;

        FILE* fpInput = fopen(fileName, "r");
        if(fpInput == NULL) {
                printf("No such file: %s\n", fileName);
        }else {
        while(!feof(fpInput)) {
                    memset(buf, 0, sizeof(buf));
                    tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
                    if(strlen(buf) > 2 && buf[0] == '+') {
                //initialize
                generatorName[0] = 0;
                for( i = 0; i < 3; i++) {
                    generators[i][0] = 0;
                    generatorAlges[i] = 0;
                }
                // line such as + C2v 0.000000
                sscanf(buf + 2, "%s", generatorName);
                //line such as 2 yz z
                tmp_for_warning=fgets(buf, LINE_BUF_SIZE, fpInput);
                sscanf(buf, "%d", &generatorCount);
                if(generatorCount == 0) {
                }else if(generatorCount == 1) {
                    sscanf(buf, "%d %s", &generatorCount, generators[0]);
                }else if(generatorCount == 2) {
                    sscanf(buf, "%d %s %s", &generatorCount, generators[0], generators[1]);
                }else if(generatorCount == 3) {
                    sscanf(buf, "%d %s %s %s", &generatorCount, generators[0], generators[1], generators[2]);
                }

                // transfer string to integer
                for(i = 0 ; i < generatorCount; i++) {
                    tempNumber = 0;
                    for(k = 0; k < strlen(generators[i]); k++) {
                        switch(generators[i][k]) {
                            case 'x':
                            case 'X':
                                tempNumber += 4;
                                break;
                            case 'y':
                            case 'Y':
                                tempNumber += 2;
                                break;
                            case 'z':
                            case 'Z':
                                tempNumber += 1;
                                break;
                            default:
                                break;
                        }
                    }
                    generatorAlges[i] = tempNumber;
                }
                //
                if(generatorCount == 0) {
                    strcpy(tempGenerator, generatorName);
                    //
                    symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
                    StringCopy(&symGenerators[tmpTotal], tempGenerator, strlen(tempGenerator));
                    tmpTotal++;
                }else if(generatorCount == 1) {
                    strcpy(tempGenerator, generatorName);
                    strcat(tempGenerator, ": ");
                    strcat(tempGenerator, generators[0]);
                    //
                    symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
                    StringCopy(&symGenerators[tmpTotal], tempGenerator, strlen(tempGenerator));
                    tmpTotal++;
                }else if(generatorCount == 2) {
                    tempResult = generatorAlges[0] ^ generatorAlges[1];
                    k = 0;
                    if(tempResult & 4) {
                        tempName[k++] = 'x';
                    }else if(tempResult & 2) {
                        tempName[k++] = 'y';
                    }else if(tempResult & 1) {
                        tempName[k++] = 'z';
                    }
                    tempName[k] = 0;
                    // the original generator
                    strcpy(tempGenerator, generatorName);
                    strcat(tempGenerator, ": ");
                    strcat(tempGenerator, generators[0]);
                    strcat(tempGenerator, " ");
                    strcat(tempGenerator, generators[1]);
                    //
                    symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
                    StringCopy(&symGenerators[tmpTotal], tempGenerator, strlen(tempGenerator));
                    tmpTotal++;
                    // derived generator 1
                    strcpy(tempGenerator, generatorName);
                    strcat(tempGenerator, ": ");
                    strcat(tempGenerator, generators[0]);
                    strcat(tempGenerator, " ");
                    strcat(tempGenerator, tempName);
                    //
                    symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
                    StringCopy(&symGenerators[tmpTotal], tempGenerator, strlen(tempGenerator));
                    tmpTotal++;
                    // derived generator 2
                    strcpy(tempGenerator, generatorName);
                    strcat(tempGenerator, ": ");
                    strcat(tempGenerator, generators[1]);
                    strcat(tempGenerator, " ");
                    strcat(tempGenerator, tempName);
                    //
                    symGenerators = (char**)RequestMemory(symGenerators, tmpTotal, 10, sizeof(char*));
                    StringCopy(&symGenerators[tmpTotal], tempGenerator, strlen(tempGenerator));
                    tmpTotal++;
                }else if(generatorCount == 3) {
                    //TODO:
                }
            }
        }
    }
    *totalCount = tmpTotal;
    if(fpInput) fclose(fpInput);
    if(tmp_for_warning!=NULL)
        tmp_for_warning=NULL;
        return symGenerators;
}

//defining basis set
char** MXYZFile::GetGlobalBasisSet(const char* basisDir, char** eleSymbols, int eleCount,
        int* globalTotalStringCount, int* globalRecommendedCount, int* globalOtherCount) {
    int totalStringCountFirst, recommendedCountFirst, otherCountFirst, totalStringCount, recommendedCount, otherCount;
    int index, i, k;
    char** firstSet = NULL;
    char** leftSet = NULL;
    char** globalBasisSet = NULL;
    int* indicator = NULL;

    int firstSymbolLen = 0;
    int leftSymbolLen = 0;
    int tempTotalStringCount = 0;
    int tempRecommendedCount = 0;
    int tempOtherCount = 0;

    *globalTotalStringCount = 0;
    *globalRecommendedCount = 0;
    *globalOtherCount = 0;
    //--------------------------------grp
    if(eleSymbols == NULL || eleCount <= 1) {
    }else {
        index = 0;
        do{
            //find the first element symbol whose basis set is not empty
            firstSymbolLen = strlen(eleSymbols[index]) + 1;
            if(firstSet) {
                FreeStringArray(firstSet, totalStringCountFirst);
                // std::cout << "firstSet " << firstSet[index] << "  totalStringCountFirst "<< totalStringCountFirst << std::endl;

            }
            // std::cout << "firstSymbolLen " << firstSymbolLen << std::endl;
            firstSet = GetBasisSet(basisDir, eleSymbols[index], &totalStringCountFirst, &recommendedCountFirst, &otherCountFirst);
            
            // std::cout << "basisdir: " << basisDir << " elesym: " << eleSymbols[index] << " totalStringCountFirst " << totalStringCountFirst << " recommendedCountFirst " << recommendedCountFirst << " otherCountFirst " << otherCountFirst << std::endl;
            index++;
        }while(firstSet == NULL && recommendedCountFirst <= 0 && index < eleCount);

        if(firstSet == NULL || index == eleCount) {
        }else {
            //initialize all the basis set of the first element as global basis set by default
            indicator = new int[totalStringCountFirst];
            for(i = 0; i < totalStringCountFirst; i++) {
                indicator[i] = 1;
            }
            // std::cout << "index " << index << std::endl;
            for(; index < eleCount; index++) {
                leftSymbolLen = strlen(eleSymbols[index]) + 1;
                leftSet = GetBasisSet(basisDir, eleSymbols[index], &totalStringCount, &recommendedCount, &otherCount);
                // std::cout << "basisdir: " << basisDir << " eleSymbols[index]: " << eleSymbols[index] << " totalStringCount " << totalStringCount << " recommendedCount " << recommendedCount << " otherCount " << otherCount << std::endl;
                if(leftSet == NULL || recommendedCount <= 0)
                    continue;
                for(i = 0; i < totalStringCountFirst; i++) {
                    //if it is not valid, it will not be valid anymore
                    if(indicator[i]) {
                        for(k = 0; k < totalStringCount; k++) {
                            // std::cout << "i " << i << " k " << k << " f[i]+fn " << firstSet[i]+firstSymbolLen << " l[k]+ll " << leftSet[k]+leftSymbolLen << std::endl;
                            if(strcmp(firstSet[i]+firstSymbolLen, leftSet[k]+leftSymbolLen) == 0) {
                                break;
                            }
                        }

                        if(k >= totalStringCount) {
                            //no match
                            indicator[i] = 0;
                        }
                        // std::cout << "indicator[i] " << indicator[i] << std::endl;
                    }
                }
                if(leftSet) {
                    FreeStringArray(leftSet, totalStringCount);
                }
            }
            //collect count
            for(i = 0; i < totalStringCountFirst; i++) {
                if(i < recommendedCountFirst) {
                    if(indicator[i]) {
                        tempRecommendedCount++;
                    }
                }else {
                    // if(indicator[i]) {
                        tempOtherCount++;
                    // }
                }

            }
            // std::cout << "tempRecommendedCount: " << tempRecommendedCount << " tempOtherCount " << tempOtherCount << std::endl;
            tempTotalStringCount = tempRecommendedCount + tempOtherCount;
            //collect result
            if(tempTotalStringCount > 0) {
                globalBasisSet = (char**)RequestMemory(globalBasisSet, 0, tempTotalStringCount, sizeof(char*));
                k = 0;

                for(i = 0; i < totalStringCountFirst; i++) {
                    if(i < recommendedCountFirst) {
                        if(indicator[i]) {
                            StringCopy(&globalBasisSet[k], firstSet[i]+firstSymbolLen, strlen(firstSet[i]+firstSymbolLen));
                            k++;
                        }
                    }else {
                        // if(indicator[i]) {
                            StringCopy(&globalBasisSet[k], firstSet[i]+firstSymbolLen, strlen(firstSet[i]+firstSymbolLen));
                            k++;
                        // }
                    }
                }

                *globalTotalStringCount = tempTotalStringCount;
                *globalRecommendedCount = tempRecommendedCount;
                *globalOtherCount = tempOtherCount;
            }
            if(firstSet) {
                FreeStringArray(firstSet, totalStringCountFirst);
            }
            if(indicator) {
                delete[] indicator;
            }
        }
    }
//-------------------------------grp
    return globalBasisSet;
}
