/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/statline.h>

#include "groupviewerdlg.h"
#include "stringutil.h"
#include "keywordui.h"
#include "keywordconst.h"
#include "mingpnl.h"

BEGIN_EVENT_TABLE(GroupViewerDlg, wxDialog)
    EVT_BUTTON(wxID_OK, GroupViewerDlg::OnDialogOK)
    EVT_BUTTON(wxID_CANCEL, GroupViewerDlg::OnDialogCancel)
    EVT_CLOSE(GroupViewerDlg::OnClose)
END_EVENT_TABLE()

GroupViewerDlg::GroupViewerDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style) {
    pPnlProperty = NULL;
    groupUI = NULL;
    groupModel = NULL;
}

GroupViewerDlg::~GroupViewerDlg() {
    if(groupUI) {
        delete groupUI;
    }
}


void GroupViewerDlg::SetGroupModel(GroupModel* grpModel) {
    groupModel = grpModel;

        groupModel->GetChildModule()->UpdateRelation();

        SetTitle(StringUtil::CharPointer2String(groupModel->GetChildModule()->GetAppear()));
        SetSize(600, 400);
        CreateControls();

        GetSizer()->SetSizeHints(this);
        Center();
}

void GroupViewerDlg::CreateControls(void) {
    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);

    pPnlProperty = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER);
    wxBoxSizer* bsProp = new wxBoxSizer(wxHORIZONTAL);
    pPnlProperty->SetSizer(bsProp);
    groupUI = new GroupUI(pPnlProperty, groupModel);
        //groupUI = new GroupUI(this, groupModel);
    bsProp->Add(groupUI, 1, wxEXPAND|wxALL, 2);

    wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnOk = new wxButton(this, wxID_OK, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    wxButton * btnCancel = new wxButton(this, wxID_CANCEL, wxEmptyString, wxDefaultPosition, wxSize(100,30));

    bsBottom->AddStretchSpacer(1);
    bsBottom->Add(btnOk, 0, wxALL, 2);
    bsBottom->Add(btnCancel, 0, wxALL, 2);

    bsTop->Add(pPnlProperty, 1, wxEXPAND|wxALL, 2);
        //bsTop->Add(groupUI, 1, wxEXPAND|wxALL, 2);
    bsTop->AddSpacer(3);
    bsTop->Add(bsBottom, 0, wxEXPAND | wxALL, 2);

    this->SetSizer(bsTop);
}

void GroupViewerDlg::OnDialogOK(wxCommandEvent& event) {
    MingPnl::Get()->DoSaveHistoryFile();
    HideDialog(wxID_OK);
}

void GroupViewerDlg::OnDialogCancel(wxCommandEvent& event) {
    HideDialog(wxID_CANCEL);
}

void GroupViewerDlg::OnClose(wxCloseEvent& event){
    HideDialog(wxID_CANCEL);
}

void GroupViewerDlg::HideDialog(int returnCode) {
    groupUI->UpdateToModel(returnCode == wxID_CANCEL);

    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
        //Destroy();
}

bool GroupViewerDlg::HasValue(void) const {
        if(groupUI)
                return groupUI->HasValue();
        else
                return false;
}
