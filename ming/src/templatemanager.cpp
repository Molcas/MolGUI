/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdio.h>
#include <string.h>

#include "templatemanager.h"
#include "cstringutil.h"
#include "keywordconst.h"

#define LINE_BUF_SIZE 512

TemplateManager::TemplateManager(){
}

TemplateManager::~TemplateManager(){
}

bool TemplateManager::IsTemplateStart(const char* buf) {
    if(IsStringEqualNoCase(buf, CONST_TEMPLATE_START, strlen(CONST_TEMPLATE_START)))
        return true;

    return false;
}

bool TemplateManager::IsTemplateHelp(const char* buf) {
    if(IsStringEqualNoCase(buf, CONST_TEMPLATE_HELP, strlen(CONST_TEMPLATE_HELP)))
        return true;

    return false;
}

char** TemplateManager::GetTemplateNames(const char* fileName, int* count) {
    FILE* fp = NULL;
    char buf[LINE_BUF_SIZE];
    char** templateNames = NULL;
    int templateCount = 0;

    *count = 0;
    fp = fopen(fileName, "r");
        if(fp == NULL) {
                printf("No such file: %s\n", fileName);
                return NULL;
        }

        while(!feof(fp)) {
        if(fgets(buf, LINE_BUF_SIZE, fp) == NULL) buf[0]=0;
        if(IsTemplateStart(buf)) {
            templateNames = (char**)RequestMemory(templateNames, templateCount, 8, sizeof(char*));
            StringRightTrim(buf, NULL);
            StringCopy(&templateNames[templateCount], buf+strlen(CONST_TEMPLATE_START), strlen(buf)-strlen(CONST_TEMPLATE_START));
            templateCount++;
        }
    }
    *count = templateCount;
    if(fp) fclose(fp);
    return templateNames;

}
