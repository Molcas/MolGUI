/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/filename.h>
#include <wx/dir.h>
#include <wx/statline.h>
#include "outputfilelistdlg.h"
#include "fileutil.h"
#include "tools.h"
#include "stringutil.h"
#include "mingutil.h"
#include "fileeditdlg.h"

BEGIN_EVENT_TABLE(OutputFileListDlg, wxDialog)
        EVT_LISTBOX_DCLICK(CTRL_ID_LIST_GV_FILE, OutputFileListDlg::OnGvListDClick)
        EVT_LISTBOX_DCLICK(CTRL_ID_LIST_TEXT_FILE, OutputFileListDlg::OnTextListDClick)
        EVT_LISTBOX(CTRL_ID_LIST_GV_FILE, OutputFileListDlg::OnGvListClick)
        EVT_LISTBOX(CTRL_ID_LIST_TEXT_FILE, OutputFileListDlg::OnTextListClick)

        EVT_TEXT(CTRL_ID_TC_PROJECT, OutputFileListDlg::OnTextCtrlProject)
        EVT_BUTTON(CTRL_ID_BTN_DIR, OutputFileListDlg::OnDirBtn)
        EVT_BUTTON(wxID_CLOSE, OutputFileListDlg::OnDialogOk)
        EVT_BUTTON(CTRL_ID_BTN_SHOW, OutputFileListDlg::OnShowButton)
        EVT_CLOSE(OutputFileListDlg::OnClose)
END_EVENT_TABLE()

OutputFileListDlg::OutputFileListDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style) {
        lbGvFileList = NULL;
        lbTextFileList = NULL;
        isGv = true;

        CreateControls();
}

OutputFileListDlg::~OutputFileListDlg() {
}

void OutputFileListDlg::CreateControls(void) {
        wxBoxSizer* bsToft = new wxBoxSizer(wxVERTICAL);

        wxStaticText* lbFileDir = new wxStaticText(this, wxID_ANY, wxT("Directory"), wxDefaultPosition, wxSize(100, wxDefaultSize.GetHeight()));
        tcDir = new wxTextCtrl(this, CTRL_ID_TC_DIR);
        wxButton* btnDir = new wxButton(this, CTRL_ID_BTN_DIR, wxT("..."), wxDefaultPosition, wxSize(35, wxDefaultSize.GetHeight()));
        wxBoxSizer* bsDir = new wxBoxSizer(wxHORIZONTAL);
        bsDir->Add(lbFileDir, 0, wxALL, 2);
        bsDir->Add(tcDir, 1, wxEXPAND | wxALL, 2);
        bsDir->Add(btnDir, 0, wxALL, 2);

        wxStaticText* lbProject = new wxStaticText(this, wxID_ANY, wxT("Project"), wxDefaultPosition, wxSize(100, wxDefaultSize.GetHeight()));
        tcProject = new wxTextCtrl(this, CTRL_ID_TC_PROJECT);
        wxBoxSizer* bsProject = new wxBoxSizer(wxHORIZONTAL);
        bsProject->Add(lbProject, 0, wxALL, 2);
        bsProject->Add(tcProject, 1, wxEXPAND | wxALL, 2);

        wxBoxSizer* bsGv = new wxBoxSizer(wxVERTICAL);
        wxStaticText* lbGv = new wxStaticText(this, wxID_ANY, wxT("GV Files"));
        lbGvFileList = new wxListBox(this, CTRL_ID_LIST_GV_FILE, wxDefaultPosition, wxSize(200, 250), 0, NULL, wxLB_SINGLE);


        wxStaticText* lbGvPara = new wxStaticText(this, wxID_ANY, wxT("GV Parameters"));
        tcGvPara = new wxTextCtrl(this, CTRL_ID_TC_GV_PARA);

        bsGv->Add(lbGv, 0, wxALL, 2);
        bsGv->Add(lbGvFileList, 1, wxEXPAND | wxALL, 2);
        bsGv->Add(lbGvPara, 0, wxALL, 2);
        bsGv->Add(tcGvPara, 0, wxEXPAND | wxALL, 2);

        wxBoxSizer* bsText = new wxBoxSizer(wxVERTICAL);
        wxStaticText* lbText = new wxStaticText(this, wxID_ANY, wxT("Text Files"));
        lbTextFileList = new wxListBox(this, CTRL_ID_LIST_TEXT_FILE, wxDefaultPosition, wxSize(200, 250), 0, NULL, wxLB_SINGLE);
        bsText->Add(lbText, 0, wxALL, 2);
        bsText->Add(lbTextFileList, 1, wxEXPAND | wxALL, 2);

        wxBoxSizer* bsMiddle = new wxBoxSizer(wxHORIZONTAL);
        bsMiddle->Add(bsGv, 1, wxEXPAND | wxALL, 2);
        bsMiddle->Add(bsText, 1, wxEXPAND | wxALL, 2);

        wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
        wxButton * btnShow = new wxButton(this, CTRL_ID_BTN_SHOW, wxT("S&how"), wxDefaultPosition, wxSize(100, 30));
        wxButton * btnClose = new wxButton(this, wxID_CLOSE, wxEmptyString, wxDefaultPosition, wxSize(100, 30));
        bsBottom->Add(btnShow, 0, wxALL, 2);
        bsBottom->Add(btnClose, 0, wxALL, 2);

        bsToft->Add(bsDir, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 2);
        bsToft->Add(bsProject, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 2);
        wxStaticLine* staticLine = new wxStaticLine(this);
        bsToft->Add(staticLine, 0, wxEXPAND | wxALL, 2);
        bsToft->Add(bsMiddle, 1, wxEXPAND | wxALL, 2);
        bsToft->AddSpacer(3);
        bsToft->Add(bsBottom, 0, wxALIGN_RIGHT | wxALL, 2);

        //SetMinSize(wxSize(250, 300));
        this->SetSizer(bsToft);
        GetSizer()->SetSizeHints(this);

        Center();
}

void OutputFileListDlg::SetOutputFileDir(wxString fileDir, wxString projName) {
        tcDir->ChangeValue(fileDir);
        tcProject->ChangeValue(projName);
        FilterFiles();
}


wxString OutputFileListDlg::GetOutputDir(void) {
        wxString dir = tcDir->GetValue();
#if (defined(__LINUX__) || defined(__MAC__))
        if(!StringUtil::EndsWith(dir, wxT("/"), true)) {
                dir += wxT("/");
        }
#else
        if(!StringUtil::EndsWith(dir, wxT("\\"), true)) {
                dir += wxT("\\");
        }
#endif
        return dir;
}

void OutputFileListDlg::FilterFiles(void) {
        unsigned i = 0, j = 0;
        bool isGv = true;
        wxArrayString fileLines;
        wxArrayString avaFileList;
        wxArrayString selectedGvFileList;
        wxArrayString selectedTextFileList;
        wxString filePrefix = wxEmptyString;
        wxString fileName = wxEmptyString;

        wxString fileDir = tcDir->GetValue();
        wxString projName = tcProject->GetValue();

        if(lbGvFileList->GetSelection() != wxNOT_FOUND) {
                lbGvFileList->Deselect(lbGvFileList->GetSelection());
        }
        lbGvFileList->Clear();
        if(lbTextFileList->GetSelection() != wxNOT_FOUND) {
                lbTextFileList->Deselect(lbTextFileList->GetSelection());
        }
        lbTextFileList->Clear();

        if (wxDir::Exists(fileDir)) {
                fileLines = FileUtil::LoadStringLineFile(MingUtil::GetSavedDbFile());
                wxDir::GetAllFiles(fileDir, &avaFileList, wxEmptyString, wxDIR_FILES);

                for (i = 0; i < fileLines.GetCount(); i++) {
                        if (fileLines[i].Cmp(wxT("gv")) == 0) {
                                isGv = true;
                                continue;
                        } else if (fileLines[i].Cmp(wxT("text")) == 0) {
                                isGv = false;
                                continue;
                        }
                        filePrefix = fileLines[i];
                        filePrefix.Replace(wxT("$Project"), projName);

                        for (j = 0; j < avaFileList.GetCount(); j++) {
                                fileName = wxFileName(avaFileList[j]).GetFullName();
                                // if empty, show all
                                if (StringUtil::StartsWith(fileName, filePrefix, true) || (projName.IsEmpty() && fileName.Find(filePrefix) != wxNOT_FOUND)) {
                                        if (isGv) {
                                                if (selectedGvFileList.Index(fileName) == wxNOT_FOUND)
                                                        selectedGvFileList.Add(fileName);
                                        } else {
                                                if (selectedTextFileList.Index(fileName) == wxNOT_FOUND)
                                                        selectedTextFileList.Add(fileName);
                                        }
                                }
                        }
                }

                lbGvFileList->Append(selectedGvFileList);
                lbTextFileList->Append(selectedTextFileList);
        }
}

void OutputFileListDlg::OnGvListDClick(wxCommandEvent& event) {
        OnGvListClick(event);
        DoShowOrbit();
}

void OutputFileListDlg::OnGvListClick(wxCommandEvent& event) {
        isGv = true;
        if (lbTextFileList->GetSelection() != wxNOT_FOUND) {
                lbTextFileList->Deselect(lbTextFileList->GetSelection());
        }
}

void OutputFileListDlg::DoShowOrbit(void) {
        wxString selectedFile = lbGvFileList->GetStringSelection();
        if (!selectedFile.IsEmpty()) {
                selectedFile = GetOutputDir() + selectedFile;
#if !(defined(__LINUX__) || defined(__MAC__))
                MingUtil::ExecGvWindows(selectedFile, tcGvPara->GetValue());
#else
                //ProcessOutputDlg gvProcessDlg(this, wxID_ANY);
                //gvProcessDlg.ExecGvMolcas(selectedFile, tcGvPara->GetValue());
#endif
        }
}

void OutputFileListDlg::OnTextListDClick(wxCommandEvent& event) {
        OnTextListClick(event);
        DoShowText();
}

void OutputFileListDlg::OnTextListClick(wxCommandEvent& event) {
        isGv = false;
        if (lbGvFileList->GetSelection() != wxNOT_FOUND) {
                lbGvFileList->Deselect(lbGvFileList->GetSelection());
        }

}

void OutputFileListDlg::OnShowButton(wxCommandEvent& event) {
        if (isGv)
                DoShowOrbit();
        else
                DoShowText();
}

void OutputFileListDlg::DoShowText(void) {
        if (lbTextFileList->GetSelection() == wxNOT_FOUND)
                return;
        wxString fileName = lbTextFileList->GetString(lbTextFileList->GetSelection());
        fileName = GetOutputDir() + fileName;
        FileEditDlg fileEditDlg(this, wxID_ANY);
        fileEditDlg.SetNeedSubmitBtn(false);
        fileEditDlg.SetNeedSaveBtn(true);
        fileEditDlg.CreateControls();
        fileEditDlg.ReadFile(fileName);

        fileEditDlg.ShowModal();
}

void OutputFileListDlg::OnDialogOk(wxCommandEvent& event) {
        HideDialog(wxID_OK);
}

void OutputFileListDlg::OnClose(wxCloseEvent& event) {
        HideDialog(wxID_OK);
}

void OutputFileListDlg::HideDialog(int returnCode) {
        if (IsModal()) {
                EndModal(returnCode);
        } else {
                SetReturnCode(returnCode);
                this->Show(false);
        }
        //Destroy();
}

void OutputFileListDlg::OnDirBtn(wxCommandEvent& event) {
        wxDirDialog dirDialog(this, wxEmptyString, tcDir->GetValue());
        if (dirDialog.ShowModal() == wxID_OK) {
                tcDir->SetValue(dirDialog.GetPath());
                FilterFiles();
        }
}

void OutputFileListDlg::OnTextCtrlProject(wxCommandEvent& event) {
        FilterFiles();
}
