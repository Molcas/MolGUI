/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "selectionsingleui.h"
#include "keywordconst.h"
#include "cstringutil.h"

SelectionSingleUI::SelectionSingleUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel){
        arrayPanel = NULL;
        CreateUIControls();
}

SelectionSingleUI::~SelectionSingleUI() {
    if(arrayPanel)
        delete[] arrayPanel;
}

SelectionModel* SelectionSingleUI::GetSelectionModel(void) const {
    return (SelectionModel*)keywordModel;
}

void SelectionSingleUI::CreateUIControls(void) {
    CreateToftSizer(wxVERTICAL, true);

         int i = 0;
    arrayPanel = new wxPanel*[GetSelectionModel()->GetSelectionCount()];
    for(i = 0; i < GetSelectionModel()->GetSelectionCount(); i++) {
                arrayPanel[i] = KeywordUI::CreateModelUI(this, GetSelectionModel()->GetSelectionKeywordModel(i));
                GetSizer()->Add(arrayPanel[i], 0, wxEXPAND | wxALL, 2);
        }

    GetSizer()->SetSizeHints(this);
    UpdateFromModel();
}

void SelectionSingleUI::UpdateFromModel(void) {
    KeywordUI::UpdateFromModel();

        int i;
        for(i = 0; i < GetSelectionModel()->GetSelectionCount(); i++) {
                ((KeywordUI*)arrayPanel[i])->UpdateFromModel();
        }
}

void SelectionSingleUI::UpdateToModel(bool isRestore) {
        int i;
        for(i = 0; i < GetSelectionModel()->GetSelectionCount(); i++) {
                ((KeywordUI*)arrayPanel[i])->UpdateToModel(isRestore);
        }
}

bool SelectionSingleUI::HasValue(void) const {
        int i;
    if(GetSelectionModel()->IsEnabled()) {
        for(i = 0; i < GetSelectionModel()->GetSelectionCount(); i++) {
                        if(((KeywordUI*)arrayPanel[i])->HasValue()) {
                                return true;
                        }
                }
    }
    return false;
}
