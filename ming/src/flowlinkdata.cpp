/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "flowchartmodel.h"
#include "flowlinkdata.h"
#include "cstringutil.h"

FlowLinkData::FlowLinkData(FlowChartModel* chartModel) {
    flowChartModel = chartModel;
        Init();
}

void FlowLinkData::Init(void) {
    maxNameLinkIndex = -1;
    nameLinkIndex = -1;
    //initial from 0
    valueLinkIndex = 0;
    for(int i = 0; i < MAX_FLOW_LINK_LEVEL; i++) {
        nameLinkTarget[i] = NULL;
        valueLinkUnusedIndex[i] = -1;
    }
}

FlowLinkData::~FlowLinkData() {
}


int FlowLinkData::GetMaxNameLinkIndex(void) const {
    return maxNameLinkIndex;
}

/**
 * All name links will be organized with a single level.
 */
int FlowLinkData::AllocNameLinkIndex(const char* targetName) {
    for(int i = 0; i < MAX_FLOW_LINK_LEVEL; i++) {
        if(nameLinkTarget[i] == NULL) {
            //recored the targetName
            nameLinkTarget[i] = (char *)targetName;
            break;
        }
        if(strcmp(targetName, nameLinkTarget[i]) == 0) {
            break;
        }
    }
    if(nameLinkIndex < 0) {
        //do not found this target name, alloc 0
        nameLinkIndex = 0;
    }else {
        nameLinkIndex++;
    }

    if(nameLinkIndex > maxNameLinkIndex) {
        //update the maximize name link level
        maxNameLinkIndex = nameLinkIndex;
    }
        return nameLinkIndex;
}

int FlowLinkData::DeallocNameLinkIndex(const char* targetName) {
    int index = -1;
    for(int i = 0; i < MAX_FLOW_LINK_LEVEL; i++) {
        if(nameLinkTarget[i] != NULL && strcmp(targetName, nameLinkTarget[i]) == 0) {
            index = nameLinkIndex--;
            break;
        }
    }
        return index;
}

int FlowLinkData::AllocValueLinkIndex(void) {
    int min = MAX_FLOW_LINK_LEVEL + 100;
    int index = -1;
    for(int i = 0; i < MAX_FLOW_LINK_LEVEL; i++) {
        //implement it as a priority stack, the smallest value always pop first
        if(valueLinkUnusedIndex[i] >= 0 && valueLinkUnusedIndex[i] < min) {
            min = valueLinkUnusedIndex[i];
            index = i;
        }
    }
    if(index != -1) {
        //As this index will be allocated, remove it from the stack
        valueLinkUnusedIndex[index] = -1;
    }else {
        min = valueLinkIndex++;
    }
    return min;
}

void FlowLinkData::DeallocValueLinkIndex(int index) {
    for(int i = 0; i < MAX_FLOW_LINK_LEVEL; i++) {
        if(valueLinkUnusedIndex[i] < 0) {
            //just put it back on a blank position
            valueLinkUnusedIndex[i] = index;
            break;
        }
    }
}

int FlowLinkData::FindValueLinkIndex(CommandProperty* endCmdProp, bool isAnchor, const char* name, const char* value) {
    ElementProperty* next = flowChartModel->GetElementPropertyHead();
    CommandProperty* cmdProp = NULL;
    XmlElement* elemXml = NULL;
    char* xmlName = NULL;
    const char* attrName = NULL;
    const char* attrValue = NULL;
    int linkIndex = -1;

    if(isAnchor) {
        attrName = ATTR_LINKANCHOR;
        attrValue = ATTR_LINKVALUEINDEX;
    }else {
        attrName = ATTR_NAME;
        attrValue = ATTR_LINKTARGET;
    }

    while(next != NULL && next != endCmdProp) {
        if(next->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
            cmdProp = (CommandProperty*)next;
            if((isAnchor && cmdProp->IsValueLinkCommandAnchor()) || (!isAnchor && cmdProp->IsValueLinkCommandTarget())) {
                elemXml = cmdProp->GetXmlElement();
                xmlName = GetAttributeString(elemXml, attrName);
                if(strcmp(xmlName, name) == 0) {
                    if(cmdProp->IsEqualIndexedPropValue(atoi(GetAttributeString(elemXml, attrValue)), value)) {
                        linkIndex = cmdProp->GetLinkIndex();
                        endCmdProp->SetLinkElemProp(next);
                        break;
                    }
                }
            }
        }
        next = next->GetNextElement();
    }

    return linkIndex;
}

void FlowLinkData::CalcLinkIndex(void) {
        ElementProperty* next = flowChartModel->GetElementPropertyHead();
        CommandProperty* cmdProp = NULL;
        XmlElement* elemXml = NULL;
        char* xmlName = NULL;
        char* xmlLinkValue = NULL;
        int linkIndex = -1;

    Init();
    while(next != NULL) {
        next->SetLinkElemProp(NULL);
        next = next->GetNextElement();
    }
    next = flowChartModel->GetElementPropertyHead();
        while(next != NULL) {
        linkIndex = -1;
                if(next->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
            cmdProp = (CommandProperty*)next;
            elemXml = cmdProp->GetXmlElement();
                        xmlName = GetAttributeString(elemXml, ATTR_NAME);
            if(cmdProp->IsNameLinkCommand()) {
                //if it is a name link command, apply for a new link index
                linkIndex = AllocNameLinkIndex(GetAttributeString(elemXml, ATTR_LINKANCHOR));
                        }else if(cmdProp->IsValueLinkCommandAnchor()) {
                xmlLinkValue = cmdProp->GetIndexedPropValue(atoi(GetAttributeString(elemXml, ATTR_LINKVALUEINDEX)));
                if(xmlLinkValue != NULL) {
                    linkIndex = FindValueLinkIndex(cmdProp, false, GetAttributeString(elemXml, ATTR_LINKANCHOR), xmlLinkValue);
                    free(xmlLinkValue);
                }
                if(linkIndex == -1) {
                    //not found its correspoing link pair before this command, apply for a new link index
                    linkIndex = AllocValueLinkIndex();
                }else {
                    //found its pair, return this index back for reuse.
                    DeallocValueLinkIndex(linkIndex);
                }
                        }else if(cmdProp->IsValueLinkCommandTarget()) {
                xmlLinkValue = cmdProp->GetIndexedPropValue(atoi(GetAttributeString(elemXml, ATTR_LINKTARGET)));
                if(xmlLinkValue != NULL) {
                                    linkIndex = FindValueLinkIndex(cmdProp, true, xmlName, xmlLinkValue);
                    free(xmlLinkValue);
                }
                if(linkIndex == -1) {
                    linkIndex = AllocValueLinkIndex();
                }else {
                    DeallocValueLinkIndex(linkIndex);
                }
                        }else {
                //may be a target of name link, give back it link index
                linkIndex = DeallocNameLinkIndex(xmlName);
                        }
        }
                next->SetLinkIndex(linkIndex);
                next = next->GetNextElement();
        }

}
