/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wx.h>
#include "stringutil.h"
#include <stdlib.h>
#include <string.h>

#include "groupmodel.h"
#include "inputparser.h"
#include "keywordconst.h"
#include "xmlparser.h"
#include "cstringutil.h"

GroupModel::GroupModel(ModuleProperty* parentModule, XmlElement* elem) : KeywordModel(parentModule, elem){
    // create a container for its subelements.
    SetChildModule(new ModuleProperty(elem, this, ELEM_PROP_TYPE_GROUP));
    selectedIndex = -1;
}

GroupModel::~GroupModel() {
}

int GroupModel::GetSubKeywordModelCount(void) const {
        return GetChildModule()->GetSubElementCount();
}

KeywordModel* GroupModel::GetSubKeywordModel(const char* name) const {
        return GetChildModule()->GetKeywordModel(name);
}

int GroupModel::GetSelectedIndex(void) const {
    return selectedIndex;
}

void GroupModel::SetSelectedIndex(int index) {
    if(index >= -1 && index < GetSubKeywordModelCount()) {
        selectedIndex = index;
    }
}

KeywordKind GroupModel::GetGroupKind(void) {
    char* kindChar = GetAttributeString(GetXmlElement(), ATTR_KIND);
    KeywordKind kind = KEYWORD_KIND_NULL;
    if(kindChar != NULL) {
        if(strcmp(KIND_BOX, kindChar) == 0) {
            kind = KEYWORD_KIND_BOX;
        }else if(strcmp(KIND_BLOCK, kindChar) == 0) {
            kind = KEYWORD_KIND_BLOCK;
        }else if(strcmp(KIND_RADIO, kindChar) == 0) {
            kind = KEYWORD_KIND_RADIO;
        }
    }

    return kind;
}

WindowKind GroupModel::GetWindowKind(void) {
    char* kindChar = GetAttributeValue(ATTR_WINDOW);
    WindowKind kind = WINDOW_KIND_POPUP;
    if(kindChar != NULL) {
        if(strcmp(ATTR_VALUE_INPLACE, kindChar) == 0) {
            kind = WINDOW_KIND_INPLACE;
        }else if(strcmp(ATTR_VALUE_TAB, kindChar) == 0) {
            kind = WINDOW_KIND_TAB;
        }
    }
    return kind;
}

bool GroupModel::SaveAsInputFormat(FILE* fp, bool isEnv) {
    KeywordKind kind = GetGroupKind();
    int subKeyCount = 0;
    int i = 0;
    KeywordModel* tmpModel = NULL;
    bool hasValue = false;

    subKeyCount = GetSubKeywordModelCount();
    if(KEYWORD_KIND_RADIO == kind) {
        //only output the selected keyword
        i = GetSelectedIndex();
        if(i >= 0 && i < subKeyCount) {
                        tmpModel = GetChildModule()->GetKeywordModel(i);
            if(tmpModel != NULL) {
                tmpModel->SaveAsInputFormat(fp);
            }
        }
    }else {
        if(KEYWORD_KIND_BLOCK == kind) {
            for(i = 0; i < subKeyCount; i++) {
                                tmpModel = GetChildModule()->GetKeywordModel(i);
                if(tmpModel != NULL && tmpModel->GetValue() != NULL) {
                    hasValue = true;
                    break;
                }
            }
           wxString value=StringUtil::CharPointer2String(GetName());
            if(hasValue&&!value.IsSameAs(wxT("SPIN/SYMMETRY")))
                fprintf(fp, "%s\n", GetName());
        }
        for(i = 0; i < subKeyCount; i++) {
                        tmpModel = GetChildModule()->GetKeywordModel(i);
            if(tmpModel != NULL) {
                tmpModel->SaveAsInputFormat(fp);
            }
        }
        if(KEYWORD_KIND_BLOCK == kind && hasValue) {
             wxString value=StringUtil::CharPointer2String(GetName());
             if(!value.IsSameAs(wxT("SPIN/SYMMETRY")))
                fprintf(fp, "*End of %s\n", GetName());
        }
    }
    return true;
}

void GroupModel::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
    if(GetGroupKind() == KEYWORD_KIND_BLOCK) {
        // the start block
        buf[0] = 0;
                GetChildModule()->ReadFromInputFormat(fp, buf, bufSize, availableXYZFile);
    }
}

void GroupModel::DuplicateValue(KeywordModel* const oldKeywordModel) {
    int oldSelectedIndex = ((GroupModel*)oldKeywordModel)->GetSelectedIndex();
    KeywordModel::DuplicateValue(oldKeywordModel);
    SetSelectedIndex(oldSelectedIndex);
}
