/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/filename.h>

#include "mxyzfile.h"
#include "keyworddata.h"
#include "mingpnl.h"
#include "fileutil.h"
#include "stringutil.h"
#include "tools.h"
#include "envutil.h"
#include "mingutil.h"
#include "listboxui.h"
#include "basissetdlg.h"
#include "listitemseldlg.h"

#include "cstringutil.h"
#include "keywordconst.h"
#include  "manager.h"

#define CTRL_MOD_BTN_ID CTRL_WIN_CTRL_CUSTOM_START + 1

BEGIN_EVENT_TABLE(ListBoxUI, KeywordUI)
        EVT_BUTTON(CTRL_WIN_CTRL_ID, ListBoxUI::OnModifyButton)
END_EVENT_TABLE()

ListBoxUI::ListBoxUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel) {
        uiType = UI_TYPE_NULL;
        CreateUIControls();
}

ListBoxModel* ListBoxUI::GetListBoxModel(void) {
        return (ListBoxModel*)keywordModel;
}

void ListBoxUI::CreateUIControls(void) {
        CreateToftSizer(wxVERTICAL, true);
        wxSize size(300, 150);

        if (strcmp(GetListBoxModel()->GetName(), ATTR_VALUE_BASIS) == 0 || strcmp(GetListBoxModel()->GetName(), ATTR_VALUE_BASISXYZ) == 0) {
                uiType = UI_TYPE_BASIS_SET;
        } else if (strcmp(GetListBoxModel()->GetName(), ATTR_VALUE_GROUP) == 0) {
                uiType = UI_TYPE_GROUP;
                size = wxSize(300, 30);
        }

        listBox = new wxListBox(this, wxID_ANY, wxDefaultPosition, size);

        windowCtrl = new wxButton(this, CTRL_WIN_CTRL_ID, wxT("Modify"), wxDefaultPosition, wxSize(80, 30));
        wxBoxSizer* bottom = new wxBoxSizer(wxHORIZONTAL);
        bottom->Add(windowCtrl, 0, wxALL, 2);

        GetSizer()->Add(listBox, 1, wxEXPAND | wxALL, 2); //wxALIGN_CENTER | 
        GetSizer()->Add(bottom, 0, wxALIGN_RIGHT | wxALL, 2);

        //
        GetSizer()->SetSizeHints(this);
        UpdateFromModel();
}

void ListBoxUI::OnModifyButton(wxCommandEvent& event) {

        switch (uiType) {
        case UI_TYPE_BASIS_SET:
                ShowBasisSetDlg();
                break;
        case UI_TYPE_GROUP:
                ShowGroupDlg();
                break;
        default:
                break;
        }
}

char** ListBoxUI::ConvertXYZ2SymInput(int* elemCount) {
        char* fileFullName = NULL;
        char** eleSymbols = NULL;
        int count = -1;
        wxString fileFullNameWX = wxEmptyString;
        wxString objFilePathWX = wxEmptyString;
        char* objFilePath = NULL;
        double thre = 0.1;
        char* fullPath = NULL;

        *elemCount = 0;
        KeywordModel* coordModel = keywordModel->GetParentModule()->GetKeywordModel(ATTR_VALUE_COORD);
        KeywordModel* threModel = keywordModel->GetParentModule()->GetKeywordModel(ATTR_VALUE_SYMMETRY_THRESHOLD);
        if (coordModel && coordModel->GetValue()) {
                fileFullName = coordModel->GetValue();
                fileFullNameWX = StringUtil::CharPointer2String(fileFullName);
                if(FileUtil::IsRelativePath(fileFullNameWX)) {
                        fileFullNameWX = MingPnl::Get()->GetFilePath() + fileFullNameWX;
                        StringUtil::String2CharPointer(&fullPath, fileFullNameWX);
                }
                if (!wxFileName::FileExists(fileFullNameWX)) {
                        Tools::ShowError(wxString::Format(wxT("No such file exists: ") + fileFullNameWX));
                } else {
                        //objFilePathWX = wxFileName(fileFullNameWX).GetFullName();
                        objFilePathWX = MingUtil::GetWorkTmpDir() + wxT("findsym.inp");
                        // as "findsym.out" only read findsym.inp as its inputfile
                        //objFilePathWX = FileUtil::GetPathPrefix() + wxT("findsym.inp");

                        StringUtil::String2CharPointer(&objFilePath, objFilePathWX);
                        if (threModel && threModel->GetValue()) {
                                thre = atof(threModel->GetValue());
                        }
                        if(fullPath) {
                                count = MXYZFile::ConvertXYZ2SymmetryInput(fullPath, objFilePath, thre, &eleSymbols);
                                free(fullPath);
                        }else {
                                count = MXYZFile::ConvertXYZ2SymmetryInput(fileFullName, objFilePath, thre, &eleSymbols);
                        }
                        *elemCount = count;
                        if (objFilePath)
                                free(objFilePath);
                }
        } else {
                Tools::ShowInfo(wxT("Please select an XYZ file first."));
        }
        return eleSymbols;
}

void ListBoxUI::ShowBasisSetDlg(void) {
        char** eleSymbols = NULL;
        int count = -1;

        eleSymbols = ConvertXYZ2SymInput(&count);
        // wxMessageBox("is it filled 0?");
        if (eleSymbols != NULL) {
//begin : hurukun : 23/11/2009
                BasisSetDlg basisSetDlg(MingPnl::Get()->GetActiveDlg(),wxID_ANY);
//end   : hurukun : 23/11/2009
                basisSetDlg.SetElemSymbols(eleSymbols, count); //fill the list of elements inside
                // std::cout << "elesym " << eleSymbols[1] << " count " << count << std::endl;
                basisSetDlg.SetSelectedBasisSet(listBox->GetStrings()); // fill the list in basis set editor in selected basis set
                // std::cout << "listbox " << listBox->GetCount() << std::endl;
                // basisSetDlg.set
                if (basisSetDlg.ShowModal() == wxID_OK) {
                        listBox->Set(basisSetDlg.GetSelectedBasisSet()); //fill the listbox of basisset outside once you selected
                        // wxMessageBox("is it filled -4?");
                }
                FreeStringArray(eleSymbols, count);
        }
        // wxMessageBox("is it filled?");
}

wxString ListBoxUI::GenerateFindSymOut(void) {
        wxString        workDir=MingUtil::GetWorkTmpDir();
/*        wxString        fileInput=workDir+wxT("findsym.inp");
        wxString        fileOutput=workDir+wxT("findsym.out");
        wxString        fileError=workDir+wxT("findsym.err");*/
        wxString        fileInput=wxGetCwd()+platform::PathSep()+wxT("findsym.inp"),
                        fileAutoOutput=wxGetCwd()+platform::PathSep()+wxT("findsym.out");
        wxString        fileOutput=workDir+wxT("findsym.out");
        wxString        fileError=workDir+wxT("findsym.err");

//        wxString        oldDir=wxGetCwd();
//        wxSetWorkingDirectory(workDir);
//wxMessageBox(workDir)        ;
        SimuProject* pProject=Manager::Get()->GetProjectManager()->GetActiveProject();
        if(pProject==NULL)
                return wxEmptyString;
        wxString        dataFile=pProject->GetFileName();
        dataFile=FileUtil::ChangeFileExt(dataFile,wxT(".xyz"));
//wxMessageBox(dataFile);
        wxCopyFile(dataFile,fileInput);

        wxString        gvFile=EnvUtil::GetMolcasGVDir();
        if(gvFile.IsEmpty()){
                 wxMessageBox(wxT("Please install gv and modify the settings at first."));
//                wxSetWorkingDirectory(oldDir);
                return wxEmptyString;
        }

    wxString execProcName = gvFile+wxT(" --findsym");
//wxMessageBox(wxGetCwd());
//wxMessageBox(fileOutput);
//wxMessageBox(execProcName);
    if(Tools::ExecModule(execProcName, fileOutput, fileError)==false)
                 fileOutput=wxEmptyString;

    wxCopyFile(fileAutoOutput,fileOutput);
//        wxSetWorkingDirectory(oldDir);
//wxMessageBox(wxGetCwd());
        return                 fileOutput;


/*        char** eleSymbols = NULL;
        int count = -1;

        wxString fileFullNameWX = wxEmptyString;
        wxString fileInputWX = MingUtil::GetWorkTmpDir() + wxT("findsym.inp");

        eleSymbols = ConvertXYZ2SymInput(&count);
        if (eleSymbols != NULL) {
                FreeStringArray(eleSymbols, count);
        } else {
                return wxEmptyString;
        }
        if (!wxFileName::FileExists(fileInputWX)) {
                wxLogError(wxT("No such file exists: ") + fileInputWX);
                return wxEmptyString;
        } else {
                double thre = 0.1;
                KeywordModel* threModel = keywordModel->GetParentModule()->GetKeywordModel(ATTR_VALUE_SYMMETRY_THRESHOLD);
                if (threModel && threModel->GetValue()) {
                        thre = atof(threModel->GetValue());
                }
//begin : hurukun : 27/11/2009
//
//                wxString        pname(wxT("/findsym.out"));
//
//end   : hurukun : 27/11/2009
                //fileFullNameWX = FileUtil::GetWorkTmpDir() + wxT("findsym.out");
                fileFullNameWX = EnvUtil::GetWorkDir() +pname;// wxT("/findsym.exe");
                if(!wxFileExists(fileFullNameWX)) {
                        fileFullNameWX = EnvUtil::GetBaseDir() +pname;// wxT("/findsym.exe");
                }
                MingUtil::GenerateSymmetryWorkFile(fileInputWX, fileFullNameWX, thre);

                if (!wxFileName::FileExists(fileFullNameWX)) {
                        wxLogError(wxT("No such file exists: ") + fileFullNameWX);
                        return wxEmptyString;
                }
        }
        return fileFullNameWX;*/
}

void ListBoxUI::ShowGroupDlg() {
        int totalCount = 0;
        char** symGenerators = NULL;
        char* fileFullName = NULL;
        wxArrayString arrayItems;

        wxString fileFullNameWX = GenerateFindSymOut();
        if (!fileFullNameWX.IsEmpty()) {
                StringUtil::String2CharPointer(&fileFullName, fileFullNameWX);
                symGenerators = MXYZFile::GetSymmetryGenerator(fileFullName, &totalCount);
                if (symGenerators) {
                        arrayItems.Clear();
                        for (int i = 0; i < totalCount; i++) {
                                arrayItems.Add(StringUtil::CharPointer2String(symGenerators[i]));
                        }
                        FreeStringArray(symGenerators, totalCount);
//begin : hurukun : 23/11/2009
                        ListItemSelDlg itemDlg(MingPnl::Get()->GetActiveDlg());
//end   : hurukun : 23/11/2009
                        itemDlg.SetSelectableItems(arrayItems);
                        itemDlg.SetSelectedItems(listBox->GetStrings());
                        itemDlg.SetSelectableItemCount(1);
                        if (itemDlg.ShowModal() == wxID_OK) {
                                listBox->Set(itemDlg.GetSelectedItems());
                        }
                }
                if (fileFullName) free(fileFullName);
        }
}

wxString ListBoxUI::AddSymmetryGeneratorName(const char* value) {
        int totalCount = 0;
        int colIndex = 0;
        char** symGenerators = NULL;
        char* fileFullName = NULL;
        char* nonBlank = NULL;
        wxString addedName = wxEmptyString;

        wxString fileFullNameWX = GenerateFindSymOut();
        if (!fileFullNameWX.IsEmpty()) {
                StringUtil::String2CharPointer(&fileFullName, fileFullNameWX);
                symGenerators = MXYZFile::GetSymmetryGenerator(fileFullName, &totalCount);
                if (symGenerators) {
                        for (int i = 0; i < totalCount; i++) {
                                colIndex  = SubStringIndex(symGenerators[i], ":", 0);
                                if (colIndex >= 0) {
                                        nonBlank = &symGenerators[i][colIndex+1];
                                } else {
                                        nonBlank = symGenerators[i];
                                }
                                nonBlank = StringLeftTrim(nonBlank, NULL);
                                if (IsStringEqualNoCase(value, nonBlank, strlen(value))) {
                                        addedName = StringUtil::CharPointer2String(symGenerators[i]);
                                        break;
                                }
                        }
                        FreeStringArray(symGenerators, totalCount);
                }
                if (fileFullName) free(fileFullName);
        }
        if (addedName.IsEmpty()) {
                addedName = StringUtil::CharPointer2String(value);
        }
        return addedName;
}

void ListBoxUI::UpdateFromModel(void) {
        char* charValue = GetListBoxModel()->GetValue();
        char** charValues = NULL;
        int count = 0, i = 0;
        wxArrayString strArray;

        KeywordUI::UpdateFromModel();
        if (charValue == NULL) {
                listBox->Clear();
        } else {
                charValues = StringTokenizer(charValue, VALUE_DELIM, &count, 0);
                for (i = 0; i < count; i++) {
                        if (GetListBoxModel()->IsSymmetryGenerator()) {
                                SetUniqueAtomCount(charValues[i]);
                                strArray.Add(AddSymmetryGeneratorName(charValues[i]));
                        } else {
                                strArray.Add(StringUtil::CharPointer2String(charValues[i]));
                        }
                }
                listBox->Set(strArray);
        }
}

void ListBoxUI::UpdateToModel(bool isRestore) {
        unsigned i;
        wxString strValue = wxEmptyString;
        char* charValue = NULL;
        if (isRestore) {
                //keywordModel->SetSelected(origValue, false);
        } else {
                wxArrayString selected = listBox->GetStrings();
                for (i = 0; i < selected.GetCount(); i++) {
                        strValue += selected[i];
                        if (i < selected.GetCount() - 1) {
                                strValue += StringUtil::CharPointer2String(VALUE_DELIM);
                        }
                }
//wxMessageBox(strValue);
                StringUtil::String2CharPointer(&charValue, strValue);
                GetListBoxModel()->SetValue(charValue);
                if (GetListBoxModel()->IsSymmetryGenerator()) {
                        SetUniqueAtomCount(charValue);
                }
                if (charValue)
                        free(charValue);
        }
}

void ListBoxUI::SetUniqueAtomCount(const char* symgen) {
        wxString fileFullNameWX = GenerateFindSymOut();
        char* fileFullName = NULL;
        int uniqueAtom = 0;
        if (!fileFullNameWX.IsEmpty()) {
                StringUtil::String2CharPointer(&fileFullName, fileFullNameWX);
                uniqueAtom = MXYZFile::GetUniqueAtomCount(fileFullName, symgen);
                //FileUtil::ShowInfoMessage(wxString::Format("%d", uniqueAtom));
                GetKeywordData()->SetUniqueAtoms(uniqueAtom);
        }
}

bool ListBoxUI::HasValue(void) const {
        return !listBox->IsEmpty();
}
