/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "elementproperty.h"
#include "moduleproperty.h"
#include "commandproperty.h"
#include "keywordconst.h"
#include "cstringutil.h"

static int currInternalId = 0;

ElementProperty::ElementProperty(XmlElement* elem) {
    Init(elem);
}

ElementProperty::ElementProperty(XmlElement* elem, KeywordModel* parentKeywordModel) {
    Init(elem);
    this->parentKeywordModel = parentKeywordModel;
}

void ElementProperty::Init(XmlElement* elem) {
        nextElemProp = NULL;

    subElemCount = 0;
    xmlElement = elem;

    propValue = NULL;
        keywordModels = NULL;
        needShowSubelements = false;
        subXmlElements = NULL;
    parentKeywordModel = NULL;
    linkElemProp = NULL;
        internalId = currInternalId++;
        linkIndex = -1;
        isUserChecked = false;
}

ElementProperty::~ElementProperty() {
    if(subXmlElements)
        free(subXmlElements);
}

KeywordModel* ElementProperty::GetParentKeywordModel(void) const {
    return parentKeywordModel;
}

int ElementProperty::GetLinkIndex(void) const {
        return linkIndex;
}

void ElementProperty::SetLinkIndex(int index) {
        linkIndex = index;
}


/**
 * The name.
 */
char* ElementProperty::GetName(void) const{
    return GetAttributeString(xmlElement, ATTR_NAME);
}

/**
 * The appear string.
 */
char* ElementProperty::GetAppear(void) const{
    char* appear = GetAttributeString(xmlElement, ATTR_APPEAR);
    if(appear == NULL)
        return GetName();
    return appear;
}

void ElementProperty::SetPropValue(const char* newValue) {
    // free memory allocated before
    if(this->propValue != NULL) {
        free(this->propValue);
    }
    if(newValue != NULL) {
        StringCopy(&this->propValue, newValue, strlen(newValue));
    }else {
        this->propValue = NULL;
    }
}

char* ElementProperty::GetPropValue(void) const {
    return this->propValue;
}

ElementProperty* ElementProperty::GetNextElement(void) const {
        return nextElemProp;
}

void ElementProperty::SetNextElement(ElementProperty* elem) {
        nextElemProp = elem;
}

ElementProperty* ElementProperty::GetLinkElemProp(void) const {
    return linkElemProp;
}

void ElementProperty::SetLinkElemProp(ElementProperty* elemProp) {
    linkElemProp = elemProp;
}

/**
 * Add the given elementproperty to the end of the list.
 * It is used to build flowchart as a flowchart is a sequential connected elements.
 * @return: the position of the given elementproperty in the list.
 */
int ElementProperty::AddElement(ElementProperty* prop, int elemId) {
        int pos = 0;
        ElementProperty* next = this;
        do {
                pos++;
                //wxMessageDialog dlg(NULL, wxString::Format(wxT("%d"), next->GetInternalId()));
                //dlg.ShowModal();
                if(next->GetInternalId() == elemId || next->GetNextElement() == NULL) {
                        prop->SetNextElement(next->GetNextElement());
                        next->SetNextElement(prop);
                        break;
                }
                next = next->GetNextElement();
        }while(next != NULL);

        return pos;
}

int ElementProperty::GetSubElementCount() const {
    return subElemCount;
}

XmlElement* ElementProperty::GetSubElement(int index) const {
    if(index >= 0 && index < GetSubElementCount() && subXmlElements != NULL) {
        return subXmlElements[index];
    }
    return NULL;
}

XmlElement* ElementProperty::GetXmlElement(void) const {
    return xmlElement;
}

/**
 * Create approriate of subclass of ElementProperty with the type of given xmlelement.
 * Factory method.
 */
ElementProperty* ElementProperty::CreateElementProperty(XmlElement* elem) {
        ElementProperty* elemProp = NULL;
        if(elem != NULL) {
                if(strcmp(elem->name, ELEM_COMMAND) == 0) {
                        elemProp = new CommandProperty(elem);
                }else if(strcmp(elem->name, ELEM_MODULE) == 0) {
                        elemProp = new ModuleProperty(elem);
                }
        }
        return elemProp;
}

int ElementProperty::GetInternalId(void) const {
    return internalId;
}

bool ElementProperty::IsUserChecked(void) const {
    return isUserChecked;
}

void ElementProperty::SetUserChecked(bool checked) {
    isUserChecked = checked;
}
