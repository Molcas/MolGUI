/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/statline.h>
#include <wx/notebook.h>

#include "moduleviewerdlg.h"
#include "stringutil.h"
#include "tools.h"
#include "mingutil.h"
#include "keywordui.h"
#include "mingpnl.h"
#include "keywordmodel.h"
#include "groupmodel.h"
#include "inputparser.h"
#include "keywordconst.h"

#define CTRL_ID_CKB_VIEW_ADVANCED 100
#define CTRL_ID_CKB_VIEW_EMPTY 101
#define CTRL_ID_BTN_SAVE_GLOBAL_ENV 102

BEGIN_EVENT_TABLE(ModuleViewerDlg, wxDialog)
    EVT_BUTTON(wxID_OK, ModuleViewerDlg::OnDialogOK)
    EVT_BUTTON(wxID_CANCEL, ModuleViewerDlg::OnDialogCancel)
    EVT_BUTTON(wxID_SAVE, ModuleViewerDlg::OnSaveGlobalEnv)
    EVT_BUTTON(CTRL_ID_BTN_SAVE_GLOBAL_ENV, ModuleViewerDlg::OnSaveGlobalEnv)
    EVT_CHECKBOX(CTRL_ID_CKB_VIEW_ADVANCED, ModuleViewerDlg::OnCheckbox)
    EVT_CHECKBOX(CTRL_ID_CKB_VIEW_EMPTY, ModuleViewerDlg::OnCheckbox)
    EVT_CLOSE(ModuleViewerDlg::OnClose)
END_EVENT_TABLE()

ModuleViewerDlg::ModuleViewerDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos,
                            const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style)
 {
    pTopNotebook = NULL;
    pPnlCommon = NULL;
    moduleProperty = NULL;
    keywordUIArray = NULL;
    ckbAdvanced = NULL;
    ckbEmpty = NULL;
#ifdef __MAC__
    labelHidden = NULL;
#endif

}

ModuleViewerDlg::~ModuleViewerDlg() {
    if(keywordUIArray) {
        delete[] keywordUIArray;
    }
}

void ModuleViewerDlg::SetModuleProperty(ModuleProperty* prop, bool isEnvModule, bool isGlobal) {
    int i = 0;
    KeywordModel* keywordModel = NULL;
        if(prop == NULL || prop->GetPropType() != ELEM_PROP_TYPE_MODULE) return;
    moduleProperty = prop;
    moduleProperty->UpdateRelation();
    if(isEnvModule) {
                if(isGlobal) {
                    SetTitle(wxT("Global Environment Setting"));
                }else {
                        SetTitle(wxT("Project Environment Setting"));
                }
        }else {
                SetTitle(StringUtil::CharPointer2String(moduleProperty->GetName()) + wxT(" - ") + GetTitle());
        }
#ifdef __MAC__
        SetSize(850, 400);
#else
        SetSize(800, 400);
#endif
        CreateControls(isEnvModule, isGlobal);

        keywordUIArray = new KeywordUI*[moduleProperty->GetSubElementCount()];
    for(i = 0; i < moduleProperty->GetSubElementCount(); i++) {
        keywordModel = moduleProperty->GetKeywordModel(i);
        if(keywordModel != NULL) {
                        if(keywordModel->GetKind() == KEYWORD_KIND_GROUP && ((GroupModel*)keywordModel)->GetWindowKind() == WINDOW_KIND_TAB ) {
                keywordUIArray[i] = KeywordUI::CreateModelUI(pTopNotebook, keywordModel);
            }else {
                keywordUIArray[i] = KeywordUI::CreateModelUI(pPnlCommon, keywordModel);
            }
        }else {
            keywordUIArray[i] = NULL;
        }
    }

    ckbAdvanced->SetValue(MingPnl::Get()->IsShowAdvanced());
        ckbEmpty->SetValue(MingPnl::Get()->IsShowEmpty());
        DoArrangePanel();
        GetSizer()->Fit(this);
        // GetSizer()->SetSizeHints(this);
        Center();
//        wxMessageBox(wxT("Set Module property"));
}

void ModuleViewerDlg::CreateControls(bool isEnvModule, bool isGlobal) {
    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);

    pTopNotebook = new wxNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER);

        pPnlCommon = new wxScrolledWindow(pTopNotebook, wxID_ANY, wxDefaultPosition, wxSize(400, 250), wxVSCROLL);
        pPnlCommon->SetScrollbars(30,30,100,100);

    wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    ckbAdvanced = new wxCheckBox(this, CTRL_ID_CKB_VIEW_ADVANCED, wxT("Advanced"), wxDefaultPosition, wxSize(150, 30));
    ckbEmpty = new wxCheckBox(this, CTRL_ID_CKB_VIEW_EMPTY, wxT("Show empty"), wxDefaultPosition, wxSize(150, 30));
#ifdef __MAC__
    labelHidden = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100, wxDefaultSize.GetHeight()));
    labelHidden->SetForegroundColour(*wxRED);
#endif
    bsBottom->AddSpacer(5);
    bsBottom->Add(ckbAdvanced, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 4);
    bsBottom->Add(ckbEmpty, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 4);
#ifdef __MAC__
    bsBottom->Add(labelHidden, 0, wxALIGN_CENTER_VERTICAL, 4);
#endif
    bsBottom->AddStretchSpacer(1);
        if(isEnvModule && !isGlobal) {
                wxButton * btnSaveGlobal = new wxButton(this, CTRL_ID_BTN_SAVE_GLOBAL_ENV, wxT("Save As Global"), wxDefaultPosition, wxSize(150,30));
                bsBottom->Add(btnSaveGlobal, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 4);
        }
        if(isGlobal) {
                wxButton * btnSave = new wxButton(this, wxID_SAVE, wxEmptyString, wxDefaultPosition, wxSize(100,30));
                bsBottom->Add(btnSave, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 4);
        }else {
                wxButton * btnOk = new wxButton(this, wxID_OK, wxEmptyString, wxDefaultPosition, wxSize(100,30));
                bsBottom->Add(btnOk, 0, wxALL, 2);
        }

    wxButton * btnCancel = new wxButton(this, wxID_CANCEL, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    bsBottom->Add(btnCancel, 0, wxALL, 2);

    bsTop->Add(pTopNotebook, 1, wxEXPAND|wxALL, 2);
    bsTop->AddSpacer(3);
    bsTop->Add(bsBottom, 0, wxEXPAND | wxALL, 2);

    this->SetSizer(bsTop);
}

void ModuleViewerDlg::DoArrangePanel(void) {

    int pageCount = 0;
    int i = 0;

    char* level = NULL;
    int rowPerc = 0;
    int rowCount = 0;

        bool isShowAdvanced = ckbAdvanced->GetValue();
        bool isShowEmpty = ckbEmpty->GetValue();

    //remove all pages
    pageCount = pTopNotebook->GetPageCount();
    for(i = pageCount - 1; i >= 0; i--) {
        pTopNotebook->RemovePage(i);
    }

    wxBoxSizer* bsColumn = new wxBoxSizer(wxVERTICAL);
    pPnlCommon->SetSizer(bsColumn);
    pPnlCommon->SetMinSize(wxSize(800, 450));

        bool isShowKeyword = false;
    for(i = 0; i < moduleProperty->GetSubElementCount(); i++) {
        if(keywordUIArray[i] == NULL) continue;
                isShowKeyword = false;

                level = GetAttributeString(keywordUIArray[i]->GetKeywordModel()->GetXmlElement(), ATTR_LEVEL);
        if(level == NULL || strcmp(level, LEVEL_BASIC) == 0 || isShowAdvanced) {
                        if(isShowEmpty || keywordUIArray[i]->HasValue()) {
                                isShowKeyword = true;
                        }
                }
                keywordUIArray[i]->Show(isShowKeyword);
                if(isShowKeyword) {
            if(keywordUIArray[i]->GetKeywordModel()->GetKind() == KEYWORD_KIND_GROUP && ((GroupModel*)keywordUIArray[i]->GetKeywordModel())->GetWindowKind() == WINDOW_KIND_TAB ) {
                pTopNotebook->AddPage(keywordUIArray[i], keywordUIArray[i]->GetAppearString());
                if(keywordUIArray[i]->GetKeywordModel()->GetHelp() != NULL)
                        keywordUIArray[i]->SetToolTip(StringUtil::CharPointer2String(keywordUIArray[i]->GetKeywordModel()->GetHelp()));
            }else {
                rowCount = keywordUIArray[i]->GetRowSize();
                                   rowPerc = (rowCount > 1) ? rowCount : 0;
                                   bsColumn->Add(keywordUIArray[i], rowPerc, wxEXPAND | wxALL, 2);
                                   //wxLogError(wxString::Format("%d", keywordUIArray[i]->GetSize().GetWidth()));
            }
        }
    }
        pPnlCommon->GetSizer()->Fit(pPnlCommon);
        //begin : hurukun : 19/11/2009
#ifdef __MAC__
        SetMaxSize(wxSize(850, 750));
#else
        SetMaxSize(wxSize(800, 750));
#endif
        SetMinSize(wxSize(800, 450));
        pTopNotebook->AddPage(pPnlCommon, StringUtil::CharPointer2String(moduleProperty->GetName()), true);
        //end   : hurukun : 19/11/2009
        pPnlCommon->Update();
        //
        ShowHiddenWarning();
}

void ModuleViewerDlg::ShowHiddenWarning(void) {
    int i = 0;
    bool needShowHiddenText = false;
        if(!ckbAdvanced->IsChecked()) {
        for(i = 0; i < moduleProperty->GetSubElementCount(); i++) {
            if(keywordUIArray[i] == NULL || keywordUIArray[i]->IsShown())
                continue;
            if(keywordUIArray[i]->HasValue()) {
                needShowHiddenText = true;
                break;
            }
        }
    }
    if(needShowHiddenText) {
#ifdef __MAC__
                labelHidden->SetLabel(wxT("Hidden Input!"));
#else
                ckbAdvanced->SetForegroundColour(*wxRED);
                ckbAdvanced->SetToolTip(wxT("Hidden Input!"));
#endif
    }else {
#ifdef __MAC__
        labelHidden->SetLabel(wxEmptyString);
#else
        ckbAdvanced->SetForegroundColour(*wxBLACK);
        ckbAdvanced->SetToolTip(wxEmptyString);
#endif
    }
}

void ModuleViewerDlg::OnCheckbox(wxCommandEvent& event) {
    DoArrangePanel();
    // GetSizer()->SetSizeHints(this);
//begin : hurukun : 23/11/2009
   GetSizer()->Fit(this);
//end   : hurukun : 23/11/2009
    Center();
}

void ModuleViewerDlg::OnDialogOK(wxCommandEvent& event) {
//wxMessageBox(wxT("ModuleViewerDlg::OnDialogOK--235"));
    int i;
    for(i = 0; i < moduleProperty->GetSubElementCount(); i ++) {
        if(keywordUIArray[i] != NULL) {
            if(!keywordUIArray[i]->InputValidate()) {
                                if(!keywordUIArray[i]->GetAppearString().IsSameAs(wxT("Basis set"))){
                        Tools::ShowWarning(wxT("The input of ") + keywordUIArray[i]->GetAppearString() + wxT(" is required!"));
                        return;
                                }
            }
        }
    }
    if(!moduleProperty->IsUserChecked()) {
        moduleProperty->SetUserChecked(true);
    }
//wxMessageBox(wxT("ModuleViewerDlg::OnDialogOK--249"));
    MingPnl::Get()->DoSaveHistoryFile();
//wxMessageBox(wxT("ModuleViewerDlg::OnDialogOK--251"));
    UpdateToModel(false);
//wxMessageBox(wxT("ModuleViewerDlg::OnDialogOK--253"));
    HideDialog(wxID_OK);
//wxMessageBox(wxT("ModuleViewerDlg::OnDialogOK--255"));
}

void ModuleViewerDlg::OnDialogCancel(wxCommandEvent& event) {
        UpdateToModel(true);
    HideDialog(wxID_CANCEL);
}

void ModuleViewerDlg::OnClose(wxCloseEvent& event){
        UpdateToModel(true);
    HideDialog(wxID_CANCEL);
}

void ModuleViewerDlg::UpdateToModel(bool isRestore) {
    int i;
    for(i = 0; i < moduleProperty->GetSubElementCount(); i ++) {
        if(keywordUIArray[i] != NULL) {
            keywordUIArray[i]->UpdateToModel(isRestore);
        }
    }
}

void ModuleViewerDlg::HideDialog(int returnCode) {
    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
        //Destroy();
}

void ModuleViewerDlg::OnSaveGlobalEnv(wxCommandEvent& event) {
        char* fileName = NULL;
        UpdateToModel(false);
        StringUtil::String2CharPointer(&fileName, MingUtil::GetGlobalEnvFile());
        if(InputParser::SaveAsGlobalEnvModule(moduleProperty, fileName)) {
                Tools::ShowInfo(wxT("Successfully saved!"));
        }else {
                Tools::ShowError(wxT("Failed to be saved!"));
        }
        if(fileName) free(fileName);
        if(event.GetId() == wxID_SAVE) {
                HideDialog(wxID_OK);
        }
}
