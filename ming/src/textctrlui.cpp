/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "selectionui.h"
#include "textctrlui.h"
#include "stringutil.h"

#include "cstringutil.h"
#include <float.h>


BEGIN_EVENT_TABLE(TextCtrlUI, KeywordUI)
        EVT_TEXT(CTRL_WIN_CTRL_ID, TextCtrlUI::OnText)
END_EVENT_TABLE()

TextCtrlUI::TextCtrlUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel){
    origValue = NULL;
    CreateUIControls();
    isRealtimeUpdate = false;
}

TextCtrlUI::~TextCtrlUI() {
    if(origValue) {
        free(origValue);
    }
}

wxTextCtrl* TextCtrlUI::GetTextCtrl(void) const {
    return (wxTextCtrl*)windowCtrl;
}

TextCtrlModel* TextCtrlUI::GetTextCtrlModel(void) const{
    return (TextCtrlModel*)keywordModel;
}

void TextCtrlUI::CreateUIControls(void) {
    long style = 0;
    char* value =GetTextCtrlModel()->GetDefaultValue();
    wxString labelText =GetAppearString();

    if(value != NULL) {
       labelText += wxT(" (")+StringUtil::CharPointer2String(value)+ wxT(")");
   }


    CreateToftSizer(wxHORIZONTAL, false);
    FormatTextCtrlType formatType = FORMAT_TEXT_CTRL_NULL;

    if(GetTextCtrlModel()->GetKind() == KEYWORD_KIND_STRINGS) {
                char* sizeChar = GetTextCtrlModel()->GetAttributeValue(ATTR_SIZE);
            if(sizeChar != NULL) {
                        formatType = FORMAT_TEXT_CTRL_STRINGS;
                }
                style = wxTE_MULTILINE;
                wxStaticBox *sbGrid = new wxStaticBox(this, wxID_ANY, labelText);
                wxSizer *sbsGrid = new wxStaticBoxSizer(sbGrid, wxVERTICAL);
                SetSizer(sbsGrid);
         }else {
                if(GetTextCtrlModel()->GetKind() == KEYWORD_KIND_INT) {
                        formatType = FORMAT_TEXT_CTRL_INT;
                }else if(GetTextCtrlModel()->GetKind() == KEYWORD_KIND_REAL) {
                        formatType = FORMAT_TEXT_CTRL_REAL;
                }
                wxStaticText* label = new wxStaticText(this, wxID_ANY, labelText);
            //this will dynamically adjust the label size
            label->SetLabel(labelText);
            if(label->GetSize().GetWidth() < UI_LABEL_WIDTH) {
            label->SetMinSize(wxSize(UI_LABEL_WIDTH, wxDefaultSize.GetHeight()));
        }
            label->SetToolTip(StringUtil::CharPointer2String(keywordModel->GetHelp()));
            GetSizer()->Add(label, 0, wxALIGN_CENTER | wxALL, 2);
        }
        wxSize size(400, 200);
        if(!GetTextCtrlModel()->IsComment()) {
        if(KEYWORD_KIND_INT == GetTextCtrlModel()->GetKind()){
            size = wxSize(UI_INT_WIDTH, UI_HEIGHT_UNIT);
        }else if(KEYWORD_KIND_REAL == GetTextCtrlModel()->GetKind()) {
            size = wxSize(UI_REAL_WIDTH, UI_HEIGHT_UNIT);
        }else{
            size = wxSize(GetColumnSize() * UI_WIDTH_UNIT, GetRowSize() * UI_HEIGHT_UNIT);
        }
    }

        if(formatType == FORMAT_TEXT_CTRL_NULL) {
                windowCtrl = new wxTextCtrl(this, CTRL_WIN_CTRL_ID, wxEmptyString, wxDefaultPosition, size, style);//, wxTextValidator(wxFILTER_NUMERIC));
        }else {
            windowCtrl = new FormatTextCtrl(this, CTRL_WIN_CTRL_ID, wxEmptyString, wxDefaultPosition, size, style);
            if(formatType == FORMAT_TEXT_CTRL_STRINGS) {
                        ((FormatTextCtrl*)windowCtrl)->SetMaxLineCount(GetTextCtrlModel()->GetAttributeValue(ATTR_SIZE));
                }else {
                    ((FormatTextCtrl*)windowCtrl)->SetFormatType(formatType, GetTextCtrlModel()->GetMinValue(), GetTextCtrlModel()->GetMaxValue());
                }
        }

        if(KEYWORD_KIND_STRING == GetTextCtrlModel()->GetKind() || KEYWORD_KIND_STRINGS == GetTextCtrlModel()->GetKind() || KEYWORD_KIND_CUSTOM == GetTextCtrlModel()->GetKind()) {
        GetSizer()->Add(windowCtrl, 1, wxEXPAND | wxALL, 2);
    }else {
            // std::cout << "Custom here?" << keywordModel->GetName() << std::endl;
        GetSizer()->Add(windowCtrl, 0, wxALIGN_CENTER | wxALL, 2);
    }

    //
    if(GetTextCtrlModel()->GetAttributeValue(ATTR_REALTIMEUPDATE) != NULL) {
        isRealtimeUpdate = true;
    }else if(strcmp(GetTextCtrlModel()->GetName(), ATTR_VALUE_SYMMETRY_THRESHOLD) == 0) {
        // for assurance, this value should be realtimeupdated
        // as it will be used by basis set and findsym
        isRealtimeUpdate = true;
    }
    //
    GetSizer()->SetSizeHints(this);
        //windowCtrl->SetMinSize(wxSize(10, windowCtrl->GetSize().GetHeight()));
 UpdateFromModel();
}

void TextCtrlUI::UpdateFromModel(void) {
    KeywordUI::UpdateFromModel();
    wxString strValue = wxEmptyString;
    char* charValue =NULL;
   #if defined (__LINUX__)    //chaochao added molcasrc
      strValue=Getdefaultvalue_text();
   #endif
    charValue = GetTextCtrlModel()->GetValue();

    if(charValue == NULL)
    {

    //charValue = GetTextCtrlModel()->GetDefaultValue();

    }
  else {
        StringCopy(&origValue, charValue, strlen(charValue));
        strValue = StringUtil::CharPointer2String(charValue);
   }
        GetTextCtrl()->SetValue(strValue);
}

void TextCtrlUI::UpdateToModel(bool isRestore) {
    wxString inputValue = wxEmptyString;
    if(isRestore) {
        if(isRealtimeUpdate) {
            GetTextCtrlModel()->SetValue(origValue);
        }
    }else{
        char* value = NULL;
        if(HasValue()) {
            inputValue = GetTextCtrl()->GetValue();
            StringUtil::String2CharPointer(&value, inputValue);
        }
        GetTextCtrlModel()->SetValue(value);
        if(value != NULL)
            free(value);
    }
}

void TextCtrlUI::OnText(wxCommandEvent& event) {
        if(isRealtimeUpdate) {
                char* charValue = NULL;
                StringUtil::String2CharPointer(&charValue, GetTextCtrl()->GetValue());
                GetTextCtrlModel()->SetValue(charValue);
                if(charValue != NULL)
                        free(charValue);
        }
}

bool TextCtrlUI::HasValue(void) const {
        wxString value =GetTextCtrl()->GetValue();
    if(GetTextCtrlModel()->IsEnabled() && !StringUtil::IsEmptyString(value)) {
        return true;
    }
    return false;
}

BEGIN_EVENT_TABLE(FormatTextCtrl, wxTextCtrl)
        EVT_CHAR(FormatTextCtrl::OnInputChar)
        EVT_KILL_FOCUS(FormatTextCtrl::OnKillFocus)
END_EVENT_TABLE()

FormatTextCtrl::FormatTextCtrl(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos,
                const wxSize& size,        long style, const wxValidator& validator, const wxString& name) : wxTextCtrl(parent, id, value, pos, size, style, validator, name){
        m_formatType = FORMAT_TEXT_CTRL_NULL;
        m_minValue = -DBL_MAX;
        m_maxValue = DBL_MAX;
}

void FormatTextCtrl::SetFormatType(FormatTextCtrlType type, const char* charMinValue, const char* charMaxValue) {
        m_formatType = type;
        wxString minValueWX = wxEmptyString;
        wxString maxValueWX = wxEmptyString;
        if(charMinValue != NULL) {
                minValueWX = StringUtil::CharPointer2String(charMinValue);
        }
        if(charMaxValue != NULL) {
                maxValueWX = StringUtil::CharPointer2String(charMaxValue);
        }

        if(!minValueWX.IsEmpty() && !minValueWX.ToDouble(&m_minValue)) {
                m_minValue = -DBL_MAX;
        }
        if(!maxValueWX.IsEmpty() && !maxValueWX.ToDouble(&m_maxValue)) {
                m_maxValue = DBL_MAX;
        }
}

void FormatTextCtrl::SetFormatType(FormatTextCtrlType type, double minValue, double maxValue) {
        m_formatType = type;
        m_minValue = minValue;
        m_maxValue = maxValue;
}

void FormatTextCtrl::SetMaxLineCount(const char* lineCount) {
        m_formatType = FORMAT_TEXT_CTRL_STRINGS;
        wxString lineCountWX = wxEmptyString;
        if(lineCount != NULL) {
                lineCountWX = StringUtil::CharPointer2String(lineCount);
        }
        if(!lineCountWX.ToDouble(&m_maxValue)) {
                m_maxValue = DBL_MAX;
        }
}

void FormatTextCtrl::OnInputChar(wxKeyEvent& event) {
        bool isValid = false;
        if(m_formatType == FORMAT_TEXT_CTRL_INT || m_formatType == FORMAT_TEXT_CTRL_REAL) {
                int keyCode = event.GetKeyCode();

                if(keyCode == WXK_HOME || keyCode == WXK_END || keyCode == WXK_LEFT || keyCode == WXK_RIGHT
                        || keyCode == WXK_BACK || keyCode == WXK_TAB || keyCode == WXK_RETURN || keyCode == WXK_DELETE) {
                        isValid = true;
                }else if (keyCode == '=') {
                }else if(keyCode == '.') {
                        if(m_formatType == FORMAT_TEXT_CTRL_INT) {
                        }else if(m_formatType == FORMAT_TEXT_CTRL_REAL) {
                                isValid = GetValue().Find(wxChar('.')) == wxNOT_FOUND;
                        }
                }else if((keyCode == 'e' || keyCode == 'E' || keyCode == 'd' || keyCode == 'D') && m_formatType == FORMAT_TEXT_CTRL_REAL) {
                        isValid = GetValue().Find(wxChar('D')) == wxNOT_FOUND &&
                                GetValue().Find(wxChar('d')) == wxNOT_FOUND &&
                                GetValue().Find(wxChar('E')) == wxNOT_FOUND &&
                                GetValue().Find(wxChar('e')) == wxNOT_FOUND &&
                                GetInsertionPoint() > 0;
                }else if(wxIsalpha(keyCode)) {
                }else if(wxIsdigit(keyCode) || keyCode == '+' || keyCode == '-') {
                        wxString value = GetValue();
                        long insertPos = GetInsertionPoint();
                        if (keyCode == '+' || keyCode == '-') {
                                isValid = (insertPos == 0l) ;
                                if(!isValid && !value.IsEmpty()) {
                                        isValid = value.Last() == wxChar('E') || value.Last() == wxChar('e') || value.Last() == wxChar('D') || value.Last() == wxChar('d');
                                        // 'd' or 'D' is double in Fortran
                                }
                        }
                        if(!isValid) {
                                value = value.Mid(0, insertPos) + wxChar(keyCode) + value.Mid(insertPos);
                                if(m_formatType == FORMAT_TEXT_CTRL_INT) {
                                        long longValue = 0;
                                        if(value.ToLong(&longValue) && longValue <= m_maxValue && longValue >= m_minValue) {
                                                isValid = true;
                                        }
                                }else {
                                        double doubleValue = 0.0;
                                        value.Replace(wxT("d"), wxT("e"));
                                        value.Replace(wxT("D"), wxT("e"));
                                        //wxMessageBox(wxT("value = ") + value);
                                        if(value.ToDouble(&doubleValue) && doubleValue <= m_maxValue && doubleValue >= m_minValue) {
                                                isValid = true;
                                        }
                                        //wxMessageBox(wxString::Format(wxT("aa=%lf"), doubleValue));
                                }
                        }
                }
        }else if(m_formatType == FORMAT_TEXT_CTRL_STRINGS) {
                int keyCode = event.GetKeyCode();
                if(keyCode == WXK_RETURN) {
                        if(GetNumberOfLines() < (int)m_maxValue) {
                                isValid = true;
                        }
                }else {
                        isValid = true;
                }
        }else {
                isValid = true;
        }
        if(!isValid) {
                wxBell();
                SetBackgroundColour(*wxRED);
        }else {
                SetBackgroundColour(*wxWHITE);
        }
        event.Skip(isValid);
}

void FormatTextCtrl::OnKillFocus(wxFocusEvent& event) {
        bool isValid = false;
        if(m_formatType == FORMAT_TEXT_CTRL_REAL){
                wxString value = GetValue();
                value = value.Trim();
                value = value.Trim(false);
                if(!value.IsEmpty()) {
                        double doubleValue = 0.0;
                        value.Replace(wxT("d"), wxT("e"));
                        value.Replace(wxT("D"), wxT("e"));
                        if(value.ToDouble(&doubleValue) && doubleValue <= m_maxValue && doubleValue >= m_minValue) {
                                isValid = true;
                        }
                }else {
                        isValid = true;
                }
                if(!isValid) {
                        wxBell();
                        SetBackgroundColour(*wxRED);
                }else {
                        SetBackgroundColour(*wxWHITE);
                }
        }
        event.Skip(isValid);
}


#if defined (__LINUX__)    //added by chaochao molcasrc

wxString TextCtrlUI::Getdefaultvalue_text(void)
{  wxArrayString env_mol;
   env_mol=molcasrc_var();
   if(strcmp(GetTextCtrlModel()->GetName(),ATTR_VALUE_ENV_MOLCASMEM)==0)
   return env_mol[7];
   return wxEmptyString;
}
#endif
