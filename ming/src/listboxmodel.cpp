/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <string.h>

#include "listboxmodel.h"
#include "inputparser.h"
#include "cstringutil.h"
#include "keyworddata.h"

ListBoxModel::ListBoxModel(ModuleProperty* parentModule, XmlElement* elem) : KeywordModel(parentModule, elem){
}

bool ListBoxModel::IsSymmetryGenerator(void) {
    return strcmp(parentModule->GetName(), ATTR_VALUE_GATEWAY) == 0 && strcmp(GetName(), ATTR_VALUE_GROUP) == 0;
}

void ListBoxModel::SetValue(const char* newValue) {
    int symNum = 1;
    int count = 0;
    char** tokens = NULL;
    char* tempValue = (char *)newValue;

    KeywordModel::SetValue(newValue);
    //tempValue = "C2v : yz z";
    if(newValue != NULL) {
        //if(strcmp(parentModule->GetName(), ATTR_VALUE_GATEWAY) == 0 && strcmp(GetName(), ATTR_VALUE_GROUP) == 0 ) {
        if(IsSymmetryGenerator()) {
            //group symmetry
            if(strcmp(tempValue, ATTR_VALUE_GROUP_FULL) == 0) {
                symNum = 8;
            }else if(strcmp(tempValue, ATTR_VALUE_GROUP_NOSYM) == 0) {
                symNum = 1;
            }else {
                tokens = StringTokenizer(tempValue, " ", &count, 0);
                if(tokens) {
                    symNum = 0;
                    for(int i = 0; i < count; i++) {
                        if(IsCharEqualNoCase(tokens[i][0], 'x')
                            ||IsCharEqualNoCase(tokens[i][0], 'y') ||IsCharEqualNoCase(tokens[i][0], 'z')) {
                                symNum++;
                        }
                    }
                    // pow of 2
                    if(symNum == 0) {
                        symNum = 1;
                    }else if(symNum == 1) {
                        symNum = 2;
                    }else if(symNum == 2) {
                        symNum = 4;
                    }else if(symNum == 3) {
                        symNum = 8;
                    }else {
                        symNum = 1;
                    }
                    FreeStringArray(tokens, count);
                }
            }
            GetKeywordData()->SetSymmetryNumber(symNum);
        }
    }
}

bool ListBoxModel::SaveAsInputFormat(FILE* fp, bool isEnv) {
    char** charValues = NULL;
    char tempBuf[1024];
    char* nonBlankBuf = NULL;
    int count = 0, i = 0;
    int colonIndex = -1;
    if(GetValue() != NULL) {
        charValues = StringTokenizer(GetValue(), VALUE_DELIM, &count, 0);
        if(charValues != NULL) {
            fprintf(fp, "%s\n", GetName());
            for(i = 0; i < count; i++) {
                colonIndex = SubStringIndex(charValues[i], ":", 0);
                if(colonIndex >= 0) {
                    tempBuf[0] = 0;
                    strcpy(tempBuf, charValues[i]);
                    nonBlankBuf = tempBuf;
                    nonBlankBuf = &nonBlankBuf[colonIndex+1];
                    nonBlankBuf = StringLeftTrim(nonBlankBuf, NULL);
                    fprintf(fp, " %s", nonBlankBuf);
                }else {
                    fprintf(fp, " %s", charValues[i]);
                }
                if( i < count - 1) {
                    fprintf(fp, ",");
                }else {
                    fprintf(fp, "\n");
                }
            }
            FreeStringArray(charValues, count);
        }
    }
    return true;
}

void ListBoxModel::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
    char* nonBlankBuf = NULL;
    char** charValues = NULL;
    int count = 0, i = 0;
    char dataValue[2048];
    dataValue[0] = 0;

    while(!feof(fp)) {
        if(strlen(buf) <= 0) {
            if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
        }
        if(InputParser::IsEndOfInput(buf)) break;
        nonBlankBuf = StringTrim(buf, NULL);

        charValues = StringTokenizer(nonBlankBuf, ",", &count, 0);
        if(charValues != NULL) {
            for(i = 0; i < count; i++) {
                strcat(dataValue, charValues[i]);
                if( i < count - 1) {
                    strcat(dataValue, VALUE_DELIM);
                }
            }
            FreeStringArray(charValues, count);
        }

        SetValue(dataValue);
        buf[0] = 0;
        break;
    }
}
