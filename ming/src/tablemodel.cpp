/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "tablemodel.h"
#include "keyworddata.h"
#include "inputparser.h"

#include "keywordconst.h"
#include "xmlparser.h"
#include "cstringutil.h"

TableModel::TableModel(ModuleProperty* parentModule, XmlElement* elem) : KeywordModel(parentModule, elem){
    //computedNumber = 0;
    rowCount = 1;
    columnCount = GetElementAttrSize();
    //FileUtil::ShowErrorMessage(wxString::Format("%s =%s %d", GetName(), sizeChar, columnCount));
        if(IsComputedArray() && !IsComputedArrayWithSizeOne()) {
                rowCount = 3;
        }
}

int TableModel::GetElementAttrSize(void) const  {
    int size = 1;
    char* sizeChar = GetAttributeString(xmlElement, ATTR_SIZE);

    if(sizeChar != NULL) {
        if(strcmp(sizeChar, ATTR_VALUE_NSYM) == 0) {
            size = GetKeywordData()->GetSymmetryNumber();
        }else if(strcmp(sizeChar, ATTR_VALUE_UNIATOMS) == 0) {
            size = GetKeywordData()->GetUniqueAtoms();
        }else if(strcmp(sizeChar, ATTR_VALUE_DEGFREEDOM) == 0) {
            size = 3*GetKeywordData()->GetUniqueAtoms();
        }else {
            size = atoi(sizeChar);
        }
    }
    //FileUtil::ShowInfoMessage(wxString::Format("tablecol %d", size));
    return size;
}

int TableModel::GetColumnCount(void) const {
    if(GetKind() == KEYWORD_KIND_INTS_LOOKUP || GetKind() == KEYWORD_KIND_REALS_LOOKUP || GetKind() == KEYWORD_KIND_STRINGS_LOOKUP) {
        return GetElementAttrSize();
    }
    return columnCount;
}

void TableModel::SetRowCount(int count) {
        rowCount = count;
}

int TableModel::GetRowCount(void) const {
        return rowCount;
}

/*int TableModel::GetComputedNumber(void) const {
    return computedNumber;
}

void TableModel::SetComputedNumber(int newNumber) {
    computedNumber = newNumber;
}*/

bool TableModel::IsComputedArray(void) const {
        return (GetKind() == KEYWORD_KIND_INTS_COMPUTED || GetKind() == KEYWORD_KIND_REALS_COMPUTED || GetKind() == KEYWORD_KIND_STRINGS_COMPUTED);
}

bool TableModel::IsComputedArrayWithSizeOne(void) const{
        return IsComputedArray() && GetElementAttrSize() == 1;
}

bool TableModel::SaveAsInputFormat(FILE* fp, bool isEnv) {
    char* charValue = GetValue();
    char** charValues = NULL;
    char** lineValues = NULL;
    int lineCount = 0, valueCount, i = 0, j = 0;

    if(charValue != NULL) {
        fprintf(fp, "%s\n", GetName());
        lineValues = StringTokenizer(charValue, LINE_DELIM, &lineCount, 1);
        if(IsComputedArray()) {
            fprintf(fp, "%d\n", lineCount);
        }
        for(i = 0; i < lineCount; i++) {
            charValues = StringTokenizer(lineValues[i], VALUE_DELIM, &valueCount, 1);
            for(j = 0; j < valueCount; j++) {
                if(j == 0) {
                    fprintf(fp, " ");
                }
                fprintf(fp, "%s", charValues[j]);
                if( j < valueCount - 1) {
                    fprintf(fp, " ");
                }else {
                    fprintf(fp, "\n");
                }
            }
            if(charValues)
                free(charValues);
        }
        if(lineValues)
            free(lineValues);
    }
    return true;
}

void TableModel::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
    int colSize = 0;
    char* nonBlankChar = NULL;
    char token[512];
    char linBuf[2048];
    int i = 0;
    int len = 0;
    int rowCount = 1;
    int currRow = 0;
    token[0] = 0;
    linBuf[0] = 0;

    //if(!GetAttributeInt(&colSize, xmlElement, ATTR_SIZE)) {
    //    colSize = 0;
    //}
    colSize = GetElementAttrSize();

    while(IsComputedArray() && !feof(fp)) {
        if(strlen(buf) <= 0) {
            if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
            if(InputParser::IsEndOfInput(buf)) break;
        }
        nonBlankChar = StringTrim(buf, NULL);
        sscanf(nonBlankChar, "%d", &rowCount);
                //sometimes the number of rows may be followed by values in the same line
                i = 0;
                while(i < (int)strlen(nonBlankChar)) {
                        if(nonBlankChar[i] < '0' || nonBlankChar[i] > '9') {
                                break;
                        }
                        i++;
                }
                if(i < (int)strlen(nonBlankChar)) {
                        //remove the number of rows
                        strcpy(buf, nonBlankChar + i);
                        nonBlankChar = StringTrim(buf, NULL);
                }else {
                        buf[0] = 0;
                }
        break;
    }
    while(!feof(fp)) {
        if(currRow >= rowCount) break;
        if(strlen(buf) <= 0) {
            if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
            if(InputParser::IsEndOfInput(buf)) break;
                        nonBlankChar = StringTrim(buf, NULL);
        }
        //FileUtil::ShowInfoMessage(wxString::Format("tablemodel %s", nonBlankChar));
        for( i = 0; i < colSize; i++) {
            sscanf(nonBlankChar, "%s", token);
            len = strlen(token);
            //FileUtil::ShowInfoMessage(wxString::Format("===%d %s", len, token));
            if(len > 0) {
                strcat(linBuf, token);
                token[0] = 0;
                if(i < colSize - 1) {
                    strcat(linBuf, VALUE_DELIM);
                }
                nonBlankChar = &nonBlankChar[len + 1];
            }else {
                break;
            }
        }
                if(strlen(nonBlankChar) <= 0) {
                buf[0] = 0;
                }
        currRow++;
        if(currRow < rowCount) {
            strcat(linBuf, LINE_DELIM);
        }
        //FileUtil::ShowInfoMessage(wxString::Format("tablemodel %s", linBuf));
    }
    buf[0] = 0;
    SetValue(linBuf);

}
