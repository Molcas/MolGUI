/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/statline.h>
#include "textctrlui.h"
#include "keywordmodel.h"
#include "selectionui.h"
#include "keywordconst.h"
#include "cstringutil.h"
#include "stringutil.h"
#include "tools.h"
#include "mingpnl.h"


#define CTRL_DIR_BTN_ID CTRL_WIN_CTRL_CUSTOM_START + 1


BEGIN_EVENT_TABLE(SelectionUI, KeywordUI)
        EVT_COMBOBOX(CTRL_WIN_CTRL_ID, SelectionUI::OnComboBox)
        EVT_BUTTON(CTRL_DIR_BTN_ID, SelectionUI::OnDirBtn)
END_EVENT_TABLE()

SelectionUI::SelectionUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel) {
        int i = 0;
        KeywordModel* tmpModel = NULL;
        bool isReadOnly = true;
        bool isShowDirBtn = false;
        arraySelection.Clear();
        arraySelection.Add(wxEmptyString);
        arrayPanel = NULL;

        origSelectedValue = NULL;
        origSelectedIndex = -1;

        if (GetSelectionModel()->GetKind() == KEYWORD_KIND_SELECT &&
                        GetSelectionModel()->GetChildModule()->IsNeedShowSubelements()) {
                arrayPanel = new wxPanel*[GetSelectionModel()->GetSelectionCount() + 1];
                arrayPanel[0] = KeywordUI::CreateModelUI(this, NULL);
                arrayPanel[0]->SetMinSize(wxSize(500, 60));
        }
        for (i = 0; i < GetSelectionModel()->GetSelectionCount(); i++) {
                if (strcmp(ATTR_VALUE_USERINPUT, GetSelectionModel()->GetSelectionValues(false)[i]) == 0) {
                        isReadOnly = false;
                        continue;
                }else if(strcmp(ATTR_VALUE_USERDIR, GetSelectionModel()->GetSelectionValues(false)[i]) == 0) {
                        isShowDirBtn = true;
                        isReadOnly = false;        // if use dir btn, it should be editable
                        continue;
                }
         arraySelection.Add(StringUtil::CharPointer2String(GetSelectionModel()->GetSelectionValues(true)[i]));

                if (arrayPanel != NULL) {
                        tmpModel = GetSelectionModel()->GetSelectionKeywordModel(i);
                        if (!GetSelectionModel()->IsNeedShowKeywordModel(tmpModel)) {
                                tmpModel = NULL;
                        }
                        arrayPanel[i+1] = KeywordUI::CreateModelUI(this, tmpModel);
                        arrayPanel[i+1]->Show(false);
                        arrayPanel[i+1]->SetMinSize(wxSize(500, 60));
                }
        }

 CreateUIControls(isReadOnly, isShowDirBtn);
}

SelectionUI::~SelectionUI() {
        if (arrayPanel)
                delete[] arrayPanel;
        if (origSelectedValue != NULL) {
                free(origSelectedValue);
        }
}

wxComboBox* SelectionUI::GetComboBox(void) const {
        return (wxComboBox*)windowCtrl;
}

SelectionModel* SelectionUI::GetSelectionModel(void) const {
        return (SelectionModel*)keywordModel;
}

void SelectionUI::CreateUIControls(bool isReadOnly, bool isNeedDirBtn) {
        char* value =GetSelectionModel()->GetDefaultValue();
        wxString labelText =GetAppearString();
        if(value != NULL) {
                labelText += wxT(" (")+StringUtil::CharPointer2String(value)+ wxT(")");
        }

        CreateToftSizer(wxVERTICAL, false);

        wxBoxSizer* bsTopLine = new wxBoxSizer(wxHORIZONTAL);

        wxString strDefaultValue=wxEmptyString;
#if defined (__LINUX__)     //chaochao added
         strDefaultValue= Getdefaultvalue_sel();
#endif
//wxMessageBox(strDefaultValue);
        wxStaticText* label = new wxStaticText(this, wxID_ANY, GetAppearString());
        //this will dynamically adjust the label size
        //label->SetLabel(GetAppearString());
        label->SetLabel(labelText);

        if (label->GetSize().GetWidth() < UI_LABEL_WIDTH) {
                label->SetMinSize(wxSize(UI_LABEL_WIDTH, wxDefaultSize.GetHeight()));
        }
        label->SetToolTip(StringUtil::CharPointer2String(keywordModel->GetHelp()));
        if(isReadOnly) {
                windowCtrl = new wxComboBox(this, CTRL_WIN_CTRL_ID, strDefaultValue, wxDefaultPosition, wxDefaultSize, arraySelection, wxCB_READONLY);

    }else {
                windowCtrl = new wxComboBox(this, CTRL_WIN_CTRL_ID, strDefaultValue, wxDefaultPosition, wxDefaultSize, arraySelection);
    }

        bsTopLine->Add(label, 0, wxALIGN_CENTER | wxALL, 2);
        bsTopLine->Add(windowCtrl, 1, wxALIGN_CENTER_VERTICAL | wxALL, 2);
        if(isNeedDirBtn) {
                wxButton* dirBtn = new wxButton(this, CTRL_DIR_BTN_ID, wxT("..."), wxDefaultPosition, wxSize(30, wxDefaultSize.GetHeight()));
                bsTopLine->Add(dirBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
        }

        GetSizer()->Add(bsTopLine, 0, wxEXPAND | wxALL, 2);

        if (arrayPanel && arrayPanel[0]) {
                GetSizer()->Add(arrayPanel[0], 0, wxEXPAND | wxALL, 2);
                arrayPanel[0]->Show(true);
        }
         GetSizer()->SetSizeHints(this);
        UpdateFromModel();

}

void SelectionUI::UpdateFromModel(void) {
        KeywordUI::UpdateFromModel();
        wxString strValue=wxEmptyString;
#if defined (__LINUX__)   //chaochao added
         strValue= Getdefaultvalue_sel();
#endif
//wxMessageBox(strValue);
        if(GetSelectionModel()->GetOmittedSelectionValueCount() > 0) {
            char* charValue = GetSelectionModel()->GetValue();
            if(charValue != NULL){
                StringCopy(&origSelectedValue, charValue, strlen(charValue));
                strValue =StringUtil::CharPointer2String(charValue);
        }else{
                        strValue=wxEmptyString;
                }

       GetComboBox()->SetValue(strValue); //very important
//wxMessageBox(strValue);
        }else {
                origSelectedIndex = GetSelectionModel()->GetSelectedIndex() + 1;
                //wxLogError(GetSelectionModel()->GetName() + wxString::Format(" %d", origSelectedIndex));
                if (origSelectedIndex > 0) {
                        GetComboBox()->SetSelection(origSelectedIndex);
            if (arrayPanel) {
                                OnSelected();
                                if (arrayPanel[origSelectedIndex]) {
                                        ((KeywordUI*)arrayPanel[origSelectedIndex])->UpdateFromModel();
                                }
                        }
            else if (origSelectedIndex == 0)
                           GetComboBox()->SetValue(strValue);//chaochao added
                }
    }

}

void SelectionUI::UpdateToModel(bool isRestore) {
        if(GetSelectionModel()->GetOmittedSelectionValueCount() > 0) {
                if (isRestore) {

                        GetSelectionModel()->SetValue(origSelectedValue);
                } else {
                        char* value = NULL;
                        wxString inputValue = wxEmptyString;
            inputValue = GetComboBox()->GetValue();
            StringUtil::String2CharPointer(&value, inputValue);
                GetSelectionModel()->SetValue(value);
                if(value != NULL)
                           free(value);
                }
        }else {
                if (isRestore) {
                        GetSelectionModel()->SetSelectedIndex(origSelectedIndex - 1);
                } else {
                        int selIndex = -1;
                        if (GetSelectionModel()->IsEnabled()) {
                                if (arrayPanel == NULL || GetComboBox()->GetCurrentSelection() > 0) {
                                        //FileUtil::String2CharPointer(&charValue, GetComboBox()->GetValue());
                                        selIndex = GetComboBox()->GetCurrentSelection() - 1;
                                }
                        }
                         GetSelectionModel()->SetSelectedIndex(selIndex);
                }
                if (arrayPanel && arrayPanel[GetSelectionModel()->GetSelectedIndex() + 1]) {
                        ((KeywordUI*)arrayPanel[GetSelectionModel()->GetSelectedIndex() + 1])->UpdateToModel(isRestore);
                }
        }
}

void SelectionUI::OnComboBox(wxCommandEvent& event) {
        int index = GetComboBox()->GetCurrentSelection() - 1;
        GetSelectionModel()->SetSelectedIndex(index);
        if (arrayPanel) {
                GetSelectionModel()->NotifySelection();
        }
}

void SelectionUI::OnSelected(void) {
        if (arrayPanel == NULL) return;
        int i;
        int index = GetSelectionModel()->GetSelectedIndex() + 1;
        int count = GetSelectionModel()->GetSelectionCount() + 1;

        if (index < 0 || index >= count) return;
//begin : hurukun : 26/11/2009 --and a pair of ()
        if (arrayPanel == NULL || (arrayPanel[index] != NULL && arrayPanel[index]->IsShown())) {
//end   : hurukun : 26/11/2009
                return;
        }

        for (i = 0; i < count; i++) {
                if (arrayPanel[i] && arrayPanel[i]->IsShown()) {
                        arrayPanel[i]->Show(false);
                        GetSizer()->Detach(arrayPanel[i]);
                }
        }
        if (arrayPanel[index]) {
                GetSizer()->Add(arrayPanel[index], 0, wxEXPAND | wxALL, 2);
                arrayPanel[index]->Show(true);
        }
        GetSizer()->Layout();
        //GetParent()->GetSizer()->Layout();
        //GetParent()->Refresh();
        //GetParent()->Update();
}

bool SelectionUI::HasValue(void) const {
        if (GetSelectionModel()->IsEnabled() && GetComboBox()->GetCurrentSelection() > 0) {
                return true;
        }
        return false;
}

void SelectionUI::OnDirBtn(wxCommandEvent& event) {
//begin : hurukun : 23/11/2009
        wxDirDialog dirDialog(MingPnl::Get()->GetActiveDlg(), wxT("Directory Selection"), wxEmptyString);
//end   : hurukun : 23/11/2009
        if (dirDialog.ShowModal() == wxID_OK) {
                GetComboBox()->SetValue(dirDialog.GetPath());
        }
}
#if defined (__LINUX__)    //added by chaochao molcasrc
wxString SelectionUI::Getdefaultvalue_sel(void)
{

     wxArrayString env_mol;
     env_mol=molcasrc_var();
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_WORKDIR)==0)
             return env_mol[1];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_NEW_WORKDIR)==0)
             return env_mol[2];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_KEEP_FILES)==0)
             return env_mol[3];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_PROJECT)==0)
             return env_mol[4];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_OUTPUTDIR)==0)
             return env_mol[5];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_SAVE)==0)
             return env_mol[6];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_MOLCASDISK)==0)
             return env_mol[8];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_TRAP)==0)
             return env_mol[12];
     if(strcmp(GetSelectionModel()->GetName(),ATTR_VALUE_ENV_PRINT)==0)
             return env_mol[13];

     return wxEmptyString;

 }

#endif
