/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "checkboxui.h"
#include "groupui.h"
#include "stringutil.h"

BEGIN_EVENT_TABLE(CheckboxUI, KeywordUI)
        EVT_CHECKBOX(CTRL_WIN_CTRL_ID, CheckboxUI::OnCheckbox)
END_EVENT_TABLE()

CheckboxUI::CheckboxUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel){
    groupIndex = -1;
    origValue = false;
    CreateUIControls();
}

wxCheckBox* CheckboxUI::GetCheckBox(void) const {
    return (wxCheckBox*)windowCtrl;
}

void CheckboxUI::SetGroupIndex(int index) {
    groupIndex = index;
}

void CheckboxUI::CreateUIControls(void) {
    CreateToftSizer(wxHORIZONTAL, false);
    if(keywordModel->IsGroupRadio()) {
        wxStaticText* label = new wxStaticText(this, wxID_ANY, GetAppearString());
            label->SetLabel(GetAppearString());
            label->SetToolTip(StringUtil::CharPointer2String(keywordModel->GetHelp()));
            GetSizer()->Add(label, 0, wxALIGN_CENTER | wxALL, 2);
    }else {
        windowCtrl = new wxCheckBox(this, CTRL_WIN_CTRL_ID, GetAppearString(), wxDefaultPosition, wxDefaultSize);
        GetSizer()->Add(windowCtrl, 1, wxALIGN_CENTER | wxALL, 2);
    }
    GetSizer()->SetSizeHints(this);
    UpdateFromModel();
}

void CheckboxUI::OnCheckbox(wxCommandEvent& event) {
    DoUIEvent(GetCheckBox()->IsChecked());
}

void CheckboxUI::DoUIEvent(bool checked) {
    keywordModel->SetSelected(checked, false);
    keywordModel->NotifySelection();
}

void CheckboxUI::OnSelected(void) {
    if(GetCheckBox()) {
        GetCheckBox()->SetValue(keywordModel->IsSelected());
    }else if(keywordModel->IsGroupRadio()) {
                //the ancestor should be like this
                //if the panel structure has been modified in GroupUI
                //here should also be modified consequently.
                ((GroupUI*)(GetParent()->GetParent()))->SetRadioButton(groupIndex, keywordModel->IsSelected());
    }
}


void CheckboxUI::UpdateFromModel(void) {
    KeywordUI::UpdateFromModel();

    origValue = keywordModel->IsSelected();
    //keywordModel->NotifySelection();
    if(keywordModel->IsEnabled()) {
        OnSelected();
    }
}

void CheckboxUI::UpdateToModel(bool isRestore) {
    if(isRestore) {
        keywordModel->SetSelected(origValue, false);
    }else{
    }
}

bool CheckboxUI::HasValue(void) const {
    return GetCheckBox()->IsChecked();
}
