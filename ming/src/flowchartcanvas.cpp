/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "commandproperty.h"
#include "inputparser.h"
#include "flowchartcanvas.h"
#include "stringutil.h"
#include "mingutil.h"
#include "tools.h"

BEGIN_EVENT_TABLE(FlowChartCanvas, wxPanel)
        EVT_PAINT(FlowChartCanvas::OnPaint)
        EVT_SIZE(FlowChartCanvas::OnSize)
        EVT_CHAR(FlowChartCanvas::OnKeyChar)
END_EVENT_TABLE()

static wxPen nameLinkPen(wxColour(0x80,0x80,0x80), 2, wxPENSTYLE_SOLID);
static wxPen valueLinkPen(wxColour(0x80,0x80,0x80), 2, wxPENSTYLE_SOLID);
static wxBrush valueLinkBrush(wxColour(0x80,0x80,0x80), wxBRUSHSTYLE_SOLID);

FlowChartCanvas::FlowChartCanvas(wxWindow *parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize){
        flowChartModel = NULL;
        flowShapes = NULL;
        // if the parent is wxScrollWindow, change its color also
        parent->SetBackgroundColour(*wxWHITE);
        this->SetBackgroundColour(*wxWHITE);
        this->SetSizer(NULL);
}

wxScrolledWindow* FlowChartCanvas::GetParentScrolledWin(void) {
    return (wxScrolledWindow*)GetParent();
}

FlowChartCanvas::~FlowChartCanvas() {
    if(flowShapes != NULL) {
        DeleteAllComponents();
    }
}

void FlowChartCanvas::OnSize(wxSizeEvent& event) {
    event.Skip();
    //RePosition();
}

void FlowChartCanvas::OnPaint(wxPaintEvent& event) {
        wxPaintDC dc(this);
        DrawLinks();
        event.Skip();
}

void FlowChartCanvas::RePosition(bool isScroll) {
        int shapeWidth = 250;
        int shapeHeight = 20;
        int shapeSpace = 5;
        int parentWidth = GetParent()->GetClientSize().GetWidth();

        if(parentWidth <= 0) parentWidth = 420;
        int shapePosX = (parentWidth - shapeWidth) / 2;
        int shapePosY = shapeSpace;

    //Tools::ShowError(wxString::Format(wxT("parent width %d"),parentWidth));
        FlowChartShape* next = flowShapes;
    while(next != NULL) {
                shapeHeight = 20 + next->GetLabelLineCount() * 20;
                next->SetSize(shapePosX, shapePosY, shapeWidth, shapeHeight);
                next->Update();
                next->Refresh();
        next = next->GetNextShape();

                shapePosY += shapeSpace + shapeHeight;
    }

    SetMaxSize(wxSize(parentWidth, shapePosY));
        SetMinSize(wxSize(parentWidth, shapePosY));
        GetParent()->GetSizer()->FitInside(this);
        GetParent()->FitInside();

        if(isScroll) GetParentScrolledWin()->Scroll(0, shapePosY);
    //((wxScrolledWindow*)GetParent())->SetScrollRate(100, 100);
        //((wxScrolledWindow*)GetParent())->SetVirtualSizeHints(1024, 800);
        //Useless since wxWidgets 2.9 (/usr/include/wx-3.0/wx/window.h:452)
        // GetParentScrolledWin()->SetVirtualSizeHints(parentWidth, shapePosY, parentWidth, shapePosY);

        Refresh();
        Update();
        DrawLinks();
}

void FlowChartCanvas::UpdateLinks(bool isMultiLine) {
    flowChartModel->GetFlowLinkData()->CalcLinkIndex();
    if(isMultiLine) {
        RePosition(false);
    }else {
        Refresh();
        DrawLinks();
    }
}

/**
 *
 */
void FlowChartCanvas::DrawLinks(void) {
    wxClientDC dc(this);

    FlowChartShape* curr = NULL;
    FlowChartShape* linkShape = NULL;
    ElementProperty * elemProp = NULL;
        CommandProperty* cmdProp = NULL;

    int linkIndex = -1;

    curr = flowShapes;
    while(curr != NULL) {
        elemProp = curr->GetElementProperty();
        if(elemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
                        cmdProp = (CommandProperty*)elemProp;
            linkIndex = elemProp->GetLinkIndex();
            //FileUtil::ShowInfoMessage(wxString::Format("%d", linkIndex));
                        if(linkIndex >= 0) {
                // may have link targets
                                if(cmdProp->IsNameLinkCommand()) {
                    // search a name link target with the same linkIndex
                                        linkShape = FindNameLinkShape(curr, linkIndex);
                                        if(linkShape) {
                        DrawNameLink(&dc, curr, linkShape, linkIndex);
                    }
                                }else if(cmdProp->GetLinkElemProp() != NULL) {
                    // a value link
                    linkShape = FindValueLinkShape(cmdProp->GetLinkElemProp());
                    if(linkShape) {
                        DrawValueLink(&dc, curr, linkShape, linkIndex, cmdProp->IsValueLinkCommandTarget());
                    }
                                }
            }
        }
        curr = curr->GetNextShape();
    }
}

/**
 * Find a value link shape whose elemnent property is the given linkElemProp
 */
FlowChartShape* FlowChartCanvas::FindValueLinkShape(ElementProperty* linkElemProp) {
        FlowChartShape* findNext = flowShapes;
        ElementProperty* findElemProp = NULL;

        while(findNext != NULL) {
                findElemProp = findNext->GetElementProperty();
                if(findElemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
            if(findElemProp == linkElemProp) {
                return findNext;
            }
                }
                findNext = findNext->GetNextShape();
        }
        return NULL;
}

FlowChartShape* FlowChartCanvas::FindNameLinkShape(FlowChartShape* startShape, int linkIndex) {
    char* anchorName = GetAttributeString(startShape->GetElementProperty()->GetXmlElement(), ATTR_LINKANCHOR);
        FlowChartShape* findNext = startShape->GetNextShape();
        ElementProperty* findElemProp = NULL;
        while(findNext != NULL) {
                findElemProp = findNext->GetElementProperty();
                if(findElemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
                        if(findElemProp->GetLinkIndex() == linkIndex) {
                                if(((CommandProperty*)findElemProp)->IsNameLinkCommandTarget(anchorName)) {
                    //the same link index and the its name is the anchor name of the given startShape
                    return findNext;
                }
                        }
                }
                findNext = findNext->GetNextShape();
        }
        return NULL;
}

void FlowChartCanvas::DrawNameLink(wxDC* dc, const FlowChartShape* shape1, const FlowChartShape* shape2, int index) {
    wxPoint point1 = shape1->GetPosition();
    wxPoint point2 = shape2->GetPosition();
    int shapeWidth = 0;
    int shapeHeight = 0;
    int lim = 30;
    shape1->GetSize(&shapeWidth, &shapeHeight);

    dc->SetBrush(*wxTRANSPARENT_BRUSH);
    dc->SetPen(nameLinkPen);
    index = flowChartModel->GetFlowLinkData()->GetMaxNameLinkIndex() - index + 1;
    //FileUtil::ShowInfoMessage(wxString::Format("%d, %d", flowChartModel->GetFlowLinkData()->GetMaxNameLinkIndex(), index));
    dc->DrawRectangle(point1.x-index*lim, point1.y+shapeHeight/2, shapeWidth+index*lim*2, point2.y - point1.y);
}

void FlowChartCanvas::DrawValueLink(wxDC* dc, const FlowChartShape* shape1, const FlowChartShape* shape2, int index, bool isShape1Target) {
    wxPoint points[4];
    int shapeWidth = 0;
    int shapeHeight = 0;
    int lim = 30;
    int arrowLen = 15;
    int arrowHeight = 3;
    int miny = 0, maxy = 0;
    int targety = 0;
    wxPoint point1 = shape1->GetPosition();
    wxPoint point2 = shape2->GetPosition();
    shape1->GetSize(&shapeWidth, &shapeHeight);

    if(point2.y < point1.y) {
        miny = point2.y;
        maxy = point1.y;
    }else {
        miny = point1.y;
        maxy = point2.y;
    }
    if(isShape1Target) {
        targety = point1.y;
    }else {
        targety = point2.y;
    }

    //draw lines
    index = index + 1;
    points[0] = wxPoint(point1.x+shapeWidth, maxy + shapeHeight/2);
    points[1] = wxPoint(point1.x+shapeWidth+lim/2+index*lim, maxy + shapeHeight/2);
        points[2] = wxPoint(point1.x+shapeWidth+lim/2+index*lim, miny + shapeHeight/2);
        points[3] = wxPoint(point1.x+shapeWidth, miny + shapeHeight/2);
    dc->SetBrush(*wxTRANSPARENT_BRUSH);
    dc->SetPen(valueLinkPen);
    dc->DrawLines(4, points);

    //draw arrow
    points[0] = wxPoint(point1.x+shapeWidth, targety + shapeHeight/2);
    points[1] = wxPoint(point1.x+shapeWidth+arrowLen, targety + shapeHeight/2 + arrowHeight);
        points[2] = wxPoint(point1.x+shapeWidth+arrowLen, targety + shapeHeight/2 - arrowHeight);
        dc->SetBrush(valueLinkBrush);
        dc->DrawPolygon(3, points);

}

void FlowChartCanvas::OnAddElement(ElementProperty* prop, int elemId, bool updatePosition) {
        FlowChartShape* shape = new FlowChartShape(this, wxID_ANY, prop);

        if(flowShapes == NULL) {
                flowShapes = shape;
        }else {
                flowShapes->AddShape(shape, elemId);
        }
    if(updatePosition) {
        RePosition();
    }
}

void FlowChartCanvas::OnAddElement(int elemId, int count) {
    if(flowChartModel == NULL) return;
    ElementProperty* next = flowChartModel->GetElementPropertyHead();
    ElementProperty* addNext = NULL;
    int addedCount = 0;
    int preElemId = -1;
    while(next != NULL) {
        //FileUtil::ShowInfoMessage(wxString::Format(wxT("id %d, givenid %d"), next->GetInternalId(), elemId));
        if(next->GetInternalId() == elemId || next->GetNextElement() == NULL) {
            addNext = next;
            do{
                OnAddElement(addNext, preElemId, false);
                addedCount++;
                preElemId = addNext->GetInternalId();
                addNext = addNext->GetNextElement();
            }while(addedCount < count && addNext != NULL);
            break;
        }
        preElemId = next->GetInternalId();
        next = next->GetNextElement();
    }

    RePosition();
}

void FlowChartCanvas::SetFlowChartModel(FlowChartModel* chartModel) {
        flowChartModel = chartModel;
        if(flowChartModel->GetEnvSetting() == NULL) {
                char* fileName = NULL;
//wxMessageBox(MingUtil::GetGlobalEnvFile());
                StringUtil::String2CharPointer(&fileName, MingUtil::GetGlobalEnvFile());
                flowChartModel->SetEnvSetting(InputParser::CreateEnvModule(fileName));
                if(fileName) free(fileName);
        }
    ElementProperty* next = chartModel->GetElementPropertyHead();
    while(next != NULL) {
        OnAddElement(next, -1, false);
        next = next->GetNextElement();
    }
        flowChartModel->GetFlowLinkData()->CalcLinkIndex();
        RePosition();
}

void FlowChartCanvas::UpdateExclusiveFocusOrSelection(FlowChartShape* shape, bool isFocus) {
    FlowChartShape* next = flowShapes;
    while(next != NULL) {
        if(next->GetId() != shape->GetId()) {
            if(isFocus)
                next->SetHasFocus(false);
            else
                next->SetSelected(false);
        }
        next = next->GetNextShape();
    }
}

void FlowChartCanvas::SetShapeFocus(int elemId) {
    FlowChartShape* next = flowShapes;
    while(next != NULL) {
        next->SetHasFocus(next->GetElementProperty()->GetInternalId() == elemId);
        next->SetSelected(next->GetElementProperty()->GetInternalId() == elemId);

        next = next->GetNextShape();
    }
    RePosition();
}

int FlowChartCanvas::GetInternalIdWithFocus(void) {
        int elemId = -1;
    FlowChartShape* next = flowShapes;
    while(next != NULL) {
        if(next->IsHasFocus()) {
            elemId = next->GetElementProperty()->GetInternalId();
                        break;
        }
                next = next->GetNextShape();
    }
        return elemId;
}

ElementProperty* FlowChartCanvas::GetElemPropWithFocus(void) {
        ElementProperty* elemProp = NULL;
    FlowChartShape* next = flowShapes;
    while(next != NULL) {
        if(next->IsHasFocus()) {
            elemProp = next->GetElementProperty();
                        break;
        }
                next = next->GetNextShape();
    }
        return elemProp;
}

void FlowChartCanvas::DeleteSelectedComponents(bool isClear) {
    int elemId = -1;
    FlowChartShape* next = flowShapes;
    FlowChartShape* prev = NULL;
    FlowChartShape* afterTheLastDelete = NULL;
    bool deleted = false;
        ElementProperty * elemProp = NULL;
        int linkIndex = -1;
        FlowChartShape* linkShape = NULL;

        //if user select command such as if, its corresponding command endif should also be deleted
        if(!isClear) {
                while(next != NULL) {
                        if(next->IsSelected()) {
                                elemProp = next->GetElementProperty();
                                if(elemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
                                        linkIndex = elemProp->GetLinkIndex();
                                        if(linkIndex >= 0 && ((CommandProperty*)elemProp)->IsNameLinkCommand()) {
                                                linkShape = FindNameLinkShape(next, linkIndex);
                                                if(linkShape) {
                                                        linkShape->SetSelected(true);
                                                }
                                        }
                                }
                        }
                        next = next->GetNextShape();
                }
        }


        next = flowShapes;
    while(next != NULL) {
        if(isClear || next->IsSelected()) {
            elemId = next->GetElementProperty()->GetInternalId();
            if(prev == NULL) {
                flowShapes = next->GetNextShape();
                delete next;
                next = flowShapes;
            }else {
                prev->SetNextShape(next->GetNextShape());
                delete next;
                next = prev->GetNextShape();
                        }
            flowChartModel->DeleteElement(elemId);
                        deleted = true;
                        afterTheLastDelete = next;
        }else {
            prev = next;
            next = next->GetNextShape();
        }
    }
        if(deleted) {
        if(afterTheLastDelete != NULL) {
            //if the deleted module is not the last one, select its next by default
            afterTheLastDelete->SetSelected(true);
            afterTheLastDelete->SetHasFocus(true);
        }else if(prev != NULL) {
            //if the deleted module is the last one, select its previous by default
            prev->SetSelected(true);
            prev->SetHasFocus(true);
        }
                RePosition(false);
        }
}

void FlowChartCanvas::DeleteAllComponents(void) {
    FlowChartShape* next = flowShapes;

    while(next != NULL) {
        flowShapes = next->GetNextShape();
        delete next;
        next = flowShapes;
    }
    flowShapes = NULL;
}

void FlowChartCanvas::OnKeyChar(wxKeyEvent& event) {
    event.Skip(true);
    if(event.GetKeyCode() == WXK_DELETE || event.GetKeyCode() == WXK_NUMPAD_DELETE) {
        DeleteSelectedComponents(false);
    }else if(event.GetKeyCode() == WXK_PAGEUP|| event.GetKeyCode() == WXK_NUMPAD_PAGEUP ) {
        MoveSelectedComponentUp();
    }else if(event.GetKeyCode() == WXK_PAGEDOWN|| event.GetKeyCode() == WXK_NUMPAD_PAGEDOWN ) {
        MoveSelectedComponentDown();
    }
    //if(event.GetKeyCode() == WXK_INSERT || event.GetKeyCode() == WXK_NUMPAD_INSERT) {
        //GetMingMainDialog()->DoAddFlowChartComponent(selectedCompType);
    //}
}

void FlowChartCanvas::MoveSelectedComponentUp(void) {
    int elemId = -1;
    FlowChartShape* ancester = NULL;
    FlowChartShape* parent = NULL;
    FlowChartShape* curr = flowShapes;
    FlowChartShape* child = NULL;

    while(curr != NULL) {
        elemId = curr->GetElementProperty()->GetInternalId();
        if(!curr->IsSelected()) {
            ancester = parent;
            parent = curr;
            curr = curr->GetNextShape();
        }else {
            child = curr->GetNextShape();
            if(NULL != parent) {
                if(NULL != ancester) {
                    ancester->SetNextShape(curr);
                }
                curr->SetNextShape(parent);
                parent->SetNextShape(child);
                if(flowShapes == parent) {
                    flowShapes = curr;
                }
                flowChartModel->MoveElementUp(elemId);
                RePosition(false);
            }
            //
            break;
        }
    }
}

void FlowChartCanvas::MoveSelectedComponentDown(void) {
    int elemId = -1;
    FlowChartShape* parent = NULL;
    FlowChartShape* curr = flowShapes;
    FlowChartShape* child = NULL;
    FlowChartShape* grandchild = NULL;

    while(curr != NULL) {
        elemId = curr->GetElementProperty()->GetInternalId();
        if(!curr->IsSelected()) {
            parent = curr;
            curr = curr->GetNextShape();
        }else {
            child = curr->GetNextShape();
            if(NULL != child) {
                grandchild = child->GetNextShape();
                if(NULL != parent) {
                    parent->SetNextShape(child);
                }
                child->SetNextShape(curr);
                curr->SetNextShape(grandchild);
                if(flowShapes == curr) {
                    flowShapes = child;
                }
                flowChartModel->MoveElementDown(elemId);
                RePosition(false);
            }
            //
            break;
        }
    }
}
