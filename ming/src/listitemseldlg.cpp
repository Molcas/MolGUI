/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/statline.h>
#include <wx/notebook.h>

#include "mxyzfile.h"

#include "listitemseldlg.h"
#include "stringutil.h"

#include "cstringutil.h"

BEGIN_EVENT_TABLE(ListItemSelDlg, wxDialog)
    EVT_BUTTON(wxID_OK, ListItemSelDlg::OnDialogOK)
    EVT_BUTTON(wxID_CANCEL, ListItemSelDlg::OnDialogCancel)
    EVT_CLOSE(ListItemSelDlg::OnClose)

    EVT_LISTBOX(CTRL_ID_LB_ITEMS, ListItemSelDlg::OnLbItemsClicked)
    EVT_LISTBOX_DCLICK(CTRL_ID_LB_ITEMS, ListItemSelDlg::OnLbItemsDClicked)
    EVT_LISTBOX_DCLICK(CTRL_ID_LB_SELECTED, ListItemSelDlg::OnLbSelectedDClicked)

    EVT_BUTTON(CTRL_ID_BTN_ADDITEM, ListItemSelDlg::OnAddItem)
    EVT_BUTTON(CTRL_ID_BTN_DELITEM, ListItemSelDlg::OnDelItem)

END_EVENT_TABLE()

ListItemSelDlg::ListItemSelDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style) {
    pLbItems = NULL;
    pLbSelected = NULL;
    selecteableItemCount = -1;
    CreateControls();
}

ListItemSelDlg::~ListItemSelDlg() {
}



void ListItemSelDlg::CreateControls(void) {

    wxBoxSizer* bsItems = new wxBoxSizer(wxVERTICAL);
    wxStaticText* lbItems = new wxStaticText(this, wxID_ANY, wxT("Available Items"));
           lbItems->SetLabel(wxT("Available Items"));
    pLbItems = new wxListBox(this, CTRL_ID_LB_ITEMS, wxDefaultPosition, wxSize(200,200));
    bsItems->Add(lbItems, 0, wxEXPAND | wxALIGN_CENTER | wxALL, 2);
    bsItems->Add(pLbItems, 1, wxEXPAND|wxALL, 2);
    //
    //
    wxBoxSizer* bsBtns = new wxBoxSizer(wxVERTICAL);
    wxButton* btnAdd = new wxButton(this, CTRL_ID_BTN_ADDITEM, wxT(">>"), wxDefaultPosition, wxSize(60, 30));
    wxButton* btnDel = new wxButton(this, CTRL_ID_BTN_DELITEM, wxT("<<"), wxDefaultPosition, wxSize(60, 30));
    bsBtns->AddStretchSpacer(1);
    bsBtns->Add(btnAdd, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
    bsBtns->Add(btnDel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
    bsBtns->AddStretchSpacer(1);
    //
    wxBoxSizer* bsSelected = new wxBoxSizer(wxVERTICAL);
    wxStaticText* lbSelected = new wxStaticText(this, wxID_ANY, wxT("Selected Items"));
           lbSelected->SetLabel(wxT("Selected Items"));
    pLbSelected = new wxListBox(this, CTRL_ID_LB_SELECTED, wxDefaultPosition, wxSize(200,200));
    bsSelected->Add(lbSelected, 0, wxEXPAND | wxALIGN_CENTER | wxALL, 2);
    bsSelected->Add(pLbSelected, 1, wxEXPAND|wxALL, 2);
    //
    wxBoxSizer* bsCenter = new wxBoxSizer(wxHORIZONTAL);
    bsCenter->Add(bsItems, 1, wxEXPAND|wxALL, 2);
    bsCenter->Add(bsBtns, 0, wxEXPAND|wxALL, 2);
    bsCenter->Add(bsSelected, 1, wxEXPAND|wxALL, 2);

    wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnOk = new wxButton(this, wxID_OK, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    wxButton * btnCancel = new wxButton(this, wxID_CANCEL, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    bsBottom->AddStretchSpacer(1);
    bsBottom->Add(btnOk, 0, wxALL, 2);
    bsBottom->Add(btnCancel, 0, wxALL, 2);

    wxStaticLine* line = new wxStaticLine(this, wxID_STATIC, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);

    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
    bsTop->Add(bsCenter, 1, wxEXPAND|wxALL, 2);
    bsTop->AddSpacer(3);
    bsTop->Add(line, 0, wxEXPAND | wxALL, 2);
    bsTop->Add(bsBottom, 0, wxEXPAND| wxALIGN_RIGHT| wxALL, 2);

    this->SetSizer(bsTop);
    SetSize(500, 300);
        Center();

}

void ListItemSelDlg::SetSelectableItems(char** items, int count){
    int i ;
    for (i= 0; i < count; i++ ) {
        pLbItems->Append(StringUtil::CharPointer2String(items[i]));
    }
}

void ListItemSelDlg::SetSelectableItems(const wxArrayString& items){
    pLbItems->Set(items);
}

void ListItemSelDlg::OnDialogOK(wxCommandEvent& event) {
    HideDialog(wxID_OK);
}

void ListItemSelDlg::OnDialogCancel(wxCommandEvent& event) {
    HideDialog(wxID_CANCEL);
}

void ListItemSelDlg::OnClose(wxCloseEvent& event){
    HideDialog(wxID_CANCEL);
}

void ListItemSelDlg::HideDialog(int returnCode) {
    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
        //Destroy();
}

wxArrayString ListItemSelDlg::GetSelectedItems(void) {
    return pLbSelected->GetStrings();
}

void ListItemSelDlg::SetSelectedItems(const wxArrayString& items) {
    pLbSelected->Set(items);
}

void ListItemSelDlg::OnLbItemsClicked(wxCommandEvent& event) {
    if(pLbItems->GetSelection() == wxNOT_FOUND) return;
    if(pLbSelected->FindString(pLbItems->GetStringSelection()) != wxNOT_FOUND) {
        pLbItems->Deselect(pLbItems->GetSelection());
    }

}

void ListItemSelDlg::OnLbItemsDClicked(wxCommandEvent& event) {
    OnAddItem(event);
}

void ListItemSelDlg::OnLbSelectedDClicked(wxCommandEvent& event) {
    if(pLbSelected->GetSelection() != wxNOT_FOUND) {
        pLbSelected->Delete(pLbSelected->GetSelection());
    }
}

void ListItemSelDlg::OnAddItem(wxCommandEvent& event) {
    wxString selectedItems = pLbItems->GetStringSelection();
    if(selectedItems.IsEmpty()) return;
    if(selecteableItemCount == 1) {
        pLbSelected->Clear();
    }else{
        if(selecteableItemCount > 0 && selecteableItemCount <= (int)pLbSelected->GetCount()) return;
    }
    if(pLbSelected->FindString(selectedItems) == wxNOT_FOUND) {
        pLbSelected->Append(selectedItems);
    }
}

void ListItemSelDlg::OnDelItem(wxCommandEvent& event) {
    if(pLbSelected->GetSelection() != wxNOT_FOUND) {
        pLbSelected->Delete(pLbSelected->GetSelection());
    }
}

void ListItemSelDlg::SetSelectableItemCount(int count) {
    selecteableItemCount = count;
}
