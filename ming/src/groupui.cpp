/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "groupui.h"
#include "checkboxui.h"
#include "keywordconst.h"
#include "cstringutil.h"

#define CTRL_CLEAR_BTN_ID CTRL_WIN_CTRL_CUSTOM_START + 1

BEGIN_EVENT_TABLE(GroupUI, KeywordUI)
        EVT_RADIOBUTTON(wxID_ANY, GroupUI::OnRadioButton)
        EVT_BUTTON(CTRL_WIN_CTRL_ID, GroupUI::OnEnableButton)
        EVT_BUTTON(CTRL_CLEAR_BTN_ID, GroupUI::OnClearButton)
END_EVENT_TABLE()

GroupUI::GroupUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel) {
        arrayPanel = NULL;
        arrayRadioBtn = NULL;
        pnlGroup = NULL;
        origRadioIndex = -1;
        btnEnable = NULL;
        btnClear = NULL;
        CreateUIControls();
}

GroupUI::~GroupUI() {
        if (arrayPanel)
                delete[] arrayPanel;
        if (arrayRadioBtn)
                delete[] arrayRadioBtn;
}


GroupModel* GroupUI::GetGroupModel(void) const {
        return (GroupModel*)keywordModel;
}

void GroupUI::CreateUIControls(void) {
        int i = 0;
        int subCount = 0;
        int nonNullSubCount = 0;
        long style = -1;
        KeywordModel* tmpModel = NULL;
        wxSizer* addedSizer = NULL;
        bool isGroupRadio = GetGroupModel()->GetGroupKind() == KEYWORD_KIND_RADIO;

        //if the panel structure has been modified in GroupUI
        //should also modify checkboxui.cpp consequently.
        CreateToftSizer(wxVERTICAL, GetGroupModel()->GetWindowKind() == WINDOW_KIND_INPLACE);
        wxBoxSizer* bsTopBtns = new wxBoxSizer(wxHORIZONTAL);
        if (GetGroupModel()->GetWindowKind() == WINDOW_KIND_TAB) {
                btnEnable = new wxButton(this, CTRL_WIN_CTRL_ID, wxT("Enable"), wxDefaultPosition, wxSize(60, 30));
                bsTopBtns->Add(btnEnable, 0, wxALL, 2);
        }
        /*if(isGroupRadio) {
            btnClear = new wxButton(this, CTRL_CLEAR_BTN_ID, wxT("Clear"), wxDefaultPosition, wxSize(60,30));
            bsTopBtns->Add(btnClear, 0, wxALL, 2);
        }*/
        pnlGroup = new wxPanel(this, wxID_ANY);
        pnlGroup->SetSizer(new wxBoxSizer(wxVERTICAL));
        GetSizer()->Add(bsTopBtns, 0, wxALL, 2);
        GetSizer()->Add(pnlGroup, 1, wxEXPAND | wxALL, 2);

        subCount = GetGroupModel()->GetSubKeywordModelCount();
        if (subCount > 0) {
                arrayPanel = new KeywordUI*[subCount];
                for (i = 0; i < subCount; i++) {
                        tmpModel = GetGroupModel()->GetChildModule()->GetKeywordModel(i);
                        if (tmpModel != NULL) {
                                arrayPanel[i] = KeywordUI::CreateModelUI(pnlGroup, tmpModel);
                        } else {
                                arrayPanel[i] = NULL;
                        }
                }

                if (isGroupRadio) {
                        arrayRadioBtn = new wxRadioButton*[subCount];
                        for (i = 0; i < subCount; i++) {
#if !(defined(__LINUX__) || defined(__MAC__))
                                style = wxRB_SINGLE;
#else
                                if (style == -1) {
                                        style = wxRB_GROUP;
                                } else {
                                        style = 0;
                                }
#endif
                                if (arrayPanel[i] != NULL) {
                                        arrayRadioBtn[i] = new wxRadioButton(pnlGroup, CTRL_WIN_RADIO_START + i, wxEmptyString, wxDefaultPosition, wxDefaultSize, style);
                                        if (GetGroupModel()->GetChildModule()->GetKeywordModel(i)->GetKind() == KEYWORD_KIND_SINGLE) {
                                                ((CheckboxUI*)arrayPanel[i])->SetGroupIndex(i);
                                        }
                                        nonNullSubCount++;
                                } else {
                                        arrayRadioBtn[i] = NULL;
                                }
                        }
                }


                if (isGroupRadio) {
                        addedSizer = new wxFlexGridSizer(nonNullSubCount, 2, 2, 2);
                        ((wxFlexGridSizer*)addedSizer)->AddGrowableCol(1, 1);
                        pnlGroup->GetSizer()->Add(addedSizer, 1, wxEXPAND | wxALL, 2);
                } else {
                        addedSizer = pnlGroup->GetSizer();
                }
                for (i = 0; i < subCount; i++) {
                        if (arrayPanel[i] != NULL) {
                                if (isGroupRadio) {
                                        addedSizer->Add(arrayRadioBtn[i], 0, wxEXPAND | wxALL, 2);
                                }
                                addedSizer->Add(arrayPanel[i], 0, wxEXPAND | wxALL, 2);
                        }
                }
        }
        GetSizer()->SetSizeHints(this);
        UpdateFromModel();
}

void GroupUI::SetRadioButton(int index, bool value) {
        if (index >= 0 && index < GetGroupModel()->GetSubKeywordModelCount() && arrayRadioBtn[index]) {
                arrayRadioBtn[index]->SetValue(value);
        }
}

void GroupUI::OnRadioButton(wxCommandEvent& event) {
        int radioIndex = event.GetId() - CTRL_WIN_RADIO_START;
        int subCount = GetGroupModel()->GetSubKeywordModelCount();
        if (radioIndex < 0 || radioIndex >= subCount)
                return;

        GetGroupModel()->SetSelectedIndex(radioIndex);

        if (GetGroupModel()->GetChildModule()->GetKeywordModel(radioIndex)->GetKind() == KEYWORD_KIND_SINGLE) {
                ((CheckboxUI*)arrayPanel[radioIndex])->DoUIEvent(true);
        }
        //FileUtil::ShowInfoMessage(wxString::Format("get %d = %d", radioIndex, GetGroupModel()->GetSelectedIndex()));
#if !(defined(__LINUX__) || defined(__MAC__))
        int i = 0;
        for (i = 0; i < subCount; i++) {
                if (i != radioIndex && arrayRadioBtn[i] != NULL) {
                        arrayRadioBtn[i]->SetValue(false);
                }
        }
#endif
}

void GroupUI::OnEnableButton(wxCommandEvent& event) {
}

void GroupUI::OnClearButton(wxCommandEvent& event) {
        GetGroupModel()->SetSelectedIndex(-1);
        int i = 0;
        int subCount = GetGroupModel()->GetSubKeywordModelCount();
        for (i = 0; i < subCount; i++) {
                if (arrayRadioBtn[i] != NULL) {
                        arrayRadioBtn[i]->SetValue(false);
                }
        }
}

void GroupUI::UpdateFromModel(void) {
        int i = 0;
        int subCount = GetGroupModel()->GetSubKeywordModelCount();

        if (GetGroupModel()->GetGroupKind() == KEYWORD_KIND_RADIO) {
                //FileUtil::ShowInfoMessage(wxString::Format("in %d", origRadioIndex));
                for (i = 0; i < subCount; i++) {
                        if (!arrayPanel[i]) continue;
                        //arrayRadioBtn[i]->SetValue(i == origRadioIndex);
                        if (arrayPanel[i]->GetKeywordModel()->GetValue() != NULL) {
                                arrayRadioBtn[i]->SetValue(true);
                                origRadioIndex = i;
                        } else {
                                arrayRadioBtn[i]->SetValue(false);
                        }
                        //arrayRadioBtn[i]->Refresh();
                        //arrayRadioBtn[i]->Update();
                        //arrayRadioBtn[i]->UpdateWindowUI();
                }
                //origRadioIndex = GetGroupModel()->GetSelectedIndex();
        }

        for (i = 0; i < subCount; i++) {
                if (arrayPanel[i])
                        arrayPanel[i]->UpdateFromModel();
        }
}

void GroupUI::UpdateToModel(bool isRestore) {
        int i = 0;
        int subCount = GetGroupModel()->GetSubKeywordModelCount();
        bool isGroupRadio = GetGroupModel()->GetGroupKind() == KEYWORD_KIND_RADIO;
        if (isRestore) {
                GetGroupModel()->SetSelectedIndex(origRadioIndex);
        }
        for (i = 0; i < subCount; i++) {
                if (!arrayPanel[i])  continue;
                if (isRestore || !isGroupRadio) {
                        arrayPanel[i]->UpdateToModel(isRestore);
                } else if (isGroupRadio) {
                        if (i != GetGroupModel()->GetSelectedIndex()) {
                                arrayPanel[i]->GetKeywordModel()->SetValue(NULL);
                        } else {
                                arrayPanel[i]->UpdateToModel(isRestore);
                        }
                }
        }
}

bool GroupUI::HasValue(void) const {
        int i = 0;
        int subCount = GetGroupModel()->GetSubKeywordModelCount();

        if (subCount > 0 && arrayPanel) {
                if (GetGroupModel()->GetGroupKind() == KEYWORD_KIND_RADIO) {
                        return GetGroupModel()->GetSelectedIndex() >= 0 && GetGroupModel()->GetSelectedIndex() < subCount;
                } else {
                        for (i = 0; i < subCount; i++) {
                                if (!arrayPanel[i])  continue;
                                if (arrayPanel[i]->HasValue()) {
                                        return true;
                                }
                        }
                }
        }

        return false;
}
