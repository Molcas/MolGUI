/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "checkboxmodel.h"

CheckboxModel::CheckboxModel(ModuleProperty* parentModule, XmlElement* elem) : KeywordModel(parentModule, elem){
}

bool CheckboxModel::SaveAsInputFormat(FILE* fp, bool isEnv) {
    //if(IsSelected() ||
    //    GetParentModule()->GetPropType() == ELEM_PROP_TYPE_SELECT){
        if(IsSelected()) {
        fprintf(fp, "%s\n", GetName());
    }
    return true;
}

void CheckboxModel::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
    SetSelected(true, false);
    buf[0] = 0;
}
