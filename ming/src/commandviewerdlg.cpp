/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/statline.h>

#include "commandviewerdlg.h"
#include "stringutil.h"
#include "cstringutil.h"
#include "manager.h"
#include "envutil.h"

#define CTRL_BTN_ID_START 100
#define CTRL_BTN_ID_END 200
//static const char* COMM_PREF = ">>";

BEGIN_EVENT_TABLE(CommandViewerDlg, wxDialog)
    EVT_BUTTON(wxID_ANY, CommandViewerDlg::OnButton)
    EVT_CLOSE(CommandViewerDlg::OnClose)
END_EVENT_TABLE()

CommandViewerDlg::CommandViewerDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style )
//begin : hurukun : 20/11/2009
// : wxDialog(parent, id, title, pos, size, style) {
{
        wxDialog::Create(parent,id,title,pos,size,style);
//end   : hurukun : 20/11/2009
    commandProperty = NULL;
    currInputCtrlID = CTRL_BTN_ID_START;
    inputCtrls.Clear();
}
CommandViewerDlg::~CommandViewerDlg() {
}

void CommandViewerDlg::SetCommandProperty(CommandProperty* prop) {
    commandProperty = prop;
    SetTitle(StringUtil::CharPointer2String(commandProperty->GetName()) + wxT(" - ") + GetTitle());

        CreateControls();
        GetSizer()->SetSizeHints(this);
        Center();

        UpdateFromModel();
}

void CommandViewerDlg::CreateControls(void) {
    wxBoxSizer* bsTop;
    wxSizer* sizerInput = NULL;
    int k = 0;

    bsTop = new wxBoxSizer(wxVERTICAL);
    if(commandProperty->GetSubElementCount() > 0) {
        for(k = 0; k < commandProperty->GetSubElementCount(); k++) {
            sizerInput = CreateInputHorizontalSizer(commandProperty->GetSubElement(k));
            bsTop->Add(sizerInput, 1, wxEXPAND|wxALL, 2);
        }
    }else {
        sizerInput = CreateInputHorizontalSizer(commandProperty->GetXmlElement());
        bsTop->Add(sizerInput, 1, wxEXPAND|wxALL, 2);
    }

    wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnOk = new wxButton(this, wxID_OK, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    wxButton * btnCancel = new wxButton(this, wxID_CANCEL, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    bsBottom->Add(btnOk, 0, wxALL, 2);
    bsBottom->Add(btnCancel, 0, wxALL, 2);

    wxStaticLine* line = new wxStaticLine(this, wxID_STATIC, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
    bsTop->AddSpacer(3);
    bsTop->Add(line, 0, wxEXPAND | wxALL, 2);
    bsTop->Add(bsBottom, 0, wxALIGN_RIGHT| wxALL, 2);

    this->SetSizer(bsTop);
}

void CommandViewerDlg::OnButton(wxCommandEvent& event) {
    unsigned i = 0;
    wxTextCtrl* tempTextCtrl = NULL;
    int id = event.GetId();
    if(id == wxID_OK || id == wxID_CANCEL) {
        HideDialog(id);
    }else {
                wxString        defDir=wxEmptyString;
                wxString        tmpPropertyName=StringUtil::CharPointer2String(commandProperty->GetName());
                if(tmpPropertyName.IsSameAs(wxT("COPY"))){
                        wxTreeCtrl*        pTree=Manager::Get()->GetProjectManager()->GetTree();
                        if(pTree!=NULL){
                                wxTreeItemId        itemId=pTree->GetSelection();
                                ProjectTreeItemData *item = itemId.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(itemId) : NULL;
                                if(item!=NULL){
                                        wxString        jobName=item->GetTitle();
                                        SimuProject*        pProject=item->GetProject();
                                        if(pProject!=NULL)
                                        {
                                                defDir=pProject->GetFileName().BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+jobName;
                                        }
                                }
                        }
                }
        wxFileDialog fileDialog(GetParent(), wxT("Open File"), defDir, wxEmptyString, wxT("All files (*)|*"), wxFD_OPEN| wxSTAY_ON_TOP);
        if (fileDialog.ShowModal() == wxID_OK) {

                    id--;
                    for(i = 0; i < inputCtrls.GetCount(); i++) {
                //FileUtil::ShowInfoMessage(wxString::Format("win %d", inputCtrls[i]->GetId()));
                if(inputCtrls[i]->GetId() == id) {
                    tempTextCtrl = wxDynamicCast(inputCtrls[i], wxTextCtrl);
                    if(tempTextCtrl) {
                        tempTextCtrl->SetValue(fileDialog.GetPath());
                    }
                    break;
                }
            }
      }
    }
}

void CommandViewerDlg::OnClose(wxCloseEvent& event){
    HideDialog(wxID_CANCEL);
}

wxSizer* CommandViewerDlg::CreateInputHorizontalSizer(XmlElement* elem) {
    wxBoxSizer* inputSizer = new wxBoxSizer(wxHORIZONTAL);
    char* format;
    char* nonBlank = NULL;
    char* charLabels = NULL;
    char* attrValues = NULL;
    char** attrValueArray = NULL;
    int attrValueCount = 0;
    char** optionValues = NULL;
    int optionCount = 0;

    int fileIndex = -1;
    char forChar;
    int index = 0;
    //int preIndex = 0;
    int inputCount = 0;
    int k;
    bool isReadOnly = true;
    wxStaticText* labelCtrl = NULL;
    wxTextCtrl* textCtrl = NULL;
    wxComboBox* comboCtrl = NULL;
    wxArrayString arraySelection;
        char* defaultValue = NULL;

    format = GetAttributeString(elem, ATTR_FORMAT);
    if(format == NULL || strlen(format) <= 2) {
        return inputSizer;
    }
    nonBlank = StringLeftTrim((char*)format, NULL);

        defaultValue = GetAttributeString(elem, ATTR_DEFAULTVALUE);
    attrValues = GetAttributeString(elem, ATTR_VALUES);
    if(attrValues) {
        attrValueArray = StringTokenizer(attrValues, LOGIC_DELIM, &attrValueCount, 1);
    }

    if(!GetAttributeInt(&fileIndex, elem, ATTR_FILEINDEX)) {
        fileIndex = -1;
    }
    //preIndex = index;
    index = SubStringIndex(nonBlank, "%", 0);
    while(index > 0) {
        if(index < (int)strlen(nonBlank) - 1) {
            forChar = nonBlank[index + 1];
            if('s' == forChar || 'd' == forChar) {
                //StringCopy(&charLabels, nonBlank, index - preIndex);
                StringCopy(&charLabels, nonBlank, index);
                labelCtrl = new wxStaticText(this, wxID_ANY, StringUtil::CharPointer2String(charLabels));
                labelCtrl->SetLabel(StringUtil::CharPointer2String(charLabels));
                inputSizer->Add(labelCtrl, 0, wxALIGN_CENTER|wxALL, 2);
                isReadOnly = true;
                if(attrValueCount > inputCount) {
                    arraySelection.Clear();
                    optionValues =  StringTokenizer(attrValueArray[inputCount], LOGIC_OR, &optionCount, 1);
                    for(k = 0; k < optionCount; k++) {
                        if(strcmp(optionValues[k], ATTR_VALUE_USERINPUT) == 0) {
                            arraySelection.Add(wxEmptyString);
                            isReadOnly = false;
                        }else {
                            arraySelection.Add(StringUtil::CharPointer2String(optionValues[k]));
                        }
                    }
                    if(optionValues) FreeStringArray(optionValues, optionCount);
                    if(isReadOnly) {
                        comboCtrl = new wxComboBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, arraySelection, wxCB_READONLY);
                        inputSizer->Add(comboCtrl, 0, wxEXPAND | wxALL, 2);
                    } else {
                        comboCtrl = new wxComboBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, arraySelection);
                        inputSizer->Add(comboCtrl, 1,  wxEXPAND | wxALL, 2);
                    }
                                        if(defaultValue) {
                                                comboCtrl->SetValue(StringUtil::CharPointer2String(defaultValue));
                                        }else if(arraySelection.GetCount() > 0) {
                        comboCtrl->SetValue(arraySelection[0]);  //�������Ĭ��ֵ�����״���ΪĬ��ֵ�����û��Ĭ��ֵ�����״�����Ϊlist�б�ѡ���еĵ�һ����
                                                                  //�б���ѡ���һ��ֵ�п�����wxEmptyString��Ҳ�п�������ֵ��
                    }
                    inputCtrls.Add(comboCtrl);
                }else {
                    textCtrl = new wxTextCtrl(this, currInputCtrlID++, wxEmptyString, wxDefaultPosition, wxSize(200, 30));
                                        if(defaultValue) {
                                                textCtrl->SetValue(StringUtil::CharPointer2String(defaultValue));
                                        }
                    inputSizer->Add(textCtrl, 1, wxEXPAND | wxALL, 2);
                    inputCtrls.Add(textCtrl);
                    if(inputCount == fileIndex) {
                        wxButton* fileBtn = new wxButton(this, currInputCtrlID++, wxT("..."), wxDefaultPosition, wxSize(30, 30));
                        inputSizer->Add(fileBtn, 0, wxALL, 2);
                    }
                }
                inputCount++;
            }
            if(charLabels) {
                free(charLabels);
                charLabels = NULL;
            }
        }else {
            break;
        }
        nonBlank = &nonBlank[index + 2];        //it's safe as each string is ended with '\0'
        index = SubStringIndex(nonBlank, "%", 0);
    }
    if(strlen(nonBlank) > 0) {
        StringCopy(&charLabels, nonBlank, (int)strlen(nonBlank));
        labelCtrl = new wxStaticText(this, wxID_ANY, StringUtil::CharPointer2String(charLabels));
        labelCtrl->SetLabel(StringUtil::CharPointer2String(charLabels));
        inputSizer->Add(labelCtrl, 0, wxALIGN_CENTER|wxALL, 2);
        if(charLabels) {
            free(charLabels);
            charLabels = NULL;
        }
    }

    if(attrValueArray) FreeStringArray(attrValueArray, attrValueCount);
    return inputSizer;
}

void CommandViewerDlg::SetCtrlValue(int ctrlIndex, const char* value) {
    wxTextCtrl* textCtrl = NULL;
    wxComboBox* comboCtrl = NULL;

    textCtrl = wxDynamicCast(inputCtrls[ctrlIndex], wxTextCtrl);
    if(textCtrl) {
        textCtrl->SetValue(StringUtil::CharPointer2String(value));
    }

    comboCtrl = wxDynamicCast(inputCtrls[ctrlIndex], wxComboBox);
    if(comboCtrl) {
        comboCtrl->SetValue(StringUtil::CharPointer2String(value));
    }
}

wxString CommandViewerDlg::GetCtrlValue(int ctrlIndex) {
    wxTextCtrl* textCtrl = NULL;
    wxComboBox* comboCtrl = NULL;

    textCtrl = wxDynamicCast(inputCtrls[ctrlIndex], wxTextCtrl);
    if(textCtrl) {
        return textCtrl->GetValue();
    }

    comboCtrl = wxDynamicCast(inputCtrls[ctrlIndex], wxComboBox);
    if(comboCtrl) {
        return comboCtrl->GetValue();
    }

    return wxEmptyString;
}

void CommandViewerDlg::UpdateFromModel(void) {
    char* charValue = commandProperty->GetPropValue();
    char** propValues = NULL;
    int propCount = 0;
    int k;
    if(charValue != NULL) {
        propValues =  StringTokenizer(charValue, VALUE_DELIM, &propCount, 1);
        for(k = 0; k < propCount; k++) {
            SetCtrlValue(k, propValues[k]);
        }
        if(propValues) FreeStringArray(propValues, propCount);
    }
}

void CommandViewerDlg::UpdateToModel(bool isRestore) {
    wxString strValue = wxEmptyString;
    wxString strAllValues = wxEmptyString;
    char* charValue = NULL;
    int i;
    if(isRestore) {
    }else {
        for(i = 0; i < (int)inputCtrls.GetCount(); i++) {
            strValue = GetCtrlValue(i);
            strValue = strValue.Trim().Trim(false);
            strAllValues +=  strValue;
            if(i < (int)inputCtrls.GetCount() - 1) {
                strAllValues += StringUtil::CharPointer2String(VALUE_DELIM);
            }
        }

        StringUtil::String2CharPointer(&charValue, strAllValues);
        commandProperty->SetPropValue(charValue);
        if(charValue)
            free(charValue);
    }
}

void CommandViewerDlg::HideDialog(int returnCode) {
    if(wxID_OK == returnCode) {
        ////GetMingMainDialog()->DoSaveHistoryFile();
    }
    UpdateToModel(wxID_OK != returnCode);
    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
        //Destroy();
}
