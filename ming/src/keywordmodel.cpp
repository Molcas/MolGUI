/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "keywordmodel.h"

#include "xmlparser.h"
#include "keywordconst.h"
#include "keywordparser.h"
#include "cstringutil.h"

#include "tools.h"
#include "fileutil.h"
#include "wx/filename.h"
#include "stringutil.h"

KeywordModel::KeywordModel(ModuleProperty* parentModule, XmlElement* elem) {
    LogicData lData;
    this->parentModule = parentModule;
        this->childModule = NULL;
    xmlElement = elem;
    isEnable = true;
    isGroupRadio = false;
    value = NULL;
    changeListener = NULL;
    requireData.values = NULL;
    exclusiveData.values = NULL;

    // parse REQUIRE and EXCLUSIVE value ready for use.
    lData = GetAttributeLogicData(xmlElement, ATTR_REQUIRE);
        if(NULL != lData.values) {
        requireData.ope = lData.ope;
        requireData.count = lData.count;
        requireData.values = lData.values;
        }

    lData = GetAttributeLogicData(xmlElement, ATTR_EXCLUSIVE);
        if(NULL != lData.values) {
        exclusiveData.ope = lData.ope;
        exclusiveData.count = lData.count;
        exclusiveData.values = lData.values;
        }
}

KeywordModel::~KeywordModel() {
    if(value)
        free(value);
    if(requireData.values)
        FreeStringArray(requireData.values, requireData.count);
    if(exclusiveData.values)
        FreeStringArray(exclusiveData.values, exclusiveData.count);
}

void KeywordModel::RegisterChangeListener(KeywordChangeListener* changeListener) {
    this->changeListener = changeListener;
}

void KeywordModel::UnRegisterChangeListener(void) {
        this->changeListener = NULL;
}

ModuleProperty* KeywordModel::GetParentModule(void) const {
    return parentModule;
}

ModuleProperty* KeywordModel::GetChildModule(void) const {
        return childModule;
}

void KeywordModel::SetChildModule(ModuleProperty* moduleProp) {
        childModule = moduleProp;
}

void KeywordModel::SetEnabled(bool isEnabled, bool updateUI) {
    this->isEnable = isEnabled;
        if(!isEnable) {
                SetValue(NULL);
                ClearData(updateUI);
        }
    if(updateUI && changeListener != NULL) {
                // update UI
        changeListener->OnEnabled();
        }
}

bool KeywordModel::IsEnabled(void) const {
    return isEnable;
}

bool KeywordModel::IsSelected(void) const{
    if(value && strcmp(value, "TRUE") == 0) {
        return true;
    }
    return false;
}

/**
 * Set this keyword is selected or not.
 * @note: it should be called for SINGLE model
 */
void KeywordModel::SetSelected(bool selected, bool updateUI) {
        if(GetKind() != KEYWORD_KIND_SINGLE)
                return;
    if(selected) {
        SetValue("TRUE");
    }else {
        SetValue("FALSE");
    }
    if(updateUI && changeListener != NULL) {
                // update UI
        changeListener->OnSelected();
    }
}

void KeywordModel::ClearData(bool updateUI) {
        //TODO: clear its data in the model
        if(updateUI && changeListener != NULL) {
                // update UI
        changeListener->OnClearData();
    }
}

void KeywordModel::SetValue(const char* newValue) {
    if(this->value != NULL) {
        free(this->value);
    }
    if(newValue != NULL) {
        StringCopy(&this->value, newValue, strlen(newValue));
    }else {
        this->value = NULL;
    }

    if(parentModule->GetPropType() == ELEM_PROP_TYPE_SELECT) {
        parentModule->GetParentKeywordModel()->SetValue(GetName());
    }
}

char* KeywordModel::GetValue(void) const {
    return this->value;
}

char* KeywordModel::GetElementTag(void) const {
    return xmlElement->name;
}

char* KeywordModel::GetHelp(void) const {
    //New format keyword.xml by GRP for Ignacio
    // return GetElementP_Help(xmlElement);
    return GetElementHelp(xmlElement);
}

//New format keyword.xml by GRP for Ignacio
// char* KeywordModel::GetP_Help(void) const {
//     return GetElementP_Help(xmlElement);
// }

char* KeywordModel::GetName(void) const {
    char* name = StringRightTrim(GetAttributeValue(ATTR_NAME), NULL);
    if(name && strlen(name) == 0) {
            Tools::ShowError(wxT("The names of some keywords are empty."));
    }
    return name;
    //return GetAttributeValue(ATTR_NAME);
}

char* KeywordModel::GetAlso(void) const {
    char* also = StringRightTrim(GetAttributeValue(ATTR_ALSO), NULL);
    return also;
}

/**
 * The attribute value of ATTR_APPEAR.
 * If the value is empty, the value of GetName() will be returned.
 */
char* KeywordModel::GetAppear(void) const {
    char* appear = GetAttributeValue(ATTR_APPEAR);
    if(appear == NULL) {
        appear = GetName();
    }
    return appear;
}

char* KeywordModel::GetKindString(void) const {
        return GetKeywordKindString(xmlElement);
}

KeywordKind KeywordModel::GetKind(void) const {
    return GetKeywordKind(xmlElement);
}

char* KeywordModel::GetMinValue(void) const{
    return GetAttributeValue(ATTR_MINVALUE);
}

char* KeywordModel::GetMaxValue(void) const{
    return GetAttributeValue(ATTR_MAXVALUE);
}

char* KeywordModel::GetAttributeValue(const char* attrName) const{
    return GetAttributeString(xmlElement, attrName);
}

bool KeywordModel::IsUndefined(void) const{
    if(GetName() != NULL) {
        return strcmp(GetName(), ATTR_VALUE_UNDEFINED) == 0;
    }
    return false;
}

bool KeywordModel::IsInputRequired(void) const {
    char* inputValue = GetAttributeValue(ATTR_GUI_INPUT);
    if(inputValue != NULL){
        if(strcmp(inputValue, ATTR_VALUE_REQUIRED) == 0)
            return true;
        else{
            if(GetKind()==KEYWORD_KIND_DIR){
                wxString tdir=StringUtil::CharPointer2String(inputValue);
                if(!wxFileName::IsDirWritable(tdir)){
                    wxMessageBox(tdir+wxT(" isn't writable!"));
                    return true;
                }
            }
            return false;
        }
    }
    return false;
    //return true;
}

XmlElement* KeywordModel::GetXmlElement(void) {
    return xmlElement;
}

LogicData* KeywordModel::GetRequireData(void) {
    return &requireData;
}

LogicData* KeywordModel::GetExclusiveData(void) {
    return &exclusiveData;
}

/**
 * When the state of its UI is changed, call this function to inform other keywords
 * to change their states accordingly.
 * Or when the data in this model has been changed, inform its UI to change.
 */
void KeywordModel::NotifySelection(void) {
    if(GetKind()== KEYWORD_KIND_SELECT) {
        if(changeListener != NULL) {
            //update UI
            changeListener->OnSelected();
        }
    }else {
        parentModule->UpdateSelection(this);
    }
        if(exclusiveData.values != NULL) {
                NotifyExclusive();
        }
}

/**
 * When the given keywordmodel has been changed,
 * update this state accordingly due to REQUIRE.
 */
void KeywordModel::ReceiveSelection(const KeywordModel* updatedKeyword) {
    int i = 0;
    KeywordModel* requiredModel = NULL;
    bool isSelected, decision = false;
        if(NULL == updatedKeyword) {
        return;
        }
        if(NULL != requireData.values) {
                for(i = 0; i < requireData.count; i++) {
                        requiredModel = parentModule->GetTopModuleProperty()->GetKeywordModel(requireData.values[i]);
                        if(NULL == requiredModel) continue;
                        isSelected = requiredModel->IsSelected();
                        if(requireData.ope == ENUM_LOGIC_AND) {
                                if(!isSelected) {
                                        /* if the logic operation is AND
                                                when we found one of its required keyword is not selected
                                                this keyword should not be enabled */
                                        break;
                                }else {
                                        if(i == requireData.count - 1) {
                                                /* only when all its required keywords are selected,
                                                        it could be enabled */
                                                decision = true;
                                        }
                                }
                        }else{
                                if(isSelected) {
                                        /* if the logic operation is OR
                                                when we found one of its required keyword is selected
                                                this keyword should be enabled */
                                        decision = true;
                                        break;
                                }
                        }
                }
                if(decision != IsEnabled()) {
                        SetEnabled(decision, true);
                }
        }
        if(NULL != GetChildModule()) {
                GetChildModule()->UpdateSelection(updatedKeyword);
        }
}

/**
 * If this keywordmodel is selected, notify otherwise keywordmodels,
 * due to EXCLUSIVE.
 */
void KeywordModel::NotifyExclusive(void) {
    int i = 0;
    KeywordKind kind;
    KeywordModel* exclusiveModel = NULL;

    if(NULL == exclusiveData.values)
        return;

    for(i = 0; i < exclusiveData.count; i++) {
                exclusiveModel = parentModule->GetTopModuleProperty()->GetKeywordModel(exclusiveData.values[i]);
        if(NULL != exclusiveModel) {
                        kind = exclusiveModel->GetKind();
                        switch(kind) {
                                case KEYWORD_KIND_SINGLE:
                    /* If other keywordmodel is SIGNLE,
                        uncheck it if this keywormodel has been selected. */
                                        if(IsSelected()) {
                            exclusiveModel->SetSelected(!IsSelected(), true);
                        }
                        break;
                            case KEYWORD_KIND_INTS:
                                case KEYWORD_KIND_INTS_COMPUTED:
                                case KEYWORD_KIND_REALS:
                                case KEYWORD_KIND_REALS_COMPUTED:
                    /* If other keywordmodels are ARRAY-like */
                                        if(this->GetKind() == KEYWORD_KIND_SINGLE) {
                        /* enable or not as their enable states can be controlled by
                            selecting this keywordmodel */
                                                exclusiveModel->SetEnabled(!IsSelected(), true);
                                        }else{
                        /* clear their data */
                                                exclusiveModel->ClearData(true);
                                        }
                                        break;
                            default:
                                        exclusiveModel->SetEnabled(!IsSelected(), true);
                        }
        }
    }
}

/**
 * How many columns will be occupied by this model.
 */
int KeywordModel::GetColumnSize(void) const {
        int size = 1;
        KeywordKind kind = GetKind();
        if(kind == KEYWORD_KIND_STRING || kind == KEYWORD_KIND_STRINGS || kind == KEYWORD_KIND_CUSTOM) {
        // need more space for input
                size = 2;
        }else if( kind == KEYWORD_KIND_INTS || kind == KEYWORD_KIND_INTS_COMPUTED ||kind == KEYWORD_KIND_REALS || kind == KEYWORD_KIND_REALS_COMPUTED) {
        if(!GetAttributeInt(&size, xmlElement, ATTR_SIZE))  // not specify the size attribute
                        size = 3;
                if(size > 4)
            size = 4;
        else
            size = 2;
        }
        return size;
}

/**
 * How many rows will be occupied by this model.
 */
int KeywordModel::GetRowSize(void) const {
        int size = 1;
        bool succ = false;
        KeywordKind kind = GetKind();
        switch(kind){
        case KEYWORD_KIND_INTS:
        case KEYWORD_KIND_INTS_COMPUTED:
        case KEYWORD_KIND_INTS_LOOKUP:
        case KEYWORD_KIND_REALS:
        case KEYWORD_KIND_REALS_COMPUTED:
        case KEYWORD_KIND_REALS_LOOKUP:
        case KEYWORD_KIND_STRINGS_COMPUTED:
                succ = GetAttributeInt(&size, xmlElement, ATTR_WINDOW_SIZE);
                if(!succ) size = 1;
                break;
        case KEYWORD_KIND_STRINGS_LOOKUP:
        case KEYWORD_KIND_STRINGS:
                succ = GetAttributeInt(&size, xmlElement, ATTR_WINDOW_SIZE);
                if(!succ && !GetAttributeInt(&size, xmlElement, ATTR_SIZE)) size = 3;
                break;
        case KEYWORD_KIND_CUSTOM:
        case KEYWORD_KIND_UNKNOWN:
                size = 3;
                break;
        default:
                break;
        }
        return size;
}

void KeywordModel::SetGroupRadio(bool isRadio) {
    isGroupRadio = isRadio;
}

bool KeywordModel::IsGroupRadio(void) const {
    return isGroupRadio;
}

void KeywordModel::DuplicateValue(KeywordModel* const oldKeywordModel) {
    SetEnabled(oldKeywordModel->IsEnabled(), false);
    if(GetChildModule() != NULL) {
        GetChildModule()->DuplicateValue(oldKeywordModel->GetChildModule());
    }
    SetValue(oldKeywordModel->GetValue());
}
