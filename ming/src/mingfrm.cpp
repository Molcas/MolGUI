/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "mingfrm.h"
#include "stringutil.h"
#include "tools.h"
#include "fileutil.h"
#include "envutil.h"
#include "mingapp.h"

#include "resource.h"

int CTRL_ID_BTN_SETTINGS = wxNewId();

BEGIN_EVENT_TABLE(MingFrm, wxFrame)
        EVT_CLOSE(MingFrm::OnClose)

        EVT_BUTTON(wxID_OPEN, MingFrm::OnFileOpen)
        EVT_BUTTON(wxID_CLOSE, MingFrm::OnFileClose)
        EVT_BUTTON(wxID_SAVE, MingFrm::OnFileSave)
        EVT_BUTTON(wxID_SAVEAS, MingFrm::OnFileSaveAs)
        EVT_BUTTON(wxID_EXIT, MingFrm::OnFileExit)

        EVT_BUTTON(CTRL_ID_BTN_SETTINGS, MingFrm::OnEnvSettings)
        EVT_BUTTON(wxID_HELP, MingFrm::OnHelp)
END_EVENT_TABLE()

MingFrm::MingFrm(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, style) {
        wxBitmap bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK+ wxT("app.png"), wxBITMAP_TYPE_PNG);
        wxIcon frameIcon;
        frameIcon.CopyFromBitmap(bmp);
        SetIcon(frameIcon);
        m_pMingPnl = NULL;
        m_helpCtrl = NULL;
}

MingFrm::~MingFrm() {
        if(m_helpCtrl) delete m_helpCtrl;
}

void MingFrm::Init(void) {
//begin : hurukun : 27/11/2009
        SetSize(1024, 755);
        SetMinSize(wxSize(1000,755));
//end   : hurukun :27/11/2009
        CreateControls();
        CreateStatusBar(2, wxST_SIZEGRIP);
    int widths[2] = {-3, 100};
    SetStatusWidths(2, widths);
    SetStatusText(wxGetApp().GetAppName(), 1);
        Center();
}

void MingFrm::CreateControls(void) {
        wxBoxSizer* bsFrame = new wxBoxSizer(wxVERTICAL);
        this->SetSizer(bsFrame);
        int btnHeight = 35;
        int btnWidth = 90;
        if(platform::macosx) {
                btnHeight = 30;
                btnWidth = 80;
        }
        wxSize btnSize(btnWidth, btnHeight);
        wxBoxSizer* bsTopLine = new wxBoxSizer(wxHORIZONTAL);

        int fileIds[] = {wxID_OPEN, wxID_CLOSE, wxID_SAVE, wxID_SAVEAS, CTRL_ID_BTN_SETTINGS, wxID_EXIT, wxID_HELP};
        wxString fileLbs[] = {wxEmptyString, wxEmptyString, wxEmptyString, wxT("Save As..."), wxT("Se&ttings"), wxEmptyString, wxEmptyString};
        wxString fileToolTips[] = {wxT("Open file"), wxT("Close file"), wxT("Save file"), wxT("Save file as"), wxT("Set MOLCAS project properties"), wxT("Quit"), wxT("Help")};
        bsTopLine->AddSpacer(5);
        wxButton* btn;
        for (int i = 0; i < 7; i++) {
                btn = new wxButton(this, fileIds[i], fileLbs[i], wxDefaultPosition, btnSize);
                btn->SetToolTip(fileToolTips[i]);
                if(i == 6) {
                        bsTopLine->AddStretchSpacer(1);
                }
                bsTopLine->Add(btn, 0, wxALIGN_CENTER_HORIZONTAL, 2);
        }
        bsTopLine->AddSpacer(5);

        m_pMingPnl = new MingPnl(this, wxID_ANY,wxEmptyString,true);
        m_pMingPnl->SetFrame(this);

        bsFrame->AddSpacer(5);
        bsFrame->Add(bsTopLine, 0, wxEXPAND | wxALL, 2);
        bsFrame->Add(m_pMingPnl, 1, wxEXPAND | wxALL, 2);

        if(platform::windows) {
                //on Windows, the default colour of wxFrame is different with wxDialog
                this->SetBackgroundColour(m_pMingPnl->GetBackgroundColour());
        }
}

void MingFrm::OnClose(wxCloseEvent& event) {
        DoFileExit();
}

void MingFrm::DoFileOpen(wxString openFileName) {
        m_pMingPnl->DoFileOpen(openFileName);
}

void MingFrm::OnFileOpen(wxCommandEvent& event) {
        wxString filePath = wxEmptyString;
        wxString defaultDir = wxEmptyString;
        if(!m_pMingPnl->GetFileName().IsEmpty()) {
                defaultDir = wxFileName(m_pMingPnl->GetFileName()).GetPath();;
        }
        wxFileDialog fileDialog(this, wxT("Open File"), defaultDir, wxEmptyString, wxT("MOLCAS Input files (*.input)|*.input|All files (*)|*"), wxFD_OPEN);
        if (fileDialog.ShowModal() == wxID_OK) {
                filePath = fileDialog.GetPath();
                m_pMingPnl->DoFileOpen(filePath);
                m_pMingPnl->ClearHistoryDir();
        }
}

void MingFrm::OnFileClose(wxCommandEvent& event) {
        m_pMingPnl->DoFileClose();
}

void MingFrm::OnFileSave(wxCommandEvent& event) {
        m_pMingPnl->DoSaveInputFile(m_pMingPnl->GetFileName());
}

void MingFrm::OnFileSaveAs(wxCommandEvent& event) {
        m_pMingPnl->DoSaveInputFile(wxEmptyString);
}

void MingFrm::OnFileExit(wxCommandEvent& event) {
        DoFileExit();
}

void MingFrm::DoFileExit(void) {
        if(m_pMingPnl->OnBtnClose()) {
                Destroy();
        }
}

void MingFrm::OnEnvSettings(wxCommandEvent& event) {
        m_pMingPnl->DoEnvSettings();
}

void MingFrm::OnHelp(wxCommandEvent& event) {
       //begin : hurukun 05/11/2009
        
       wxString filePath = EnvUtil::GetPrgResDir() + wxT("/doc/ming/ming.hhp");
        
       //end :   hurukun 05/11/2009
        if(m_helpCtrl == NULL) {
                m_helpCtrl = new wxHtmlHelpController;
              //begin : hurukun 05/11/2009
                //if(!m_helpCtrl->AddBook(wxFileName(EnvUtil::GetBaseDir() + wxT("/doc/ming/ming.hhp")))) {
                 if(!m_helpCtrl->AddBook(wxFileName(filePath))) {
              //end   : hurukun 05/11/2009
                // if use zip file, please uncomment the follwing line, and comment the above line.
                // and zip all files as simupac.zip
                //if(!m_helpCtrl->Initialize(EnvUtil::GetHomeDir() + wxT("/doc/simupac"))) {
                        wxLogError(wxT("Cannot initialize the help system."));
                }
        }
        m_helpCtrl->DisplayContents();
}
