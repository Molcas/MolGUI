/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/filename.h>
#include <wx/textfile.h>
#include "mxyzfile.h"
#include "mingutil.h"
#include "cstringutil.h"
#include "tools.h"
#include "envutil.h"
#include "stringutil.h"
#include "fileutil.h"
#include "simuprjdef.h"

static wxString strMolcasDir = wxEmptyString;
static wxString strGvWindowsPath = wxEmptyString;

wxString MingUtil::GetMolcasHome(void) {
        wxString str(wxT("MOLCAS_HOME"));
        wxString value;
        if (wxGetEnv(str, &value)) {
                return value;
        }
        return wxEmptyString;
}

/**
 * Another method to get molcas home
 */
wxString MingUtil::GetMolcasDir(void) {
/*        if (strMolcasDir.IsEmpty()) {
                wxString molcas = wxT("MOLCAS has been found at");
                wxArrayString output, errors;
                wxExecute(MingUtil::GetMolcasCommand(), output, errors, wxEXEC_ASYNC);
                unsigned i, j;
                int index = wxNOT_FOUND;
                if (errors.Count() > 0) {
                    //Tools::ShowError(wxT("Sorry, cannot find MOLCAS!"));
                }
                for (i = 0; i < output.Count(); i++) {
                        //wxLogError(output[i]);
                        index = output[i].Find(molcas);
                        if (index != wxNOT_FOUND) {
                                molcas = output[i].Mid(index + molcas.Len());
                                j = 0;
                                while (j < molcas.Len() && molcas[j] == wxChar(' ')) {
                                        j++;
                                }
                                if (j != 0) molcas = molcas.Mid(j);
                                j = molcas.Len() - 1;
                                while (j >= 0 && molcas[j] == wxChar(' ')) {
                                        j--;
                                }
                                if (j != molcas.Len() - 1) molcas = molcas.Mid(0, j + 1);
                                strMolcasDir = molcas;
                                break;
                        }
                }
        }
        return strMolcasDir;*/
        return EnvUtil::GetMolcasDir();
}

wxString MingUtil::GetMolcasBinDir(void) {
        wxString home = GetMolcasHome();
        if (!home.IsEmpty()) {
                home += wxString(wxT("/bin/"));
        }
        return home;
}

wxString MingUtil::GetMolcasCommand(void) {
        return wxT("molcas ");
}

wxString MingUtil::GetGlobalEnvFile(void) {
        //        return EnvUtil::GetMolcasRCFile();
        return EnvUtil::GetWorkDir() + wxT("/molcasenv.rc");
}

wxString MingUtil::GetSavedDbFile(void) {    
    return EnvUtil::GetPrgResDir() + wxT("/res/ming/data/saved.db");
        
}

wxString MingUtil::GetMolcasBasisDir(void) {
    return EnvUtil::GetPrgResDir() + wxT("/res/ming/basis/");
        
}

wxString MingUtil::GetMolcasBasisWorkDir(void) {
    return EnvUtil::GetWorkDir() + wxT("/basis/");
}

wxString MingUtil::GetJobOutputDir(const wxString defaultJobPath)
{
     wxArrayString list;
     list.Add(wxT("*.*Orb"));
     list.Add(wxT("*.grid"));
     list.Add(wxT("*.molden"));
     for (int i=0; i<(int)list.GetCount(); i++) {
          wxString tmpStr = wxDir::FindFirst(defaultJobPath, list[i], wxDIR_DEFAULT);
          if (!tmpStr.IsEmpty()) {
               #if defined (__WINDOWS__)
               return tmpStr.BeforeLast(wxT('\\'));
               #else
               return tmpStr.BeforeLast(wxT('/'));
               #endif
          }
     }
     // std::cout << "GetJobOutputDir path: " << defaultJobPath << std::endl;
     return defaultJobPath;
}

wxString MingUtil::GetWorkTmpDir(void) {
    return EnvUtil::GetWorkDir() + wxT("/tmp/");
}

wxString MingUtil::GetPreviewFile(void) {
    return GetWorkTmpDir() + wxT("preview.input");
}

wxString MingUtil::GetMingIconFile(void) {
    return EnvUtil::GetPrgResDir() + platform::PathSep() + wxT("res") + platform::PathSep() + wxT("ming") + platform::PathSep() + wxT("mingicon.png");
        
}

void MingUtil::GenerateBasisSetWorkFiles(void) {
if(!platform::windows) {
        wxArrayString output;
        wxString shellPrefix = wxT("help basis ");
        shellPrefix = GetMolcasCommand() + shellPrefix;

        wxString elemSymbol = wxEmptyString;
        int index = 0;
        do {
                elemSymbol = StringUtil::CharPointer2String(AtomSymbols[index]);
                if (elemSymbol.Len() > 0) {
                        output.Clear();
                        // invoke shell program
//add by bao
#if (!defined (__WINDOWS__) && !defined(_WIN32) && !defined(_WIN32_))
                wxShell(shellPrefix + elemSymbol, output);
#endif
                        FileUtil::OutputStringLineFile(GetMolcasBasisWorkDir() + elemSymbol + wxT(".basis"), output);
                }
                index ++;
        } while (AtomSymbols[index] != NULL);
}
}

void MingUtil::GenerateBasisSetFiles(void) {
        char* workDir = NULL;
        char* resDir = NULL;

        //GenerateBasisSetWorkFiles();
        StringUtil::String2CharPointer(&workDir, GetMolcasBasisWorkDir());
        StringUtil::String2CharPointer(&resDir, GetMolcasBasisDir());
        MXYZFile::GenerateBasisSetFiles(workDir, resDir);
        if (workDir)
                free(workDir);
        if (resDir)
                free(resDir);
}
//add by bao
int MingUtil::GenerateSymmetryWorkFile(wxString inputFile, wxString outFile, double threshold) {
        /*wxArrayString output, error;
        wxString findsymCommand = GetMolcasBinDir() + wxT("findsym.exe");
        wxShell(wxT("cp ") + inputFile + wxT(" ") + GetPathPrefix() + wxT("findsym.inp"));
        if(!wxFileName::FileExists(findsymCommand)) {
            wxLogError(wxT("No such file exists: ") + findsymCommand);
        }else{
            wxExecute(findsymCommand, output, error);
        }*/

        char* inputFileName = NULL;
        //char* outputFileName = NULL;
        char what[8];
        //double thr;
        int rc = 0; //linput, lwhat;

        //thr = threshold;
        strcpy(what, "list");
        StringUtil::String2CharPointer(&inputFileName, inputFile);
        //linput = strlen(inputFileName) + 1;
        //lwhat = 8;
//add by bao
#if (!defined (__WINDOWS__) && !defined(_WIN32) && !defined(_WIN32_))/// tmp_changehurukun : 5/12/2009
 //findsym(inputFileName, &linput, what, &lwhat, &thr, &rc);
#endif
        if (inputFileName) free(inputFileName);

        return rc;
}

void MingUtil::ExecGvWindows(wxString fileName, wxString para) {
  /*      if (strGvWindowsPath.IsEmpty()) {
                wxString gvWinPathStr = EnvUtil::GetWorkDir() + wxT("/gvwinpath.dat");
                wxFileName gvWinPath(gvWinPathStr);
                if (!gvWinPath.FileExists()) {
                        wxFileDialog fileDialog(NULL, wxT("Select GV program"), wxEmptyString, wxEmptyString, wxT("MOLCAS GV program (*.exe)|*.exe|All files (*)|*"), wxFD_OPEN);
                        if (fileDialog.ShowModal() == wxID_OK) {
                                strGvWindowsPath = fileDialog.GetPath();
                                wxArrayString aryString;
                                aryString.Add(strGvWindowsPath);;
                                FileUtil::OutputStringLineFile(gvWinPathStr, aryString);
                        } else {
                                return;
                        }
                } else {
                        strGvWindowsPath = FileUtil::LoadStringLineFileStr(gvWinPathStr);
                }
 //               strGvWindowsPath = strGvWindowsPath.Trim();
 //               strGvWindowsPath = strGvWindowsPath.Trim(true);
                strGvWindowsPath += wxT(" ") ;
        }
*/
        strGvWindowsPath=EnvUtil::GetMolcasGVDir();

//wxMessageBox(strGvWindowsPath+fileName);
        wxExecute(strGvWindowsPath + wxT(" ")+fileName + wxT(" ") + para, wxEXEC_ASYNC);
        // std::cout << "MingUtil::ExecGvWindows: " << strGvWindowsPath << ";" << std::endl;
}


wxString MingUtil::GetHistoryDir(void) {
        return EnvUtil::GetHistoryDir() + wxT("/ming/");
}
