/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/file.h>
#include <wx/filename.h> 
#include <wx/splitter.h>
#include <wx/tooltip.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>

#include <wx/combobox.h>
#include "mingpnl.h"
#include "stringutil.h"
#include "tools.h"
#include "fileutil.h"
#include "envutil.h"
#include "mingutil.h"
#include "keywordconst.h"
#include "keywordparser.h"
#include "cstringutil.h"
#include "templatemanager.h"
#include "keyworddata.h"
#include "inputparser.h"
#include "fileeditdlg.h"
#include "slidingwindow.h"
#include "xmlfile.h"
#include "dlgmachinelist.h"
#include "resource.h"
#include "dlgtemplate.h"
#include "simuprjdef.h"
#include "manager.h"
#include "simuproject.h"

#include "manager.h"
#include "projectmanager.h"

#include <fstream>
#include <string>
#include <vector>
using namespace std;

enum {
//        CTRL_ID_BTN_SETTING = 800,
        CTRL_ID_BTN_PREVIEW,
        CTRL_ID_BTN_EDIT,

        CTRL_ID_BTN_ADD_COMPONENT = 1001,
        CTRL_ID_BTN_DUPLICATE_COMPONENT,
        CTRL_ID_BTN_DELETE_COMPONENT,
        CTRL_ID_BTN_CLEAR_COMPONENT,
        CTRL_ID_BTN_UNDO,
        CTRL_ID_BTN_REDO,

        //CTRL_ID_BTN_TEST,
        //CTRL_ID_TEXTCTRL_PROJECT,
        CTRL_ID_ABOUT,

        CTRL_ID_CKB_ADVANCED,
        CTRL_ID_CKB_EMPTY,
#ifdef __MING__
        CTRL_ID_CKB_FILEPATH,
#endif

        CTRL_ID_CKB_TOOLTIP,
        CTRL_ID_LBX_TEMPLATE,

        CTRL_ID_TGL_COMMAND_START = 1200,
        CTRL_ID_TGL_COMMAND_END = 1250,
        CTRL_ID_TGL_PROGRAM_START = 1251,
        CTRL_ID_TGL_PROGRAM_END = 1400,
        CTRL_ID_CKB_REMOTESUBMIT,
        CTRL_ID_COMB_HOSTNAME,
        CTRL_ID_BTN_NEWHOSTNAME
};
/*
int CTRL_ID_BTN_SETTING = wxNewId();
int CTRL_ID_BTN_PREVIEW = wxNewId();
int CTRL_ID_BTN_EDIT = wxNewId();

int CTRL_ID_BTN_ADD_COMPONENT = wxNewId();
int CTRL_ID_BTN_DUPLICATE_COMPONENT = wxNewId();
int CTRL_ID_BTN_DELETE_COMPONENT = wxNewId();
int CTRL_ID_BTN_CLEAR_COMPONENT = wxNewId();
int CTRL_ID_BTN_UNDO = wxNewId();
int CTRL_ID_BTN_REDO = wxNewId();

//int CTRL_ID_BTN_TEST = wxNewId();
//CTRL_ID_TEXTCTRL_PROJECT,
int CTRL_ID_ABOUT = wxNewId();

int CTRL_ID_CKB_ADVANCED = wxNewId();
int CTRL_ID_CKB_EMPTY = wxNewId();
int CTRL_ID_CKB_FILEPATH = wxNewId();
int CTRL_ID_CKB_TOOLTIP = wxNewId();
int CTRL_ID_LBX_TEMPLATE = wxNewId();

int CTRL_ID_TGL_COMMAND_START = wxNewId();
int CTRL_ID_TGL_COMMAND_00 = wxNewId();
int CTRL_ID_TGL_COMMAND_01 = wxNewId();
int CTRL_ID_TGL_COMMAND_02 = wxNewId();
int CTRL_ID_TGL_COMMAND_03 = wxNewId();
int CTRL_ID_TGL_COMMAND_04 = wxNewId();
int CTRL_ID_TGL_COMMAND_05 = wxNewId();
int CTRL_ID_TGL_COMMAND_06 = wxNewId();
int CTRL_ID_TGL_COMMAND_07 = wxNewId();
int CTRL_ID_TGL_COMMAND_08 = wxNewId();
int CTRL_ID_TGL_COMMAND_09 = wxNewId();
int CTRL_ID_TGL_COMMAND_10 = wxNewId();
int CTRL_ID_TGL_COMMAND_11 = wxNewId();
int CTRL_ID_TGL_COMMAND_12 = wxNewId();
int CTRL_ID_TGL_COMMAND_13 = wxNewId();
int CTRL_ID_TGL_COMMAND_14 = wxNewId();
int CTRL_ID_TGL_COMMAND_15 = wxNewId();
int CTRL_ID_TGL_COMMAND_16 = wxNewId();
int CTRL_ID_TGL_COMMAND_17 = wxNewId();
int CTRL_ID_TGL_COMMAND_18 = wxNewId();
int CTRL_ID_TGL_COMMAND_19 = wxNewId();
int CTRL_ID_TGL_COMMAND_20 = wxNewId();
int CTRL_ID_TGL_COMMAND_21 = wxNewId();
int CTRL_ID_TGL_COMMAND_22 = wxNewId();
int CTRL_ID_TGL_COMMAND_23 = wxNewId();
int CTRL_ID_TGL_COMMAND_24 = wxNewId();
int CTRL_ID_TGL_COMMAND_25 = wxNewId();
int CTRL_ID_TGL_COMMAND_26 = wxNewId();
int CTRL_ID_TGL_COMMAND_27 = wxNewId();
int CTRL_ID_TGL_COMMAND_28 = wxNewId();
int CTRL_ID_TGL_COMMAND_29 = wxNewId();

int CTRL_ID_TGL_COMMAND_END = wxNewId();
int CTRL_ID_TGL_PROGRAM_START = wxNewId();
int CTRL_ID_TGL_PROGRAM_END = wxNewId();
*/
static MingPnl* m_mingPnl = NULL;

BEGIN_EVENT_TABLE(MingPnl, wxPanel)
        EVT_BUTTON(CTRL_ID_BTN_ADD_COMPONENT, MingPnl::OnAddComponent)
        EVT_BUTTON(CTRL_ID_BTN_DUPLICATE_COMPONENT, MingPnl::OnDuplicateComponent)
        EVT_BUTTON(CTRL_ID_BTN_DELETE_COMPONENT, MingPnl::OnDeleteComponent)
        EVT_BUTTON(CTRL_ID_BTN_CLEAR_COMPONENT, MingPnl::OnClearComponent)
        EVT_BUTTON(CTRL_ID_BTN_UNDO, MingPnl::OnUndo)
        EVT_BUTTON(CTRL_ID_BTN_REDO, MingPnl::OnRedo)

        EVT_TOGGLEBUTTON(wxID_ANY, MingPnl::OnToggleButtons)
        EVT_CHECKBOX(CTRL_ID_CKB_ADVANCED, MingPnl::OnCheckbox)

        EVT_CHECKBOX(CTRL_ID_CKB_EMPTY, MingPnl::OnCheckbox)
#ifdef __MING__
        EVT_CHECKBOX(CTRL_ID_CKB_FILEPATH, MingPnl::OnCheckbox)
#endif
        EVT_CHECKBOX(CTRL_ID_CKB_TOOLTIP, MingPnl::OnCheckbox)
        EVT_LISTBOX(CTRL_ID_LBX_TEMPLATE, MingPnl::OnLbTempateClicked)
        EVT_LISTBOX_DCLICK(CTRL_ID_LBX_TEMPLATE, MingPnl::OnLbTemplateDClicked)

//        EVT_BUTTON(CTRL_ID_BTN_SETTING, MingPnl::OnSettingEnv)
        EVT_BUTTON(CTRL_ID_BTN_PREVIEW, MingPnl::OnPreviewScript)
        EVT_BUTTON(CTRL_ID_BTN_EDIT, MingPnl::OnEditScript)

        EVT_BUTTON(CTRL_ID_ABOUT, MingPnl::OnAbout)
        EVT_BUTTON(CTRL_ID_BTN_NEWHOSTNAME,MingPnl::OnNewHost)
END_EVENT_TABLE()

MingPnl* MingPnl::Get(void) {
        return m_mingPnl;
}

void MingPnl::Set(MingPnl* pPnl) {
        Free();
        m_mingPnl = pPnl;
}

void MingPnl::Free(void) {
        if(m_mingPnl != NULL) {
                delete m_mingPnl;
                m_mingPnl = NULL;
        }
}

MingPnl::MingPnl(wxWindow* parent, wxWindowID id,wxString title,bool isNewjob) : GenericPnl(parent, id) {
        m_frame = NULL;
        m_intSelectedComponent = 0;
        m_selectedCompType = MOL_COMP_TYPE_NULL;
        m_lastClickedTime = 0;
        m_pFlowCanvas = NULL;
        m_flowChartModel = NULL;
        m_pLbxTemplate = NULL;
        m_title=title;

        m_isShowAdvanced = false;
        m_isShowEmpty = true;
        m_isUseAbsolutePath = true; //the default option is use the absolute path
        m_fileName = wxEmptyString;
        m_hisFileCount = -1;
        m_isDirty=false;
        m_isNewJob=isNewjob;
//begin : hurukun : 27/11/2009
        m_pActiveDlg=NULL;
//end   : hurukun : 27/11/2009
        char* keywordFile = NULL;
        wxString keywordXml = EnvUtil::GetPrgResDir() + wxTRUNK_MING_DAT_PATH+wxT("keyword.xml");
        if(!wxFileName::FileExists(keywordXml)) {
                Tools::ShowError(wxT("No such file exists: ") + keywordXml);
        }else {
                StringUtil::String2CharPointer(&keywordFile, keywordXml);
                // std::cout << "keywordfile " << keywordFile << std::endl;
                GetKeywordData()->LoadKeyword(keywordFile);
                free(keywordFile);
        }
        wxToolTip::Enable(true);

        CreateControls();
        InitTemplate();
        //
        ClearHistoryDir();
        FileUtil::CreateDirectory(MingUtil::GetMolcasBasisWorkDir());
        FileUtil::CreateDirectory(MingUtil::GetWorkTmpDir());

//        m_additionalBtnLabels.Add(wxT("Se&ttings"));
//        m_additionalBtnIds.Add(CTRL_ID_BTN_SETTING);
        Set(this);
}

MingPnl::~MingPnl() {
        m_mingPnl = NULL;
//begin : hurukun : 23/11/2009
        m_pActiveDlg=NULL;
//end   : hurukun : 23/11/2009
}

void MingPnl::SetFrame(wxFrame* frame) {
        m_frame = frame;
}

long MingPnl::GetButtonStyle(void) {
        return BTN_SAVE | BTN_SUBMIT | BTN_CLOSE;
}


bool MingPnl::IsShowAdvanced(void) const {
        return m_isShowAdvanced;
}

bool MingPnl::IsShowEmpty(void) const {
        return m_isShowEmpty;
}

bool MingPnl::IsUseAbsolutePath(void) const {
        return m_isUseAbsolutePath;
}

void MingPnl::CreateControls(void) {
        int i = 0;
        wxWindow* btn;
        int btnHeight = 35;
        if(platform::macosx) {
                btnHeight = 30;
        }

        wxBoxSizer* bsCenter = new wxBoxSizer(wxHORIZONTAL);

        wxBoxSizer* bsMain = new wxBoxSizer(wxVERTICAL);
        this->SetSizer(bsMain);
        bsMain->Add(bsCenter,0, wxEXPAND | wxALL, 1);

        //m_cbxRemoteSubmit=new wxCheckBox(this,CTRL_ID_CKB_REMOTESUBMIT,wxT("Submit to Remote:"),wxDefaultPosition,wxDefaultSize);
        m_combHostname=new wxComboBox(this,CTRL_ID_COMB_HOSTNAME,wxEmptyString,wxDefaultPosition,wxDefaultSize);
        m_combHostname->SetWindowStyle(wxCB_DROPDOWN);
        wxButton *btnNewHostname=new wxButton(this,CTRL_ID_BTN_NEWHOSTNAME,wxT("Manage Hosts"),wxDefaultPosition,wxDefaultSize);
        wxBoxSizer *bsBottom=new wxBoxSizer(wxHORIZONTAL);
        //bsBottom->Add(m_cbxRemoteSubmit,0, wxALIGN_CENTER_VERTICAL|wxALL, 2);
        bsBottom->Add(m_combHostname,0, wxALL, 2);
        bsBottom->Add(btnNewHostname,0, wxALL, 2);
        bsMain->Add(bsBottom,0,wxEXPAND|wxALL,1);
        bsMain->Fit(this);


        SlidingWindow* swMain = new SlidingWindow(this, wxID_ANY, wxDefaultPosition, wxSize(350, 320));
        swMain->Init();
        wxArrayString swLabels;
        swLabels.Add(wxT("Commands"));
        swLabels.Add(wxT("Modules"));
        swLabels.Add(wxT("Templates"));
        swMain->AddPages(swLabels);
        InitPanel(swMain->GetPage(0), GetKeywordData()->GetCommandCount(), GetKeywordData()->GetCommands(), &m_commandBtns, CTRL_ID_TGL_COMMAND_START);
        InitPanel(swMain->GetPage(1), GetKeywordData()->GetModuleCount(), GetKeywordData()->GetModules(), &m_programBtns, CTRL_ID_TGL_PROGRAM_START);
        wxPanel* pnlTemplate = swMain->GetPage(2);
        pnlTemplate->SetSizer(new wxBoxSizer(wxVERTICAL));
        m_pLbxTemplate = new wxListBox(pnlTemplate, CTRL_ID_LBX_TEMPLATE, wxDefaultPosition, wxSize(200, 200));
        pnlTemplate->GetSizer()->Add(m_pLbxTemplate, 1, wxEXPAND | wxALL, 2);
        swMain->GetSizer()->SetMinSize(wxSize(380, 320));

        wxBoxSizer* pBsRight = new wxBoxSizer(wxVERTICAL);
        wxPanel* pPnlSw = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(500, 500), wxSUNKEN_BORDER);
        wxBoxSizer* pBsPnlSw = new wxBoxSizer(wxVERTICAL);
        pPnlSw->SetSizer(pBsPnlSw);
        wxScrolledWindow *pSwGroups = new wxScrolledWindow(pPnlSw, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL);
        pSwGroups->SetScrollbars(30, 30, 100, 100);
        pSwGroups->SetClientSize(600, 400);
        wxBoxSizer* pBsSwGroups = new wxBoxSizer(wxVERTICAL);
        pSwGroups->SetSizer(pBsSwGroups);
        pBsPnlSw->Add(pSwGroups, 1,  wxEXPAND | wxALL, 0);
        m_pFlowCanvas = new FlowChartCanvas(pSwGroups);
        pBsSwGroups->Add(m_pFlowCanvas, 1,  wxEXPAND | wxALL, 0);

        wxStaticBox *sbBox = new wxStaticBox(this, wxID_ANY, wxEmptyString);
        wxStaticBoxSizer* pBsPnlTools = new wxStaticBoxSizer(sbBox, wxVERTICAL);

        int editIds[] = {CTRL_ID_BTN_ADD_COMPONENT, CTRL_ID_BTN_DUPLICATE_COMPONENT, CTRL_ID_BTN_DELETE_COMPONENT, CTRL_ID_BTN_CLEAR_COMPONENT,
                                         CTRL_ID_BTN_UNDO,  CTRL_ID_BTN_REDO, CTRL_ID_BTN_PREVIEW, CTRL_ID_BTN_EDIT};
        wxString editLbs[] = {wxT("&Add"), wxT("Duplicate"), wxT("&Delete"), wxT("&Clear"), wxT("&Undo") , wxT("&Redo"),
                                                wxT("&Preview"), wxT("&Edit")};
        wxString editToolTips[] = {wxT("Add component"), wxT("Duplicate selected component"), wxT("Delete selected component"),
                                                wxT("Delete all components"), wxT("Undo"), wxT("Redo"), wxT("Preview input file"), wxT("Edit input file")};
        wxSize btnSize = wxSize(110, btnHeight);
        pBsPnlTools->AddSpacer(5);
        for (i = 0; i < 8; i++) {
                btn = new wxButton(this, editIds[i], editLbs[i], wxDefaultPosition, btnSize);
                btn->SetToolTip(editToolTips[i]);
                if(i == 4 || i == 6) {
                        pBsPnlTools->AddSpacer(10);
                }
                pBsPnlTools->Add(btn, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 1);
        }
        pBsPnlTools->AddStretchSpacer(1);

#ifndef __MING__
        int ckbIds[] = {CTRL_ID_CKB_ADVANCED, CTRL_ID_CKB_EMPTY, CTRL_ID_CKB_TOOLTIP};
    wxString ckbLbs[] = {wxT("Advanced"), wxT("Show empty"),  wxT("Tooltips")}; //delete the option of Absolute Path. The default option is to use the absolute path��
        wxString ckbToolTips[] = {wxT("Show advanced keywords"), wxT("Show keywords without values"), wxT("Enable tooltips")};
    bool ckbValues[] = {m_isShowAdvanced, m_isShowEmpty,true} ;
        wxCheckBox* ckb = NULL;
        for(i = 0; i < 3; i++) {
                ckb = new wxCheckBox(this, ckbIds[i], ckbLbs[i], wxDefaultPosition, wxSize(120, 30));
                ckb->SetToolTip(ckbToolTips[i]);
                ckb->SetValue(ckbValues[i]);
                pBsPnlTools->Add(ckb, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 2);
        }
#else
    int ckbIds[] = {CTRL_ID_CKB_ADVANCED, CTRL_ID_CKB_EMPTY, CTRL_ID_CKB_FILEPATH, CTRL_ID_CKB_TOOLTIP};
        wxString ckbLbs[] = {wxT("Advanced"), wxT("Show empty"), wxT("Absolute paths"), wxT("Tooltips")};
        wxString ckbToolTips[] = {wxT("Show advanced keywords"), wxT("Show keywords without values"), wxT("Use absolute file path in input"), wxT("Enable tooltips")};
        bool ckbValues[] = {m_isShowAdvanced, m_isShowEmpty, m_isUseAbsolutePath, true} ;
        wxCheckBox* ckb = NULL;
        for(i = 0; i < 4; i++) {
                ckb = new wxCheckBox(this, ckbIds[i], ckbLbs[i], wxDefaultPosition, wxSize(120, 30));
                ckb->SetToolTip(ckbToolTips[i]);
                ckb->SetValue(ckbValues[i]);
                pBsPnlTools->Add(ckb, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 2);
        }
#endif

        wxBitmap bitmap(MingUtil::GetMingIconFile(), wxBITMAP_TYPE_PNG);
        wxBitmapButton* logBtn = new wxBitmapButton(this, CTRL_ID_ABOUT, bitmap, wxDefaultPosition, wxSize(145,145), wxNO_BORDER);

        pBsRight->Add(logBtn, 0, wxEXPAND|wxALL, 1);
        pBsRight->Add(pBsPnlTools, 1, wxEXPAND|wxALL, 1);

        bsCenter->Add(swMain, 0, wxEXPAND | wxALL, 2);
        //bsCenter->Add(pSplitterWin, 1, wxEXPAND | wxALL, 5);
        bsCenter->Add(pPnlSw, 1, wxEXPAND | wxALL, 2);

        bsCenter->Add(pBsRight, 0, wxEXPAND | wxALL, 5);

        RefreshHostList();
        GetSizer()->Layout();
        SetMinSize(wxSize(1000, 650));

}

void MingPnl::InitPanel(wxPanel* parent, int count, XmlElement** data, WindowPtrArray* winArray, int ctrlIdStart) {
        int i = 0, rows, cols;
        wxToggleButton* tbn;
        wxString name;
        wxString appear;
        wxString help;
        wxString p_help;
        char* charName = NULL;
        char* level = NULL;
        wxGridSizer *gsProgram;
        int real_count;
        // real count (remove hidden)
        real_count = count;
        for (i = 0; i < count; i++) {
            level = GetAttributeString(data[i], ATTR_LEVEL);
            if (level != NULL && strcmp(level, LEVEL_HIDDEN) == 0)
                real_count--;
        }
        if (real_count > 20) {
                cols = 3;
        } else {
                cols = 3;
        }
        // rows = (int)((count * 1.0 / cols) + 0.5) * cols;
        // rows = wxRound(count/cols);
        rows = (int)((real_count+cols-1)/cols);
        gsProgram = new wxGridSizer(rows, cols, 2, 2);

        // std::cout << "Mingpnl count " << count << std::endl;
        for (i = 0; i < count; i++) {
                charName = GetAttributeString(data[i], ATTR_NAME);
                name = StringUtil::CharPointer2String(charName);
                appear = StringUtil::CharPointer2String(GetAttributeString(data[i], ATTR_APPEAR));
                level = GetAttributeString(data[i], ATTR_LEVEL);
                if (level != NULL && strcmp(level, LEVEL_HIDDEN) == 0)
                        continue;
                if (appear.IsEmpty()) {
                        appear = name;
                }

                // std::cout << "i " << i << std::endl;
                // std::cout << "charName " << charName << std::endl;
                // std::cout << "name " << name << std::endl;
                // std::cout << "appear " << appear << std::endl;
                // std::cout << "level " << level << std::endl;
                //New format keyword.xml by GRP for Ignacio
                // XmlElement** helpElems = GetSubElements(data[i], ELEM_HELP, &count);
                
                help = StringUtil::CharPointer2String(GetElementHelp(data[i]));
                p_help = StringUtil::CharPointer2String(GetElementP_Help(data[i]));
                // std::cout << "help: " << help << std::endl;
                // std::cout << "p_help: " << p_help << std::endl;
                tbn = new wxToggleButton(parent, ctrlIdStart + i, appear, wxDefaultPosition, wxDefaultSize);
                tbn->SetToolTip(help);
                //gsProgram->Add(tbn, 0, wxEXPAND|wxALIGN_CENTER_HORIZONTAL, 5);
                gsProgram->Add(tbn, 0, wxEXPAND, 5);
                winArray->Add((wxWindow*) tbn);

                if (strcmp(charName, ATTR_VALUE_GATEWAY) == 0) {
                        tbn->SetValue(true);
                        m_intSelectedComponent = i;
                        m_selectedCompType = MOL_COMP_TYPE_MODULE;
                }
        }

        parent->SetSizer(new wxBoxSizer(wxVERTICAL));
        parent->GetSizer()->Add(gsProgram, 0, wxEXPAND, 2);
        parent->GetSizer()->AddStretchSpacer(1);
        parent->Fit();
}

void MingPnl::InitTemplate(void) {
        char* templateFile = NULL;
        char** templates = NULL;
        int templateCount = 0;
        wxArrayString templateArray;
        
        wxString templateFileWX = EnvUtil::GetPrgResDir() + wxTRUNK_MING_DAT_PATH+wxT("inputs.tpl");
    
        if (!wxFileName::FileExists(templateFileWX)) {
                Tools::ShowError(wxT("No such template file exists: ") + templateFileWX);
        } else {
                StringUtil::String2CharPointer(&templateFile, templateFileWX);
                templates = TemplateManager::GetTemplateNames(templateFile, &templateCount);
                for (int i = 0; i < templateCount; i++) {
                        templateArray.Add(StringUtil::CharPointer2String(templates[i]));
                }
                m_pLbxTemplate->Append(templateArray);
                free(templateFile);
                FreeStringArray(templates, templateCount);
        }
        wxString        molcasRcFile=MingUtil::GetGlobalEnvFile();
        if(!wxFileExists(molcasRcFile)){
                wxFileOutputStream        ofs(molcasRcFile);//open the file for reading
                if(!ofs.IsOk())//fail to open the file
                        return;
                wxTextOutputStream        oftext(ofs);
//                if(!oftext.IsOk())
//                        return;
//                oftext<<wxT("*@ENV")<<endl;
//                oftext<<wxT(">>>>>>>>> export MOLCASMEM=220")<<endl;
//                oftext<<wxT("*@ENDENV")<<endl;
        }
}

bool MingPnl::OnButton(long style) {
        if(style == BTN_CLOSE) {
                return OnBtnClose();
        }else if(style == BTN_SAVE) {
                return OnBtnSave();
        }else if(style == BTN_RESET) {
                return OnBtnReset();
        }else if(style == BTN_SUBMIT) {
                return OnBtnSubmit();
//        }else if(style == CTRL_ID_BTN_SETTING) {
//                DoEnvSettings();
//                return false;
        }
        return true;
}

bool MingPnl::OnBtnClose(void) {
//wxMessageBox(wxT("MingPnl::OnBtnClose"));
        return SaveFileWithOption(false);
}

bool MingPnl::OnBtnSave(void) {
     if(DoSaveInputFile(GetFileName())){
            SaveSelectHost();
            return true;
     }else
        return false;
}

bool MingPnl::OnBtnReset(void) {
        return false;
}

bool MingPnl::OnBtnSubmit(void) {
    if(OnBtnSave()){
        if(m_combHostname->GetSelection()>0){
            if(SaveJobInfo())
                return true;
            else
                return false;
            }
            return true;

    }
        return false;
}

wxString MingPnl::GetFileName(void) const {
        return m_fileName;
}

wxString MingPnl::GetFilePath(void) {
        if (m_fileName.IsEmpty()) {
                return wxT("./");
        }
        return wxFileName(m_fileName).GetPathWithSep();
}

void MingPnl::OnAddComponent(wxCommandEvent& event) {
        DoAddFlowChartComponent(m_selectedCompType);
        m_isDirty=true;
}

void MingPnl::OnDuplicateComponent(wxCommandEvent& event) {
        ElementProperty* elemPropFocus = m_pFlowCanvas->GetElemPropWithFocus();
        ElementProperty* elemPropDup = NULL;
        int addedElemId = -1;

        if (elemPropFocus != NULL) {
                elemPropDup = m_flowChartModel->AddXmlElement(elemPropFocus->GetXmlElement(), elemPropFocus->GetInternalId());
                elemPropDup->DuplicateValue(elemPropFocus);
                addedElemId = elemPropDup->GetInternalId();
                if (addedElemId != -1) {
                        m_pFlowCanvas->SetShapeFocus(addedElemId);
                }
        }
}

void MingPnl::DoAddFlowChartComponent(MolCompType compType) {
        // -1 to add at the end, >=0 to add after the given element id.
        int elemId = m_pFlowCanvas->GetInternalIdWithFocus();
        int addedElemId = -1;
        XmlElement* elem = NULL;
        char* linkAnchorStr = NULL;
        ElementProperty* elemProp = NULL;
        if (compType == MOL_COMP_TYPE_MODULE) {
                elem = GetKeywordData()->GetModules()[m_intSelectedComponent];
        } else if (compType == MOL_COMP_TYPE_COMMAND) {
                elem = GetKeywordData()->GetCommands()[m_intSelectedComponent];
        }

        if (compType != MOL_COMP_TYPE_NULL && compType != MOL_COMP_TYPE_TEMPLATE && m_flowChartModel == NULL) {
                // as for template, it will also load a flowchartmodel
                m_flowChartModel = new FlowChartModel();
                m_flowChartModel->RegisterChangeListener(m_pFlowCanvas);
                m_pFlowCanvas->SetFlowChartModel(m_flowChartModel);
        }

        if (elem != NULL) {
                // not a template component
                elemProp = m_flowChartModel->AddXmlElement(elem, elemId);
                addedElemId = elemProp->GetInternalId();
                //if it is a pair command
                if (compType == MOL_COMP_TYPE_COMMAND) {
                        linkAnchorStr = GetAttributeString(elem, ATTR_LINKANCHOR);
                        if (linkAnchorStr != NULL && GetAttributeString(elem, ATTR_LINKVALUEINDEX) == NULL) {
                                elem = GetKeywordData()->GetCommand(linkAnchorStr);
                                elemProp = m_flowChartModel->AddXmlElement(elem, elemProp->GetInternalId());
                                addedElemId = elemProp->GetInternalId();
                        }
                }
                DoSaveHistoryFile();
        } else if (MOL_COMP_TYPE_TEMPLATE == compType) {
                addedElemId = DoAddTemplate(elemId);
        }

        if (elemId != -1 && addedElemId != -1) {
                m_pFlowCanvas->SetShapeFocus(addedElemId);
        }
}

void MingPnl::OnToggleButtons(wxCommandEvent& event) {
        int btnId = event.GetId();
        if (btnId >= CTRL_ID_TGL_COMMAND_START && btnId <= CTRL_ID_TGL_COMMAND_END) {
                btnId -= CTRL_ID_TGL_COMMAND_START;
                UnselectOtherToggleButtons(event.GetId(), &m_commandBtns);
                UnselectOtherToggleButtons(-1, &m_programBtns);
                if (m_pLbxTemplate->GetSelection() != wxNOT_FOUND) {
                        m_pLbxTemplate->Deselect(m_pLbxTemplate->GetSelection());
                }
                if (m_selectedCompType != MOL_COMP_TYPE_COMMAND) {
                        m_selectedCompType = MOL_COMP_TYPE_COMMAND;
                        m_lastClickedTime = wxGetLocalTimeMillis();
                } else {
                        if (m_intSelectedComponent == btnId) {
                                wxLongLong currTime = wxGetLocalTimeMillis() - m_lastClickedTime;
                                if (currTime.ToDouble() < 350) {
                                        DoAddFlowChartComponent(MOL_COMP_TYPE_COMMAND);
                                } else {
                                        m_lastClickedTime = wxGetLocalTimeMillis();
                                }
                        } else {
                                m_lastClickedTime = wxGetLocalTimeMillis();
                        }
                }
                m_intSelectedComponent = btnId;
        } else if (btnId >= CTRL_ID_TGL_PROGRAM_START && btnId <= CTRL_ID_TGL_PROGRAM_END) {
                btnId -= CTRL_ID_TGL_PROGRAM_START;
                UnselectOtherToggleButtons(event.GetId(), &m_programBtns);
                UnselectOtherToggleButtons(-1, &m_commandBtns);
                if (m_pLbxTemplate->GetSelection() != wxNOT_FOUND) {
                        m_pLbxTemplate->Deselect(m_pLbxTemplate->GetSelection());
                }
                if (m_selectedCompType != MOL_COMP_TYPE_MODULE) {
                        m_selectedCompType = MOL_COMP_TYPE_MODULE;
                        m_lastClickedTime = wxGetLocalTimeMillis();
                } else {
                        if (m_intSelectedComponent == btnId) {
                                wxLongLong currTime = wxGetLocalTimeMillis() - m_lastClickedTime;
                                if (currTime.ToDouble() < 350) {
                                        DoAddFlowChartComponent(MOL_COMP_TYPE_MODULE);
                                } else {
                                        m_lastClickedTime = wxGetLocalTimeMillis();
                                }
                        } else {
                                m_lastClickedTime = wxGetLocalTimeMillis();
                        }
                }
                m_intSelectedComponent = btnId;
        }
}

void MingPnl::UnselectOtherToggleButtons(int btnId, WindowPtrArray* winArray) {
        int i, count;
        wxToggleButton* tgn = NULL;
        count = (int)winArray->GetCount();
        for (i = 0 ; i < count; i++) {
                tgn = (wxToggleButton*)winArray->Item(i);
                tgn->SetValue(tgn->GetId() == btnId);
        }
}


void MingPnl::OnDeleteComponent(wxCommandEvent& event) {
        m_pFlowCanvas->DeleteSelectedComponents(false);
}

void MingPnl::OnClearComponent(wxCommandEvent& event) {
        DoClearComponent(true);
}

void MingPnl::DoClearComponent(bool saveHis) {
        m_pFlowCanvas->DeleteSelectedComponents(true);
//        UpdateFrameTitle(wxEmptyString);   //add by bao 070715
        if (m_flowChartModel) {
                m_flowChartModel->SetEnvSetting(NULL);
        }
        GetKeywordData()->SetUniqueAtoms(0);
        GetKeywordData()->SetSymmetryNumber(8);
        if (saveHis) DoSaveHistoryFile();
}

void MingPnl::OnCheckbox(wxCommandEvent& event) {
        int id = event.GetId();
        if (CTRL_ID_CKB_ADVANCED == id) {
                m_isShowAdvanced = event.IsChecked();
        } else if (CTRL_ID_CKB_EMPTY == id) {
                m_isShowEmpty = event.IsChecked();
        }
#ifdef __MING__
    else if (CTRL_ID_CKB_FILEPATH == id) {
        m_isUseAbsolutePath = event.IsChecked();
        }
#endif
  else if (CTRL_ID_CKB_TOOLTIP == id) {
                wxToolTip::Enable(event.IsChecked());
        }
}

void MingPnl::OnLbTempateClicked(wxCommandEvent& event) {
        UnselectOtherToggleButtons(-1, &m_commandBtns);
        UnselectOtherToggleButtons(-1, &m_programBtns);
        m_intSelectedComponent = -1;
        m_selectedCompType = MOL_COMP_TYPE_TEMPLATE;
}

void MingPnl::OnLbTemplateDClicked(wxCommandEvent& event) {
        int elemId = m_pFlowCanvas->GetInternalIdWithFocus();
        int addedElemId = DoAddTemplate(elemId);
        if (elemId != -1 && addedElemId != -1) {
                m_pFlowCanvas->SetShapeFocus(addedElemId);
        }
}

void MingPnl::UpdateFrameTitle(const wxString& fileName) {
        m_fileName = fileName;
        //
        if(m_frame) {
                wxString title = fileName;
                if (StringUtil::IsEmptyString(title)) {
                title = wxT("MING");
            } else {
                title += wxT(" - MING");
            }
                m_frame->SetTitle(title);
        }
}

int MingPnl::DoAddTemplate(int elemId) {
        int addedElemId = -1;
    
        wxString templateFileWX = EnvUtil::GetPrgResDir() +wxTRUNK_MING_DAT_PATH+wxT("inputs.tpl");
        
        char* templateFile = NULL;
        wxString selectedWX = m_pLbxTemplate->GetStringSelection();
        char* selectedStr = NULL;
        InputParser parser;
        FlowChartModel* tempModel = NULL;
        bool isAdded = true;
        if (selectedWX != wxEmptyString) {
                if (m_flowChartModel != NULL && !m_flowChartModel->IsEmpty()) {
                        wxMessageDialog dialog(this, wxT("Would you like to overwrite current flowchart?"), wxT("MING"),
                                                                   wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION);
                        switch (dialog.ShowModal()) {
                        case wxID_YES:
                                DoClearComponent(false);
                                elemId = -1;
                                break;
                        case wxID_NO:
                                break;
                        case wxID_CANCEL:
                                isAdded = false;
                                break;
                        default:
                                break;
                        }
                }

                if (isAdded) {
                        StringUtil::String2CharPointer(&selectedStr, selectedWX);
                        StringUtil::String2CharPointer(&templateFile, templateFileWX);
                        tempModel = parser.Parsing(templateFile, selectedStr, NULL);
                        if (tempModel) {
                                addedElemId = tempModel->GetLastInternalId();
                                if (m_flowChartModel == NULL) {
                                        m_flowChartModel = tempModel;
                                        m_pFlowCanvas->SetFlowChartModel(m_flowChartModel);
                                        m_flowChartModel->RegisterChangeListener(m_pFlowCanvas);
                                } else {
                                        m_flowChartModel->AddFlowChartModel(tempModel, elemId);
                                }
                        }
                        if (selectedStr)
                                free(selectedStr);
                        if (templateFile)
                                free(templateFile);

                        DoSaveHistoryFile();
                }
        }
        return addedElemId;
}

void MingPnl::DoSaveHistoryFile(void) {
        m_hisFileCount++;
        if (m_flowChartModel != NULL) {
                DoSaveInputFile(GetHistoryFileName(m_hisFileCount), true);
        }
        if (!wxFile::Exists(GetHistoryFileName(m_hisFileCount))) {
                wxFile file;
                file.Create(GetHistoryFileName(m_hisFileCount), true);
        }
        //FileUtil::ShowInfoMessage(wxT("Saving ") + GetHistoryFileName(hisFileCount));
        FindWindow(CTRL_ID_BTN_UNDO)->Enable(true);
}

void MingPnl::OnUndo(wxCommandEvent& event) {
        DoLoadPrevHistoryFile();
}

void MingPnl::OnRedo(wxCommandEvent& event) {
        DoLoadNextHistoryFile();
}

void MingPnl::DoLoadPrevHistoryFile(void) {
        if (m_hisFileCount > 0) {
                m_hisFileCount--;
                DoFileOpen(GetHistoryFileName(m_hisFileCount), true);
                //FileUtil::ShowInfoMessage(wxT("Prev ") + GetHistoryFileName(hisFileCount));
        }
        if (m_hisFileCount == 0) {
                FindWindow(CTRL_ID_BTN_UNDO)->Enable(false);
        }
        if (wxFile::Exists(GetHistoryFileName(1))) {
                FindWindow(CTRL_ID_BTN_REDO)->Enable(true);
        }
        m_isDirty=false;
}

void MingPnl::DoLoadNextHistoryFile(void) {
        //FileUtil::ShowInfoMessage(wxT("Next ") + GetHistoryFileName(hisFileCount + 1));
        if (wxFile::Exists(GetHistoryFileName(m_hisFileCount + 1))) {
                DoFileOpen(GetHistoryFileName(m_hisFileCount + 1), true);
                m_hisFileCount++;
                FindWindow(CTRL_ID_BTN_UNDO)->Enable(true);
        }

        if (!wxFile::Exists(GetHistoryFileName(m_hisFileCount + 1))) {
                FindWindow(CTRL_ID_BTN_REDO)->Enable(false);
        }
}

wxString MingPnl::GetHistoryFileName(int hisFileIndex) {
        return MingUtil::GetHistoryDir() + wxString::Format(wxT("%.3d"), hisFileIndex);
}

void MingPnl::ClearHistoryDir(void) {
        FileUtil::RemoveDirectory(MingUtil::GetHistoryDir(), false);
        FileUtil::CreateDirectory(MingUtil::GetHistoryDir());
        m_hisFileCount = -1;
        FindWindow(CTRL_ID_BTN_UNDO)->Enable(false);
        FindWindow(CTRL_ID_BTN_REDO)->Enable(false);
}

void MingPnl::DoFileOpen(wxString openFileName, bool isNotUpdateTitle) {
        char* fullFileName = NULL;
        char* availableXYZFileName = NULL;

        InputParser parser;
        StringUtil::String2CharPointer(&fullFileName, openFileName);
        wxString availableXYZFileNameWX = GetAvailableXYZFileName(openFileName);
        StringUtil::String2CharPointer(&availableXYZFileName, availableXYZFileNameWX);
        m_pFlowCanvas->DeleteAllComponents();
        if (m_flowChartModel)
                delete m_flowChartModel;
        m_flowChartModel = parser.Parsing(fullFileName, NULL, availableXYZFileName);
        if (m_flowChartModel != NULL) {
                m_pFlowCanvas->SetFlowChartModel(m_flowChartModel);
                m_flowChartModel->RegisterChangeListener(m_pFlowCanvas);
        } else {
                Tools::ShowInfo(wxT("The selected file is an empty input file."));
        }
        if (fullFileName)
                free(fullFileName);
        if (availableXYZFileName)
                free(availableXYZFileName);
        if (!isNotUpdateTitle) {
                UpdateFrameTitle(openFileName);
        }
        LoadSelectHost();
}

wxString MingPnl::GetAvailableXYZFileName(wxString& openFileName) {
        int i = 0;
        wxFileName fileName(openFileName);
        wxString availableFileName = wxEmptyString;
        do {
                availableFileName = fileName.GetPath() + wxFileName::GetPathSeparator() + fileName.GetName() + wxString::Format(wxT("_auto%d.xyz"), i);
                if (!wxFileName(availableFileName).FileExists()) {
                        break;
                }
                i++;
        } while (true);
        return availableFileName;
}

/*
void MingPnl::OnSettingEnv(wxCommandEvent& event) {
        ModuleViewerDlg viewDlg(this, wxID_ANY);
        ModuleProperty* newProperty = NULL;

        if (m_flowChartModel != NULL && m_flowChartModel->GetEnvSetting() != NULL) {
                viewDlg.SetModuleProperty(m_flowChartModel->GetEnvSetting(), true);
        }else {
                char* fileName = NULL;
                StringUtil::String2CharPointer(&fileName, MingUtil::GetGlobalEnvFile());
                newProperty = InputParser::CreateEnvModule(fileName);
                viewDlg.SetModuleProperty(newProperty, true, m_flowChartModel == NULL || m_flowChartModel->IsEmpty() || m_flowChartModel->GetEnvSetting() != NULL);
                if(fileName) free(fileName);
        }
    if(viewDlg.ShowModal() == wxID_OK) {
                if(newProperty != NULL) {
                        if(m_flowChartModel != NULL && !m_flowChartModel->IsEmpty() && m_flowChartModel->GetEnvSetting() == NULL) {
                                m_flowChartModel->SetEnvSetting(newProperty);
                        }else {
                                delete newProperty;
                        }
                }
        }else if(newProperty != NULL) {
                delete newProperty;
        }
}
*/

void MingPnl::OnPreviewScript(wxCommandEvent& event) {
        if(m_flowChartModel != NULL && !m_flowChartModel->IsEmpty()) {
                DoSaveInputFile(MingUtil::GetPreviewFile(), true);
                FileEditDlg fileEditDlg(this, wxID_ANY);
                fileEditDlg.SetNeedSubmitBtn(false);
                fileEditDlg.SetNeedSaveBtn(false);
                fileEditDlg.CreateControls();
                fileEditDlg.ReadFile(MingUtil::GetPreviewFile());
                fileEditDlg.ShowModal();
        }
}

void MingPnl::OnEditScript(wxCommandEvent& event) {
        bool needShow = true;
        if(m_flowChartModel == NULL || m_flowChartModel->IsEmpty()) {
                return;
        }
        if (!m_fileName.IsEmpty() && wxFile::Exists(m_fileName)) {
                if (wxNO == wxMessageBox(wxString(wxT("File ")) + m_fileName + wxString(wxT(" exists. Do you want to overwrite it?")),
                                                                 wxString(wxT("MING")), wxYES_NO  | wxICON_QUESTION)) {
                        needShow = false;
                }
        }
        if (needShow) {
                DoSaveInputFile(m_fileName);
                if (!m_fileName.IsEmpty()) {
                        FileEditDlg fileEditDlg(this, wxID_ANY);
                        fileEditDlg.SetNeedSubmitBtn(true);
                        fileEditDlg.SetNeedSaveBtn(true);
                        fileEditDlg.CreateControls();
                        fileEditDlg.ReadFile(m_fileName);
                        SimuProject*        pP=Manager::Get()->GetProjectManager()->GetActiveProject();
                        switch (fileEditDlg.ShowModal()) {
                        case wxID_SAVE:
                                DoFileOpen(m_fileName);
                                DoSaveHistoryFile();
                                break;
                        case wxID_APPLY:
                                ////DoSubmitInputFile();
//wxMessageBox(wxT("MingPnl::OnEditScript"));
                                Manager::Get()->GetProjectManager()->SubmitJob(pP,m_title,false);
                                ((GenericDlg*)GetParent())->Close();
                                break;
                        default:
                                break;
                        }
                }
        }
}

void MingPnl::DoFileClose(void) {
        if (SaveFileWithOption(true)) {
                DoClearComponent(true);
        }
}

bool MingPnl::SaveFileWithOption(bool isOverwrite) {
bool isContinue = true;
        if (m_flowChartModel != NULL) {
                wxString msg = wxEmptyString;
                if (isOverwrite) {
                //begin : hurukun : 24/11/2009
                //        if (m_fileName.IsEmpty()) {

        //                        return DoSaveInputFile(m_fileName);
                //        }
                //        msg = wxT("Do you want to overwrite the current input file?");
        //        } else {
                        msg = wxT("Do you want to save the changes?");
                //end   : hurukun : 24/11/2009
                }

                if(msg.IsEmpty()){
                        return true;
                }

                wxMessageDialog dialog(this, msg, wxT("MING"),
                                                           wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION);
                switch (dialog.ShowModal()) {
                case wxID_YES:
                        isContinue = DoSaveInputFile(m_fileName);
                        break;
                case wxID_NO:
                        if (isOverwrite) {
                                //begin : hurukun : 24/11/2009
                                //isContinue = DoSaveInputFile(wxEmptyString);
                                isContinue=true;
                                //end   : hurukun : 24/11/2009
                        }
                        break;
                case wxID_CANCEL:
                        isContinue = false;
                        break;
                default:
                        break;
                }
        }
        return isContinue;
}

bool MingPnl::DoSaveInputFile(wxString localFileName, bool isNotUpdateTitle) {
        bool isSaved = false;
        char* charFileName = NULL;
        //char* outputDir = NULL;
        wxString tempFileName = localFileName;
        if (m_flowChartModel == NULL) {
                //ShowStatus(wxT("Please create an input flowchart."));
        }else
        {

//wxMessageBox(tempFileName);
                if (tempFileName.IsEmpty())
                {
                        wxFileDialog dialog(this, wxT("Save File"), wxEmptyString, tempFileName,
                                                                wxT("MOLCAS Input files (*.input)|*.input"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
                        if (dialog.ShowModal() == wxID_OK)
                        {
                                tempFileName = dialog.GetPath();
                                if (!tempFileName.IsEmpty())
                                {
                                        if(platform::macosx && StringUtil::EndsWith(tempFileName, wxT(".input."), true))
                                        {
                                                tempFileName = tempFileName.Mid(0, tempFileName.Len() - 1);
                                        }else if(!StringUtil::EndsWith(tempFileName, wxT(".input"), true))
                                        {
                                                tempFileName += wxT(".input");
                                        }
                                }
                        } else
                        {
                                return false;
                        }
                }
                if (!tempFileName.IsEmpty())
                {
                        /*if (flowChartModel->GetEnvSetting() != NULL) {
                                //set default molcas_output as the same directory of this input.
                                if (!isNotUpdateTitle && flowChartModel->GetEnvSetting()->GetOutputDir() == NULL) {
                                        FileUtil::String2CharPointer(&outputDir, wxFileName(tempFileName).GetPath());
                                        flowChartModel->GetEnvSetting()->SetOutputDir(outputDir);
                                        free(outputDir);
                                }
                        }*/
//wxMessageBox(tempFileName);
                        if(tempFileName.Right(MINPUT_FILE_SUFFIX_LEN).IsSameAs(wxMINPUT_FILE_SUFFIX)){
                                if(!wxDirExists(tempFileName.BeforeLast(platform::PathSep().GetChar(0))))
                                        FileUtil::CreateDirectory(tempFileName.BeforeLast(platform::PathSep().GetChar(0)));
                        }
                        StringUtil::String2CharPointer(&charFileName, tempFileName);

                        vector<string> temp;

                        if (!m_isNewJob) {

                                ifstream in(charFileName);

                                string s;

                                while (getline(in, s)) {

                                        if (0 == s.compare("Constraints")){

                                                temp.push_back(s);

                                                while(getline(in, s)) {

                                                        if (0 != s.compare("End of constraints")){

                                                                temp.push_back(s);

                                                        } else {

                                                                break;
                                                        }
                                                }
                                                temp.push_back("End of constraints");

                                                break;
                                        }
                                }

                                in.close();

                        } else{

                                temp.push_back("Constraints");

                                wxString name;

                                name = Manager::Get()->GetProjectManager()->GetActiveProject()->GetResultDir(name);

                                name.RemoveLast();

                                wxString simName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".sim");

                                wxString xyzName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".xyz");

                                const int lennum = 2;
                                const int angnum = 3;
                                const int dihnum = 4;

                                int len = 0;
                                int ang = 0;
                                int dih = 0;

                                vector<string> v;

                                ifstream in(xyzName.ToAscii());

                                int count = 0;

                                in >> count;

                                string s;

                                getline(in, s);
                                getline(in, s);

                                v.push_back("");

                                for (int i=0; i<count; ++i) {

                                        in >> s;
                                        v.push_back(s);
                                        getline(in, s);

                                }
                                in.close();

                                in.open(simName.ToAscii());

                                vector<string> vec;

                                int f = 0;

                                while(getline(in, s)) {

                                        if (0 == s.compare("$BEGIN CONSTRAINTS")) {



                                                while (in >> s){

                                                        if (0 == s.compare("$END")) break;

                                                        string elem;

                                                        string value;

                                                        int num;

                                                        char a[20];

                                                        float res;

                                                        switch(s.at(0)) {

                                                        case 'L':
                                                                len++;

                                                                sprintf(a, "%d", len);

                                                                elem += "r";

                                                                elem += a;

                                                                elem += " = Bond ";

                                                                for (int i=0; i<lennum; ++i) {

                                                                        in >> num;

                                                                        elem += v[num];
                                                                        sprintf(a, "%d", num);
                                                                        elem += a;
                                                                        elem += " ";
                                                                }


                                                                sprintf(a, "%d", len);

                                                                value += "r";

                                                                value += a;

                                                                value += " = ";

                                                                in >> res;
                                                                sprintf(a, "%0.4f", res);

                                                                value += a;

                                                                vec.push_back(value);

                                                                f = 1;

                                                                break;

                                                        case 'A':
                                                                ang++;

                                                                sprintf(a, "%d", ang);

                                                                elem += "a";

                                                                elem += a;

                                                                elem += " = Angle ";

                                                                for (int i=0; i<angnum; ++i) {

                                                                        in >> num;

                                                                        elem += v[num];
                                                                        sprintf(a, "%d", num);
                                                                        elem += a;
                                                                        elem += " ";
                                                                }


                                                                sprintf(a, "%d", ang);

                                                                value += "a";

                                                                value += a;
                                                                value += " = ";
                                                                in >> res;
                                                                sprintf(a, "%0.4f", res);

                                                                value += a;
                                                                vec.push_back(value);

                                                                f = 1;

                                                                break;

                                                        case 'D':
                                                                dih++;

                                                                sprintf(a, "%d", dih);

                                                                elem += "d";

                                                                elem += a;

                                                                elem += " = Dihedral ";

                                                                for (int i=0; i<dihnum; ++i) {

                                                                        in >> num;

                                                                        elem += v[num];
                                                                        sprintf(a, "%d", num);
                                                                        elem += a;
                                                                        elem += " ";
                                                                }

                                                                sprintf(a, "%d", dih);

                                                                value += "d";

                                                                value += a;
                                                                value += " = ";
                                                                in >> res;
                                                                sprintf(a, "%0.4f", res);

                                                                value += a;
                                                                vec.push_back(value);

                                                                f = 1;

                                                                break;

                                                        default :
                                                                break;
                                                        }
                                                        temp.push_back(elem);
                                                }
                                                if (1 == f) {
                                                        temp.push_back("value");
                                                        for (unsigned int i=0; i<vec.size(); ++i) {
                                                                temp.push_back(vec[i]);
                                                        }
                                                }
                                        }
                                }

                                if (1 == f) {
                                        temp.push_back("End of constraints");
                                } else {
                                        temp.pop_back();
                                }
                        }

                        m_flowChartModel->SaveAsInputFormat(charFileName);


                        ifstream in(charFileName);
                        string s;
                        vector<string> vs;
                        while (getline(in, s)) {
                                vs.push_back(s);
                                if (0 == s.compare(" &SLAPAF")) {
                                        getline(in, s);
                                        if(0 == s.compare("Constraints")) {
                                                for (; 0 != s.compare("End of constraints");getline(in, s));
                                        } else {
                                                vs.push_back(s);
                                        }
                                        for (unsigned int i=0; i<temp.size(); ++i) {
                                                vs.push_back(temp[i]);
                                        }

                                }
                        }

                        in.close();

                        ofstream out(charFileName);
                        for (unsigned int i=0; i<vs.size(); ++i) {
                                out << vs[i] << endl;
                        }
                        out.close();

                        if (charFileName) free(charFileName);
                        if (!isNotUpdateTitle)
                        {
                        //        UpdateFrameTitle(tempFileName);   //add by bao 070715
                                isSaved = true;

                                wxFileName currFile(m_fileName);
                                if (currFile.FileExists())
                                {
                                        m_currFileDateTime = currFile.GetModificationTime();
                                }
                        }
                }
        }

    return isSaved;
}

bool MingPnl::SaveFileWhenRelativePath(void) {
        if(!m_isUseAbsolutePath && m_fileName.IsEmpty()) {
                Tools::ShowInfo(wxT("As using relative file path, please save the flowchart first."));
                return DoSaveInputFile(m_fileName, false);
        }
        return true;
}

void MingPnl::DoEnvSettings(void) {
        ModuleViewerDlg viewDlg(this, wxID_ANY);
//begin : hurukun : 23/11/2009
        m_pActiveDlg=&viewDlg;
//end   : hurukun : 23/11/2009
        ModuleProperty* newProperty = NULL;

        if (m_flowChartModel != NULL && m_flowChartModel->GetEnvSetting() != NULL) {
                viewDlg.SetModuleProperty(m_flowChartModel->GetEnvSetting(), true);
        }else {
                char* fileName = NULL;
                StringUtil::String2CharPointer(&fileName, MingUtil::GetGlobalEnvFile());
                newProperty = InputParser::CreateEnvModule(fileName);
                viewDlg.SetModuleProperty(newProperty, true, m_flowChartModel == NULL || m_flowChartModel->IsEmpty() || m_flowChartModel->GetEnvSetting() != NULL);
                if(fileName) free(fileName);
        }

    if(viewDlg.ShowModal() == wxID_OK) {
                if(newProperty != NULL) {
                        if(m_flowChartModel != NULL && !m_flowChartModel->IsEmpty() && m_flowChartModel->GetEnvSetting() == NULL) {
                                m_flowChartModel->SetEnvSetting(newProperty);
                        }else {
                                delete newProperty;
                        }
                }
        }else if(newProperty != NULL) {
                delete newProperty;
        }
}
void MingPnl::OnAbout(wxCommandEvent& event) {
        AboutDlg dlg(this);
        dlg.ShowModal();
}

BEGIN_EVENT_TABLE(AboutDlg, wxDialog)
    EVT_BUTTON(wxID_CLOSE, AboutDlg::OnDialogClose)
    EVT_CLOSE(AboutDlg::OnClose)
END_EVENT_TABLE()

AboutDlg::AboutDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
                                  : wxDialog(parent, id, title, pos, size, style) {
        CreateControls();
}

AboutDlg::~AboutDlg() {
}

void AboutDlg::CreateControls(void) {
        wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);

        wxBitmap bitmap(MingUtil::GetMingIconFile(), wxBITMAP_TYPE_PNG);
        wxStaticBitmap* staticBitmap = new wxStaticBitmap(this, wxID_ANY, bitmap);

        wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnClose = new wxButton(this, wxID_CLOSE, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    bsBottom->AddStretchSpacer(1);
    bsBottom->Add(btnClose, 0, wxALL, 2);

    wxNotebook* pNbInfo = new wxNotebook(this, wxID_ANY);
    wxPanel* pPnlDes = new wxPanel(pNbInfo, wxID_ANY);
    pPnlDes->SetSizer(new wxBoxSizer(wxVERTICAL));
    wxTextCtrl* pTcDesc = new wxTextCtrl(pPnlDes, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(300,300), wxTE_MULTILINE|wxTE_READONLY);
        pPnlDes->GetSizer()->Add(pTcDesc, 1, wxEXPAND|wxALL, 2);
    wxPanel* pPnlInfo = new wxPanel(pNbInfo, wxID_ANY);
    pPnlInfo->SetSizer(new wxBoxSizer(wxVERTICAL));

           pNbInfo->AddPage(pPnlDes, wxT("Description"), true);
        pNbInfo->AddPage(pPnlInfo, wxT("Information"), false);

        bsTop->Add(staticBitmap, 0, wxALIGN_CENTER|wxALL, 2);
        bsTop->Add(pNbInfo, 1, wxEXPAND|wxALL, 2);
        bsTop->AddSpacer(3);
    bsTop->Add(bsBottom, 0, wxEXPAND | wxALL, 2);

    this->SetSizer(bsTop);
    SetSize(450, 450);
    Center();

    //
    
    pTcDesc->LoadFile(EnvUtil::GetPrgResDir() +wxTRUNK_MING_PATH+ wxT("desc.txt"));
    

    
    wxString info = FileUtil::LoadStringLineFileStr(EnvUtil::GetPrgResDir() + wxTRUNK_MING_PATH+wxT("info.txt"));
    
    wxStaticText* pSt = new wxStaticText(pPnlInfo, wxID_ANY, info, wxDefaultPosition, wxSize(300, 200));
    pPnlInfo->GetSizer()->AddSpacer(5);
    pPnlInfo->GetSizer()->Add(pSt, 1, wxEXPAND | wxALL, 5);
}

void AboutDlg::OnDialogClose(wxCommandEvent& event) {
    HideDialog();
}

void AboutDlg::OnClose(wxCloseEvent& event){
    HideDialog();
}

void AboutDlg::HideDialog(void) {
        if (IsModal()) {
                EndModal(wxID_CLOSE);
        } else {
                SetReturnCode(wxID_CLOSE);
                this->Show(false);
        }
}

void MingPnl::ReplaceScript(wxString scriptfile)
{
        if (!wxFileExists(scriptfile))return;
        wxString strLine;
        wxString strContent=wxEmptyString;
        wxFileInputStream fis(scriptfile);
        wxTextInputStream tis(fis);
        while(!fis.Eof())
        {
                strLine = tis.ReadLine();
                strLine.Trim();
                strLine.Trim(false);
                if(!strLine.IsEmpty()){
            strContent=strContent+strLine;
            strContent=strContent+wxT("\n");
        }
        }

    wxFileName finputfile(m_fileName);
    strContent.Replace(wxT("$inputfile"),finputfile.GetName());
    wxFileOutputStream fos(scriptfile);
    wxTextOutputStream tos(fos);
    tos<<strContent<<endl;



}

void MingPnl::RefreshHostList()
{
    MachineInfoArray machineinfoarray;
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
        MachineFile::LoadFile(fileName,machineinfoarray);
        int totalhost=machineinfoarray.GetCount();

        wxArrayString hostnameary;
        hostnameary.Add(wxT("Local"));
        for(int i=0;i<totalhost;i++)
        {
                        m_hostarray.Add(machineinfoarray[i].machineid);
                        //wxMessageBox(machineinfoarray[i].machineid);
                        hostnameary.Add(machineinfoarray[i].masterid+wxT(": ")+ machineinfoarray[i].shortname);
                        //wxMessageBox(machineinfoarray[i].shortname);
        }
        m_combHostname->Clear();
        m_combHostname->Append(hostnameary);
        m_combHostname->SetSelection(0);
}


void MingPnl::OnNewHost(wxCommandEvent &event)
{
//        DlgSSHInfo *sshinfo=new DlgSSHInfo(this);
//        sshinfo->ShowModal();
        DlgMachineList * machinelist=new DlgMachineList(this,wxID_ANY,wxT("Hosts List"));
        if(!machinelist->GetMachineList())return;
    machinelist->ShowModal();
        RefreshHostList();
}

void MingPnl::SaveSelectHost(){
        if(m_combHostname->GetSelection()>0){
        wxString fileName=GetFileName()+wxT(".host");
        if(wxFileExists(fileName))wxRemoveFile(fileName);
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
    tos << m_hostarray[m_combHostname->GetSelection()-1] << endl;
        }
}
void MingPnl::LoadSelectHost(){
    unsigned int i;
    wxString hostname=wxEmptyString;
        wxString fileName=GetFileName()+wxT(".host");
        if(!wxFileExists(fileName))return;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        hostname=tis.ReadLine();
    if(hostname.IsNull()||hostname.IsEmpty())return;
    for(i=0;i<m_hostarray.GetCount();i++){
        if(hostname.Cmp(m_hostarray[i])==0){
            m_combHostname->SetSelection(i+1);
            return;
        }
    }
    m_combHostname->SetSelection(0);
}

bool MingPnl::SaveJobInfo()
{
    MachineInfo minfo=MachineInfo();

    MachineInfoArray machineinfoarray;
    wxString fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("machine.conf");
        MachineFile::LoadFile(fileName,machineinfoarray);
        int totalhost=machineinfoarray.GetCount();

        wxString hostname=m_hostarray[m_combHostname->GetSelection()-1];
        wxString scriptOld,scriptNew;
        for(int i=0;i<totalhost;i++)
        {
                if(hostname.Cmp(machineinfoarray[i].machineid)==0){
        minfo.machineid=machineinfoarray[i].machineid;
                minfo.fullname=machineinfoarray[i].fullname;
                minfo.shortname=machineinfoarray[i].shortname;
                minfo.interprocessor=machineinfoarray[i].interprocessor;
                minfo.nodes=machineinfoarray[i].nodes;
                minfo.username=machineinfoarray[i].username;
                minfo.password=machineinfoarray[i].password;
                minfo.protocal=machineinfoarray[i].protocal;
                minfo.submitway=machineinfoarray[i].submitway;
                minfo.masterid=machineinfoarray[i].masterid;
                minfo.masterkey=machineinfoarray[i].masterkey;
                if(minfo.submitway.Cmp(wxT("One command with variables"))==0)
            minfo.remotecmd=machineinfoarray[i].remotecmd;
        else
            minfo.remotecmd=wxT("no use");
                minfo.remoteworkbase=machineinfoarray[i].remoteworkbase;
                if(minfo.submitway.Cmp(wxT("One command with variables"))==0)
            minfo.scriptfile=wxT("no use");
        else{
            minfo.scriptfile=GetFilePath()+machineinfoarray[i].scriptfile;

            if(minfo.protocal.Cmp(wxT("HTTP"))!=0){
            scriptNew=minfo.scriptfile;
            scriptOld=EnvUtil::GetUserDataDir() + platform::PathSep() + machineinfoarray[i].scriptfile;

            if(wxFileExists(scriptOld)){
                wxCopyFile(scriptOld,scriptNew,true);
            }else{
                wxMessageBox(wxT("Script file remote machine is not found!"));
                return false;
            }
            }
            break;
            }
                }
        }

        fileName=EnvUtil::GetUserDataDir() + platform::PathSep() + wxT("job.conf");
        //if(wxFileExists(fileName))wxRemoveFile(fileName);
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);

    tos << wxT("1") << endl;
    tos << m_fileName << endl;
    tos << minfo.fullname << endl;
    tos << minfo.shortname << endl;
    tos << minfo.username << endl;
    tos << minfo.password << endl;
    tos << minfo.remotecmd << endl;
    tos << minfo.scriptfile << endl;
    tos << minfo.protocal << endl;
    tos << minfo.submitway << endl;
    tos << minfo.remoteworkbase << endl;
    tos << minfo.masterid << endl;
    tos << minfo.masterkey << endl;
    tos << minfo.machineid << endl;

    if(minfo.protocal.Cmp(wxT("HTTP"))!=0){
        if(minfo.submitway.Cmp(wxT("One command with variables"))!=0){
            DlgTemplate *dlgtemplate=new DlgTemplate(this);
            dlgtemplate->AppendTemplate(minfo.scriptfile);
            if(dlgtemplate->ShowModal()==wxID_CANCEL)return false;
            ReplaceScript(minfo.scriptfile);

#if (defined (__WINDOWS__) || defined(_WIN32) || defined(_WIN32_))
wxString exeproc=EnvUtil::GetPrgRootDir()+wxT("\\modules\\dos2unix.exe \"")+minfo.scriptfile+wxT("\"");
//wxMessageBox(exeproc);
wxExecute(exeproc, wxEXEC_SYNC);
#endif

    }
        }
        return true;
}
