/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/statline.h>
#include <wx/notebook.h>

#include "mxyzfile.h"

#include "basissetdlg.h"
#include "stringutil.h"
#include "tools.h"
#include "mingutil.h"
#include "cstringutil.h"

static wxString STR_GLOBAL(wxT("Global"));

BEGIN_EVENT_TABLE(BasisSetDlg, wxDialog)
    EVT_BUTTON(wxID_OK, BasisSetDlg::OnDialogOK)
    EVT_BUTTON(wxID_CANCEL, BasisSetDlg::OnDialogCancel)
    EVT_CLOSE(BasisSetDlg::OnClose)

    EVT_LISTBOX(CTRL_ID_LB_ELEMENTS, BasisSetDlg::OnLbElementClicked)
    EVT_LISTBOX(CTRL_ID_LB_RECOMMENDED, BasisSetDlg::OnListBoxClicked)
    EVT_LISTBOX(CTRL_ID_LB_OTHER, BasisSetDlg::OnListBoxClicked)
    EVT_LISTBOX_DCLICK(CTRL_ID_LB_RECOMMENDED, BasisSetDlg::OnListBoxDClicked)
    EVT_LISTBOX_DCLICK(CTRL_ID_LB_OTHER, BasisSetDlg::OnListBoxDClicked)
    EVT_LISTBOX(CTRL_ID_LB_SELECTED, BasisSetDlg::OnLbSelectedClicked)
    EVT_LISTBOX_DCLICK(CTRL_ID_LB_SELECTED, BasisSetDlg::OnLbSelectedDClicked)

    EVT_BUTTON(CTRL_ID_BTN_ADD, BasisSetDlg::OnAddBasisSet)
    EVT_BUTTON(CTRL_ID_BTN_DEL, BasisSetDlg::OnDelBasisSet)
    EVT_BUTTON(CTRL_ID_BTN_CUSTOM_ADD, BasisSetDlg::OnCustomBasisSet)
    EVT_BUTTON(CTRL_ID_BTN_CUSTOM_MOD, BasisSetDlg::OnCustomBasisSet)
END_EVENT_TABLE()

BasisSetDlg::BasisSetDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style )
//begin : hurukun : 20/11/2009
// : wxDialog(parent, id, title, pos, size, style) {
{
          wxDialog::Create(parent,id,title,pos,size,style);
//end   : hurukun : 20/11/2009
    pLbElements = NULL;
    pLbRecommended = NULL;
    pLbOther = NULL;
    pLbSelected = NULL;
    pTcModify = NULL;
    selectedList = CTRL_ID_LB_ELEMENTS;

    eleSymbols = NULL;
    eleCount = 0;
    eleSeletced = NULL;
    CreateControls();
}

BasisSetDlg::~BasisSetDlg() {
    if(eleSymbols) {
        //will be freed by listboxui.cpp
    }
    if(eleSeletced) {
        delete eleSeletced;
    }
}



void BasisSetDlg::CreateControls(void) {

    wxBoxSizer* bsElements = new wxBoxSizer(wxVERTICAL);
    wxStaticText* lbElements = new wxStaticText(this, wxID_ANY, wxT("Elements"));
           lbElements->SetLabel(wxT("Elements"));
    pLbElements = new wxListBox(this, CTRL_ID_LB_ELEMENTS, wxDefaultPosition, wxSize(150,300));
    bsElements->Add(lbElements, 0, wxEXPAND | wxALL, 2); //| wxALIGN_CENTER 
    bsElements->Add(pLbElements, 1, wxEXPAND |wxALL, 2); //
    //
    wxNotebook* pNbElemBasisSet = new wxNotebook(this, wxID_ANY);

        wxPanel* pPnlRecommended = new wxPanel(pNbElemBasisSet, wxID_ANY);
    wxBoxSizer* bsRecommended = new wxBoxSizer(wxVERTICAL);
    pPnlRecommended->SetSizer(bsRecommended);
    pLbRecommended = new wxListBox(pPnlRecommended, CTRL_ID_LB_RECOMMENDED, wxDefaultPosition, wxSize(200,300));
    bsRecommended->Add(pLbRecommended, 1, wxEXPAND|wxALL, 2);

        wxPanel* pPnlOther = new wxPanel(pNbElemBasisSet, wxID_ANY);
    wxBoxSizer* bsOther = new wxBoxSizer(wxVERTICAL);
    pPnlOther->SetSizer(bsOther);
    pLbOther = new wxListBox(pPnlOther, CTRL_ID_LB_OTHER, wxDefaultPosition, wxSize(200,300));
    bsOther->Add(pLbOther, 1, wxEXPAND|wxALL, 2);

        pNbElemBasisSet->AddPage(pPnlRecommended, wxT("Recommended"), true);
        pNbElemBasisSet->AddPage(pPnlOther, wxT("Others"), false);
        // std::cout << "plbother-> " << pLbOther->GetCount() << " plbrec " << pLbRecommended->GetCount() << std::endl;
    //
    wxBoxSizer* bsBtns = new wxBoxSizer(wxVERTICAL);
    wxButton* btnAdd = new wxButton(this, CTRL_ID_BTN_ADD, wxT(">>"), wxDefaultPosition, wxSize(70, 30));
    wxButton* btnDel = new wxButton(this, CTRL_ID_BTN_DEL, wxT("<<"), wxDefaultPosition, wxSize(70, 30));
    wxButton* btnCusAdd = new wxButton(this, CTRL_ID_BTN_CUSTOM_ADD, wxT("&Add"), wxDefaultPosition, wxSize(70, 30));
    wxButton* btnCusMod = new wxButton(this, CTRL_ID_BTN_CUSTOM_MOD, wxT("C&hange"), wxDefaultPosition, wxSize(70, 30));
    bsBtns->AddStretchSpacer(1);
    bsBtns->Add(btnAdd, 0,  wxALL, 2); //wxALIGN_CENTER_VERTICAL |
    bsBtns->Add(btnDel, 0,  wxALL, 2); //wxALIGN_CENTER_VERTICAL |
    bsBtns->AddStretchSpacer(1);
    bsBtns->Add(btnCusAdd, 0,  wxALL, 2); //wxALIGN_CENTER_VERTICAL |
    bsBtns->Add(btnCusMod, 0,  wxALL, 2); //wxALIGN_CENTER_VERTICAL |
    //
    wxBoxSizer* bsSelected = new wxBoxSizer(wxVERTICAL);
    wxStaticText* lbSelected = new wxStaticText(this, wxID_ANY, wxT("Selected Basis Sets"));
           lbSelected->SetLabel(wxT("Selected Basis Sets"));
    pLbSelected = new wxListBox(this, CTRL_ID_LB_SELECTED, wxDefaultPosition, wxSize(200,300));
    pTcModify = new wxTextCtrl(this, CTRL_ID_TC_MODIFY, wxEmptyString, wxDefaultPosition, wxSize(60, 30));

    bsSelected->Add(lbSelected, 0, wxEXPAND | wxALL, 2); //| wxALIGN_CENTER
    bsSelected->Add(pLbSelected, 1, wxEXPAND|wxALL, 2);
    bsSelected->Add(pTcModify, 0, wxEXPAND|wxALL, 2);
    //
    wxBoxSizer* bsCenter = new wxBoxSizer(wxHORIZONTAL);
    bsCenter->Add(bsElements, 2, wxEXPAND|wxALL, 2);
        bsCenter->Add(pNbElemBasisSet, 3, wxEXPAND|wxALL, 2);
    bsCenter->Add(bsBtns, 0, wxEXPAND|wxALL, 2);
    bsCenter->Add(bsSelected, 3, wxEXPAND|wxALL, 2);

    wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnOk = new wxButton(this, wxID_OK, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    wxButton * btnCancel = new wxButton(this, wxID_CANCEL, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    bsBottom->AddStretchSpacer(1);
    bsBottom->Add(btnOk, 0, wxALL, 2);
    bsBottom->Add(btnCancel, 0, wxALL, 2);

    wxStaticLine* line = new wxStaticLine(this, wxID_STATIC, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);

    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);
    bsTop->Add(bsCenter, 1, wxEXPAND|wxALL, 2);
    bsTop->AddSpacer(3);
    bsTop->Add(line, 0, wxEXPAND | wxALL, 2);
    bsTop->Add(bsBottom, 0, wxEXPAND | wxALL, 2); //| wxALIGN_RIGHT

    this->SetSizer(bsTop);
    SetSize(800, 500);
        Center();
    // wxMessageBox("is it filled?");
}

void BasisSetDlg::SetElemSymbols(char** eleSymbols, int count){
    int i;
    this->eleSymbols = eleSymbols;
    eleCount = count;
    if(eleSeletced)
        delete eleSeletced;
    //
    if(eleCount > 0){
                pLbElements->Append(STR_GLOBAL);
                eleSeletced = new int[eleCount+1];
                eleSeletced[0] = 0;   // for global
        }
    for (i= 0; i < eleCount; i++ ) {
        pLbElements->Append(StringUtil::CharPointer2String(eleSymbols[i]));
        eleSeletced[i+1] = 0;   // for each element
    }
        //
        pLbElements->SetSelection(0);
        DoLbElementClicked();
}

void BasisSetDlg::OnDialogOK(wxCommandEvent& event) {
    HideDialog(wxID_OK);
}

void BasisSetDlg::OnDialogCancel(wxCommandEvent& event) {
    HideDialog(wxID_CANCEL);
}

void BasisSetDlg::OnClose(wxCloseEvent& event){
    HideDialog(wxID_CANCEL);
}

void BasisSetDlg::HideDialog(int returnCode) {
    //int notSelectedElemIndex = 0;
  //  if(wxID_OK == returnCode && !IsValidSelection(&notSelectedElemIndex)) {
 //       Tools::ShowInfo(wxT("Please select basis set for element: ") + pLbElements->GetString(notSelectedElemIndex));
 //       return;
  //  }

    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
        //Destroy();
}

wxArrayString BasisSetDlg::GetSelectedBasisSet(void) {
    //pLbSelected->GetStrings();
    return pLbSelected->GetStrings();
}

void BasisSetDlg::SetSelectedBasisSet(const wxArrayString& basisSets) {
    pLbSelected->Set(basisSets);
}

void BasisSetDlg::OnLbElementClicked(wxCommandEvent& event) {
        DoLbElementClicked();
}

void BasisSetDlg::DoLbElementClicked(void) {
    wxString selectedElem;
    char* basisDir = NULL;
    char* eleSymbol = NULL;
    int totalStringCount = 0;
    int recommendedCount = 0;
    int otherCount = 0;
    char** basisSets = NULL;
    int i;
    selectedElem = pLbElements->GetStringSelection();
    if(selectedElem.IsEmpty()) return;

    StringUtil::String2CharPointer(&basisDir, MingUtil::GetMolcasBasisDir());
    // std::cout << "at the begining totalStringCount " << totalStringCount << " recommendedCount " << recommendedCount << " otherCount " << otherCount << std::endl;
    if(STR_GLOBAL.Cmp(selectedElem) == 0) {
        basisSets = MXYZFile::GetGlobalBasisSet(basisDir, eleSymbols, eleCount, &totalStringCount, &recommendedCount, &otherCount);
        //  std::cout << "STR_GLOBAL GetGlobalBasisSet: eleCount " << eleCount << " totalStringCount " << totalStringCount << " recommendedCount " << recommendedCount << " otherCount " << otherCount << std::endl;
    }else{
        StringUtil::String2CharPointer(&eleSymbol, selectedElem);
        // retrieve corresponding basis set with the selected element.
        basisSets = MXYZFile::GetBasisSet(basisDir, eleSymbol, &totalStringCount, &recommendedCount, &otherCount);
        // std::cout << "STR_GLOBAL else: " << recommendedCount << " otherCount " << otherCount << std::endl;
    }
    // std::cout << "basissetdlg: recomended " << recommendedCount << " otherCount " << otherCount << std::endl;
    // deselect some items that have been selected, prepared to be clear
    // otherwise, if the listbox contains some currently selected items,
    // invoke Clear() may cause some errors.
    if(pLbRecommended->GetSelection() != wxNOT_FOUND) {
        pLbRecommended->Deselect(pLbRecommended->GetSelection());
    }
    pLbRecommended->Clear();
    if(pLbOther->GetSelection() != wxNOT_FOUND) {
        pLbOther->Deselect(pLbOther->GetSelection());
    }
    pLbOther->Clear();

    if(basisSets) {
        // std::cout << "basissetdlg: if basiset recomended " << recommendedCount << " otherCount " << otherCount << std::endl;
        if(totalStringCount != recommendedCount + otherCount) {
            wxLogError(wxString::Format(wxT("Basis Set data for element %s is not consistent."), eleSymbol));
        }else{
            for(i = 0; i < recommendedCount; i++) {
                pLbRecommended->Append(StringUtil::CharPointer2String(basisSets[i]));
                // std::cout << "i: " << i << " basisSets[i] " << basisSets[i] << std::endl;
            }
            for(i = 0; i < otherCount; i++) {
                pLbOther->Append(StringUtil::CharPointer2String(basisSets[recommendedCount+i]));
                // std::cout << "i: " << i << " basisSets[recommendedCount+i] " << basisSets[recommendedCount+i] << std::endl;
            }
        }
    }

    if(basisDir)
        free(basisDir);
    if(eleSymbol)
        free(eleSymbol);
    if(basisSets)
        FreeStringArray(basisSets, totalStringCount);
}

void BasisSetDlg::OnLbSelectedClicked(wxCommandEvent& event){
    if(pLbSelected->GetSelection() == wxNOT_FOUND) return;
    wxString selectedBasis = pLbSelected->GetStringSelection();
    if(!selectedBasis.IsEmpty()) {
        // put the string into the textfile for user modification
        pTcModify->ChangeValue(selectedBasis);
    }
}

void BasisSetDlg::OnListBoxClicked(wxCommandEvent& event) {
    wxListBox* listBox = NULL;
    selectedList = event.GetId();
    if(selectedList == CTRL_ID_LB_RECOMMENDED) {
        listBox = pLbRecommended;
    }else {
        listBox = pLbOther;
    }
    if(listBox->GetSelection() == wxNOT_FOUND) return;
    if(IsSelected(listBox->GetStringSelection())) {
        // if this basis set has been selected, do not allow to select it again
        listBox->Deselect(listBox->GetSelection());
    }
}

void BasisSetDlg::OnListBoxDClicked(wxCommandEvent& event) {
    OnAddBasisSet(event);
}

void BasisSetDlg::OnAddBasisSet(wxCommandEvent& event) {
    wxListBox* listBox = NULL;
    if(selectedList == CTRL_ID_LB_RECOMMENDED) {
        listBox = pLbRecommended;
    }else {
        listBox = pLbOther;
    }

    wxString selectedBasis = listBox->GetStringSelection();
    if(selectedBasis.IsEmpty()) return;
    //if(pLbSelected->FindString(selectedBasis) == wxNOT_FOUND) {
    if(!IsSelected(selectedBasis)) {
        pLbSelected->Append(selectedBasis);
    }
}

void BasisSetDlg::OnLbSelectedDClicked(wxCommandEvent& event) {
     OnDelBasisSet(event);
}

void BasisSetDlg::OnDelBasisSet(wxCommandEvent& event) {
    if(pLbSelected->GetSelection() != wxNOT_FOUND) {
        pLbSelected->Delete(pLbSelected->GetSelection());
        pTcModify->ChangeValue(wxEmptyString);
    }
}

void BasisSetDlg::OnCustomBasisSet(wxCommandEvent& event) {
    wxString inputBasis = pTcModify->GetValue();
    inputBasis = inputBasis.Trim();
    if(inputBasis.IsEmpty()) return;

        //if(pLbSelected->FindString(inputBasis) != wxNOT_FOUND) return;
    if(event.GetId() == CTRL_ID_BTN_CUSTOM_ADD && !IsSelected(inputBasis)) {
        pLbSelected->Append(inputBasis);
    }else {
        if(pLbSelected->GetSelection() != wxNOT_FOUND && !IsSelected(inputBasis, pLbSelected->GetSelection())){
            pLbSelected->SetString(pLbSelected->GetSelection(), inputBasis);
        }
    }

}

bool BasisSetDlg::IsSelected(wxString selBasisSet, int exceptIndex) {
    int i;
    for(i = 0; i < (int)pLbSelected->GetCount(); i++) {
        if(i == exceptIndex) continue;
        if(IsStartWithSameElement(selBasisSet, pLbSelected->GetString(i))) {
            return true;
        }
    }
    return false;
}

bool BasisSetDlg::IsStartWithSameElement(wxString ele1Sym, wxString ele2Sym) {
    int dot1Index = -1, dot2Index = -1;
    dot1Index = ele1Sym.Find('.');
    dot2Index = ele2Sym.Find('.');
    if(dot1Index == dot2Index) {
        if(dot1Index == wxNOT_FOUND) {
            // global basis set
            return true;
        }else if(dot1Index <= 2) {
            // element basis set
            if(ele1Sym.Mid(0, dot1Index).Cmp(ele2Sym.Mid(0, dot2Index)) == 0) {
                return true;
            }
        }
    }
    return false;
}

bool BasisSetDlg::IsValidSelection(int* notSelectedElemIndex) {
    unsigned i, k;
    int dotIndex = -1;
    wxString eleSym = wxEmptyString;
    for(i = 0; i < pLbSelected->GetCount(); i++) {
        dotIndex = pLbSelected->GetString(i).Find('.') ;
        if(dotIndex == wxNOT_FOUND || dotIndex > 2) {
            //have selected a global basis set
            return true;
        }
    }
    for(i = 1; i < pLbElements->GetCount(); i++) {
        eleSym = pLbElements->GetString(i) + wxT(".");
        for(k = 0; k < pLbSelected->GetCount(); k++) {
            if(IsStartWithSameElement(eleSym, pLbSelected->GetString(k))) {
                break;
            }
        }
        // not found basis set for this element
        if(k >= pLbSelected->GetCount()) {
            *notSelectedElemIndex = i;
            return false;
        }
    }
    return true;
}
