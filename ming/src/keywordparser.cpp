/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "xmlparser.h"
#include "keywordparser.h"
#include "keywordconst.h"
#include "cstringutil.h"

#include "envutil.h"

XmlElement** GetModules(XmlElement* rootElem, int* count) {
        return GetSubElements(rootElem, ELEM_MODULE, count);
}

XmlElement** GetKeywords(XmlElement* moduleElem, int* count) {
        return GetSubElements(moduleElem, ELEM_KEYWORD, count);
}

XmlElement** GetCommands(XmlElement* rootElem, int* count) {
        XmlElement** emilElems = GetSubElements(rootElem, ELEM_EMIL, count);
        // only one EMIL tag
        if(1 != *count) {
                *count = 0;
                if(NULL != emilElems) free(emilElems);
                return NULL;
        }

        XmlElement* emilElem = emilElems[0];
        free(emilElems);
        return GetSubElements(emilElem, ELEM_COMMAND, count);
}

XmlElement* GetUndefined(XmlElement* commentElem) {
    int count = 0;
        XmlElement** undefineds = GetSubElements(commentElem, ELEM_KEYWORD, &count);
        // only one UNDEFINED tag
        if(1 != count) {
                if(NULL != undefineds) free(undefineds);
                return NULL;
        }

        XmlElement* undefined = undefineds[0];
        free(undefineds);
        return undefined;
}

char* GetKeywordKindString(XmlElement* elem) {
    char* kindChar = NULL;
    if(strcmp(elem->name, ELEM_GROUP) == 0){
        kindChar = (char*)KIND_GROUP;
    }else {
        kindChar = GetAttributeString(elem, ATTR_KIND);
        if(kindChar == NULL) {
            if(strcmp(elem->name, ELEM_SELECT) == 0){
                kindChar = (char*)KIND_SELECT;
            }
        }
    }
    return kindChar;
}

KeywordKind GetKeywordKind(XmlElement* elem) {
    int *count;
    char* kindChar = GetKeywordKindString(elem);
    //XmlElement** nameChar = GetSubElements(elem,kindChar,count);
    //XmlElement** helpElems = GetSubElements(elem, ELEM_HELP, &count);

    KeywordKind kind = KEYWORD_KIND_NULL;
    if(kindChar == NULL) {
        kind = KEYWORD_KIND_NULL;
    }else if(strcmp(KIND_BOX, kindChar) == 0) {
        kind = KEYWORD_KIND_BOX;
    }else if(strcmp(KIND_BLOCK, kindChar) == 0) {
        kind = KEYWORD_KIND_BLOCK;
    }else if(strcmp(KIND_CHOICE, kindChar) == 0) {
        kind = KEYWORD_KIND_CHOICE;
    }else if(strcmp(KIND_FILE, kindChar) == 0) {
        kind = KEYWORD_KIND_FILE;
    }else if(strcmp(KIND_DIR, kindChar) == 0) {
        kind = KEYWORD_KIND_DIR;
    }else if(strcmp(KIND_GROUP, kindChar) == 0) {
        kind = KEYWORD_KIND_GROUP;
    }else if(strcmp(KIND_INT, kindChar) == 0) {
        kind = KEYWORD_KIND_INT;
    }else if(strcmp(KIND_INTS, kindChar) == 0) {
        kind = KEYWORD_KIND_INTS;
    }else if(strcmp(KIND_INTS_COMPUTED, kindChar) == 0) {
        kind = KEYWORD_KIND_INTS_COMPUTED;
    }else if(strcmp(KIND_INTS_LOOKUP, kindChar) == 0) {
        kind = KEYWORD_KIND_INTS_LOOKUP;
    }else if(strcmp(KIND_LIST, kindChar) == 0) {
        kind = KEYWORD_KIND_LIST;
    }else if(strcmp(KIND_RADIO, kindChar) == 0) {
        kind = KEYWORD_KIND_RADIO;
    }else if(strcmp(KIND_REAL, kindChar) == 0) {
        kind = KEYWORD_KIND_REAL;
    }else if(strcmp(KIND_REALS, kindChar) == 0) {
        kind = KEYWORD_KIND_REALS;
    }else if(strcmp(KIND_REALS_COMPUTED, kindChar) == 0) {
        kind = KEYWORD_KIND_REALS_COMPUTED;
    }else if(strcmp(KIND_REALS_LOOKUP, kindChar) == 0) {
        kind = KEYWORD_KIND_REALS_LOOKUP;
    }else if(strcmp(KIND_SELECT, kindChar) == 0) {
        kind = KEYWORD_KIND_SELECT;
    }else if(strcmp(KIND_SINGLE, kindChar) == 0) {
        kind = KEYWORD_KIND_SINGLE;
    }else if(strcmp(KIND_STRING, kindChar) == 0) {
        // if(strcmp(GetAttributeString(elem,ATTR_NAME), (char *)ATTR_VALUE_BASISXYZ) == 0) {
        //     kind = KEYWORD_KIND_LIST;
        // } else {
            kind = KEYWORD_KIND_STRING;
        // }
    }else if(strcmp(KIND_STRINGS, kindChar) == 0) {
        kind = KEYWORD_KIND_STRINGS;
    }else if(strcmp(KIND_STRINGS_COMPUTED, kindChar) == 0) {
        kind = KEYWORD_KIND_STRINGS_COMPUTED;
    }else if(strcmp(KIND_STRINGS_LOOKUP, kindChar) == 0) {
        kind = KEYWORD_KIND_STRINGS_LOOKUP;
    }else if(strcmp(KIND_CUSTOM, kindChar) == 0) {
        if(strcmp(GetAttributeString(elem,ATTR_NAME), (char *)ATTR_VALUE_COORD) == 0) {
            // std::cout << "CUSTOM and COORD "<< GetAttributeString(elem,ATTR_NAME) << std::endl;
            kind = KEYWORD_KIND_FILE;
        } else {
            // std::cout << "CUSTOM and else "<< GetAttributeString(elem,ATTR_NAME) << std::endl;
            kind = KEYWORD_KIND_STRING;
        }
    }else if(strcmp(KIND_UNKNOWN, kindChar) == 0) {
        kind = KEYWORD_KIND_UNKNOWN;
    }
    return kind;
}

char* GetElementP_Help(XmlElement* elem) {
        int count;
        if(NULL == elem) return NULL;
        XmlElement** helpElems = GetSubElements(elem, ELEM_ROOT_HELP, &count);
        if(1 != count) {
                if(NULL == helpElems) free(helpElems);
                // std::cout << "helpElem Null: "  << std::endl;
                return NULL;
        }
        // std::cout << "helpElem32: " << helpElems << std::endl;
        XmlElement* helpElem = helpElems[1];

        free(helpElems);
        return helpElem->content;
}

char* GetElementHelp(XmlElement* elem) {
        int count;
        if(NULL == elem) return NULL;
        XmlElement** helpElems = GetSubElements(elem, ELEM_HELP, &count);
        // only one help tag
        // std::cout << "count " << count << std::endl;
        // std::cout << "helpElems " << helpElems << std::endl;
        // std::cout << "elem " << elem << std::endl;
        // std::cout << "elem->name " << elem->name << std::endl;
        // std::cout << "elem->attrCount " << elem->attrCount << std::endl;
        // std::cout << "elem->attrList " << elem->attrList << std::endl;
        // std::cout << "elem->content " << elem->content << std::endl;
        // std::cout << "elem->parent " << elem->parent << std::endl;
        // std::cout << "elem->subElemCount " << elem->subElemCount << "\n" << std::endl;
        // std::cout << "elem->subElemList " << elem->subElemList << std::endl;

        if(1 != count) {
                if(NULL == helpElems) free(helpElems);
                return NULL;
        }

        XmlElement* helpElem = helpElems[0];
        free(helpElems);
        return helpElem->content;
}

LogicData GetAttributeLogicData(XmlElement* elem, const char* attrName) {
    LogicData lData;
    lData.ope = ENUM_LOGIC_OR;
    lData.count = 0;
    lData.values = NULL;

        char* strValue = GetAttributeString(elem, attrName);
        if(strValue != NULL) {
                if(strchr(strValue, '.')) {
                        lData.ope = ENUM_LOGIC_OR;
                        lData.values = StringTokenizer(strValue, LOGIC_OR, &lData.count, 0);
                } else {
                        lData.ope = ENUM_LOGIC_AND;
                        lData.values = StringTokenizer(strValue, LOGIC_AND, &lData.count, 0);
                }
        }
        return lData;
}



void DumpAttributeString(XmlElement* elem, const char* attrName) {
        char* attrValue = GetAttributeString(elem, attrName);
        if(NULL != attrValue) {
                printf("%s=%s\n", attrName, attrValue);
        }
}

void DumpAttributeInt(XmlElement* elem, const char* attrName) {
        int value;
        if(GetAttributeInt(&value, elem, attrName)) {
                printf("%s=%d\n", attrName, value);
        }
}

void DumpAttributeDouble(XmlElement* elem, const char* attrName) {
        double value;
        if(GetAttributeDouble(&value, elem, attrName)) {
                printf("%s=%f\n", attrName, value);
        }
}

void DumpAttributeStringArray(XmlElement* elem, const char* attrName) {
        int count, i;
        char** attrValues = GetAttributeStringArray(elem, attrName, &count);
        if(NULL != attrValues) {
                for(i = 0; i < count; i++) {
                        printf("%s[%d]=%s\n", attrName, i, attrValues[i]);
                }
                FreeStringArray(attrValues, count);
        }
}

void DumpAttributeLogicData(XmlElement* elem, const char* attrName) {
    int i;
        LogicData lData = GetAttributeLogicData(elem, attrName);
        if(NULL != lData.values) {
                if(lData.ope == ENUM_LOGIC_AND) {
                        printf("LOGIC AND\n");
                }else {
                        printf("LOGIC OR\n");
                }
                for(i = 0; i < lData.count; i++) {
                        printf("%s[%d]=%s\n", attrName, i, lData.values[i]);
                }
                FreeStringArray(lData.values, lData.count);
        }
}

void DumpAttributeIntArray(XmlElement* elem, const char* attrName) {
        int count, i;
        int* attrValues = GetAttributeIntArray(elem, attrName, &count);
        if(NULL != attrValues) {
                for(i = 0; i < count; i++) {
                        printf("%s[%d]=%d\n", attrName, i, attrValues[i]);
                }
                free(attrValues);
        }
}

void DumpAttributeDoubleArray(XmlElement* elem, const char* attrName) {
        int count, i;
        double* attrValues = GetAttributeDoubleArray(elem, attrName, &count);
        if(NULL != attrValues) {
                for(i = 0; i < count; i++) {
                        printf("%s[%d]=%f\n", attrName, i, attrValues[i]);
                }
                free(attrValues);
        }
}

void DumpAttribute(XmlElement* elem) {
        DumpAttributeString(elem, ATTR_MODULE);
        DumpAttributeString(elem, ATTR_NAME);
        DumpAttributeString(elem, ATTR_KIND);
        DumpAttributeString(elem, ATTR_LEVEL);
        DumpAttributeInt(elem, ATTR_MINVALUE);
        DumpAttributeInt(elem, ATTR_MAXVALUE);
        DumpAttributeInt(elem, ATTR_DEFAULTVALUE);
        DumpAttributeLogicData(elem, ATTR_REQUIRE);
        DumpAttributeLogicData(elem, ATTR_EXCLUSIVE);
}
