/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "commandproperty.h"

#include "flowchartshape.h"
#include "stringutil.h"
#include "tools.h"
#include "commandviewerdlg.h"
#include "flowchartcanvas.h"
#include "mingpnl.h"

#include "keywordconst.h"
#include "xmlparser.h"
#include "cstringutil.h"

#define SELECT_WIDTH 2
#define DSELECT_WIDTH (2*SELECT_WIDTH)
#define FOCUS_WIDTH 3
#define ROUND_RADIUS 15.0
#define PARA_DISP 15
#define LINE_CHAR_COUNT 30

BEGIN_EVENT_TABLE(FlowChartShape, wxWindow)
    EVT_PAINT(FlowChartShape::OnPaint)
    EVT_MOUSE_EVENTS(FlowChartShape::OnMouseEvent)
    EVT_ERASE_BACKGROUND(FlowChartShape::OnEraseBackground)
    //EVT_CHAR(FlowChartShape::OnKeyChar)
END_EVENT_TABLE()

//IMPLEMENT_DYNAMIC_CLASS(FlowChartShape, wxWindow);

static wxPen lightPen(wxColour(0xFF,0xFF,0xFF), 1, wxPENSTYLE_SOLID);
static wxPen grayPen(wxColour(0xC0,0xC0,0xC0), 1, wxPENSTYLE_SOLID);
static wxPen darkPen(wxColour(0x80,0x80,0x80), 1, wxPENSTYLE_SOLID);
static wxPen selPen(wxColour(0xFF, 0x80, 0x40), FOCUS_WIDTH, wxPENSTYLE_SOLID);
static wxPen focusPen(wxColour(0x00,0x00,0x00), 1, wxPENSTYLE_DOT);

static wxBrush brushRectOver(wxColour(0x80, 0x90, 0xFF), wxBRUSHSTYLE_SOLID);
static wxBrush brushRectOverChecked(wxColour(0xC4, 0xCB, 0xFF), wxBRUSHSTYLE_SOLID);

static wxBrush brushRoundRectOver(wxColour(0x00,0xFF,0x00), wxBRUSHSTYLE_SOLID);
static wxBrush brushRoundRectOverChecked(wxColour(0xC0,0xFF,0xC0), wxBRUSHSTYLE_SOLID);
static wxPen bgRoundRectPen(wxColour(0xFF, 0x80, 0x40), 1, wxPENSTYLE_SOLID);

static wxBrush brushParaOverNotNeedInput(wxColour(0xBC,0xBA,0xA8), wxBRUSHSTYLE_SOLID);
static wxBrush brushParaOver(wxColour(0xFF, 0x80, 0xFF), wxBRUSHSTYLE_SOLID);
static wxBrush brushParaOverValid(wxColour(0xFF, 0xC0, 0xFF), wxBRUSHSTYLE_SOLID);

static wxBrush brushCommentOver(wxColour(0xC0, 0xDF, 0x7F), wxBRUSHSTYLE_SOLID);

FlowChartShape::FlowChartShape(FlowChartCanvas *parent, wxWindowID id, ElementProperty* prop, const wxSize& size): wxWindow(parent, id) {
    parentCanvas = parent;
        nextChartShape = NULL;
        pBitmap = NULL;
        keywordModel = NULL;
        elemProp = prop;
        isPainting = false;
        isSelected = false;
        isHasFocus = false;
        shapeState = STATE_UP;

        moduleViewerDlg = NULL;
}

FlowChartShape::~FlowChartShape() {
}

FlowChartShape* FlowChartShape::GetNextShape(void) const {
        return nextChartShape;
}

void FlowChartShape::SetNextShape(FlowChartShape* shape) {
        nextChartShape = shape;
}

/**
 * Add after the given element id in the list.
 * @return: the position of the given shape in the list.
 */
int FlowChartShape::AddShape(FlowChartShape* shape, int elemId) {
        int pos = 0;
        FlowChartShape* next = this;
        do {
                pos++;
                if(next->GetElementProperty()->GetInternalId() == elemId || next->GetNextShape() == NULL) {
                        shape->SetNextShape(next->GetNextShape());
                        next->SetNextShape(shape);
                        break;
                }
                next = next->GetNextShape();
        }while(next != NULL);

        return pos;
}

ElementProperty* FlowChartShape::GetElementProperty(void) const {
    return elemProp;
}

void FlowChartShape::OnPaint(wxPaintEvent & event){
    wxPaintDC dc(this);
        if(!isPainting) {
        // Draw the shapes on a bitmap first
        DrawOnBitmap();
        isPainting = false;
    }
    // Draw the bitmap on to the screen.
    dc.DrawBitmap(*pBitmap, 0, 0, true);
}

void FlowChartShape::Redraw(void) {
    if(!isPainting) {
           DrawOnBitmap();
           wxClientDC dc(this);
           dc.DrawBitmap(*pBitmap, 0, 0, true);
           isPainting = false;
    }
}

void FlowChartShape::SetSize(int x, int y, int width, int height, int sizeFlags) {
        wxWindow::SetSize(x, y, width, height, sizeFlags);
        if(pBitmap)
                delete pBitmap;
        //resize the bitmap
        pBitmap = new wxBitmap(width, height);
}

void FlowChartShape::SetShapeState(ShapeState state) {
    if(state == STATE_DOWN) {
           isHasFocus = true;
           isSelected = true;
    }
    if(GetShapeState() != state) {
        shapeState = state;
        // when the stata is changed, redraw it.
        Redraw();
    }
}

ShapeState FlowChartShape::GetShapeState(void) const{
        return shapeState;
}

bool FlowChartShape::IsSelected(void) const {
        return isSelected;
}

void FlowChartShape::SetSelected(bool selected) {
        isSelected = selected;
        Redraw();
}

bool FlowChartShape::IsHasFocus(void) const {
        return isHasFocus;
}

void FlowChartShape::SetHasFocus(bool focus) {
        isHasFocus = focus;
        Redraw();
}

int FlowChartShape::GetLabelLineCount(void) const {
        int lineCount = 1;
         char labelString[2048];
         char* valueShowLabel = NULL;

        if(elemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
        valueShowLabel = GetAttributeString(elemProp->GetXmlElement(), ATTR_SHOWVALUE);
        if(valueShowLabel != NULL && IsStringEqualNoCase(valueShowLabel, ATTR_VALUE_TRUE, strlen(ATTR_VALUE_TRUE))) {
            ((CommandProperty*)elemProp)->ToLabelString(labelString, 2048);
            lineCount += strlen(labelString) / LINE_CHAR_COUNT;
        }
    }

    return lineCount;
}

void FlowChartShape::DrawOnBitmap(void) {
    char labelString[2048];
    char multiLineLabelString[2048];
    char* valueShowLabel = NULL;
        int width, height;
        wxString label;
    wxMemoryDC dc;
    unsigned i = 0, j = 0;

    wxSize clientSize = GetClientSize();
    width = clientSize.GetWidth();
    height = clientSize.GetHeight();

        isPainting = true;
    if(pBitmap == NULL) {
                pBitmap = new wxBitmap(width, height);
        }
    dc.SelectObject(*pBitmap);
        // clear its background with its parent background
        wxBrush brushOver(GetParent()->GetBackgroundColour(), wxBRUSHSTYLE_SOLID);
        dc.SetBrush(brushOver);
        dc.SetPen(*wxTRANSPARENT_PEN);
        dc.DrawRectangle(0, 0, width, height);
        //
        if(elemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
        DrawParallelogram(dc, width, height);
    }else {
        //module
            if(strcmp(elemProp->GetName(), ATTR_VALUE_GATEWAY) == 0) {
                    DrawRoundRect(dc, width, height);
                }else if(strcmp(elemProp->GetName(), ATTR_VALUE_COMMENT) == 0) {
                        DrawComment(dc, width, height);
                }else {
                    DrawRectangle(dc, width, height);
            }
    }
        // draw label
    if(elemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
        valueShowLabel = GetAttributeString(elemProp->GetXmlElement(), ATTR_SHOWVALUE);
        if(valueShowLabel != NULL && IsStringEqualNoCase(valueShowLabel, ATTR_VALUE_TRUE, strlen(ATTR_VALUE_TRUE))) {
            ((CommandProperty*)elemProp)->ToLabelString(labelString, 2048);
            for(i = 0; i < strlen(labelString); i++, j++) {
                                if(i > 0 && i % LINE_CHAR_COUNT == 0 && i < strlen(labelString) - 1) {
                                        multiLineLabelString[j] = '\n';
                                        j++;
                                }
                                multiLineLabelString[j] = labelString[i];
                        }
                        multiLineLabelString[j] = '\0';

            label = StringUtil::CharPointer2String(multiLineLabelString);
        }else {
            label = StringUtil::CharPointer2String(elemProp->GetAppear());
        }
    }else {
        label = StringUtil::CharPointer2String(elemProp->GetAppear());
    }

        wxSize labelSize = dc.GetMultiLineTextExtent(label);
        width = (width  - labelSize.GetWidth()) / 2;
        height = (height  - labelSize.GetHeight()) / 2;
        if(width < 0) width = 0;
        if(height < 0) height = 0;
        //dc.DrawText(label, width, height);
        dc.DrawLabel(label, wxRect(width, height, labelSize.GetWidth(), labelSize.GetHeight()));
}

void FlowChartShape::DrawRoundRect(wxDC& dc, int width, int height) {
    if(isSelected) {
                dc.SetPen(selPen);
                dc.DrawRoundedRectangle(0, 0, width, height, ROUND_RADIUS+3);
        }
        if(elemProp->IsUserChecked()) {
        dc.SetBrush(brushRoundRectOverChecked);
    } else {
        dc.SetBrush(brushRoundRectOver);
    }
        switch(GetShapeState()) {
                case STATE_UP:
                        dc.SetPen(lightPen);
                        dc.DrawRoundedRectangle(SELECT_WIDTH, SELECT_WIDTH, width-1-DSELECT_WIDTH, height-1-DSELECT_WIDTH, ROUND_RADIUS);
                        dc.SetPen(darkPen);
                        dc.DrawRoundedRectangle(2+SELECT_WIDTH, 2+SELECT_WIDTH, width-2-DSELECT_WIDTH, height-2-DSELECT_WIDTH, ROUND_RADIUS);
                        dc.SetPen(grayPen);
                        dc.DrawRoundedRectangle(1+SELECT_WIDTH, 1+SELECT_WIDTH, width-2-DSELECT_WIDTH, height-2-DSELECT_WIDTH, ROUND_RADIUS-1);
                        //dc.SetPen(bgRoundRectPen);
                        dc.SetPen(*wxTRANSPARENT_PEN);
                        dc.DrawRoundedRectangle(1+SELECT_WIDTH, 1+SELECT_WIDTH, width-3-DSELECT_WIDTH, height-3-DSELECT_WIDTH, ROUND_RADIUS-1);
                        break;
                case STATE_DOWN:
                        dc.SetPen(lightPen);
                        dc.DrawRoundedRectangle(1+SELECT_WIDTH, 1+SELECT_WIDTH, width-1-DSELECT_WIDTH, height-1-DSELECT_WIDTH, ROUND_RADIUS);
                        dc.SetPen(darkPen);
                        dc.DrawRoundedRectangle(SELECT_WIDTH, SELECT_WIDTH, width-2-DSELECT_WIDTH, height-2-DSELECT_WIDTH, ROUND_RADIUS);
                        dc.SetPen(grayPen);
                        dc.DrawRoundedRectangle(1+SELECT_WIDTH, 1+SELECT_WIDTH, width-2-DSELECT_WIDTH, height-2-DSELECT_WIDTH, ROUND_RADIUS-1);
                        //dc.SetPen(bgRoundRectPen);
                        dc.SetPen(*wxTRANSPARENT_PEN);
                        dc.DrawRoundedRectangle(2+SELECT_WIDTH, 2+SELECT_WIDTH, width-3-DSELECT_WIDTH, height-3-DSELECT_WIDTH, ROUND_RADIUS-1);
                        break;
           default:
                        break;
        }

        if(isHasFocus) {
                dc.SetPen(focusPen);
                dc.DrawRoundedRectangle(FOCUS_WIDTH+SELECT_WIDTH, FOCUS_WIDTH+SELECT_WIDTH, width-2*(FOCUS_WIDTH+SELECT_WIDTH), height-2*(FOCUS_WIDTH+SELECT_WIDTH), ROUND_RADIUS-3);
        }

}



void FlowChartShape::DrawRectangle(wxDC& dc, int width, int height) {
    if(elemProp->IsUserChecked()) {
        dc.SetBrush(brushRectOverChecked);
    }else {
        dc.SetBrush(brushRectOver);
    }

        if(isSelected) {
                dc.SetPen(selPen);
                dc.DrawRectangle(0, 0, width, height);
        }else {
        dc.SetPen(*wxTRANSPARENT_PEN);
        dc.DrawRectangle(SELECT_WIDTH, SELECT_WIDTH, width - SELECT_WIDTH * 2, height - SELECT_WIDTH * 2);
    }

        if(isHasFocus) {
                dc.SetPen(focusPen);
                dc.DrawRectangle(FOCUS_WIDTH + SELECT_WIDTH, FOCUS_WIDTH + SELECT_WIDTH, width - 2 * (FOCUS_WIDTH + SELECT_WIDTH), height - 2 * (FOCUS_WIDTH + SELECT_WIDTH));
        }

    switch(GetShapeState()) {
                case STATE_UP:
                        dc.SetPen(lightPen);
                        dc.DrawLine(SELECT_WIDTH, SELECT_WIDTH, width - 1 - SELECT_WIDTH, SELECT_WIDTH);
                        dc.DrawLine(SELECT_WIDTH, SELECT_WIDTH, SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        dc.SetPen(darkPen);
                        dc.DrawLine(SELECT_WIDTH, height - 1 - SELECT_WIDTH, width - 1 - SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        dc.DrawLine(width - 1 - SELECT_WIDTH, SELECT_WIDTH, width - 1 - SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        dc.SetPen(grayPen);
                        dc.DrawLine(1 + SELECT_WIDTH, height - 2 - SELECT_WIDTH, width - 2 - SELECT_WIDTH, height - 2 - SELECT_WIDTH);
                        dc.DrawLine(width - 2 - SELECT_WIDTH, 1 + SELECT_WIDTH, width - 2 - SELECT_WIDTH, height - 2 - SELECT_WIDTH);
                        break;
                case STATE_DOWN:
                        dc.SetPen(darkPen);
                        dc.DrawLine(SELECT_WIDTH, SELECT_WIDTH, width - 1 - SELECT_WIDTH, SELECT_WIDTH);
                        dc.DrawLine(SELECT_WIDTH, SELECT_WIDTH, SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        dc.SetPen(grayPen);
                        dc.DrawLine(1 + SELECT_WIDTH, 1 + SELECT_WIDTH, width - 2 - SELECT_WIDTH, 1 + SELECT_WIDTH);
                        dc.DrawLine(1 + SELECT_WIDTH, 1 + SELECT_WIDTH, 1 + SELECT_WIDTH, height - 2 - SELECT_WIDTH);
                        dc.SetPen(lightPen);
                        dc.DrawLine(1 + SELECT_WIDTH, height - 1 - SELECT_WIDTH, width - 1 - SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        dc.DrawLine(width - 1 - SELECT_WIDTH, 1 + SELECT_WIDTH, width - 1 - SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        break;
           default:
                        break;
        }
}

void FlowChartShape::DrawParallelogram(wxDC& dc, int width, int height) {
        wxPoint points[4];

    if(elemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND &&
            !((CommandProperty*)elemProp)->IsNeedUserInput()) {
        dc.SetBrush(brushParaOverNotNeedInput);
    }else if(elemProp->IsValid()) {
        dc.SetBrush(brushParaOverValid);
    }else {
        dc.SetBrush(brushParaOver);
    }
        if(isSelected) {
                dc.SetPen(selPen);
        points[0] = wxPoint(PARA_DISP, 0);
        points[1] = wxPoint(width-2, 0);
            points[2] = wxPoint(width-2-PARA_DISP, height-1);
            points[3] = wxPoint(1, height-1);
        }else {
        dc.SetPen(*wxTRANSPARENT_PEN);
        points[0] = wxPoint(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH);
        points[1] = wxPoint(width-2-SELECT_WIDTH, SELECT_WIDTH);
            points[2] = wxPoint(width-2-PARA_DISP-SELECT_WIDTH, height-1-SELECT_WIDTH);
            points[3] = wxPoint(1+SELECT_WIDTH, height-1-SELECT_WIDTH);
    }
        dc.DrawPolygon(4, points);

        if(isHasFocus) {
                dc.SetPen(focusPen);
        points[0] = wxPoint(PARA_DISP + FOCUS_WIDTH + SELECT_WIDTH, FOCUS_WIDTH + SELECT_WIDTH);
        points[1] = wxPoint(width-3-FOCUS_WIDTH-SELECT_WIDTH, FOCUS_WIDTH + SELECT_WIDTH);
        points[2] = wxPoint(width-1-PARA_DISP-FOCUS_WIDTH - SELECT_WIDTH, height-1-FOCUS_WIDTH - SELECT_WIDTH);
        points[3] = wxPoint(3+SELECT_WIDTH+FOCUS_WIDTH, height-1-FOCUS_WIDTH - SELECT_WIDTH);
        dc.DrawPolygon(4, points);
    }

        switch(GetShapeState()) {
                case STATE_UP:
                        dc.SetPen(lightPen);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, width-1-SELECT_WIDTH, SELECT_WIDTH);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, 1+SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        dc.SetPen(darkPen);
                        dc.DrawLine(1+SELECT_WIDTH, height - 1-SELECT_WIDTH, width - 1 - PARA_DISP-SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.DrawLine(width -2-SELECT_WIDTH, SELECT_WIDTH, width - 1 - PARA_DISP-SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.SetPen(grayPen);
                        dc.DrawLine(2+SELECT_WIDTH, height - 2-SELECT_WIDTH, width - 2 - PARA_DISP-SELECT_WIDTH, height - 2-SELECT_WIDTH);
                        dc.DrawLine(width -3-SELECT_WIDTH, 1+SELECT_WIDTH, width - 2 - PARA_DISP-SELECT_WIDTH, height - 2-SELECT_WIDTH);
                        break;
                case STATE_DOWN:
                        dc.SetPen(darkPen);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, width - 1-SELECT_WIDTH, SELECT_WIDTH);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, 1+SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.SetPen(grayPen);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, 1+SELECT_WIDTH, width - 1-SELECT_WIDTH, 1+SELECT_WIDTH);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, 1+SELECT_WIDTH, 2+SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.SetPen(lightPen);
                        dc.DrawLine(2+SELECT_WIDTH, height - 1-SELECT_WIDTH, width - 1 - PARA_DISP-SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.DrawLine(width-2-SELECT_WIDTH, 1+SELECT_WIDTH, width - 1 - PARA_DISP-SELECT_WIDTH, height-1-SELECT_WIDTH);
                        break;
           default:
                        break;
        }
}

void FlowChartShape::DrawComment(wxDC& dc, int width, int height) {
        wxPoint points[4];

    dc.SetBrush(brushCommentOver);

        if(isSelected) {
                dc.SetPen(selPen);
                points[0] = wxPoint(PARA_DISP, 0);
                points[1] = wxPoint(width-2-PARA_DISP, 0);
                points[2] = wxPoint(width-2, height-1);
                points[3] = wxPoint(1, height-1);
        }else {
                dc.SetPen(*wxTRANSPARENT_PEN);
                points[0] = wxPoint(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH);
                points[1] = wxPoint(width-2-PARA_DISP-SELECT_WIDTH, SELECT_WIDTH);
                points[2] = wxPoint(width-2-SELECT_WIDTH, height-1-SELECT_WIDTH);
                points[3] = wxPoint(1+SELECT_WIDTH, height-1-SELECT_WIDTH);
        }
        dc.DrawPolygon(4, points);

        if(isHasFocus) {
                dc.SetPen(focusPen);
                points[0] = wxPoint(PARA_DISP + FOCUS_WIDTH + SELECT_WIDTH, FOCUS_WIDTH + SELECT_WIDTH);
                points[1] = wxPoint(width-2-PARA_DISP-FOCUS_WIDTH-SELECT_WIDTH, FOCUS_WIDTH + SELECT_WIDTH);
                points[2] = wxPoint(width-2-FOCUS_WIDTH-SELECT_WIDTH, height-1-FOCUS_WIDTH - SELECT_WIDTH);
                points[3] = wxPoint(3+SELECT_WIDTH+FOCUS_WIDTH, height-1-FOCUS_WIDTH - SELECT_WIDTH);
                dc.DrawPolygon(4, points);
        }

        switch(GetShapeState()) {
                case STATE_UP:
                        dc.SetPen(lightPen);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, width-1-PARA_DISP-SELECT_WIDTH, SELECT_WIDTH);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, 1+SELECT_WIDTH, height - 1 - SELECT_WIDTH);
                        dc.SetPen(darkPen);
                        dc.DrawLine(1+SELECT_WIDTH, height - 1-SELECT_WIDTH, width-1-SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.DrawLine(width-2-PARA_DISP-SELECT_WIDTH, SELECT_WIDTH, width-1-SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.SetPen(grayPen);
                        dc.DrawLine(2+SELECT_WIDTH, height - 2-SELECT_WIDTH, width-2-SELECT_WIDTH, height - 2-SELECT_WIDTH);
                        dc.DrawLine(width-3-PARA_DISP-SELECT_WIDTH, 1+SELECT_WIDTH, width-2-SELECT_WIDTH, height - 2-SELECT_WIDTH);
                        break;
                case STATE_DOWN:
                        dc.SetPen(darkPen);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, width-1-PARA_DISP-SELECT_WIDTH, SELECT_WIDTH);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, SELECT_WIDTH, 1+SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.SetPen(grayPen);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, 1+SELECT_WIDTH, width-1-PARA_DISP-SELECT_WIDTH, 1+SELECT_WIDTH);
                        dc.DrawLine(PARA_DISP+SELECT_WIDTH, 1+SELECT_WIDTH, 2+SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.SetPen(lightPen);
                        dc.DrawLine(2+SELECT_WIDTH, height - 1-SELECT_WIDTH, width-1-SELECT_WIDTH, height - 1-SELECT_WIDTH);
                        dc.DrawLine(width-2-PARA_DISP-SELECT_WIDTH, 1+SELECT_WIDTH, width-1-SELECT_WIDTH, height-1-SELECT_WIDTH);
                        break;
                default:
                        break;
        }
}

void FlowChartShape::OnEraseBackground(wxEraseEvent& event) {
}

void FlowChartShape::DispathEvent(void) {
}

void FlowChartShape::OnMouseEvent(wxMouseEvent& event) {
    if(event.LeftDClick()) {
        if(!IsHasFocus()) {
            parentCanvas->UpdateExclusiveFocusOrSelection(this, true);
        }
        SetShapeState(STATE_DOWN);
        if(elemProp->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
//wxMessageBox(wxT("FlowChartShape::ShowCommandPropertyViewer()"));
            ShowCommandPropertyViewer();
        }else {
//wxMessageBox(wxT("FlowChartShape::ShowModulePropertyViewer()"));
            ShowModulePropertyViewer();
        }
        SetShapeState(STATE_UP);
    }else if(event.LeftDown()) {
        if(!IsHasFocus()) {
            parentCanvas->UpdateExclusiveFocusOrSelection(this, true);
        }
        SetShapeState(STATE_DOWN);
        if(!event.ControlDown()) {
            parentCanvas->UpdateExclusiveFocusOrSelection(this, false);
        }
        //SetShapeState(STATE_UP);
    }else if(event.LeftUp()) {
                SetShapeState(STATE_UP);
                //SetFocus();
        parentCanvas->SetFocusIgnoringChildren();
        }
}

void FlowChartShape::ShowModulePropertyViewer(void) {
        if(MingPnl::Get()->SaveFileWhenRelativePath()) {
//begin : hurukun : 23/11/2009
                ModuleViewerDlg viewDlg(MingPnl::Get(), wxID_ANY);
                MingPnl::Get()->SetActiveDlg(&viewDlg);
//end   : hurukun : 23/11/2009
                viewDlg.SetModuleProperty((ModuleProperty*)elemProp);
            viewDlg.ShowModal();
        }
}

void FlowChartShape::ShowCommandPropertyViewer(void) {
    if(!((CommandProperty*)elemProp)->IsNeedUserInput()) return;
//begin : hurukun : 23/11/2009
    CommandViewerDlg viewDlg(MingPnl::Get(), wxID_ANY);
    MingPnl::Get()->SetActiveDlg(&viewDlg);
//end   : hurukun : 23/11/2009
        viewDlg.SetCommandProperty((CommandProperty*)elemProp);
    if(viewDlg.ShowModal() == wxID_OK) {
        parentCanvas->UpdateLinks(GetLabelLineCount() > 1);
    }
}

void FlowChartShape::OnKeyChar(wxKeyEvent& event) {
    Tools::ShowError(wxT("Key Event"));
}
