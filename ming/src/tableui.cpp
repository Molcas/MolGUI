/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "tableui.h"
#include "stringutil.h"
#include "keyworddata.h"
#include "cstringutil.h"
#include <float.h>

#define CTRL_TEXT_CTRL_ID CTRL_WIN_CTRL_CUSTOM_START + 1

BEGIN_EVENT_TABLE(TableUI, KeywordUI)
        EVT_GRID_CELL_CHANGED(TableUI::OnCellValueChanged)
        EVT_TEXT(CTRL_TEXT_CTRL_ID, TableUI::OnText)
        EVT_SIZE(TableUI::OnSize)
END_EVENT_TABLE()

TableUI::TableUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel){
    textCtrl = NULL;
        CreateUIControls();
}

wxGrid* TableUI::GetTable(void) const {
    return (wxGrid*)windowCtrl;
}

TableModel* TableUI::GetTableModel(void) const {
    return (TableModel*)keywordModel;
}

void TableUI::OnSize(wxSizeEvent& event) {
    event.Skip();
    SetColumnSize();
        /*int i;
        int colCount = GetTableModel()->GetColumnCount();
    event.Skip();
    int colWidth = (event.GetSize().GetWidth() - 40)/ colCount;
    if(colWidth < 0) colWidth = 50;
    for(i = 0; i < colCount; i++) {
                GetTable()->SetColSize(i, colWidth);
        }*/
    //GetTable()->AutoSizeColumns();
    //GetTable()->Refresh();
}

void TableUI::CreateUIControls(void) {
    int rowCount = GetTableModel()->GetRowCount();
    int colCount = GetTableModel()->GetColumnCount();

    if(rowCount > 3) rowCount = 3;
    //if(colCount > 3) colCount = 3;

    CreateToftSizer(wxVERTICAL, true);
    if(GetTableModel()->IsComputedArray()) {
        textCtrl = new wxTextCtrl(this, CTRL_TEXT_CTRL_ID, wxEmptyString, wxDefaultPosition, wxSize(UI_WIDTH_UNIT/2, UI_HEIGHT_UNIT));
    }
    windowCtrl = new wxGrid(this, wxID_ANY, wxDefaultPosition, wxDefaultSize);
        //GetTable()->SetDefaultEditor(new wxGridCellNumberEditor(10));


        switch(GetTableModel()->GetKind()) {
        case KEYWORD_KIND_INTS:
        case KEYWORD_KIND_INTS_LOOKUP:
        case KEYWORD_KIND_INTS_COMPUTED:
                        GetTable()->SetDefaultEditor(new GridCellFormatEditor(FORMAT_TEXT_CTRL_INT, GetTableModel()->GetMinValue(), GetTableModel()->GetMaxValue()));
            break;
        case KEYWORD_KIND_REALS:
        case KEYWORD_KIND_REALS_COMPUTED:
        case KEYWORD_KIND_REALS_LOOKUP:
                        GetTable()->SetDefaultEditor(new GridCellFormatEditor(FORMAT_TEXT_CTRL_REAL, GetTableModel()->GetMinValue(), GetTableModel()->GetMaxValue()));
            break;
        default:
                break;
    }

    GetTable()->CreateGrid(rowCount, colCount);
    GetTable()->SetRowLabelSize(1);
    GetTable()->SetColLabelSize(1);
    SetRowSize();
    SetColumnSize();
    //GetTable()->AutoSizeColumns();
    //GetTable()->SetBackgroundColour(*wxLIGHT_GREY);
    if(textCtrl != NULL) {
        GetSizer()->Add(textCtrl, 0, wxALIGN_LEFT | wxALL, 2);
    }
    GetSizer()->Add(windowCtrl, 1, wxEXPAND | wxALL, 2);
    //GetSizer()->SetSizeHints(this);
    UpdateFromModel();
}

void TableUI::OnText(wxCommandEvent& event) {
    wxString value = textCtrl->GetValue();
    long lvalue = 0;
    int ivalue = 0;
    if(!value.IsEmpty()) {
        if(value.ToLong(&lvalue)) {
            ivalue = (int)lvalue;
            //GetTableModel()->SetComputedNumber(ivalue);
            UpdateRowCount(ivalue);
        }else {

        }
    }
}

void TableUI::UpdateRowCount(int rowNumber) {
        int diff = 0;
        if(GetTableModel()->IsComputedArrayWithSizeOne()) {
                diff = GetTable()->GetNumberCols() - rowNumber;
            if(diff < 0) {
                GetTable()->AppendCols(-diff);
            }else if(diff > 0) {
                GetTable()->DeleteCols(rowNumber, diff);
            }
            SetColumnSize();
        }else {
            diff = GetTable()->GetNumberRows() - rowNumber;
            if(diff < 0) {
                GetTable()->AppendRows(-diff);
            }else if(diff > 0) {
                GetTable()->DeleteRows(rowNumber, diff);
            }
            SetRowSize();
        }
}

void TableUI::SetRowSize(void) {
        int i;
        for(i = 0; i < GetTable()->GetNumberRows(); i++) {
                GetTable()->SetRowSize(i, 30);
        }
}

void TableUI::SetColumnSize(void) {
        int i = 0;
        //int colCount = GetTableModel()->GetColumnCount();
        int colCount = GetTable()->GetNumberCols();
        int colWidth = 0;
        if(colCount > 0) colWidth = (GetSize().GetWidth() - 40)/ colCount;

        if(colWidth < 0) colWidth = 50;
        switch(GetTableModel()->GetKind()) {
        case KEYWORD_KIND_INTS:
        case KEYWORD_KIND_INTS_LOOKUP:
        case KEYWORD_KIND_INTS_COMPUTED:
                        if(colWidth > UI_INT_WIDTH)
                    colWidth = UI_INT_WIDTH;
            break;
        case KEYWORD_KIND_REALS:
        case KEYWORD_KIND_REALS_COMPUTED:
        case KEYWORD_KIND_REALS_LOOKUP:
                        if(colWidth > UI_REAL_WIDTH)
                    colWidth = UI_REAL_WIDTH;
            break;
        default:
                break;
    }

    for(i = 0; i < colCount; i++) {
        //FileUtil::ShowInfoMessage(wxString::Format("table %d %d", i, colWidth));
                GetTable()->SetColSize(i, colWidth);
        }
}

void TableUI::OnCellValueChanged(wxGridEvent& event) {
        keywordModel->NotifySelection();;
        event.Skip();
}

void TableUI::OnClearData(void) {
        int i, j;
        for(i = 0; i < GetTable()->GetNumberRows(); i++) {
                for(j = 0; j < GetTable()->GetNumberCols(); j++) {
                        GetTable()->SetCellValue(i, j, wxEmptyString);
                }
        }
}

void TableUI::UpdateFromModel(void) {
    char* charValue = GetTableModel()->GetValue();
    char** charValues = NULL;
    char** lineValues = NULL;
    int lineCount = 0, valueCount, i = 0, j = 0;

        KeywordUI::UpdateFromModel();
    //FileUtil::ShowInfoMessage(wxString::Format("table %s", charValue));
    if(charValue == NULL) {
        OnClearData();
        if(GetTableModel()->IsComputedArray()) {
            if(textCtrl != NULL) {
                textCtrl->ChangeValue(wxString::Format(wxT("%d"), 0));
            }
        }
    }else {
        lineValues = StringTokenizer(charValue, LINE_DELIM, &lineCount, 1);
        UpdateRowCount(lineCount);
        for(i = 0; i < lineCount; i++) {
                        if(GetTableModel()->IsComputedArrayWithSizeOne()) {
                                GetTable()->SetCellValue(0, i, StringUtil::CharPointer2String(lineValues[i]));
                        }else {
                    charValues = StringTokenizer(lineValues[i], VALUE_DELIM, &valueCount, 1);
                    for(j = 0; j < GetTable()->GetNumberCols(); j++) {
                        if(j < valueCount) {
                            GetTable()->SetCellValue(i, j, StringUtil::CharPointer2String(charValues[j]));
                        }
                        else
                            GetTable()->SetCellValue(i, j, wxEmptyString);
                    }
                    if(charValues)
                        FreeStringArray(charValues, valueCount);
                        }
        }
        if(textCtrl != NULL) {
            textCtrl->ChangeValue(wxString::Format(wxT("%d"), lineCount));
        }
        if(lineValues)
            FreeStringArray(lineValues, lineCount);
    }
}

void TableUI::UpdateToModel(bool isRestore) {
    wxString strValue = wxEmptyString;
    char* charValue = NULL;
    bool isEmpty = true;
        int i, j;
    if(isRestore) {

    }else{
                if(GetTableModel()->IsEnabled()) {
                        wxString value;
                        for(i = 0; i < GetTable()->GetNumberRows(); i++) {
                                for(j = 0; j < GetTable()->GetNumberCols(); j++) {
                                        value = GetTable()->GetCellValue(i, j);
                                        if(!StringUtil::IsEmptyString(value)) {
                                                isEmpty = false;
                                        }
                                        strValue += GetTable()->GetCellValue(i, j);
                                        if(j < GetTable()->GetNumberCols() -1) {
                                                if(GetTableModel()->IsComputedArrayWithSizeOne()) {
                                                        strValue += StringUtil::CharPointer2String(LINE_DELIM);
                                                }else {
                                                        strValue += StringUtil::CharPointer2String(VALUE_DELIM);
                                                }
                                        }
                                }
                                if(!GetTableModel()->IsComputedArrayWithSizeOne() && i < GetTable()->GetNumberRows() - 1) {
                                        strValue += StringUtil::CharPointer2String(LINE_DELIM);
                                }
                        }
                        StringUtil::String2CharPointer(&charValue, strValue);
                        if(isEmpty) {
                                GetTableModel()->SetValue(NULL);
                        }else{
                                GetTableModel()->SetValue(charValue);
                        }
                        if(charValue)
                                free(charValue);
                }else {
                        GetTableModel()->SetValue(NULL);
                }
    }
}

bool TableUI::HasValue(void) const {
    int i, j;
    if(GetTableModel()->IsEnabled()) {
                wxString value;
                for(i = 0; i < GetTable()->GetNumberRows(); i++) {
                        for(j = 0; j < GetTable()->GetNumberCols(); j++) {
                                value = GetTable()->GetCellValue(i, j);
                                if(!StringUtil::IsEmptyString(value)) {
                                        return true;
                                }
                        }
                }
    }
    return false;
}


GridCellFormatEditor::GridCellFormatEditor(FormatTextCtrlType type, const char* charMinValue, const char* charMaxValue) {
        m_formatType = type;
        wxString minValueWX = wxEmptyString;
        wxString maxValueWX = wxEmptyString;
        if(charMinValue != NULL) {
                minValueWX = StringUtil::CharPointer2String(charMinValue);
        }
        if(charMaxValue != NULL) {
                maxValueWX = StringUtil::CharPointer2String(charMaxValue);
        }

        if(!minValueWX.ToDouble(&m_minValue)) {
                m_minValue = -DBL_MAX;
        }
        if(!maxValueWX.ToDouble(&m_maxValue)) {
                m_maxValue = DBL_MAX;
        }
}

GridCellFormatEditor::GridCellFormatEditor(FormatTextCtrlType type, double minValue, double maxValue) {
        m_formatType = type;
        m_minValue = minValue;
        m_maxValue = maxValue;
}

void GridCellFormatEditor::Create(wxWindow* parent, wxWindowID id, wxEvtHandler* evtHandler) {
        m_control = new FormatTextCtrl(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize,
                wxTE_PROCESS_ENTER | wxTE_PROCESS_TAB | wxNO_BORDER);
        ((FormatTextCtrl*)m_control)->SetFormatType(m_formatType, m_minValue, m_maxValue);
        wxGridCellEditor::Create(parent, id, evtHandler);
}

wxGridCellEditor* GridCellFormatEditor::Clone() const {
        return new GridCellFormatEditor(m_formatType, m_minValue, m_maxValue);
}
