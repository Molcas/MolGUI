/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
*               2021, Ignacio Fdez. Galván                             *
***********************************************************************/

#include <stdlib.h>
#include <string.h>
#include <wx/msgdlg.h>
#include "manager.h"
#include "simuproject.h"
#include "fileutil.h"
#include "stringutil.h"

#include "moduleproperty.h"
#include "checkboxmodel.h"
#include "groupmodel.h"
#include "textctrlmodel.h"
#include "selectionmodel.h"
#include "filemodel.h"
#include "tablemodel.h"
#include "listboxmodel.h"
#include "keyworddata.h"
#include "inputparser.h"

#include "keywordparser.h"
#include "keywordconst.h"
#include "cstringutil.h"

#define MEM_INC_SIZE 10

ModuleProperty::ModuleProperty(XmlElement* elem) : ElementProperty(elem) {
        propType = ELEM_PROP_TYPE_MODULE;
        Init();
}

ModuleProperty::ModuleProperty(XmlElement* elem, KeywordModel* parentKeywordModel, ElemPropType elemType): ElementProperty(elem, parentKeywordModel) {
        propType = elemType;
        Init();
}

ModuleProperty::~ModuleProperty() {
        int i = 0;
        /* as keywordModels is a pointer to the array of KeywordModel*,
                the array is dynamically allocated. */
        if (keywordModels) {
                for (i = 0; i < subElemCount; i++) {
                        if (keywordModels[i]) delete keywordModels[i];
                }
                //delete[] keywordModels;
                free(keywordModels);
                keywordModels = NULL;
        }
}

ModuleProperty* ModuleProperty::GetTopModuleProperty(void) const {
        if (GetParentKeywordModel() == NULL) {
                return (ModuleProperty*)this;
        } else {
                return GetParentKeywordModel()->GetParentModule()->GetTopModuleProperty();
        }
}

ElemPropType ModuleProperty::GetPropType(void) const {
        return propType;
}

void ModuleProperty::SetPropType(ElemPropType type) {
        propType = type;
}

bool ModuleProperty::IsCommentModule(void) const {
        return strcmp(GetName(), ATTR_VALUE_COMMENT) == 0;
}

bool ModuleProperty::IsEnvModule(void) const {
        return strcmp(GetName(), ATTR_VALUE_ENV) == 0;
}

KeywordModel* ModuleProperty::GetKeywordModel(const char* name, KeywordModel** keywordModelSelect) const {
        int i = 0;
        char* keywordName = NULL;
        char* keywordAlso = NULL;
        KeywordModel* kModel = NULL;
        bool isEnv = IsEnvModule();
        for (i = 0; i < subElemCount; i++) {
                if (keywordModels[i] == NULL) continue;
                keywordName = keywordModels[i]->GetName();
                keywordAlso = keywordModels[i]->GetAlso();

                if (keywordName == NULL) continue;
                if (IsEmptyString(keywordName, strlen(keywordName))) continue;

                if (keywordModels[i]->GetKind() == KEYWORD_KIND_SELECT) {
                        kModel = keywordModels[i]->GetChildModule()->GetKeywordModel(name);
                        if (kModel) {
                                if (keywordModelSelect != NULL) {
                                        *keywordModelSelect = keywordModels[i];
                                }
                                return kModel;
                        }
                } else if (keywordModels[i]->GetKind() == KEYWORD_KIND_GROUP && ((GroupModel*)keywordModels[i])->GetGroupKind() != KEYWORD_KIND_BLOCK) {
                        kModel = keywordModels[i]->GetChildModule()->GetKeywordModel(name);
                        if (kModel) {
                                return kModel;
                        }
                } else if (isEnv) {
                        if (IsStringEqualNoCase(keywordName, name, strlen(keywordName))) {
                                return keywordModels[i];
                        }
                } else if (IsStringEqualNoCase(keywordName, name, 4)) {
                        //may be keywordName is a prefix of name
                        if (strlen(name) == strlen(keywordName) || (strlen(name) >= strlen(keywordName) && strlen(keywordName) >= 4) ||
                                        (strlen(name) > strlen(keywordName) && strlen(keywordName) < 4 && (name[strlen(keywordName)] == ' '
                                                        || name[strlen(keywordName)] == '\n' || name[strlen(keywordName)] == '\r'
                                                        || name[strlen(keywordName)] == '\t' || name[strlen(keywordName)] == '='))) {
                                //FileUtil::ShowInfoMessage(FileUtil::CharPointer2String(name) + wxT("###") + FileUtil::CharPointer2String(keywordName) + wxT("---") + wxString::Format(wxT("%d, %d"), strlen(keywordName), isEnv));
                                return keywordModels[i];
                        }
                } else if ((keywordAlso != NULL) and IsStringEqualNoCase(keywordAlso, name, 4)) {
                        //may be keywordAlso is a prefix of name
                        if (strlen(name) == strlen(keywordAlso) || (strlen(name) >= strlen(keywordAlso) && strlen(keywordAlso) >= 4) ||
                                        (strlen(name) > strlen(keywordAlso) && strlen(keywordAlso) < 4 && (name[strlen(keywordAlso)] == ' '
                                                        || name[strlen(keywordAlso)] == '\n' || name[strlen(keywordAlso)] == '\r'
                                                        || name[strlen(keywordAlso)] == '\t' || name[strlen(keywordAlso)] == '='))) {
                                //FileUtil::ShowInfoMessage(FileUtil::CharPointer2String(name) + wxT("###") + FileUtil::CharPointer2String(keywordAlso) + wxT("---") + wxString::Format(wxT("%d, %d"), strlen(keywordAlso), isEnv));
                                return keywordModels[i];
                        }
                }

        }
        return NULL;
}

KeywordModel* ModuleProperty::GetKeywordModel(int index) const {
        if (index >= 0 && index < subElemCount)
                return keywordModels[index];
        return NULL;
}

void ModuleProperty::ClearData(void) {

}

bool ModuleProperty::IsNeedShowSubelements(void) const {
        return needShowSubelements;
}

bool ModuleProperty::IsValid(void) {
        // do not to check
        return true;
}

KeywordModel* ModuleProperty::InsertKeywordModelAfter(const char* preKeywordName) {
        int index = 0;
        int i = 0;

        KeywordModel* userinputModel = new TextCtrlModel(this, GetKeywordData()->GetUndefined());

        if (preKeywordName != NULL) {
                for (i = 0; i < subElemCount; i++) {
                        if (keywordModels[i] != NULL && strcmp(keywordModels[i]->GetName(), preKeywordName) == 0) {
                                index = i + 1;
                                break;
                        }
                }
        }

        keywordModels = (KeywordModel**)RequestMemory(keywordModels, subElemCount, MEM_INC_SIZE, sizeof(KeywordModel*));
        subElemCount++;
        for (i = subElemCount; i > index; i--) {
                keywordModels[i] = keywordModels[i - 1];
        }
        keywordModels[index] = userinputModel;
        return userinputModel;
}

void ModuleProperty::Init(void) {
        int i = 0;
        KeywordKind kind;
        needShowSubelements = false;
        bool isGroupRadio = false;

        if (GetParentKeywordModel() != NULL && GetParentKeywordModel()->GetKind() == KEYWORD_KIND_GROUP) {
                isGroupRadio = ((GroupModel*)GetParentKeywordModel())->GetGroupKind() == KEYWORD_KIND_RADIO;
        }
        // Get all subelements of the given xmlelement
        // subElementCount is the number of subelements
        XmlElement** elems = GetKeywords(xmlElement, &subElemCount);
        //keywordModels = new KeywordModel*[subElemCount];

        for (i = 0; i < subElemCount; i++) {
                kind = GetKeywordKind(elems[i]);
                // std::cout << "GetKeywordKind: " << kind << std::endl;
                keywordModels = (KeywordModel**)RequestMemory(keywordModels, i, MEM_INC_SIZE, sizeof(KeywordModel*));
                switch (kind) {
                case KEYWORD_KIND_NULL:
                        keywordModels[i] = NULL;
                        break;
                case KEYWORD_KIND_CHOICE:
                        needShowSubelements = true;
                        keywordModels[i] = new SelectionModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_FILE:
                        needShowSubelements = true;
                        keywordModels[i] = new FileModel(this, elems[i]);
                        // std::cout << "ModuleProperty::InsertKeywordModelAfter KEYWORD_KIND_FILE " << KEYWORD_KIND_FILE << std::endl;
                        if (strcmp(GetName(), ATTR_VALUE_GATEWAY) == 0 && strcmp(keywordModels[i]->GetName(), ATTR_VALUE_COORD) == 0) {
                                keywordModels[i]->SetValue(GetKeywordData()->GetXYZFile());
                                if(GetKeywordData()->GetXYZFile()==NULL){
                                        SimuProject* pProject = Manager::Get()->GetProjectManager()->GetSelectedProject();
                                    wxString molFile = ProjectTreeItemData::GetFileName(pProject, wxEmptyString, ITEM_MOLECULE);
                                    wxString xyzfilename =FileUtil::ChangeFileExt(molFile, wxT(".xyz"));
                                        char        *xyzFile;
                                        StringUtil::String2CharPointer(&xyzFile,xyzfilename);
                                        // std::cout << "xyzFile: " << xyzFile << std::endl;
                                        keywordModels[i]->SetValue(xyzFile);
                                }
                        }
                        break;
                case KEYWORD_KIND_DIR:
                        needShowSubelements = true;
                        keywordModels[i] = new FileModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_GROUP:
                        needShowSubelements = true;
                        keywordModels[i] = new GroupModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_INT:
                        needShowSubelements = true;
                        keywordModels[i] = new TextCtrlModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_INTS:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_INTS_COMPUTED:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_INTS_LOOKUP:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_LIST:
                        needShowSubelements = true;
                        keywordModels[i] = new ListBoxModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_REAL:
                        needShowSubelements = true;
                        keywordModels[i] = new TextCtrlModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_REALS:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_REALS_COMPUTED:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_REALS_LOOKUP:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_SELECT:
                        keywordModels[i] = new SelectionModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_SINGLE:
                        keywordModels[i] = new CheckboxModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_STRING:
                        needShowSubelements = true;
                        keywordModels[i] = new TextCtrlModel(this, elems[i]);
                        // std::cout << "STRING moduleproperty.cpp Before " << GetName() << "/" << keywordModels[i]->GetName() << "/" << ATTR_VALUE_BASISXYZ << std::endl;
                        if (strcmp(GetName(), ATTR_VALUE_GATEWAY) == 0 && strcmp(keywordModels[i]->GetName(), ATTR_VALUE_BASISXYZ) == 0) {
                                // std::cout << "STRING moduleproperty.cpp " << keywordModels[i]->GetName() << std::endl;
                                keywordModels[i] = new ListBoxModel(this, elems[i]);
                        } else {
                                //keywordModels[i] = new TextCtrlModel(this, elems[i]);
                                // std::cout << "STRING moduleproperty.cpp else " << keywordModels[i]->GetName() << std::endl;
                        }
                        break;
                case KEYWORD_KIND_STRINGS:
                        needShowSubelements = true;
                        keywordModels[i] = new TextCtrlModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_STRINGS_COMPUTED:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_STRINGS_LOOKUP:
                        needShowSubelements = true;
                        keywordModels[i] = new TableModel(this, elems[i]);
                        break;
                case KEYWORD_KIND_CUSTOM:
                        needShowSubelements = true;
                        keywordModels[i] = new TextCtrlModel(this, elems[i]);
                        // std::cout << "moduleproperty.cpp CUSTOM " << KEYWORD_KIND_CUSTOM << std::endl;
                        if (strcmp(GetName(), ATTR_VALUE_SLAPAF) == 0 && strcmp(keywordModels[i]->GetName(), ATTR_VALUE_CONSTRAINTS) == 0) {
                                keywordModels[i]->SetValue(GetKeywordData()->GetOptConstraint());
                        } else {
                        // std::cout << "ModuleProperty::InsertKeywordModelAfter KEYWORD_KIND_CUSTOM " << KEYWORD_KIND_CUSTOM << std::endl;
                                if (strcmp(GetName(), ATTR_VALUE_GATEWAY) == 0 && strcmp(keywordModels[i]->GetName(), ATTR_VALUE_COORD) == 0) {
                                        // std::cout << "ModuleProperty::InsertKeywordModelAfter ATTR_VALUE_COORD CUsTOM " << ATTR_VALUE_COORD << std::endl;
                                        keywordModels[i]->SetValue(GetKeywordData()->GetXYZFile());
                                        if(GetKeywordData()->GetXYZFile()==NULL){
                                                SimuProject* pProject = Manager::Get()->GetProjectManager()->GetSelectedProject();
                                        wxString molFile = ProjectTreeItemData::GetFileName(pProject, wxEmptyString, ITEM_MOLECULE);
                                        wxString xyzfilename =FileUtil::ChangeFileExt(molFile, wxT(".xyz"));
                                                char        *xyzFile;
                                                StringUtil::String2CharPointer(&xyzFile,xyzfilename);
                                                // std::cout << "xyzFile: " << xyzFile << std::endl;
                                                keywordModels[i]->SetValue(xyzFile);
                                        }
                                } else {
                                        
                                }
                        }
                case KEYWORD_KIND_UNKNOWN:
                        needShowSubelements = true;
                        keywordModels[i] = new TextCtrlModel(this, elems[i]);
                        break;
                default:
                        keywordModels[i] = NULL;
                }
                if (keywordModels[i] != NULL) {
                        keywordModels[i]->SetGroupRadio(isGroupRadio);
                }
        }
        if (elems) free(elems);
}

/**
 * When the given keywordmodel has been selected or not,
 * this function will search all its exclisive keywords
 * and those keywords who require this keyword,
 * update their states accordingly.
 */
void ModuleProperty::UpdateSelection(const KeywordModel* updatedKeywordModel) {
        int i = 0;
        KeywordModel* tmpModel = NULL;
        if (NULL == updatedKeywordModel) return;

        for (i = 0; i < GetSubElementCount(); i++) {
                tmpModel = GetKeywordModel(i);
                if (tmpModel == NULL || strcmp(tmpModel->GetName(), updatedKeywordModel->GetName()) == 0) {
                        continue;
                }
                tmpModel->ReceiveSelection(updatedKeywordModel);
        }
}

void ModuleProperty::UpdateRelation(void) {
        int i = 0;
        KeywordModel* tmpModel = NULL;

        for (i = 0; i < GetSubElementCount(); i++) {
                tmpModel = GetKeywordModel(i);
                if (tmpModel != NULL && tmpModel->GetKind() == KEYWORD_KIND_SINGLE) {
                        UpdateSelection(tmpModel);
                }
        }
}

bool ModuleProperty::SaveAsInputFormat(FILE* fp) {
     int i = 0;
     KeywordModel* tmpModel = NULL;
     bool isEnv = IsEnvModule();
     if (!isEnv) {
          if (!IsCommentModule()) {
               fprintf(fp, "\n &%s\n", GetName());
          }
          for (i = 0; i < GetSubElementCount(); i++) {
               tmpModel = GetKeywordModel(i);
               if (tmpModel != NULL && tmpModel->IsEnabled()) {
                    tmpModel->SaveAsInputFormat(fp, isEnv);
               }
          }
     }
     return true;
}

/*
bool ModuleProperty::SaveAsInputFormat(FILE* fp) {
        int i = 0;
        KeywordModel* tmpModel = NULL;

        bool isEnv = IsEnvModule();

        if (isEnv) {
                fprintf(fp, "%s\n", CONST_ENV_START);
        } else if (!IsCommentModule()) {
                //fprintf(fp, "\n &%s &END\n", GetName());
                fprintf(fp, "\n &%s\n", GetName());
        }
        for (i = 0; i < GetSubElementCount(); i++) {
                tmpModel = GetKeywordModel(i);
                if (tmpModel != NULL && tmpModel->IsEnabled()) {
                        tmpModel->SaveAsInputFormat(fp, isEnv);
                }
        }
        //if(!isEnv && !IsCommentModule()) {
        //    fprintf(fp, "%s\n", CONST_END_OF_INPUT);
        //}
        if (isEnv) {
                fprintf(fp, "%s\n", CONST_ENV_END);
        }
        return true;
}
*/

void ModuleProperty::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
        char* nonBlankBuf;
        //char* preKeywordName = NULL;
        KeywordModel* keywordModel = NULL;
        KeywordModel* preKeywordModel = NULL;
        bool started = false;
        KeywordModel* keywordModelSelect = NULL;
        bool isEnv = IsEnvModule();

        //FileUtil::ShowInfoMessage(wxString::Format(wxT("Module - %s"), GetName()));
        while (!feof(fp)) {
                if (strlen(buf) <= 0) {
                        if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                }
                if (InputParser::IsEndOfInput(buf)) break;
                //FileUtil::ShowInfoMessage(wxString::Format(wxT("Module =%s= Line - %s"), GetName(), buf));
                nonBlankBuf = StringLeftTrim(buf, NULL);
                if (nonBlankBuf == NULL || strlen(nonBlankBuf) <= 0) {
                        buf[0] = 0;
                        continue;   //blank line;
                }
                if (IsStringEqualNoCase(nonBlankBuf, CONST_END_OF_INPUT, strlen(CONST_END_OF_INPUT))) {
                        buf[0] = 0;
                        break;
                }
                if (IsStringEqualNoCase(nonBlankBuf, CONST_ENV_END, strlen(CONST_ENV_END))) {
                        buf[0] = 0;
                        break;
                }
                if (GetPropType() == ELEM_PROP_TYPE_GROUP && IsStringEqualNoCase(nonBlankBuf, CONST_END_OF, strlen(CONST_END_OF))) {
                        // for group with kind of block, this is the end of a block
                        buf[0] = 0;
                        break;
                }
                if (InputParser::IsCommandStart(nonBlankBuf)) {
                        if (isEnv) {
                                nonBlankBuf = InputParser::RemoveCommandPrefix(nonBlankBuf);
                                if (IsStringEqualNoCase(nonBlankBuf, CONST_EXPORT, strlen(CONST_EXPORT))) {
                                        nonBlankBuf += strlen(CONST_EXPORT);
                                        nonBlankBuf = StringLeftTrim(nonBlankBuf, NULL);
                                }
                        } else {
                                //the start of a command means the end of this module
                                if (!GetKeywordData()->IsInternalCommand(InputParser::RemoveCommandPrefix(nonBlankBuf))) {
                                        if (IsCommentModule()) {
                                                //unrecognize command
                                                keywordModel = GetKeywordModel(0);
                                                keywordModel->ReadFromInputFormat(fp, buf, bufSize, availableXYZFile);
                                                //FileUtil::ShowInfoMessage(wxString::Format(wxT("After Comment Line - %s"), buf));
                                        }
                                        break;
                                }
                        }
                }
                //if(nonBlankBuf[0] == '&' && strlen(nonBlankBuf) > 1) {
                if (InputParser::IsModuleStart(nonBlankBuf)) {
                        if (started) {
                                break;
                        } else {
                                started = true;
                        }
                        //module name line
                        if (!InputParser::IsCommentBegin(buf)) {
                                int semicolonIndex = SubStringIndex(buf, ";", 0);
                                if (semicolonIndex >= 0) {
                                        strcpy(buf, buf + semicolonIndex + 1);
                                } else {
                                        buf[0] = 0;
                                }
                        }
                        continue;
                }

                if (InputParser::IsCommentLine(&nonBlankBuf[0])) {
                        if (IsCommentModule()) {
                                //comment line; only for comment module
                                keywordModel = GetKeywordModel(0);
                                keywordModel->ReadFromInputFormat(fp, buf, bufSize, availableXYZFile);
                                //FileUtil::ShowInfoMessage(wxString::Format(wxT("After Comment Line - %s"), buf));
                                break;
                        } else {
                                buf[0] = 0;
                                continue;
                        }
                }

                //read keyword name
                if (keywordModel != NULL) {
                        // there are still some not implemented keyword, use this to keep order
                        // otherwise the content may go to the first userinput
                        preKeywordModel = keywordModel;
                }
                //FileUtil::ShowInfoMessage(wxString::Format("keyword %s", nonBlankBuf));
                keywordModelSelect = NULL;
                keywordModel = GetKeywordModel(nonBlankBuf, &keywordModelSelect);
                if (keywordModel != NULL) {
                        // FileUtil::ShowInfoMessage(wxString::Format(wxT("Keyword: %s in module %s"), keywordModel->GetName(), GetName()));
                        // This is a line of keyword
                        int equalIndex = SubStringIndex(buf, "=", 0);
                        if (equalIndex >= 0) {
                                strcpy(buf, buf + equalIndex + 1);
                        } else {
                                buf[0] = 0;
                        }
                        keywordModel->ReadFromInputFormat(fp, buf, bufSize, availableXYZFile);
                        if (keywordModelSelect) {
                                ((SelectionModel*)keywordModelSelect)->SetSelectedModel(keywordModel);
                        }
                } else {
                        //FileUtil::ShowInfoMessage(wxString::Format(wxT("No Keyword: %s in module %s"), buf, GetName()));
                        //if not recognized content, put some on the default keyword model.
                        if (preKeywordModel != NULL && preKeywordModel->IsUndefined()) {
                                preKeywordModel->ReadFromInputFormat(fp, buf, bufSize, availableXYZFile);
                        } else {
                                if (preKeywordModel != NULL) {
                                        // insert a keywordmodel
                                        keywordModel = InsertKeywordModelAfter(preKeywordModel->GetName());
                                } else {
                                        keywordModel = InsertKeywordModelAfter(NULL);
                                }
                                keywordModel->ReadFromInputFormat(fp, buf, bufSize, availableXYZFile);
                        }
                }
                //buf[0] = 0;
                //
        }
}

void ModuleProperty::DuplicateValue(ElementProperty* const oldElemProp) {
        KeywordModel* curKeywordModel = NULL;
        KeywordModel* oldKeywordModel = NULL;

        for (int i = 0; i < GetSubElementCount(); i++) {
                curKeywordModel = GetKeywordModel(i);
                oldKeywordModel = ((ModuleProperty*)oldElemProp)->GetKeywordModel(i);
                if (curKeywordModel != NULL && oldKeywordModel != NULL && oldKeywordModel->IsEnabled()) {
                        curKeywordModel->DuplicateValue(oldKeywordModel);
                }
        }
}

char* ModuleProperty::GetProjectName(void) const {
        KeywordModel* keyModel = GetKeywordModel(ATTR_VALUE_ENV_PROJECT);
        if(keyModel != NULL) {
                return keyModel->GetValue();
        }
        return NULL;
}

char* ModuleProperty::GetOutputDir(void) const {
        KeywordModel* keyModel = GetKeywordModel(ATTR_VALUE_ENV_OUTPUTDIR);
        if(keyModel != NULL) {
                return keyModel->GetValue();
        }
        return NULL;
}
