/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <string.h>
#include <stdlib.h>

#include "textctrlmodel.h"
#include "inputparser.h"
#include "keywordconst.h"
#include "xmlparser.h"
#include "cstringutil.h"
#include "stringutil.h"

TextCtrlModel::TextCtrlModel(ModuleProperty* parentModule, XmlElement* elem) : KeywordModel(parentModule, elem){
        isUndefined = false;
        if(strcmp(ATTR_VALUE_UNDEFINED, GetName()) == 0) {
                isUndefined = true;
        }
}

char* TextCtrlModel::GetDefaultValue(void) {
    return GetAttributeValue(ATTR_DEFAULTVALUE);
}

bool TextCtrlModel::IsComment(void) const {
    return parentModule->IsCommentModule();
}

bool TextCtrlModel::SaveAsInputFormat(FILE* fp, bool isEnv) {
    char* value = GetValue();
    int len = 0;
    if(value != NULL) {
        if(isUndefined) {
            len = strlen(value);
            fprintf(fp, "%s", value);
            if(value[len - 1] != '\n') {
                fprintf(fp, "\n");
            }
        }else if(isEnv) {
                        fprintf(fp, "%s %s=%s\n", CONST_EXPORT_OUTPUT, GetName(), value);
                }else{
            fprintf(fp, "%s\n", GetName());
            //if(GetKind() == KEYWORD_KIND_STRINGS) {
            //    fprintf(fp, "%d\n", GetLineCount(value, strlen(value)));
            //}
            fprintf(fp, " %s\n", value);
            if(IsOptConstraint()) {
                fprintf(fp, "End of %s\n", GetName());
//                wxString value=StringUtil::CharPointer2String(GetName());
//                wxMessageBox(value+wxT("-TextCtrlModel"));
            }
        }
    }
    return true;
}

void TextCtrlModel::AppendData(char* buf) {
        char* orgValue = NULL;
        char* newValue = NULL;
        char* copyValue = NULL;
        int len = 0;
    int newlen  = 0;

    int k = SubStringIndex(buf, "*/", 0);

        orgValue = GetValue();
        if(orgValue) {
                len = strlen(orgValue);
        }

    if(k > 0) {
        newlen = k + 2;
    }else if(k == 0) {
        newlen = 2;
    }else {
        newlen = strlen(buf);
    }

        if(newlen > 0) {
        len += newlen + 3;
        newValue = (char*)malloc(len*sizeof(char));
        if(newValue) {
                newValue[0] = 0;
                if(orgValue)
                        strcat(newValue, orgValue);
                StringCopy(&copyValue, buf, newlen);
                strcat(newValue, copyValue);
                if(k >= 0) {
                strcat(newValue, "\n");
            }
                SetValue(newValue);
                free(newValue);
                free(copyValue);
        }
    }
    if(k > 0) {
        InputParser::IsCommentEnd(buf, true);
    }else {
        buf[0] = 0;
    }
}

void TextCtrlModel::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
    char* nonBlankBuf = NULL;
    bool isMultiLineComment = false;
    if(isUndefined) {//parentModule->IsCommentModule()) {
        isMultiLineComment = InputParser::IsMultiLineComment(buf);
        AppendData(buf);
        while(!feof(fp)) {
            if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
            if(InputParser::IsEndOfInput(buf)) break;
            nonBlankBuf = StringLeftTrim(buf, NULL);
            if(nonBlankBuf == NULL || strlen(nonBlankBuf) <= 0) continue;   //blank line;

            if(isMultiLineComment) {
                // this conditon should be put first, other when a line starts with */ will be regarded as a comment start line.
                if(!InputParser::IsCommentEnd(&nonBlankBuf[0])) {
                    AppendData(buf);
                }else {
                    AppendData(buf);
                    break;
                }
            }else if(InputParser::IsCommentLine(&nonBlankBuf[0])) {
                AppendData(buf);
            }else {
                break;
            }
        }
    }else {
        //normal textctrl model
        if(GetKind() != KEYWORD_KIND_STRINGS) {
            while(!feof(fp)) {
                                if(strlen(buf) <= 0) {
                        if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                                }
                if(InputParser::IsEndOfInput(buf)) break;
                nonBlankBuf = StringLeftTrim(buf, NULL);
                if(nonBlankBuf == NULL || strlen(nonBlankBuf) <= 0) continue;   //blank line;
                nonBlankBuf = StringRightTrim(nonBlankBuf, NULL);
                SetValue(nonBlankBuf);
                buf[0] = 0;
                break;
            }
        }else {
            if(IsOptConstraint() && GetValue() != NULL) {
                    //clear optimization constraints setted by molgui
                                    SetValue(NULL);
                        }
            while(!feof(fp)) {
                                if(strlen(buf) <= 0) {
                        if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                                }
                if(InputParser::IsEndOfInput(buf)) break;

                nonBlankBuf = StringLeftTrim(buf, NULL);
                if(nonBlankBuf == NULL || strlen(nonBlankBuf) <= 0) {
                                        buf[0] = 0;
                                        continue;   //blank line;
                                }
                                if(InputParser::IsCommentLine(&nonBlankBuf[0])) {
                                        buf[0] = 0;
                                        continue;
                                }

                if(parentModule->GetKeywordModel(nonBlankBuf)
                ||IsStringEqualNoCase(nonBlankBuf, CONST_END_OF_INPUT, strlen(CONST_END_OF_INPUT))
                ||InputParser::IsModuleStart(nonBlankBuf) ||InputParser::IsCommandStart(nonBlankBuf)) {
                    // start of another keyword
                    break;
                }
                if(IsStringEqualNoCase(nonBlankBuf, CONST_END_OF, strlen(CONST_END_OF))) {
                    buf[0] = 0;
                    break;
                }
                AppendData(nonBlankBuf);
                buf[0] = 0;
            }
        }
    }
}

bool TextCtrlModel::IsOptConstraint(void ) const {
    return strcmp(GetName(), ATTR_VALUE_CONSTRAINTS) == 0 && strcmp(GetParentModule()->GetName(), ATTR_VALUE_SLAPAF) == 0;
}
