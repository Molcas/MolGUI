/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/statline.h>
#include <wx/notebook.h>

#include "tableeditdlg.h"
#include "keywordmodel.h"
#include "keywordconst.h"

BEGIN_EVENT_TABLE(TableEditDlg, wxDialog)
    EVT_BUTTON(wxID_OK, TableEditDlg::OnDialogOK)
    EVT_BUTTON(wxID_CANCEL, TableEditDlg::OnDialogCancel)
    EVT_CLOSE(TableEditDlg::OnClose)
END_EVENT_TABLE()

TableEditDlg::TableEditDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style) {
}

TableEditDlg::~TableEditDlg() {
}

void TableEditDlg::CreateControls(void) {
    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer* bsTopLine = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnAddRow = new wxButton(this, wxID_ANY, wxT("&Add"), wxDefaultPosition, wxSize(100,30));
    wxButton * btnDelRow = new wxButton(this, wxID_ANY, wxT("&Delete"), wxDefaultPosition, wxSize(100,30));
    bsTopLine->Add(btnAddRow, 0, wxALL, 2);
    bsTopLine->Add(btnDelRow, 0, wxALL, 2);

    wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnOk = new wxButton(this, wxID_OK, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    wxButton * btnCancel = new wxButton(this, wxID_CANCEL, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    bsBottom->Add(btnOk, 0, wxALL, 2);
    bsBottom->Add(btnCancel, 0, wxALL, 2);

    bsTop->Add(bsTopLine, 0, wxALIGN_LEFT|wxALL, 2);
    bsTop->AddSpacer(3);
    bsTop->Add(bsBottom, 0, wxALIGN_RIGHT|wxALL, 2);

    this->SetSizer(bsTop);

    Center();
}

void TableEditDlg::OnDialogOK(wxCommandEvent& event) {
    HideDialog(wxID_OK);
}

void TableEditDlg::OnDialogCancel(wxCommandEvent& event) {
    HideDialog(wxID_CANCEL);
}

void TableEditDlg::OnClose(wxCloseEvent& event){
    HideDialog(wxID_CANCEL);
}

void TableEditDlg::HideDialog(int returnCode) {
    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
        Destroy();
}
