/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
*               2021, Ignacio Fdez. Galván                             *
***********************************************************************/

#include "molcasfiles.h"

#include <wx/filename.h>
#include <wx/arrstr.h>
#include <wx/gdicmn.h>
#include <wx/txtstrm.h>
#include <wx/wfstream.h>

#include "fileutil.h"
#include "stringutil.h"
#include "envutil.h"
#include "mingutil.h"
#include "tools.h"

#include "renderingdata.h"
#include "molfile.h"
#include "xyzfile.h"
#include "fileeditdlg.h"

#include "envsettings.h"
#include "dlgoutput.h"
#include "manager.h"
#include "wx/filename.h"

#define LINE_BUF_SIZE 512

#ifndef POPUPMENU_PRJRES_ID_TEXT
int POPUPMENU_PRJRES_ID_TEXT = wxNewId();
int POPUPMENU_PRJRES_ID_GV = wxNewId();
#endif


MolcasOutputInfo        g_mopInfo[] = {
    {wxT(".input"), wxT("Input"),        ITEM_JOB_RESULT_INPUT_MOLCAS,        ITEM_JOB_RESULT_OUTER_INPUT_MOLCAS,        OUT_COMMON_FILE},
    {wxT(".output"), wxT("Output"),  ITEM_JOB_RESULT_OUTPUT_MOLCAS,        ITEM_JOB_RESULT_OUTER_OUTPUT_MOLCAS, OUT_COMMON_FILE},
    {wxT(".error"), wxT("Error"), ITEM_JOB_RESULT_ERROR_MOLCAS,        ITEM_JOB_RESULT_OUTER_ERROR_MOLCAS,        OUT_COMMON_FILE},
    {wxT(".grid"), wxT("Density"),   ITEM_JOB_RESULT_GRID_MOLCAS,        ITEM_JOB_RESULT_OUTER_GRID_MOLCAS,        OUT_COMMON_FILE}, //.a.grid; .b.grid
    {wxT(".structure"), wxT("Energy Statistics"), ITEM_JOB_RESULT_STRUCTURE_MOLCAS, ITEM_JOB_RESULT_OUTER_STRUCTURE_MOLCAS, OUT_COMMON_FILE},
    {wxT(".xml"), wxT("Summary"),          ITEM_JOB_RESULT_XMLDUMP_XML_MOLCAS, ITEM_JOB_RESULT_OUTER_XMLDUMP_XML_MOLCAS, OUT_COMMON_FILE},

    {wxT(".GssOrb"), wxT("GssOrb"),        ITEM_JOB_RESULT_GSSORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_GSSORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".ScfOrb"), wxT("ScfOrb"),        ITEM_JOB_RESULT_SCFORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_SCFORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".UhfOrb"), wxT("UhfOrb"),        ITEM_JOB_RESULT_UHFORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_UHFORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".UnaOrb"), wxT("UnaOrb"),        ITEM_JOB_RESULT_UNAORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_UNAORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".RasOrb"), wxT("RasOrb"),        ITEM_JOB_RESULT_RASORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_RASORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".Pt2Orb"), wxT("Pt2sOrb"),        ITEM_JOB_RESULT_PT2ORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_PT2ORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".MP2Orb"), wxT("MP2Orb"),        ITEM_JOB_RESULT_MP2ORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_MP2ORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".GvOrb"), wxT("GvOrb"),        ITEM_JOB_RESULT_GVORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_GVORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".IPAOrb"), wxT("IPAOrb"),        ITEM_JOB_RESULT_IPAORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_IPAORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".DPAOrb"), wxT("DPAOrb"),        ITEM_JOB_RESULT_DPAORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_DPAORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".LocOrb"), wxT("LocOrb"),        ITEM_JOB_RESULT_LOCORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_LOCORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".SpdOrb"), wxT("SpdOrb"),        ITEM_JOB_RESULT_SPDORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_SPDORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".SiOrb"), wxT("SiOrb"),        ITEM_JOB_RESULT_SIORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_SIORB_MOLCAS, OUT_ORB_FILE},
    {wxT(".SsOrb"), wxT("SsOrb"),        ITEM_JOB_RESULT_SSORB_MOLCAS,        ITEM_JOB_RESULT_OUTER_SSORB_MOLCAS, OUT_ORB_FILE},

    {wxT(".geo.molden"), wxT("Intermediate Result"),                ITEM_JOB_RESULT_GEO_MOLDEN_MOLCAS,                        ITEM_JOB_RESULT_OUTER_GEO_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".freq.molden"), wxT("Frequence"),                                ITEM_JOB_RESULT_FREQ_MOLDEN_MOLCAS,                        ITEM_JOB_RESULT_OUTER_FREQ_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".mp2.molden"), wxT("Mp2"),                                                ITEM_JOB_RESULT_MP2_MOLDEN_MOLCAS,                        ITEM_JOB_RESULT_OUTER_MP2_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".guessorb.molden"), wxT("GuessOrb"),                        ITEM_JOB_RESULT_GUESSORB_MOLDEN_MOLCAS,                ITEM_JOB_RESULT_OUTER_GUESSORB_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".localisation.molden"), wxT("Localisation"),        ITEM_JOB_RESULT_LOCALISATION_MOLDEN_MOLCAS,        ITEM_JOB_RESULT_OUTER_LOCALISATION_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".LoProp.molden"), wxT("LoProp"),                                ITEM_JOB_RESULT_LOPROP_MOLDEN_MOLCAS,                ITEM_JOB_RESULT_OUTER_LOPROP_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".scf.molden"), wxT("SCF"),                                                ITEM_JOB_RESULT_SCF_MOLDEN_MOLCAS,                        ITEM_JOB_RESULT_OUTER_SCF_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".casvb.molden"), wxT("Casvb"),                                        ITEM_JOB_RESULT_CASVB_MOLDEN_MOLCAS,                ITEM_JOB_RESULT_OUTER_CASVB_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".rasscf.molden"), wxT("RasScf"),                                ITEM_JOB_RESULT_RASSCF_MOLDEN_MOLCAS,                ITEM_JOB_RESULT_OUTER_RASSCF_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".mep.molden"), wxT("Mep"),                                                ITEM_JOB_RESULT_MEP_MOLDEN_MOLCAS,                        ITEM_JOB_RESULT_OUTER_MEP_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".saddle.molden"), wxT("Saddle"),                                ITEM_JOB_RESULT_SADDLE_MOLDEN_MOLCAS,                ITEM_JOB_RESULT_OUTER_SADDLE_MOLDEN_MOLCAS, OUT_MOLDEN_FILE},
    {wxT(".irc.molden"), wxT("Irc"),                                                ITEM_JOB_RESULT_IRC_MOLDEN_MOLCAS,                        ITEM_JOB_RESULT_OUTER_IRC_MOLDEN_MOLCAS, OUT_MOLDEN_FILE}
};

/*
instruction:        By default, the output files of the job will be under "projectDir/jobName" if the "MOLCAS_OUTPUT" is not set,
                                or, only the ".input; .output; .error; .status" file will be under this directory, other files will be under:
                                Mingutil::GetJobOutputDir();
*/
#define        DEF_DIR_FILE_NUM        5
wxString        defDirFileSuffix[DEF_DIR_FILE_NUM] = {
    wxT(".input"),
    wxT(".output"),
    wxT(".log"),
    wxT(".error"),
    wxT(".err")
};

BEGIN_EVENT_TABLE(DlgGridSelect, wxDialog)
    EVT_BUTTON(btnID_ALPHA, DlgGridSelect::OnAlpha)
    EVT_BUTTON(btnID_BETA, DlgGridSelect::OnBeta)
    EVT_CLOSE(DlgGridSelect::OnClose)
END_EVENT_TABLE()

MolcasFiles::MolcasFiles(SimuProject *simuProject, wxString resultDir) : AbstractProjectFiles(simuProject, resultDir) {
}

MolcasFiles::~MolcasFiles() {

}

bool MolcasFiles::CreateInputFile(void) {
    wxString molFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_MOLECULE);
    //        wxString jobFile = ProjectTreeItemData::GetFileName(m_pSimuProject, wxEmptyString, ITEM_JOB_INFO);

    if(wxFileExists(molFile) ) {
        RenderingData tmpData(NULL);
        MolFile::LoadFile(molFile, &tmpData);
        //        XYZFile::SaveFile(m_pSimuProject->GetResultFileName(m_name) + wxT(".xyz"), tmpData.GetAtomBondArray(), false);
        //        wxCopyFile(jobFile, m_pSimuProject->GetResultFileName(m_name) + wxT(".input"));
        return true;
    }
    return false;
}

wxString MolcasFiles::GetExecPara(void) {
    wxString strreturn = wxEmptyString;
    strreturn = m_pSimuProject->GetResultFileName(m_name) + wxT(".input");
#if defined (__WINDOWS__)
    wxFileName *pfile = new wxFileName(strreturn);
    strreturn = wxT("/cygdrive/") + strreturn.Left(1) + pfile->GetPathWithSep(wxPATH_UNIX) + pfile->GetFullName();
#endif
    return strreturn;
}

void MolcasFiles::BuildResultTree(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode) {

    if(m_pSimuProject == NULL)
        return;
    if(!jobResultNode.IsOk())
        return;
    pTree = Manager::Get()->GetProjectManager()->GetTree();
    if(pTree == NULL)
        return;

    BuildItems(pTree, jobResultNode);

    BuildOuterFileTree(pTree, jobResultNode); //job files from other jobs
    pTree->Collapse(jobResultNode);
}
bool MolcasFiles::BuildOuterFileTree(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode)
{
    wxString         jobName = pTree->GetItemText(jobResultNode);
    wxString        outerFilePath = m_pSimuProject->GetFileName();

    outerFilePath = outerFilePath.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + jobName + platform::PathSep() + wxRESULT_OUTER_FILE_DIR;
    //wxMessageBox(outerFilePath);
    wxString fileName, fileSuffix, title;
    int itemNumber = sizeof(g_mopInfo) / sizeof(MolcasOutputInfo);
    wxTreeItemId        outerFileNode;
    ProjectTreeItemData *itemData = NULL;
    wxDir   outerFileDir(outerFilePath);
    //wxMessageBox(outerFilePath);
    if(!::wxDirExists(outerFilePath))
        return false;
    bool cont = outerFileDir.GetFirst(&fileName, wxEmptyString, wxDIR_DEFAULT);
    while ( cont ) {
        //wxMessageBox(fileName);
        if(fileName.IsSameAs(wxT(".")) || fileName.IsSameAs(wxT("..")))
            continue;
        fileSuffix = FileUtil::GetSuffix(fileName);
        if(fileName.Left(fileName.Len() - fileSuffix.Len() - 1).IsSameAs(jobName)) /////////
            continue;
        //wxMessageBox(fileSuffix);
        for(int i = 0; i < itemNumber; i++) {
            if(fileSuffix.IsSameAs(g_mopInfo[i].fileSuffix.AfterFirst(wxT('.')))) { //available outer file;
                if(!outerFileNode.IsOk()) {
                    outerFileNode = GetOuterFileNode(pTree, jobResultNode);
                }
                //                                title=fileName.Left(fileName.Len()-fileSuffix.Len());
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name,  g_mopInfo[i].ofItemType);
                pTree->AppendItem(outerFileNode, fileName, TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
                break;
            } else if(fileSuffix.IsSameAs(wxT("xmldump.xml"))) {
                if(!outerFileNode.IsOk()) {
                    outerFileNode = GetOuterFileNode(pTree, jobResultNode);
                }
                itemData = new ProjectTreeItemData(m_pSimuProject, m_name,  ITEM_JOB_RESULT_OUTER_XMLDUMP_XML_MOLCAS);
                pTree->AppendItem(outerFileNode, fileName, TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
                break;
            }
        }

        cont = outerFileDir.GetNext(&fileName);
    }
    pTree->Expand(jobResultNode);
    if(outerFileNode.IsOk())
        pTree->Expand(outerFileNode);
    return true;
}
wxTreeItemId        MolcasFiles::GetOuterFileNode(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode)
{
    wxTreeItemId        outerFileNode = 0;
    int itemCount = pTree->GetChildrenCount(jobResultNode, false);
    wxTreeItemIdValue        cookie;
    wxTreeItemId        child = pTree->GetFirstChild(jobResultNode, cookie);
    ProjectTreeItemData *item;
    for(int j = 0; j < itemCount; j++) {
        item = child.IsOk() ? (ProjectTreeItemData *)pTree->GetItemData(child) : NULL;
        if(item == NULL)        break;
        if(item->GetType() == ITEM_JOB_RESULT_OUTER_FILE) {
            outerFileNode = child;
            break;
        }
        child = pTree->GetNextChild(jobResultNode, cookie);
    }
    if(!outerFileNode.IsOk()) { //create a new one
        item = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_OUTER_FILE);
        outerFileNode = pTree->AppendItem(jobResultNode, wxT("OuterFile"),  TREE_IMAGE_JOB_RESULTS,  TREE_IMAGE_JOB_RESULTS, item);
    }
    if(pTree->GetChildrenCount(outerFileNode) > 0)
        pTree->DeleteChildren(outerFileNode);
    return outerFileNode;
}

void MolcasFiles::AddFileTypeToTree(wxTreeCtrl *pTree, wxTreeItemId &parentNode, wxString &type, wxArrayString &files)
{
    ProjectTreeItemData *itemData = NULL;
    // By default we add the files to the Job node
    wxTreeItemId tmpParent = parentNode;
    wxString itemName = wxEmptyString;
    // If more than one file of this type, create a sub-directory
    if(files.GetCount() > 1)
    {
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_DIR);
        if(type == wxT("h5")) {
            itemName = wxT("HDF5");
        } else {
            itemName = StringUtil::UpperFirstChar(type);
        }
        tmpParent = pTree->AppendItem(parentNode, itemName, TREE_IMAGE_JOB_RESULTS, TREE_IMAGE_JOB_RESULTS, itemData);
    }
    // Add the files to the tree
    for(unsigned int j = 0; j < files.GetCount(); j++)
    {
        wxFileName fileName(files[j]);
        // Remove project name
        itemName = fileName.GetFullName().AfterFirst('.');
        // Capitalize first letter
        itemName[0] = wxToupper(itemName[0]);
        itemData = new ProjectTreeItemData(m_pSimuProject, fileName.GetFullPath(), ITEM_JOB_RESULT_OUTPUT_MOLCAS);
        pTree->AppendItem(tmpParent, itemName, TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
    }
    // cout << defDirFileSuffix[i].Mid(1).fn_str() << ": " << files[0].fn_str() << endl;
}

void MolcasFiles::BuildItems(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode)
{
    ProjectTreeItemData *itemData = NULL;
    wxTreeItemId tmpParent, orbitalsNode, moldenNode, otherFilesNode;
    wxString tmpFileName = wxEmptyString;

    wxString jobName = pTree->GetItemText(jobResultNode);
    wxString defJobDir = m_pSimuProject->GetFileName();
    defJobDir = defJobDir.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + jobName;

    // Find all files in the job directory
    wxArrayString filesList;
    wxDir::GetAllFiles(defJobDir, &filesList);
    filesList.Sort();

    // Store the files in a hash map. Key: type, Value: wxArrayString (the file names).
    wxArrayStringHashMap filesTypesMap;
    wxArrayStringHashMap::iterator it;
    wxSortedArrayString typesList;
    wxArrayStringHashMap moldenFilesTypesMap;
    wxSortedArrayString moldenTypesList;
    // Will contain files not starting with jobName.
    wxSortedArrayString otherFiles;
    for(unsigned int i = 0; i < filesList.GetCount(); i++)
    {
        wxString fileName = wxFileName::FileName(filesList[i]).GetFullName();
        if(fileName.StartsWith(jobName+wxT(".")))
        {
            wxString suffix = FileUtil::GetMolcasFileType(filesList[i]);
            if(suffix.IsSameAs(wxT("molden")))
            {
                // Remove .molden from file name to get the real type
                wxString tmp = filesList[i];
                tmp.Replace(wxT(".molden"), wxT(""));
                suffix = FileUtil::GetMolcasFileType(tmp);
                moldenFilesTypesMap[suffix].Add(filesList[i]);
                moldenTypesList.Add(suffix);
            }
            else
            {
                filesTypesMap[suffix].Add(filesList[i]);
                typesList.Add(suffix);
            }
        }
        else
        {
            otherFiles.Add(filesList[i]);
        }
    }

    // The ItemData will contain the full path of the file
    // Start with job files (input, output, error etc...).
    for(unsigned int i = 0; i < DEF_DIR_FILE_NUM; i++)
    {
        it = filesTypesMap.find(defDirFileSuffix[i].Mid(1));
        if(it != filesTypesMap.end())
        {
            wxString &type = it->first;
            wxArrayString &files = it->second;
            AddFileTypeToTree(pTree, jobResultNode, type, files);
            filesTypesMap.erase(it);
        }
    }

    // Create Orbitals and Molden folders
    itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_ORB);
    orbitalsNode = pTree->AppendItem(jobResultNode, wxT("Orbitals"), TREE_IMAGE_JOB_RESULTS, TREE_IMAGE_JOB_RESULTS, itemData);

    // Go on with the results files
    for(unsigned int i = 0; i < typesList.GetCount(); i++)
    {
        it = filesTypesMap.find(typesList[i]);
        if(it == filesTypesMap.end())
            continue;
        wxString &type = it->first;
        wxArrayString &files = it->second;
        tmpParent = jobResultNode;
        if(StringUtil::Contains(type, wxT("orb"), true))
            tmpParent = orbitalsNode;
        AddFileTypeToTree(pTree, tmpParent, type, files);
        filesTypesMap.erase(it);
    }
    if(pTree->GetChildrenCount(orbitalsNode) == 0)
        pTree->Delete(orbitalsNode);

    // Go on Molden files
    if(moldenTypesList.GetCount() > 0)
    {
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_MOLDEN);
        moldenNode = pTree->AppendItem(jobResultNode, wxT("Molden"), TREE_IMAGE_JOB_RESULTS, TREE_IMAGE_JOB_RESULTS, itemData);
        for(unsigned int i = 0; i < moldenTypesList.GetCount(); i++)
        {
            it = moldenFilesTypesMap.find(moldenTypesList[i]);
            if(it == moldenFilesTypesMap.end())
                continue;
            wxString &type = it->first;
            wxArrayString &files = it->second;
            tmpParent = moldenNode;
            AddFileTypeToTree(pTree, tmpParent, type, files);
            moldenFilesTypesMap.erase(it);
        }
    }

    // Finish with other files
    if(!otherFiles.IsEmpty())
    {
        itemData = new ProjectTreeItemData(m_pSimuProject, m_name, ITEM_JOB_RESULT_DIR);
        otherFilesNode = pTree->AppendItem(jobResultNode, wxT("Misc"), TREE_IMAGE_JOB_RESULTS, TREE_IMAGE_JOB_RESULTS, itemData);
        for(unsigned int j = 0; j < otherFiles.GetCount(); j++)
        {
            wxFileName fileName(otherFiles[j]);
            // Keep only filename
            wxString itemName = fileName.GetFullName();
            itemData = new ProjectTreeItemData(m_pSimuProject, fileName.GetFullPath(), ITEM_JOB_RESULT_OUTPUT_MOLCAS);
            pTree->AppendItem(otherFilesNode, itemName, TREE_IMAGE_JOB_RESULT_EXIST, TREE_IMAGE_JOB_RESULT_EXIST, itemData);
        }
    }
}

bool MolcasFiles::PostJobTerminated(bool isFinal) {
    wxString outFile = GetOutputFileName();
    if(!wxFileExists(outFile)) {
        return true;        // when the job file does not exist, terminate the processing
    }
    //GetSummary();
    if(!HasSuccess()) //SONG add it, if the first line of sum file is 1, then won't do the surface Cal.
    {
        //        Tools::ShowError(wxT("Sorry, Job Failed!"));
        return true;
    }

    if(!isFinal) {
        //ConvertVib();
    }

    //        GetXYZ();
    //        Tools::ShowStatus(wxT("Job Finished!"),1);
    return true;
}
wxString MolcasFiles::GetOutFileName(wxString &fileSuffix)
{
    //wxMessageBox(fileSuffix);
    //wxMessageBox(m_name);
    wxString outfile = m_pSimuProject->GetResultFileName(m_name), tmpDefDir;
    ///wxMessageBox(outfile);
    tmpDefDir = outfile.BeforeLast(platform::PathSep().GetChar(0));
    //wxMessageBox(outfile);
    wxString        tmpStr = MingUtil::GetJobOutputDir(tmpDefDir);
    //wxMessageBox(outfile);
    for(int j = 0; j < DEF_DIR_FILE_NUM; j++) {
        if(fileSuffix.IsSameAs(defDirFileSuffix[j])) {
            if(fileSuffix.IsSameAs(wxT(".output"))) {
                outfile = GetOutputFileName();
                if(wxFileExists(outfile))
                    break;
            } else if(fileSuffix.IsSameAs(wxT(".error"))) {
                outfile = tmpDefDir + platform::PathSep() + m_name + wxT(".err");
                if(wxFileExists(outfile))
                    break;
            }
            outfile = tmpDefDir + platform::PathSep() + m_name + fileSuffix;
            //wxMessageBox(outfile);
            break;
        } else {
            outfile = tmpStr + platform::PathSep() + m_name + fileSuffix;
        }
    }
    return outfile;
}
wxString MolcasFiles::GetOutputFileName()
{
    wxString        strTmpDir = m_pSimuProject->GetResultFileName(m_name).BeforeLast(platform::PathSep().GetChar(0));
    wxArrayString   was = FileUtil::GetFileBySuffix(strTmpDir, wxT(".log"));
    for(int i = 0; i < (int)was.GetCount(); i++) {
        if(was[i].Find(m_name) != wxNOT_FOUND) {
            return strTmpDir + platform::PathSep() + was[i];
        }
    }
    return m_pSimuProject->GetResultFileName(m_name) + wxT(".output");
}

wxString MolcasFiles::GetInputFileName(void)
{
    return m_pSimuProject->GetResultFileName(m_name) + wxT(".input");
}

bool MolcasFiles::GetSummary()
{
    wxString execDir = EnvSettingsData::Get()->GetChemSoftDir(PROJECT_MOLCAS);
    execDir += platform::PathSep() + EnvUtil::GetProgramName(wxT("getsum"));
    if(!wxFileExists(execDir)) {
        Tools::ShowError(execDir + wxT("  does not exist!"));
        return false;
    }
    wxString infile = GetOutputFileName();
    wxString sumfile = GetSummaryFileName();
    wxString quiproc = execDir + wxT(" ") + infile + wxT(" ") + sumfile;
    //long processId = wxExecute(quiproc, wxEXEC_SYNC);
    wxExecute(quiproc, wxEXEC_SYNC);
    // std::cout << "MolcasFiles::GetSummary: " << quiproc << std::endl;
    //wxCopyFile(jobFile, m_pSimuProject->GetResultFileName(m_name) + wxT(".input"));

    return true;
}

bool MolcasFiles::ConvertVib()
{
    wxString execDir = EnvSettingsData::Get()->GetChemSoftDir(PROJECT_MOLCAS);
    execDir += platform::PathSep() + EnvUtil::GetProgramName(wxT("convervib"));
    if(!wxFileExists(execDir)) {
        Tools::ShowError(execDir + wxT("  does not exist!"));
        return false;
    }
    wxString infile = GetOutputFileName();
    wxString vibfile = GetVibOutputFileName();
    wxString quiproc = execDir + wxT(" ") + infile + wxT(" ") + vibfile;
    //long processId = wxExecute(quiproc, wxEXEC_SYNC);
    wxExecute(quiproc, wxEXEC_SYNC);
    // std::cout << "MolcasFiles::ConvertVib: " << quiproc << std::endl;
    //wxCopyFile(jobFile, m_pSimuProject->GetResultFileName(m_name) + wxT(".input"));

    return true;
}

void MolcasFiles::GetXYZ()
{
    wxString inputDir = m_pSimuProject->GetResultFileName(m_name) + wxT(".opt.xyz");
    wxString xyzDir = m_pSimuProject->GetResultFileName(m_name) + wxT(".xyz");
    if(!wxFileExists(inputDir))
        return;
    else
    {
        if(wxFileExists(xyzDir)) wxRemoveFile(xyzDir);
        wxCopyFile(inputDir, xyzDir, true);
        return;
    }
}

void        MolcasFiles::Display(wxString &outfile, wxString &title)
{
    if(outfile.IsEmpty() || !wxFileExists(outfile))
        return;

    wxString type = FileUtil::GetMolcasFileType(outfile);
    // std::cout << "MolcasFiles::Display type: " << type << std::endl;
    // Get command for this type of file
    wxString cmd = EnvSettingsData::Get()->GetFileTypeCommand(type);
    // std::cout << "MolcasFiles::Display cmd ant: " << cmd << std::endl;
    // wxMessageBox(type+_T("\n")+cmd);
    // If no command display as text file
    if(cmd.IsEmpty())
    {
        DlgOutput *pdlg = new DlgOutput(Manager::Get()->GetAppWindow(), wxID_ANY);
        pdlg->AppendFile(outfile, title);
        pdlg->SetLabel(title);
        pdlg->Show(true);
    }
    else
    {
        // Replace $f by the file name
        //wxString &type2 = type);
        //wxString cmdf;
        // std::cout << "MolcasFiles::Display: cmdf" << cmdf << std::endl;
        if(type == wxT("molden")) {
            wxString moldenFile=EnvUtil::GetMoldenPath();
            cmd = moldenFile + wxT(" ") + outfile;
            // std::cout << "MolcasFiles::Display: cmdf" << cmd << std::endl;
        } else {
            cmd.Replace(wxT("$f"),outfile);
        }
        // std::cout << "MolcasFiles::Display outfile: " << typeid(outfile).name() << std::endl;
        wxExecute(cmd, wxEXEC_ASYNC);
        //wxExecute(wxT("\"")+moldenFile+wxT(" ")+outfile+wxT("\""), wxEXEC_ASYNC);
        // std::cout << "MolcasFiles::Display: " << cmd << std::endl;
        // TODO: get error messages
    }
}

void MolcasFiles::ShowResultItem(ProjectTreeItemData *item)
{
    wxString fileName = item->GetItemVal(); //Full path
    Display(fileName, fileName);
}

bool        MolcasFiles::ModifySummaryFile()
{
    wxString outfile = m_pSimuProject->GetResultFileName(m_name);
    outfile = outfile.BeforeLast(platform::PathSep().GetChar(0));
    outfile = MingUtil::GetJobOutputDir(outfile);
    wxString        orFileName = outfile + platform::PathSep() + wxT("xmldump.xml");
    if(wxFileExists(orFileName)) {
        //wxMessageBox(orFileName);
        wxString         nFileName = outfile + platform::PathSep() + m_name + wxT(".xmldump.xml");
        //wxMessageBox(nFileName);
        wxFileInputStream        ifs(orFileName);//open the file for reading
        wxFileOutputStream        ofs(nFileName);
        if(!ifs.IsOk() || !ofs.IsOk()) //fail to open the file
            return false;
        wxTextInputStream        iftext(ifs);
        wxTextOutputStream        oftext(ofs);
        wxString        tmpStr;
        oftext << wxT("<root>") << endl;
        tmpStr = iftext.ReadLine();
        while(!tmpStr.IsEmpty()) {
            oftext << tmpStr << endl;
            tmpStr = iftext.ReadLine();
        }
        oftext << wxT("</root>") << endl;
    }
    return true;
}
wxString MolcasFiles::GetSuffix(ProjectTreeItemType itemType)
{
    DlgGridSelect dlggridselect(Manager::Get()->GetAppWindow(), wxID_ANY);
    wxString fileSuf = wxEmptyString;
    switch(itemType) {
    case ITEM_MOPACOUTPUT:
        fileSuf = wxT(".mopac");
        break;
    case ITEM_JOB_RESULT_INPUT_MOLCAS:
        fileSuf = wxMINPUT_FILE_SUFFIX;
        break;
    case ITEM_JOB_RESULT_OUTPUT_MOLCAS:
        fileSuf = wxMOUTPUT_FILE_SUFFIX;
        break;
    case ITEM_JOB_RESULT_ERROR_MOLCAS:
        fileSuf = wxERROR_FILE_SUFFIX;
        break;
    case ITEM_JOB_RESULT_GRID_MOLCAS:
        if(HasDensity() == 1)
            fileSuf = wxT(".grid");
        else
        {
            if (dlggridselect.ShowModal() == 1) {
                fileSuf = wxT(".a.grid");
            } else {
                fileSuf = wxT(".b.grid");
            }
        }
        break;
    case ITEM_JOB_RESULT_XMLDUMP_XML_MOLCAS:
        fileSuf = wxT(".xmldump.xml");
        break;
    case ITEM_JOB_RESULT_STRUCTURE_MOLCAS:
        fileSuf = wxT(".structure");
        break;
    case ITEM_JOB_RESULT_MP2_MOLDEN_MOLCAS:
        fileSuf = wxT(".mp2.molden");
        break;
    case ITEM_JOB_RESULT_GUESSORB_MOLDEN_MOLCAS:
        fileSuf = wxT(".guessorb.molden");
        break;
    case ITEM_JOB_RESULT_LOCALISATION_MOLDEN_MOLCAS:
        fileSuf = wxT(".localisation.molden");
        break;
    case ITEM_JOB_RESULT_LOPROP_MOLDEN_MOLCAS:
        fileSuf = wxT(".LoProp.molden");
        break;
    case ITEM_JOB_RESULT_SCF_MOLDEN_MOLCAS:
        fileSuf = wxT(".scf.molden");
        break;
    case ITEM_JOB_RESULT_CASVB_MOLDEN_MOLCAS:
        fileSuf = wxT(".casvb.molden");
        break;
    case ITEM_JOB_RESULT_GEO_MOLDEN_MOLCAS:
        fileSuf = wxT(".geo.molden");
        break;
    case ITEM_JOB_RESULT_FREQ_MOLDEN_MOLCAS:
        fileSuf = wxT(".freq.molden");
        break;
    case ITEM_JOB_RESULT_RASSCF_MOLDEN_MOLCAS:
        fileSuf = wxT(".rasscf.molden");
        break;
    case ITEM_JOB_RESULT_MEP_MOLDEN_MOLCAS:
        fileSuf = wxT(".mep.molden");
        break;
    case ITEM_JOB_RESULT_SADDLE_MOLDEN_MOLCAS:
        fileSuf = wxT(".saddle.molden");
        break;
    case ITEM_JOB_RESULT_IRC_MOLDEN_MOLCAS:
        fileSuf = wxT(".irc.molden");
        break;
    case ITEM_JOB_RESULT_GSSORB_MOLCAS:
        fileSuf = wxT(".GssOrb");
        break;
    case ITEM_JOB_RESULT_SCFORB_MOLCAS:
        fileSuf = wxT(".ScfOrb");
        break;
    case ITEM_JOB_RESULT_UHFORB_MOLCAS:
        fileSuf = wxT(".UhfOrb");
        break;
    case ITEM_JOB_RESULT_UNAORB_MOLCAS:
        fileSuf = wxT(".UnaOrb");
        break;
    case ITEM_JOB_RESULT_RASORB_MOLCAS:
        fileSuf = wxT(".RasOrb");
        break;
    case ITEM_JOB_RESULT_PT2ORB_MOLCAS:
        fileSuf = wxT(".Pt2Orb");
        break;
    case ITEM_JOB_RESULT_MP2ORB_MOLCAS:
        fileSuf = wxT(".Mp2Orb");
        break;
    case ITEM_JOB_RESULT_GVORB_MOLCAS:
        fileSuf = wxT(".gvOrb");
        break;
    case ITEM_JOB_RESULT_IPAORB_MOLCAS:
        fileSuf = wxT(".IPAOrb");
        break;
    case ITEM_JOB_RESULT_DPAORB_MOLCAS:
        fileSuf = wxT(".DPAOrb");
        break;
    case ITEM_JOB_RESULT_LOCORB_MOLCAS:
        fileSuf = wxT(".LocOrb");
        break;
    case ITEM_JOB_RESULT_SPDORB_MOLCAS:
        fileSuf = wxT(".SpdOrb");
        break;
    case ITEM_JOB_RESULT_SIORB_MOLCAS:
        fileSuf = wxT(".SiOrb");
        break;
    case ITEM_JOB_RESULT_SSORB_MOLCAS:
        fileSuf = wxT(".SsOrb");
        break;
    default :
        break;
    }
    return fileSuf;
}
bool MolcasFiles::HasInput()
{
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxMINPUT_FILE_SUFFIX;
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;

}
bool MolcasFiles::HasOutput()
{
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxMOUTPUT_FILE_SUFFIX;
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;

}
bool MolcasFiles::HasEnergy_Statistics() {
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxT(".structure");
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;
}
bool MolcasFiles::HasError_Info() {
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxERROR_FILE_SUFFIX;
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;
}
int MolcasFiles::HasDensity() {
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name);
    tmpFileName = tmpFileName.BeforeLast(platform::PathSep().GetChar(0));
    tmpFileName = MingUtil::GetJobOutputDir(tmpFileName);
    tmpFileName = tmpFileName + platform::PathSep();
    //wxMessageBox(tmpFileName+m_name);
    if(wxFileExists(tmpFileName + m_name + wxT(".grid")))return 1;
    if(wxFileExists(tmpFileName + m_name + wxT(".a.grid")))return 2;
    return -1;
}
bool MolcasFiles::HasOptimization() {
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxT(".geo.molden");
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;

}
bool MolcasFiles::HasFrequence() {
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxT(".freq.molden");
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;
}
bool MolcasFiles::HasGssOrb()
{
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxT(".GssOrb");
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;
}
bool MolcasFiles::HasScfOrb()
{
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxT(".ScfOrb");
    if(wxFileExists(tmpFileName))
        return true;
    else
        return false;
}
bool MolcasFiles::HasDump()
{
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name) + wxT(".xmldump.xml");
    if(wxFileExists(tmpFileName))
        return true;
    tmpFileName = tmpFileName.BeforeLast(platform::PathSep().GetChar(0)) + platform::PathSep() + wxT("xmldump.xml");
    if(wxFileExists(tmpFileName)) {
        //    wxRenameFile(tmpFileName,m_pSimuProject->GetResultFileName(m_name)+wxT(".xmldump.xml"));
        return true;
    }
    return false;
}
DlgGridSelect::DlgGridSelect(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &pos,
                             const wxSize &size, long style ) : wxDialog(parent, id, title, pos, size, style)
{
    CreateGUIControls();
}

DlgGridSelect::~DlgGridSelect()
{

}

void DlgGridSelect::CreateGUIControls()
{
    wxBoxSizer *bsTop = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *bsBottom = new wxBoxSizer(wxHORIZONTAL);

    SetSizer(bsTop);
    pstMsg = new wxStaticText(this, wxID_ANY, wxEmptyString,
                              wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
    pstMsg->SetLabel(wxT("Which type of grid file you want to open?"));
    wxButton        *btnAlpha = new wxButton(this, btnID_ALPHA, wxT("Alpha"), wxPoint(10, 10), wxDefaultSize);
    wxButton        *btnBeta = new wxButton(this, btnID_BETA, wxT("Beta"), wxPoint(10, 10), wxDefaultSize);
    bsTop->Add(pstMsg, 0, wxALL | wxALIGN_CENTER, 5);
    bsBottom->Add(btnAlpha, 0, wxALL, 2);
    bsBottom->Add(btnBeta, 0, wxALL, 2);
    bsTop->Add(bsBottom, 0, wxALL | wxALIGN_CENTER, 5);
    Layout();
    Refresh();
}

void DlgGridSelect::OnAlpha(wxCommandEvent &event) {
    if(IsModal())
    {
        EndModal(btnID_ALPHA);
    }
    else
    {
        SetReturnCode(btnID_ALPHA);
        this->Show(false);
    }

}

void DlgGridSelect::OnBeta(wxCommandEvent &event) {
    if(IsModal())
    {
        EndModal(btnID_BETA);
    }
    else
    {
        SetReturnCode(btnID_BETA);
        this->Show(false);
    }

}




void DlgGridSelect::OnClose(wxCloseEvent &event)
{
    if(IsModal())
    {
        EndModal(btnID_ALPHA);
    }
    else
    {
        SetReturnCode(btnID_ALPHA);
        this->Show(false);
    }
}



bool MolcasFiles::HasSuccess()
{
    wxString tmpFileName = m_pSimuProject->GetResultFileName(m_name);
    if(wxFileExists(tmpFileName + wxT(".log")))
        tmpFileName = tmpFileName + wxT(".log");
    else if(wxFileExists(tmpFileName + wxT(".output")))
        tmpFileName = tmpFileName + wxT(".output");
    else
        return false;
    FILE *fpInput = NULL;
    char buf[LINE_BUF_SIZE];
    int substrIndex = 0;
    char *fnamechar = NULL;
    StringUtil::String2CharPointer(&fnamechar, tmpFileName);
    fpInput = fopen(fnamechar, "r");
    if(fpInput == NULL) {
        printf("Open file error: %s\n", fnamechar);
    }

    while(!feof(fpInput)) {
        memset(buf, 0, sizeof(buf));
        if(fgets(buf, LINE_BUF_SIZE, fpInput) != NULL)
        {
            size_t len = strlen(buf);
            if(len == 0) continue;
            substrIndex = StringUtil::SubStringIndex(buf, "Happy landing", 0);
            if(substrIndex >= 0) {
                return true;
            }
        }
    }
    return false;
}

wxString MolcasFiles::GetExecCommand(void) {
    wxString execProcName = wxEmptyString;
    if(platform::windows) {
        execProcName = wxT("molcas.bat");
    }
    return execProcName;
}

/*void MolcasFiles::PopDispMenu()
{
        wxMenu menu;
        menu.Append(POPUPMENU_PRJRES_ID_TEXT, wxT("Text"),wxT("Show content by text."));
        menu.Append(POPUPMENU_PRJRES_ID_GV, wxT("GV"),wxT("Show content by gv."));
        wxTreeCtrl*        pTree=Manager::Get()->GetProjectManager()->GetTree();
        if(pTree==NULL)
                return;
        wxTreeItemId        itemId=pTree->GetSelection();
        if(!itemId.IsOk())
                return;
        wxRect        rect;
        pTree->GetBoundingRect(itemId,rect,true);
        wxPoint pos(rect.x,rect.y+rect.height);
        pTree->PopupMenu(&menu, pos);
}*/
