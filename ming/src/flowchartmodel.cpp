/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>

#include "elementproperty.h"
#include "flowchartmodel.h"
#include "flowlinkdata.h"

#include "keywordconst.h"
#include "xmlparser.h"

FlowChartModel::FlowChartModel() {
        elemPropHead = NULL;
        flowListener = NULL;
        envModule = NULL;
        flowLinkData = new FlowLinkData(this);
}

FlowChartModel::~FlowChartModel() {
    if(elemPropHead != NULL) {
        DeleteAllElements();
    }
}

void FlowChartModel::RegisterChangeListener(FlowChartChangeListener* listener) {
        flowListener = listener;
}

ElementProperty* FlowChartModel::GetElementPropertyHead(void) const {
    return elemPropHead;
}

void FlowChartModel::ClearElementPropertyHead(void)  {
    elemPropHead = NULL;
}

FlowLinkData* FlowChartModel::GetFlowLinkData(void) const {
    return flowLinkData;
}

ElementProperty* FlowChartModel::AddXmlElement(XmlElement* elem, int elemId) {
        // create corresponding element property.
        ElementProperty* prop = ElementProperty::CreateElementProperty(elem);
        if(prop != NULL) {
                if(elemPropHead == NULL) {
            // the first component in this model.
                        elemPropHead = prop;
                }else {
            // not the first, add after the given element id.
                        elemPropHead->AddElement(prop, elemId);
                }
                if(prop->GetPropType() == ELEM_PROP_TYPE_COMMAND) {
            // re tag link data for draw link info
            flowLinkData->CalcLinkIndex();
        }
        // when there is a change listener, inform it
                if(flowListener)
                        flowListener->OnAddElement(prop, elemId);
        }

        return prop;
}

bool FlowChartModel::IsEmpty(void) const {
    return NULL == elemPropHead;
}

void FlowChartModel::AddFlowChartModel(FlowChartModel* fModel, int elemId) {
    int count = 1;
    ElementProperty* nextProp = NULL;
    ElementProperty* lastPropInModel = fModel->GetElementPropertyHead();
    int firstElemId = -1;

    if(lastPropInModel == NULL || fModel == NULL) return;

    firstElemId = lastPropInModel->GetInternalId();
    while(lastPropInModel->GetNextElement() != NULL) {
        lastPropInModel = lastPropInModel->GetNextElement();
        count++;
    }

    if(elemPropHead == NULL) {
        // the first component in this model.
                elemPropHead = fModel->GetElementPropertyHead();
        }else {
        nextProp = elemPropHead;
            do {
                    if(nextProp->GetInternalId() == elemId || nextProp->GetNextElement() == NULL) {
                            lastPropInModel->SetNextElement(nextProp->GetNextElement());
                            nextProp->SetNextElement(fModel->GetElementPropertyHead());
                            break;
                    }
                    nextProp = nextProp->GetNextElement();
            }while(nextProp != NULL);
    }
        if(flowLinkData)
           flowLinkData->CalcLinkIndex();
        // when there is a change listener, inform it
        if(flowListener)
                flowListener->OnAddElement(firstElemId, count);

        //avoid to delete its property when delete FlowChartModel
        fModel->ClearElementPropertyHead();
        delete fModel;
}

int FlowChartModel::DeleteElements(int internalIds[], int arrayLen) {
    int count = 0;
    int i;
    for(i = 0; i < arrayLen; i++) {
        if(DeleteElement(internalIds[i], false)) {
            count++;
        }
    }
    //for simplicity, retag all link data
    flowLinkData->CalcLinkIndex();
    return count;
}

bool FlowChartModel::DeleteElement(int internalId, bool updateLink) {
    bool succ = false;
    ElementProperty* next = elemPropHead;
    ElementProperty* prev = NULL;
    while(next != NULL) {
        if(next->GetInternalId() == internalId) {
            if(prev == NULL) {
                elemPropHead = next->GetNextElement();
            }else {
                prev->SetNextElement(next->GetNextElement());
            }
            delete next;
            succ = true;
            break;
                }
        prev = next;
        next = next->GetNextElement();
        }
        if(updateLink) {
        flowLinkData->CalcLinkIndex();
    }
    return succ;
}

void FlowChartModel::DeleteAllElements(void) {
        ElementProperty* next = elemPropHead;

        while(next != NULL) {
                elemPropHead = next->GetNextElement();
                delete next;
                next = elemPropHead;
        }
        elemPropHead = NULL;
}

int FlowChartModel::GetLastInternalId(void) {
    int elemId = -1;
        ElementProperty* next = elemPropHead;
        while(next != NULL) {
        elemId = next->GetInternalId();
        next = next->GetNextElement();
        }

    return elemId;
}

bool FlowChartModel::SaveAsInputFormat(const char* fileName) {
    FILE* fp = NULL;
    if(elemPropHead == NULL) return false;
    fp = fopen(fileName, "w");
    if(fp == NULL) {
        printf("No such file: %s\n", fileName);
        return false;
    }

    if(envModule != NULL) {
        envModule->SaveAsInputFormat(fp);
    }
    ElementProperty* next = elemPropHead;
    while(next != NULL) {
        // let each element propertyy decide its output format and content.
        next->SaveAsInputFormat(fp);
        next = next->GetNextElement();
    }
    fclose(fp);
    return true;
}

ModuleProperty* FlowChartModel::GetEnvSetting(void) const {
        return envModule;
}

void FlowChartModel::SetEnvSetting(ModuleProperty* envSetting) {
        if(envModule) {
        delete envModule;
    }
        envModule = envSetting;
}

void FlowChartModel::MoveElementUp(int internalId) {
    ElementProperty* ancester = NULL;
    ElementProperty* parent = NULL;
    ElementProperty* curr = elemPropHead;
    ElementProperty* child = NULL;

    while(curr != NULL) {
        if(curr->GetInternalId() != internalId) {
            ancester = parent;
            parent = curr;
            curr = curr->GetNextElement();
        }else {
            child = curr->GetNextElement();
            if(NULL != parent) {
                if(NULL != ancester) {
                    ancester->SetNextElement(curr);
                }
                curr->SetNextElement(parent);
                parent->SetNextElement(child);
                if(elemPropHead == parent) {
                    elemPropHead = curr;
                }
            }
            break;
        }
    }
}

void FlowChartModel::MoveElementDown(int internalId) {
    ElementProperty* parent = NULL;
    ElementProperty* curr = elemPropHead;
    ElementProperty* child = NULL;
    ElementProperty* grandchild = NULL;

    while(curr != NULL) {
        if(curr->GetInternalId() != internalId) {
            parent = curr;
            curr = curr->GetNextElement();
        }else {
            child = curr->GetNextElement();
            if(NULL != child) {
                grandchild = child->GetNextElement();
                if(NULL != parent) {
                    parent->SetNextElement(child);
                }
                child->SetNextElement(curr);
                curr->SetNextElement(grandchild);
                if(elemPropHead == curr) {
                    elemPropHead = child;
                }
            }
            //
            break;
        }
    }
}
