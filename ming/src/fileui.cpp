/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "fileui.h"
#include "stringutil.h"
#include "tools.h"
#include "fileutil.h"
#include "mingutil.h"
#include "fileeditdlg.h"
#include "mingpnl.h"
#include "cstringutil.h"
#include "ctrlinst.h"
#include <wx/dir.h>
#include "manager.h"
#include "projectmanager.h"
#include "simuprjdef.h"
#include "simuproject.h"
#include "envutil.h"


#define CTRL_FILE_BTN_ID CTRL_WIN_CTRL_CUSTOM_START + 1
#define CTRL_INPUT_TC_ID CTRL_WIN_CTRL_CUSTOM_START + 2
#define CTRL_EDIT_BTN_ID CTRL_WIN_CTRL_CUSTOM_START + 3
#define CTRL_VIEW_BTN_ID CTRL_WIN_CTRL_CUSTOM_START + 4

BEGIN_EVENT_TABLE(FileUI, KeywordUI)
        EVT_BUTTON(CTRL_FILE_BTN_ID, FileUI::OnFileButton)
        EVT_BUTTON(CTRL_VIEW_BTN_ID, FileUI::OnViewButton)
        EVT_BUTTON(CTRL_EDIT_BTN_ID, FileUI::OnEditButton)
        EVT_TEXT(CTRL_WIN_CTRL_ID, FileUI::OnText)
END_EVENT_TABLE()

FileUI::FileUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel) {
        origValue = NULL;
        CreateUIControls();
}

FileUI::~FileUI() {
        if (origValue) {
                free(origValue);
        }
}

wxTextCtrl* FileUI::GetFileInputCtrl(void) const {
        return (wxTextCtrl*)windowCtrl;
}

FileModel* FileUI::GetFileModel(void) const {
        return (FileModel*)keywordModel;
}

void FileUI::CreateUIControls(void) {
        CreateToftSizer(wxHORIZONTAL, false);

        wxStaticText* label = new wxStaticText(this, wxID_ANY, GetAppearString());
        //this will dynamically adjust the label size
        label->SetLabel(GetAppearString());
        label->SetToolTip(StringUtil::CharPointer2String(keywordModel->GetHelp()));
//#ifdef __MING__
    windowCtrl = new wxTextCtrl(this, CTRL_WIN_CTRL_ID, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
        windowCtrl->SetMinSize(wxSize(250, 30));
//        SimuProject* pProject = Manager::Get()->GetProjectManager()->GetSelectedProject();
//    wxString molFile = ProjectTreeItemData::GetFileName(pProject, wxEmptyString, ITEM_MOLECULE);
//    wxString xyzfilename =FileUtil::ChangeFileExt(molFile, wxT(".xyz"));
//    if (strcmp(GetFileModel()->GetName(), ATTR_VALUE_COORD) == 0) //modify by chaochao
//       ((wxTextCtrl*)windowCtrl)->SetValue(xyzfilename);
        wxButton* fileBtn = new wxButton(this, CTRL_FILE_BTN_ID, wxT("..."), wxDefaultPosition, wxSize(30, 30));
        GetSizer()->Add(label, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
        GetSizer()->Add(windowCtrl, 1, wxALL, 2);
        GetSizer()->Add(fileBtn, 0, wxALL, 2);
        // std::cout << "FileUI::CreateUIControls 0" << GetFileModel()->GetName() << std::endl;
        if (strcmp(GetFileModel()->GetName(), ATTR_VALUE_COORD) == 0) {
                // std::cout << "FileUI::CreateUIControls coord" << ATTR_VALUE_COORD << std::endl;
                wxButton* editBtn = new wxButton(this, CTRL_EDIT_BTN_ID, wxT("Edit"), wxDefaultPosition, wxSize(45, 30));
                wxButton* viewBtn = new wxButton(this, CTRL_VIEW_BTN_ID, wxT("View"), wxDefaultPosition, wxSize(45, 30));
                GetSizer()->Add(editBtn, 0, wxALL, 2);
                GetSizer()->Add(viewBtn, 0, wxALL, 2);
//#if !(defined(__LINUX) || defined(__MAC__))
//        viewBtn->Enable(false);
        }
//#endif
/*#else
    windowCtrl = new wxTextCtrl(this, CTRL_WIN_CTRL_ID, wxEmptyString, wxDefaultPosition, wxSize(200,30), wxTE_READONLY  );
    windowCtrl->SetMinSize(wxSize(200, 30));
    SimuProject* pProject = Manager::Get()->GetProjectManager()->GetSelectedProject();
    wxString molFile = ProjectTreeItemData::GetFileName(pProject, wxEmptyString, ITEM_MOLECULE);
    wxString xyzfilename =FileUtil::ChangeFileExt(molFile, wxT(".xyz"));
    if (strcmp(GetFileModel()->GetName(), ATTR_VALUE_COORD) == 0) //modify by chaochao
       ((wxTextCtrl*)windowCtrl)->SetValue(xyzfilename);
        GetSizer()->Add(label, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
        GetSizer()->Add(windowCtrl, 1, wxALL, 2);
    if (strcmp(GetFileModel()->GetName(), ATTR_VALUE_COORD) == 0) {
                wxButton* viewBtn = new wxButton(this, CTRL_VIEW_BTN_ID, wxT("View"), wxPoint(220,-1), wxSize(45, 30));
                GetSizer()->Add(viewBtn, 0, wxALL|wxEXPAND, 2);
//#if !(defined(__LINUX) || defined(__MAC__))
  //      viewBtn->Enable(false);
//#endif
        }
#endif*/
        GetSizer()->SetSizeHints(this);
        UpdateFromModel();

}

void FileUI::OnFileButton(wxCommandEvent& event) {
        wxString defaultDir = wxEmptyString;
        if (!GetFileInputCtrl()->GetValue().IsEmpty()) {
                defaultDir = wxFileName(GetFileInputCtrl()->GetValue()).GetPath();
        }
        if(GetFileModel()->GetKind() == KEYWORD_KIND_FILE || strcmp(GetFileModel()->GetName(), ATTR_VALUE_COORD) == 0) {
                wxFileDialog fileDialog(Tools::GetAppFrame(), wxT("Open File"), defaultDir, wxEmptyString, wxT("XYZ files (*.xyz)|*.xyz|All files (*)|*"), wxFD_OPEN);
                // std::cout << "FileUI::OnFileButton " << GetFileModel()->GetKind() << std::endl;
                if (fileDialog.ShowModal() == wxID_OK) {
                        wxString fileName = fileDialog.GetPath();
                        if(MingPnl::Get()->IsUseAbsolutePath()) {
                                GetFileInputCtrl()->ChangeValue(fileName);
                                UpdateToModel(false);
                        }else {
                                wxString prefix = MingPnl::Get()->GetFilePath();
                                if(StringUtil::StartsWith(fileName, prefix, false)) {
                                        fileName = fileName.Mid(prefix.Len());
                                        GetFileInputCtrl()->ChangeValue(fileName);
                                        UpdateToModel(false);
                                }else {
                                        Tools::ShowInfo(wxT("Please select an XYZ file in the same directory as the input file:\n") + MingPnl::Get()->GetFilePath());
                                }
                        }
                }
        }else {
                wxDirDialog dirDialog(Tools::GetAppFrame(), wxEmptyString, defaultDir);
                if (dirDialog.ShowModal() == wxID_OK) {
                        GetFileInputCtrl()->ChangeValue(dirDialog.GetPath());
                UpdateToModel(false);
                }
        }
}

void FileUI::OnEditButton(wxCommandEvent& event) {
        char* charValue = GetFileModel()->GetValue();
        if (charValue == NULL) {
                Tools::ShowInfo(wxT("Please select an XYZ file first."));
                return;
        }
        wxString filePath = StringUtil::CharPointer2String(charValue);
        if(FileUtil::IsRelativePath(filePath)) {
                filePath = MingPnl::Get()->GetFilePath() + filePath;
        }
        FileEditDlg fileEditDlg(Tools::GetAppFrame(), wxID_ANY);
        fileEditDlg.SetNeedSubmitBtn(false);
        fileEditDlg.CreateControls();
        fileEditDlg.ReadFile(filePath);
        fileEditDlg.ShowModal();
}

void FileUI::OnViewButton(wxCommandEvent& event) {
    if(EnvUtil::GetMolcasGVDir().IsEmpty()){
        wxMessageBox(wxT("Can't find molcas-gv, please install it at first."));
        return;
    }
//wxMessageBox(wxT("B1"));
         char* charValue = GetFileModel()->GetValue();
        if (charValue == NULL) {
                Tools::ShowInfo(wxT("Please select an XYZ file first."));
                return;
        }
        wxString fileFullName = StringUtil::CharPointer2String(charValue);
        if(FileUtil::IsRelativePath(fileFullName)) {
                fileFullName = MingPnl::Get()->GetFilePath() + fileFullName;
        }
        wxFileName fileName(fileFullName);
        if (!fileName.FileExists()) {
                Tools::ShowError(wxT("No such file exists: ") + fileFullName);
        }
        wxDateTime beforeDateTime = GetLastModificationTime(fileFullName);
//wxMessageBox(wxT("B2"));
#if !(defined(__LINUX__) || defined(__MAC__))
        MingUtil::ExecGvWindows(fileFullName);//wxMessageBox(wxT("B2"));
#else
        //ProcessOutputDlg gvProcessDlg(this, wxID_ANY);
        //gvProcessDlg.ExecGvMolcas(fileFullName);
        Manager::Get()->GetProjectManager()->GetActiveProject()->CloseView(true,false);
        wxString       gvFile=EnvUtil::GetMoldenPath();// gvFile=EnvUtil::GetMolcasGVDir();
//wxString msg=wxT("\"")+gvFile+wxT(" ")+fileFullName+wxT("\"");
//wxMessageBox(msg);
        if(!gvFile.IsEmpty()) {
                 wxExecute(gvFile+wxT(" ")+fileFullName, wxEXEC_ASYNC);
                //  wxExecute(wxT("\"")+gvFile+wxT(" ")+fileFullName+wxT("\""), wxEXEC_ASYNC);
                //  std::cout << "OnViewButton: " << fileFullName << ";" << std::endl;
        } else {
                wxMessageBox(wxT("Please install gv and modify the settings at first."));
        }
#endif
//wxMessageBox(wxT("B3"));

        wxDateTime afterDateTime = GetLastModificationTime(fileFullName);
        if (afterDateTime.IsLaterThan(beforeDateTime)) {
                wxMessageDialog dialog(this, wxT("XYZ files have been created or modified. Do you want to reselect an XYZ file?"), wxT("MING"), wxYES_DEFAULT | wxYES_NO | wxICON_QUESTION);
                if (dialog.ShowModal() == wxID_YES) {
                        OnFileButton(event);
                }
        }
}

wxDateTime FileUI::GetLastModificationTime(wxFileName fileName) {
        wxDir dir(fileName.GetPath());
        wxString otherFile;
        wxDateTime lastDateTime = fileName.GetModificationTime();
        wxDateTime tempDateTime;
        if (dir.GetFirst(&otherFile, fileName.GetName() + wxT("*") + fileName.GetExt())) {
                tempDateTime = wxFileName(fileName.GetPath() + wxT("/") + otherFile).GetModificationTime();
                if (tempDateTime.IsLaterThan(lastDateTime))
                        lastDateTime = tempDateTime;
                while (dir.GetNext(&otherFile)) {
                        tempDateTime = wxFileName(fileName.GetPath() + wxT("/") + otherFile).GetModificationTime();
                        if (tempDateTime.IsLaterThan(lastDateTime))
                                lastDateTime = tempDateTime;
                }
        }
        return lastDateTime;
}

void FileUI::UpdateFromModel(void) {
        KeywordUI::UpdateFromModel();
        bool hasReplace = false;
        char* charValue = GetFileModel()->GetValue();
        wxString strValue = wxEmptyString;
        //#if defined (__LINUX__) //added by chaochao molcasrc
        // strValue= Getdefaultvalue_file();
        //#endif
        if (charValue != NULL) {
                StringCopy(&origValue, charValue, strlen(charValue));
                strValue = StringUtil::CharPointer2String(charValue);
                wxString molcas = wxT("$MOLCAS");
                if (strValue.StartsWith(molcas)) {
                        strValue = MingUtil::GetMolcasDir() + strValue.Mid(molcas.Len());
                        hasReplace = true;
                } else {
                        /*if (strValue.Find(wxChar(':')) == wxNOT_FOUND &&
                                        !(StringUtil::StartsWith(strValue, wxT("/"), true) || StringUtil::StartsWith(strValue, wxT("."), true))) {
                                //not started with a path
                                //strValue = wxT("./") + strValue;
                                strValue = GetMingMainDialog()->GetCurrInputFilePath() + strValue;
                                hasReplace = true;
                        }*/
                }
        }
        GetFileInputCtrl()->ChangeValue(strValue);
        if (hasReplace) UpdateToModel(false);
}

void FileUI::UpdateToModel(bool isRestore) {
        wxString inputValue = wxEmptyString;
        if (isRestore) {
                GetFileModel()->SetValue(origValue);
        } else {
                char* value = NULL;
                if (HasValue()) {
                        inputValue = GetFileInputCtrl()->GetValue();
                        StringUtil::String2CharPointer(&value, inputValue);
                }
                GetFileModel()->SetValue(value);
                if (value != NULL)
                        free(value);
        }
}

void FileUI::OnText(wxCommandEvent& event) {
        wxString inputValue = wxEmptyString;
        char* charValue = NULL;

        event.Skip();
        inputValue = GetFileInputCtrl()->GetValue();
        StringUtil::String2CharPointer(&charValue, inputValue);
        GetFileModel()->SetValue(charValue);
        if (charValue != NULL)
                free(charValue);
}

bool FileUI::HasValue(void) const {
        wxString value = GetFileInputCtrl()->GetValue();
        if (GetFileModel()->IsEnabled() && !StringUtil::IsEmptyString(value)) {
                return true;
        }
        return false;
}


#if defined (__LINUX__)    //added by chaochao molcasrc
wxString FileUI::Getdefaultvalue_file(void)
{

     wxArrayString env_mol;
     env_mol=molcasrc_var();
     if(strcmp(GetFileModel()->GetName(), ATTR_VALUE_ENV_LICENSE)==0)
     return env_mol[15];
     return wxEmptyString;

 }

#endif
