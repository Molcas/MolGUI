/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "selectionmodel.h"
#include "inputparser.h"
#include "keywordconst.h"
#include "xmlparser.h"
#include "cstringutil.h"
#include "fileutil.h"

SelectionModel::SelectionModel(ModuleProperty* parentModule, XmlElement* elem) : KeywordModel(parentModule, elem){
    int i = 0;
    seletionValues = NULL;
    appearValues = NULL;
    selectedIndex = -1;
    selectionCount = 0;
    isColonValue = false;
    omittedSelectionValueCount = 0;

    KeywordModel* subKeyModel = NULL;
    if(GetKeywordKind(elem) == KEYWORD_KIND_SELECT) {
        // create a container for its subelements.
        // do not use contiains attribute, use the subkeywords directly
        // seletionValues = GetAttributeStringArray(xmlElement, ATTR_CONTAINS, &selectionCount);

                SetChildModule(new ModuleProperty(elem, this, ELEM_PROP_TYPE_SELECT));
                selectionCount = GetChildModule()->GetSubElementCount();
                if(selectionCount > 0) {
                        seletionValues = (char**)RequestMemory(seletionValues, 0, selectionCount, sizeof(char*));
                        appearValues = (char**)RequestMemory(appearValues, 0, selectionCount, sizeof(char*));
                        for(i = 0; i < selectionCount; i++) {
                                subKeyModel = GetChildModule()->GetKeywordModel(i);
                                StringCopy(&seletionValues[i], subKeyModel->GetName(), strlen(subKeyModel->GetName()));
                                StringCopy(&appearValues[i], subKeyModel->GetAppear(), strlen(subKeyModel->GetAppear()));
                                /*if(subKeyModel == NULL) {
                                        if(strcmp(ATTR_VALUE_DEFAULT, seletionValues[i]) != 0) {
                                                FileUtil::ShowErrorMessage(wxT("SELECT ") + FileUtil::CharPointer2String(GetName())
                                                        + wxT(" in MODULE ") + FileUtil::CharPointer2String(parentModule->GetName())
                                                        + wxT(" does not contain a keyword with name ")
                                                        + FileUtil::CharPointer2String(seletionValues[i]));
                                        }
                                        StringCopy(&appearValues[i], seletionValues[i], strlen(seletionValues[i]));
                                }else {
                                        StringCopy(&appearValues[i], subKeyModel->GetAppear(), strlen(subKeyModel->GetAppear()));
                                }*/
                        }
                }
    }else {
        seletionValues = GetAttributeStringArray(xmlElement, ATTR_LIST, &selectionCount);
        for(i = 0; i < selectionCount; i++) {
            if(SubStringIndex(seletionValues[i], ":", 0) > 0) {
                                isColonValue = true;
                        }else if (strcmp(ATTR_VALUE_USERINPUT, seletionValues[i]) == 0 || strcmp(ATTR_VALUE_USERDIR, seletionValues[i]) == 0) {
                                omittedSelectionValueCount++;
                        }
        }
    }
}

SelectionModel::~SelectionModel() {
    if(seletionValues != NULL)
        FreeStringArray(seletionValues, selectionCount);
    if(appearValues != NULL)
        FreeStringArray(appearValues, selectionCount);
}

char* SelectionModel::GetDefaultValue(void) {
    return GetAttributeValue(ATTR_DEFAULTVALUE);
}

int SelectionModel::GetSelectionCount(void) const{
    return selectionCount;
}

int SelectionModel::GetOmittedSelectionValueCount(void) const {
        return omittedSelectionValueCount;
}

char** SelectionModel::GetSelectionValues(bool isAppear) const {
        if(isAppear && appearValues != NULL)
                return appearValues;
    return seletionValues;
}

char** SelectionModel::GetAppearValues(void) const {
    return appearValues;
}

KeywordModel* SelectionModel::GetSelectedKeywordModel(void) const {
    return GetChildModule()->GetKeywordModel(GetSelectedValue());
}

KeywordModel* SelectionModel::GetSelectionKeywordModel(int index) const {
        return GetChildModule()->GetKeywordModel(seletionValues[index]);
}

char* SelectionModel::GetSelectedValue(void) const{
    if(selectedIndex < 0 || selectedIndex >= GetSelectionCount())
        return NULL;
    return seletionValues[selectedIndex];
}

int SelectionModel::GetSelectedIndex(void) const {
    return selectedIndex;
}

void SelectionModel::SetSelectedIndex(int index) {
    if(index >= -1 && index < GetSelectionCount()) {
        selectedIndex = index;
    }
}

void SelectionModel::SetSelectedModel(KeywordModel* keywordModel) {
        int i = 0;
        for(i = 0; i < GetSelectionCount(); i++) {
                /*if(GetChildModule()->GetKeywordModel(i) != NULL) {
                        wxLogError(wxString::Format("===%s = %d", GetChildModule()->GetKeywordModel(i)->GetName(), i));
                }*/
                if(GetChildModule()->GetKeywordModel(i) == keywordModel) {
                        //wxLogError(wxString::Format("%s = %d, count= %d", keywordModel->GetName(), i, GetSelectionCount()));
                        SetSelectedIndex(i);
                        break;
                }
        }
}

bool SelectionModel::IsNeedShowKeywordModel(KeywordModel* keywordModel) const {
    if(keywordModel == NULL) return false;
    if(keywordModel->GetKind() == KEYWORD_KIND_SINGLE) return false;
    return true;
}

bool SelectionModel::SaveAsInputFormat(FILE* fp, bool isEnv) {
    unsigned i = 0;

    if(GetKind() == KEYWORD_KIND_SELECT) {
                if(GetChildModule() != NULL && !GetChildModule()->IsNeedShowSubelements()) {
                        for(i = 0; i < (unsigned)GetSelectionCount(); i++) {
                                GetSelectionKeywordModel(i)->SaveAsInputFormat(fp, isEnv);
                        }
                }else{
                KeywordModel* selectedKeyModel = GetSelectedKeywordModel();
                if(selectedKeyModel) {
                                if(selectedKeyModel->GetKind() == KEYWORD_KIND_SINGLE) {
                                        selectedKeyModel->SetSelected(true, false);
                                }
                    selectedKeyModel->SaveAsInputFormat(fp, isEnv);
                }
                }
    }else {
                char* outValue = NULL;
                if(GetOmittedSelectionValueCount() > 0) {
                        outValue = GetValue();
                }else {
                        outValue = GetSelectedValue();
                }
                if(outValue) {
            if(isEnv) {
                                fprintf(fp, "%s %s=", CONST_EXPORT_OUTPUT, GetName());
                        }else {
                                fprintf(fp, "%s\n", GetName());
                        }
            if(isColonValue) {
                                if(!isEnv) {
                        fprintf(fp, " ");
                                }
                for(i = 0; i < strlen(outValue); i++) {
                    if(outValue[i] == ':') {
                                                break;
                                        }
                    fprintf(fp, "%c", outValue[i]);
                }
                fprintf(fp, "\n");
            }else {
                                if(!isEnv) {
                        fprintf(fp, " %s\n", outValue);
                                }else {
                                        fprintf(fp, "%s\n", outValue);
                                }
            }
                }
    }
    return true;
}

void SelectionModel::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
    int i = 0;
    char* nonBlankBuf = NULL;
    while(!feof(fp)) {
                if(strlen(buf) <= 0) {
                if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                }
        if(InputParser::IsEndOfInput(buf)) break;
        nonBlankBuf = StringTrim(buf, NULL);
        if(GetKind() == KEYWORD_KIND_SELECT) {
            //shoul not go here, directly processed in moduleproperty in finding a keyword
        }else {
            //FileUtil::ShowInfoMessage(wxString::Format("selection %s", nonBlankBuf));
            if(GetOmittedSelectionValueCount() <= 0) {
                for(i = 0; i < selectionCount; i++) {
                    if(SubStringIndex(seletionValues[i], nonBlankBuf, 0) == 0) {
                        //SetValue(seletionValues[i]);
                        SetSelectedIndex(i);
                        break;
                    }
                }
            }else{
                SetValue(nonBlankBuf);
            }
            buf[0] = 0;
            break;
        }
    }
}

void SelectionModel::DuplicateValue(KeywordModel* const oldKeywordModel) {
    int oldSelectedIndex = ((SelectionModel*)oldKeywordModel)->GetSelectedIndex();
    KeywordModel::DuplicateValue(oldKeywordModel);
    //FileUtil::ShowErrorMessage(wxString::Format("%d", oldSelectedIndex));
    SetSelectedIndex(oldSelectedIndex);
}
