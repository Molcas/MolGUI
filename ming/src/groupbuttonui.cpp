/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "groupbuttonui.h"
#include "stringutil.h"
#include "groupviewerdlg.h"

#include "cstringutil.h"

BEGIN_EVENT_TABLE(GroupButtonUI, KeywordUI)
        EVT_BUTTON(CTRL_WIN_CTRL_ID, GroupButtonUI::OnButton)
END_EVENT_TABLE()

GroupButtonUI::GroupButtonUI(wxWindow *parent, KeywordModel* keywordModel) : KeywordUI(parent, keywordModel){
#ifdef __MAC__
        m_labelHidden = NULL;
#endif
        CreateUIControls();
}

GroupButtonUI::~GroupButtonUI() {
}

wxButton* GroupButtonUI::GetDetailButton(void) {
    return (wxButton*)windowCtrl;
}

GroupModel* GroupButtonUI::GetGroupModel(void){
    return (GroupModel*)keywordModel;
}

void GroupButtonUI::CreateUIControls(void) {
    CreateToftSizer(wxHORIZONTAL, false);

    wxStaticText* label = new wxStaticText(this, wxID_ANY, GetAppearString());
    //this will dynamically adjust the label size
    label->SetLabel(GetAppearString());
    if(label->GetSize().GetWidth() < UI_LABEL_WIDTH) {
        label->SetMinSize(wxSize(UI_LABEL_WIDTH, wxDefaultSize.GetHeight()));
    }
    label->SetToolTip(StringUtil::CharPointer2String(keywordModel->GetHelp()));

    windowCtrl = new wxButton(this, CTRL_WIN_CTRL_ID, wxT("Details..."), wxDefaultPosition, wxSize(80, 30));

    GetSizer()->Add(label, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
    GetSizer()->Add(windowCtrl, 0, wxALL, 2);
#ifdef __MAC__
        m_labelHidden = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(100, wxDefaultSize.GetHeight()));
        m_labelHidden->SetForegroundColour(*wxRED);
        GetSizer()->Add(m_labelHidden, 0, wxALIGN_CENTER_VERTICAL, 4);
#endif
    GetSizer()->SetSizeHints(this);

        SetHiddenColor();
}

void GroupButtonUI::OnButton(wxCommandEvent& event) {
    //GroupViewerDlg viewDlg(GetMingMainDialog(), wxID_ANY);
        GroupViewerDlg viewDlg(GetParent(), wxID_ANY);
        viewDlg.SetGroupModel(GetGroupModel());
    viewDlg.ShowModal();
    SetHiddenColor();
}

void GroupButtonUI::UpdateFromModel(void) {
}

void GroupButtonUI::UpdateToModel(bool isRestore) {
}

bool GroupButtonUI::HasValue(void) const{
    GroupViewerDlg viewDlg(GetParent(), wxID_ANY);
        viewDlg.SetGroupModel(((GroupButtonUI*)this)->GetGroupModel());
    return viewDlg.HasValue();
}

void GroupButtonUI::SetHiddenColor(void) {
        if(HasValue()) {
#ifdef __MAC__
                m_labelHidden->SetLabel(wxT("Hidden Input!"));
#else
                windowCtrl->SetForegroundColour(*wxRED);
                windowCtrl->SetToolTip(wxT("Hidden Input!"));
#endif
        }else {
#ifdef __MAC__
                m_labelHidden->SetLabel(wxEmptyString);
#else
                windowCtrl->SetForegroundColour(*wxBLACK);
                windowCtrl->SetToolTip(wxEmptyString);
#endif
        }
}
