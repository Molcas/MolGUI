/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/textfile.h>
#include <wx/filename.h>

#include "fileeditdlg.h"
#include "tools.h"
#include "mingpnl.h"

BEGIN_EVENT_TABLE(FileEditDlg, wxDialog)
    EVT_BUTTON(wxID_SAVE, FileEditDlg::OnDialogSave)
    EVT_BUTTON(CTRL_ID_BTN_SUBMIT, FileEditDlg::OnDialogSubmit)
    EVT_BUTTON(wxID_CLOSE, FileEditDlg::OnDialogOk)
    EVT_BUTTON(wxID_CANCEL, FileEditDlg::OnDialogCancel)
    EVT_CLOSE(FileEditDlg::OnClose)
END_EVENT_TABLE()

FileEditDlg::FileEditDlg(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog(parent, id, title, pos, size, style)
{
     needSubmitBtn = false;
     needSaveBtn = false;
     textCtrl = NULL;
     fileName = wxEmptyString;
}

FileEditDlg::~FileEditDlg() {
}

void FileEditDlg::SetNeedSubmitBtn(bool isNeed) {
    needSubmitBtn = isNeed;
}

void FileEditDlg::SetNeedSaveBtn(bool isNeed) {
    needSaveBtn = isNeed;
}

void FileEditDlg::UpdateDialogTitle(void) {
        wxString title = fileName;
        if(needSaveBtn) {
            if(title == wxEmptyString) {
                    title = wxT("File Editor");
            }else{
                    title += wxT(" - File Editor");
            }
    }else {
        title = wxT("File Viewer");
    }
        SetTitle(title);
}

void FileEditDlg::ReadFile(wxString file) {
    if(!wxFileName::FileExists(file)){
        Tools::ShowError(wxT("No such file exists: ") + file);
    }else {
        fileName = file;
        UpdateDialogTitle();
        textCtrl->LoadFile(fileName);
    }
}

void FileEditDlg::CreateControls(void) {
    wxBoxSizer* bsTop = new wxBoxSizer(wxVERTICAL);

    textCtrl = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(600, 400), wxTE_MULTILINE);

        wxFont font;
        font.SetNativeFontInfoUserDesc(wxT("Monospace"));
        font.SetPointSize(10);
        if(font.Ok()) {
                textCtrl->SetFont(font);
        }

    wxBoxSizer* bsBottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton * btnSave = NULL;
    if(needSaveBtn) {
        btnSave = new wxButton(this, wxID_SAVE, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    }
    wxButton * btnSubmit = NULL;
    if(needSubmitBtn) {
        btnSubmit =  new wxButton(this, CTRL_ID_BTN_SUBMIT, wxT("S&ubmit"), wxDefaultPosition, wxSize(100,30));
    }
    wxButton * btnOkCancel = NULL;
    if(needSubmitBtn) {
        btnOkCancel = new wxButton(this, wxID_CANCEL, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    }else {
        btnOkCancel = new wxButton(this, wxID_CLOSE, wxEmptyString, wxDefaultPosition, wxSize(100,30));
    }

    if(needSaveBtn&&btnSave) {
        bsBottom->Add(btnSave, 0, wxALL, 2);
    }else {
        textCtrl->SetEditable(false);
    }
    if(btnSubmit) {
        bsBottom->Add(btnSubmit, 0, wxALL, 2);
    }
    bsBottom->Add(btnOkCancel, 0, wxALL, 2);

    bsTop->Add(textCtrl, 1, wxEXPAND |wxALL, 2);
    bsTop->AddSpacer(3);
    bsTop->Add(bsBottom, 0, wxALIGN_RIGHT|wxALL, 2);

    this->SetSizer(bsTop);

    Center();
}

void FileEditDlg::OnDialogSave(wxCommandEvent& event) {
        if(SaveFile()) {
                HideDialog(wxID_SAVE);
        }

}

void FileEditDlg::OnDialogSubmit(wxCommandEvent& event) {
        if(SaveFile()) {
                HideDialog(wxID_APPLY);
        }
}

void FileEditDlg::OnDialogOk(wxCommandEvent& event) {
    HideDialog(wxID_OK);
}

void FileEditDlg::OnDialogCancel(wxCommandEvent& event) {
    if(SaveFile()) {
        HideDialog(wxID_CANCEL);
    }
}

bool FileEditDlg::SaveFile(void) {
        bool isContinue = true;
    if(textCtrl->IsModified()) {
        if(fileName.IsEmpty()) {
        }else {
            wxMessageDialog dialog(this, wxT("Do you want to save the changes?"), wxT("MING"),
                        wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION);
            switch(dialog.ShowModal()) {
                        case wxID_YES:
                                textCtrl->SaveFile(fileName);
                                        MingPnl::Get()->DoFileOpen(fileName,true);
                                break;
                        case wxID_NO:
                                break;
                        case wxID_CANCEL:
                                isContinue = false;
                                break;
                        default:
                                break;
            }
        }
    }
        return isContinue;
}

void FileEditDlg::OnClose(wxCloseEvent& event){
    HideDialog(wxID_CANCEL);
}

void FileEditDlg::HideDialog(int returnCode) {
    if(IsModal()) {
        EndModal(returnCode);
    }else{
        SetReturnCode(returnCode);
        this->Show(false);
    }
        //Destroy();
}
