/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "slidingwindow.h"

#define CTRL_BTN_START 1023
#define LABEL_HEIGHT 35

BEGIN_EVENT_TABLE(SlidingWindow, wxPanel)
        EVT_BUTTON(wxID_ANY, SlidingWindow::OnButton)
END_EVENT_TABLE()

SlidingWindow::SlidingWindow(wxWindow* parent, wxWindowID id, const wxPoint& pos,
                                                         const wxSize& size, long style, const wxString& name) : wxPanel(parent, id, pos, size, style | wxSUNKEN_BORDER, name) {
        winCount = -1;
}

SlidingWindow::~SlidingWindow() {
}

void SlidingWindow::Init(void) {
}

void SlidingWindow::OnButton(wxCommandEvent& event) {
        int id = event.GetId() - CTRL_BTN_START;
        //int pos = 0;
        //int i = 0;
        int j = 0;
        int currHeight = 0;
        int finalPos = 0;
        wxSplitterWindow* sw = NULL;
        currHeight = this->GetClientSize().GetHeight();

        //wxLogError(wxString::Format(wxT("height %d"), currHeight));
        if (id >= 0 && id < winCount) {
                for (j = 0; j < winCount - 1; j++) {
                        sw = (wxSplitterWindow*)(splitWinArray[j]);
                        //pos = sw->GetSashPosition();
                        //i = pos;
                        if (j == id) {
                                finalPos = currHeight - (winCount - 1) * (LABEL_HEIGHT + 2) - 8;
                                /*while(i < finalPos) {
                                    sw->SetSashPosition(i, true);
                                    GetPage(id)->Update();
                                    i += 5;
                                }*/
                        } else {
                                finalPos = LABEL_HEIGHT;
                                /*while(i > finalPos) {
                                    sw->SetSashPosition(i, true);
                                    GetPage(id)->Update();
                                    i -= 5;
                                }*/
                        }
                        sw->SetSashPosition(finalPos, true);
                }
        }
}

wxPanel* SlidingWindow::GetPage(int level) {
        return (wxPanel*)(pageArray[level]);
}

void SlidingWindow::AddPages(wxArrayString& labels) {
        int i = 0;
        winCount = labels.GetCount();
        SetSizer(new wxBoxSizer(wxVERTICAL));
        wxSplitterWindow* parentSW = NULL;
        wxSplitterWindow* currSW = NULL;
        wxPanel* panel = NULL;
        for (i = 0; i < winCount - 1; i++) {
                if (i == 0) {
                        currSW = new wxSplitterWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_NOBORDER);
                        GetSizer()->Add(currSW, 1, wxEXPAND | wxALL, 2);
                } else {
                        parentSW = currSW;
                        currSW = new wxSplitterWindow(parentSW, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_NOBORDER);
                        parentSW->SplitHorizontally(panel, currSW, LABEL_HEIGHT);
                        parentSW->SetMinimumPaneSize(LABEL_HEIGHT);
                }
                splitWinArray.Add(currSW);
                panel = CreatePanel(currSW, labels[i], i);
        }
        wxPanel* lastPanel = CreatePanel(currSW, labels[i], i);
        //wxLogError(wxString::Format(wxT("parent width %d"),LABEL_HEIGHT));
        currSW->SplitHorizontally(panel, lastPanel, LABEL_HEIGHT);
        currSW->SetMinimumPaneSize(LABEL_HEIGHT);
        //for the last panel, this is the same for the last second panel
        splitWinArray.Add(currSW);
}

wxPanel* SlidingWindow::CreatePanel(wxWindow* parent, wxString& label, int level) {
        wxPanel* panel = new wxPanel(parent, wxID_ANY);
        panel->SetSizer(new wxBoxSizer(wxVERTICAL));

#ifdef __MAC__
        //int width = GetSize().GetWidth();
        int width = 380;
        int height = LABEL_HEIGHT;

        wxBitmap bitmap(width, height);
        wxMemoryDC memDC;
        memDC.SelectObject(bitmap);
        memDC.SetBackground(wxColour(0x80, 0x80, 0x00));
        memDC.SetPen(*wxWHITE);
        memDC.Clear();

        wxSize labelSize = memDC.GetMultiLineTextExtent(label);
        width = (width  - labelSize.GetWidth()) / 2;
        height = (height  - labelSize.GetHeight()) / 2;
        if (width < 0) width = 0;
        if (height < 0) height = 0;
        memDC.DrawText(label, width, height);

        memDC.SelectObject(wxNullBitmap);
        wxBitmapButton* btn = new wxBitmapButton(panel, CTRL_BTN_START + level, bitmap, wxDefaultPosition, wxSize(100, LABEL_HEIGHT));
#else
        //wxLogError(wxString::Format(wxT("parent width %d"),LABEL_HEIGHT));
        wxButton* btn = new wxButton(panel, CTRL_BTN_START + level, label, wxDefaultPosition, wxSize(100, LABEL_HEIGHT));
        btn->SetBackgroundColour(wxColour(0x80, 0x80, 0x00));
        //btn->SetForegroundColour(*wxWHITE);
#endif
        wxPanel* page = new wxPanel(panel, wxID_ANY); // wxDefaultPosition, wxSize(300, 300), wxFULL_REPAINT_ON_RESIZE );
        panel->GetSizer()->Add(btn, 0, wxEXPAND | wxBOTTOM, 2);
        panel->GetSizer()->Add(page, 1, wxEXPAND | wxALL, 0);

        pageArray.Add(page);
        pnlArray.Add(panel);
        return panel;
}
