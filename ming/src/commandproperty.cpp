/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "commandproperty.h"
#include "inputparser.h"
#include "keywordparser.h"
#include "keywordconst.h"
#include "cstringutil.h"

#include "stringutil.h"
#include "tools.h"

CommandProperty::CommandProperty(XmlElement *elem) : ElementProperty(elem) {
    // Get all subelements of the given xmlelement
    // subElementCount is the number of subelements
    subXmlElements = GetSubElements(xmlElement, ELEM_SUBCOMMAND, &subElemCount);
}

CommandProperty::~CommandProperty() {
}

char *CommandProperty::GetFormat(XmlElement *elem, bool optional) const {
    if(elem) {
        if(optional)
            return GetAttributeString(elem, ATTR_OPTIONALFORMAT);
        else
            return GetAttributeString(elem, ATTR_FORMAT);
    }
    return NULL;
}

void CommandProperty::GetFormat(const char *format, int index, const char *value, char *outFormat) {
    int i = 0, len = strlen(format);
    int count = 0, k = 0;
    outFormat[0] = 0;
    for(i = 0; i < len; i++) {
        if(format[i] == '%') {
            count++;
            if(count == index) {
                strcat(outFormat, value);
                strcat(outFormat, &format[i + 2]);
                break;
            }
        }
        outFormat[k] = format[i];
        k++;
        outFormat[k] = 0;
    }

}

char *CommandProperty::GetOptionalName(void) const {
    return GetAttributeString(xmlElement, ATTR_OPTIONALNAME);
}


ElemPropType CommandProperty::GetPropType(void) const {
    return ELEM_PROP_TYPE_COMMAND;
}

void CommandProperty::SetPropValue(const char *newValue) {
    if(IsNeedUserInput()) {
        ElementProperty::SetPropValue(newValue);
    }
}

void CommandProperty::ClearData(void) {

}

bool CommandProperty::IsValid(void) {
    bool isVal = true;
    char **propValues = NULL;
    char *tempProp = NULL;
    int propCount = 0;
    int i;

    if(IsNeedUserInput() && GetPropValue()) {
        propValues =  StringTokenizer(GetPropValue(), VALUE_DELIM, &propCount, 1);
        for(i = 0; i < propCount; i++) {
            tempProp = StringTrim(propValues[i], NULL);
            if(tempProp == NULL || strlen(tempProp) == 0) {
                isVal = false;
                break;
            }
        }
        if(propValues) {
            FreeStringArray(propValues, propCount);
        }
    } else {
        isVal = !IsNeedUserInput();
    }

    return isVal;
}

bool CommandProperty::IsNeedUserInput(void) const {
    int index = -1;
    char *formatChars = NULL;
    char formatChar = 0;

    if(subElemCount > 0) {
        return true;
    }
    formatChars = GetFormat(xmlElement, false);
    if(formatChars != NULL) {
        index = SubStringIndex(formatChars, "%", 0);
        if(index >= 0 && index < (int)strlen(formatChars) - 1) {
            formatChar = formatChars[index + 1];
            if( 'f' == formatChar || 'd' == formatChar || 'l' == formatChar
                    || 's' == formatChar || 'u' == formatChar || 'c' == formatChar)
                return true;
        }
    }
    return false;
}

bool CommandProperty::IsValueLinkCommandAnchor(void) const {
    char *linkAnchor = GetAttributeString(xmlElement, ATTR_LINKANCHOR);
    char *linkValueIndex = GetAttributeString(xmlElement, ATTR_LINKVALUEINDEX);
    if(linkAnchor != NULL && linkValueIndex != NULL) {
        return true;
    }
    return false;
}

bool CommandProperty::IsValueLinkCommandTarget(void) const {
    if(GetAttributeString(xmlElement, ATTR_LINKTARGET) != NULL) {
        return true;
    }
    return false;
}

bool CommandProperty::IsNameLinkCommand(void) const {
    if(GetAttributeString(xmlElement, ATTR_LINKANCHOR) != NULL && !IsValueLinkCommandAnchor()) {
        return true;
    }
    return false;
}

bool CommandProperty::IsNameLinkCommandTarget(const char *name) const {
    if(strcmp(GetAttributeString(xmlElement, ATTR_NAME), name) == 0) {
        return true;
    }
    return false;
}

char *CommandProperty::GetIndexedPropValue(int index) {
    char *value = NULL;
    char **values = NULL;
    int propCount = -1;
    if(GetPropValue() != NULL) {
        values = StringTokenizer(GetPropValue(), VALUE_DELIM, &propCount, 1);
        if(index >= 0 && index < propCount) {
            StringCopy(&value, values[index], strlen(values[index]));
        }
        if(values) {
            FreeStringArray(values, propCount);
        }
    }
    return value;
}

bool CommandProperty::IsEqualIndexedPropValue(int index, const char *cmpValue) {
    bool isEqual = false;
    char *value = GetIndexedPropValue(index);
    if(value) {
        if(strcmp(value, cmpValue) == 0)  isEqual = true;
        free(value);
    }
    return isEqual;
}

void CommandProperty::ToLabelString(char *label, int len) {
    char buf[1024];
    int k = 0;
    int count = 0;

    label[0] = 0;
    if(GetSubElementCount() > 0) {
        for(k = 0; k < GetSubElementCount(); k++) {
            count += StringPrintf(buf, GetFormat(GetSubElement(k), false), GetPropValue(), count);
            if(strlen(label) + strlen(buf) + 2 > (unsigned)len) {
                return;
            } else {
                strcat(label, buf);
                if(k < GetSubElementCount() - 1) {
                    strcat(label, "\n");
                }
            }
        }
    } else {
        StringPrintf(buf, GetFormat(xmlElement, false), GetPropValue(), 0);
        //strcat(label, ">> ");
        if(strlen(buf) + 1 <= (unsigned)len) {
            strcat(label, buf);
        }
        if(strlen(label) <= 0) {
            strcat(label, GetFormat(xmlElement, false));
        }
    }
}

bool CommandProperty::SaveAsInputFormat(FILE *fp) {
    char buf[1024];
    int k = 0;
    int count = 0;

    if(GetSubElementCount() > 0) {
        for(k = 0; k < GetSubElementCount(); k++) {
            count += StringPrintf(buf, GetFormat(GetSubElement(k), false), GetPropValue(), count);
            if(strlen(buf) > 0) {
                fprintf(fp, "\n>>>>>>>> %s <<<<<<<<\n", buf);
            }
        }
    } else {
        StringPrintf(buf, GetFormat(xmlElement, false), GetPropValue(), 0);
        if(strlen(buf) > 0) {
            fprintf(fp, "\n>>>>>>>> %s <<<<<<<<\n", buf);
        }
    }
    return true;
}

int CommandProperty::StringPrintf(char *buf, const char *format, const char *values, int valueIndex) {
    char tempString[1024];
    char *tempChar = (char *)format;
    int index = 0;
    char forChar = 0;
    char *exactChar = NULL;
    char **propValues = NULL;
    int propCount = 0;
    int currCount = 0;

    tempString[0] = 0;
    buf[0] = 0;
    propValues =  StringTokenizer(values, VALUE_DELIM, &propCount, 1);
    currCount = valueIndex;

    index = SubStringIndex(tempChar, "%", 0);
    while(index > 0 && currCount < propCount) {
        if((index < (int)strlen(tempChar) - 1) && strlen(propValues[currCount]) > 0) {
            tempString[0] = 0;
            StringCopy(&exactChar, tempChar, index);
            strcat(buf, exactChar);
            forChar = tempChar[index + 1];
            if('s' == forChar) {
                strcat(buf, propValues[currCount]);
            } else if('d' == forChar) {
                sprintf(tempString, "%d%c", atoi(propValues[currCount]), '\0');
                strcat(buf, tempString);
            } else if('f' == forChar) {
                sprintf(tempString, "%f%c", (float)atof(propValues[currCount]), '\0');
                strcat(buf, tempString);
            }
            if(exactChar) {
                free(exactChar);
                exactChar = NULL;
            }
        }
        currCount++;
        tempChar = &tempChar[index + 2];
        index = SubStringIndex(tempChar, "%", 0);
    }
    if(strlen(tempChar) > 0) {
        StringCopy(&exactChar, tempChar, (int)strlen(tempChar));
        strcat(buf, exactChar);
        if(exactChar) {
            free(exactChar);
            exactChar = NULL;
        }
    }

    if(propValues) FreeStringArray(propValues, propCount);

    return currCount - valueIndex;;
}

bool CommandProperty::StringScanf(XmlElement *elem, char *buf, const char *format, const char *line) {
    char *attrValues = NULL;
    char **attrValueArray = NULL;
    int attrValueCount = 0;
    char **optionValues = NULL;
    int optionCount = 0;
    int i = 0, k = 0;
    char newFormat[1024];
    int stuffIndex = -1;
    char *stuffValue = NULL;
    bool isSucc = false;

    newFormat[0] = 0;
    attrValues = GetAttributeString(elem, ATTR_VALUES);
    if(attrValues) {
        attrValueArray = StringTokenizer(attrValues, LOGIC_DELIM, &attrValueCount, 0);
        if(attrValueCount > 1) {
            //only when more than two %, we will replace one of predefined words
            for(i = 0; i < attrValueCount; i++) {
                if(SubStringIndex(attrValueArray[i], ATTR_VALUE_USERINPUT, 0) < 0) {
                    //not user free input
                    optionValues =  StringTokenizer(attrValueArray[i], LOGIC_OR, &optionCount, 0);

                    for(k = 0; k < optionCount; k++) {
                        // the input line contains such predefined word
                        if(SubStringIndex((char *)line, optionValues[k], 0) >= 0) {
                            stuffIndex = i + 1;
                            StringCopy(&stuffValue, optionValues[k], strlen(optionValues[k]));
                            //replace the original format with the predefined word
                            GetFormat(format, i + 1, optionValues[k], newFormat);

                            break;
                        }
                    }
                    FreeStringArray(optionValues, optionCount);
                    break;
                }
            }
        }
        FreeStringArray(attrValueArray, attrValueCount);
        if(strlen(newFormat) <= 0) {
            //if not find matched predefined word, still use the original format
            strcat(newFormat, format);
        }
    } else {
        strcat(newFormat, format);
    }
    isSucc = StringScanf(buf, newFormat, InputParser::RemoveCommandPrefix((char *)line), stuffIndex, stuffValue);
    if(stuffValue) free(stuffValue);
    return isSucc;
}


bool CommandProperty::StringScanf(char *buf, const char *format, const char *line, int stuffIndex, const char *stuffValue) {
    int i, k, preK = 0;
    int lenFormat = (int)strlen(format);
    int lenLine = (int)strlen(line);
    i = 0;
    k = 0;
    bool hasStarted = false;
    char *tempBuf = NULL;
    bool isMatched = true;
    int count = 0;

    while(i < lenFormat && k < lenLine) {
        //FileUtil::ShowInfoMessage(wxString::Format(wxT("%d %c ** %d %c"), i, format[i], k, line[k]));
        if(hasStarted) {
            if(format[i] == line[k] || line[k] == ' ') {
                count++;
                if(count == stuffIndex) {
                    if(strlen(buf) > 0) {
                        strcat(buf, VALUE_DELIM);
                    }
                    strcat(buf, stuffValue);
                    count++;
                }
                //FileUtil::ShowInfoMessage(wxString::Format(wxT("buf value == %s "), buf));

                StringCopy(&tempBuf, &line[preK], k - preK);
                //FileUtil::ShowInfoMessage(wxString::Format(wxT("Format str value == %s "), tempBuf));
                if(strlen(buf) > 0) {
                    strcat(buf, VALUE_DELIM);
                }
                strcat(buf, tempBuf);
                if(tempBuf) {
                    free(tempBuf);
                }
                hasStarted = false;
                if(line[k] != ' ') {
                    i++;
                }
            }
            k++;
        } else if(line[k] == ' ') {
            k++;
        } else if(format[i] == ' ') {
            i++;
        } else if(format[i] == '%') {
            i += 2;
            hasStarted = true;
            while(i < lenFormat && format[i] == ' ') {
                i++;
            }
            preK = k;
        } else if(IsCharEqualNoCase(format[i], line[k])) {//if(format[i] == line[k]) {
            if(format[i] == '>' && line[k] == '>') {
                if(format[i + 1] == '>' && line[k + 1] != '>') {
                    //some old version the command may be started with '>', not ">>"
                    i++;
                } else if(format[i + 1] != '>' && line[k + 1] == '>') {
                    //some old version the command may be started with multiple '>>>>>>>>>>>>>>>'
                    while(k + 1 < lenLine && line[k + 1] == '>') {
                        k++;
                    }
                }
            }
            i++;
            k++;
        } else {
            Tools::ShowInfo(wxString::Format(wxT("Cannot match format ") + StringUtil::CharPointer2String(format) + wxT(" === ") + StringUtil::CharPointer2String(line)));
            isMatched = false;
            buf[0] = 0;
            break;
        }
    }
    // the last one
    if(isMatched && hasStarted && (int)strlen(line) > preK) {
        StringCopy(&tempBuf, &line[preK], strlen(line) - preK);
        //FileUtil::ShowInfoMessage(wxString::Format(wxT("Format str value == %s "), tempBuf));
        if(strlen(buf) > 0) {
            strcat(buf, VALUE_DELIM);
        }
        strcat(buf, tempBuf);
        if(tempBuf) {
            free(tempBuf);
        }
    }

    return isMatched;
}

void CommandProperty::ReadFromInputFormat(FILE *fp, char *buf, int bufSize, const char *availableXYZFile) {
    char propv[1024];
    char **subElemVs = NULL;
    char *name = NULL;
    char *bufName = NULL;
    int subElemCount = 0;
    int k = 0;

    propv[0] = 0;
    subElemCount = GetSubElementCount();
    if(subElemCount > 0) {
        subElemVs = (char **)malloc(subElemCount * sizeof(char *));
        memset(subElemVs, 0, subElemCount * sizeof(char *));
        do {
            // remove blank
            bufName = InputParser::RemoveCommandPrefix(buf);
            // remove blank
            bufName = StringLeftTrim(bufName, NULL);
            // remove command name
            bufName += strlen(GetName());
            bufName = StringLeftTrim(bufName, NULL);
            // start with subcomment name
            for(k = 0; k < subElemCount; k++) {
                name = GetAttributeString(GetSubElement(k), ATTR_NAME);
                if(IsStringEqualNoCase(bufName, name, strlen(name))) {
                    propv[0] = 0;
                    if(!StringScanf(GetSubElement(k), propv, GetFormat(GetSubElement(k), false), buf)) {
                        //if cannot match with normal format, try optional format
                        if(GetFormat(GetSubElement(k), true) != NULL) {
                            StringScanf(GetSubElement(k), propv, GetFormat(GetSubElement(k), true), buf);
                        }
                    }
                    StringCopy(&subElemVs[k], propv, strlen(propv));
                    propv[0] = 0;
                    break;
                }
            }
            if(k >= subElemCount) {
                Tools::ShowInfo(wxString::Format(wxT("No such command: ") + StringUtil::CharPointer2String(buf)));
            }
            buf[0] = 0;
            if(feof(fp)) {
                break;
            } else {
                if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                bufName = StringLeftTrim(buf, NULL);
                if(bufName == NULL || strlen(bufName) <= 0) {
                    if(feof(fp)) break;
                    if(fgets(buf, bufSize, fp) == NULL) buf[0]=0; //blank line
                    bufName = StringLeftTrim(buf, NULL);
                }
                if(strlen(bufName) > 2 && bufName[0] == '>' && bufName[1] == '>') {
                    bufName = StringLeftTrim(bufName + 2, NULL);
                } else {
                    break;
                }
            }
            /*if(feof(fp)) {
                break;
            }else {
                if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                bufName = StringLeftTrim(buf, NULL);
                if(strlen(bufName) > 2 && bufName[0] == '>' && bufName[1] == '>') {
                    bufName = StringLeftTrim(bufName + 2, NULL);
                }else {
                    break;
                }
                        }*/
        } while(IsStringEqualNoCase(bufName, GetName(), strlen(GetName())));
        propv[0] = 0;
        for(k = 0; k < subElemCount; k++) {
            if(subElemVs[k]) {
                strcat(propv, subElemVs[k]);
            }
            if( k < subElemCount - 1) {
                strcat(propv, VALUE_DELIM);
            }
        }
        if(subElemVs) FreeStringArray(subElemVs, subElemCount);
    } else {
        //try optional name and format first
        bufName = GetOptionalName();
        if(bufName != NULL && IsStringEqualNoCase(buf, bufName, strlen(bufName))) {
            //use optional name and format
            StringScanf(xmlElement, propv, GetFormat(xmlElement, true), buf);
        } else {
            //use normal name and format
            StringScanf(xmlElement, propv, GetFormat(xmlElement, false), buf);
        }
        buf[0] = 0;
    }
    SetPropValue(propv);
}

void CommandProperty::DuplicateValue(ElementProperty *const oldElemProp) {
    SetPropValue(oldElemProp->GetPropValue());
}
