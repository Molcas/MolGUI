/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "filemodel.h"
#include "inputparser.h"
#include "cstringutil.h"

FileModel::FileModel(ModuleProperty* parentModule, XmlElement* elem) : KeywordModel(parentModule, elem){
    inputData = NULL;
}

FileModel::~FileModel() {
    if(inputData)
        free(inputData);
}

char* FileModel::GetInputData(void) const {
    return inputData;
}

void FileModel::SetInputData(char* value) {
    if(this->inputData != NULL) {
        free(this->inputData);
    }
    if(value) {
        StringCopy(&this->inputData, value, strlen(value));
    }else {
        this->inputData = NULL;
    }
}

bool FileModel::SaveAsInputFormat(FILE* fp, bool isEnv) {
    char* value = GetValue();
    if(value != NULL) {
                if(isEnv) {
                        fprintf(fp, "%s %s=%s\n", CONST_EXPORT_OUTPUT, GetName(), value);
                }else {
                        if(strcmp(GetName(), ATTR_VALUE_COORD) == 0) {
                                fprintf(fp, "%s=%s\n", GetName(), value);
                        }else {
                        fprintf(fp, "%s\n", GetName());
                        fprintf(fp, " %s\n", value);
                        }
                }
    }
    return true;
}

void FileModel::ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) {
    char* nonBlankBuf = NULL;
        long n = 0;
    while(!feof(fp)) {
                if(strlen(buf) <= 0) {
                if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                }
        if(InputParser::IsEndOfInput(buf)) break;
        nonBlankBuf = StringTrim(buf, NULL);

                if(strcmp(GetName(), ATTR_VALUE_COORD) == 0) {
                        long value = atol(nonBlankBuf);
                        if(value) {
                                //an embeded xyz file
                                FILE* fpXYZ = fopen(availableXYZFile, "w");
                                if(fpXYZ == NULL) {
                                        printf("Cannot create file: %s\n", availableXYZFile);
                                        SetValue(nonBlankBuf);
                                }else {
                                        fprintf(fpXYZ, "%ld\n", value);
                                        for(n = 0; n <= value; n++) { // one more line
                                                if(fgets(buf, bufSize, fp) == NULL) buf[0]=0;
                                                fprintf(fpXYZ, "%s", buf);
                                        }
                                        fclose(fpXYZ);
                                        SetValue(availableXYZFile);
                                }
                        }else {
                                SetValue(nonBlankBuf);
                        }
                }
        buf[0] = 0;
        break;
    }
}
