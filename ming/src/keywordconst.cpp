/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "keywordconst.h"

const char* ELEM_COMMAND = "COMMAND";
const char* ELEM_SUBCOMMAND = "SUBCOMMAND";
const char* ELEM_EMIL = "EMIL";
const char* ELEM_GROUP = "GROUP";
const char* ELEM_HELP = "HELP";
const char* ELEM_KEYWORD = "KEYWORD";
const char* ELEM_MODULE = "MODULE";
const char* ELEM_SELECT = "SELECT";

//New group-GRP
const char* ELEM_ROOT_HELP = "root";
const char* ELEM_P_HELP = "p";
const char* ELEM_PRE_HELP = "pre";

const char* ATTR_ALSO = "ALSO";
const char* ATTR_APPEAR = "APPEAR";
const char* ATTR_CONTAINS = "CONTAINS";
const char* ATTR_COMMENT = "COMMENT";
const char* ATTR_DEFAULTVALUE = "DEFAULT_VALUE";
const char* ATTR_EXCLUSIVE = "EXCLUSIVE";
const char* ATTR_FILEINDEX = "FILE_INDEX";
const char* ATTR_FORMAT = "FORMAT";
const char* ATTR_GUI_INPUT = "INPUT";
const char* ATTR_KIND = "KIND";
const char* ATTR_LEVEL = "LEVEL";
const char* ATTR_LINKTARGET = "LINK_TARGET";
const char* ATTR_LINKANCHOR = "LINK_ANCHOR";
const char* ATTR_LINKVALUEINDEX = "LINK_VALUE_INDEX";
const char* ATTR_LIST = "LIST";
const char* ATTR_MAXVALUE = "MAX_VALUE";
const char* ATTR_MEMBER = "MEMBER";
const char* ATTR_MINVALUE = "MIN_VALUE";
const char* ATTR_MODULE = "MODULE";
const char* ATTR_NAME = "NAME";
const char* ATTR_ONSCREEN = "ONSCREEN";
const char* ATTR_OPTIONALNAME = "OPTIONAL_NAME";
const char* ATTR_OPTIONALFORMAT = "OPTIONAL_FORMAT";
const char* ATTR_REALTIMEUPDATE = "REALTIME_UPDATE";
const char* ATTR_REQUIRE = "REQUIRE";
const char* ATTR_SELECT = "SELECT";
const char* ATTR_SHOWVALUE = "SHOWVALUE";
const char* ATTR_SIZE = "SIZE";
const char* ATTR_VALUES = "VALUES";
const char* ATTR_WINDOW = "WINDOW";
const char* ATTR_WINDOW_SIZE = "WINDOW_SIZE";

const char* ATTR_VALUE_ENV = "ENVIRONMENT";
const char* ATTR_VALUE_ENV_PROJECT = "MOLCAS_PROJECT";
const char* ATTR_VALUE_ENV_OUTPUTDIR = "MOLCAS_OUTPUT";
const char* ATTR_VALUE_ENV_WORKDIR="MOLCAS_WORKDIR";
const char* ATTR_VALUE_ENV_NEW_WORKDIR="MOLCAS_NEW_WORKDIR";
const char* ATTR_VALUE_ENV_KEEP_FILES="MOLCAS_KEEP_FILES";
const char* ATTR_VALUE_ENV_SAVE="MOLCAS_SAVE";
const char* ATTR_VALUE_ENV_MOLCASMEM="MOLCASMEM";
const char* ATTR_VALUE_ENV_MOLCASDISK="MOLCASDISK";
const char* ATTR_VALUE_ENV_TRAP="MOLCAS_TRAP";
const char* ATTR_VALUE_ENV_PRINT="MOLCAS_PRINT";
const char* ATTR_VALUE_ENV_LICENSE="MOLCAS_LICENSE";
const char* ATTR_VALUE_NAME = "NAME";
const char* ATTR_VALUE_DEFAULT = "DEFAULT";
const char* ATTR_VALUE_GATEWAY = "GATEWAY";
const char* ATTR_VALUE_COORD = "COORD";
const char* ATTR_VALUE_BASIS = "BASIS";
const char* ATTR_VALUE_BASISXYZ = "BASIS (XYZ)";
const char* ATTR_VALUE_GROUP = "GROUP";
const char* ATTR_VALUE_GROUP_FULL = "FULL";
const char* ATTR_VALUE_GROUP_NOSYM = "NOSYM";
const char* ATTR_VALUE_SYMMETRY_THRESHOLD = "SYMT";
const char* ATTR_VALUE_SLAPAF = "SLAPAF";
const char* ATTR_VALUE_CONSTRAINTS = "CONSTRAINTS";
const char* ATTR_VALUE_USERINPUT = "????";                //A place holder for user input
const char* ATTR_VALUE_USERDIR = "?DIR";                // A place hodler for user to select a directory
const char* ATTR_VALUE_COMMENT = "COMMENT";
const char* ATTR_VALUE_UNDEFINED = "UNDEFINED";
const char* ATTR_VALUE_NSYM = "NSYM";
const char* ATTR_VALUE_UNIATOMS = "UNI_ATOMS";
const char* ATTR_VALUE_DEGFREEDOM = "DEG_FREEDOM";
const char* ATTR_VALUE_TRUE = "TRUE";
const char* ATTR_VALUE_FALSE = "FALSE";
const char* ATTR_VALUE_POPUP = "POPUP";
const char* ATTR_VALUE_INPLACE = "INPLACE";
const char* ATTR_VALUE_TAB = "TAB";
const char* ATTR_VALUE_REQUIRED = "REQUIRED";

const char* LOGIC_OR = ".OR.";
const char* LOGIC_AND = ",";
const char* LOGIC_DELIM = "|";

const char* KIND_CHOICE = "CHOICE";
const char* KIND_CUSTOM = "CUSTOM";
const char* KIND_DIR = "DIR";
const char* KIND_FILE = "FILE";
const char* KIND_GROUP = "GROUP";
const char* KIND_INT = "INT";
const char* KIND_INTS = "INTS";
const char* KIND_INTS_COMPUTED = "INTS_COMPUTED";
const char* KIND_INTS_LOOKUP = "INTS_LOOKUP";
const char* KIND_LIST = "LIST";
const char* KIND_REAL = "REAL";
const char* KIND_REALS = "REALS";
const char* KIND_REALS_COMPUTED = "REALS_COMPUTED";
const char* KIND_REALS_LOOKUP = "REALS_LOOKUP";
const char* KIND_SELECT = ELEM_SELECT;
const char* KIND_SINGLE = "SINGLE";
const char* KIND_STRING = "STRING";
const char* KIND_STRINGS = "STRINGS";
const char* KIND_STRINGS_COMPUTED = "STRINGS_COMPUTED";
const char* KIND_STRINGS_LOOKUP = "STRINGS_LOOKUP";
const char* KIND_UNKNOWN = "UNKNOWN";
const char* KIND_RADIO = "RADIO";
const char* KIND_BOX = "BOX";
const char* KIND_BLOCK = "BLOCK";


const char* LEVEL_BASIC = "BASIC";
const char* LEVEL_ADVANCED = "ADVANCED";
const char* LEVEL_GUI = "GUI";
const char* LEVEL_NOTIMPLEMENTED = "NOTIMPLEMENTED";
const char* LEVEL_HIDDEN = "HIDDEN";

const char* VALUE_DELIM = "@#";
const char* LINE_DELIM = "@^";

const char* CONST_END_OF = "*End of ";
const char* CONST_END_OF_INPUT = "End of input";
const char* CONST_EXPORT = "export";
const char* CONST_EXPORT_OUTPUT = ">>>>>>>>> export";
const char* CONST_ENV_START = "*@ENV";
const char* CONST_ENV_END = "*@ENDENV";
const char* CONST_TEMPLATE_START = "@";
const char* CONST_TEMPLATE_HELP = "*";
