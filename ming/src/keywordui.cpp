/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "cstringutil.h"
#include "mingutil.h"
//added by chaochao
#include "keywordui.h"
#include "stringutil.h"
#include "groupui.h"
#include "groupbuttonui.h"
#include "checkboxui.h"
#include "textctrlui.h"
#include "selectionui.h"
#include "selectionsingleui.h"
#include "fileui.h"
#include "tableui.h"
#include "listboxui.h"
#include "envutil.h"
#include "keywordconst.h"
#include "keyworddata.h"
#include <cstring>
#include "manager.h"
#include "fileutil.h"
#include "simuproject.h"

#include "keywordconst.h"

#define LINE_BUF_SIZE 128
#define LINE_FILE_SIZE 50    //the maximum lenth of the filename, also including the absolute path of the file


KeywordUI::KeywordUI(wxWindow *parent, KeywordModel* keywordModel) : wxPanel(parent, wxID_ANY) {//, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER){
    windowCtrl = NULL;
    this->keywordModel = keywordModel;
        //this->SetBackgroundColour(*wxWHITE);
        if(this->keywordModel != NULL)
           this->keywordModel->RegisterChangeListener(this);
}

KeywordUI::~KeywordUI() {
        if(this->keywordModel != NULL)
                this->keywordModel->UnRegisterChangeListener();
}

KeywordModel* KeywordUI::GetKeywordModel(void) {
    return keywordModel;
}

void KeywordUI::CreateToftSizer(int orient, bool needBox) {
    wxSizer* bsTop = NULL;
    if(keywordModel == NULL) return;
    if(needBox) {
        wxStaticBox *sbBox = new wxStaticBox(this, wxID_ANY, GetAppearString());
        bsTop = new wxStaticBoxSizer(sbBox, orient);
    }else{
        bsTop = new wxBoxSizer(orient);
    }

    this->SetSizer(bsTop);
    SetToolTip(StringUtil::CharPointer2String(keywordModel->GetHelp()));
}

wxString KeywordUI::GetAppearString(void) {
        return StringUtil::CharPointer2String(keywordModel->GetAppear());
}

KeywordUI* KeywordUI::CreateModelUI(wxWindow* parent, KeywordModel* keywordModel) {
    KeywordUI* panel = NULL;
    // const char* compTwo1 = "COORD";
    // const char* compTwo2 = "COORD";
    // const char* compTwo3 = (const char*)keywordModel->GetName();//"COORD";
    //       char* compTwo4 = (char*)ATTR_VALUE_COORD;

    // char *TransChar = (char *)compTwo1; //testing conversions
    // std::cout << keywordModel->GetName() << std::endl;
    // std::cout << "comps /" << &compTwo1 << "/" << &compTwo2 << "/" << compTwo3 << "/" << compTwo4 << "/" << &ATTR_VALUE_COORD << "/" << keywordModel->GetName() << std::endl;
    // std::cout << "type comps /" << typeid(compTwo1).name() << "/" << typeid(compTwo2).name() << "/" << typeid(compTwo3).name() << "/" << typeid(compTwo4).name() << "/" << typeid(ATTR_VALUE_COORD).name() << "/" << typeid(keywordModel->GetName()).name() << "/" << std::endl;
    //if (keywordModel->GetName() == (char *)compTwo2) {std::cout << "sucComp" << std::endl;}
    // std::cout << "---------------" << std::endl;
    // if (strcmp(compTwo1,ATTR_VALUE_COORD) == 0) {std::cout << "sucCESS Comp1" << std::endl;}
    // if (compTwo1 == compTwo3) {std::cout << "sucCESS Comp2" << std::endl;}
    // if (compTwo2 == compTwo3) {std::cout << "sucCESS Comp3" << std::endl;}
    // if (compTwo4 == keywordModel->GetName()) {std::cout << "sucCESS Comp4" << std::endl;}
    
    // std::cout << "KeywordUI::CreateModelUI: keywordModel->GetKind() " <<  keywordModel->GetKind() << std::endl;
    if(keywordModel != NULL) {
        // std::cout << "keywordui.cpp " << keywordModel->GetKindString() << "/" << keywordModel->GetKind() << "/" << keywordModel->GetName() << "/" << keywordModel->GetAppear() << std::endl;
                switch(keywordModel->GetKind()){
        case KEYWORD_KIND_NULL:
                        break;
        case KEYWORD_KIND_CHOICE:
                        panel = new SelectionUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_FILE:
                        panel = new FileUI(parent, keywordModel);
                        break;
        case KEYWORD_KIND_DIR:
                        panel = new FileUI(parent, keywordModel);
                        break;
        case KEYWORD_KIND_GROUP:
            if (((GroupModel*)keywordModel)->GetWindowKind() == WINDOW_KIND_POPUP) {
                panel = new GroupButtonUI(parent, keywordModel);
            }else {
                panel = new GroupUI(parent, keywordModel);
            }
                        break;
        case KEYWORD_KIND_INT:
                        panel = new TextCtrlUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_INTS:
                        panel = new TableUI(parent, keywordModel);
            break;
                   case KEYWORD_KIND_INTS_COMPUTED:
                        panel = new TableUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_INTS_LOOKUP:
                        panel = new TableUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_LIST:
                        panel = new ListBoxUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_REAL:
                        panel = new TextCtrlUI(parent, keywordModel);
                        break;
        case KEYWORD_KIND_REALS:
                        panel = new TableUI(parent, keywordModel);
            break;
                case KEYWORD_KIND_REALS_COMPUTED:
                        panel = new TableUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_REALS_LOOKUP:
                        panel = new TableUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_SELECT:
                        if(keywordModel->GetKind() == KEYWORD_KIND_SELECT &&
                                !keywordModel->GetChildModule()->IsNeedShowSubelements()) {
                                panel = new SelectionSingleUI(parent, keywordModel);
                        }else {
                                panel = new SelectionUI(parent, keywordModel);
                        }
            break;
        case KEYWORD_KIND_SINGLE:
            panel = new CheckboxUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_STRING:
                        // std::cout << "STRING keywordui.cpp " << keywordModel->GetKindString() << "/" << keywordModel->GetKind() << "/" << keywordModel->GetName() << "/" << keywordModel->GetAppear() << std::endl;
                        if (strcmp(ATTR_VALUE_BASISXYZ, (const char*)keywordModel->GetName()) == 0) {
                            // std::cout << "STRING keywordui.cpp if " << keywordModel->GetName() << "/" << ATTR_VALUE_BASISXYZ << std::endl;
                            panel = new ListBoxUI(parent, keywordModel);
                        } else {
                            // std::cout << "STRING keywordui.cpp else " << keywordModel->GetName() << std::endl;
                            panel = new TextCtrlUI(parent, keywordModel);
                        }
            break;
        case KEYWORD_KIND_STRINGS:
                        panel = new TextCtrlUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_STRINGS_COMPUTED:
                        panel = new TableUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_STRINGS_LOOKUP:
                        panel = new TableUI(parent, keywordModel);
            break;
        case KEYWORD_KIND_CUSTOM:
                        // std::cout << "KeywordUI::CreateModelUI: GetKind() " <<  keywordModel->GetKind() << " name " << keywordModel->GetName() << " att " << keywordModel->GetAttributeValue(ATTR_NAME) << "Value coord: " << ATTR_VALUE_COORD << std::endl;
                        //char *temp = "Coord";//keywordModel->GetAttributeValue(ATTR_NAME));
                        //char *temp = (char*)ATTR_VALUE_COORD;
                        //keywordModel->GetAttributeValue(ATTR_NAME)
                        // std::cout << t << " " << ATTR_VALUE_COORD;
                        // std::cout << "GetName type: " << typeid(keywordModel->GetName()).name() << " and " << typeid(TransChar).name() << std::endl;
                        // std::cout << "GetModel /" << keywordModel->GetName() << "/GetTempName/" << TransChar << "/" << std::endl;
                        // std::cout << "Attrib(TESTING): " << keywordModel->GetAttributeValue(ATTR_NAME)<< " Type: "<< typeid(keywordModel->GetAttributeValue(ATTR_NAME)).name() << std::endl;
                        if (strcmp(ATTR_VALUE_COORD, (const char*)keywordModel->GetName()) == 0) {
                            // keywordModel->SetValue(GetKeywordData()->GetXYZFile());
                            // if(GetKeywordData()->GetXYZFile()==NULL){
                            //             SimuProject* pProject = Manager::Get()->GetProjectManager()->GetSelectedProject();
                            //         wxString molFile = ProjectTreeItemData::GetFileName(pProject, wxEmptyString, ITEM_MOLECULE);
                            //         wxString xyzfilename =FileUtil::ChangeFileExt(molFile, wxT(".xyz"));
                            //             char        *xyzFile;
                            //             StringUtil::String2CharPointer(&xyzFile,xyzfilename);
                            //             // std::cout << "xyzFile: " << xyzFile << std::endl;
                            //             keywordModel->SetValue(xyzFile);
                            // }
                            panel = new FileUI(parent, keywordModel);
                        } else {
                            // std::cout << "CUSTOM keywordui.cpp " << keywordModel->GetName() << std::endl;
                            panel = new TextCtrlUI(parent, keywordModel);
                        }
            break;
        case KEYWORD_KIND_UNKNOWN:
                        panel = new TextCtrlUI(parent, keywordModel);
            break;
        default:
                        break;
        }
    }
    if(panel == NULL) {
        panel = new KeywordUI(parent, NULL);
    }
    return panel;
}

int KeywordUI::GetColumnSize(void) const {
        int size = 1;
        if(keywordModel != NULL)  {
                size = keywordModel->GetColumnSize();
        }
        return size;
}

int KeywordUI::GetRowSize(void) const {
        int size = 1;
        if(keywordModel != NULL)  {
                size = keywordModel->GetRowSize();
                size = (size > 4) ? 4 : size;
        }
        return size;
}

void KeywordUI::OnEnabled(void) {
    if(keywordModel == NULL) return;
    windowCtrl->Enable(keywordModel->IsEnabled());
}

void KeywordUI::OnSelected(void) {
}

void KeywordUI::OnClearData(void) {
}

void KeywordUI::UpdateFromModel(void) {
    if(keywordModel == NULL) return;
    if(windowCtrl) {
        windowCtrl->SetToolTip(StringUtil::CharPointer2String(keywordModel->GetHelp()));
        windowCtrl->Enable(keywordModel->IsEnabled());
    }
}

void KeywordUI::UpdateToModel(bool isRestore) {
}

bool KeywordUI::InputValidate(void) {
    if(GetKeywordModel()->IsInputRequired() && !HasValue()) {
        return false;
    }
    return true;
}

bool KeywordUI::HasValue(void) const {
    return false;
}
// cheng added for linux molcasrc
#if defined (__LINUX__)

char *KeywordUI::To_upper(char *x){
     int i,len;
     char *buf_upper=NULL;
     len=strlen(x);
     buf_upper=(char*)malloc(len+1);
     for(i=0;i<len;i++)
     buf_upper[i]=toupper(x[i]);
     return buf_upper;
}

wxArrayString KeywordUI::molcasrc_var(void){

    char *fileName=NULL;
    char buf[LINE_BUF_SIZE],buf_upper[LINE_BUF_SIZE],temp[LINE_BUF_SIZE];
    char env_var[envir_number][envir_len]; //env_var[i] is the  value of the ith environment variable
    char *molcasrcDir=NULL,*userhome=NULL;
    FILE *fp=NULL,*fp1=NULL,*fp2=NULL;
    int index,subindex=0,var_index=0,i=0,j=0;
    wxArrayString env_molcas;
    wxString strValue1=wxEmptyString,strValue2=wxEmptyString;
    wxString val;
    env_molcas.Clear();
    for(i=0;i<envir_number;i++){
                strcpy(env_var[i],"");
              env_molcas.Add(wxEmptyString);
    }
/*#ifdef        __LINUX__
        strValue1=wxGetUserHome();
        strValue1=strValue1+wxT("/.Molcas/molcasrc");
        if(!wxFileExists(strValue1)){
            strValue1=MingUtil::GetMolcasDir();
            strValue1=strValue1+wxT("/molcasrc");
        }
#else
    strValue1=MingUtil::GetMolcasDir();
    strValue1=strValue1+wxT("/molcasrc");
#endif*/
        strValue1=MingUtil::GetGlobalEnvFile();//EnvUtil::GetMolcasRCFile();
//wxMessageBox(strValue1);
    strValue2=wxGetUserHome();//wxGetHomeDir();
    strValue2=strValue2+wxT("/molcasrc");
//wxMessageBox(strValue2);
    StringUtil::String2CharPointer(&molcasrcDir,strValue1);
    StringUtil::String2CharPointer(&userhome,strValue2);
    StringCopy(&fileName,userhome,LINE_FILE_SIZE);
    fp1=fopen(fileName,"r");
    fp=fp1;
    if(fp1== NULL){
           memset(fileName,0,sizeof(*fileName));
       StringCopy(&fileName,molcasrcDir,LINE_FILE_SIZE);
       fp2=fopen(fileName,"r");
       if(fp2!=NULL) fp=fp2;
    }
    if(fp!=NULL){
            while(!feof(fp)){
                     subindex=-1;
                     memset(buf,0,sizeof(buf));
                     memset(buf_upper,0,sizeof(buf_upper));
                     memset(temp,0,sizeof(temp));
                     if(fgets(buf,LINE_BUF_SIZE,fp) == NULL) buf[0]=0;
                     StringRightTrim(buf,NULL);
                     strcpy(buf_upper,To_upper(buf));
                     for(index=1;index<envir_number;index++){
                                 subindex=SubStringIndex(buf_upper,environment_variable[index],0);
                         if(subindex>=0) break;
            }

                    if(subindex>=0){
                           var_index=SubStringIndex(buf,"=",0)+1;
                           for(i=var_index,j=0;buf[i];i++,j++)
                                   temp[j]=buf[i];
                           temp[j]='\0';
                           strcpy(env_var[index],temp);
                 }

            }//end of while

                   fclose(fp);

             for(i=0;i<envir_number;i++)
             env_molcas[i]=StringUtil::CharPointer2String(env_var[i]);

          } //end of (fp!=NULL)
    return env_molcas;
}
#endif
