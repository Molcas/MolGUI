/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <string.h>

#include "inputparser.h"
#include "keyworddata.h"
#include "templatemanager.h"
#include "cstringutil.h"

#include "tools.h"
#include "stringutil.h"

#define LINE_BUF_SIZE 512

InputParser::InputParser() {
}

InputParser::~InputParser() {
}

bool InputParser::IsEndOfInput(char* buf) {
    return TemplateManager::IsTemplateStart(buf);
}

bool InputParser::IsCommentBegin(char* buf) {
    int k = SubStringIndex(buf, "/*", 0);
    if(k >= 0) {
        //keep comment information
        if(k > 0) strcpy(buf, &buf[k]);
        return true;
    }
    return false;
}

bool InputParser::IsCommentLine(char* buf) {
    char* notBlankLine = NULL;
    if(buf != NULL && strlen(buf) > 1) {
        notBlankLine = StringLeftTrim(buf, NULL);
        if(notBlankLine[0] == '*' || (notBlankLine[0] == '/' && notBlankLine[1] == '*')) {
            return true;
        }
    }
    return false;
}

/*
bool InputParser::IsCommentLineWithModuleStart(char* buf) {
        //e.g. * &SEWARD
        char* notBlankLine = NULL;
        if(buf != NULL && strlen(buf) > 1) {
                notBlankLine = StringLeftTrim(buf, NULL);
                if(notBlankLine[0] == '*'){
                        if(strlen(notBlankLine) > 1) {
                                notBlankLine = StringLeftTrim(&notBlankLine[1], NULL);
                                if(IsModuleStart(notBlankLine)) {
                                        return true;
                                }
                        }
                }
        }
        return false;
}*/

bool InputParser::IsModuleStart(char* nonBlankBuf) {
    if(nonBlankBuf[0] == '&' && strlen(nonBlankBuf) > 1)
        return true;
    return false;
}

bool InputParser::IsCommandStart(char* nonBlankBuf) {
    if((nonBlankBuf[0] == '>' && strlen(nonBlankBuf) > 2) || nonBlankBuf[0] == '!')
        return true;
    return false;
}

bool InputParser::IsMultiLineComment(char* buf) {
    char* notBlankLine = NULL;
    if(buf != NULL && strlen(buf) > 1) {
        notBlankLine = StringLeftTrim(buf, NULL);
        if(notBlankLine[0] == '/' && notBlankLine[1] == '*') {
            return true;
        }
    }
    return false;
}

bool InputParser::IsCommentEnd(char* buf, bool trimComment) {
    int k = SubStringIndex(buf, "*/", 0);
    if(k >= 0) {
        if(trimComment) {
            //keep normal information
            strcpy(buf, &buf[k+2]);
        }
        return true;
    }
    return false;
}

char* InputParser::RemoveCommandPrefix(char* buf) {
    int k = 0, len = 0;
    char* nonBlankBuf = StringLeftTrim(buf, NULL);
    //remove the prefix ">>" or '>'
    if(nonBlankBuf[0] != '>') {
        //some old version does not start with >>
    }else {
        len = strlen(nonBlankBuf);
        k = 0;
        while(k < len && nonBlankBuf[k] == '>') {
            k++;
        }
        nonBlankBuf = &nonBlankBuf[k];
    }
    return StringLeftTrim(nonBlankBuf, NULL);
}

FlowChartModel* InputParser::Parsing(const char* fileName, const char* startLine, const char* availableXYZFile) {
    char buf[LINE_BUF_SIZE];
    bool found = false;
    FILE* fpInput = NULL;
    FlowChartModel* chartModel = NULL;
    fpInput = fopen(fileName, "r");
        if(fpInput == NULL) {
                printf("No such file: %s\n", fileName);
                return NULL;
        }
        if(startLine != NULL) {
        buf[0] = 0;
        while(!feof(fpInput)) {
            if(fgets(buf, LINE_BUF_SIZE, fpInput) == NULL) buf[0]=0;
            //FileUtil::ShowInfoMessage(wxString::Format(wxT("Input Line - %s"), buf));
            if(TemplateManager::IsTemplateStart(buf)
                && IsStringEqualNoCase(buf + strlen(CONST_TEMPLATE_START), startLine, strlen(startLine))) {
                found = true;
                break;
            }
        }
    }else {
        found = true;
    }
        if(found) {
        chartModel = Parsing(GetKeywordData(), fpInput, availableXYZFile);
    }
        if(fpInput) fclose(fpInput);
        return chartModel;
}

FlowChartModel* InputParser::Parsing(KeywordData* keywordData, FILE* fpInput, const char* availableXYZFile) {
    char buf[LINE_BUF_SIZE];
    char* nonBlankBuf = NULL;
    XmlElement* xmlElem = NULL;
    ElementProperty* elemProp = NULL;
    LineType lineType = LINE_NULL;
    //int k = 0;
    //int len = 0;
    FlowChartModel* chartModel = new FlowChartModel();
    ModuleProperty* envSetting = NULL;

        buf[0] = 0;
        while(!feof(fpInput) || strlen(buf) > 0) {
        if(strlen(buf) <= 0) {
            if(fgets(buf, LINE_BUF_SIZE, fpInput) == NULL) buf[0]=0;
        }
        if(IsEndOfInput(buf)) break;
        lineType = LINE_NULL;

        //FileUtil::ShowInfoMessage(wxString::Format(wxT("Real Input Line - %s"), buf));

        nonBlankBuf = StringLeftTrim(buf, NULL);
        if(nonBlankBuf == NULL || strlen(nonBlankBuf) <= 0) {
            buf[0] = 0;
            continue;   //blank line;
        }
        if(IsStringEqualNoCase(nonBlankBuf, CONST_ENV_START, strlen(CONST_ENV_START))) {
            buf[0] = 0;
            if(envSetting == NULL) {
                envSetting = CreateEnvModule(NULL);
                chartModel->SetEnvSetting(envSetting);
            }
            envSetting->ReadFromInputFormat(fpInput, buf, LINE_BUF_SIZE, availableXYZFile);
            continue;
        }
        if(TemplateManager::IsTemplateHelp(&nonBlankBuf[0])) {
            buf[0] = 0;
            continue;
        }
        if(IsCommentLine(&nonBlankBuf[0])) {
            //comment line;
            lineType = LINE_COMMENT;
            xmlElem = keywordData->GetComment();
        }else if(IsModuleStart(nonBlankBuf)) {//nonBlankBuf[0] == '&' && strlen(nonBlankBuf) > 1) {
            lineType = LINE_MODULE;
            //module line
            xmlElem = keywordData->GetModule(&nonBlankBuf[1]);
        }else if(IsCommandStart(nonBlankBuf)) { //(nonBlankBuf[0] == '>' && strlen(nonBlankBuf) > 2) || nonBlankBuf[0] == '!') {
            lineType = LINE_COMMAND;
            //command line
            //remove the prefix ">>" or '>'
            nonBlankBuf = RemoveCommandPrefix(nonBlankBuf);
            //nonBlankBuf = StringLeftTrim(nonBlankBuf, NULL);
            nonBlankBuf = StringTrim(nonBlankBuf, NULL);
            nonBlankBuf = StringRightTrim(nonBlankBuf, "<");
            xmlElem = keywordData->GetCommand(&nonBlankBuf[0]);
        }

        //FileUtil::ShowInfoMessage(wxString::Format(wxT("Input Line - %s"), buf));
        if(lineType != LINE_NULL) {
            if(xmlElem == NULL) {
                Tools::ShowInfo(wxT("Error: no such module/command defined - ") + StringUtil::CharPointer2String(buf));
                xmlElem = keywordData->GetComment();
            }
            elemProp = chartModel->AddXmlElement(xmlElem);
            elemProp->ReadFromInputFormat(fpInput, buf, LINE_BUF_SIZE, availableXYZFile);

            if(IsCommentBegin(buf)) {
                xmlElem = keywordData->GetComment();
                elemProp = chartModel->AddXmlElement(xmlElem);
                elemProp->ReadFromInputFormat(fpInput, buf, LINE_BUF_SIZE, availableXYZFile);
            }
        }else {
            //IsStringEqualNoCase(nonBlankBuf, CONST_END_OF_INPUT, strlen(CONST_END_OF_INPUT)))
            Tools::ShowError(wxT("Unenclosed text - ") + StringUtil::CharPointer2String(buf));
            buf[0] = 0;
        }
    }

    return chartModel;
}

ModuleProperty* InputParser::CreateEnvModule(const char* fileName) {
        ModuleProperty* envModule = (ModuleProperty*)ElementProperty::CreateElementProperty(GetKeywordData()->GetModule(ATTR_VALUE_ENV));
        if(fileName != NULL) {
                FILE* fpInput = NULL;
                fpInput = fopen(fileName, "r");
                //if cannot open, do nothing
                if(fpInput != NULL) {
                        char buf[LINE_BUF_SIZE];
                        char* nonBlankBuf = NULL;
                        buf[0] = 0;
                        while(!feof(fpInput) || strlen(buf) > 0) {
                        if(strlen(buf) <= 0) {
                            if(fgets(buf, LINE_BUF_SIZE, fpInput) == NULL) buf[0]=0;
                        }
                        if(IsEndOfInput(buf)) break;
                                nonBlankBuf = StringLeftTrim(buf, NULL);
                        if(nonBlankBuf == NULL || strlen(nonBlankBuf) <= 0) {
                            buf[0] = 0;
                            continue;   //blank line;
                        }
                        if(IsStringEqualNoCase(nonBlankBuf, CONST_ENV_START, strlen(CONST_ENV_START))) {
                            buf[0] = 0;
                            envModule->ReadFromInputFormat(fpInput, buf, LINE_BUF_SIZE, NULL);
                        }else {
                                        buf[0] = 0;
                                }
                        }
                        fclose(fpInput);
                }
        }
        return envModule;
}

bool InputParser::SaveAsGlobalEnvModule(ModuleProperty* envModule, const char* fileName) {
        FILE* fpOutput = NULL;
        if(!envModule) {
                return false;
        }
    fpOutput = fopen(fileName, "w");
        if(fpOutput == NULL) {
                printf("No such file: %s\n", fileName);
                return false;
        }
        envModule->SaveAsInputFormat(fpOutput);
        fclose(fpOutput);
        return true;
}
