/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <stdlib.h>
#include <string.h>
#include "keyworddata.h"
#include "keywordparser.h"
#include "cstringutil.h"
#include "keywordconst.h"
#include <iostream>

KeywordData* globalKeywordData = new KeywordData();

KeywordData* GetKeywordData(void) {
        return globalKeywordData;
}

KeywordData::KeywordData() {
    rootElementList = NULL;
    modules = NULL;
    moduleCount = 0;
    commands = NULL;
    commandCount = 0;
        undefined = NULL;
        comment = NULL;
        symmetryNumber = 8;
        uniqueAtoms = 1;

        xyzFilePath = NULL;
        optConstraint = NULL;
}

KeywordData::~KeywordData() {
    if(modules) {
        free(modules);
    }
    if(commands) {
        free(commands);
    }
    if(rootElementList) {
        FreeXmlElementList(rootElementList);
    }
        if(xyzFilePath) free(xyzFilePath);
        if(optConstraint) free(optConstraint);
}

int KeywordData::GetSymmetryNumber(void) const {
    return symmetryNumber;
}

void KeywordData::SetSymmetryNumber(int newNum) {
    symmetryNumber = newNum;
}

int KeywordData::GetUniqueAtoms(void) const {
    return uniqueAtoms;
}

void KeywordData::SetUniqueAtoms(int newNum) {
    uniqueAtoms = newNum;
}

/**
 * bridge with molgui
 */
void KeywordData::SetXYZFile(const char* xyzFile) {
        if(xyzFilePath) free(xyzFilePath);
        if(xyzFile != NULL) {
                StringCopy(&xyzFilePath, xyzFile, strlen(xyzFile));
        }else {
                xyzFilePath = NULL;
        }
}

char* KeywordData::GetXYZFile(void) const {
        return xyzFilePath;
}

/**
 * bridge with molgui
 */
void KeywordData::SetOptConstraint(const char* constraintPara) {
        if(optConstraint) free(optConstraint);
        if(constraintPara != NULL) {
                StringCopy(&optConstraint, constraintPara, strlen(constraintPara));
        }else {
                optConstraint = NULL;
        }
}

char* KeywordData::GetOptConstraint(void) const{
        return optConstraint;
}

XmlElement* KeywordData::GetRootElement(void) {
    return &rootElementList->elem;
}

XmlElement** KeywordData::GetModules(void) {
    if(modules == NULL) {
        modules = ::GetModules(GetRootElement(), &moduleCount);
    }
    return modules;
}

XmlElement* KeywordData::GetModule(int index) {
    return GetModules()[index];
}

XmlElement** KeywordData::GetCommands(void) {
    if(commands == NULL) {
        commands = ::GetCommands(GetRootElement(), &commandCount);
    }
    return commands;
}

XmlElement* KeywordData::GetCommand(int index) {
    return GetCommands()[index];
}

int KeywordData::GetModuleCount() {
    if(modules == NULL) {
        GetModules();
    }
    return moduleCount;
}

int KeywordData::GetCommandCount() {
    if(commands == NULL) {
        GetCommands();
    }
    return commandCount;
}

XmlElement* KeywordData::GetUndefined() {
    if(undefined == NULL) {
                undefined = ::GetUndefined(GetComment());
    }
        return undefined;
}

XmlElement* KeywordData::GetComment() {
        if(comment == NULL) {
                comment = GetModule(ATTR_VALUE_COMMENT);
        }
        return comment;
}

void KeywordData::LoadKeyword(const char* fileName) {
    rootElementList = ParseXmlDocument(fileName, true);
}

XmlElement* KeywordData::GetModule(const char* name) {
    int i = 0;
    char* moduleName = NULL;
    const char *gr = "GRID_IT"; // for get rid of modules
    for(i = 0; i < GetModuleCount(); i++) {
        moduleName = GetAttributeString(GetModule(i), ATTR_NAME);
        // std::cout << "KeywordData::GetModule moduleName: " << moduleName << "/" << i << "/" << GetModuleCount() << "/" << name << std::endl;
        //-----------------section to change modules with problmes or require aditional installations
        if (IsStringEqualNoCase(name, gr, strlen(gr))) {
                // KeywordData* keywordData;
                return GetModule(2); //sustituing to 2 (is comment module)
        //---------------------------------------
        } else {
            if(moduleName != NULL && IsStringEqualNoCase(moduleName, name, strlen(moduleName))) { //&& !IsStringEqualNoCase(name, ab, strlen(ab))
                return GetModule(i);
            }
        }
    }
    return NULL;
}

XmlElement* KeywordData::GetCommand(const char* name) {
    int i = 0;
    char* commandName = NULL;
    int gotoIndex = SubStringIndex((char*)name, "GOTO", 0);

    for(i = 0; i < GetCommandCount(); i++) {
        commandName = GetAttributeString(GetCommand(i), ATTR_NAME);
        // std::cout << "KeywordData::GetCommand commandName: " << commandName << std::endl;
        if(gotoIndex >= 0) {
            //IF GOTO is a special command as its name is not the prefix string of a line
            if(commandName!= NULL && SubStringIndex(commandName, "GOTO", 0) >= 0) {
                return GetCommand(i);
            }
        }else {
            if(commandName != NULL && IsStringEqualNoCase(commandName, name, strlen(commandName))) {
                return GetCommand(i);
            }
            commandName = GetAttributeString(GetCommand(i), ATTR_FORMAT);
            if(commandName != NULL && IsStringEqualNoCase(commandName, name, strlen(commandName))) {
                return GetCommand(i);
            }
        }
    }
    return NULL;
}

bool KeywordData::IsInternalCommand(const char* name) {
    char* level = NULL;
    XmlElement* xml = GetCommand(name);
    if(xml != NULL) {
        level  = GetAttributeString(xml, ATTR_LEVEL);
        if(level != NULL && strcmp(level, LEVEL_HIDDEN) == 0) {
            return true;
        }
    }
    return false;
}
