/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_FILE_UTIL_H__
#define __MOLCAS_JOB_MOL_UI_FILE_UTIL_H__

#include "wx.h"

class MingUtil {
        public:

            static wxString GetMolcasHome(void);
                static wxString GetMolcasDir(void);
            static wxString GetMolcasBinDir(void);
                static wxString GetMolcasCommand(void);
                static wxString GetGlobalEnvFile(void);
                static wxString GetSavedDbFile(void) ;
                static wxString GetMolcasBasisDir(void);
        static wxString GetWorkTmpDir(void);
        static wxString GetPreviewFile(void);
        static wxString GetMingIconFile(void);
        static wxString GetHistoryDir(void);
                static wxString GetJobOutputDir(const wxString defaultJobPath);
                /**
                 * Generate usable basis set for each element.
                 */
        static void GenerateBasisSetFiles(void);
                /**
                 * Generate symmetry generator file by invoking findsysm.exe
                 */
        static int GenerateSymmetryWorkFile(wxString inputFile, wxString outFile, double threshold);

                static void ExecGvWindows(wxString fileName, wxString para = wxEmptyString);
                static wxString GetMolcasBasisWorkDir(void);

        private:
                /**
                 * Invoke molcas help basis to output its basis set result
                 */
        static void GenerateBasisSetWorkFiles(void);

};

#endif
