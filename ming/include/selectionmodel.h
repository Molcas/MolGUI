/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_SELECTION_MODEL_H__
#define __MOLCAS_JOB_MOL_MODEL_SELECTION_MODEL_H__

#include "keywordmodel.h"

/**
 * Selection Model is quite different with other models.
 * It may conatin subkeywords.
 * When all its subkeywords are single type, then they will be shrink
 * to one item in the selection
 * If some subkeywords are not single type, it means these subelements
 * also need user input, they will be displayed.
 */
class SelectionModel : public KeywordModel{
    public:
        SelectionModel(ModuleProperty* parentModule, XmlElement* elem);
        ~SelectionModel();

        char* GetDefaultValue(void);
        char** GetSelectionValues(bool isAppear) const;
        char** GetAppearValues(void) const;
        int GetSelectionCount(void) const;
        int GetOmittedSelectionValueCount(void) const;

        char* GetSelectedValue(void) const;
        int GetSelectedIndex(void) const;
        void SetSelectedIndex(int index);
        void SetSelectedModel(KeywordModel* keywordModel);
        KeywordModel* GetSelectedKeywordModel(void) const;
        KeywordModel* GetSelectionKeywordModel(int index) const;
        /**
         * Decide whether the specified keywordmodel need to be displayed or not.
         */
        bool IsNeedShowKeywordModel(KeywordModel* keywordModel) const;

        virtual bool SaveAsInputFormat(FILE* fp, bool isEnv = false);
        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile);
        virtual void DuplicateValue(KeywordModel* const oldKeywordModel);

    private:
        int selectionCount;
        char** seletionValues;
        char** appearValues;
        int selectedIndex;
        bool isColonValue;
        int omittedSelectionValueCount;                // count the number of omitted values (not shown on UI, like ???? or ?DIR)
};

#endif
