/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_GROUP_UI_H__
#define __MOLCAS_JOB_MOL_UI_GROUP_UI_H__

#include "wx.h"
#include "keywordmodel.h"
#include "groupmodel.h"
#include "keywordui.h"

class GroupUI : public KeywordUI {
        private:
                DECLARE_EVENT_TABLE();
                void OnRadioButton(wxCommandEvent& event);
                void OnEnableButton(wxCommandEvent& event);
                void OnClearButton(wxCommandEvent& event);

        protected:
                void CreateUIControls(void);

        public:
                GroupUI(wxWindow *parent, KeywordModel* keywordModel);
                ~GroupUI();
                GroupModel* GetGroupModel(void) const;
                void SetRadioButton(int index, bool value);

                virtual void UpdateFromModel(void);
                virtual void UpdateToModel(bool isRestore);
                virtual bool HasValue(void) const;

        private:
                KeywordUI** arrayPanel;
                wxRadioButton** arrayRadioBtn;
                int origRadioIndex;
                wxButton* btnEnable;
                wxButton* btnClear;
                wxPanel* pnlGroup;
};

#endif
