/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_FLOWCHART_CANVAS_H__
#define __MOLCAS_JOB_MOL_UI_FLOWCHART_CANVAS_H__

#include "elementproperty.h"
#include "flowchartmodel.h"
#include "flowchartchangelistener.h"

#include "flowchartshape.h"
#include "xmlparser.h"

/**
 * Draw flowchart
 * also implement FlowChartChangeListener
 * when there is a change on FlowChartModel, change the canvas correspondingly.
 */
class FlowChartCanvas : public wxPanel, public FlowChartChangeListener {
private:
        DECLARE_EVENT_TABLE();

public:
        FlowChartCanvas(wxWindow *parent);
        virtual ~FlowChartCanvas();
        void OnPaint(wxPaintEvent& event);
        void OnSize(wxSizeEvent& event);
        void OnKeyChar(wxKeyEvent& event);

    wxScrolledWindow* GetParentScrolledWin(void);
    void SetShapeFocus(int elemId) ;
        virtual void OnAddElement(ElementProperty* prop, int elemId, bool updatePosition = true);
        virtual void OnAddElement(int elemId, int count);
        /**
         * when delete or add element, call this function to position elements
         * properly.
         */
        void RePosition(bool isScroll = true);
        /**
         * Delete selected components
         *@param isClear: true to delete all component
         */
    void DeleteSelectedComponents(bool isClear);
    void DeleteAllComponents(void);
        void SetFlowChartModel(FlowChartModel* chartModel);
        int GetInternalIdWithFocus(void);
        ElementProperty* GetElemPropWithFocus(void);
        /**
         * When the specified flowchartshape has gotten focus,
         * make other shapes to lost their focuses.
         */
        void UpdateExclusiveFocusOrSelection(FlowChartShape* shape, bool isFocus);
        /**
         * Force flowchartmodelto recalculate command links
         */
    void UpdateLinks(bool isMultiLine);

protected:
    /**
     * Draw command links
     */
    void DrawLinks(void);
    /**
     * Find a value link shape whose elemnent property is the given linkElemProp
     */
    FlowChartShape* FindValueLinkShape(ElementProperty* linkElemProp);
    /**
     * From the startShape, find a name link target with the same linkindex
     */
        FlowChartShape* FindNameLinkShape(FlowChartShape* startShape, int linkIndex);
        /**
         * Draw a name link between two shapes with the given level
         */
        void DrawNameLink(wxDC* dc, const FlowChartShape* shape1, const FlowChartShape* shape2, int level);
        /**
         * Draw a value link between two shapes with the given level
         * @param isShape1Target: true -> shape1 is the link target, otherwise shpae2 is the link target
         */
        void DrawValueLink(wxDC* dc, const FlowChartShape* shape1, const FlowChartShape* shape2, int level, bool isShape1Target);

        void MoveSelectedComponentUp(void);
        void MoveSelectedComponentDown(void);

private:
        FlowChartModel* flowChartModel;
        FlowChartShape* flowShapes;

};

#endif
