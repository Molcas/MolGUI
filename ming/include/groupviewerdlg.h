/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_GROUP_VIEWER_DLG_H__
#define __MOLCAS_JOB_MOL_UI_GROUP_VIEWER_DLG_H__

#include "groupmodel.h"
#include "wx.h"
#include "keywordui.h"
#include "groupui.h"

/**
 * Show the property of a group
 */
class GroupViewerDlg : public wxDialog {
    DECLARE_EVENT_TABLE()
public:
        //begin : hurukun : 19/11/2009
    GroupViewerDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Group Properties Setting"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
 //           long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX);
                        long style = wxDEFAULT_DIALOG_STYLE | wxMAXIMIZE_BOX);
        //end   : hurukun : 19/11/2009
    ~GroupViewerDlg();

        void SetGroupModel(GroupModel* grpModel);
    void HideDialog(int returnCode);
    bool HasValue(void) const;

private:
    void CreateControls(void);
    void OnDialogOK(wxCommandEvent& event);
    void OnDialogCancel(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

private:
    GroupModel* groupModel;
    GroupUI* groupUI;
    wxPanel* pPnlProperty;
};

#endif
