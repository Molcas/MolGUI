/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_TABLE_MODEL_H__
#define __MOLCAS_JOB_MOL_MODEL_TABLE_MODEL_H__

#include "keywordmodel.h"

/**
 * For multilpe data inputs.
 */
class TableModel : public KeywordModel{

    public:
        TableModel(ModuleProperty* parentModule, XmlElement* elem);
        int GetElementAttrSize(void) const ;
        int GetColumnCount(void) const;
        int GetRowCount(void) const;
        void SetRowCount(int count);

        //int GetComputedNumber(void) const;
        //void SetComputedNumber(int newNumber);

        bool IsComputedArray(void) const;
        bool IsComputedArrayWithSizeOne(void) const;

        virtual bool SaveAsInputFormat(FILE* fp, bool isEnv = false);
        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile);

    private:
        //int computedNumber;
        int columnCount;
                int rowCount;

};

#endif
