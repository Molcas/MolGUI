/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_KEYWORD_DATA_H__
#define __MOLCAS_JOB_MOL_MODEL_KEYWORD_DATA_H__

#include "xmlparser.h"

/**
 * A container for the content in keyword.xml
 */
class KeywordData {
    public:
        KeywordData();
        ~KeywordData();

                XmlElement* GetRootElement(void);
                XmlElement** GetModules(void);
                XmlElement* GetModule(int index);
                XmlElement* GetModule(const char* name);
                XmlElement** GetCommands(void);
                XmlElement* GetCommand(int index);
                XmlElement* GetCommand(const char* name);
                XmlElement* GetComment();
                XmlElement* GetUndefined();
                int GetModuleCount();
                int GetCommandCount();

        bool IsInternalCommand(const char* name);
        void LoadKeyword(const char* fileName);

        int GetSymmetryNumber(void) const;
        void SetSymmetryNumber(int newNum);

        int GetUniqueAtoms(void) const;
        void SetUniqueAtoms(int newNum);

                void SetXYZFile(const char* xyzFile);
                char* GetXYZFile(void) const;
                void SetOptConstraint(const char* constraintPara);
                char* GetOptConstraint(void) const;

    private:
        XmlElementList* rootElementList;
        int commandCount;
        XmlElement** commands;
        int moduleCount;
        XmlElement** modules;
                XmlElement* comment;
        XmlElement* undefined;
        /**
         * this is for global number of symmetry generator, just put here for global access
         */
        int symmetryNumber;
        int uniqueAtoms;
                char* xyzFilePath;
                char* optConstraint;
};

KeywordData* GetKeywordData(void);

#endif
