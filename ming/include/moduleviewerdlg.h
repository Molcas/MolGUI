/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_MODULE_VIEWER_DLG_H__
#define __MOLCAS_JOB_MOL_UI_MODULE_VIEWER_DLG_H__

#include <wx/notebook.h>

#include "moduleproperty.h"
#include "wx.h"
#include "keywordui.h"
#include "xmlparser.h"

/**
 * Show the property of a module
 */
class ModuleViewerDlg : public wxDialog {
    DECLARE_EVENT_TABLE()
public:
    ModuleViewerDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Module Properties Setting"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
            long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX);
    ~ModuleViewerDlg();
        void SetModuleProperty(ModuleProperty* prop, bool isEnvModule = false, bool isGlobal = false);
    void HideDialog(int returnCode);

private:
    void CreateControls(bool isEnvModule, bool isGlobal);
    void OnDialogOK(wxCommandEvent& event);
    void OnDialogCancel(wxCommandEvent& event);
    void OnSaveGlobalEnv(wxCommandEvent& event);
    void OnCheckbox(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

    void DoArrangePanel(void);
    void ShowHiddenWarning(void);

    void UpdateToModel(bool isRestore);

private:
    ModuleProperty* moduleProperty;
    KeywordUI** keywordUIArray;
    wxCheckBox* ckbAdvanced;
    wxCheckBox* ckbEmpty;
    wxNotebook* pTopNotebook;
    wxScrolledWindow* pPnlCommon;
#ifdef __MAC__
    wxStaticText* labelHidden;
#endif
};

#endif
