/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_CHECKBOX_UI_H__
#define __MOLCAS_JOB_MOL_UI_CHECKBOX_UI_H__

#include "checkboxmodel.h"
#include "keywordui.h"

class CheckboxUI : public KeywordUI {
private:
        DECLARE_EVENT_TABLE();
    void OnCheckbox(wxCommandEvent& event);

protected:
           void CreateUIControls(void);

public:
    CheckboxUI(wxWindow *parent, KeywordModel* keywordModel);
    wxCheckBox* GetCheckBox(void) const;
    void SetGroupIndex(int index);

    void DoUIEvent(bool checked);
        virtual void OnSelected(void);
        virtual void UpdateFromModel(void);
        virtual void UpdateToModel(bool isRestore);
        virtual bool HasValue(void) const;

private:
    int groupIndex;
    bool origValue;
};

#endif
