/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_KEYWORD_CHANGE_LISTENER_H__
#define __MOLCAS_JOB_MOL_MODEL_KEYWORD_CHANGE_LISTENER_H__

/**
 * A UI Control implements this interface.
 * When KeywordModel is changed, the keymodel will fire these notifications.
 */
class KeywordChangeListener {
    public:
                virtual ~KeywordChangeListener() {}
                /**
                 * when the keyword is enabled or not.
                 */
                virtual void OnEnabled(void) = 0;
                /**
                 * when the keyword is selected or not.
                 */
                virtual void OnSelected(void) = 0;
                /**
                 * when the keyword should clear its data as there are some input
                 * in its exclusive controls.
                 */
                virtual void OnClearData(void) = 0;
};

#endif
