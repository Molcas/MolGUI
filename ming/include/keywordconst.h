/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_XML_KEYWORD_CONST_H__
#define __MOLCAS_JOB_MOL_XML_KEYWORD_CONST_H__

extern const char* ELEM_COMMAND;
extern const char* ELEM_SUBCOMMAND;
extern const char* ELEM_EMIL;
extern const char* ELEM_GROUP;
extern const char* ELEM_HELP;
extern const char* ELEM_KEYWORD;
extern const char* ELEM_MODULE;
extern const char* ELEM_SELECT;

//--GRP
extern const char* ELEM_ROOT_HELP;
extern const char* ELEM_P_HELP;
extern const char* ELEM_PRE_HELP;


extern const char* ATTR_ALSO;
extern const char* ATTR_APPEAR;
extern const char* ATTR_CONTAINS;
extern const char* ATTR_COMMENT;
extern const char* ATTR_DEFAULTVALUE;
extern const char* ATTR_EXCLUSIVE;
extern const char* ATTR_FILEINDEX;
extern const char* ATTR_FORMAT;
extern const char* ATTR_GUI_INPUT;
extern const char* ATTR_KIND;
extern const char* ATTR_LEVEL;
extern const char* ATTR_LINKTARGET;
extern const char* ATTR_LINKANCHOR;
extern const char* ATTR_LINKVALUEINDEX;
extern const char* ATTR_LIST;
extern const char* ATTR_MAXVALUE;
extern const char* ATTR_MEMBER;
extern const char* ATTR_MINVALUE;
extern const char* ATTR_MODULE;
extern const char* ATTR_NAME;
extern const char* ATTR_ONSCREEN;
extern const char* ATTR_OPTIONALNAME;
extern const char* ATTR_OPTIONALFORMAT;
extern const char* ATTR_REALTIMEUPDATE;
extern const char* ATTR_REQUIRE;
extern const char* ATTR_SELECT;
extern const char* ATTR_SHOWVALUE;
extern const char* ATTR_SIZE;
extern const char* ATTR_VALUES;
extern const char* ATTR_WINDOW;
extern const char* ATTR_WINDOW_SIZE;

extern const char* ATTR_VALUE_ENV;
extern const char* ATTR_VALUE_ENV_PROJECT;
extern const char* ATTR_VALUE_ENV_OUTPUTDIR;
extern const char* ATTR_VALUE_ENV_WORKDIR;
extern const char* ATTR_VALUE_ENV_NEW_WORKDIR;
extern const char* ATTR_VALUE_ENV_KEEP_FILES;
extern const char* ATTR_VALUE_ENV_SAVE;
extern const char* ATTR_VALUE_ENV_MOLCASMEM;
extern const char* ATTR_VALUE_ENV_MOLCASDISK;
extern const char* ATTR_VALUE_ENV_TRAP;
extern const char* ATTR_VALUE_ENV_PRINT;
extern const char* ATTR_VALUE_ENV_LICENSE;
extern const char* ATTR_VALUE_NAME;
extern const char* ATTR_VALUE_DEFAULT;
extern const char* ATTR_VALUE_GATEWAY;
extern const char* ATTR_VALUE_COORD;
extern const char* ATTR_VALUE_BASIS;
extern const char* ATTR_VALUE_BASISXYZ;
extern const char* ATTR_VALUE_GROUP;
extern const char* ATTR_VALUE_GROUP_FULL;
extern const char* ATTR_VALUE_GROUP_NOSYM;
extern const char* ATTR_VALUE_SYMMETRY_THRESHOLD;
extern const char* ATTR_VALUE_SLAPAF;
extern const char* ATTR_VALUE_CONSTRAINTS;
extern const char* ATTR_VALUE_USERINPUT;
extern const char* ATTR_VALUE_USERDIR;
extern const char* ATTR_VALUE_COMMENT;
extern const char* ATTR_VALUE_UNDEFINED;
extern const char* ATTR_VALUE_NSYM;
extern const char* ATTR_VALUE_UNIATOMS;
extern const char* ATTR_VALUE_DEGFREEDOM;
extern const char* ATTR_VALUE_TRUE;
extern const char* ATTR_VALUE_FALSE;
extern const char* ATTR_VALUE_POPUP;
extern const char* ATTR_VALUE_INPLACE;
extern const char* ATTR_VALUE_TAB;
extern const char* ATTR_VALUE_REQUIRED;

extern const char* LOGIC_OR;
extern const char* LOGIC_AND;
extern const char* LOGIC_DELIM;

extern const char* LEVEL_BASIC;
extern const char* LEVEL_ADVANCED;
extern const char* LEVEL_GUI;
extern const char* LEVEL_NOTIMPLEMENTED;
extern const char* LEVEL_HIDDEN;

extern const char* KIND_CHOICE;
extern const char* KIND_FILE;
extern const char* KIND_DIR;
extern const char* KIND_GROUP;
extern const char* KIND_INT;
extern const char* KIND_INTS;
extern const char* KIND_INTS_COMPUTED;
extern const char* KIND_INTS_LOOKUP;
extern const char* KIND_LIST;
extern const char* KIND_REAL;
extern const char* KIND_REALS;
extern const char* KIND_REALS_COMPUTED;
extern const char* KIND_REALS_LOOKUP;
extern const char* KIND_SELECT;
extern const char* KIND_SINGLE;
extern const char* KIND_STRING;
extern const char* KIND_STRINGS;
extern const char* KIND_STRINGS_COMPUTED;
extern const char* KIND_STRINGS_LOOKUP;
extern const char* KIND_CUSTOM;
extern const char* KIND_UNKNOWN;
extern const char* KIND_RADIO;
extern const char* KIND_BOX;
extern const char* KIND_BLOCK;

extern const char* CONST_END_OF;
extern const char* CONST_END_OF_INPUT;
extern const char* CONST_EXPORT;
extern const char* CONST_EXPORT_OUTPUT;
extern const char* CONST_ENV_START;
extern const char* CONST_ENV_END;
extern const char* CONST_TEMPLATE_START;
extern const char* CONST_TEMPLATE_HELP;

typedef enum _keywordKind {
    KEYWORD_KIND_NULL,
    KEYWORD_KIND_BLOCK,
    KEYWORD_KIND_BOX,
    KEYWORD_KIND_CHOICE,
    KEYWORD_KIND_FILE,
    KEYWORD_KIND_DIR,
    KEYWORD_KIND_GROUP,
    KEYWORD_KIND_INT,
    KEYWORD_KIND_INTS,
    KEYWORD_KIND_INTS_COMPUTED,
    KEYWORD_KIND_INTS_LOOKUP,
    KEYWORD_KIND_LIST,
    KEYWORD_KIND_RADIO,
    KEYWORD_KIND_REAL,
    KEYWORD_KIND_REALS,
    KEYWORD_KIND_REALS_COMPUTED,
    KEYWORD_KIND_REALS_LOOKUP,
    KEYWORD_KIND_SELECT,
    KEYWORD_KIND_SINGLE,
    KEYWORD_KIND_STRING,
    KEYWORD_KIND_STRINGS,
    KEYWORD_KIND_STRINGS_COMPUTED,
    KEYWORD_KIND_STRINGS_LOOKUP,
    KEYWORD_KIND_CUSTOM,
    KEYWORD_KIND_UNKNOWN
}KeywordKind;

typedef enum _windowKind {
    WINDOW_KIND_POPUP,
    WINDOW_KIND_TAB,
    WINDOW_KIND_INPLACE
}WindowKind;

/**
 * used for seperate value be value
 * e.g. "a ${VALUE_DELIM} b ${VALUE_DELIM} c" means it contans three items, a, b and c.
 */
extern const char* VALUE_DELIM;
/**
 * used for seperate line be line
 * e.g. "line1 ${LINE_DELIM} line2" means it contans two lines, line1 and line2.
 */
extern const char* LINE_DELIM;

#endif
