/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_FILE_EDIT_DLG_H__
#define __MOLCAS_JOB_MOL_UI_FILE_EDIT_DLG_H__

#include "wx.h"

/**
 * Manipulate file contents dialog.
 */
class FileEditDlg : public wxDialog {
    DECLARE_EVENT_TABLE()
public:
    FileEditDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("File Editor"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(700, 500), long style = wxDEFAULT_DIALOG_STYLE | wxMAXIMIZE_BOX | wxRESIZE_BORDER);
    ~FileEditDlg();

    void HideDialog(int returnCode);
    void SetNeedSubmitBtn(bool isNeed);
    void SetNeedSaveBtn(bool isNeed);
    void CreateControls(void);
    void ReadFile(wxString file);
    void UpdateDialogTitle(void);

private:
    void OnDialogSubmit(wxCommandEvent& event);
    void OnDialogSave(wxCommandEvent& event);
    void OnDialogOk(wxCommandEvent& event);
    void OnDialogCancel(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);
    bool SaveFile(void);

private:
    bool needSubmitBtn;
    bool needSaveBtn;
    wxTextCtrl* textCtrl;
    wxString fileName;

    enum {
        CTRL_ID_BTN_SUBMIT = 1
    };
};

#endif
