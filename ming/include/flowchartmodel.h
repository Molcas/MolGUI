/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_FLOWCHART_MODEL_H__
#define __MOLCAS_JOB_MOL_MODEL_FLOWCHART_MODEL_H__

#include "flowchartchangelistener.h"
#include "flowlinkdata.h"
#include "keywordmodel.h"
#include "elementproperty.h"

/**
 * The data model for flowchart
 */
class FlowChartModel {
    public:
        FlowChartModel();
        ~FlowChartModel();

        /**
         * As original structure data for commands and modules are xmlelements,
         * therefore use this method to add a command and module to this model.
                 * @param elemId: -1 to add at the end
                 *                                         >=0 to add after the given element id.
         */
                ElementProperty* AddXmlElement(XmlElement* elem, int elemId = -1);
                /**
                 * Load template
                 */
                void AddFlowChartModel(FlowChartModel* fModel, int elemId = -1);
                ElementProperty* GetElementPropertyHead(void) const;
                void ClearElementPropertyHead(void);
                int GetLastInternalId(void);
                bool IsEmpty(void) const;
                /**
                 * Delete elements with the given internal ids
                 * @param arrayLen: the length of array
         * @return: returh the number of elements deleted.
                 */
                int DeleteElements(int internalIds[], int arrayLen);
                /**
                 * Delete one element with the given internal id.
                 * @return: true is success.
                 */
                bool DeleteElement(int internalId, bool updateLink = true);
                void DeleteAllElements(void);

                void MoveElementUp(int internalId);
                void MoveElementDown(int internalId);

                /**
                 * When there is a change in this model, the registered change listener
                 * will be informed.
                 * @note: only one listener can be added now.
                 */
                void RegisterChangeListener(FlowChartChangeListener* listener);

                bool SaveAsInputFormat(const char* fileName);

                FlowLinkData* GetFlowLinkData(void) const;

                ModuleProperty* GetEnvSetting(void) const ;
        void SetEnvSetting(ModuleProperty* envSetting);

    private:
        /**
         * Data components in the model.
         */
                ElementProperty* elemPropHead;
                ModuleProperty* envModule;
                /**
                 * Change listener.
                 */
                FlowChartChangeListener* flowListener;
                /**
                 * For keeping command link relationship
                 */
                FlowLinkData* flowLinkData;
};

#endif
