/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_SELECTION_UI_H__
#define __MOLCAS_JOB_MOL_UI_SELECTION_UI_H__

#include "wx.h"
#include "keywordmodel.h"
#include "selectionmodel.h"
#include "keywordui.h"

class SelectionUI : public KeywordUI {
        public:
                SelectionUI(wxWindow *parent, KeywordModel* keywordModel);
                ~SelectionUI();
                wxComboBox* GetComboBox(void) const;
                SelectionModel* GetSelectionModel(void) const;
                virtual void OnSelected(void);
                virtual void UpdateFromModel(void);
                virtual void UpdateToModel(bool isRestore);
                virtual bool HasValue(void) const;
                #if defined (__LINUX__)
                wxString Getdefaultvalue_sel(void);//get the default environment value in molcarc file.
        #endif
        private:
                DECLARE_EVENT_TABLE();
                void OnComboBox(wxCommandEvent& event);
                void OnDirBtn(wxCommandEvent& event);

        protected:
                void CreateUIControls(bool isReadOnly, bool isNeedDirBtn);

        private:
                wxArrayString arraySelection;
                wxPanel** arrayPanel;
                int origSelectedIndex;
                char* origSelectedValue;

};

#endif
