/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_XML_KEYWORD_PARSER_H__
#define __MOLCAS_JOB_MOL_XML_KEYWORD_PARSER_H__

#include "keywordconst.h"

typedef enum _lineType {
    LINE_NULL,
    LINE_COMMENT,
    LINE_COMMAND,
    LINE_MODULE
}LineType;

typedef enum _logicOpe {
        ENUM_LOGIC_OR,
        ENUM_LOGIC_AND
}LogicOpe;

typedef struct _logicData {
    LogicOpe ope;
    int count;
    char** values;
}LogicData;

/**
 * @param rootElem: the root element of xml document;
 * @param count: (out) the number of modules;
 * @return the pointer array to modules
 */
XmlElement** GetModules(XmlElement* rootElem, int* count);

XmlElement** GetKeywords(XmlElement* moduleElem, int* count);

XmlElement* GetUndefined(XmlElement* commentElem);

/**
 * @param rootElem: the root element of xml document;
 * @param count: (out) the number of commands;
 * @return the pointer array to commands
 */
XmlElement** GetCommands(XmlElement* rootElem, int* count);

char* GetKeywordKindString(XmlElement* elem);

KeywordKind GetKeywordKind(XmlElement* elem);

/**
 * The attrName contains some logic operations.
 * This string will be parsed, and the result is contained in LogicData.
 */
LogicData GetAttributeLogicData(XmlElement* elem, const char* attrName);
/**
 * Get the help string of the given element.
 */
char* GetElementHelp(XmlElement* elem);

char* GetElementP_Help(XmlElement* elem);

void DumpAttribute(XmlElement* elem);

#endif
