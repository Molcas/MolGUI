/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_LISTBOX_MODEL_H__
#define __MOLCAS_JOB_MOL_MODEL_LISTBOX_MODEL_H__

#include "keywordmodel.h"

class ListBoxModel : public KeywordModel{
    public:
        ListBoxModel(ModuleProperty* parentModule, XmlElement* elem);

        bool IsSymmetryGenerator(void);
        virtual void SetValue(const char* newValue);
        virtual bool SaveAsInputFormat(FILE* fp, bool isEnv = false);
        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile);

    private:

};

#endif
