/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_TEXTCTRL_UI_H__
#define __MOLCAS_JOB_MOL_UI_TEXTCTRL_UI_H__

#include "wx.h"
#include "keywordmodel.h"
#include "textctrlmodel.h"
#include "keywordui.h"

class TextCtrlUI : public KeywordUI {
private:
        DECLARE_EVENT_TABLE();
    void OnText(wxCommandEvent& event);

protected:
    void CreateUIControls(void);

public:
        TextCtrlUI(wxWindow *parent, KeywordModel* keywordModel);
    ~TextCtrlUI();

    wxTextCtrl* GetTextCtrl(void) const;
    TextCtrlModel* GetTextCtrlModel(void) const;
    virtual void UpdateFromModel(void);
        virtual void UpdateToModel(bool isRestore);
        virtual bool HasValue(void) const;
        #if defined (__LINUX__)
        wxString Getdefaultvalue_text(void); //get the default value of MOLCASMEM in molcasrc
    #endif
private:
    char* origValue;
    bool isRealtimeUpdate;
};

typedef enum _FormatTextCtrlType {
        FORMAT_TEXT_CTRL_NULL,
        FORMAT_TEXT_CTRL_INT,
        FORMAT_TEXT_CTRL_REAL,
        FORMAT_TEXT_CTRL_STRINGS
}FormatTextCtrlType;

class FormatTextCtrl : public wxTextCtrl {
public:
        FormatTextCtrl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& value = wxEmptyString,
                const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
                long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("FormatTextCtrl"));
        void SetFormatType(FormatTextCtrlType type, const char* charMinValue, const char* charMaxValue);
        void SetFormatType(FormatTextCtrlType type, double minValue, double maxValue);
        void SetMaxLineCount(const char* lineCount);

private:
        DECLARE_EVENT_TABLE();
        void OnInputChar(wxKeyEvent& event);
        void OnKillFocus(wxFocusEvent& event);

private:
        FormatTextCtrlType m_formatType;
        double m_minValue;
        double m_maxValue;
};

#endif
