/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_TEMPLATE_MANAGER_H__
#define __MOLCAS_JOB_MOL_MODEL_TEMPLATE_MANAGER_H__

/**
 * An molcas template manger.
 */
class TemplateManager {
    public:
        TemplateManager();
        ~TemplateManager();

        static bool IsTemplateStart(const char* buf);
        static bool IsTemplateHelp(const char* buf);
        static char** GetTemplateNames(const char* fileName, int* count);
    private:
};
#endif
