/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_FLOWCHART_CHANGE_LISTENER_H__
#define __MOLCAS_JOB_MOL_MODEL_FLOWCHART_CHANGE_LISTENER_H__

#include "elementproperty.h"
#include "flowchartmodel.h"

/**
 * The FlowChartCanvas will implement this interface to receive notifications
 * when FlowChartModel changes.
 */
class FlowChartChangeListener {
    public:
                virtual ~FlowChartChangeListener() {}
                /**
                 * when the flowchartmodel add an element.
                 */
        virtual void OnAddElement(ElementProperty* prop, int elemId, bool updatePosition=true) = 0;
        /**
         * start from elemId, there are total count elements added.
         */
        virtual void OnAddElement(int elemId, int count) = 0;
                /**
                 * when the flowchartmodel delete elements.
                 */
        //virtual void OnDeleteElement() = 0;
};

#endif
