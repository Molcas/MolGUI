/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_FLOWCHART_SHAPE_H__
#define __MOLCAS_JOB_MOL_UI_FLOWCHART_SHAPE_H__

#include "wx.h"
#include "moduleviewerdlg.h"
#include "keywordmodel.h"

typedef enum  _shapeState{
    STATE_UP,
    STATE_DOWN,
}ShapeState;

class FlowChartCanvas;

/**
 * The UI representation of a command or a module
 */
class FlowChartShape : public wxWindow {
public:
    //DECLARE_DYNAMIC_CLASS (FlowChartShape);

private:
    DECLARE_EVENT_TABLE();

public:
    FlowChartShape(FlowChartCanvas *parent, wxWindowID id, ElementProperty* prop, const wxSize& size = wxDefaultSize);
        virtual ~FlowChartShape();
    void OnPaint(wxPaintEvent &event);
    void OnMouseEvent(wxMouseEvent& event);
    void OnKeyChar(wxKeyEvent& event);
    void OnEraseBackground(wxEraseEvent &event);

        virtual void SetSize(int x, int y, int width, int height, int sizeFlags = wxSIZE_AUTO);

    void ShowModulePropertyViewer(void);
        void ShowCommandPropertyViewer(void);
        int GetLabelLineCount(void) const;

    ElementProperty* GetElementProperty(void) const;
    /**
     * FlowChartShapes are connected in a list.
     */
        FlowChartShape* GetNextShape(void) const;
        void SetNextShape(FlowChartShape* shape);
        int AddShape(FlowChartShape* shape, int elemId);

        bool IsSelected(void) const;
        void SetSelected(bool selected);

        bool IsHasFocus(void) const;
        void SetHasFocus(bool focus);

protected:
    void DrawOnBitmap(void);
    void Redraw(void);
    void DispathEvent(void);
    /**
     * Draw different shapes.
     */
    void DrawRectangle(wxDC& dc, int width, int height);
    void DrawRoundRect(wxDC& dc, int width, int height);
    void DrawParallelogram(wxDC& dc, int width, int height);
        void DrawComment(wxDC& dc, int width, int height);
        void SetShapeState(ShapeState state);
        ShapeState GetShapeState(void) const;

private:
        FlowChartShape* nextChartShape;
        FlowChartCanvas* parentCanvas;
        ModuleViewerDlg* moduleViewerDlg;
        //
        wxBitmap* pBitmap;
        ElementProperty* elemProp;
    KeywordModel* keywordModel;
    ShapeState shapeState;
    bool isPainting;
    bool isSelected;
    bool isHasFocus;
};

#endif
