/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_TABLE_EDIT_DLG_H__
#define __MOLCAS_JOB_MOL_UI_TABLE_EDIT_DLG_H__

#include "wx.h"

/**
 * Manipulate table contents dialog.
 */
class TableEditDlg : public wxDialog {
    DECLARE_EVENT_TABLE()
public:
        //begin : hurukun : 19/11/2009
    TableEditDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Table Editor"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
 //           long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX);
          long style = wxDEFAULT_DIALOG_STYLE |  wxMAXIMIZE_BOX);
        //end   : hurukun : 19/11/2009
    ~TableEditDlg();

    void HideDialog(int returnCode);

private:
    void CreateControls(void);
    void OnDialogOK(wxCommandEvent& event);
    void OnDialogCancel(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

private:

};

#endif
