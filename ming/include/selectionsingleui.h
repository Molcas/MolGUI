/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_SELECTION_SINGLE_UI_H__
#define __MOLCAS_JOB_MOL_UI_SELECTION_SINGLE_UI_H__

#include "wx.h"
#include "keywordmodel.h"
#include "selectionmodel.h"
#include "keywordui.h"

class SelectionSingleUI : public KeywordUI {

protected:
    void CreateUIControls(void);

private:
    wxPanel** arrayPanel;

public:
        SelectionSingleUI(wxWindow *parent, KeywordModel* keywordModel);
        ~SelectionSingleUI();
    SelectionModel* GetSelectionModel(void) const;

    virtual void UpdateFromModel(void);
    virtual void UpdateToModel(bool isRestore);
    virtual bool HasValue(void) const;
};

#endif
