/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_SLIDING_WINDOW_H__
#define __MOLCAS_JOB_MOL_UI_SLIDING_WINDOW_H__

#include "wx.h"
#include "datatype.h"
#include <wx/splitter.h>

class SlidingWindow : public wxPanel {
    DECLARE_EVENT_TABLE()
public:

    SlidingWindow(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL , const wxString& name = wxT("slidingWindow"));
    ~SlidingWindow();

    void Init(void);
    void AddPages(wxArrayString& labels);
    wxPanel* GetPage(int level);

private:
    void OnButton(wxCommandEvent& event);
    wxPanel* CreatePanel(wxWindow* parent, wxString& label, int level);

private:
    int winCount;
    WindowPtrArray splitWinArray;
    WindowPtrArray pageArray;
    WindowPtrArray pnlArray;
};

#endif
