/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_KEYWORD_MODEL_H__
#define __MOLCAS_JOB_MOL_MODEL_KEYWORD_MODEL_H__

#include "xmlparser.h"
#include "keywordparser.h"
#include "moduleproperty.h"
#include "keywordchangelistener.h"

/**
 * A data model for a keyword.
 */
class KeywordModel {
    public:
                /**
                 * @param parentModule: the parent module of this keywordmodel
                 * @param elem: the structure of the keyword
                 */
        KeywordModel(ModuleProperty* parentModule, XmlElement* elem);
        virtual ~KeywordModel();

        /**
         * The keyword is enabled or not for input.
         * @param updateUI: notify UI or not
         */
        void SetEnabled(bool isEnabled, bool updateUI);
        bool IsEnabled(void) const;

                /**
                 * As Keyword is a subelement of Module
                 */
                ModuleProperty* GetParentModule(void) const;
        /**
         * Containes all its subkeywords.
         */
        ModuleProperty* GetChildModule(void) const;
                void SetChildModule(ModuleProperty* moduleProp);
                /**
                 * The keyword is selected or not.
                 */
                void SetSelected(bool selected, bool updateUI);
        bool IsSelected(void) const;

        /**
         * Clear all its contained data or not.
         */
        void ClearData(bool updateUI);

        /**
         * If this keywordmodel is selected, notify otherwise keywordmodels,
         * due to REQUIRE or EXCLUSIVE
         */
        void NotifySelection(void);
        /**
         * The given keywordmodel has been selected or not, update the state correspondingly.
         */
        void ReceiveSelection(const KeywordModel* updatedKeyword);
        /**
         * If this keywordmodel is selected, notify otherwise keywordmodels,
         * due to the EXCLUSIVE constraint.
         */
        void NotifyExclusive(void);

        /**
         * The value of a keyword will be saved as a string,
         * no matter its kind.
         */
        virtual void SetValue(const char* newValue);
        char* GetValue(void) const;
        /**
         * The element tag.
         */
        char* GetElementTag(void) const;

        // char* GetP_Help(void) const;
        char* GetHelp(void) const;
        /**
         * The attribute value of ATTR_NAME. (Worked as xmlelement id)
         */
        char* GetName(void) const;
        /**
         * The attribute value of ATTR_ALSO. (Worked as xmlelement id)
         */
        char* GetAlso(void) const;
        /**
         * The attribute value of ATTR_APPEAR.
         * If the value is empty, the value of GetName() will be returned.
         */
        char* GetAppear(void) const;
        /**
         * The string kind of the xmlelement.
         * @See GetKind(void)
         */
        char* GetKindString(void) const;
        /**
         * Return enum type of kind.
         */
        KeywordKind GetKind(void) const;
        char* GetMinValue(void) const;
        char* GetMaxValue(void) const;
        /**
         * Get the attribute value of the given attribute name.
         */
        char* GetAttributeValue(const char* attrName) const;
        bool IsUndefined(void) const;
        /**
         * Indicates whether this keyword must be input something or not.
         */
        bool IsInputRequired(void) const;
        /**
         * Get the xmlelement of this keywordmodel represents.
         */
        XmlElement* GetXmlElement(void);
        /**
         * The attribute value of REQUIRE.
         */
        LogicData* GetRequireData(void);
        /**
         * The attribute value of EXCLUSIVE.
         */
        LogicData* GetExclusiveData(void);

        /**
         * Get the how many columns the keyword needs to show its content.
         */
                virtual int GetColumnSize(void) const;
        /**
         * Get the how many sizes the keyword needs to show its content.
         */
                virtual int GetRowSize(void) const;

        /**
         * Register a change listener.
         * If the value of the keyword changes, the listener will be notified.
         */
        void RegisterChangeListener(KeywordChangeListener* changeListener);
                void UnRegisterChangeListener(void);

        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) = 0;
        virtual bool SaveAsInputFormat(FILE* fp, bool isEnv = false) = 0;
        virtual void DuplicateValue(KeywordModel* const oldKeywordModel);

        void SetGroupRadio(bool isRadio);
            bool IsGroupRadio(void) const;

    protected:
                /* The parent module of this keymodel. */
        ModuleProperty* parentModule;
                /* If this keywordmodel contains child keywords. */
                ModuleProperty* childModule;
        /* The xmlelement of this keymodel represents. */
        XmlElement* xmlElement;
        /* Is enabled or not */
        bool isEnable;
        /* The value of the keymodel. */
        char* value;
        /* The logic operation data given by REQUIRE. */
        LogicData requireData;
        /* The logic operation data given by EXCLUSIVE. */
        LogicData exclusiveData;
        /* The change listener. */
        KeywordChangeListener* changeListener;
        /* Is a model of Group with type RADIO*/
        bool isGroupRadio;

};

#endif
