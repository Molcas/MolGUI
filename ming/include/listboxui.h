/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_LISTBOX_UI_H__
#define __MOLCAS_JOB_MOL_UI_LISTBOX_UI_H__

#include "listboxmodel.h"
#include "keywordui.h"

/**
 * Show list like data.
 * Now it is used for showing basis set and group generator.
 */
class ListBoxUI : public KeywordUI {
        private:
                DECLARE_EVENT_TABLE();

                void OnModifyButton(wxCommandEvent& event);
                void SetUniqueAtomCount(const char* symgen);
                wxString AddSymmetryGeneratorName(const char* value);

                wxString GenerateFindSymOut(void);
                void ShowBasisSetDlg(void);
                void ShowGroupDlg(void);

        protected:
                void CreateUIControls(void);
                char** ConvertXYZ2SymInput(int* elemCount);

        public:
                ListBoxUI(wxWindow *parent, KeywordModel* keywordModel);

                ListBoxModel* GetListBoxModel(void);
                virtual void UpdateFromModel(void);
                virtual void UpdateToModel(bool isRestore);
                virtual bool HasValue(void) const;

        private:
                wxListBox* listBox;
                int uiType;

                enum {
                        UI_TYPE_NULL,
                        UI_TYPE_BASIS_SET,
                        UI_TYPE_GROUP
                };
};

#endif
