/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_MING_FRM_H__
#define __MOLCAS_JOB_MOL_UI_MING_FRM_H__

#include "wx.h"
#include "mingpnl.h"


/**
 * The main frame for showing the details of the job generator.
 */
class MingFrm : public wxFrame {
        public:

                MingFrm(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("MING"),
                                  const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
                                  long style = wxCAPTION | wxSYSTEM_MENU | wxRESIZE_BORDER | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX|wxSTAY_ON_TOP);

                ~MingFrm();

                void Init(void);
                void CreateControls(void);
                void DoFileOpen(wxString openFileName);

        private:
                DECLARE_EVENT_TABLE()
                void OnClose(wxCloseEvent& event);
                void OnFileOpen(wxCommandEvent& event);
                void OnFileClose(wxCommandEvent& event);
                void OnFileSave(wxCommandEvent& event);
                void OnFileSaveAs(wxCommandEvent& event);
                void OnFileExit(wxCommandEvent& event);
                void DoFileExit(void);
                void OnEnvSettings(wxCommandEvent& event);
                void OnHelp(wxCommandEvent& event);

        private:
                MingPnl* m_pMingPnl;
                wxHtmlHelpController* m_helpCtrl;
};


#endif
