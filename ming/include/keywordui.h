/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_KEYWORD_UI_H__
#define __MOLCAS_JOB_MOL_UI_KEYWORD_UI_H__

#include "wx.h"
#include "keywordmodel.h"
#include "keywordchangelistener.h"
#include "stringutil.h"
#define CTRL_WIN_CTRL_ID 100
#define CTRL_WIN_CTRL_CUSTOM_START 105

#define CTRL_WIN_RADIO_START 120

#define UI_LABEL_WIDTH 100
#define UI_INT_WIDTH 50
#define UI_REAL_WIDTH 100
#define UI_WIDTH_UNIT 100
#define UI_HEIGHT_UNIT 30
// added by chaochao molcasrc
#if defined (__LINUX__)
#define envir_number  16
#define envir_len 25
const char environment_variable[envir_number][envir_len]={
                                 "","MOLCAS_WORKDIR",
                                 "MOLCAS_NEW_WORKDIR",
                                   "MOLCAS_KEEP_FILES",
                                 "MOLCAS_PROJECT",
                                 "MOLCAS_OUTPUT",
                                 "MOLCAS_SAVE",
                                 "MOLCASMEM",
                                 "MOLCASDISK",
                                 "MOLCASRAMD",
                                  "MOLCAS_MOLDEN",
                                  "MOLCAS_PROPERTIES",
                                 "MOLCAS_TRAP",
                                 "MOLCAS_PRINT",
                                 "MOLCAS_LINK",
                                 "MOLCAS_LICENSE",
                               };
#endif //chaochao  added molcasrc

/**
 * A parent class for different keyword
 */
class KeywordUI : public wxPanel, public KeywordChangeListener {
protected:
    /**
     * Create sizer for this ui
     * Called by derived classes.
     * @param orient: the orientation of the sizer: horizontal or vertical
     * @param needBox: whether draw a surrounding line box around this ui.
     */
    void CreateToftSizer(int orient, bool needBox);


protected:
    wxWindow* windowCtrl;
    KeywordModel* keywordModel;

public:
        KeywordUI(wxWindow *parent, KeywordModel* keywordModel);
        ~KeywordUI();
        wxString GetAppearString(void);

        KeywordModel* GetKeywordModel(void);
        #if defined (__LINUX__)
        wxArrayString  molcasrc_var(void);//chaochao
        char *To_upper(char *);// added by chaochao
    #endif
        /**
         * whether this ui contains some user input
         */
        virtual bool HasValue(void) const;

    virtual void OnEnabled(void);
        virtual void OnSelected(void);
        virtual void OnClearData(void);

    /**
     * Update data from keywordmodel
     */
    virtual void UpdateFromModel(void);
        virtual void UpdateToModel(bool isRestore);
        /**
         * Input has been inputed and is valid
         */
        virtual bool InputValidate(void);
        int GetColumnSize(void) const;
        int GetRowSize(void) const;

    /**
     * Factory method for creating corresponding keywordui with the given keywordmodel.
     */
    static KeywordUI* CreateModelUI(wxWindow* parent, KeywordModel* keywordModel);
};

#endif
