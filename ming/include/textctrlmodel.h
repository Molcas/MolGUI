/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_TEXTCTRL_MODEL_H__
#define __MOLCAS_JOB_MOL_MODEL_TEXTCTRL_MODEL_H__

#include "keywordmodel.h"

/**
 * For one line or multiple line data inputs.
 */
class TextCtrlModel : public KeywordModel{
    public:
        TextCtrlModel(ModuleProperty* parentModule, XmlElement* elem);
        char* GetDefaultValue(void);

        bool IsComment(void) const;
        bool IsOptConstraint(void ) const;

        void AppendData(char* buf);
        virtual bool SaveAsInputFormat(FILE* fp, bool isEnv = false);
        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile);

        private:
                bool isUndefined;
};

#endif
