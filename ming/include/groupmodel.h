/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_GROUP_MODEL_H__
#define __MOLCAS_JOB_MOL_MODEL_GROUP_MODEL_H__

#include "keywordmodel.h"

/**
 * Group Model is quite different with other models.
 * It may conatin subkeywords.
 */
class GroupModel : public KeywordModel{
    public:
        GroupModel(ModuleProperty* parentModule, XmlElement* elem);
        ~GroupModel();

        KeywordModel* GetSubKeywordModel(const char* name) const;
        int GetSubKeywordModelCount(void) const;
        /**
         * Return enum type of group kind.
         */
        KeywordKind GetGroupKind(void);
        WindowKind GetWindowKind(void);
        int GetSelectedIndex(void) const;
        void SetSelectedIndex(int index);

        virtual bool SaveAsInputFormat(FILE* fp, bool isEnv = false);
        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile);
        virtual void DuplicateValue(KeywordModel* const oldKeywordModel);

    private:
        int selectedIndex;
};

#endif
