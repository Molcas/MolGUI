/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_MODEL_CONST_H__
#define __MOLCAS_JOB_MOL_MODEL_MODEL_CONST_H__

typedef enum _elemPropType {
        ELEM_PROP_TYPE_NULL,
        ELEM_PROP_TYPE_COMMAND,
        ELEM_PROP_TYPE_MODULE,
        ELEM_PROP_TYPE_SELECT,
        ELEM_PROP_TYPE_GROUP
}ElemPropType;

typedef enum _molCompType {
        MOL_COMP_TYPE_NULL,
        MOL_COMP_TYPE_COMMAND,
        MOL_COMP_TYPE_MODULE,
        MOL_COMP_TYPE_TEMPLATE
}MolCompType;

#endif
