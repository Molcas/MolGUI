/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_LIST_ITEM_SEL_DLG_H__
#define __MOLCAS_JOB_MOL_UI_LIST_ITEM_SEL_DLG_H__

#include "wx.h"

class ListItemSelDlg : public wxDialog {
    DECLARE_EVENT_TABLE()
public:
        //begin : hurukun : 19/11/2009
    ListItemSelDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("List Item Selection Dialog"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
//            long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX);
            long style = wxDEFAULT_DIALOG_STYLE| wxMAXIMIZE_BOX);
        //end   : hurukun : 19/11/2009
    ~ListItemSelDlg();

    void HideDialog(int returnCode);

    void SetSelectableItems(char** items, int count);
    void SetSelectableItems(const wxArrayString& items);
    wxArrayString GetSelectedItems(void);
    void SetSelectedItems(const wxArrayString& items);
    /**
     * The maximum number of items to be selected.
     */
    void SetSelectableItemCount(int count);

private:
    void CreateControls(void);
    void OnDialogOK(wxCommandEvent& event);
    void OnDialogCancel(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

    void OnLbItemsClicked(wxCommandEvent& event);
    void OnLbItemsDClicked(wxCommandEvent& event);
    void OnLbSelectedDClicked(wxCommandEvent& event);

    void OnAddItem(wxCommandEvent& event);
    void OnDelItem(wxCommandEvent& event);

private:
    wxListBox* pLbItems;
    wxListBox* pLbSelected;
    int selecteableItemCount;

    enum {
        CTRL_ID_LB_ITEMS = 1000,
        CTRL_ID_LB_SELECTED,

        CTRL_ID_BTN_ADDITEM,
        CTRL_ID_BTN_DELITEM
    };
};

#endif
