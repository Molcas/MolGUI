/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_COMMAND_PROPERTY_H__
#define __MOLCAS_JOB_MOL_MODEL_COMMAND_PROPERTY_H__

#include "elementproperty.h"
#include "keywordmodel.h"
#include "keywordconst.h"

/**
 * A data model for a command.
 */
class CommandProperty : public ElementProperty {
    public:
        CommandProperty(XmlElement* elem);
        ~CommandProperty();

        /**
         * Get the type of elementproperty.
         * It should be ELEM_PROP_TYPE_COMMAND.
         */
        virtual ElemPropType GetPropType(void) const;
        virtual void SetPropValue(const char* newValue);

        virtual bool SaveAsInputFormat(FILE* fp);
        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile);

        virtual void DuplicateValue(ElementProperty* const oldElemProp);

        /**
         * printf buf as the format with values from valueIndex
         * @return: number of values has been printed
         */
        int StringPrintf(char* buf, const char* format, const char* values, int valueIndex);
        /**
         * scanf line as format
         * if matched, return true, results will be saved in buf
         * otherwise return false;
         */
        bool StringScanf(char* buf, const char* format, const char* line, int stuffIndex, const char* stuffValue);
        bool StringScanf(XmlElement* elem, char* buf, const char* format, const char* line);
        virtual bool IsValid(void);
        virtual void ClearData(void);

        void GetFormat(const char* format, int index, const char* value, char* outFormat) ;
        char* GetFormat(XmlElement* elem, bool optional) const;
        char* GetOptionalName(void) const;
        bool IsNeedUserInput(void) const;

        char* GetIndexedPropValue(int index);
        bool IsEqualIndexedPropValue(int index, const char* cmpValue);
        bool IsValueLinkCommandAnchor(void) const;
        bool IsValueLinkCommandTarget(void) const;
        bool IsNameLinkCommand(void) const;
        bool IsNameLinkCommandTarget(const char* name) const;

        void ToLabelString(char* label, int len);
};

#endif
