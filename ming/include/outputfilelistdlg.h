/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_OUTPUT_FILE_LIST_DLG_H__
#define __MOLCAS_JOB_MOL_UI_OUTPUT_FILE_LIST_DLG_H__

#include "wx.h"

/**
 * List molcas output file dialog.
 */
class OutputFileListDlg : public wxDialog {

public:
        //begin : hurukun : 19/11/2009
    OutputFileListDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Output File List"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(350, 500),
 //           long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX);
         long style = wxDEFAULT_DIALOG_STYLE |wxSTAY_ON_TOP| wxMAXIMIZE_BOX);
        //end   : hurukun : 19/11/2009
    ~OutputFileListDlg();

    void SetOutputFileDir(wxString fileDir, wxString projName);

private:
        DECLARE_EVENT_TABLE()
        void CreateControls(void);
        void OnDialogOk(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

        void OnDirBtn(wxCommandEvent& event);
        void OnGvListDClick(wxCommandEvent& event);
        void OnGvListClick(wxCommandEvent& event);
        void OnTextListDClick(wxCommandEvent& event);
        void OnTextListClick(wxCommandEvent& event);
        void OnTextCtrlProject(wxCommandEvent& event);
        void OnShowButton(wxCommandEvent& event);
        void DoShowOrbit(void);
        void DoShowText(void);
    void HideDialog(int returnCode);

    void FilterFiles(void);
        wxString GetOutputDir(void);

private:
        enum {
                CTRL_ID_LIST_GV_FILE = 105,
                CTRL_ID_LIST_TEXT_FILE,
                CTRL_ID_BTN_SHOW,
                CTRL_ID_TC_DIR,
                CTRL_ID_TC_PROJECT,
                CTRL_ID_BTN_DIR,
                CTRL_ID_TC_GV_PARA
        };
        wxListBox* lbGvFileList;
        wxListBox* lbTextFileList;
        wxTextCtrl* tcDir;
        wxTextCtrl* tcProject;
        wxTextCtrl* tcGvPara;
        bool isGv;
};

#endif
