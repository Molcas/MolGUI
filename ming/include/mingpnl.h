/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_MING_PNL_H__
#define __MOLCAS_JOB_MOL_UI_MING_PNL_H__
#include "genericdlg.h"
#include <wx/tglbtn.h>
#include <wx/help.h>
#include <wx/html/helpctrl.h>

#include "datatype.h"
#include "flowchartcanvas.h"
#include "xmlparser.h"
#include "modelconst.h"


class MingPnl : public GenericPnl {
public:
        bool SaveJobInfo();
        void LoadSelectHost(void);
        void SaveSelectHost(void);
        MingPnl(wxWindow* parent, wxWindowID id = wxID_ANY,wxString title=wxEmptyString,bool isNewjob=true);
        virtual ~MingPnl();

        static MingPnl* Get(void);
        static void Set(MingPnl* pPnl);
        static void Free(void);

        virtual long GetButtonStyle(void);
        virtual bool OnButton(long style);

        void SetFrame(wxFrame* frame);


        bool IsShowAdvanced(void) const;
        bool IsShowEmpty(void) const;
        bool IsUseAbsolutePath(void) const;

        void InitPanel(wxPanel* parent, int count, XmlElement** data, WindowPtrArray* winArray, int ctrlIdStart);
        void UnselectOtherToggleButtons(int btnId, WindowPtrArray* winArray) ;
        void InitTemplate(void);

        void DoFileOpen(wxString openFileName, bool isNotUpdateTitle = false);
        void DoFileClose(void);

        bool DoSaveInputFile(wxString localFileName, bool isNotUpdateTitle = false);
        void DoEnvSettings(void);

        void UpdateFrameTitle(const wxString& fileName);
        wxString GetFileName(void) const;
        wxString GetFilePath(void);
        bool SaveFileWhenRelativePath(void);

        void DoSaveHistoryFile(void);
        void ClearHistoryDir(void);

        bool OnBtnClose(void);
//begin : hurukun : 23/11/2009
        void SetActiveDlg(wxDialog * pdlg){m_pActiveDlg=pdlg;}
        wxDialog* GetActiveDlg(){return m_pActiveDlg;}
//end   : hurukun : 23/11/2009
void CreateControls(void);
protected:

private:
        DECLARE_EVENT_TABLE();
        //

        bool OnBtnSubmit(void);
        bool OnBtnReset(void);
        bool OnBtnSave(void);
        //
        void OnAddComponent(wxCommandEvent& event);
        /**
         * Add a component to flowchart
         * @param compType: the component type to be added
         */
        void DoAddFlowChartComponent(MolCompType compType);
        int DoAddTemplate(int elemId);
        void OnDuplicateComponent(wxCommandEvent& event);
        void OnDeleteComponent(wxCommandEvent& event);
        void OnClearComponent(wxCommandEvent& event);
        void DoClearComponent(bool saveHis);

        void OnToggleButtons(wxCommandEvent& event);
        void OnCheckbox(wxCommandEvent& event);
        void OnLbTempateClicked(wxCommandEvent& event);
        void OnLbTemplateDClicked(wxCommandEvent& event);

        void OnUndo(wxCommandEvent& event);
        void OnRedo(wxCommandEvent& event);
        void DoLoadPrevHistoryFile(void);
        void DoLoadNextHistoryFile(void);
        wxString GetHistoryFileName(int hisFileIndex);

        void OnPreviewScript(wxCommandEvent& event);
        void OnEditScript(wxCommandEvent& event);
//        void OnSettingEnv(wxCommandEvent& event);

        void OnAbout(wxCommandEvent& event);
        //GUI Create
//        void CreateControls(void);
        void PostCreate(void);

        wxString GetAvailableXYZFileName(wxString& openFileName);
        bool SaveFileWithOption(bool isOverwrite);;
        void OnNewHost(wxCommandEvent& event);
        void RefreshHostList(void);
    void ReplaceScript(wxString scriptfile);
private:
        WindowPtrArray m_commandBtns;
        WindowPtrArray m_programBtns;
        int m_intSelectedComponent;
        MolCompType m_selectedCompType;
        wxLongLong m_lastClickedTime;
        FlowChartCanvas* m_pFlowCanvas;
        FlowChartModel* m_flowChartModel;
        wxListBox* m_pLbxTemplate;

        wxDateTime m_currFileDateTime;
        int m_hisFileCount;

        bool m_isShowAdvanced;
        bool m_isShowEmpty;
        bool m_isUseAbsolutePath;
        wxString m_fileName;
        wxFrame* m_frame;
        bool m_isDirty;
//begin : hurukun : 23/11/2009
        wxDialog * m_pActiveDlg;
//end   : hurukun : 23/11/2009
        wxComboBox *m_combHostname;
//        wxCheckBox *m_cbxRemoteSubmit;
        wxArrayString m_hostarray;
        wxString        m_title;
        bool m_isNewJob;
};


class AboutDlg : public wxDialog {
        public:
                AboutDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("About..."),
                                  const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
                                  long style = wxDEFAULT_DIALOG_STYLE| wxSTAY_ON_TOP|wxCLOSE_BOX);
                ~AboutDlg();

        private:
                DECLARE_EVENT_TABLE();
                void OnDialogClose(wxCommandEvent& event);
            void OnClose(wxCloseEvent& event);;
                void CreateControls(void);
                void HideDialog(void);
};

#endif
