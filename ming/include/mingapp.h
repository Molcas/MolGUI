/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_MING_APP_H__
#define __MOLCAS_JOB_MOL_UI_MING_APP_H__

#include "wx.h"

/**
 * The start point of the mol job generator as an independent application
 */
class MingApp : public wxApp {
        public:
                MingApp() {}
                virtual ~MingApp();
                virtual bool OnInit();
                virtual int OnExit();
        private:
                bool StartMing(wxString progName, wxString inputFileName, wxString xyzFileName, wxString constraintFileName);
};

DECLARE_APP(MingApp);

#endif
