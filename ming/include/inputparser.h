/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_INPUT_PARSER_H__
#define __MOLCAS_JOB_MOL_MODEL_INPUT_PARSER_H__

#include <stdio.h>
#include <stdlib.h>

#include "xmlparser.h"
#include "elementproperty.h"
#include "flowchartmodel.h"
#include "keyworddata.h"

/**
 * An molcas input file parser.
 */
class InputParser {
    public:
        InputParser();
        ~InputParser();

        static bool IsEndOfInput(char* buf);
        static bool IsCommentBegin(char* buf);
        static bool IsCommentEnd(char* buf, bool trimComment = false);
        static bool IsCommentLine(char* buf);
                static bool IsCommentLineWithModuleStart(char* buf);
        static bool IsMultiLineComment(char* buf);
        static bool IsModuleStart(char* nonBlankBuf);
        static bool IsCommandStart(char* nonBlankBuf);
        static char* RemoveCommandPrefix(char* buf);

        static bool SaveAsGlobalEnvModule(ModuleProperty* envModule, const char* fileName);
        static ModuleProperty* CreateEnvModule(const char* fileName) ;

        FlowChartModel* Parsing(const char* fileName, const char* startLine, const char* availableXYZFile);
        FlowChartModel* Parsing(KeywordData* keywordData, FILE* fpInput, const char* availableXYZFile);

    private:

};
#endif
