/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_MODULE_PROPERTY_H__
#define __MOLCAS_JOB_MOL_MODEL_MODULE_PROPERTY_H__

#include "elementproperty.h"

/**
 * A data model for a module.
 */
class ModuleProperty : public ElementProperty {
    public:
        ModuleProperty(XmlElement* elem);
        ModuleProperty(XmlElement* elem, KeywordModel* parentKeywordModel, ElemPropType elemType);
        ~ModuleProperty();
        /**
         * Get the type of elementproperty.
         * It should be ELEM_PROP_TYPE_MODULE or ELEM_PROP_TYPE_SELECT.
         */
                virtual ElemPropType GetPropType(void) const;
                void SetPropType(ElemPropType type);

                /**
                 * Get a keyword model with the specified index in this module.
                 * @param index: the sequence number in this module
                 */
        KeywordModel* GetKeywordModel(int index) const;
        /**
         * Get a keywword model with the specified keyword name.
         * @param name: the value of the keyword attribute NAME.
         * @param keywordModelSelect: the find keywordmodel is a subkeyword of selection
         */
        KeywordModel* GetKeywordModel(const char* name, KeywordModel** keywordModelSelect = NULL) const;
                /**
                 * When the given keywordmodel has been selected or not,
                 * this function will search all its exclisive keywords
                 * and those keywords who require this keyword, update their states accordingly.
                 */
        void UpdateSelection(const KeywordModel* updatedKeywordModel);
        void UpdateRelation(void);
        /**
         * Whether show subelements or not.
         * It is used when the type of moduleproperty is SELECT.
         * If all subelements of the SELECT is SINGLE, i.e. Checkbox,
         * no need to show subments anymore as the SELECT is enough to show all information.
         * Otherwise, the subelement may need user input, so show these subelements.
         */
        bool IsNeedShowSubelements(void) const ;
        bool IsCommentModule(void) const;

                ModuleProperty* GetTopModuleProperty(void) const;
        virtual bool SaveAsInputFormat(FILE* fp);
        virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile);

        virtual void DuplicateValue(ElementProperty* const oldElemProp);

        virtual bool IsValid(void);
        virtual void ClearData(void);

        void Init(void);

        bool IsEnvModule(void) const;
        char* GetProjectName(void) const;
        char* GetOutputDir(void) const;

        protected:
                KeywordModel* InsertKeywordModelAfter(const char* preKeywordName);

    private:
                /* the type of the module property */
                ElemPropType propType;
};

#endif
