/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_BASIS_SET_DLG_H__
#define __MOLCAS_JOB_MOL_UI_BASIS_SET_DLG_H__

#include "wx.h"

/**
 * Display Basis Set for Elements in an XYZ file.
 */
class BasisSetDlg : public wxDialog {
    DECLARE_EVENT_TABLE()
public:
        //begin : hurukun : 19/11/2009
    BasisSetDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Basis Set Editor"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
//            long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX);
                        long style = wxDEFAULT_DIALOG_STYLE);
        //end   : hurukun : 19/11/2009
    ~BasisSetDlg();

    void HideDialog(int returnCode);
    /**
     * The element list and the number of elements.
     */
    void SetElemSymbols(char** eleSymbols, int count);
    /**
     * Return the selected basis set.
     */
    wxArrayString GetSelectedBasisSet(void);
    /**
     * If there are some selected basis set, use this function to set them in the selected basis set list.
     */
    void SetSelectedBasisSet(const wxArrayString& basisSets);

private:
    void CreateControls(void);
    void OnDialogOK(wxCommandEvent& event);
    void OnDialogCancel(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

    void OnLbElementClicked(wxCommandEvent& event);
        void DoLbElementClicked(void);
    void OnLbSelectedClicked(wxCommandEvent& event);
    void OnListBoxClicked(wxCommandEvent& event);
    void OnListBoxDClicked(wxCommandEvent& event);
    void OnLbSelectedDClicked(wxCommandEvent& event);

    void OnAddBasisSet(wxCommandEvent& event);
    void OnDelBasisSet(wxCommandEvent& event);
    void OnCustomBasisSet(wxCommandEvent& event);

    /**
     * decide an element's basis set has been selected or not
     * @param exceptIndex: do not include the string with the given index
     */
    //bool IsSelected(wxString eleSymbol);
    bool IsSelected(wxString selBasisSet, int exceptIndex = -1);
    /**
     * The given two basis sets start with the same element symbol.
     */
    bool IsStartWithSameElement(wxString ele1Sym, wxString ele2Sym);
    /**
     * Is all elements have a corresponding basis set.
     * @param notSelectedElemIndex: If not, this para gives the index of elements in listbox of elements.
     */
    bool IsValidSelection(int* notSelectedElemIndex);

private:
    wxListBox* pLbElements;
    wxListBox* pLbRecommended;
    wxListBox* pLbOther;
    wxListBox* pLbSelected;
    wxTextCtrl* pTcModify;
    int selectedList;

    char** eleSymbols;
    int eleCount;
    int* eleSeletced;       // to decide an element's basis set has been selected or not

    enum {
        CTRL_ID_LB_ELEMENTS = 1000,
        CTRL_ID_LB_RECOMMENDED,
        CTRL_ID_LB_OTHER,
        CTRL_ID_LB_SELECTED,
        CTRL_ID_BTN_ADD,
        CTRL_ID_BTN_DEL,
        CTRL_ID_BTN_CUSTOM_ADD,
        CTRL_ID_BTN_CUSTOM_MOD,
        CTRL_ID_TC_MODIFY,
    };
};

#endif
