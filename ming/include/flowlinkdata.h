/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_FLOW_LINK_DATA_H__
#define __MOLCAS_JOB_MOL_MODEL_FLOW_LINK_DATA_H__

#include "commandproperty.h"

#define MAX_FLOW_LINK_LEVEL 20
class FlowChartModel;

class FlowLinkData {
        public:
                FlowLinkData(FlowChartModel* chartModel);
                ~FlowLinkData();
        /**
         * used for drawing name links
         */
        int GetMaxNameLinkIndex(void) const;
        /**
         * refresh link index in the given flowchartmodel
         */
        void CalcLinkIndex(void);
        protected:
        void Init(void);
        int AllocNameLinkIndex(const char* targetName);
        int DeallocNameLinkIndex(const char* targetName);
                int AllocValueLinkIndex(void);
                void DeallocValueLinkIndex(int index);
        /**
         *
         * @return the
         */
        int FindValueLinkIndex(CommandProperty* endCmdProp, bool isAnchor, const char* name, const char* value);

        private:
        FlowChartModel* flowChartModel;
                char* nameLinkTarget[MAX_FLOW_LINK_LEVEL];
                int nameLinkIndex;
                int maxNameLinkIndex;
                int valueLinkUnusedIndex[MAX_FLOW_LINK_LEVEL];
                int valueLinkIndex;
};

#endif
