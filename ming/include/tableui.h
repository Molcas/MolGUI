/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_TABLE_UI_H__
#define __MOLCAS_JOB_MOL_UI_TABLE_UI_H__

#include <wx/grid.h>

#include "keywordmodel.h"
#include "tablemodel.h"
#include "keywordui.h"
#include "textctrlui.h"

/**
 * Show table-like data.
 */
class TableUI : public KeywordUI {
private:
        DECLARE_EVENT_TABLE();
    void OnSize(wxSizeEvent& event);
    void OnCellValueChanged(wxGridEvent& event);
    void OnText(wxCommandEvent& event);
    void SetRowSize(void);
    void SetColumnSize(void);

protected:
    void CreateUIControls(void);
    void UpdateRowCount(int rowNumber);

public:
        TableUI(wxWindow *parent, KeywordModel* keywordModel);
    virtual void UpdateFromModel(void);
    virtual void UpdateToModel(bool isRestore);
    virtual bool HasValue(void) const;

        wxGrid* GetTable(void) const;
    TableModel* GetTableModel(void) const;
    virtual void OnClearData(void);

private:
    wxTextCtrl* textCtrl;
};

class GridCellFormatEditor : public wxGridCellTextEditor {
public:
        GridCellFormatEditor(FormatTextCtrlType type, const char* charMinValue, const char* charMaxValue);
        GridCellFormatEditor(FormatTextCtrlType type, double minValue, double maxValue);
    virtual void Create(wxWindow* parent, wxWindowID id, wxEvtHandler* evtHandler);
        virtual wxGridCellEditor *Clone() const;
        double GetMinValue() { return m_minValue; }
        double GetMaxValue() { return m_maxValue; }

private:
        FormatTextCtrlType m_formatType;
        double m_minValue;
        double m_maxValue;
};


#endif
