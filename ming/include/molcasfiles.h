/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_PRJ_MOLCAS_FILES_H__
#define __MOLCAS_JOB_MOL_PRJ_MOLCAS_FILES_H__

#include "wx.h"
//#include "dlgoutput.ch"
#include "projectfiles.h"
#include "projectgroup.h"

extern int POPUPMENU_PRJRES_ID_TEXT ;
extern int POPUPMENU_PRJRES_ID_GV ;

#define                OUT_COMMON_FILE     0
#define                OUT_ORB_FILE        1
#define                OUT_MOLDEN_FILE     2
struct MolcasOutputInfo {
    wxString            fileSuffix;
    wxString            title;
    ProjectTreeItemType itemType;
    ProjectTreeItemType ofItemType;
    unsigned int        fileType;//        0:normal          1: Orb                2:molden
};

/// Hash map to temporary store file names by type.
WX_DECLARE_STRING_HASH_MAP(wxArrayString, wxArrayStringHashMap);

class MolcasFiles : public AbstractProjectFiles {
public:
    bool HasSuccess();
    void GetXYZ();
    bool GetSummary();
    bool ConvertVib();
    virtual bool HasOutput();
    virtual bool HasEnergy_Statistics();
    virtual bool HasError_Info();
    virtual int  HasDensity();
    virtual bool HasOptimization();
    virtual bool HasFrequence();
    virtual bool HasInput();
    virtual bool HasScfOrb();
    virtual bool HasGssOrb();
    virtual bool HasDump();


    wxString GetOutputFileName();
    MolcasFiles(SimuProject *simuProject, wxString resultDir);
    ~MolcasFiles();

    virtual bool CreateInputFile(void);
    virtual wxString GetExecCommand(void);
    virtual wxString GetExecPara(void);
    //            virtual wxString GetExecLogName(void);
    virtual bool PostJobTerminated(bool isFinal);
    virtual void BuildResultTree(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode);
    virtual void ShowResultItem(ProjectTreeItemData *item);
    virtual wxString GetInputFileName(void);
    virtual bool        BuildOuterFileTree(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode);
    virtual wxString GetSuffix(ProjectTreeItemType itemType);
public:
    virtual wxString        GetOutFileName(wxString &fileSuffix);
private:
    void        Display(wxString &outfile, wxString &title);
    void        AddFileTypeToTree(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode, wxString &type, wxArrayString &files);
    void        BuildItems(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode);
    wxTreeItemId        GetOuterFileNode(wxTreeCtrl *pTree, wxTreeItemId &jobResultNode);
    bool        ModifySummaryFile();
};

class DlgGridSelect: public wxDialog {
    DECLARE_EVENT_TABLE()
public:
    DlgGridSelect(wxWindow *parent, wxWindowID id = wxID_ANY, const wxString &title = wxT("Grid Select"),
                  const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxSize(300, 100),
                  //begin : hurukun : 20/11/2009
                  //         long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER );
                  long style = wxDEFAULT_DIALOG_STYLE | wxSTAY_ON_TOP );
    //end   : hurukun : 20/11/2009
    virtual ~DlgGridSelect();
private:
    wxStaticText *pstMsg;
private:
    void OnAlpha(wxCommandEvent &event);
    void OnBeta(wxCommandEvent &event);
    void OnClose(wxCloseEvent &event);
    void CreateGUIControls(void);
    enum {
        btnID_ALPHA = 1,
        btnID_BETA,
    };

};

#endif
