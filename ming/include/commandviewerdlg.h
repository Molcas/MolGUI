/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_COMMAND_VIEWER_DLG_H__
#define __MOLCAS_JOB_MOL_UI_COMMAND_VIEWER_DLG_H__

#include "commandproperty.h"
#include "wx.h"
#include "datatype.h"

/**
 * For display the property of a command
 */
class CommandViewerDlg : public wxDialog {

private:
    DECLARE_EVENT_TABLE()

public:
        //begin : hurukun : 19/11/2009
    CommandViewerDlg(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Command Properties Setting"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
 //           long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER | wxMAXIMIZE_BOX);
             long style = wxDEFAULT_DIALOG_STYLE);
        //end   : hurukun : 19/11/2009
    ~CommandViewerDlg();

        void SetCommandProperty(CommandProperty* prop);
    void HideDialog(int returnCode);

    wxString GetCtrlValue(int ctrlIndex);
    void SetCtrlValue(int ctrlIndex, const char* value);

private:
    void CreateControls(void);
    void OnButton(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);

    wxSizer* CreateInputHorizontalSizer(XmlElement* elem);

    void UpdateFromModel(void);
    void UpdateToModel(bool isRestore);

private:
    CommandProperty* commandProperty;
    WindowPtrArray inputCtrls;
    int currInputCtrlID;
};

#endif
