/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_GROUP_BUTTON_UI_H__
#define __MOLCAS_JOB_MOL_UI_GROUP_BUTTON_UI_H__

#include "groupmodel.h"
#include "keywordmodel.h"
#include "keywordui.h"

class GroupButtonUI : public KeywordUI {
private:
        DECLARE_EVENT_TABLE();
    void OnButton(wxCommandEvent& event);

protected:
    void CreateUIControls(void);
    void SetHiddenColor(void) ;

public:
        GroupButtonUI(wxWindow *parent, KeywordModel* keywordModel);
        ~GroupButtonUI();

    wxButton* GetDetailButton(void);
    GroupModel* GetGroupModel(void);
    virtual void UpdateFromModel(void);
    virtual void UpdateToModel(bool isRestore);
    virtual bool HasValue(void) const;

private:
#ifdef __MAC__
        wxStaticText* m_labelHidden;
#endif
};

#endif
