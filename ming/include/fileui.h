/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_UI_FILE_UI_H__
#define __MOLCAS_JOB_MOL_UI_FILE_UI_H__


#include "filemodel.h"
#include "keywordmodel.h"
#include "keywordui.h"
#include <wx/filename.h>
//#include "renderingdata.h"  commend by chaochao

class FileUI : public KeywordUI {
        private:
                DECLARE_EVENT_TABLE();
                void OnFileButton(wxCommandEvent& event);
                void OnEditButton(wxCommandEvent& event);
                void OnViewButton(wxCommandEvent& event);
                void OnText(wxCommandEvent& event);

        protected:
                void CreateUIControls(void);
                wxDateTime GetLastModificationTime(wxFileName fileName);

        public:
                FileUI(wxWindow *parent, KeywordModel* keywordModel);
                ~FileUI();

                wxTextCtrl* GetFileInputCtrl(void) const;
                FileModel* GetFileModel(void) const;
                virtual void UpdateFromModel(void);
                virtual void UpdateToModel(bool isRestore);
                virtual bool HasValue(void) const;
             #if defined (__LINUX__)    //added by chaochao molcasrc
                wxString Getdefaultvalue_file(void);//get the default vaule of environment variable (license) in molcasrc
             #endif
        private:
                char* origValue;
};

#endif
