/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __MOLCAS_JOB_MOL_MODEL_ELEMENT_PROPERTY_H__
#define __MOLCAS_JOB_MOL_MODEL_ELEMENT_PROPERTY_H__

#include <stdio.h>

#include "modelconst.h"
#include "xmlparser.h"

class KeywordModel;

/**
 * Commands and Programs (Modules) will be modelled as element property.
 * As the structure of Select subelement of a program is the same as a program,
 * therefore, it is also approriate for the container for the subelements of Select.
 * ElementProperty is a container for XmlElement. In addition, it contains corresponding values.
 */
class ElementProperty {
    public:
                /**
                 * Construct with xml data.
                 */
        ElementProperty(XmlElement* elem);
        ElementProperty(XmlElement* elem, KeywordModel* parentKeywordModel);
        virtual ~ElementProperty();

                /**
                 * The name of the element.
                 */
        char* GetName(void) const;
        /**
         * if it is null, return GetName()
         */
        char* GetAppear(void) const;
        /**
         * The type of the element.
         */
        virtual ElemPropType GetPropType(void) const = 0;
        /**
         * The number of the subelements contained in this element.
         */
        int GetSubElementCount() const;
        XmlElement* GetSubElement(int index) const;
        XmlElement* GetXmlElement(void) const;
                /**
                 * Element Property could be linked as a list used by flowchart.
                 */
        ElementProperty* GetNextElement(void) const;
        /**
         * Set the next element property in a flowchart.
         */
                void SetNextElement(ElementProperty* elem);
                /**
                 * Add an element property in the list.
                 */
                int AddElement(ElementProperty* prop, int elemId);

                virtual bool SaveAsInputFormat(FILE* fp) = 0;
                virtual void ReadFromInputFormat(FILE* fp, char* buf, int bufSize, const char* availableXYZFile) = 0;
                virtual void ClearData(void) = 0;
                /**
                 * All required input values have been inputed.
                 */
                virtual bool IsValid(void) = 0;
                bool IsUserChecked(void) const;
                void SetUserChecked(bool modified);
                /**
                 * Duplicate an instance with all values.
                 */
                virtual void DuplicateValue(ElementProperty* const oldElemProp) = 0;
                /**
                 * A creation factory for ElementProperty with a given xml data.
                 */
                static ElementProperty* CreateElementProperty(XmlElement* elem);

        virtual void SetPropValue(const char* newValue);
        char* GetPropValue(void) const;

        int GetInternalId(void) const;
                int GetLinkIndex(void) const;
                void SetLinkIndex(int index);
        KeywordModel* GetParentKeywordModel(void) const;

        ElementProperty* GetLinkElemProp(void) const;
        void SetLinkElemProp(ElementProperty* elemProp);

    protected:
                /* next element property in element property list. */
                ElementProperty* nextElemProp;
                /* the original xml structure. */
        XmlElement* xmlElement;
        /* the number of subelements in this xmlelement. */
        int subElemCount;
        /* now it is only used by commandproperty.*/
        XmlElement** subXmlElements;
                /* pointer to the array of keymodel*.
                 * earch subelements will be modelled as a keywordmodel.
                 * this field is used for ModuleProperty.
                 * Why put here? As when converting ModuleProperty* to ElementProperty*,
                 * and then back to ModuleProperty*,
                 * there are memory errors in Linux.
                 */
                KeywordModel** keywordModels;
                /* show subelements or not.
                 * used for Select.
                 */
                bool needShowSubelements;
                KeywordModel* parentKeywordModel;
                ElementProperty* linkElemProp;

        private:
        void Init(XmlElement* elem);

        private:
        int internalId;
                int linkIndex;
        char* propValue;
        bool isUserChecked;
};

#endif
