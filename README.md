MolGUI
======

MolGUI is a graphical interface for preparing and managing Molcas calculations.
Although it has been available for a long time, it has been mostly unmaintained
and not regularly tested (developers often prefer using text interfaces).

After the release of OpenMolcas, MolGUI is now also released under the Lesser
General Public License (LGPL). It can be used together with OpenMolcas, but
users should keep in mind that it was designed for Molcas and hasn't been
completely updated.

Compilation
-----------

MolGUI is configured with [CMake](https://cmake.org). You should be able to
compile with these simple instructions:

1.  Clone the repository:

    ```
    git clone https://gitlab.com/Molcas/MolGUI.git
    ```

2.  Create a new directory and run `cmake` from it, passing the `-D MOLCAS=...`
    option with the path of a Molcas or OpenMolcas (after `deed160`,
    2018-09-14) build:

    ```
    mkdir build
    cd build
    cmake -D MOLCAS=/path/to/(Open)Molcas/build/ ../MolGUI
    ```

3.  Compile with `make`:

    ```
    make
    ```
