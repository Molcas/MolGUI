/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/grid.h>
#include <wx/string.h>
#include <wx/menu.h>
#include <wx/toolbar.h>

#include "envutil.h"
#include "resource.h"

#include "atomlistfrm.h"

#include "stringutil.h"
#include "tools.h"

#include "commons.h"
#include "zmatrix.h"
#include "measure.h"
#include "measurepnl.h"
#include "auisharedwidgets.h"

#define COL_COUNT_DEFAULT 3
#define COL_COUNT_ZMATRIX 6
#define COL_COUNT_CARTESIAN 3
#define COL_COUNT_LAYER 1

#define COL_COUNT (COL_COUNT_DEFAULT + COL_COUNT_ZMATRIX + COL_COUNT_CARTESIAN + COL_COUNT_LAYER)

enum {
        MENU_ID_EDIT_ZMATRIX,
        MENU_ID_EDIT_RECONNECT,

        MENU_ID_COL_ZMATRIX,
        MENU_ID_COL_CARTESIAN_COOR,
        MENU_ID_COL_LAYER,

        TOOL_ID_COL_ZMATRIX,
        TOOL_ID_COL_CARTESIAN_COOR,
        TOOL_ID_COL_LAYER,

        TOOL_ID_REFRESH
};

static const wxChar *colHeaders[] = {
        wxT("HighLight"),
        wxT("#"),
        wxT("Symbol"),
        //Z-Matrix
         wxT("NA"),
         wxT("NB"),
         wxT("NC"),
         wxT("Bond"),
         wxT("Angle"),
         wxT("Dihedral"),
         //Cartesian Coords
         wxT("X"),
         wxT("Y"),
         wxT("Z"),
         //Layer
         wxT("Layer")
};

enum {
        COL_ID_HIGHLIGHT = 0,
        COL_ID_SEQNO,
        COL_ID_SYMBOL,
        COL_ID_NA,
        COL_ID_NB,
        COL_ID_NC,
        COL_ID_BOND,
        COL_ID_ANGLE,
        COL_ID_DIHEDRAL,
        COL_ID_X,
        COL_ID_Y,
        COL_ID_Z,
        COL_ID_LAYER
};

static const wxString aryStrLayer[] =
{
    wxT("High"),
    wxT("Medium"),
    wxT("Low")
};

////Event Table Start
BEGIN_EVENT_TABLE(AtomListFrm, wxFrame)
        ////Manual Code Start
        EVT_MENU(wxID_EXIT, AtomListFrm::OnQuit)
        EVT_CLOSE(AtomListFrm::OnClose)
        //EVT_MENU(MENU_ID_EDIT_RECONNECT, AtomListFrm::OnEditReconnect)

        EVT_MENU_RANGE(MENU_ID_COL_ZMATRIX, MENU_ID_COL_LAYER, AtomListFrm::OnMenuShowCols)
        EVT_TOOL_RANGE(TOOL_ID_COL_ZMATRIX, TOOL_ID_COL_LAYER, AtomListFrm::OnToolShowCols)
        EVT_TOOL(TOOL_ID_REFRESH, AtomListFrm::OnRefresh)
        EVT_GRID_RANGE_SELECT(AtomListFrm::OnRangeSelected)
        ////Manual Code End
END_EVENT_TABLE()
////Event Table End

AtomListFrm::AtomListFrm(wxFrame *parent, wxWindowID id, const wxString &title, const wxPoint& pos, const wxSize& size, long style)
                : wxFrame(parent, id, title, pos, size, wxMINIMIZE_BOX|wxMAXIMIZE_BOX|wxCLOSE_BOX|wxFRAME_FLOAT_ON_PARENT)
{
//        wxFrame::Create(parent, id, title, pos, size, style|wxSTAY_ON_TOP|wxFRAME_FLOAT_ON_PARENT) ;
        m_grid = NULL;
        m_atomGridTable = NULL;
        m_pCtrlInst = NULL;

        CreateMenu();
        InitToolBar();
        wxBitmap bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK+ wxT("app.png"), wxBITMAP_TYPE_PNG);
        wxIcon frameIcon;
        frameIcon.CopyFromBitmap(bmp);
        SetIcon(frameIcon);
        SetSize(800, 400);
}

AtomListFrm::~AtomListFrm() {
        //delete m_atomGridTable;

}

void AtomListFrm::OnQuit(wxCommandEvent& WXUNUSED(event)) {
        Close(true);
}

//------------added by xia---------------------
void AtomListFrm::OnClose(wxCloseEvent& event){
m_pCtrlInst->FreeAtomListFrm();
}
//----------------------------------------

void AtomListFrm::CreateMenu(void) {
        //create the menu bar
        wxMenu *fileMenu = new wxMenu;
        fileMenu->Append(wxID_EXIT, wxT("E&xit"));

//        wxMenu *editMenu = new wxMenu;
        //wxMenu *editZMatrixMenu = new wxMenu;
        //editZMatrixMenu->Append(MENU_ID_EDIT_RECONNECT, wxT("Reconnect"));
        //editMenu->Append(MENU_ID_EDIT_ZMATRIX, wxT("Z-Matrix"), editZMatrixMenu);

//        wxMenu *rowMenu = new wxMenu;

        wxMenu *colMenu = new wxMenu;
        colMenu->AppendCheckItem(MENU_ID_COL_ZMATRIX, wxT("Z-Matrix"));
        colMenu->AppendCheckItem(MENU_ID_COL_CARTESIAN_COOR, wxT("Cartesian Coordinates"));
        colMenu->AppendCheckItem(MENU_ID_COL_LAYER, wxT("Layer"));

        //Load the menu bar
        wxMenuBar *menuBar = new wxMenuBar;
        menuBar->Append(fileMenu, wxT("&File"));
//begin : hurukun : 30/11/2009
//        menuBar->Append(editMenu, wxT("&Edit"));
//        menuBar->Append(rowMenu, wxT("&Rows"));
//edn   : hurukun : 30/11/2009
        menuBar->Append(colMenu, wxT("&Columns"));

        SetMenuBar(menuBar);
}

void AtomListFrm::InitToolBar(void) {
        wxBitmap bmp;

        CreateToolBar(wxNO_BORDER | wxTB_FLAT | wxTB_HORIZONTAL);

        bmp = wxBitmap(CheckImageFile(wxT("alzmatrix.png")), wxBITMAP_TYPE_PNG);
        GetToolBar()->AddTool(TOOL_ID_COL_ZMATRIX, wxT("Z"), bmp, wxT("Show Z-Matrix Columns"), wxITEM_CHECK);
        bmp = wxBitmap(CheckImageFile(wxT("alcartesian.png")), wxBITMAP_TYPE_PNG);
        GetToolBar()->AddTool(TOOL_ID_COL_CARTESIAN_COOR , wxT("C"), bmp, wxT("Show Cartesian Coordinates Columns"), wxITEM_CHECK);
        bmp = wxBitmap(CheckImageFile(wxT("allayer.png")), wxBITMAP_TYPE_PNG);
        GetToolBar()->AddTool(TOOL_ID_COL_LAYER, wxT("L"), bmp, wxT("Show Layer Columns"), wxITEM_CHECK);

        GetToolBar()->AddSeparator();
        bmp = wxBitmap(CheckImageFile(wxT("refresh.png")), wxBITMAP_TYPE_PNG);
        GetToolBar()->AddTool(TOOL_ID_REFRESH, wxT("Refresh"), bmp, wxT("Refresh"));

        GetToolBar()->Realize();

         GetMenuBar()->Check(MENU_ID_COL_ZMATRIX, true);
         GetMenuBar()->Check(MENU_ID_COL_CARTESIAN_COOR, true);
         GetMenuBar()->Check(MENU_ID_COL_LAYER, false);
          GetToolBar()->ToggleTool(TOOL_ID_COL_ZMATRIX, true);
          GetToolBar()->ToggleTool(TOOL_ID_COL_CARTESIAN_COOR, true);
          GetToolBar()->ToggleTool(TOOL_ID_COL_LAYER, false);
}

void AtomListFrm::InitTable(MolView* pMolView, CtrlInst* pCtrlInst) {
        m_pMolView = pMolView;
        m_pCtrlInst = pCtrlInst;

        wxBoxSizer* pBsMain = new wxBoxSizer(wxVERTICAL);
        this->SetSizer(pBsMain);

        m_grid = new wxGrid(this, wxID_ANY);
        pBsMain->Add(m_grid, 1, wxEXPAND |wxALL, 5);

        m_grid->SetColMinimalAcceptableWidth(0);
        m_grid->EnableDragColSize(false);
        m_atomGridTable = new AtomGridTable(m_pMolView, pCtrlInst);
    m_atomGridTable->SetAttrProvider(new RowColorGridCellAttrProvider);
    m_grid->SetTable(m_atomGridTable, false);
    m_grid->SetSelectionMode(wxGrid::wxGridSelectRows);
    //m_grid->SetRowLabelSize(0);
    SetColAttrs();
    UpdateColSizes();

        /*
        //Set Cell Alignment Center
        for (i = 0; i < m_atom_num; i++)
                for (j = 0; j < 12; j++)
                        m_grid->SetCellAlignment(i, j, wxALIGN_CENTRE, wxALIGN_CENTRE);

        //Set Color( one row white one row blue)
        wxGridCellAttr *pattr;
        pattr = new wxGridCellAttr;
        m_color = new wxColor(200, 250, 230);
        pattr->SetBackgroundColour(*m_color);
        for (i = 0; i < m_atom_num; i++)
                m_grid->SetRowAttr(2*i, pattr);

        //set precision
        m_grid->SetColFormatFloat(6);
        m_grid->SetColFormatFloat(7, 8, 3);
        m_grid->SetColFormatFloat(8, 8, 3);
        for (i = 9; i < 12; i++) {
                m_grid->SetColFormatFloat(i);
        }

        //Set the first column button
        *m_color = wxColour(220, 220, 220);
        wxGridCellBitmapRenderer *pc_render = new wxGridCellBitmapRenderer;
        for (i = 0; i < m_atom_num; i++) {
                m_grid->SetCellBackgroundColour(i, 0, *m_color);
                m_grid->SetReadOnly(i, 0, true);
                m_grid->SetCellValue(i, 0, wxT("false"));
                m_grid->SetCellRenderer(i, 0, pc_render);
        }

        */
}

void AtomListFrm::SetColAttrs(void) {
        wxGridCellAttr* attrReadOnly = new wxGridCellAttr;
        attrReadOnly->SetReadOnly();
        attrReadOnly->SetAlignment(wxALIGN_CENTRE, wxALIGN_CENTRE);

        wxGridCellAttr* attrComboElem = new wxGridCellAttr;
        attrComboElem->SetEditor(new wxGridCellChoiceEditor(ELEM_COUNT, ELEM_NAMES));
        attrComboElem->SetAlignment(wxALIGN_CENTRE, wxALIGN_CENTRE);

        wxGridCellAttr* attrComboLayer = new wxGridCellAttr;
        attrComboLayer->SetEditor(new wxGridCellChoiceEditor(WXSIZEOF(aryStrLayer), aryStrLayer));
        attrComboLayer->SetAlignment(wxALIGN_CENTRE, wxALIGN_CENTRE);

        wxGridCellAttr* attrValue = new wxGridCellAttr;
        attrValue->SetAlignment(wxALIGN_RIGHT, wxALIGN_CENTRE);

        m_grid->SetColAttr(COL_ID_SEQNO, attrReadOnly);
        m_grid->SetColAttr(COL_ID_SYMBOL, attrComboElem);
        m_grid->SetColAttr(COL_ID_NA, attrReadOnly);
        m_grid->SetColAttr(COL_ID_NB, attrReadOnly);
        m_grid->SetColAttr(COL_ID_NC, attrReadOnly);

        m_grid->SetColAttr(COL_ID_BOND, attrValue);
        m_grid->SetColAttr(COL_ID_ANGLE, attrValue);
        m_grid->SetColAttr(COL_ID_DIHEDRAL, attrValue);
        m_grid->SetColAttr(COL_ID_X, attrValue);
        m_grid->SetColAttr(COL_ID_Y, attrValue);
        m_grid->SetColAttr(COL_ID_Z, attrValue);

        m_grid->SetColAttr(COL_ID_LAYER, attrComboLayer);
}

void AtomListFrm::UpdateFrameTitle(wxString title) {
    if (StringUtil::IsEmptyString(title)) {
        title = wxT("Atom List Editor");
    } else {
        title += wxT(" - Atom List Editor");
    }
    SetTitle(title);
}

void AtomListFrm::UpdateMenuShowCols(int evnetId, bool isMenu) {
    int menuIds[3] = {  MENU_ID_COL_ZMATRIX,
                        MENU_ID_COL_CARTESIAN_COOR,
                        MENU_ID_COL_LAYER};
    int toolIds[3] = {  TOOL_ID_COL_ZMATRIX,
                        TOOL_ID_COL_CARTESIAN_COOR,
                        TOOL_ID_COL_LAYER};
    int index = 0;

    if (!isMenu) {
        index = evnetId - TOOL_ID_COL_ZMATRIX;
        GetMenuBar()->Check(menuIds[index], GetToolBar()->GetToolState(toolIds[index]));
    } else {
        index = evnetId - MENU_ID_COL_ZMATRIX;
        GetToolBar()->ToggleTool(toolIds[index], GetMenuBar()->IsChecked(menuIds[index]));
    }

        UpdateColSizes();
}

void AtomListFrm::UpdateColSizes(void) {
        int colWidths[] = {0, 0, 50, 50, 50, 50, 80, 80, 80, 80, 80, 80, 60};
        int i = 0, size = 0;
        for(i = 0; i < COL_COUNT; i++) {
                size = colWidths[i];
                if(i < COL_COUNT_DEFAULT) {
                }else if(i < COL_COUNT_DEFAULT + COL_COUNT_ZMATRIX) {
                        if(!GetToolBar()->GetToolState(TOOL_ID_COL_ZMATRIX)) {
                                size = 0;
                        }
                }else if(i < COL_COUNT_DEFAULT + COL_COUNT_ZMATRIX + COL_COUNT_CARTESIAN) {
                        if(!GetToolBar()->GetToolState(TOOL_ID_COL_ZMATRIX + 1)) {
                                size = 0;
                        }
                }else {
                        if(!GetToolBar()->GetToolState(TOOL_ID_COL_ZMATRIX + 2)) {
                                size = 0;
                        }
                }

                m_grid->SetColSize(i, size);
        }

        m_grid->ForceRefresh();
}

void AtomListFrm::OnRefresh(wxCommandEvent& event) {
        m_grid->SetTable(m_atomGridTable, false);
        UpdateColSizes();
}

void AtomListFrm::OnMenuShowCols(wxCommandEvent& event) {
        UpdateMenuShowCols(event.GetId(), true);
}

void AtomListFrm::OnToolShowCols(wxCommandEvent& event) {
        UpdateMenuShowCols(event.GetId(), false);
}


void AtomListFrm::OnRangeSelected(wxGridRangeSelectEvent& event) {
        int i = 0;
        if(m_pCtrlInst) {
                SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
                for(i = event.GetTopRow(); i <= event.GetBottomRow(); i++) {
                        pSelUtil->RemoveManuSelected(i);
                        if(event.Selecting()) {
                                pSelUtil->AddManuSelectedIndex(i, ATOM_SELECT_SINGLE);
                        }
                }
        }

    event.Skip();
    m_pMolView->RefreshGraphics();
}

wxGridCellBitmapRenderer::wxGridCellBitmapRenderer() {

}

wxGridCellBitmapRenderer::~wxGridCellBitmapRenderer() {

}

void wxGridCellBitmapRenderer::Draw(wxGrid &grid, wxGridCellAttr &attr, wxDC &dc, const wxRect &rect, int row, int col, bool isSelected) {
        wxGridCellRenderer::Draw(grid, attr, dc, rect, row, col, isSelected);

        wxBitmap bmp;
        int left, top;
        int width, height;

        bmp = wxBitmap(CheckImageFile(wxT("alcartesian.png")), wxBITMAP_TYPE_PNG);
        width = bmp.GetWidth();
        height = bmp.GetHeight();

        left = rect.x + rect.width / 2 - width / 2;
        top = rect.y + rect.height / 2 - height / 2;

        dc.DrawBitmap(bmp, left, top, true);

}

wxGridCellRenderer * wxGridCellBitmapRenderer::Clone() const {
        return new wxGridCellBitmapRenderer;
}

wxSize wxGridCellBitmapRenderer::GetBestSize(wxGrid &grid, wxGridCellAttr &attr, wxDC &dc, int row, int col) {
        wxBitmap bmp;
        bmp = wxBitmap(CheckImageFile(wxT("alcartesian.png")), wxBITMAP_TYPE_PNG);
        return wxSize(bmp.GetWidth(), bmp.GetHeight());
}


AtomGridTable::AtomGridTable(MolView* pMolView, CtrlInst* pCtrlInst) {
        m_pMolView = pMolView;
        m_pCtrlInst = pCtrlInst;
}

int AtomGridTable::GetNumberRows(){
        int count = 0;
        if(m_pCtrlInst) {
                count = m_pCtrlInst->GetRenderingData()->GetAtomBondArray().GetCount();
        }
        return count;
}

int AtomGridTable::GetNumberCols(){
        return COL_COUNT;
}

bool AtomGridTable::IsEmptyCell(int row, int col){
        if((row <= 0 && (COL_ID_NA == col || COL_ID_BOND == col))
                || (row <= 1 && (COL_ID_NB == col || COL_ID_ANGLE == col))
                || (row <= 2 && (COL_ID_NC == col || COL_ID_DIHEDRAL == col))) {
                return true;
        }
        return false;
}

wxString AtomGridTable::GetValue(int row, int col){
        if((row <= 0 && (COL_ID_NA == col || COL_ID_BOND == col) )
                || (row <= 1 && (COL_ID_NB == col || COL_ID_ANGLE == col) )
                || (row <= 2 && (COL_ID_NC == col || COL_ID_DIHEDRAL == col))
                || m_pCtrlInst == NULL) {
                return wxEmptyString;
        }
        AtomBondArray& atomBondArray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        if(COL_ID_SEQNO == col) {
                return wxString::Format(wxT("%d"), row + 1);
        }else if(COL_ID_SYMBOL == col) {
                int elemId = atomBondArray[row].atom.elemId;
                if (elemId == DEFAULT_HYDROGEN_ID) {
                        elemId = (int)ELEM_H;
                }
                if(elemId==-5)return ELEM_NAMES[ELEM_COUNT];
                return ELEM_NAMES[elemId-1];
        }else if(COL_ID_X == col || COL_ID_Y == col || COL_ID_Z == col) {
                return wxString::Format(wxT("%.3f"), atomBondArray[row].atom.pos[col - COL_ID_X]);
        }else if(COL_ID_LAYER == col) {
                short layer = atomBondArray[row].atom.layer;
                //return aryStrLayer[];
                if(layer == LAYER_HIGH) {
                        return aryStrLayer[0];
                }else if(layer == LAYER_MEDIUM) {
                        return aryStrLayer[1];
                }else {
                        return aryStrLayer[2];
                }
        }else {
                //zmatrix
                ZMatrix internalCoordinateItem;
                FetchZMatrix(atomBondArray, internalCoordinateItem, row);
                switch(col) {
                        case COL_ID_NA:
                                return wxString::Format(wxT("%d"), internalCoordinateItem.bond.atomIndex+1);
                        case COL_ID_NB:
                                return wxString::Format(wxT("%d"), internalCoordinateItem.angle.atomIndex+1);
                        case COL_ID_NC:
                                return wxString::Format(wxT("%d"), internalCoordinateItem.dihedral.atomIndex+1);
                        case COL_ID_BOND:
                                return wxString::Format(wxT("%.3f"), internalCoordinateItem.bond.value);
                        case COL_ID_ANGLE:
                                return wxString::Format(wxT("%.3f"), internalCoordinateItem.angle.value);
                        case COL_ID_DIHEDRAL:
                                return wxString::Format(wxT("%.3f"), internalCoordinateItem.dihedral.value);
                        default:
                                break;
                }
        }
        return wxEmptyString;
}

void AtomGridTable::SetValue( int row, int col, const wxString& value ){
        if(m_pCtrlInst == NULL)
                return;
        AtomBondArray& atomBondArray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        MeasureInfo mInfo = MEASURE_SUCCESS;
        double doubleValue = BP_NULL_VALUE;
        double oldValue = BP_NULL_VALUE;
        int i = 0;
        if(COL_ID_SYMBOL == col) {
                for(i = 0; i < ELEM_COUNT; i++) {
                        if(ELEM_NAMES[i].Cmp(value) == 0) {
                                atomBondArray[row].atom.elemId = (ElemId)(i + 1);
                                atomBondArray[row].atom.SetDispRadius(GetElemInfo((ElemId)(i + 1))->dispRadius);
                                break;
                        }
                }
        }else if(COL_ID_X == col || COL_ID_Y == col || COL_ID_Z == col) {
                if(value.ToDouble(&doubleValue)) {
                        atomBondArray[row].atom.pos[col - COL_ID_X] = doubleValue;
                }
        }else if(COL_ID_LAYER == col) {
                if(aryStrLayer[0].Cmp(value) == 0) {
                        atomBondArray[row].atom.layer = LAYER_HIGH;
                }else if(aryStrLayer[1].Cmp(value) == 0) {
                        atomBondArray[row].atom.layer = LAYER_MEDIUM;
                }else {
                        atomBondArray[row].atom.layer = LAYER_LOW;
                }
        }else {
                //zmatrix
                if(COL_ID_NA == col || COL_ID_NB == col || COL_ID_NC == col) {
                        long longValue = BP_NULL_VALUE;
                        if(value.ToLong(&longValue) && longValue >= 0 && longValue < row) {

                        }else {
                                Tools::ShowInfo(wxString::Format(wxT("Please input an integer less than %d!"), row));
                        }
                }else if(COL_ID_BOND == col) {
                        if(value.ToDouble(&doubleValue) && doubleValue > 0.0) {
                                mInfo = ChangeBondDistance(doubleValue, StringUtil::ToInt(GetValue(row, COL_ID_NA)) - 1, row, atomBondArray, true);
                        }else {
                                Tools::ShowInfo(wxT("Please input a positive bond distance value!"));
                        }
                }else if(COL_ID_ANGLE == col) {
                        if(value.ToDouble(&doubleValue) && doubleValue >= 0.0 && doubleValue <= 360.0) {
                                if(doubleValue < 0.1) doubleValue = 0.1f;
                    else if(doubleValue > 179.9 && doubleValue <= 180.0) doubleValue = 179.9f;
                    else if(doubleValue > 180.0 && doubleValue < 180.1) doubleValue = 180.1f;
                    else if(doubleValue > 359.9) doubleValue = 359.9f;
                    int na = StringUtil::ToInt(GetValue(row, COL_ID_NA)) - 1;
                    int nb = StringUtil::ToInt(GetValue(row, COL_ID_NB)) - 1;
                    m_pCtrlInst->GetOldAngleValue(nb, na, row, oldValue);
                    mInfo = ChangeBondAngle(doubleValue, oldValue, nb, na, row, atomBondArray, true);
                    if(mInfo == MEASURE_SUCCESS) {
                        m_pCtrlInst->SetOldAngleValue(nb, na, row, (float)doubleValue);
                           }
                        }else {
                                Tools::ShowInfo(wxT("Please input an angle value in [0, 360.0] degree!"));
                        }
                }else if(COL_ID_DIHEDRAL == col) {
                        if(value.ToDouble(&doubleValue) && doubleValue >= -180.0 && doubleValue <= 180.0) {
                                mInfo = ChangeDihedral(doubleValue, StringUtil::ToInt(GetValue(row, COL_ID_NC)) - 1, StringUtil::ToInt(GetValue(row, COL_ID_NB)) - 1, StringUtil::ToInt(GetValue(row, COL_ID_NA)) - 1, row, atomBondArray, true);
                        }else {
                                Tools::ShowInfo(wxT("Please input a dihedral value in [-180.0, 180.0] degree!"));
                        }
                }
                if(mInfo != MEASURE_SUCCESS) {
                        MeasurePnl::ShowMeasureInfo(mInfo);
                }
        }
        m_pMolView->RefreshGraphics();
}

wxString AtomGridTable::GetColLabelValue(int col){
        return colHeaders[col];
}

RowColorGridCellAttrProvider::RowColorGridCellAttrProvider() {
    m_attrForOddRows = new wxGridCellAttr;
    m_attrForOddRows->SetBackgroundColour(wxColour(200, 250, 230));
    m_attrForEmptyCell = new wxGridCellAttr;
    m_attrForEmptyCell->SetBackgroundColour(wxColour(220, 220, 220));
    m_attrForEmptyCell->SetReadOnly();
}

RowColorGridCellAttrProvider::~RowColorGridCellAttrProvider() {
        m_attrForOddRows->DecRef();
        m_attrForEmptyCell->DecRef();
}

wxGridCellAttr *RowColorGridCellAttrProvider::GetAttr(int row, int col, wxGridCellAttr::wxAttrKind  kind) const {
        wxGridCellAttr *attr = wxGridCellAttrProvider::GetAttr(row, col, kind);

        if((row <= 0 && (COL_ID_NA == col || COL_ID_BOND == col))
                || (row <= 1 && (COL_ID_NB == col || COL_ID_ANGLE == col))
                || (row <= 2 && (COL_ID_NC == col || COL_ID_DIHEDRAL == col))) {
                if (!attr) {
                        attr = m_attrForEmptyCell;
                        attr->IncRef();
                } else {
                        if (!attr->HasBackgroundColour()) {
                                wxGridCellAttr *attrNew = attr->Clone();
                                attr->DecRef();
                                attr = attrNew;
                                attr->SetBackgroundColour(wxColour(220, 220, 220));
                                attr->SetReadOnly();
                        }
                }
        }else if (row % 2) {
                if (!attr) {
                        attr = m_attrForOddRows;
                        attr->IncRef();
                } else {
                        if (!attr->HasBackgroundColour()) {
                                wxGridCellAttr *attrNew = attr->Clone();
                                attr->DecRef();
                                attr = attrNew;
                                attr->SetBackgroundColour(wxColour(200, 250, 230));
                        }
                }
        }

        return attr;
}
