/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "zmatrix.h"
#include "commons.h"
#include "measure.h"
#include "measureutils.h"
#include <stdio.h>
#include <wx/arrimpl.cpp>

ZMatrix::ZMatrix()
{
        for(int i = 0; i < ATOM_MAX_BOND_COUNT; i++) {
        bonds[i] = ChemBond();
    }
    bondCount = 0;
}



ZMatrixItem::ZMatrixItem() {
    atomIndex = BP_NULL_VALUE;
    value = BP_NULL_VALUE;
}

WX_DEFINE_OBJARRAY(ZMatrixArray);




static  int  CheckZMatrixIndex (int atom1, int atom2, int atom3,  const AtomBondArray& atomBondArray);

static  void FetchZMatrixIndex (const AtomBondArray& atomBondArray, ZMatrix& internalCorItem, int rootAtom);


/***********************************************************************
 * here check that the three atoms given should not be lining
 * if lining, return 0; else return 1
 ***********************************************************************/
int CheckZMatrixIndex (int atom1, int atom2, int atom3,  const AtomBondArray& atomBondArray)
{
  int result;
  double angle;

  result = 1;
  // here we require that the angle should not be 180 or 0!
  angle = MeasureBondAngle(-1.0,atom1,atom2,atom3,atomBondArray);
  if (fabs(angle)<MEASURE_PREC || fabs(180.0-fabs(angle))<MEASURE_PREC) {
    result = 0;
  }

  return result;
}



/****************************************************************************
 * this function is used as fetching the index in the z-matrix for the
 * rootAtom
 * the atom index writen into the zmatrix is:
 * rootAtom  atom1  atom2  atom3
 ****************************************************************************/
void  FetchZMatrixIndex (const AtomBondArray& atomBondArray, ZMatrix& internalCorItem, int rootAtom)
{

  int i;
  int atomLabel;
  int atom1;                        // their definition see above
  int atom2;
  // indicating that whether the bond ,angle or dihedral index has been found
  bool finishFlag;

  // stuff the bond index: atom1
  // firstly, we try to use the atom bond linking with the rootAtom
  // there will be two rounds of searching, first round is to search
  // whether we can find a heavy atom
  finishFlag = false;
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
    atomLabel = atomBondArray[rootAtom].bonds[i].atomIndex;
    if ( atomLabel >= rootAtom) {
      continue;
    }
    else if (atomLabel == BP_NULL_VALUE){
      continue;
    }
    else if ((ElemId)atomBondArray[atomLabel].atom.elemId == ELEM_H) {
      continue;
    }
    else {
      internalCorItem.bond.atomIndex = atomLabel;
      finishFlag = true;
      break;
    }
  }

  // the second round of search to find the bond index
  // this round will be find any atom which linking to the atomIndex
  if (! finishFlag) {

    for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
      atomLabel = atomBondArray[rootAtom].bonds[i].atomIndex;
      if ( atomLabel >= rootAtom) {
        continue;
      }
      else if (atomLabel == BP_NULL_VALUE){
        continue;
      }
      else {
        internalCorItem.bond.atomIndex = atomLabel;
        finishFlag = true;
        break;
      }
    }

  }

  // so the search is still not finished yet, that means we have
  // no previous atoms linking to the atomIndex. so choose arbitray one
  if (! finishFlag) {

    internalCorItem.bond.atomIndex = rootAtom - 1;
    finishFlag = true;

  }
  atom1 = internalCorItem.bond.atomIndex;


  // stuff the angle index: atom2
  // firstly, we try to use the atom linking with the atom1
  // there will be two rounds of searching, first round is to search
  // whether we can find a heavy atom
  finishFlag = false;
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
    atomLabel = atomBondArray[atom1].bonds[i].atomIndex;
    if ( atomLabel >= rootAtom ) {
      continue;
    }
    else if (atomLabel == BP_NULL_VALUE){
      continue;
    }
    else if ((ElemId)atomBondArray[atomLabel].atom.elemId == ELEM_H) {
      continue;
    }
    else if (CheckZMatrixIndex (rootAtom, atom1, atomLabel, atomBondArray)== 0){
      continue;
    }
    else {
      internalCorItem.angle.atomIndex = atomLabel;
      finishFlag = true;
      break;
    }
  }


  // second round: use an arbitrary atom found linking with atom1
  if (! finishFlag) {
    for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
      atomLabel = atomBondArray[atom1].bonds[i].atomIndex;
      if ( atomLabel >= rootAtom ) {
        continue;
      }
      else if (atomLabel == BP_NULL_VALUE){
        continue;
      }
      else if (CheckZMatrixIndex(rootAtom, atom1, atomLabel, atomBondArray)== 0){
        continue;
      }
      else {
        internalCorItem.angle.atomIndex = atomLabel;
        finishFlag = true;
        break;
      }
    }
  }


  // finally choose the arbitrary atom
  if (! finishFlag) {
    for (i=rootAtom-1; i>=0; i--) {
      if ( i == atom1) {
        continue;
      }
      else if (CheckZMatrixIndex (rootAtom,atom1,i,atomBondArray) == 0) {
        continue;
      }
      else {
        internalCorItem.angle.atomIndex = i;
        finishFlag = true;
        break;
      }
    }
  }
  atom2 = internalCorItem.angle.atomIndex;


  // stuff the dihedral index: atom3
  // firstly search the atoms connecting with atom2, whether we can find
  // a proper one (H is ok)
  finishFlag = false;
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
    atomLabel = atomBondArray[atom2].bonds[i].atomIndex;
    if ( atomLabel >= rootAtom || atomLabel == atom1) {
      continue;
    }
    else if (atomLabel == BP_NULL_VALUE){
      continue;
    }
    else if (CheckZMatrixIndex (atom1, atom2, atomLabel, atomBondArray) == 0) {
      continue;
    }
    else {
      internalCorItem.dihedral.atomIndex = atomLabel;
      finishFlag = true;
      break;
    }
  }


  // second round: search the atoms which linking with atom1
  if (! finishFlag) {
    for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
      atomLabel = atomBondArray[atom1].bonds[i].atomIndex;
      if ( atomLabel >= rootAtom || atomLabel == atom2) {
        continue;
      }
      else if (atomLabel == BP_NULL_VALUE){
        continue;
      }
      else if (CheckZMatrixIndex (atom1, atom2, atomLabel, atomBondArray)==0) {
        continue;
      }
      else {
        internalCorItem.dihedral.atomIndex = atomLabel;
        finishFlag = true;
        break;
      }
    }
  }


  // finally we have to choose an arbitrary one
  if (! finishFlag) {
    for (i=rootAtom-1; i>=0; i--) {
      if ( i == atom1) {
        continue;
      }
      if ( i == atom2) {
        continue;
      }
      else if (CheckZMatrixIndex (atom1,atom2,i,atomBondArray) == 0) {
        continue;
      }
      else {
        internalCorItem.dihedral.atomIndex = i;
        finishFlag = true;
        break;
      }
    }
  }


}





/***************************************************************************
 * the function which used to converte coordinates from
 * the xyz  to the zmatrix for the atom specified by atomIndex
 * the result will be written into the zmatrixArray
 * if there's angle is 180 or 0, we will return an error message
 * because in this case the dihedral is not certain
 * if the dihedral is not fixed according to the z matrix provided by the
 * user, an error message also returned
****************************************************************************/
void  FetchZMatrix (const AtomBondArray& atomBondArray, ZMatrix& internalCorItem, int rootAtom)
{

  int i;
  int atom;
  int atom1;
  int atom2;
  int atom3;
  double dihedral;

  // the first three atoms need not to call the function below
  if (rootAtom == 0) {
    return;
  }
  else if (rootAtom == 1) {
    internalCorItem.bond.atomIndex = 0;
    internalCorItem.bond.value = MeasureBondDistance(rootAtom, 0, atomBondArray);
    return;
  }
  else if (rootAtom == 2) {

    atom1 = 1;
    // search that whether 0 or 1 linking with rootAtom
    for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
      atom = atomBondArray[rootAtom].bonds[i].atomIndex;
      if ( atom == 0) {
        atom1 = 0;
        break;
      }
      else if (atom == 1) {
        atom1 = 1;
        break;
      }
      else {
        continue;
      }
  }
    if (atom1 == 1) {
      atom2 = 0;
    }
    else {
      atom2 = 1;
    }

    internalCorItem.bond.atomIndex = atom1;
    internalCorItem.angle.atomIndex = atom2;

    // bond length
    internalCorItem.bond.value = MeasureBondDistance(rootAtom, atom1, atomBondArray);

    // angle
    internalCorItem.angle.value= MeasureBondAngle(-1.0, rootAtom, atom1, atom2, atomBondArray);

    return;

  }

  // deal with the situation which rootAtom>=3
  else {

    // fetch the conectivity for the atomIndex
    FetchZMatrixIndex (atomBondArray, internalCorItem, rootAtom);


    atom1 = internalCorItem.bond.atomIndex;
    atom2 = internalCorItem.angle.atomIndex;
    atom3 = internalCorItem.dihedral.atomIndex;

    // bond length
    internalCorItem.bond.value = MeasureBondDistance(rootAtom, atom1, atomBondArray);

    // angle
    internalCorItem.angle.value= MeasureBondAngle(-1.0, rootAtom, atom1, atom2, atomBondArray);

    // dihedral
    MeasureDihedral(dihedral, rootAtom, atom1, atom2, atom3, atomBondArray);
    internalCorItem.dihedral.value = dihedral;

  }

}







/*******************************************************************************
 * change the zmatrix coordinate into the xyz form
 * used to load in the coordinates into the atomBondArray
********************************************************************************/
void  IntToXYZ (AtomBondArray& atomBondArray,ZMatrixArray& zmatrixArray)
{
    int i;
    int nAtoms;
  //  FILE * debug;

    // determine how much atoms we have
    nAtoms = zmatrixArray.GetCount();

    // debug codes
//        debug = fopen ("test.txt", "w");
//        for (i=0; i<nAtoms; i++) {
//                fprintf (debug, " %d   %d  %f   %d   %f   %d    %f\n", i,  zmatrixArray[i].bond.atomIndex,
//                        zmatrixArray[i].bond.value, zmatrixArray[i].angle.atomIndex,
//                        zmatrixArray[i].angle.value, zmatrixArray[i].dihedral.atomIndex,
//                        zmatrixArray[i].dihedral.value);
//        }
//    fclose(debug);

    // the first atom
    atomBondArray[0].atom.pos.x = 0.0;
    atomBondArray[0].atom.pos.y = 0.0;
    atomBondArray[0].atom.pos.z = 0.0;


    // the second atom
    if (nAtoms >= 2) {
      atomBondArray[1].atom.pos.x = zmatrixArray[1].bond.value;
      atomBondArray[1].atom.pos.y = 0.0;
      atomBondArray[1].atom.pos.z = 0.0;
    } else {
      return;
    }

    // the third atom,placed on the xy plane, y>0
    if (nAtoms >= 3) {
      if (zmatrixArray[2].bond.atomIndex == 0) {
        atomBondArray[2].atom.pos.x = zmatrixArray[2].bond.value*cos(zmatrixArray[2].angle.value*RADIAN);
        atomBondArray[2].atom.pos.y = zmatrixArray[2].bond.value*sin(zmatrixArray[2].angle.value*RADIAN);
      }
      else {
        atomBondArray[2].atom.pos.x = zmatrixArray[1].bond.value - zmatrixArray[2].bond.value*cos(zmatrixArray[2].angle.value*RADIAN);
        atomBondArray[2].atom.pos.y = zmatrixArray[2].bond.value*sin(zmatrixArray[2].angle.value*RADIAN);
      }
      atomBondArray[2].atom.pos.z = 0.0;
    } else {
      return;
    }


    // deal with the other atoms
    if (nAtoms > 3) {
      for (i=3; i<nAtoms; i++) {
            LoadInXYZ (atomBondArray,zmatrixArray, i);
      }
    }

//        fprintf (debug, "\n");
//        debug = fopen ("test.txt", "a");
//        fprintf (debug, "\n");
//        for (i=0; i<nAtoms; i++) {
//                fprintf (debug, " %f    %f   %f\n", atomBondArray[i].atom.pos.x,
//                        atomBondArray[i].atom.pos.y, atomBondArray[i].atom.pos.z);
//        }
//        fclose(debug);

}


/**********************************************************************************
 * load in xyz coordinate for each atom in the z matrix
 * this function is used in adding the z matrix item
***********************************************************************************/
void  LoadInXYZ (AtomBondArray& atomBondArray, ZMatrixArray& zmatrixArray, int atom)
{

//        FILE * debug;

    /********************************************************************
     * here we have such sequence of atom:
     * atom   1  (bondlength)  2  (angle)  3  (dihedral)
     * the atom's coordinate is what we want to seek (x,y,z)
     ********************************************************************/

    double x1 = atomBondArray[zmatrixArray[atom].bond.atomIndex].atom.pos.x;
    double y1 = atomBondArray[zmatrixArray[atom].bond.atomIndex].atom.pos.y;
    double z1 = atomBondArray[zmatrixArray[atom].bond.atomIndex].atom.pos.z;
    double x2 = atomBondArray[zmatrixArray[atom].angle.atomIndex].atom.pos.x;
    double y2 = atomBondArray[zmatrixArray[atom].angle.atomIndex].atom.pos.y;
    double z2 = atomBondArray[zmatrixArray[atom].angle.atomIndex].atom.pos.z;
    double x3 = atomBondArray[zmatrixArray[atom].dihedral.atomIndex].atom.pos.x;
    double y3 = atomBondArray[zmatrixArray[atom].dihedral.atomIndex].atom.pos.y;
    double z3 = atomBondArray[zmatrixArray[atom].dihedral.atomIndex].atom.pos.z;
    double x,y,z;
        double dihedral;
    double distance;

    double angleX  = 0.0;
    double cosalfa = 0.0;
    bool labelX = false;

    double angleY  = 0.0;
    double cosbeta = 0.0;
    bool labelY = false;

    double angleZ  = 0.0;
    double cosgama = 0.0;
    bool labelZ = false;                // used in rotation around axis


//        debug = fopen ("test.txt", "a");
//    fprintf (debug, "atom is: %d\n", atom);
//        fprintf (debug, "%f    %f   %f\n", x1, y1, z1);
//        fprintf (debug, "%f    %f   %f\n", x2, y2, z2);
//        fprintf (debug, "%f    %f   %f\n", x3, y3, z3);
//        fprintf (debug, "\n");

    // move the second atom to the origin, so as the first atom and the third atom
    x1 -= x2;
    y1 -= y2;
    z1 -= z2;
    x3 -= x2;
    y3 -= y2;
    z3 -= z2;
    distance = sqrt(x1*x1+y1*y1+z1*z1);

    // move the line between 1 and 2 to the xz plane, z>0
    if(fabs(x1) > MEASURE_PREC || fabs(y1) > MEASURE_PREC) {
        cosgama = x1/sqrt(x1*x1 + y1*y1);
        if (y1<0) //counter-clockwise
            angleZ =  acos(cosgama);
        else      //clockwise
            angleZ = -acos(cosgama);
        RotationByZ (&x1, &y1, angleZ);
        RotationByZ (&x3, &y3, angleZ);
    }
    else {
        labelZ = true;
    }

    // move the line between 1 and 2 to the x axis, x>0
    if(fabs(x1) > MEASURE_PREC || fabs(z1) > MEASURE_PREC) {
        cosbeta = x1/sqrt (x1*x1 + z1*z1);
        if (z1>0) //counter-clockwise
            angleY =  acos(cosbeta);
        else      //clockwise
            angleY = -acos(cosbeta);
        RotationByY (&x1, &z1, angleY);
        RotationByY (&x3, &z3, angleY);
    }
    else {
        labelY = true;
    }

    // move the line between 2 and 3 to the xy plane, y>0
    if(fabs(y3) > MEASURE_PREC || fabs(z3) > MEASURE_PREC) {
        cosalfa = y3/sqrt (y3*y3 + z3*z3);
        if (z3>0)         // clockwise
            angleX = -acos(cosalfa);
        else // counter-clockwise
            angleX = acos(cosalfa);
        RotationByX (&y3, &z3, angleX);
    }
    else {
        labelX = true;
    }


//        fprintf (debug, "%f    %f   %f\n", x1, y1, z1);
//        fprintf (debug, "%f    %f   %f\n", x3, y3, z3);
//        fprintf (debug, "\n");


        // restrict the dihedral within -180 to 180
        dihedral = zmatrixArray[atom].dihedral.value;
        if (dihedral <-180.0) {
                while (1) {
                        if (dihedral >= -180.0) {
                                break;
                        } else {
                            dihedral = dihedral + 360.0;
                        }
                }
        } else if (dihedral > 180.0) {
                while (1) {
                        if (dihedral <= 180.0) {
                                break;
                        } else {
                            dihedral = dihedral - 360.0;
                        }
                }
        }


//        fprintf (debug, "%f\n", dihedral);
//        fprintf (debug, "\n");


    // calculate the x, y, z value
    z = zmatrixArray[atom].bond.value*sin(zmatrixArray[atom].angle.value*RADIAN)*sin(dihedral*RADIAN);
    y = zmatrixArray[atom].bond.value*sin(zmatrixArray[atom].angle.value*RADIAN)*cos(dihedral*RADIAN);
    x = distance-zmatrixArray[atom].bond.value*cos(zmatrixArray[atom].angle.value*RADIAN);


//        fprintf (debug, "%f    %f   %f\n", x, y, z);
//        fprintf (debug, "%f    %f   %f\n", x1, y1, z1);
//        fprintf (debug, "%f    %f   %f\n", 0.0, 0.0, 0.0);
//        fprintf (debug, "%f    %f   %f\n", x3, y3, z3);
//        fprintf (debug, "\n");


    // move the x,y,z value back to the original coordinate system
    if (! labelX) {
        angleX = -angleX;
        RotationByX (&y, &z, angleX);
    }
    if (! labelY) {
        angleY = -angleY;
        RotationByY (&x, &z, angleY);
    }
    if (! labelZ) {
        angleZ = -angleZ;
        RotationByZ (&x, &y, angleZ);
    }
    x += x2;
    y += y2;
    z += z2;


//        fprintf (debug, "%f    %f   %f\n", x, y, z);
//        fclose(debug);


    // write the result back
    atomBondArray[atom].atom.pos.x = x;
    atomBondArray[atom].atom.pos.y = y;
    atomBondArray[atom].atom.pos.z = z;


}
