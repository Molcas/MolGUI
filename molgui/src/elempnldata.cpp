/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "elempnldata.h"
#include "stringutil.h"

static ElemPnlData* elemPnlData = NULL;

ElemPnlData* ElemPnlData::Get(void) {
        if(elemPnlData == NULL) {
                elemPnlData = new ElemPnlData();
                InitElemsInfo();
        }
        return elemPnlData;
}

void ElemPnlData::Free(void) {
        if(elemPnlData) delete elemPnlData;
}

ElemPnlData::ElemPnlData() {
        m_bondPos = BOND_POS_NULL;
        m_elemId = ELEM_NULL;
}

ElemPnlData::~ElemPnlData() {
}

void ElemPnlData::ClearElemId(void){
   this->m_elemId = (ElemId)BP_NULL_VALUE;
}

/*void ElemPnlData::ClearBondType(void) {
        this->m_bondType = (BondType)BP_NULL_VALUE;
}

void ElemPnlData::ClearBondPos(void) {
        this->m_bondPos = (BondPos)BP_NULL_VALUE;
}*/

void ElemPnlData::SetElemId(ElemId elemId) {
        if(elemId == ELEM_DUMMY) {
                m_elemId = (ElemId)DUMMY_ATOM_ID;
        }else {
                m_elemId = elemId;
        }
        ClearFragment();
}

ElemId ElemPnlData::GetElemId(void) const{
        return this->m_elemId;
}

void ElemPnlData::SetBondType(BondType bondType){
        this->m_bondType = bondType;
        ClearFragment();
}

BondType ElemPnlData::GetBondType(void) const{
        return this->m_bondType;
}

void ElemPnlData::SetBondPos(BondPos bondPos){
        this->m_bondPos = bondPos;
}

BondPos ElemPnlData::GetBondPos(void) const{
        return this->m_bondPos;
}

bool ElemPnlData::IsValidElemId(void) const{
        if(GetElemId() > 0 || GetElemId() == DUMMY_ATOM_ID)
        return true;
    return false;
}

bool ElemPnlData::IsValidBondType(void) const{
    if(GetBondType() > 0)
        return true;
    return false;
}

bool ElemPnlData::IsValidBondPos(void) const{
    if(GetBondPos() > 0 || (GetElemId() == DUMMY_ATOM_ID && GetBondPos() >= 0))
        return true;
    return false;
}

void ElemPnlData::SetFragment(wxString name) {
    m_fragmentName = name;
    ClearElemId();
}

wxString ElemPnlData::GetFragment(void) const {
    return m_fragmentName;
}

void ElemPnlData::ClearFragment(void) {
    m_fragmentName = wxEmptyString;
}

bool ElemPnlData::IsValidFragment(void) {
    return !StringUtil::IsEmptyString(m_fragmentName);
}

CtrlInst* ElemPnlData::GetPreviewCtrlInst(void) const {
        return m_previewCtrlInst;
}

void ElemPnlData::SetPreviewCtrlInst(CtrlInst* ctrlInst) {
        m_previewCtrlInst = ctrlInst;
}
