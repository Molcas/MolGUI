/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "matrix4x4.h"

Matrix4x4::Matrix4x4() {
        int row, col;
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        m_mat[row][col] = 0.0;
                }
        }
}

GLdouble* Matrix4x4::GetMatrix(void) const {
        return (GLdouble*)(&m_mat);
}

void Matrix4x4::Transpose(GLdouble* trans) const {
        int row, col;
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        trans[4 * col + row] = m_mat[row][col];
                }
        }
}

void Matrix4x4::SetIdentity(void) {
        int row, col;
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        m_mat[row][col] = (row == col);
                }
        }
}

void Matrix4x4::PreMultiply(Matrix4x4 mat) {
        int row, col, i;
        Matrix4x4 matTemp;
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        for(i = 0; i < 4; i++) {
                                matTemp.m_mat[row][col] += mat.m_mat[row][i] * m_mat[i][col];
                        }
                }
        }
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        m_mat[row][col] = matTemp.m_mat[row][col];
                }
        }
        //wxLogError(ToString());
}

void Matrix4x4::Multiply(Matrix4x4 mat) {
        int row, col, i;
        Matrix4x4 matTemp;
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        for(i = 0; i < 4; i++) {
                                matTemp.m_mat[row][col] += m_mat[row][i] * mat.m_mat[i][col];
                        }
                }
        }
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        m_mat[row][col] = matTemp.m_mat[row][col];
                }
        }
        //wxLogError(ToString());
}

AtomPosition3F Matrix4x4::Multiply(AtomPosition3F pos) {
        AtomPosition3F result;
        int row, i;
        for(row = 0; row < 3; row++) {
                for(i = 0; i < 3; i++) {
                        result[row] += m_mat[row][i] * pos[i];
                }
                result[row] += m_mat[row][3];
        }
        return result;
}

AtomPosition Matrix4x4::Multiply(AtomPosition pos) {
        AtomPosition result;
        int row, i;
        for(row = 0; row < 3; row++) {
                for(i = 0; i < 3; i++) {
                        result[row] += m_mat[row][i] * pos[i];
                }
                result[row] += m_mat[row][3];
        }
        return result;
}

void Matrix4x4::Translate(AtomPosition3F trans) {
        int row;
        Matrix4x4 matTemp;
        matTemp.SetIdentity();
        for(row = 0; row < 3; row++) {
                matTemp.m_mat[row][3] = trans[row];
        }
        Multiply(matTemp);
}

void Matrix4x4::PreTranslate(AtomPosition3F trans) {
        int row;
        Matrix4x4 matTemp;
        matTemp.SetIdentity();
        for(row = 0; row < 3; row++) {
                matTemp.m_mat[row][3] = trans[row];
        }
        PreMultiply(matTemp);
}

void Matrix4x4::Rotate(AtomPosition3F center, AtomPosition3F angle) {
        AtomPosition3F axis;
        int i;
        Translate(center);
        for(i = 0; i < 3; i++) {
                axis = GetAxis(i);
                RotateAxis(axis, angle[i]);
                center[i] = -center[i];
        }
        Translate(center);
}

void Matrix4x4::RotateAxis(AtomPosition3F axis, float delta) {
        Matrix4x4 matTemp;
        matTemp.SetIdentity();

        GLdouble axisVecLen = sqrt(axis.x*axis.x + axis.y*axis.y + axis.z*axis.z);
        if(axisVecLen > BP_PREC) {
                GLfloat cosA = cos(delta);
                GLfloat oneC = 1 - cosA;
                GLfloat sinA = sin(delta);
                GLfloat ux = axis.x / axisVecLen;
                GLfloat uy = axis.y / axisVecLen;
                GLfloat uz = axis.z / axisVecLen;

                matTemp.m_mat[0][0] = ux * ux * oneC + cosA;
                matTemp.m_mat[0][1] = ux * uy * oneC - uz * sinA;
                matTemp.m_mat[0][2] = ux * uz * oneC + uy * sinA;
                matTemp.m_mat[1][0] = uy * ux * oneC + uz * sinA;
                matTemp.m_mat[1][1] = uy * uy * oneC + cosA;
                matTemp.m_mat[1][2] = uy * uz * oneC - ux * sinA;
                matTemp.m_mat[2][0] = uz * ux * oneC - uy * sinA;
                matTemp.m_mat[2][1] = uz * uy * oneC + ux * sinA;
                matTemp.m_mat[2][2] = uz * uz * oneC + cosA;

                Multiply(matTemp);
        }
}

void Matrix4x4::Scale(float ratio) {
        int row;
        Matrix4x4 matTemp;
        matTemp.SetIdentity();
        for(row = 0; row < 3; row++) {
                matTemp.m_mat[row][row] = (GLfloat)ratio;
        }
        Multiply(matTemp);
}

AtomPosition3F Matrix4x4::GetAxis(int index){
        AtomPosition3F axis;
        axis.x = (float)m_mat[index][0];
        axis.y = (float)m_mat[index][1];
        axis.z = (float)m_mat[index][2];
        return axis;
}

AtomPosition3F Matrix4x4::GetXAxis(void){
        return GetAxis(0);
}

AtomPosition3F Matrix4x4::GetYAxis(void){
        return GetAxis(1);
}

AtomPosition3F Matrix4x4::GetZAxis(void) {
        return GetAxis(2);
}

Matrix4x4& Matrix4x4::operator =(const Matrix4x4& matrix) {
        int row, col;
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        m_mat[row][col] = matrix.m_mat[row][col];
                }
        }
        return *this;
}

void Matrix4x4::Normalize(void) {
        int row, col;
        double squareRoot;
        for(row = 0; row < 3; row++) {
                squareRoot = 0;
                for(col = 0; col < 3; col++) {
                        squareRoot += m_mat[row][col] * m_mat[row][col];
                }
                squareRoot = sqrt(squareRoot);
                if(squareRoot - 0 > BP_PREC) {
                        for(col = 0; col < 3; col++) {
                                m_mat[row][col] /= squareRoot;
                        }
                }
                m_mat[row][3] = 0;
        }
}

wxString Matrix4x4::ToString(void) {
        wxString str;
        int row, col;
        for(row = 0; row < 4; row++) {
                for(col = 0; col < 4; col++) {
                        str += wxString::Format(wxT("%lf, "), m_mat[row][col]);
                }
                str += wxT("\n");
        }
        return str;
}
