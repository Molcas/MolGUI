/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "molglcanvas.h"

#include "glcommons.h"
#include "tools.h"
#include "auisharedwidgets.h"
#include "ctrlids.h"
#include "uiprops.h"
#include "molrender.h"
#include "navindicator.h"
#include "elempnldata.h"
#include "molvieweventhandler.h"

#ifdef __SIMUGUI__
    #include "simuguiapp.h"
#endif

BEGIN_EVENT_TABLE(MolGLCanvas, wxGLCanvas)
    EVT_SIZE(MolGLCanvas::OnSize)
    EVT_PAINT(MolGLCanvas::OnPaint)
    EVT_ERASE_BACKGROUND(MolGLCanvas::OnEraseBackground)
    EVT_IDLE(MolGLCanvas::OnIdle)
    EVT_LEFT_DOWN(MolGLCanvas::OnLeftMouse)
    EVT_LEFT_UP(MolGLCanvas::OnLeftMouseUp)
    EVT_LEFT_DCLICK(MolGLCanvas::OnLeftMouseDClick)
    EVT_MIDDLE_DOWN(MolGLCanvas::OnMiddleMouse)
    EVT_MIDDLE_UP(MolGLCanvas::OnMiddleMouseUp)
    EVT_RIGHT_DOWN(MolGLCanvas::OnRightMouse)
    EVT_RIGHT_UP(MolGLCanvas::OnRightMouseUp)
    EVT_MOTION(MolGLCanvas::OnMouseMove)
    EVT_MOUSEWHEEL(MolGLCanvas::OnMouseWheel)
    EVT_CHAR(MolGLCanvas::OnKeyChar)

    //EVT_UPDATE_UI(wxID_ANY, MolGLCanvas::OnCanvasUpdateUI)
END_EVENT_TABLE()

int attrib[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 24, 0};

MolGLCanvas::MolGLCanvas(wxWindow *parent, ViewType viewType, wxWindowID id, const wxPoint &pos, const wxSize &size, long style, const wxString &name)
    : wxGLCanvas(parent, id, attrib, pos, size, style, name) {
    m_pContext = new wxGLContext(this);
    m_isGLInitialized = false;
    m_pMolData = NULL;
    m_viewType = viewType;
    m_selectedConnGroups.Clear();
}

MolGLCanvas::~MolGLCanvas() {
    //        Tools::ShowError(wxT("MolGLCanvas Deconstructor"), true);
    m_selectedConnGroups.Clear();
    delete m_pContext;
}


void MolGLCanvas::SetMolData(MolViewData *viewData) {
    m_pMolData = viewData;
}

MolViewData *MolGLCanvas::GetMolData() const {
    return m_pMolData;
}

void MolGLCanvas::InitFont(void) {
    m_FreeTypeFont =        new FreeTypeFont();
    wxString fontFilename = CheckFontFile(GetUIProps()->openGLProps.labelFont);
    // wxMessageBox(fontFilename);
    m_FreeTypeFont->Init(fontFilename, 16);

}

void MolGLCanvas::FreeFont(void) {
    m_FreeTypeFont->Clean();
    delete m_FreeTypeFont;
}

void MolGLCanvas::InitMaterials(void) {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    //glColorMaterial(GL_FRONT, GL_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);        // Enables Smooth Color Shading
    //glDisable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH_TEST);        // Enable Depth Buffer
    //glClearDepth(GL_DEPTH_TEST);              // Depth Buffer Setup
    glDepthFunc(GL_LEQUAL);           // The Type Of Depth Test To Do
    glDepthRange(0.01f, 5.0f);                //related with unproject and project
    // ----test the depth
    // int depth;
    // glGetIntegerv(GL_DEPTH_BITS, &depth);
    // std::cout << "depth InitMaterials: " << depth << std::endl;
    // --------
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);      //Realy Nice perspective calculations
    glClearColor(0.6f, 0.6f, 0.8f, 1.0f);
    //Used to display semi-transparent
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    //glClear(GL_ALWAYS);

    //GLfloat ligAmb[] = {0.0f, 0.0f, 0.0f, 0.1f};
    GLfloat ligDif[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ligSpe[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ligPos[] = { -0.8f, 0.7f, 1.0f, 0.0f};

    GLfloat ligAmb[] = {0.2f, 0.2f, 0.2f, 1.0f};

    /*GLfloat matAmb[] = {0.5f, 0.5f, 0.5f, 1.0f};
    GLfloat matDif[] = {0.43f, 0.47f, 0.54f, 1.0f};
    GLfloat matSpe[] = {0.4f, 0.4f, 0.4f, 1.0f};
    GLfloat matEmi[] = {0.1f, 0.1f, 0.1f, 1.0f};
    GLfloat matShi[] = {80.0f};
    */
    GLfloat matAmb[] = {0.3f, 0.3f, 0.3f, 1.0f};
    //GLfloat matDif[] = {0.1f, 0.5f, 0.8f, 1.0f};
    GLfloat matDif[] = {0.5f, 0.5f, 0.5f, 1.0f};
    GLfloat matSpe[] = {0.7f, 0.7f, 0.7f, 1.0f};
    GLfloat matEmi[] = {0.0f, 0.0f, 0.0f, 1.0f};
    GLfloat matShi[] = {100.0f};

    glLightfv(GL_LIGHT0, GL_AMBIENT, ligAmb);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ligDif);
    glLightfv(GL_LIGHT0, GL_SPECULAR, ligSpe);
    glLightfv(GL_LIGHT0, GL_POSITION, ligPos);

    glMaterialfv(GL_FRONT, GL_AMBIENT, matAmb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, matDif);
    glMaterialfv(GL_FRONT, GL_SPECULAR, matSpe);
    glMaterialfv(GL_FRONT, GL_EMISSION, matEmi);
    glMaterialfv(GL_FRONT, GL_SHININESS, matShi);
}

void MolGLCanvas::Draw(void) {
    wxClientDC dc(this);
    PaintCanvas(dc);
}

void MolGLCanvas::Paint(wxDC &dc) {
    PaintCanvas(dc);
}

void MolGLCanvas::SetCanvasBgColor(void) {
    switch(m_viewType) {
    case VIEW_GL_PREVIEW:
        glClearColor(GetUIProps()->openGLProps.previewBgColor.r,
                     GetUIProps()->openGLProps.previewBgColor.g,
                     GetUIProps()->openGLProps.previewBgColor.b,
                     GetUIProps()->openGLProps.previewBgColor.alpha);
        break;
    default:
        if(GetMolData()->GetCtrlInstCount() <= 0) {
            glClearColor(GetUIProps()->openGLProps.mainDisableBgColor.r,
                         GetUIProps()->openGLProps.mainDisableBgColor.g,
                         GetUIProps()->openGLProps.mainDisableBgColor.b,
                         GetUIProps()->openGLProps.mainDisableBgColor.alpha);
        } else {
            glClearColor(GetUIProps()->openGLProps.mainBgColor.r,
                         GetUIProps()->openGLProps.mainBgColor.g,
                         GetUIProps()->openGLProps.mainBgColor.b,
                         GetUIProps()->openGLProps.mainBgColor.alpha);
        }
        break;
    }
    InitGL();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glClearBufferfv(GL_COLOR, 0, color);
}

void MolGLCanvas::PaintCanvas(wxDC &dc) {
    int i;
    static bool isBusy = false;
    if(isBusy) return;
    if(!m_pContext)
        return;
    isBusy = true;
    //-------test
    //SetCurrent(*m_pContext);
    // if(!m_isGLInitialized) {
    //     InitGL();
    //     InitMaterials();
    //     m_isGLInitialized = true;
    // }
    // InitGL();
    // InitMaterials();
    // // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // //------DrawCtelInst
    // glMatrixMode(GL_MODELVIEW);
    // glLoadIdentity();

    // CtrlInst *pCtrlInst;
    // GLdouble modelView[16];
    // pCtrlInst->camera.Transpose(modelView);
    // glMultMatrixd(modelView);
    // //------
    // glPushMatrix();
    // glTranslatef(0.1f, 0.5f, 10.5f);
    // glColor3f(1,0,0);
    // GLUquadric *quad;
    // quad = gluNewQuadric();
    // gluSphere(quad,1,100,20);
    // glPopMatrix();
    // glFlush();
    //---DrawCtrlInst
    // glMatrixMode(GL_MODELVIEW);
    // glLoadIdentity();

     // ---------------- take off for test--------------- 
    if(GetParent()->IsShownOnScreen()) {
        SetCurrent(*m_pContext);
        if(!m_isGLInitialized) {
            InitGL();
            InitMaterials();
            m_isGLInitialized = true;
        }
        SetCanvasBgColor();
        InitFont();
        switch(m_viewType) {
        case VIEW_GL_PREVIEW:
            DrawCtrlInst(GetMolData()->GetActiveCtrlInst(), ACTION_ADD_FRAGMENT, GL_RENDER);
            break;
        default:
            if(GetMolData()->GetActionType() == ACTION_VIEW ) {
                for(i = 0; i < GetMolData()->GetCtrlInstCount(); i++) {
                    if( i != GetMolData()->GetActiveCtrlInstIndex()) {
                        DrawCtrlInst(GetMolData()->GetCtrlInst(i), GetMolData()->GetActionType(), GL_RENDER);
                    }
                }
            }
            // SetProjectionMatrix(true, 4, 0);
            DrawCtrlInst(GetMolData()->GetActiveCtrlInst(), GetMolData()->GetActionType(), GL_RENDER);
            if(m_selectedConnGroups.GetCount() > 0) {
                DrawViewVolume();
            }
            break;
        }
        glFlush();
        // Swap Your Buffers
        SwapBuffers();
        FreeFont();
    }
    //------------------------------------------test end
    isBusy = false;
}

void MolGLCanvas::InitGL(int subWinCount, int subWinIndex) {
    if(!m_pContext) {
        return;
    }
    if(GetParent()->IsShownOnScreen()) {
        SetCurrent(*m_pContext);
    }
    SetProjectionMatrix(true, subWinCount, subWinIndex);
}

void MolGLCanvas::SetProjectionMatrix(bool isInitViewport, int subWinCount, int subWinIndex) {
    int winWidth = 0, winHeight = 0;
    int subWinWidth = 0, subWinHeight = 0;
    int colCount = 1, rowCount = 1;

    GetClientSize(&winWidth, &winHeight);
    colCount = (int)ceil(sqrt(subWinCount));
    rowCount = (int)ceil(subWinCount * 1.0 / colCount);

    subWinWidth = winWidth / colCount;
    subWinHeight = winHeight / rowCount;

    if(isInitViewport) {
        // Set GL Viewport
        glViewport(subWinWidth * (subWinIndex % colCount), subWinHeight * (rowCount - 1 - subWinIndex / colCount), (GLint)subWinWidth, (GLint)subWinHeight);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        // glMatrixMode(GL_MODELVIEW);
        // glLoadIdentity();
    }

    //gluPerspective(45.0, (float)w/(float)h, 1.0, 1000.0);
    //
    GLdouble aspectRatio = (GLdouble)subWinWidth / (GLdouble)subWinHeight;
    GLdouble initSize = 7.0f;
    GLdouble initVol = 100.0f;
    //if(subWinWidth <= subWinHeight)
    //        glOrtho(-initSize, initSize, -aspectRatio * initSize, aspectRatio * initSize, -initVol, initVol);
    //else
    glOrtho(-aspectRatio * initSize, aspectRatio * initSize, -initSize, initSize, -initVol, initVol);
}

void MolGLCanvas::OnPaint(wxPaintEvent &event) {
    wxGLCanvas::SetCurrent(*m_pContext);
    wxPaintDC dc(this);
    Paint(dc);
    event.Skip();
}

void MolGLCanvas::OnSize(wxSizeEvent &event) {
    // This is necessary to update the context on some platforms
    // ... but deprecated since wxWidgets 2.9
    // wxGLCanvas::OnSize(event);
    
    // Set the GL Viewport (not called by wxGLCanvas::OnSize on all Platforms)
    Refresh();        // must be called to repaint on Windows when decreasing window size
    Update();        // must call update function, otherwise, the coordination cannot be set correctly in Linux.
    InitGL();
    if(m_isGLInitialized) {
        Draw();
    }
    // std::cout << "on MolGLCanvas::OnSize " << std::endl;
}

void MolGLCanvas::OnEraseBackground(wxEraseEvent &event) {
    // Do Nothing to Avoid Flashing on MSW
}

void MolGLCanvas::OnIdle(wxIdleEvent &event) {
    //this->Refresh();  // invoking this chews up lots of cpu cycles, not needed for static pictures, onpaint is sufficient
}

void MolGLCanvas::ClearSelectedConnGroups(void) {
    m_selectedConnGroups.Clear();
}

void MolGLCanvas::OnLeftMouse(wxMouseEvent &event) {
    long xCoord, yCoord;
    int i;
    int atomIndex[2] = {0, 0};
    CtrlInst *ctrlInst = NULL;

    event.GetPosition(&xCoord, &yCoord);

    SetFocus();

    // std::cout << "on leftMouse event: " << std::endl;

    if(VIEW_GL_PREVIEW == m_viewType) {
        ctrlInst = GetMolData()->GetActiveCtrlInst();
        GetSelectedAtom(ctrlInst, BP_NULL_VALUE, ACTION_ADD_FRAGMENT, xCoord, yCoord, ctrlInst->mouseClickedAtomIndex);
    } else {
        if(GetMolData()->GetActionType() == ACTION_VIEW && GetMolData()->GetCtrlInstCount() > 1) {
            // If more than one ctrlinst are displayed, a ctrlinst will be the active ctrlinst
            // if one of its atoms is selected
            for(i = 0; i < GetMolData()->GetCtrlInstCount(); i++) {
                if(GetSelectedAtom(GetMolData()->GetCtrlInst(i), i, GetMolData()->GetActionType(), xCoord, yCoord, GetMolData()->GetCtrlInst(i)->mouseClickedAtomIndex)) {
                    if(i != GetMolData()->GetActiveCtrlInstIndex()) {
#ifdef __SIMUGUI__
                        wxGetApp().GetAppFrame()->DoCheckMenuWindow(MENU_ID_WIN_START + i);
#endif
                        break;
                    }
                }
            }
            ctrlInst = GetMolData()->GetActiveCtrlInst();
        } else {
            ctrlInst = GetMolData()->GetActiveCtrlInst();
            if(ctrlInst == NULL) return;
            if(GetMolData()->GetActionType() == ACTION_MEASURE || GetMolData()->GetActionType() == ACTION_CONSTRAIN) {
                if(GetSelectedBond(ctrlInst, GetMolData()->GetActiveCtrlInstIndex(), GetMolData()->GetActionType(), xCoord, yCoord, atomIndex)) {
                    AuiSharedWidgets::Get()->GetMeasurePnl()->AddSelectedIndex(atomIndex);
                    Draw();
                    return;
                }
            }
            // if(GetMolData()->GetActionType() == ACTION_SYMMETRIZE) {

            //     if(GetSelectedBond(ctrlInst, GetMolData()->GetActiveCtrlInstIndex(), GetMolData()->GetActionType(), xCoord, yCoord, atomIndex)) {
            //         AuiSharedWidgets::Get()->GetSymmetrizePnl()->AddSelectedIndex(atomIndex);
            //         Draw();
            //         return;
            //     }

            // }
            //
            GetSelectedAtom(ctrlInst, GetMolData()->GetActiveCtrlInstIndex(), GetMolData()->GetActionType(), xCoord, yCoord, ctrlInst->mouseClickedAtomIndex);
        }
    }

    if(ctrlInst == NULL) return;

    ctrlInst->mouseButton = MOUSE_BUTTON_LEFT;
    ctrlInst->posCoord.x = xCoord;
    ctrlInst->posCoord.y = yCoord;
    // clear the group centroid on any mouse press
    ctrlInst->GetRenderingData()->ClearGroupCentroid();
    // clear the navigation indicator angle on any mouse press
    ctrlInst->navIndicatorAngle.x = ctrlInst->navIndicatorAngle.y = ctrlInst->navIndicatorAngle.z = 0;

    if(ctrlInst->mouseClickedAtomIndex >= 0) {
        // std::cout << "mouseClickedAtomIndex: " << ctrlInst->mouseClickedAtomIndex << std::endl;
        if(VIEW_GL_PREVIEW == m_viewType) {
            ctrlInst->AddSelectedAtom(ctrlInst->mouseClickedAtomIndex, false);
        } else {
            if(GetMolData()->GetActionType() == ACTION_MEASURE || GetMolData()->GetActionType() == ACTION_CONSTRAIN) {
                if(AuiSharedWidgets::Get()->IsShown(STR_PNL_MEASURE))
                    AuiSharedWidgets::Get()->GetMeasurePnl()->AddSelectedIndex(ctrlInst->mouseClickedAtomIndex);
            } else if(GetMolData()->GetActionType() == ACTION_DESIGN_BOND) {
                if(AuiSharedWidgets::Get()->IsShown(STR_PNL_DESIGNBOND))
                    AuiSharedWidgets::Get()->GetDesignBondPnl()->AddSelectedIndex(ctrlInst->mouseClickedAtomIndex);
            } else if(GetMolData()->GetActionType() == ACTION_DELETE_FRAGMENT || GetMolData()->GetActionType() == ACTION_LAYER) {
                AuiSharedWidgets::Get()->GetDeletePnl()->SetCtrlInst(ctrlInst);
                AuiSharedWidgets::Get()->GetDeletePnl()->AddSelectedIndex(ctrlInst->mouseClickedAtomIndex, false);
            } else if(GetMolData()->GetActionType() == ACTION_MAKE_BOND) {
                ctrlInst->MakeBond(ctrlInst->mouseClickedAtomIndex);
                //-------------xia---------------------
                MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
                //Tools::EnableMenu(GetMolView()->GetMenuBar(), wxID_SAVE, false);
                //AuiSharedWidgets::Get()->EnableTool(TOOL_ID_FILE_SAVE, true);
            } else if(GetMolData()->GetActionType() == ACTION_DEFINE_DUMMY) {
                //ctrlInst->DefineDummy(selectedIndex);
                ctrlInst->GetSelectionUtil()->AddSelectedIndex(ctrlInst->mouseClickedAtomIndex, true);
            } else if(GetMolData()->GetActionType() == ACTION_MIRROR) {
                AuiSharedWidgets::Get()->GetMirrorPnl()->SetCtrlInst(ctrlInst);
                AuiSharedWidgets::Get()->GetMirrorPnl()->AddSelectedIndex(ctrlInst->mouseClickedAtomIndex);
                // } else if(GetMolData()->GetActionType() == ACTION_SYMMETRIZE) {
                //     if(AuiSharedWidgets::Get()->IsShown(STR_PNL_SYMMETRIZE))
                //         AuiSharedWidgets::Get()->GetSymmetrizePnl()->AddSelectedIndex(ctrlInst->mouseClickedAtomIndex);
            } else {
                if(ctrlInst->GetRenderingData()->GetConnectionGroupCount() > 1) {
                    if(!event.ControlDown()) {
                        m_selectedConnGroups.Clear();
                    }
                    m_selectedConnGroups.Add(ctrlInst->GetRenderingData()->GetAtomBond(ctrlInst->mouseClickedAtomIndex).atom.connGroup);
                } else {
                    m_selectedConnGroups.Clear();
                }
            }
        }
    } else {
        if(GetMolData()->GetActionType() == ACTION_MEASURE || GetMolData()->GetActionType() == ACTION_CONSTRAIN ) {
            AuiSharedWidgets::Get()->GetMeasurePnl()->ClearSelection();
        } else if(GetMolData()->GetActionType() == ACTION_DESIGN_BOND) {
            AuiSharedWidgets::Get()->GetDesignBondPnl()->ClearSelection();
            // } else if(GetMolData()->GetActionType() == ACTION_SYMMETRIZE) {
            //     if (AuiSharedWidgets::Get()->GetSymmetrizePnl()->CanClear())
            //         AuiSharedWidgets::Get()->GetSymmetrizePnl()->ClearSelection();
        } else {
            m_selectedConnGroups.Clear();
        }
    }
    //printing test
    // MolRender::PrintLabelCenter(ctrlInst,this,"prueba de molglcanvas::onleftmouse");
    //--------------
    Draw();
    //--------------XIA 09workshop----------------------
    //                ((SimuPrjFrm*)(Manager::Get()->GetAppFrame()))->EnableMenuTool(true);
    //-----------------------------------------
    //------printing test fixed labels temp
    
        // AtomPosition tempAtom;
        // long dispRadius = 0.2f;
        // CtrlInst* pCtrlInst = GetMolData()->GetActiveCtrlInst();
        // //MolGLCanvas* pMolGLCanvas;
        // tempAtom.x = 0;
        // tempAtom.y = 0;
        // tempAtom.z = 0;
        // wxString psome = wxString::Format(wxT("x: %0.2f y: %0.2f"), xCoord,yCoord);
        // this->m_FreeTypeFont->PrintString(pCtrlInst, tempAtom, dispRadius, psome);
        //--------
}

void MolGLCanvas::OnLeftMouseUp(wxMouseEvent &event) {
    CtrlInst *ctrlInst = GetMolData()->GetActiveCtrlInst();
    if(ctrlInst != NULL) {
        ctrlInst->mouseButton = MOUSE_BUTTON_NONE;
        ctrlInst->moveType = MOVE_NONE;
        Draw();
    }
    SetCursor(*wxSTANDARD_CURSOR);
}
void MolGLCanvas::OnMiddleMouse(wxMouseEvent &event) {
    // std::cout << "MiddleMOuse pressed" << event.MiddleDown() << std::endl;
    OnLeftMouse(event);
    //OnMouseMove(event);
}
void MolGLCanvas::OnMiddleMouseUp(wxMouseEvent &event) {
    // std::cout << "MiddleMOuseUp pressed" << std::endl;
    OnLeftMouseUp(event);

}

void MolGLCanvas::DrawCtrlInst(CtrlInst *pCtrlInst, ActionType actionType, GLenum mode) {
    if(!pCtrlInst) return;

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    GLdouble modelView[16];
    pCtrlInst->camera.Transpose(modelView);
    glMultMatrixd(modelView);

    //Tools::ShowInfo(wxString::Format(wxT("ctrlinst %d %u %u %d "), GetMolData()->GetCtrlInstCount(), pCtrlInst->GetRenderingData()->GetAtomCount(), GetMolData()->GetActiveCtrlInst(), actionType));
    if(pCtrlInst->GetRenderingData()->GetAtomCount() > 0) {
        DrawBallSpoke(pCtrlInst, actionType, mode);
        if(mode == GL_RENDER && pCtrlInst->moveType != MOVE_NONE) {
            //Show navigation indicator
            NavIndicator nav(pCtrlInst);
             nav.Painting();
        }
    }
    if(pCtrlInst->GetSurface() && pCtrlInst->GetSurface()->IsRendering()) {
        pCtrlInst->GetSurface()->RenderSurface();
    }
    if(mode == GL_RENDER && pCtrlInst->isShowAxes) {
        MolRender::DrawAxes(pCtrlInst, this);
    }
}

void MolGLCanvas::DrawViewVolume(void) {
    unsigned i;
    CtrlInst *ctrlInst = GetMolData()->GetActiveCtrlInst();
    if(ctrlInst == NULL || ctrlInst->GetRenderingData()->GetAtomCount() <= 0) {
        return;
    }
    for(i = 0; i < m_selectedConnGroups.GetCount(); i++) {
        if((unsigned)((m_selectedConnGroups[i] + 1) * 2) <= ctrlInst->GetRenderingData()->GetGroupViewVolumeArray().GetCount()) {
            MolRender::DrawViewVolume(ctrlInst->GetRenderingData()->GetGroupViewVolumeArray()[m_selectedConnGroups[i] * 2], ctrlInst->GetRenderingData()->GetGroupViewVolumeArray()[m_selectedConnGroups[i] * 2 + 1]);
        } else {
        }
    }
}

void MolGLCanvas::DrawBallSpoke(CtrlInst *pCtrlInst, ActionType actionType, GLenum mode) {
    AtomBondArray atomBondArray = pCtrlInst->GetRenderingData()->GetAtomBondArray();
    int atomNum = pCtrlInst->GetRenderingData()->GetAtomCount();
    int connectedAtomIndex = -1;
    int i = 0, j = 0;
    ModelType modelType = MODEL_WIRE;
    bool isMoving = pCtrlInst->moveType != MOVE_NONE;

    for(i = 0; i < atomNum; i++) {
        if(GL_SELECT == mode) {
            glPushName(i);
        }
        //if (i==1) glClearDepth(GL_DEPTH_TEST);
        MolRender::DrawAtom(pCtrlInst, i, actionType, mode, this);

        //if(MODEL_SPACEFILLING != pCtrlInst->modelType) {
             // Enables bond drawing----------------------------------
        for(j = 0; j < atomBondArray[i].bondCount; j++) {
            //glEnable(GL_DEPTH_TEST);
            modelType = pCtrlInst->modelType;
            connectedAtomIndex = atomBondArray[i].bonds[j].atomIndex;
            if( i >= connectedAtomIndex)
                continue;
            if(!pCtrlInst->isShowHydrogens) {
                //also not show bond connected with default hydrogen
                if((atomBondArray[i].atom.IsDefaultHydrogen() || atomBondArray[atomBondArray[i].bonds[j].atomIndex].atom.IsDefaultHydrogen())) {
                    continue;
                }
            }
            if(pCtrlInst->isShowLayers) {
                if(!GetBondModelType(pCtrlInst, (LayerType)atomBondArray[i].atom.layer, (LayerType)atomBondArray[connectedAtomIndex].atom.layer, &modelType)) {
                    continue;
                }
            }

            if(MODEL_SPACEFILLING == modelType) {
                //do not need to draw bond
                continue;
            }
            if(GL_SELECT == mode) {
                glPushName(j);
            }
            if(MODEL_BALLSPOKE == modelType) {
                if(!isMoving || (isMoving && GetUIProps()->openGLProps.isMovingWithBonds)
                        || (mode == GL_SELECT && (ACTION_CHANGE_BOND == actionType || ACTION_MEASURE == actionType  || ACTION_SYMMETRIZE == actionType || ACTION_CONSTRAIN == actionType || ACTION_BREAK_BOND == actionType || ACTION_DESIGN_BOND == actionType))) {
                    MolRender::DrawBond(atomBondArray, i, j, ACTION_VIEW != actionType, isMoving);
                }
            } else if(MODEL_TUBE == modelType) {
                MolRender::DrawTube(&atomBondArray[i], &atomBondArray[connectedAtomIndex], true, isMoving);
            } else if(MODEL_WIRE == modelType || MODEL_BALLWIRE == modelType) {
                MolRender::DrawWire(&atomBondArray[i], &atomBondArray[connectedAtomIndex]);
            }
            if(GL_SELECT == mode) {
                glPopName();
            }
        }
        //-----------------------------

        //}
        if(GL_SELECT == mode) {
            glPopName();
        }
    }

    if(actionType == ACTION_MIRROR) {
        MolRender::DrawMirrorPlain(pCtrlInst);
    }
}

bool MolGLCanvas::GetBondModelType(CtrlInst *pCtrlInst, LayerType atom1Layer, LayerType atom2Layer, ModelType *pModelType) {
    bool show = false;
    ModelType modelType = MODEL_BALLSPOKE;
    LayerType highLayer = atom1Layer;

    if(pCtrlInst->isShowLayers) {
        if(((pCtrlInst->isShowLayerHigh && LAYER_HIGH == atom1Layer)
                || (pCtrlInst->isShowLayerMedium && LAYER_MEDIUM == atom1Layer)
                || (pCtrlInst->isShowLayerLow && LAYER_LOW == atom1Layer))
                && ((pCtrlInst->isShowLayerHigh && LAYER_HIGH == atom2Layer)
                    || (pCtrlInst->isShowLayerMedium && LAYER_MEDIUM == atom2Layer)
                    || (pCtrlInst->isShowLayerLow && LAYER_LOW == atom2Layer))) {

            show = true;
            if(atom2Layer > highLayer) {
                highLayer = atom2Layer;
            }
            modelType = GetUIProps()->chemProps.GetLayerModelType(highLayer);
            *pModelType = modelType;
        }
    }

    return show;
}


bool MolGLCanvas::GetSelectedBond(CtrlInst *pCtrlInst, int ctrlInstName, ActionType actionType, long xCoord, long yCoord, int atomIndex[2]) {
    bool selected = false;
    wxIntArray intArray;
    if(pCtrlInst) {
        GetSelectedElement(pCtrlInst, ctrlInstName, actionType, xCoord, yCoord, intArray);
        if(intArray.GetCount() == 3) {
            atomIndex[0] = intArray[1];
            atomIndex[1] = pCtrlInst->GetRenderingData()->GetAtomBondArray()[atomIndex[0]].bonds[intArray[2]].atomIndex;
            selected = true;
        }
    }
    return selected;
}

bool MolGLCanvas::GetSelectedAtom(CtrlInst *pCtrlInst, int ctrlInstName, ActionType actionType, long xCoord, long yCoord, int &atomIndex) {
    bool selected = false;
    wxIntArray intArray;

    atomIndex = BP_NULL_VALUE;

    GetSelectedElement(pCtrlInst, ctrlInstName, actionType, xCoord, yCoord, intArray);
    if(intArray.GetCount() >= 2 && intArray[1] < pCtrlInst->GetRenderingData()->GetAtomCount()) {
        atomIndex = intArray[1];
        selected = true;
        //wxGetApp().GetAppFrame()->ShowInfoMessage(wxString::Format(wxT("Atom Index = %d, Atom Id = %s"), intArray[1], Index2DisplayLabel(pCtrlInst, intArray[1]).mb_str()));
    }

    return selected;
}

void MolGLCanvas::GetSelectedElement(CtrlInst *pCtrlInst, int ctrlInstName, ActionType actionType, long xCoord, long yCoord, wxIntArray &intArray) {
    GLuint selectBuf[SELECT_BUF_SIZE];
    GLint hits;
    GLint viewport[4];

    glGetIntegerv(GL_VIEWPORT, viewport);
    glSelectBuffer(SELECT_BUF_SIZE, selectBuf);
    (void)glRenderMode(GL_SELECT);

    glInitNames();
    glPushName(ctrlInstName);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluPickMatrix((GLdouble)xCoord, (GLdouble)(viewport[3] - yCoord), SELECT_PREC, SELECT_PREC, viewport);

    SetProjectionMatrix();
    DrawCtrlInst(pCtrlInst, actionType, GL_SELECT);

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glFlush();

    intArray.Clear();
    hits = glRenderMode(GL_RENDER);
    if(hits > 0) {
        //wxString msg;
        GLuint names, *ptr;
        int i;
        unsigned j;
        float curMinZ = 10000000.0f, zDepth;
        //msg.Append(wxString::Format(wxT("hits=%d\n"), hits));
        ptr = (GLuint *) selectBuf;

        for(i = 0; i < hits; i++) {
            names = *ptr;
            //msg.Append(wxString::Format(wxT("number of names for this hits=%d\n"), names));
            ptr++;
            zDepth = (float) * ptr / 0x7FFFFFFF;
            //msg.Append(wxString::Format(wxT("z1 is %g;"), zDepth));
            ptr++;
            //msg.Append(wxString::Format(wxT("z2 is %g\n"), (float)*ptr/0x7FFFFFFF));
            ptr++;
            //msg.Append(wxT("names are "));
            if(zDepth < curMinZ) {
                intArray.Clear();
                for(j = 0; j < names; j++) {
                    //msg.Append(wxString::Format(wxT("%d "), *ptr));
                    intArray.Add(*ptr);
                    ptr++;
                }
                curMinZ = zDepth;
            } else {
                for(j = 0; j < names; j++) {
                    //msg.Append(wxString::Format(wxT("%d "), *ptr));
                    ptr++;
                }
            }
            //msg.Append(wxT("\n"));
        }
        //wxLogError(msg);
    }
}

void MolGLCanvas::Rotate(CtrlInst *pCtrlInst, AtomPosition3F rotAngle) {
    unsigned i;
    // two mode:
    // (1) rotate pivot is an atom when an atom is clicked
    // (2) centroid centered when no atoms are selected
    AtomPosition3F center = pCtrlInst->GetRotationCenter();
    if(m_selectedConnGroups.GetCount() > 0) {
        Matrix4x4 cloneCamera;
        cloneCamera.SetIdentity();
        cloneCamera.RotateAxis(pCtrlInst->camera.GetXAxis(), rotAngle.x);
        cloneCamera.RotateAxis(pCtrlInst->camera.GetYAxis(), rotAngle.y);
        cloneCamera.RotateAxis(pCtrlInst->camera.GetZAxis(), rotAngle.z);
        for(i = 0; i < m_selectedConnGroups.GetCount(); i++) {
            pCtrlInst->GetRenderingData()->RotateConnectionGroup(m_selectedConnGroups[i], center, cloneCamera);
        }
        pCtrlInst->SetDirty(true);
    } else {
        pCtrlInst->camera.Rotate(center, rotAngle);
    }
    //---print rot temp
    //pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, atom->pos, dispRadius, Index2DisplayLabel(atom, atomIndex, pCtrlInst->isShowGlobalLabels).mb_str());
    //-------
}

void MolGLCanvas::Translate(CtrlInst *pCtrlInst, AtomPosition3F posTrans) {
    unsigned i = 0;
    if(m_selectedConnGroups.GetCount() > 0) {
        Matrix4x4 cloneCamera = pCtrlInst->camera;
        cloneCamera.Normalize();

        AtomPosition3F xDisp = cloneCamera.GetXAxis();
        AtomPosition3F yDisp = cloneCamera.GetYAxis();
        AtomPosition3F zDisp = cloneCamera.GetZAxis();
        xDisp.Scale(posTrans.x);
        yDisp.Scale(posTrans.y);
        zDisp.Scale(posTrans.z);

        AtomPosition3F disp;
        disp.x = xDisp.x + yDisp.x + zDisp.x;
        disp.y = xDisp.y + yDisp.y + zDisp.y;
        disp.z = xDisp.z + yDisp.z + zDisp.z;

        for(i = 0; i < m_selectedConnGroups.GetCount(); i++) {
            pCtrlInst->GetRenderingData()->TranslateConnectionGroup(m_selectedConnGroups[i], disp);
        }
        pCtrlInst->SetDirty(true);
    } else {
        pCtrlInst->camera.PreTranslate(posTrans);
    }
}

void MolGLCanvas::OnLeftMouseDClick(wxMouseEvent &event) {
    SetFocus();
    if(VIEW_GL_PREVIEW == m_viewType) return;

    int atomIndex[2] = {0, 0};
    wxIntArray intArray;
    CtrlInst *ctrlInst = NULL;
    bool needAttachAtoms = false;
    long xCoord = 0, yCoord = 0;//, zCoord = 0;
    int selectedIndex = BP_NULL_VALUE;
    ActionType actionType;
    AtomPosition posWorld;
    AtomBondArray attachArray;
    bool isDirty = false;

    if((ctrlInst = GetMolData()->GetActiveCtrlInst()) == NULL) {
        return;
    }
    m_selectedConnGroups.Clear();

    // when not in build state, cannot build molecules
    actionType = GetMolData()->GetActionType();
    if(ACTION_VIEW == actionType || ACTION_MEASURE == actionType || ACTION_SYMMETRIZE == actionType || ACTION_CONSTRAIN == actionType || ACTION_DESIGN_BOND == actionType ) {
        return;
    }
    event.GetPosition(&xCoord, &yCoord);

    if(ACTION_ADD_FRAGMENT == actionType || ACTION_INSERT_FRAGMENT == actionType) {
        if(ElemPnlData::Get()->IsValidBondType()) {
            if(GetSelectedBond(ctrlInst, GetMolData()->GetActiveCtrlInstIndex(), ACTION_CHANGE_BOND, xCoord, yCoord, atomIndex)) {
                ctrlInst->GetRenderingData()->ChangeBondType(atomIndex, ElemPnlData::Get()->GetBondType());
                ctrlInst->SetDirty(true);
                Draw();
                return;
            }
        }
        if(ElemPnlData::Get()->IsValidElemId() && ElemPnlData::Get()->IsValidBondPos()) {
            needAttachAtoms = true;
        } else if(ElemPnlData::Get()->IsValidFragment()) {
            needAttachAtoms = true;
        }
        if(needAttachAtoms) {
            attachArray = ElemPnlData::Get()->GetPreviewCtrlInst()->GetPreviewData();

            if(ACTION_ADD_FRAGMENT == actionType) {
                if(ElemPnlData::Get()->GetElemId() != DUMMY_ATOM_ID || ElemPnlData::Get()->GetBondPos() > BOND_POS_NULL ) {
                    if(ctrlInst->GetRenderingData()->IsEmpty()) {
                        ctrlInst->GetRenderingData()->AttachRenderingDataEx(BP_NULL_VALUE, attachArray);
                        isDirty = true;
                    } else {
                        if(GetSelectedAtom(ctrlInst, GetMolData()->GetActiveCtrlInstIndex(), actionType, xCoord, yCoord, selectedIndex)) {
                            if(ctrlInst->GetRenderingData()->GetAtomBond(selectedIndex).atom.elemId == DEFAULT_HYDROGEN_ID) {
                                ctrlInst->GetRenderingData()->AttachRenderingDataEx(selectedIndex, attachArray);
                                isDirty = true;
                            } else {
                                if(!ElemPnlData::Get()->IsValidFragment()) {
                                    ctrlInst->GetRenderingData()->ReplaceRenderingData(selectedIndex, attachArray[0]);
                                    isDirty = true;
                                }
                            }
                        }
                    }
                }
            } else if(ACTION_INSERT_FRAGMENT == actionType) {
                double xWorld = xCoord, yWorld = yCoord, zWorld = 0.0;
                if(ctrlInst->GetRenderingData()->GetAtomCount() > 0) {
                    AtomPosition3F centroid = ctrlInst->GetRenderingData()->GetGroupCentroid(BP_NULL_VALUE);
                    /*AtomPosition refPoint;
                     refPoint.x = centroid.x;
                     refPoint.y = centroid.y;
                     refPoint.z = centroid.z;
                     refPoint = MolGLUtil::Project(refPoint);
                     zWorld = refPoint.z;
                     */
                    zWorld = centroid.z;
                }
                
                // AtomPosition3L world;
                // world.x = xWorld;
                // world.y = yWorld;
                // world.z = zWorld;
                // AtomPosition newpos = MolGLUtil::UnProject(world);
                // wxGetApp().GetAppFrame()->ShowInfoMessage(newpos.ToString());
                
                MolRender::GetWorldCoord2(xWorld, yWorld, zWorld);
                //wxGetApp().GetAppFrame()->ShowInfoMessage(wxString::Format(wxT("3 xWorld:%f, yWorld:%f, zWorld:%f"), xWorld, yWorld, zWorld));
                ctrlInst->GetRenderingData()->AttachRenderingDataEx(xWorld, yWorld, zWorld, attachArray);
                isDirty = true;
            }
        }
    } else if(ACTION_DELETE_FRAGMENT == actionType) {
        if(GetSelectedAtom(ctrlInst, GetMolData()->GetActiveCtrlInstIndex(), actionType, xCoord, yCoord, selectedIndex)) {
            AuiSharedWidgets::Get()->GetDeletePnl()->SetCtrlInst(ctrlInst);
            AuiSharedWidgets::Get()->GetDeletePnl()->AddSelectedIndex(selectedIndex, true);
        }
    } else if(ACTION_BREAK_BOND == actionType) {
        if(GetSelectedBond(ctrlInst, GetMolData()->GetActiveCtrlInstIndex(), actionType, xCoord, yCoord, atomIndex)) {
            if(ctrlInst->GetRenderingData()->BreakBond(atomIndex, true)) {
                isDirty = true;
            }
        }
    } else if(ACTION_DEFINE_DUMMY == actionType) {
        isDirty = ctrlInst->GetRenderingData()->DefineDummy(ctrlInst->GetSelectionUtil());
        if(isDirty) {
            ctrlInst->ClearSelectedAtoms();
        }
    }
    if(isDirty) {
        ctrlInst->SetDirty(true);
#ifdef __SIMUGUI__
        wxGetApp().GetAppFrame()->EnableMenuToolCtrlInst();
#else
        MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
#endif
    }
    Draw();
    //---------------XIA 09workshop-------------
    //        ((SimuPrjFrm*)(Manager::Get()->GetAppFrame()))->EnableMenuTool(true);
    //--------------------------------

}

void MolGLCanvas::OnRightMouse(wxMouseEvent &event) {
    CtrlInst *pCtrlInst = GetMolData()->GetActiveCtrlInst();

    if(pCtrlInst) {
        pCtrlInst->mouseButton = MOUSE_BUTTON_RIGHT;
        event.GetPosition(&pCtrlInst->posCoord.x, &pCtrlInst->posCoord.y);
        if(VIEW_GL_PREVIEW != m_viewType) {
            PopupMenu(MolViewEventHandler::Get()->GetMolView()->GetPopupMenu());
        }
    }
    //----------------XIA 09workshop--------------
    //                ((SimuPrjFrm*)(Manager::Get()->GetAppFrame()))->EnableMenuTool(true);
    //---------------------------------------
}

void MolGLCanvas::OnRightMouseUp(wxMouseEvent &event) {
    OnLeftMouseUp(event);
}

void MolGLCanvas::OnMouseMove(wxMouseEvent &event) {
    AtomPosition3F posTrans, rotAngle;
    posTrans.x = posTrans.y = posTrans.z = 0.0f;
    rotAngle.x = rotAngle.y = rotAngle.z = 0.0f;
    long xCoord, yCoord;
    CtrlInst *pCtrlInst = GetMolData()->GetActiveCtrlInst();
    if(pCtrlInst == NULL || event.Dragging() == false) return;
    if(pCtrlInst->mouseButton != MOUSE_BUTTON_LEFT) return;
    event.GetPosition(&xCoord, &yCoord);
    pCtrlInst->posCursor.x = xCoord;
    pCtrlInst->posCursor.y = yCoord;
    // std::cout << "OmouseMove" << std::endl;

    if(event.ShiftDown() || event.MiddleIsDown()) {
        // translate the molecule following the mouse movement
        // std::cout << "shiftdown" << std::endl;
        SetCursor(*wxCROSS_CURSOR);
        pCtrlInst->moveType = MOVE_TRANS;
        posTrans.x = (GLfloat)(pCtrlInst->posCursor.x - pCtrlInst->posCoord.x) / MOUSE_PREC_TRANSLATE;
        pCtrlInst->posCoord.x = pCtrlInst->posCursor.x;
        if(event.AltDown()) {
            posTrans.z = -(GLfloat)(pCtrlInst->posCursor.z - pCtrlInst->posCoord.z) / MOUSE_PREC_TRANSLATE;
            pCtrlInst->posCoord.z = pCtrlInst->posCursor.z;
        } else {
            posTrans.y = -(GLfloat)(pCtrlInst->posCursor.y - pCtrlInst->posCoord.y) / MOUSE_PREC_TRANSLATE;
            pCtrlInst->posCoord.y = pCtrlInst->posCursor.y;
        }
        Translate(pCtrlInst, posTrans);

    } else {
        // std::cout << "OmouseMove else" << std::endl;
        //
        //SetCursor(wxCursor(wxCURSOR_BULLSEYE));
        // use each step increment
        rotAngle.x = (GLfloat)(pCtrlInst->posCursor.y - pCtrlInst->posCoord.y) / MOUSE_PREC_ROTATE;
        // accumulate the rotation angle since last left mouse press to show the navigation indicator
        pCtrlInst->navIndicatorAngle.x += rotAngle.x;
        pCtrlInst->posCoord.y = pCtrlInst->posCursor.y;
        if(event.AltDown()) {
            pCtrlInst->moveType = MOVE_ROTATE_XZ;
            rotAngle.z = (GLfloat)(pCtrlInst->posCursor.x - pCtrlInst->posCoord.x) / MOUSE_PREC_ROTATE;
            pCtrlInst->navIndicatorAngle.z += rotAngle.z;
        } else {
            pCtrlInst->moveType = MOVE_ROTATE_XY;
            rotAngle.y = (GLfloat)(pCtrlInst->posCursor.x - pCtrlInst->posCoord.x) / MOUSE_PREC_ROTATE;
            pCtrlInst->navIndicatorAngle.y += rotAngle.y;
        }
        pCtrlInst->posCoord.x = pCtrlInst->posCursor.x;
        Rotate(pCtrlInst, rotAngle);
    }

    Draw();
}

void MolGLCanvas::OnMouseWheel(wxMouseEvent &event) {
    DoScale(event.GetWheelRotation() > 0);
//  wxMouseButton butD;// = event.ButtonIsDown();
//     std::cout << "MouseWheel: " << event.ButtonIsDown(butD) << std::endl;
//     if (butD == wxMOUSE_BTN_MIDDLE) {
//         std::cout << "MouseWheel button pressed: " << std::endl;
//     }
}

void MolGLCanvas::DoScale(bool enlarge) {
    CtrlInst *ctrlInst = GetMolData()->GetActiveCtrlInst();
    if(ctrlInst == NULL) {
        return;
    }
    // used for orthogonal projection view
    float scaleRatio = 1.0f;
    if(enlarge) {
        scaleRatio += KEY_DELTA_SCALE;
    } else {
        scaleRatio -= KEY_DELTA_SCALE;
    }

    ctrlInst->camera.Scale(scaleRatio);
    Draw();
}

void MolGLCanvas::OnKeyChar(wxKeyEvent &event) {
    bool treated = FALSE;
    AtomPosition3F posTrans, rotAngle;
    posTrans.x = posTrans.y = posTrans.z = 0.0f;
    rotAngle.x = rotAngle.y = rotAngle.z = 0.0f;

    // std::cout << "OnKeyChar: " << event.GetKeyCode() << std::endl;

    CtrlInst *pCtrlInst = GetMolData()->GetActiveCtrlInst();
    if(!pCtrlInst) return;
    
    if(event.ShiftDown()) {
        // Translate (move the molecule while it's pressing)    
        if(event.GetKeyCode() == WXK_LEFT) {
            posTrans.x = -KEY_DELTA_TRNASLATE;
            treated = TRUE;
        } else if(event.GetKeyCode() == WXK_RIGHT) {
            posTrans.x = +KEY_DELTA_TRNASLATE;
            treated = TRUE;
        } else if(event.GetKeyCode() == WXK_UP) {
            if(event.AltDown()) {
                posTrans.z = +KEY_DELTA_TRNASLATE;
            } else {
                posTrans.y = +KEY_DELTA_TRNASLATE;
            }
            treated = TRUE;
        } else if(event.GetKeyCode() == WXK_DOWN) {
            if(event.AltDown()) {
                posTrans.z = -KEY_DELTA_TRNASLATE;
            } else {
                posTrans.y = -KEY_DELTA_TRNASLATE;
            }
            treated = TRUE;
        }
        Translate(pCtrlInst, posTrans);
    } else if(event.ControlDown()) {
        // Scale
        if(event.GetKeyCode() == WXK_LEFT || event.GetKeyCode() == WXK_UP) {
            DoScale(true);
            treated = TRUE;
        } else if(event.GetKeyCode() == WXK_RIGHT || event.GetKeyCode() == WXK_DOWN) {
            DoScale(false);
            treated = TRUE;
        }
    } else if(GetMolData()->GetActionType() == ACTION_MEASURE) {
        if(event.GetKeyCode() == WXK_PAGEUP) {
            AuiSharedWidgets::Get()->GetMeasurePnl()->ChangeBondValueByPageKey(true);
            treated = TRUE;
        } else if(event.GetKeyCode() == WXK_PAGEDOWN) {
            AuiSharedWidgets::Get()->GetMeasurePnl()->ChangeBondValueByPageKey(false);
            treated = TRUE;
        }
    }
    // Rotate
    else if(event.GetKeyCode() == WXK_LEFT) {
        rotAngle.y = -KEY_DELTA_ROTATE;
        treated = TRUE;
    } else if(event.GetKeyCode() == WXK_RIGHT) {
        rotAngle.y = +KEY_DELTA_ROTATE;
        treated = TRUE;
    } else if(event.GetKeyCode() == WXK_UP) {
        if(event.AltDown()) {
            rotAngle.z = -KEY_DELTA_ROTATE;
        } else {
            rotAngle.x = -KEY_DELTA_ROTATE;
        }
        treated = TRUE;
    } else if(event.GetKeyCode() == WXK_DOWN) {
        if(event.AltDown()) {
            rotAngle.z = +KEY_DELTA_ROTATE;
        } else {
            rotAngle.x = +KEY_DELTA_ROTATE;
        }
        treated = TRUE;
    }
    Rotate(pCtrlInst, rotAngle);
    Draw();

    // Propagate event
    if(!treated)
    {
        event.ResumePropagation (wxEVENT_PROPAGATE_MAX);
        event.Skip ();
    }
}
