/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "deletepnl.h"
#include "tools.h"
#include "atommanuutils.h"
#include "molvieweventhandler.h"
#define BP_SLIDER_PRECISION 1000.0

////////////////////////////////////////////////////////////////////
//enum
////////////////////////////////////////////////////////////////////
enum {
    ID_CTRL_RB_SELETE_TYPE,
    ID_CTRL_BTN_CLEAR,
    ID_CTRL_BTN_DELETE,
    ID_CTRL_BTN_SELECT_ALL
};
//////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE(DeletePnl, wxPanel)
    EVT_BUTTON(ID_CTRL_BTN_DELETE, DeletePnl::OnBtnDelete)
    EVT_BUTTON(ID_CTRL_BTN_CLEAR, DeletePnl::OnBtnClear)
    EVT_BUTTON(ID_CTRL_BTN_SELECT_ALL, DeletePnl::OnBtnSelectAll)
    //EVT_RADIOBOX(ID_CTRL_RB_SELETE_TYPE, DeletePnl::OnRbSeleteType)
END_EVENT_TABLE()

DeletePnl::DeletePnl(wxWindow *parent, wxWindowID id, const wxPoint &pos,
                     const wxSize &size, long style, const wxString &name ) : wxPanel(parent, id, pos, size, style, name) {
    m_pCtrlInst = NULL;
    m_pCbbLayerType = NULL;
    InitPanel();
    m_actionType = ACTION_LAYER;
}

void DeletePnl::InitPanel(void) {
    int height = wxDefaultSize.GetHeight();

    wxBoxSizer *pBsMain = new wxBoxSizer(wxHORIZONTAL);
    this->SetSizer(pBsMain);

    wxString selectType[3] = {wxT("Single"), wxT("Two Atoms   "), wxT("Connected")};
    m_pRbSelectType = new wxRadioBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(320, height), 3, selectType, 1, wxRA_SPECIFY_ROWS);
    wxButton *pBtnSelectAll = new wxButton(this, ID_CTRL_BTN_SELECT_ALL, wxT("Select All"), wxDefaultPosition, wxSize(100, 30));
    m_pBtnClear = new wxButton(this, ID_CTRL_BTN_CLEAR, wxT("Select None"), wxDefaultPosition, wxSize(100, 30));
    m_pBtnDelete = new wxButton(this, ID_CTRL_BTN_DELETE, wxT("Apply"), wxDefaultPosition, wxSize(80, 30));

    pBsMain->Add(m_pRbSelectType, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    pBsMain->Add(pBtnSelectAll, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    pBsMain->Add(m_pBtnClear, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    pBsMain->AddStretchSpacer(1);

    m_pStSetLayer = new wxStaticText(this, wxID_ANY, wxT("Set Layer"), wxDefaultPosition, wxSize(70, wxDefaultSize.GetHeight()));
    pBsMain->Add(m_pStSetLayer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    wxString layerType[3] = {wxT("High"), wxT("Medium"), wxT("Low")};
    m_pCbbLayerType = new wxComboBox(this, wxID_ANY, wxT("High"), wxDefaultPosition, wxSize(85, wxDefaultSize.GetHeight()), 3, layerType, wxCB_READONLY);
    pBsMain->Add(m_pCbbLayerType, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

    pBsMain->Add(m_pBtnDelete, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

    m_pRbSelectType->SetSelection(0);
    m_pBtnDelete->Enable(false);
}

LayerType DeletePnl::GetSelectedLayer(void) {
    LayerType layer = LAYER_LOW;
    switch(m_pCbbLayerType->GetSelection()) {
    case 0:
        layer = LAYER_HIGH;
        break;
    case 1:
        layer = LAYER_MEDIUM;
        break;
    case 2:
    default:
        break;
    }
    return layer;
}

void DeletePnl::SetCtrlInst(CtrlInst *pCtrlInst) {
    m_pCtrlInst = pCtrlInst;
}

void DeletePnl::UpdateControls(ActionType actionType) {
    m_actionType = actionType;

    GetSizer()->Show(m_pStSetLayer, m_actionType == ACTION_LAYER);
    GetSizer()->Show(m_pCbbLayerType, m_actionType == ACTION_LAYER);

    m_pBtnDelete->Enable(false);
    if(m_actionType == ACTION_LAYER) {
        m_pBtnDelete->SetLabel(wxT("Apply"));
    } else {
        m_pBtnDelete->SetLabel(wxT("Delete"));
    }
    Layout();
    Refresh();
}

void DeletePnl::AddSelectedIndex(int selectedAtomIndex, bool isDoubleClicked) {
    SelectionUtil *pSelUtil = m_pCtrlInst->GetSelectionUtil();
    switch(m_pRbSelectType->GetSelection()) {
    case 0:
        //ATOM_SELECT_SINGLE
        if(m_actionType == ACTION_DELETE_FRAGMENT) {
            if(isDoubleClicked) {
                pSelUtil->ClearSelection();
                m_pCtrlInst->GetRenderingData()->DeleteAtom(selectedAtomIndex, true);
                pSelUtil->RemoveManuSelected(selectedAtomIndex);
                m_pCtrlInst->SetDirty(true);
                MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
            } else {
                pSelUtil->AddManuSelectedIndex(selectedAtomIndex, ATOM_SELECT_SINGLE);
            }
        } else if(m_actionType == ACTION_LAYER) {
            if(!isDoubleClicked) {
                pSelUtil->AddManuSelectedIndex(selectedAtomIndex, ATOM_SELECT_SINGLE);
            }
        }
        break;
    case 1:
        //ATOM_SELECT_TWO_ATOMS;
        if(pSelUtil->AddSelectedIndexLimited(2, selectedAtomIndex, true)) {
            FragmentInfo info = FragSelect(m_pCtrlInst->GetRenderingData()->GetAtomBondArray(), pSelUtil, ATOM_SELECT_TWO_ATOMS,
                                           pSelUtil->GetSelectedIndex(0), pSelUtil->GetSelectedIndex(1));
            if(info == NO_BOND_LINK_TWO_SELECT_ATOMS) {
                Tools::ShowInfo(wxT("No bond between selected atoms."));
                //pSelUtil->RemoveSelected(selectedAtomIndex);
            } else {
                pSelUtil->ClearSelection();
            }
        }

        break;
    case 2:
        //ATOM_SELECT_CONNECTED;
        pSelUtil->AddManuSelectedIndex(selectedAtomIndex, ATOM_SELECT_CONNECTED);
        break;
    default:
        //return ;
        break;
    }
    m_pBtnDelete->Enable(pSelUtil->GetManuSelectedCount() > 0);
}

void DeletePnl::OnBtnDelete(wxCommandEvent &event) {
    int atomIndex = BP_NULL_VALUE;
    int i = 0;
    if(m_actionType == ACTION_LAYER) {
        LayerType layer = GetSelectedLayer();
        AtomBondArray &atomBondarray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        for(i = 0; i < m_pCtrlInst->GetSelectionUtil()->GetManuSelectedCount(); i++) {
            atomIndex = m_pCtrlInst->GetSelectionUtil()->GetManuSelectedIndex(i);
            atomBondarray[atomIndex].atom.layer = layer;
        }
        m_pCtrlInst->GetSelectionUtil()->ClearManuSelection();
        m_pBtnDelete->Enable(false);
        //m_pCtrlInst->GetRenderingData()->DumpRenderingData(m_pCtrlInst->GetRenderingData()->GetAtomBondArray(), EnvUtil::GetLogDir() + wxT("/postionload.txt"));
    } else {
        if(m_pCtrlInst->GetRenderingData()->DeleteAtoms(m_pCtrlInst->GetSelectionUtil())) {
            m_pCtrlInst->GetSelectionUtil()->ClearAll();
            m_pCtrlInst->SetDirty(true);
            MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
        }
    }
    MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

void DeletePnl::OnBtnClear(wxCommandEvent &event) {
    DoClear();
}

void DeletePnl::DoClear(void) {
    if(m_pCtrlInst) {
        m_pCtrlInst->GetSelectionUtil()->ClearManuSelection();
        m_pBtnDelete->Enable(false);
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
    }
}

void DeletePnl::OnBtnSelectAll(wxCommandEvent &event) {
    if(m_pCtrlInst) {
        m_pCtrlInst->GetSelectionUtil()->AddManuSelectedIndex(BP_NULL_VALUE, ATOM_SELECT_ALL);
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        m_pBtnDelete->Enable(m_pCtrlInst->GetSelectionUtil()->GetManuSelectedCount() > 0);
    }
}
