/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "renderingdata.h"
#include "commons.h"

#include <wx/wfstream.h>
#include <wx/txtstrm.h>

#include "tools.h"
#include "fileutil.h"
#include "stringutil.h"

#include "xyzfile.h"
#include "symmetryutils.h"
#include "molviewdata.h"
#include "elempnldata.h"
#include "envutil.h" //SONG add it 1006
#include "resource.h"

#define VIEW_VOLUME_MIN -100000.0f
#define VIEW_VOLUME_MAX 100000.0f

static bool  GetLigandFromAtomBondArray(AtomBondArray &chemArray, ChemLigand &ligand)
{
    unsigned i;
    int j, atomIndex;
    for(i = 0 ; i < chemArray.GetCount(); i++)
    {
        if(chemArray[i].atom.elemId < DEFAULT_HYDROGEN_ID)
        {
            ligand.dummyAtomId = chemArray[i].atom.uniqueId;
            for(j = 0; j < chemArray[i].bondCount; j++)
            {
                atomIndex = chemArray[i].bonds[j].atomIndex;
                if(chemArray[atomIndex].atom.elemId != DEFAULT_HYDROGEN_ID)
                    ligand.linkedAtomIds.Add(chemArray[atomIndex].atom.uniqueId);
            }
            return true;
        }
    }
    return false;
}
/*
static void ResetLigandWithAtomBondArray(AtomBondArray& chemArray,ChemLigand& ligand)
{
        unsigned i;
        int j,atomIndex;
        wxArrayLong linkedIds;
        for(i=0 ; i<chemArray.GetCount(); i++)
        {
                if(chemArray[i].atom.uniqueId==ligand.dummyAtomId)
                {
                        linkedIds.Clear();
                        for(j=0; j< chemArray[i].bondCount;j++)
                        {
                                atomIndex=chemArray[i].bonds[j].atomIndex;
                                linkedIds.Add(chemArray[atomIndex].atom.uniqueId);
                        }
                        for(j=ligand.linkedAtomIds.GetCount()-1; j >=0; j--)
                        {
                                if(linkedIds.Index(ligand.linkedAtomIds[j])==wxNOT_FOUND )
                                {
                                        ligand.linkedAtomIds.Remove(ligand.linkedAtomIds[j]);
                                }
                        }
                        break;
                }
        }
}
*/
RenderingData::RenderingData(MolViewData *molViewData) {
    m_pMolViewData = molViewData;
    Empty();
}

RenderingData::~RenderingData() {
    m_atomBondArray.Clear();
    m_groupViewVolumeArray.Clear();
    m_constrainData.ClearAllData();
}

void RenderingData::Empty() {
    m_atomBondArray.Empty();
    m_groupViewVolumeArray.Empty();
    for(int i = 0; i < ELEM_COUNT; i++) {
        m_atomLabels[i] = 0;
    }
    m_isValidCentroid = false;
    m_connGroupCount = BP_NULL_VALUE;
    m_constrainData.ClearAllData();
}

AtomBondArray &RenderingData::GetAtomBondArray(void) {
    return m_atomBondArray;
}

AtomBondArray RenderingData::CloneAtomBondArray(void) const {
    return m_atomBondArray;
}

int RenderingData::GetAtomCount(void) const {
    return (int)m_atomBondArray.GetCount();
}

AtomBond RenderingData::GetAtomBond(int index) const {
    return m_atomBondArray[index];
}

bool RenderingData::IsEmpty() const {
    return m_atomBondArray.IsEmpty() ;
}

int *RenderingData::GetAtomLabels(void) {
    return m_atomLabels;
}

AtomPosition3FArray &RenderingData::GetGroupViewVolumeArray(void) {
    return m_groupViewVolumeArray;
}

void RenderingData::ReplaceRenderingData(int selectedIndex, AtomBond &chemAtom) {
    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();

    m_atomBondArray[selectedIndex].atom.elemId = chemAtom.atom.elemId;
    m_atomBondArray[selectedIndex].atom.SetDispRadius(chemAtom.atom.GetDispRadius(m_pMolViewData->GetActiveCtrlInst()->modelType));
    //constrainData.UpdateConstrainList(m_atomBondArray);
}
/*
bool RenderingData::LoadFile(const wxString& strFileName) {
    wxString lowerCaseFile = strFileName.Lower();

        if(lowerCaseFile.EndsWith(wxT(".gm"))) {
        GMFile::LoadFile(strFileName, this);
    }else if(lowerCaseFile.EndsWith(wxT(".xyz"))) {
        XYZFile::LoadFile(strFileName, m_atomBondArray, m_atomLabels);
        }else if(lowerCaseFile.EndsWith(wxT(".den"))) {

        }else if(lowerCaseFile.EndsWith(wxT(".vib"))) {

        }else if(lowerCaseFile.EndsWith(wxT(".ir"))) {
        //IRFile::LoadFile(strFileName, m_atomBondArray, atomLabels);
    }else if(lowerCaseFile.EndsWith(wxT(".m2msi"))) {
                //M2MSIFile::IsASCIIFile(strFileName);
        //M2MSIFile::LoadFile(strFileName, m_atomBondArray, atomLabels);
        //surfaceFileName=strFileName;
    }else {
                //if(GMFile::IsGMFile(strFileName)) {
                        //GMFile::LoadFile(strFileName, m_atomBondArray, atomLabels,jobControl,constrainData);
                //} else
        if(XYZFile::IsXYZFile(strFileName)) {
                        XYZFile::LoadFile(strFileName, m_atomBondArray, m_atomLabels);
                }
                //else if(M2MSIFile::IsM2MSIFile(strFileName))
                //{
                        //wxMessageBox(wxT("Load M2Msi File!"));
                //        M2MSIFile::LoadFile(strFileName, m_atomBondArray, atomLabels);
                //        surfaceFileName=strFileName;
                //}
                else {
                        ShowErrorMessage(wxT("Sorry, cannot recognize the file format!"));
                        return false;
                }
    }
    //DumpRenderingData(m_atomBondArray, GetLogDir() + wxT("/gmfile.dat"));
    TagConnectionGroup();
        return true;
}

void RenderingData::SaveFile(const wxString& strFileName) {
    wxString lowerCaseFile = strFileName.Lower();
    if(lowerCaseFile.EndsWith(wxT(".gm"))) {
        GMFile::SaveFile(strFileName, this, true);
    }else if(lowerCaseFile.EndsWith(wxT(".xyz"))) {
        XYZFile::SaveFile(strFileName, m_atomBondArray, false);
    }else {
        GMFile::SaveFile(strFileName, this, true);
    }
}

void RenderingData::SaveHistroyFile(const wxString& tempFileName) {
        GMFile::SaveFile(tempFileName, this, true);
}

void RenderingData::LoadHistroyFile(const wxString& tempFileName) {
        Empty();
        GMFile::LoadFile(tempFileName, this);
    TagConnectionGroup();
}
*/
/**
 * According to the given atomID and fileName, loaded corresponding position file to AtomBondArray
 * if atomid is not a null value , replace the first line with the data of the given atomID
 * The format of a position file
 * line 1: the atom numbers
 * line 2: the atom in the center
 * line 3: the connected point with other moleclus
 * other lines: other atoms
 */
void RenderingData::LoadPositionFile(AtomBondArray &localAtomBondArray, int elemId, const wxString fileName) {
    int fileAtomNum = -1;
    int fileBondNum = -1;
    int atom1, atom2;          //atom1 and atom2 connected with a bond
    short bondType = -1;
    int atomCountBase = 0;     //original atom count before loading file

    localAtomBondArray.Clear();
    wxString fileFullName = fileName;
    ElemInfo *eleInfo = NULL;
    ElemInfo *eleInfoDefaultH = GetElemInfo((ElemId)DEFAULT_HYDROGEN_ID);
    ElemInfo *eleInfoOption = NULL;
    double dist = 0.0;
    double length = 0.0;

    wxFileInputStream fis(fileFullName);
    wxTextInputStream tis(fis);
    tis >> fileAtomNum;

    int i = 0;
    AtomBond atomBond;
    for(i = 0; i < fileAtomNum; i++) {
        atomBond = AtomBond();
        tis >> atomBond.atom.elemId;
        tis >> atomBond.atom.pos.x;
        tis >> atomBond.atom.pos.y;
        tis >> atomBond.atom.pos.z;

        if(i == 0) {
            if(atomBond.atom.elemId == 0 && elemId != BP_NULL_VALUE) {
                atomBond.atom.elemId = elemId;
                eleInfo = GetElemInfo((ElemId)atomBond.atom.elemId);
            }
        }
        atomBond.atom.label = 0;
        if(atomBond.atom.elemId == DEFAULT_HYDROGEN_ID) {
            if(elemId != BP_NULL_VALUE) {
                length = sqrt(atomBond.atom.pos.x * atomBond.atom.pos.x + atomBond.atom.pos.y * atomBond.atom.pos.y + atomBond.atom.pos.z * atomBond.atom.pos.z);
                dist = eleInfo->covalentRadius + eleInfoDefaultH->covalentRadius;
                dist /= length;
                atomBond.atom.pos.x *= dist;
                atomBond.atom.pos.y *= dist;
                atomBond.atom.pos.z *= dist;
            }
        }
        eleInfoOption = GetElemInfo((ElemId)atomBond.atom.elemId);
        atomBond.atom.SetDispRadius(eleInfoOption->dispRadius);
        localAtomBondArray.Add(atomBond);
    }

    tis >> fileBondNum;
    for(i = 0; i < fileBondNum; i++) {
        tis >> atom1;
        tis >> atom2;
        tis >> bondType;
        atom1--;
        atom2--;
        atom1 += atomCountBase;
        atom2 += atomCountBase;

        localAtomBondArray[atom1].bonds[localAtomBondArray[atom1].bondCount].atomIndex = atom2;
        localAtomBondArray[atom1].bonds[localAtomBondArray[atom1].bondCount].bondType = bondType;

        localAtomBondArray[atom2].bonds[localAtomBondArray[atom2].bondCount].atomIndex = atom1;
        localAtomBondArray[atom2].bonds[localAtomBondArray[atom2].bondCount].bondType = bondType;

        localAtomBondArray[atom1].bondCount++;
        localAtomBondArray[atom2].bondCount++;
    }

    //DumpRenderingData(localAtomBondArray, EnvUtil::GetLogDir() + wxT("/postionload.txt"));
}

void RenderingData::DumpRenderingData(AtomBondArray &bondArray, const wxString fileName) {
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
    unsigned int i;
    for(i = 0; i < bondArray.GetCount(); i++) {
        tos << wxString::Format(wxT("%4d Atom %2d_%-4d === "), i, bondArray[i].atom.elemId, bondArray[i].atom.label);
        tos << wxString::Format(wxT("x=%8.2f y=%8.2f z=%8.2f r=%8.2f, connGroup=%d, layer=%d\n"),
                                bondArray[i].atom.pos.x, bondArray[i].atom.pos.y, bondArray[i].atom.pos.z,
                                bondArray[i].atom.GetDispRadius(), bondArray[i].atom.connGroup, bondArray[i].atom.layer);
    }
    tos << wxT("\n====================\n\n");
    for(i = 0; i < bondArray.GetCount(); i++) {
        tos << wxString::Format(wxT("%4d Bond %2d_%-4d count %d === "), i, bondArray[i].atom.elemId, bondArray[i].atom.label, bondArray[i].bondCount);
        for(int j = 0; j < bondArray[i].bondCount; j++) {
            tos << wxString::Format(wxT("%4d"), bondArray[i].bonds[j].atomIndex);
            tos << wxString::Format(wxT("%4d"), bondArray[i].bonds[j].bondType);
        }
        tos << endl;
    }
}

void RenderingData::LoadPreviewData(void) {
    wxString fileName = wxEmptyString;
    if(ElemPnlData::Get()->IsValidElemId() && ElemPnlData::Get()->IsValidBondPos()) {
        fileName.Printf(wxT("position_%d.dll"), ElemPnlData::Get()->GetBondPos());

        //                fileName = FileUtil::CheckFile(wxT("res/mol/dat"), fileName, true);
        fileName = EnvUtil::GetPrgResDir() + wxTRUNK_MOL_DAT_PATH + fileName;

    } else if(ElemPnlData::Get()->IsValidFragment()) {
        fileName = ElemPnlData::Get()->GetFragment() + wxT(".dll");
    }
    if(!StringUtil::IsEmptyString(fileName)) {
        LoadPositionFile(m_atomBondArray, ElemPnlData::Get()->GetElemId(), fileName);
    }
    //CenterMolecule();
    /*if(pGLCanvas)
    {
            pGLCanvas->ResetScaleRatio();
            pGLCanvas->Draw();
    }
    DumpData(chemArray);
    */
}

void RenderingData::ResetUniqueIds(AtomBondArray &chemArray) {
    for(unsigned int i = 0 ; i < chemArray.GetCount(); i++) {
        chemArray[i].atom.ResetUniqueId();
    }
}

void RenderingData::AttachRenderingDataEx(int selHydrogenIndex, AtomBondArray &chemArray) {
    bool hasLigand;
    ChemLigand ligand;

    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();
    ////m_pMolViewData->GetActiveCtrlInst()->ResetOutputList(false);


    if(IsEmpty()) {
        // the current rendering data is empty
        hasLigand = GetLigandFromAtomBondArray(chemArray, ligand);
        m_atomBondArray = chemArray;
    } else {
        ResetUniqueIds(chemArray);
        hasLigand = GetLigandFromAtomBondArray(chemArray, ligand);
        AttachRenderingData(selHydrogenIndex, chemArray);
    }
    TagConnectionGroup();
    if(hasLigand) {
        //wxMessageBox(ligand.ToString());
        m_ligandArray.Add(ligand);
    }
    //constrainData.ClearConstrainData();
    m_constrainData.UpdateConstrainList(m_atomBondArray);
}

void RenderingData::AttachRenderingData(int selHydrogenIndex, AtomBondArray &loadedArray) {
    int connectedSelAtomIndex = m_atomBondArray[selHydrogenIndex].bonds[0].atomIndex;
    float ax0, ax1, ay0, ay1, az0, az1;     /* array to store old molecule */
    float move_x, move_y, move_z;           /* for translation */
    float originx, originy, originz;
    int label_y = 1, label_z = 1;          /* to decide how to ratate-clockwise or counter */

    float acosgama = 0, asingama = 0, acosbeta = 0, asinbeta = 0;
    float bcosgama = 0, bsingama = 0, bcosbeta = 0, bsinbeta = 0;
    /*here these eight variable are used in ratation matrix*/

    float tmpx = 0, tmpy = 0, tmpz = 0;     /* to store the intermidate value */
    int atomCount = loadedArray.GetCount();

    int i, j;
    /* a0, atom in the old to form the new bond */
    ax0 = m_atomBondArray[connectedSelAtomIndex].atom.pos.x;
    ay0 = m_atomBondArray[connectedSelAtomIndex].atom.pos.y;
    az0 = m_atomBondArray[connectedSelAtomIndex].atom.pos.z;

    /* a1, atom in the old to be replaced */
    ax1 = m_atomBondArray[selHydrogenIndex].atom.pos.x;
    ay1 = m_atomBondArray[selHydrogenIndex].atom.pos.y;
    az1 = m_atomBondArray[selHydrogenIndex].atom.pos.z;
    /* loadedArray[1] is the one to be replaced;  b1
     * loadedArray[0] is the one form new bond    b0
     */

    /* the first step:move the new molecule to the a0, and move a0 and b1 to the origin */
    move_x = ax0 - loadedArray[1].atom.pos.x;
    move_y = ay0 - loadedArray[1].atom.pos.y;
    move_z = az0 - loadedArray[1].atom.pos.z;
    originx = ax0;
    originy = ay0;
    originz = az0;
    ax1 -= originx;
    ay1 -= originy;
    az1 -= originz;
    ax0 -= originx;
    ay0 -= originy;
    az0 -= originz;

    for(i = 0; i < atomCount; i++) {
        loadedArray[i].atom.pos.x += move_x - originx;
        loadedArray[i].atom.pos.y += move_y - originy;
        loadedArray[i].atom.pos.z += move_z - originz;
    }

    /* second:rotate around the z; move vec of a0 and a1 to the xz plane */
    if (fabs(ax1) < BP_PREC && fabs(ay1) < BP_PREC) {
        label_z = 0; //a0 and a1 are on the z axis, so neglect the following step
    } else {
        acosgama = ax1 / sqrt(ax1 * ax1 + ay1 * ay1);
        asingama = sqrt(1 - acosgama * acosgama);
        if (ay1 >= 0) {
            /* clockwise */
            label_z = -1;
            tmpx = acosgama * ax1 + asingama * ay1;
            tmpy = -asingama * ax1 + acosgama * ay1;
            ax1 = tmpx;
            ay1 = tmpy;
            for (i = 0; i < atomCount; i++) {
                tmpx =  acosgama * loadedArray[i].atom.pos.x  + asingama * loadedArray[i].atom.pos.y ;
                tmpy = -asingama * loadedArray[i].atom.pos.x  + acosgama * loadedArray[i].atom.pos.y ;
                loadedArray[i].atom.pos.x  = tmpx;
                loadedArray[i].atom.pos.y  = tmpy;
            }
        } else { /* counter-clockwise */
            tmpx =  acosgama * ax1 - asingama * ay1;
            tmpy =  asingama * ax1 + acosgama * ay1;
            ax1 = tmpx;
            ay1 = tmpy;
            for (i = 0; i < atomCount; i++) {
                tmpx = acosgama * loadedArray[i].atom.pos.x  - asingama * loadedArray[i].atom.pos.y ;
                tmpy =  asingama * loadedArray[i].atom.pos.x  + acosgama * loadedArray[i].atom.pos.y ;
                loadedArray[i].atom.pos.x  = tmpx;
                loadedArray[i].atom.pos.y  = tmpy;
            }
        } /* ay1 should be 0 */
    }

    /* third: rotate around the y, make a0a1 to the x axis */
    acosbeta = ax1 / sqrt (ax1 * ax1 + az1 * az1);
    asinbeta = sqrt (1 - acosbeta * acosbeta);
    if (az1 >= 0)  /* counter-clockwise */
    {
        label_y =  1;
        tmpx =  acosbeta * ax1 + asinbeta * az1;
        tmpz = -asinbeta * ax1 + acosbeta * az1;
        ax1 = tmpx;
        az1 = tmpz;
        for (i = 0; i < atomCount; i++) {
            tmpx =  acosbeta * loadedArray[i].atom.pos.x + asinbeta * loadedArray[i].atom.pos.z;
            tmpz = -asinbeta * loadedArray[i].atom.pos.x + acosbeta * loadedArray[i].atom.pos.z;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.z = tmpz;
        }
    } else {        /* clockwise */
        label_y = -1;
        tmpx =  acosbeta * ax1 - asinbeta * az1;
        tmpz =  asinbeta * ax1 + acosbeta * az1;
        ax1 = tmpx;
        az1 = tmpz;
        for (i = 0; i < atomCount; i++) {
            tmpx =  acosbeta * loadedArray[i].atom.pos.x - asinbeta * loadedArray[i].atom.pos.z;
            tmpz =  asinbeta * loadedArray[i].atom.pos.x + acosbeta * loadedArray[i].atom.pos.z;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.z = tmpz;
        }
    }
    /* here az1 should be 0 */

    /* fourth:rotate around the z, make the first bond of new molecule to the xz plane */
    if (( fabs(loadedArray[0].atom.pos.x) < BP_PREC) && (fabs(loadedArray[0].atom.pos.y) < BP_PREC)) {
        ;
    } else {
        bcosgama = loadedArray[0].atom.pos.x / sqrt (loadedArray[0].atom.pos.x * loadedArray[0].atom.pos.x + loadedArray[0].atom.pos.y * loadedArray[0].atom.pos.y);
        bsingama = sqrt (1 - bcosgama * bcosgama);
        if (loadedArray[0].atom.pos.y >= 0) /* clockwise */
        {
            for (i = 0; i < atomCount; i++) {
                tmpx =  bcosgama * loadedArray[i].atom.pos.x + bsingama * loadedArray[i].atom.pos.y;
                tmpy = -bsingama * loadedArray[i].atom.pos.x + bcosgama * loadedArray[i].atom.pos.y;
                loadedArray[i].atom.pos.x = tmpx;
                loadedArray[i].atom.pos.y = tmpy;
            }
        } else { /* counter-clockwise */
            for (i = 0; i < atomCount; i++) {
                tmpx =  bcosgama * loadedArray[i].atom.pos.x - bsingama * loadedArray[i].atom.pos.y;
                tmpy =  bsingama * loadedArray[i].atom.pos.x + bcosgama * loadedArray[i].atom.pos.y;
                loadedArray[i].atom.pos.x = tmpx;
                loadedArray[i].atom.pos.y = tmpy;
            }
        }
    }

    /* fiveth:rotate around y axis, move the first bond of new molecule to the x axis, same position with a0a1 */
    bcosbeta = loadedArray[0].atom.pos.x / sqrt (loadedArray[0].atom.pos.x * loadedArray[0].atom.pos.x + loadedArray[0].atom.pos.z * loadedArray[0].atom.pos.z);
    bsinbeta = sqrt(1 - bcosbeta * bcosbeta);
    if (loadedArray[0].atom.pos.z >= 0)  /* counter-clockwise */
    {
        for (i = 0; i < atomCount; i++) {
            tmpx =  bcosbeta * loadedArray[i].atom.pos.x + bsinbeta * loadedArray[i].atom.pos.z;
            tmpz = -bsinbeta * loadedArray[i].atom.pos.x + bcosbeta * loadedArray[i].atom.pos.z;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.z = tmpz;
        }
    } else    /* clockwise */
    {
        for (i = 0; i < atomCount; i++) {
            tmpx =  bcosbeta * loadedArray[i].atom.pos.x - bsinbeta * loadedArray[i].atom.pos.z;
            tmpz =  bsinbeta * loadedArray[i].atom.pos.x + bcosbeta * loadedArray[i].atom.pos.z;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.z = tmpz;
        }
    }

    /*sixth: reverse the operation given by a0a1, and move them back */
    for (i = 0; i < atomCount; i++) {
        if (label_y ==  1)   /* counter-clockwise */
        {
            tmpx =  acosbeta * loadedArray[i].atom.pos.x - asinbeta * loadedArray[i].atom.pos.z;
            tmpz =  asinbeta * loadedArray[i].atom.pos.x + acosbeta * loadedArray[i].atom.pos.z;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.z = tmpz;
        } else    /* clockwise */
        {
            tmpx =  acosbeta * loadedArray[i].atom.pos.x + asinbeta * loadedArray[i].atom.pos.z;
            tmpz = -asinbeta * loadedArray[i].atom.pos.x + acosbeta * loadedArray[i].atom.pos.z;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.z = tmpz;
        }

        if (label_z == 1)   /* counter-clockwise */
        {
            tmpx =  acosgama * loadedArray[i].atom.pos.x + asingama * loadedArray[i].atom.pos.y;
            tmpy = -asingama * loadedArray[i].atom.pos.x + acosgama * loadedArray[i].atom.pos.y;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.y = tmpy;
        } else  if (label_z == -1)   /* clockwise */
        {
            tmpx =  acosgama * loadedArray[i].atom.pos.x - asingama * loadedArray[i].atom.pos.y;
            tmpy =  asingama * loadedArray[i].atom.pos.x + acosgama * loadedArray[i].atom.pos.y;
            loadedArray[i].atom.pos.x = tmpx;
            loadedArray[i].atom.pos.y = tmpy;
        }

        loadedArray[i].atom.pos.x += originx;
        loadedArray[i].atom.pos.y += originy;
        loadedArray[i].atom.pos.z += originz;
    }

    /*seventh: extend the first bond of new molecule to the proper bond lengh */
    float lamda = sqrt (pow(loadedArray[0].atom.pos.x - loadedArray[1].atom.pos.x , 2)
                        + pow(loadedArray[0].atom.pos.y - loadedArray[1].atom.pos.y , 2)
                        + pow(loadedArray[0].atom.pos.z - loadedArray[1].atom.pos.z , 2));

    float bond_length = 0.0;
    bond_length += GetElemInfo((ElemId)loadedArray[0].atom.elemId)->covalentRadius;
    bond_length += GetElemInfo((ElemId)m_atomBondArray[connectedSelAtomIndex].atom.elemId)->covalentRadius;

    tmpx = loadedArray[1].atom.pos.x + bond_length / lamda * (loadedArray[0].atom.pos.x - loadedArray[1].atom.pos.x);
    tmpy = loadedArray[1].atom.pos.y + bond_length / lamda * (loadedArray[0].atom.pos.y - loadedArray[1].atom.pos.y);
    tmpz = loadedArray[1].atom.pos.z + bond_length / lamda * (loadedArray[0].atom.pos.z - loadedArray[1].atom.pos.z);
    move_x = tmpx - loadedArray[0].atom.pos.x ;
    move_y = tmpy - loadedArray[0].atom.pos.y;
    move_z = tmpz - loadedArray[0].atom.pos.z ;
    loadedArray[0].atom.pos.x = tmpx;
    loadedArray[0].atom.pos.y = tmpy;
    loadedArray[0].atom.pos.z = tmpz;

    for (i = 2; i < atomCount; i++) {
        loadedArray[i].atom.pos.x += move_x;
        loadedArray[i].atom.pos.y += move_y;
        loadedArray[i].atom.pos.z += move_z;
    }

    /* added the loaded data to the rendering data*/
    //RenderingData::DumpRenderingData(this->GetAtomBondArray(), GetLogDir() + wxT("/dumprenderingdata10.dat"));
    int curAtomCount = m_atomBondArray.GetCount();
    for(i = 0; i < curAtomCount; i++) {
        for(j = 0; j < m_atomBondArray[i].bondCount; j++) {
            if(m_atomBondArray[i].bonds[j].atomIndex > selHydrogenIndex) {
                m_atomBondArray[i].bonds[j].atomIndex--;
            } else if(m_atomBondArray[i].bonds[j].atomIndex == selHydrogenIndex) {
                m_atomBondArray[i].bonds[j].atomIndex = curAtomCount - 1;
            }
        }
    }
    m_atomBondArray.RemoveAt(selHydrogenIndex);
    curAtomCount--;
    //RenderingData::DumpRenderingData(this->GetAtomBondArray(), GetLogDir() + wxT("/dumprenderingdata11.dat"));

    if(connectedSelAtomIndex > selHydrogenIndex) {
        connectedSelAtomIndex --;
    }
    // remove loadedArray[1], the one to be replaced;
    //loadedArray.RemoveAt(1);
    loadedArray.Detach(1);
    for(i = 0; i < atomCount - 1; i++) {
        loadedArray[i].atom.label = ++m_atomLabels[ELEM_PREFIX + loadedArray[i].atom.elemId];

        for(int j = 0; j < loadedArray[i].bondCount; j++) {
            //if(loadedArray[i].bondCount == 0) {
            //        ShowInfoMessage(wxT("bond error"));
            //        break;
            //}

            if(loadedArray[i].bonds[j].atomIndex == 0) {
                loadedArray[i].bonds[j].atomIndex = curAtomCount;
            } else if(loadedArray[i].bonds[j].atomIndex == 1) {
                loadedArray[i].bonds[j].atomIndex = connectedSelAtomIndex;
            } else if(loadedArray[i].bonds[j].atomIndex > 1) {
                loadedArray[i].bonds[j].atomIndex += curAtomCount - 1;
            }
        }
        m_atomBondArray.Add(loadedArray[i]);
    }
}

void RenderingData::AttachRenderingDataEx(float xWorld, float yWorld, float zWorld, AtomBondArray &chemArray) {
    unsigned i;
    int j;
    bool hasLigand = false;
    ChemLigand ligand;

    AtomPosition3F posAttached;
    posAttached.x = xWorld;
    posAttached.y = yWorld;
    posAttached.z = 0.0f;

    for(i = 0; i < chemArray.GetCount(); i++) {
        ChemAtom::Translate(chemArray[i].atom, posAttached);
    }

    int connGroupCount = m_groupViewVolumeArray.GetCount() / 2;
    if(connGroupCount < 0) connGroupCount = 0;
    int atomCountBase = m_atomBondArray.GetCount();        //original atom count before loading this gm file
    if(atomCountBase < 0) atomCountBase = 0;

    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();
    ////m_pMolViewData->GetActiveCtrlInst()->ResetOutputList(false);

    hasLigand = GetLigandFromAtomBondArray(chemArray, ligand);

    for(i = 0; i < chemArray.GetCount(); i++) {
        for(j = 0; j < chemArray[i].bondCount; j++) {
            chemArray[i].bonds[j].atomIndex += atomCountBase;
        }
        m_atomBondArray.Add(chemArray[i]);
    }
    TagConnectionGroup();
    if(hasLigand) {
        //wxMessageBox(ligand.ToString());
        m_ligandArray.Add(ligand);
    }
    m_constrainData.UpdateConstrainList(m_atomBondArray);

    for(i = connGroupCount; i < m_groupViewVolumeArray.GetCount() / 2; i++) {
        //TranslateConnectionGroup(i, posAttached);
    }
}

bool RenderingData::ChangeBondType(int atomIndex[2], BondType bondType) {
    int i, j;
    bool chgbond = false;
    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();
    
    for(i = 0; i < 2; i++) {
        for(j = 0; j < m_atomBondArray[atomIndex[i]].bondCount; j++) {
            if(m_atomBondArray[atomIndex[i]].bonds[j].atomIndex == atomIndex[(i + 1) % 2]) {
                if(bondType == BOND_TYPE_0) {
                    for(int k = j; k < m_atomBondArray[atomIndex[i]].bondCount - 1; k++) {
                        m_atomBondArray[atomIndex[i]].bonds[k].atomIndex = m_atomBondArray[atomIndex[i]].bonds[k + 1].atomIndex;
                        m_atomBondArray[atomIndex[i]].bonds[k].bondType = m_atomBondArray[atomIndex[i]].bonds[k + 1].bondType;
                    }
                    m_atomBondArray[atomIndex[i]].bondCount--;
                } else
                    m_atomBondArray[atomIndex[i]].bonds[j].bondType = bondType;
                chgbond = true;
                break;
            }
        }
    }
    return chgbond;
}

void RenderingData::TagConnectionGroup(void) {
    int i;
    int end = (int)m_atomBondArray.GetCount();
    if(end < 0) end = 0;


    for(i = 0; i < ELEM_COUNT; i++) {
        m_atomLabels[i] = 0;
    }

    //        if(!m_groupViewVolumeArray.IsEmpty()) {
    //                m_groupViewVolumeArray.Clear();
    //        }
    m_groupViewVolumeArray.Clear();
    //DumpRenderingData(m_atomBondArray, EnvUtil::GetLogDir() + wxT("/beforecon0.txt"));
    for(i = 0; i < end; i++) {
        m_atomBondArray[i].atom.connGroup = BP_NULL_VALUE;
        int tmplabel = ++m_atomLabels[ELEM_PREFIX + m_atomBondArray[i].atom.elemId];
        m_atomBondArray[i].atom.label = tmplabel;
    }

    m_connGroupCount = 0;
    //DumpRenderingData(m_atomBondArray, EnvUtil::GetLogDir() + wxT("/beforecon.txt"));
    AtomPosition3F cubeMin, cubeMax;


    for(i = 0; i < end; i++) {
        if(BP_NULL_VALUE == m_atomBondArray[i].atom.connGroup) {
            cubeMin = AtomPosition3F();
            cubeMin.x = cubeMin.y = cubeMin.z = VIEW_VOLUME_MAX;
            cubeMax = AtomPosition3F();
            cubeMax.x = cubeMax.y = cubeMax.z = VIEW_VOLUME_MIN;
            m_groupViewVolumeArray.Add(cubeMin);
            m_groupViewVolumeArray.Add(cubeMax);

            TagConnectionGroup(i, m_connGroupCount);
            m_connGroupCount++;
        }
    }
}

void RenderingData::TagConnectionGroup(int atomIndex, int connGroupIndex) {
    int j;

    if(BP_NULL_VALUE != m_atomBondArray[atomIndex].atom.connGroup) {
        if(connGroupIndex != m_atomBondArray[atomIndex].atom.connGroup) {
            Tools::ShowError(wxT("Error in tag connection group."));
        }
        return;
    }

    m_atomBondArray[atomIndex].atom.connGroup = connGroupIndex;
    RenewGroupViewVolume(atomIndex, connGroupIndex);
    for(j = 0; j < m_atomBondArray[atomIndex].bondCount; j++) {
        TagConnectionGroup(m_atomBondArray[atomIndex].bonds[j].atomIndex, connGroupIndex);
    }
}

void RenderingData::RenewGroupViewVolume(int atomIndex, int connGroupIndex) {
    ChemAtom atom = m_atomBondArray[atomIndex].atom;

    m_groupViewVolumeArray[connGroupIndex * 2].x = MathUtil::GetMinValue(m_groupViewVolumeArray[connGroupIndex * 2].x, atom.pos.x - atom.GetDispRadius());
    m_groupViewVolumeArray[connGroupIndex * 2].y = MathUtil::GetMinValue(m_groupViewVolumeArray[connGroupIndex * 2].y, atom.pos.y - atom.GetDispRadius());
    m_groupViewVolumeArray[connGroupIndex * 2].z = MathUtil::GetMinValue(m_groupViewVolumeArray[connGroupIndex * 2].z, atom.pos.z - atom.GetDispRadius());
    m_groupViewVolumeArray[connGroupIndex * 2 + 1].x = MathUtil::GetMaxValue(m_groupViewVolumeArray[connGroupIndex * 2 + 1].x, atom.pos.x + atom.GetDispRadius());
    m_groupViewVolumeArray[connGroupIndex * 2 + 1].y = MathUtil::GetMaxValue(m_groupViewVolumeArray[connGroupIndex * 2 + 1].y, atom.pos.y + atom.GetDispRadius());
    m_groupViewVolumeArray[connGroupIndex * 2 + 1].z = MathUtil::GetMaxValue(m_groupViewVolumeArray[connGroupIndex * 2 + 1].z, atom.pos.z + atom.GetDispRadius());
}

/*
void RenderingData::TranslateConnectionGroup(int groupId, AtomPosition3F posTrans) {
        unsigned i;
        ClearGroupCentroid();
        for(i = 0; i < m_atomBondArray.GetCount(); i++) {
                if(m_atomBondArray[i].atom.connGroup == groupId) {
                        ChemAtom::Translate(m_atomBondArray[i].atom, posTrans);
                }
        }
        if(m_groupViewVolumeArray.GetCount() >= (unsigned)((groupId + 1) * 2)) {
                ChemAtom::Translate(m_groupViewVolumeArray[groupId*2], posTrans);
                ChemAtom::Translate(m_groupViewVolumeArray[groupId*2+1], posTrans);
        }
}*/

void RenderingData::TranslateConnectionGroup(int groupId, AtomPosition3F posTrans) {
    unsigned i;
    ClearGroupCentroid();

    for(i = 0; i < m_atomBondArray.GetCount(); i++) {
        if(m_atomBondArray[i].atom.connGroup == groupId) {
            ChemAtom::Translate(m_atomBondArray[i].atom, posTrans);
        }
    }
    if(m_groupViewVolumeArray.GetCount() >= (unsigned)((groupId + 1) * 2)) {
        ChemAtom::Translate(m_groupViewVolumeArray[groupId * 2], posTrans);
        ChemAtom::Translate(m_groupViewVolumeArray[groupId * 2 + 1], posTrans);
    }
}

void RenderingData::RotateConnectionGroup(int groupId, AtomPosition3F center, Matrix4x4 camera) {
    unsigned i;

    for(i = 0; i < m_groupViewVolumeArray.GetCount() / 2; i++) {
        if(BP_NULL_VALUE == groupId || i == (unsigned)groupId) {
            m_groupViewVolumeArray[i * 2].x = VIEW_VOLUME_MAX;
            m_groupViewVolumeArray[i * 2].y = VIEW_VOLUME_MAX;
            m_groupViewVolumeArray[i * 2].z = VIEW_VOLUME_MAX;
            m_groupViewVolumeArray[i * 2 + 1].x = VIEW_VOLUME_MIN;
            m_groupViewVolumeArray[i * 2 + 1].y = VIEW_VOLUME_MIN;
            m_groupViewVolumeArray[i * 2 + 1].z = VIEW_VOLUME_MIN;
        }
    }

    for(i = 0; i < m_atomBondArray.GetCount(); i++) {
        if(BP_NULL_VALUE == groupId || m_atomBondArray[i].atom.connGroup == groupId) {
            ChemAtom::Translate(m_atomBondArray[i].atom, -center.x, -center.y, -center.z);
            m_atomBondArray[i].atom.pos = camera.Multiply(m_atomBondArray[i].atom.pos);
            ChemAtom::Translate(m_atomBondArray[i].atom, center);
            RenewGroupViewVolume(i, m_atomBondArray[i].atom.connGroup);
        }
    }
    ClearGroupCentroid();
}

/*void RenderingData::RotateConnectionGroup(int groupId, AtomPosition3F rotAngle) {
        unsigned i;
        if(!m_isValidCentroid) {
                m_posCentroid = GetGroupCentroid(groupId);
                m_isValidCentroid = true;
        }

        for(i = 0; i < m_groupViewVolumeArray.GetCount() / 2; i++) {
                if(BP_NULL_VALUE == groupId || i == (unsigned)groupId) {
                        m_groupViewVolumeArray[i*2].x = VIEW_VOLUME_MAX;
                        m_groupViewVolumeArray[i*2].y = VIEW_VOLUME_MAX;
                        m_groupViewVolumeArray[i*2].z = VIEW_VOLUME_MAX;
                        m_groupViewVolumeArray[i*2+1].x = VIEW_VOLUME_MIN;
                        m_groupViewVolumeArray[i*2+1].y = VIEW_VOLUME_MIN;
                        m_groupViewVolumeArray[i*2+1].z = VIEW_VOLUME_MIN;
                }
        }

        for(i = 0; i < m_atomBondArray.GetCount(); i++) {
                if(BP_NULL_VALUE == groupId || m_atomBondArray[i].atom.connGroup == groupId) {
                        ChemAtom::Translate(m_atomBondArray[i].atom, -m_posCentroid.x, -m_posCentroid.y, -m_posCentroid.z);
                        ChemAtom::RotateWithX(m_atomBondArray[i].atom, rotAngle.x);
                        ChemAtom::RotateWithY(m_atomBondArray[i].atom, rotAngle.y);
                        ChemAtom::RotateWithZ(m_atomBondArray[i].atom, rotAngle.z);
                        ChemAtom::Translate(m_atomBondArray[i].atom, m_posCentroid);

                        RenewGroupViewVolume(i, m_atomBondArray[i].atom.connGroup);
                }
        }
        ClearGroupCentroid();
}
*/

void RenderingData::TranslateAtomsAxis(int atomIndex, int axisIndex, double offset) {
    unsigned i;
    AtomPosition3F posTrans;
    posTrans.x = 0;
    posTrans.y = 0;
    posTrans.z = 0;
    switch(axisIndex) {
    case 0:
        posTrans.x = offset;
        break;
    case 1:
        posTrans.y = offset;
        break;
    case 2:
        posTrans.z = offset;
        break;
    default:
        break;
    }
    if(atomIndex == BP_NULL_VALUE) {
        for(i = 0; i < m_atomBondArray.GetCount(); i++) {
            ChemAtom::Translate(m_atomBondArray[i].atom, posTrans);
        }
    } else {
        ChemAtom::Translate(m_atomBondArray[atomIndex].atom, posTrans);
    }
    ClearGroupCentroid();
}

bool RenderingData::AddBond(int atomIndex[2], BondType bondtype) {
    if(m_atomBondArray[atomIndex[0]].bondCount >= ATOM_MAX_BOND_COUNT || m_atomBondArray[atomIndex[1]].bondCount >= ATOM_MAX_BOND_COUNT)
        return false;

    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();

    m_atomBondArray[atomIndex[0]].bonds[m_atomBondArray[atomIndex[0]].bondCount].atomIndex = atomIndex[1];
    m_atomBondArray[atomIndex[0]].bonds[m_atomBondArray[atomIndex[0]].bondCount].bondType = bondtype;

    m_atomBondArray[atomIndex[1]].bonds[m_atomBondArray[atomIndex[1]].bondCount].atomIndex = atomIndex[0];
    m_atomBondArray[atomIndex[1]].bonds[m_atomBondArray[atomIndex[1]].bondCount].bondType = bondtype;

    m_atomBondArray[atomIndex[0]].bondCount++;
    m_atomBondArray[atomIndex[1]].bondCount++;
    return true;
}

bool RenderingData::MakeBond(int atomIndex[2]) {
    int i, j;
    int connectedIndex[2], bondCount;
    short bondIndexWithH[2], bondIndexWithAtom[2];
    bool tagConn = false;
    if(m_atomBondArray[atomIndex[0]].atom.connGroup != m_atomBondArray[atomIndex[1]].atom.connGroup) tagConn = true;

    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();

    connectedIndex[0] = m_atomBondArray[atomIndex[0]].bonds[0].atomIndex;
    connectedIndex[1] = m_atomBondArray[atomIndex[1]].bonds[0].atomIndex;

    for(i = 0; i < 2; i++) {
        bondIndexWithH[i] = -1;
        bondIndexWithAtom[i] = -1;

        bondCount = m_atomBondArray[connectedIndex[i]].bondCount;
        for(j = 0; j <  bondCount; j++) {
            if(m_atomBondArray[connectedIndex[i]].bonds[j].atomIndex == atomIndex[i]) {
                bondIndexWithH[i] = j;
            } else if(m_atomBondArray[connectedIndex[i]].bonds[j].atomIndex == connectedIndex[(i + 1) % 2]) {
                bondIndexWithAtom[i] = j;
                if(m_atomBondArray[connectedIndex[i]].bonds[j].bondType == BOND_TYPE_5)
                    return false;
            }
        }
    }

    for(i = 0; i < 2; i++) {
        if(bondIndexWithAtom[i] > bondIndexWithH[i]) {
            bondIndexWithAtom[i]--;
        }
        if(connectedIndex[i] > atomIndex[0]) connectedIndex[i]--;
        if(connectedIndex[i] > atomIndex[1]) connectedIndex[i]--;
    }
    RemoveArrayElement(m_atomBondArray, atomIndex[0], BP_NULL_VALUE);
    if(atomIndex[1] > atomIndex[0])
        atomIndex[1]--;
    //DumpRenderingData(m_atomBondArray, wxT(GetLogDir() + "/makebond0.dat"));
    RemoveArrayElement(m_atomBondArray, atomIndex[1], BP_NULL_VALUE);
    //DumpRenderingData(m_atomBondArray, wxT(GetLogDir() + "/makebond1.dat"));

    for(i = 0; i < 2; i++) {
        bondCount = m_atomBondArray[connectedIndex[i]].bondCount;
        if(bondIndexWithAtom[i] >= 0) {
            m_atomBondArray[connectedIndex[i]].bonds[bondIndexWithAtom[i]].ChangeBondTypeToNext();
        } else {
            m_atomBondArray[connectedIndex[i]].bonds[bondCount].atomIndex = connectedIndex[(i + 1) % 2];
            m_atomBondArray[connectedIndex[i]].bonds[bondCount].ChangeBondTypeToNext();
            m_atomBondArray[connectedIndex[i]].bondCount++;
        }
    }

    if(tagConn)
        TagConnectionGroup();
    //DumpRenderingData(m_atomBondArray, wxT(GetLogDir() + "/makebond.dat"));
    return true;
}

void RenderingData::RemoveArrayElement(AtomBondArray &bondArray, int removeIndex, int replaceIndex) {
    unsigned i;
    int j, k;

    if( removeIndex < 0 || removeIndex > (int)bondArray.GetCount()) return;

    for(i = 0; i < bondArray.GetCount(); i++) {
        for(j = 0; j < bondArray[i].bondCount; j++) {
            if(bondArray[i].bonds[j].atomIndex > removeIndex) {
                bondArray[i].bonds[j].atomIndex--;
            } else if(bondArray[i].bonds[j].atomIndex == removeIndex) {
                if(BP_NULL_VALUE != replaceIndex) {
                    bondArray[i].bonds[j].atomIndex = replaceIndex;
                } else {
                    bondArray[i].bondCount--;
                    for(k = j; k < bondArray[i].bondCount; k++) {
                        bondArray[i].bonds[k] = bondArray[i].bonds[k + 1];
                        if(bondArray[i].bonds[k].atomIndex > removeIndex) {
                            bondArray[i].bonds[k].atomIndex--;
                        }
                    }
                    bondArray[i].bonds[k].Clear();
                    break;
                }
            }
        }
    }
    bondArray.Detach(removeIndex);
}

void RenderingData::ClearGroupCentroid(void) {
    m_isValidCentroid = false;
}

int RenderingData::GetConnectionGroupCount(void) const {
    return m_connGroupCount;
}

int RenderingData::GetAtomIndex(long uniqueId) {
    int count = (int)m_atomBondArray.GetCount();
    for(int i = 0; i < count; i++) {
        if(m_atomBondArray[i].atom.uniqueId == uniqueId) {
            return i;
        }
    }
    return BP_NULL_VALUE;
}

bool RenderingData::DeleteAtoms(SelectionUtil *pSelUtil) {
    int i = 0;
    int atomIndex = BP_NULL_VALUE;
    bool isDeleted = false;
    int selectedCount = pSelUtil->GetManuSelectedCount();
    if(selectedCount > 0) {
        long *longAtomIds = (long *)malloc(sizeof(long) * selectedCount);
        if(longAtomIds != NULL) {
            for(i = 0; i < selectedCount; i++) {
                longAtomIds[i] = m_atomBondArray[pSelUtil->GetManuSelectedIndex(i)].atom.uniqueId;
            }
            for(i = 0; i < selectedCount; i++) {
                atomIndex = GetAtomIndex(longAtomIds[i]);
                if(atomIndex != BP_NULL_VALUE) {
                    DeleteAtom(atomIndex, false);
                }
            }
            free(longAtomIds);
            TagConnectionGroup();
            isDeleted = true;
        }
    }
    return isDeleted;
}

void RenderingData::DeleteAtom(int atomIndex, bool tagConn) {
    int i;
    int aryAtomIndex[2];

    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();

    if(m_atomBondArray[atomIndex].atom.elemId == DEFAULT_HYDROGEN_ID || m_atomBondArray[atomIndex].atom.elemId == DUMMY_ATOM_ID) {
        RemoveArrayElement(m_atomBondArray, atomIndex, BP_NULL_VALUE);
    } else {
        //break bond connection
        aryAtomIndex[0] = atomIndex;

        for(i = 0; i < m_atomBondArray[atomIndex].bondCount; i++) {
            aryAtomIndex[1] = m_atomBondArray[atomIndex].bonds[i].atomIndex;
            if(m_atomBondArray[aryAtomIndex[1]].atom.elemId != DEFAULT_HYDROGEN_ID) {
                BreakBond(aryAtomIndex, false);
            }
        }
        //delete all atoms connected with the selected atom
        while(m_atomBondArray[atomIndex].bondCount > 0) {
            i = m_atomBondArray[atomIndex].bonds[0].atomIndex;
            RemoveArrayElement(m_atomBondArray, i, BP_NULL_VALUE);
            if(atomIndex > i)
                atomIndex--;
        }
        RemoveArrayElement(m_atomBondArray, atomIndex, BP_NULL_VALUE);
        if(tagConn) {
            TagConnectionGroup();
        }
    }
}

bool RenderingData::BreakBond(int atomIndex[2], bool tagConn) {
    ElemInfo *eleInfo = NULL;
    int i, j;

    m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();

    if(DEFAULT_HYDROGEN_ID == m_atomBondArray[atomIndex[0]].atom.elemId || DEFAULT_HYDROGEN_ID == m_atomBondArray[atomIndex[1]].atom.elemId
            || DUMMY_ATOM_ID == m_atomBondArray[atomIndex[0]].atom.elemId || DUMMY_ATOM_ID == m_atomBondArray[atomIndex[1]].atom.elemId) {
        Tools::ShowError(wxT("Free valences cannot be broken."));
        return false;
    }

    eleInfo = GetElemInfo((ElemId)DEFAULT_HYDROGEN_ID);
    for(i = 0; i < 2; i++) {
        AtomBond atomBond = AtomBond();
        atomBond.atom.elemId = DEFAULT_HYDROGEN_ID;
        GetHydrogenPosition(atomIndex[i], atomIndex[(i + 1) % 2], atomBond.atom.pos);
        atomBond.atom.label = ++m_atomLabels[ELEM_PREFIX + DEFAULT_HYDROGEN_ID];
        atomBond.atom.SetDispRadius(eleInfo->dispRadius);
        atomBond.bondCount = 1;
        atomBond.bonds[0].atomIndex = atomIndex[i];
        atomBond.bonds[0].dispType = BP_NULL_VALUE;

        for(j = 0; j < m_atomBondArray[atomIndex[i]].bondCount; j++) {
            if(m_atomBondArray[atomIndex[i]].bonds[j].atomIndex == atomIndex[(i + 1) % 2]) {
                atomBond.bonds[0].bondType = m_atomBondArray[atomIndex[i]].bonds[j].bondType;
                m_atomBondArray[atomIndex[i]].bonds[j].atomIndex = m_atomBondArray.GetCount();
                break;
            }
        }
        m_atomBondArray.Add(atomBond);
    }

    if(tagConn)
        TagConnectionGroup();
    return true;
}

/**
 * use default hydrogen replace atom2Index,
 * calculate the hydrogen's position
 */
void RenderingData::GetHydrogenPosition(int atom1Index, int atom2Index, AtomPosition &pos) {
    double px1, px2, py1, py2, pz1, pz2;
    double oldDistance;

    ElemInfo *eleInfo = GetElemInfo((ElemId)m_atomBondArray[atom1Index].atom.elemId);
    float newDistance = eleInfo->covalentRadius;

    eleInfo = GetElemInfo(ELEM_H);
    newDistance += eleInfo->covalentRadius;

    px1 = m_atomBondArray[atom1Index].atom.pos.x;
    py1 = m_atomBondArray[atom1Index].atom.pos.y;
    pz1 = m_atomBondArray[atom1Index].atom.pos.z;
    px2 = m_atomBondArray[atom2Index].atom.pos.x;
    py2 = m_atomBondArray[atom2Index].atom.pos.y;
    pz2 = m_atomBondArray[atom2Index].atom.pos.z;

    oldDistance = sqrt(pow(px1 - px2, 2) + pow(py1 - py2, 2) + pow(pz1 - pz2, 2));
    pos.x = px1 + (px2 - px1) * newDistance / oldDistance;
    pos.y = py1 + (py2 - py1) * newDistance / oldDistance;
    pos.z = pz1 + (pz2 - pz1) * newDistance / oldDistance;

}

bool RenderingData::DefineDummy(SelectionUtil *pSelUtil) {
    bool isAdd = false;
    int i = 0, j = 0;
    if(pSelUtil->GetSelectedCount() > 1) {
        m_pMolViewData->GetActiveCtrlInst()->SaveHistroyFile();
        ElemInfo *eleInfo = GetElemInfo((ElemId)DUMMY_ATOM_ID);

        AtomBond atomBond = AtomBond();
        atomBond.atom.elemId = DUMMY_ATOM_ID;
        atomBond.atom.label = ++m_atomLabels[ELEM_PREFIX + DUMMY_ATOM_ID];
        atomBond.atom.SetDispRadius(eleInfo->dispRadius);
        atomBond.bondCount = 0;
        for(i = 0; i < 3; i++) {
            atomBond.atom.pos[i] = 0.0;
            //atomBond.atom.pos[i] = (m_atomBondArray[atomIndex[0]].atom.pos[i] + m_atomBondArray[atomIndex[1]].atom.pos[i])/2;
            for(j = 0; j < pSelUtil->GetSelectedCount(); j++) {
                atomBond.atom.pos[i] += m_atomBondArray[pSelUtil->GetSelectedIndex(j)].atom.pos[i];
            }
            atomBond.atom.pos[i] /= pSelUtil->GetSelectedCount();
        }

        m_atomBondArray.Add(atomBond);
        isAdd = true;
    }
    return isAdd;
}

AtomPosition3F RenderingData::GetGroupCentroid(int groupId) {
    unsigned i, j;
    long totalMass = 0;
    int mass = 0;
    AtomPosition3F centroid;
    centroid.x = centroid.y = centroid.z = 0.0f;
    for(i = 0; i < m_atomBondArray.GetCount(); i++) {
        if(BP_NULL_VALUE == groupId || m_atomBondArray[i].atom.connGroup == groupId) {
            if(m_atomBondArray[i].atom.elemId == DEFAULT_HYDROGEN_ID) {
                mass = ELEM_H;
            } else if(m_atomBondArray[i].atom.elemId <= 0) {
                continue;
            } else {
                mass = abs(m_atomBondArray[i].atom.elemId);
            }
            totalMass += mass;
            for(j = 0; j < 3; j++) {
                centroid[j] += m_atomBondArray[i].atom.pos[j] * mass;
            }
        }
    }
    if(totalMass > 0 ) {
        for(j = 0; j < 3; j++) {
            centroid[j] /= totalMass;
        }
    }
    return centroid;
}
/*
void RenderingData::Mirror(SelectionUtil* pSelUtil, double rotateAngle) {
    unsigned i, origAtomCount;
    int j, connIndex;
        AtomBondArray cloneAtomBondArray;
        double mirrorPoints[3][3] = {{0}, {0}, {0}};
        AtomBond atomBond;

        origAtomCount = m_atomBondArray.GetCount();
        for(i = 0; i < origAtomCount; i++) {
                if(!pSelUtil->IsSelected(i)) {
                        atomBond = AtomBond();
                        atomBond.Clone(m_atomBondArray[i]);
                        for(j = 0; j < atomBond.bondCount; j++) {
                                connIndex = atomBond.bonds[j].atomIndex;
                                if(pSelUtil->IsSelected(connIndex)) {
                                        m_atomBondArray[connIndex].bonds[m_atomBondArray[connIndex].bondCount].atomIndex = m_atomBondArray.GetCount();
                                        m_atomBondArray[connIndex].bonds[m_atomBondArray[connIndex].bondCount].bondType = atomBond.bonds[j].bondType;
                                        m_atomBondArray[connIndex].bonds[m_atomBondArray[connIndex].bondCount].dispType = atomBond.bonds[j].dispType;
                                        m_atomBondArray[connIndex].bondCount++;
                                }else {
                                        atomBond.bonds[j].atomIndex += origAtomCount - pSelUtil->GetAtomCountLessThan(connIndex);
                                }
                        }
                        m_atomBondArray.Add(atomBond);
                }
        }
        //DumpRenderingData(m_atomBondArray, EnvUtil::GetLogDir() + wxT("/mirror.txt"));
        pSelUtil->GetSelectedPositions(mirrorPoints, 3);
        //ShowInfoMessage(wxString::Format(wxT("%f"), symOpe->GetRotationAngle()));
        for(i = origAtomCount; i < m_atomBondArray.GetCount(); i++) {
                switch(pSelUtil->GetSelectedCount()) {
                        case 1:
                                //point symmetry
                                SymmetryInversion(i, mirrorPoints[0], m_atomBondArray, true);
                                break;
                        case 2:
                                //line symmetry
                                SymmetryRotation (i, mirrorPoints, rotateAngle, m_atomBondArray, true);
                                break;
                        case 3:
                                //plane symmetry
                                SymmetryReflection(i, mirrorPoints, m_atomBondArray, true);
                                break;
                        default:
                                break;
                }
        }

        TagConnectionGroup();
}*/

void RenderingData::Mirror(SelectionUtil *pSelUtil, bool isAll, double rotateAngle) {
    unsigned i, origAtomCount;
    int j, connIndex;
    AtomBondArray cloneAtomBondArray;
    double mirrorPoints[3][3] = {{0}, {0}, {0}};
    AtomBond atomBond;

    //DumpRenderingData(m_atomBondArray, EnvUtil::GetLogDir() + wxT("/mirrorbefore.txt"));
    origAtomCount = m_atomBondArray.GetCount();
    for(i = 0; i < origAtomCount; i++) {
        if(!pSelUtil->IsSelected(i) && (isAll || pSelUtil->IsManuSelected(i))) {
            atomBond = AtomBond();
            atomBond.Clone(m_atomBondArray[i]);
            for(j = 0; j < atomBond.bondCount; j++) {
                connIndex = atomBond.bonds[j].atomIndex;
                if(pSelUtil->IsSelected(connIndex)) {
                    m_atomBondArray[connIndex].bonds[m_atomBondArray[connIndex].bondCount].atomIndex = m_atomBondArray.GetCount();
                    m_atomBondArray[connIndex].bonds[m_atomBondArray[connIndex].bondCount].bondType = atomBond.bonds[j].bondType;
                    m_atomBondArray[connIndex].bonds[m_atomBondArray[connIndex].bondCount].dispType = atomBond.bonds[j].dispType;
                    m_atomBondArray[connIndex].bondCount++;
                } else {
                    if(isAll) {
                        atomBond.bonds[j].atomIndex += origAtomCount - pSelUtil->GetAtomCountLessThan(connIndex);
                    } else {
                        if(pSelUtil->IsManuSelected(connIndex)) {
                            atomBond.bonds[j].atomIndex = origAtomCount + pSelUtil->GetManuAtomCountLessThan(connIndex);
                        } else {
                            //remove a bond
                            atomBond.RemoveBond(j);
                            j--;
                        }
                    }
                }
            }
            m_atomBondArray.Add(atomBond);
        }
    }
    //DumpRenderingData(m_atomBondArray, EnvUtil::GetLogDir() + wxT("/mirror.txt"));
    pSelUtil->GetSelectedPositions(mirrorPoints, 3);
    //ShowInfoMessage(wxString::Format(wxT("%f"), symOpe->GetRotationAngle()));
    for(i = origAtomCount; i < m_atomBondArray.GetCount(); i++) {
        switch(pSelUtil->GetSelectedCount()) {
        case 1:
            //point symmetry
            SymmetryInversion(i, mirrorPoints[0], m_atomBondArray, true);
            break;
        case 2:
            //line symmetry
            SymmetryRotation (i, mirrorPoints, rotateAngle, m_atomBondArray, true);
            break;
        case 3:
            //plane symmetry
            SymmetryReflection(i, mirrorPoints, m_atomBondArray, true);
            break;
        default:
            break;
        }
    }

    TagConnectionGroup();
}

void RenderingData::SetAtomArray(AtomPosition3FArray &atomarray, int opmode, float enhanceswing)
{
    unsigned i = 0;
    if(atomarray.GetCount() != m_atomBondArray.Count()) return;
    //Tools::ShowError(wxString::Format("mode %d, count %d, %d", opmode, m_atomBondArray.Count(), atomarray.GetCount()));
    switch(opmode) {
    case 1:
        for (i = 0; i < m_atomBondArray.Count(); i++) {
            m_atomBondArray[i].atom.pos.x += atomarray[i].x * enhanceswing;
            m_atomBondArray[i].atom.pos.y += atomarray[i].y * enhanceswing;
            m_atomBondArray[i].atom.pos.z += atomarray[i].z * enhanceswing;
        }
        break;
    case 2:
        for (i = 0; i < m_atomBondArray.Count(); i++) {
            m_atomBondArray[i].atom.pos.x -= atomarray[i].x * enhanceswing;
            m_atomBondArray[i].atom.pos.y -= atomarray[i].y * enhanceswing;
            m_atomBondArray[i].atom.pos.z -= atomarray[i].z * enhanceswing;
        }
        break;
    case 3:
        for (i = 0; i < m_atomBondArray.Count(); i++) {
            m_atomBondArray[i].atom.pos.x = atomarray[i].x * enhanceswing;
            m_atomBondArray[i].atom.pos.y = atomarray[i].y * enhanceswing;
            m_atomBondArray[i].atom.pos.z = atomarray[i].z * enhanceswing;
        }
        break;
    }
}


ConstrainData &RenderingData::GetConstrainData()
{
    return m_constrainData;
}

void RenderingData::ReCalcDummyAtomPositions()
{
    unsigned i;
    double x, y, z;
    int atomIndex, j;
    LngLHash indexHash;
    ResetLigandArray();
    if(m_ligandArray.GetCount() <= 0)
        return;
    for(i = 0; i < m_atomBondArray.GetCount(); i++)
        indexHash[m_atomBondArray[i].atom.uniqueId] = i;
    for(i = 0 ; i < m_ligandArray.GetCount() ; i++)
    {
        x = y = z = 0.0;
        //wxMessageBox(wxT("calc ")+m_ligandArray[i].ToString());
        for(j = 0; j < (int)m_ligandArray[i].linkedAtomIds.GetCount(); j++)
        {
            //wxASSERT_MSG(indexHash.find(m_ligandArray[i].linkedAtomIds[j])!=indexHash.end();wxT("The ligand's info has changed!"));
            atomIndex = indexHash.find(m_ligandArray[i].linkedAtomIds[j])->second;
            x += m_atomBondArray[atomIndex].atom.pos.x;
            y += m_atomBondArray[atomIndex].atom.pos.y;
            z += m_atomBondArray[atomIndex].atom.pos.z;
        }
        x /= m_ligandArray[i].linkedAtomIds.GetCount();
        y /= m_ligandArray[i].linkedAtomIds.GetCount();
        z /= m_ligandArray[i].linkedAtomIds.GetCount();
        //wxASSERT_MSG(indexHash.find(m_ligandArray[i].dummyAtomId)!=indexHash.end();wxT("The ligand's info has changed!"));
        atomIndex = indexHash.find(m_ligandArray[i].dummyAtomId)->second;
        m_atomBondArray[atomIndex].atom.pos.x = x;
        m_atomBondArray[atomIndex].atom.pos.y = y;
        m_atomBondArray[atomIndex].atom.pos.z = z;
    }
}

void RenderingData::ResetLigandArray()
{
    wxSetLong indexSet;
    int i;
    if(m_ligandArray.GetCount() <= 0)
        return;
    for(i = 0; i < (int)m_atomBondArray.GetCount(); i++)
        indexSet.insert(m_atomBondArray[i].atom.uniqueId);
    for(i = m_ligandArray.GetCount() - 1; i >= 0  ; i--)
    {
        //        wxMessageBox(wxString::Format(wxT("find=%ld,i=%d"),m_ligandArray[i].dummyAtomId,i));
        ChemLigand ligand = m_ligandArray[i];
        //        wxMessageBox(wxString::Format(wxT("size=%d,i=%d"),indexSet.size(),i));
        if(indexSet.find(ligand.dummyAtomId) == indexSet.end())
        {
            //wxMessageBox(wxT("Remove ")+m_ligandArray[i].ToString());
            m_ligandArray.Detach(i);
        }
    }
    //wxMessageBox(wxT("ResetLigandArray"));
}

ChemLigandArray &RenderingData::GetLigandArray()
{
    ResetLigandArray();
    return m_ligandArray;
}


void RenderingData::GetMoleculeSize(AtomPosition3F &inLeftTop, AtomPosition3F &outRightBottom)
{
    if(m_atomBondArray.GetCount() == 0)
        return;
    AtomPosition3F posMin, posMax;
    posMin.x = m_atomBondArray[0].atom.pos.x;
    posMin.y = m_atomBondArray[0].atom.pos.y;
    posMin.z = m_atomBondArray[0].atom.pos.z;
    inLeftTop = outRightBottom = posMin;
    for(unsigned int i = 1; i < m_atomBondArray.GetCount(); i++)
    {
        posMin.x = m_atomBondArray[i].atom.pos.x - m_atomBondArray[i].atom.GetDispRadius();
        posMin.y = m_atomBondArray[i].atom.pos.y + m_atomBondArray[i].atom.GetDispRadius();
        posMin.z = m_atomBondArray[i].atom.pos.z - m_atomBondArray[i].atom.GetDispRadius();
        posMax.x = m_atomBondArray[i].atom.pos.x + m_atomBondArray[i].atom.GetDispRadius();
        posMax.y = m_atomBondArray[i].atom.pos.y - m_atomBondArray[i].atom.GetDispRadius();
        posMax.z = m_atomBondArray[i].atom.pos.z + m_atomBondArray[i].atom.GetDispRadius();
        if(posMin.x < inLeftTop.x)
            inLeftTop.x = posMin.x;
        if(posMin.y > inLeftTop.y)
            inLeftTop.y = posMin.y;
        if(posMin.z < inLeftTop.z)
            inLeftTop.z = posMin.z;
        if(posMax.x > outRightBottom.x)
            outRightBottom.x = posMax.x;
        if(posMax.y < outRightBottom.y)
            outRightBottom.y = posMax.y;
        if(posMax.z > outRightBottom.z)
            outRightBottom.z = posMax.z;
    }
}
