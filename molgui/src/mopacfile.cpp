/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

//begin : hurukun : 12/11/2009
//includes and defines for the simu-mopac file conversion
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "mopacfile.h"
#include "commons.h"
#include "fileutil.h"
#include "stringutil.h"

#include <wx/textfile.h>

#define MAX_ATOMS 500
#define BUFFERSIZE 83
#define STRINGSIZE 30
#define Key_BEGIN "$BEGIN"
#define Key_END "$END"
#define Key_COOR "COOR"
#define STRING_CATE "CARTESIAN"
#define STRING_COOR "COORDINATES"
#define CART_COOR_NB_OF_COLUMNS 5
//end   : hurukun : 12/11/2009

//begin : hurukun : 12/11/2009
wxString MopacFile::SimuToMopac(const wxString simufile, const wxString mopacfile, const wxString &paraMethod, const wxString &paraMulti, const wxString &paraOptions, const int ncharge)
{
    char *sfile, *mfile, *cpara, *tempData, *cparaEx;

    FILE *fpin, *fpout;
    char buffer[BUFFERSIZE];
    char string1[STRINGSIZE];
    char string2[STRINGSIZE];
    char string3[STRINGSIZE];
    char string4[STRINGSIZE];
    wxArrayString return_msg;

    return_msg.Add(wxEmptyString);//0
    return_msg.Add(wxT("File name can't be empty!"));//1
    return_msg.Add(wxT("Cannot open file:"));//2

    if(simufile.IsEmpty() || mopacfile.IsEmpty() || paraMethod.IsEmpty() || paraMulti.IsEmpty())
        return return_msg[1];

    wxString paraEx = paraMulti + wxT(" ") + paraOptions + wxString::Format(wxT(" charge=%d"), ncharge);
    StringUtil::String2CharPointer(&sfile, simufile);
    StringUtil::String2CharPointer(&mfile, mopacfile);
    StringUtil::String2CharPointer(&cpara, paraMethod);
    StringUtil::String2CharPointer(&cparaEx, paraEx);

    int na;
    double x, y, z;

    if ((fpin = fopen(sfile, "r")) == NULL) {
        return_msg[2] = return_msg[2] + simufile;
        return return_msg[2];
    }

    if ((fpout = fopen(mfile, "w")) == NULL) {
        return_msg[2] = return_msg[2] + mopacfile;
        return return_msg[2];
    }

    // MOPAC keyword
    fprintf(fpout, "%4s xyz %s\n  \n %4s Calculation\n", cpara, cparaEx, cpara);

    while((tempData = fgets(buffer, BUFFERSIZE, fpin)) != NULL) {
        sscanf(buffer, "%s %s", string1, string2);

        if (strcmp (Key_BEGIN, string1) == 0 && strcmp (Key_COOR, string2) == 0) {
            while(1) {
                tempData = fgets(buffer, BUFFERSIZE, fpin);
                sscanf(buffer, "%s %s %s %s", string1, string2, string3, string4);
                if (strcmp(Key_END, string1) == 0 && strcmp (Key_COOR, string2) == 0)
                    break;
                na = atoi(string1);
                x = atof(string2);
                y = atof(string3);
                z = atof(string4);
                fprintf(fpout, "%2d %15.7f 1 %15.7f 1 %15.7f 1\n", na, x, y, z);
            }
        }
    }
    fprintf(fpout, "  \n");
    fclose(fpout);
    fclose(fpin);

    return return_msg[0];
}
/* return code:
   0:  normal
   1:  can't open file
   2:  input/output files error
   3:  no final opt info
   4:
*/
wxString MopacFile::MopacToSimu(const wxString simuinfile, const wxString mopacoutfile, const wxString simuoutfile)
{
    if(simuinfile.IsEmpty() || mopacoutfile.IsEmpty() || simuoutfile.IsEmpty())
        return wxT("File name can't be empty!");
    char *sinfile, *moutfile, *soutfile, *tempData;
    StringUtil::String2CharPointer(&sinfile, simuinfile);
    StringUtil::String2CharPointer(&soutfile, simuoutfile);
    StringUtil::String2CharPointer(&moutfile, mopacoutfile);

    FILE *fmout, *fsin, *fsout;
    char buffer[BUFFERSIZE];
    char string1[STRINGSIZE];
    char string2[STRINGSIZE];
    char string3[STRINGSIZE];
    char string4[STRINGSIZE];
    char string5[STRINGSIZE];
    wxArrayString return_msg;
    int    return_key = 0;

    return_msg.Add(wxEmptyString);//0
    return_msg.Add(wxT("Cannot open file:"));//1
    return_msg.Add(wxT("Abstract Coordinates error!"));//2
    return_msg.Add(wxT("Can't open sim check file."));//3
    return_msg.Add(wxT("Can't open MOPAC output file."));//4
    return_msg.Add(wxT("Can't open sim output file."));//5
    return_msg.Add(wxT("Atomic types in ")+mopacoutfile+wxT(" not matching with input."));//6
    return_msg.Add(wxT("Number of atoms error!"));//7
    return_msg.Add(wxT("MOPAC error, check output file: "+mopacoutfile));//8


    double x, y, z;
    int    elemID, nAtomIndex = 0;
    int    nAtom[MAX_ATOMS];

    if ((fmout = fopen(moutfile, "r")) == NULL) {
        return_msg[1] = return_msg[1] + mopacoutfile;
        return return_msg[1];
    }

    if ((fsin = fopen(sinfile, "r")) == NULL) {
        return_msg[1] = return_msg[1] + simuinfile;
        return return_msg[1];
    }

    if ((fsout = fopen(soutfile, "w")) == NULL) {
        return_msg[1] = return_msg[1] + simuoutfile;
        return return_msg[1];
    }

    while((tempData = fgets(buffer, BUFFERSIZE, fsin)) != NULL) {
        sscanf(buffer, "%s %s", string1, string2);
        if (strcmp (Key_BEGIN, string1) == 0 && strcmp (Key_COOR, string2) == 0) {
            while(1) {
                tempData = fgets(buffer, BUFFERSIZE, fsin);
                sscanf(buffer, "%s %s", string1, string2);
                if (strcmp(Key_END, string1) == 0 && strcmp (Key_COOR, string2) == 0)
                    break;
                nAtom[nAtomIndex] = atoi(string1);//record the atoms in the input file.
                nAtomIndex += 1;
            }
            break;
        }
    }
    fseek(fsin, 0, SEEK_SET);
    bool        isSameContent = true, isMopacDataOver = false;
    int                Find_Final = 0, OUT_NA = 0;
    while((tempData = fgets(buffer, BUFFERSIZE, fsin)) != NULL) {
        if (strstr (buffer, "$END COOR") != NULL)//reach the same content part, read the simu-input file
            isSameContent = true;
        if(isSameContent == true)
            fprintf(fsout, "%s", buffer); //write the same content part with the simu-input file
        if (strstr (buffer, "$BEGIN COOR") != NULL) {//reach the data part, read the mopac-output file
            isSameContent = false;
            while((tempData = fgets(buffer, BUFFERSIZE, fmout)) != NULL) {
                if (strstr (buffer, "FINAL HEAT OF FORMATION") != NULL) {
                    Find_Final = 1;
                    while(1) {//find the data in mopac outputfile
                        tempData = fgets(buffer, BUFFERSIZE, fmout);
                        sscanf(buffer, "%s %s", string1, string2);
                        if (strcmp(STRING_CATE, string1) == 0 && strcmp (STRING_COOR, string2) == 0) {
                            // Read lines until the first line of coordinates: 5 columns and first data is a number
                            wxArrayString tokens;
                            do
                            {
                                tempData = fgets(buffer, BUFFERSIZE, fmout);
                                tokens = StringUtil::Split(StringUtil::CharPointer2String(buffer));
                            }while( !(tokens.GetCount()==CART_COOR_NB_OF_COLUMNS && tokens[0].IsNumber()) );
                            while(1) {
                                sscanf(buffer, "%s %s %s %s %s", string1, string2, string3, string4, string5);
                                if (string1[0] == '\0')
                                {
                                    isMopacDataOver = true;
                                    break;
                                }
                                elemID = GetElemId(StringUtil::CharPointer2String(string2));
                                x = atof(string3);
                                y = atof(string4);
                                z = atof(string5);

                                if (elemID != nAtom[OUT_NA]) {
                                    // std::cout << "CRASH: " <<string1 << " " << string2 << " " << elemID << std::endl;
                                    return_key = 6;
                                    isMopacDataOver = true;
                                    break;
                                }

                                fprintf(fsout, "%d %15.7f %15.7f %15.7f \n", elemID, x, y, z);
                                OUT_NA += 1;
                                string1[0] = '\0';

                                tempData = fgets(buffer, BUFFERSIZE, fmout);
                            }//end while
                        }//end if
                        if(isMopacDataOver == true)
                            break;
                    }//end while
                }//end if
                if(isMopacDataOver == true)
                    break;
            }//end while
            fclose(fmout);
        }//end if
    }//end while

    if (Find_Final == 0) {
        return_key = 8;
    } else if (OUT_NA != nAtomIndex && return_key != 6) {
        return_key = 7;
    }

    fclose(fsin);
    fclose(fsout);
    return return_msg[return_key];
}
//end   : hurukun : 12/11/2009
