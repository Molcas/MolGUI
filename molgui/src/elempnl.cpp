/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/filename.h>
#include <wx/statline.h>

#include "elempnl.h"

#include "abstractview.h"
#include "fileutil.h"
#include "stringutil.h"
#include "tools.h"
#include "envutil.h"

#include "ctrlids.h"
#include "togglebitmapbutton.h"
#include "commons.h"
#include "elempnldata.h"
#include "molfile.h"
#include "resource.h"
#include "wx/scrolbar.h"

#include "dlgoutput.h"
//#include "gmfile.h"
//#include "simuguiapp.h"

#define CUSTOM_FRAGMENT_INDEX 3
//bool    tog_btn_clk=false;

int CTRL_ID_NOTEBOOK_PNL = wxNewId();
int ID_SCROLLBAR = wxNewId();

BEGIN_EVENT_TABLE(ElemPnl, wxWindow)
    EVT_TOGGLEBUTTON(wxID_ANY, ElemPnl::OnToggleButton)
    EVT_LISTBOX(wxID_ANY, ElemPnl::OnListBox)
    EVT_NOTEBOOK_PAGE_CHANGED(CTRL_ID_NOTEBOOK_PNL, ElemPnl::OnNotebookPageChanged)
    EVT_PAINT(ElemPnl::OnPaint)//wangyuanlong     2010/11/5
    EVT_SIZE(ElemPnl::OnSize)
    //EVT_UPDATE_UI(CTRL_ID_CVS_GL, ElemPnl::OnGLCanvasUpdateUI)
END_EVENT_TABLE()

wxString strFragmentDirs[] = {wxT("Groups"), wxT("Rings"), wxT("Ligands"), wxT("Customs")};

ElemPnl::ElemPnl(wxWindow* parent, wxWindowID id, const wxPoint& pos,
                 const wxSize& size, long style, const wxString& name ) : wxWindow(parent, id, pos, size, style, name)
{
    wxSize szParentClient = parent->GetClientSize();
    m_pScrolWin = NULL; //wangyuanlong     2010/11/5
    m_pBookElements = NULL;
    m_PageChanged = true;
    //    tog_btn_clk=false;
    InitDefaultAtomPosition();
    InitPanel(szParentClient);
}

ElemPnl::~ElemPnl()
{
}

void ElemPnl::OnGLCanvasUpdateUI(wxUpdateUIEvent& event)
{
    //RefreshGraphics();
    //event.Skip();
    //        m_pGLPreview->Init();
    m_pGLPreview->Draw();
}

void ElemPnl::InitPanel(wxSize& szParentClient)
{
    int i = 0;
    //        wxSize szClient=GetClientSize();
    wxBoxSizer* pBsTop = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(pBsTop);

    m_molData.SetActionType(ACTION_ADD_FRAGMENT);
    CtrlInst* ctrlInst = new CtrlInst(&m_molData, 0); ///????
    m_molData.AddCtrlInst(ctrlInst);
    m_molData.SetActiveCtrlInstIndex(m_molData.GetCtrlInstCount() - 1);
    m_molData.GetActiveCtrlInst()->SetPreviewCtrlInst(true);
    ElemPnlData::Get()->SetPreviewCtrlInst(m_molData.GetActiveCtrlInst());

    //    m_pScrolWin=new wxScrolledWindow(this,wxID_ANY,wxDefaultPosition,wxDefaultSize,wxSUNKEN_BORDER|wxScrolledWindowStyle );
    //        m_pGLPreview = new MolGLCanvas(m_pScrolWin, VIEW_GL_PREVIEW, CTRL_ID_CVS_GL, wxPoint(0,0), wxSize(260, 200), wxSUNKEN_BORDER);//wangyuanlong     2010/11/5
    m_pGLPreview = new MolGLCanvas(this, VIEW_GL_PREVIEW, CTRL_ID_CVS_GL, wxDefaultPosition, wxSize(260, 200), wxSUNKEN_BORDER);
    m_pGLPreview->SetMolData(&m_molData);


    //wangyuanlong     2010/11/5     begain


    //    if(szClient.GetHeight()<550)
    //        m_pScrolWin->SetScrollbars(10,10,28,55);
    //
    m_pBookElements = new wxNotebook(this, CTRL_ID_NOTEBOOK_PNL, wxPoint(0, 202), wxDefaultSize, wxBK_TOP); //1100
    //    m_pBookElements = new wxNotebook(m_pScrolWin, CTRL_ID_NOTEBOOK_PNL, wxDefaultPosition,wxDefaultSize,wxBK_TOP);//wangyuanlong     2010/11/5

    wxScrolledWindow* pPnlElem = CreateElementPnl(m_pBookElements);
    //    wxScrollBar *scrollBar  =new wxScrollBar (pPnlElem ,ID_SCROLLBAR ,wxDefaultPosition ,wxDefaultSize ,wxSB_VERTICAL);//wxSB_HORIZONTAL
    m_pBookElements->AddPage(pPnlElem, wxT("Elements"), true); //SONG change the fragment to block for workshop2009


    //    m_pScrolWin=new wxScrolledWindow(m_pBookElements,wxID_ANY,wxDefaultPosition,wxDefaultSize,wxSUNKEN_BORDER|wxScrolledWindowStyle );
    wxArrayString fileNames;
    for (i = 0; i < 4; i++)
    {
        fileNames.Clear();
        if (i == CUSTOM_FRAGMENT_INDEX)
        {
            wxString dir = CatenateDataFile(i, wxEmptyString);
            FileUtil::CreateDirectory(dir);
            FileUtil::ListDirectoryFiles(dir, false, true, fileNames);
        }
        else
        {
            FileUtil::ListDirectoryFiles(CatenateDataFile(i, wxEmptyString), false, true, fileNames);
        }
        //        wxBoxSizer* pList = new wxBoxSizer(wxVERTICAL);
        //                m_pLbFragments[i] = new wxListBox(m_pBookElements, CTRL_ID_LB_GROUPS + i,wxDefaultPosition,wxDefaultSize, fileNames, wxLB_SINGLE | wxLB_SORT);//800

        m_pLbFragments[i] = new wxListBox(m_pBookElements, CTRL_ID_LB_GROUPS + i, wxPoint(0, 0), wxDefaultSize, fileNames, wxLB_SINGLE | wxLB_SORT | wxLB_ALWAYS_SB);

        m_pBookElements->AddPage(m_pLbFragments[i], strFragmentDirs[i], false);
    }

    //        wxBoxSizer* pBsScroll = new wxBoxSizer(wxVERTICAL);
    //    wxBoxSizer* pBsScroll = new wxBoxSizer(wxHORIZONTAL);
    //    pBsScroll->Add(m_pBookElements, 1, wxALL, 1);//wangyuanlong     2010/11/5
    //    m_pBookElements->SetSizer(pBsScroll);

    //        m_pScrolWin->SetScrollRate( 10, 10 );
    //        m_pScrolWin->SetVirtualSize( 280, 1100 );

    //        m_pScrolWin->SetSizer(pBsScroll);

    //        pBsScroll->SetSizeHints(m_pScrolWin);

    pBsTop->Add(m_pGLPreview, 0, wxALL | wxEXPAND, 2);
    pBsTop->Add(m_pBookElements, 1, wxALL | wxEXPAND, 2);
    //        pBsTop->Add(m_pGLPreview, 0, wxALL | wxEXPAND, 2);
    //    pBsTop->Add(m_pScrolWin, 1, wxALL|wxEXPAND, 2);


    Layout();
    //        this->SetSizer(pBsTop);
    //        pBsTop->Fit(this);
    pBsTop->SetSizeHints(this);

    /*
     m_pScrolWin=new wxScrolledWindow(this,wxID_ANY,wxDefaultPosition,wxDefaultSize,wxSUNKEN_BORDER|wxScrolledWindowStyle );

    //    if(szClient.GetHeight()<550)
    //        m_pScrolWin->SetScrollbars(10,10,28,55);
    //
             m_pBookElements = new wxNotebook(m_pScrolWin, CTRL_ID_NOTEBOOK_PNL, wxPoint(0,0),wxSize(260, 1100), wxBK_TOP);
    //         m_pBookElements = new wxNotebook(this, CTRL_ID_NOTEBOOK_PNL, wxPoint(0,0),wxSize(260, 1100), wxBK_TOP);

        wxPanel *pPnlElem = CreateElementPnl(m_pBookElements);
            m_pBookElements->AddPage(pPnlElem, wxT("Elements"), true); //SONG change the fragment to block for workshop2009

            wxArrayString fileNames;
            for(i = 0; i < 4; i++) {
                    fileNames.Clear();
                    if(i == CUSTOM_FRAGMENT_INDEX) {
                            wxString dir = CatenateDataFile(i, wxEmptyString);
                            FileUtil::CreateDirectory(dir);
                            FileUtil::ListDirectoryFiles(dir, false, true, fileNames);

                    }else {
                            FileUtil::ListDirectoryFiles(CatenateDataFile(i, wxEmptyString), false, true, fileNames);
                     }
            m_pLbFragments[i] = new wxListBox(m_pBookElements, CTRL_ID_LB_GROUPS + i, wxPoint(0,0), wxSize(260,800), fileNames, wxLB_SINGLE | wxLB_SORT);

            m_pBookElements->AddPage(m_pLbFragments[i], strFragmentDirs[i], false);
            }
            wxBoxSizer* pBsScroll = new wxBoxSizer(wxVERTICAL);
            m_pScrolWin->SetScrollRate( 10, 10 );
            m_pScrolWin->SetVirtualSize( 280, 1100 );
    //        m_pScrolWin->SetSizer(pBsScroll);
            pBsScroll->Add(m_pBookElements, 1, wxALL, 1);
    //        pBsScroll->SetSizeHints(m_pScrolWin);
            pBsTop->Add(m_pGLPreview, 0, wxALL | wxEXPAND, 1);
        pBsTop->Add(m_pScrolWin, 1, wxALL | wxEXPAND, 1);

            Layout();
            pBsTop->SetSizeHints(this);*/
}

void ElemPnl::OnPaint(wxPaintEvent& event)
{
    //    wxPaintDC dc(m_pScrolWin);
    wxPaintDC dc(this);
    //   m_pScrolWin->DoPrepareDC(dc);
    //   DoPrepareDC(dc);

    event.Skip();
}



void ElemPnl::OnSize(wxSizeEvent& event)
{
    if (event.GetSize().GetHeight() < 850) //610+140
    {
        m_pScrolWin->SetSize(event.GetSize().GetWidth(), event.GetSize().GetHeight() - 300); //wangyuanlong     2010/11/5
        //        m_pBookElements->SetSize(event.GetSize().GetWidth(),500);
        m_pBookElements->SetSize(event.GetSize().GetWidth(), event.GetSize().GetHeight() - 200);
        m_pGLPreview->SetSize(event.GetSize().GetWidth(), 200);
        //        pPnlRet11->SetSize(event.GetSize().GetWidth(),event.GetSize().GetHeight()-200);

    }
    else
    {
        m_pScrolWin->SetSize(event.GetSize().GetWidth(), 650); //wangyuanlong     2010/11/5
        //        m_pBookElements->SetSize(event.GetSize().GetWidth(),550);
        m_pBookElements->SetSize(event.GetSize().GetWidth(), 650);
        //        m_pLbFragments[1]->SetSize(event.GetSize().GetWidth(),595);
        //        m_pLbFragments[2]->SetSize(event.GetSize().GetWidth(),595);
        //        m_pLbFragments[3]->SetSize(event.GetSize().GetWidth(),595);
        //        m_pLbFragments[4]->SetSize(event.GetSize().GetWidth(),595);
    }

    event.Skip();
}

wxString ElemPnl::CatenateDataFile(int listBoxIndex, wxString fileName)
{
    wxString tempFile = strFragmentDirs[listBoxIndex];
    if (!StringUtil::IsEmptyString(fileName))
    {
        tempFile += platform::PathSep() + fileName;
    }
    if (listBoxIndex == CUSTOM_FRAGMENT_INDEX)
    {
        return FileUtil::BuildFullPath(EnvUtil::GetUserDataDir(), wxT("dat"), tempFile);
    }
    else
    {

        return FileUtil::BuildFullPath(EnvUtil::GetPrgResDir(), wxTRUNK_MOL_DAT_PATH, tempFile);

    }
}

wxScrolledWindow* ElemPnl::CreateElementPnl(wxBookCtrlBase* parent)
{
    int i, j;

    m_pScrolWin = new wxScrolledWindow(parent, wxID_ANY, wxPoint(0, 0), wxSize(260, 650), wxSUNKEN_BORDER | wxScrolledWindowStyle );

    //        wxPanel* pPnlRet = new wxPanel(parent,wxID_ANY,wxDefaultPosition, wxSize(260, 1500));
    m_pScrolWin->SetScrollRate( 10, 10 );
    m_pScrolWin->SetVirtualSize( 280, 650 );
    //    wxPanel* pPnlRet = new wxPanel(m_pScrolWin);
    wxBoxSizer* pBsTop = new wxBoxSizer(wxVERTICAL);
    m_pScrolWin->SetSizer(pBsTop);

    wxFlexGridSizer* pGsElems = new wxFlexGridSizer(17, 10, 0, 0);
    //wxToggleButton fontBtn;
    //        wxFont font = fontBtn.GetFont();
    //        font.SetPointSize(font.GetPointSize() - 1);

    CreateCategoryElems(m_pScrolWin, wxT("Non Metal"), wxColour(0, 255, 0), NON_METALs);
    CreateCategoryElems(m_pScrolWin, wxT("Inert Gas"), wxColour(205, 154, 154), INERT_GASSES);
    CreateCategoryElems(m_pScrolWin, wxT("Main Group Metal"), wxColour(204, 255, 153), MAIN_GROUP_METALS);
    CreateCategoryElems(m_pScrolWin, wxT("Alkaline Metal"), wxColour(102, 153, 255), ALKALINE_METALS);
    CreateCategoryElems(m_pScrolWin, wxT("Alkaline Earth Metal"), wxColour(153, 204, 255), ALKALINE_EARTH_METALS);
    CreateCategoryElems(m_pScrolWin, wxT("Transition Metal"), wxColour(255, 153, 204), TRANSITION_ELEMS);
    CreateCategoryElems(m_pScrolWin, wxT("LaAc Metal"), wxColour(192, 192, 192), LAAC_ELEMS);
    CreateDummyElem(m_pScrolWin);

    /*for(i = 0; i < ELEM_TOGGLE_BTN_COUNT; i++) {
    m_pTglElems[i] = new wxToggleButton(pPnlRet, CTRL_ID_TGL_ELEM_START + i, ELEM_NAMES[i], wxDefaultPosition, wxSize(25,25));
            m_pTglElems[i]->SetOwnFont(font);
    }*/
    //Main group elements
    for (i = 0; i < ELEM_Ca; i++)
    {
        if (i == ELEM_H)
        {
            for (j = 0; j < 3; j++)
            {
                pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
            }
            pGsElems->Add(m_pTglElems[ELEM_DUMMY - 1], 0, wxALIGN_CENTER_HORIZONTAL, 0);
            //pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
            for (j = 0; j < 4; j++)
            {
                pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
            }
        }
        if (i == ELEM_Be || i == ELEM_Mg)
        {
            for (j = 0; j < 2; j++)
            {
                pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
            }
        }
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (j = 0; j < 2; j++)
    {
        pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
    }
    for (i = ELEM_Ga - 1; i < ELEM_Sr; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (j = 0; j < 2; j++)
    {
        pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
    }
    for (i = ELEM_In - 1; i < ELEM_Ba; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (j = 0; j < 2; j++)
    {
        pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
    }
    for (i = ELEM_Tl - 1; i < ELEM_Ra; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (j = 0; j < 8; j++)
    {
        pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
    }
    //Vice-group elements
    for (i = ELEM_Sc - 1; i < ELEM_Zn; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (i = ELEM_Y - 1; i < ELEM_Cd; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    pGsElems->Add(m_pTglElems[ELEM_La - 1], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    for (i = ELEM_Hf - 1; i < ELEM_Hg; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    pGsElems->Add(m_pTglElems[ELEM_Ac - 1], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    for (i = ELEM_Rf - 1; i < ELEM_Mt; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (j = 0; j < 3; j++)
    {
        pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
    }
    //
    for (i = ELEM_Ce - 1; i < ELEM_Lu; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (i = 0; i < 6; i++)
    {
        pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
    }
    for (i = ELEM_Th - 1; i < ELEM_Lr; i++)
    {
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for (i = 0; i < 6; i++)
    {
        pGsElems->Add(new wxStaticText(m_pScrolWin, wxID_ANY, wxT("")));
    }

    //
    wxImage image;
    wxBitmap bmp;
    wxGridSizer* pBsBondPos = new wxGridSizer(2, 5, 0, 0);
    for (i = 0; i < BOND_POS_COUNT; i++)
    {
        //                EnvUtil::GetMolcasDir()+wxT("/gui")+ wxT("/res/mol/images/") + wxT("tadd.png")

        //                image.LoadFile(CheckImageFile(wxString::Format(wxT("positions/pos%d.png"), i + 1)), wxBITMAP_TYPE_PNG);
        image.LoadFile(EnvUtil::GetPrgResDir() + wxTRUNK_MOL_IMG_PATH + wxString::Format(wxT("positions/pos%d.png"), i + 1), wxBITMAP_TYPE_PNG);

        bmp = wxBitmap(image);
        m_pTglBmpBondPoss[i] = new ToggleBitmapButton(m_pScrolWin, CTRL_ID_TGL_BOND_POS_START + i, wxDefaultPosition, wxSize(50, 30));
        m_pTglBmpBondPoss[i]->SetBitmapNormal(bmp);
        pBsBondPos->Add(m_pTglBmpBondPoss[i], 0, wxALIGN_CENTER_HORIZONTAL, 5);
    }
    //
    wxGridSizer* pBsBondType = new wxGridSizer(1, BOND_TYPE_COUNT, 0, 0);
    for (i = 0; i < BOND_TYPE_COUNT; i++)
    {

        //                image.LoadFile(CheckImageFile(wxString::Format(wxT("positions/pos%d.png"), i + 1)), wxBITMAP_TYPE_PNG);
        image.LoadFile(EnvUtil::GetPrgResDir() + wxTRUNK_MOL_IMG_PATH + wxString::Format(wxT("types/type%d.png"), i + 1), wxBITMAP_TYPE_PNG);

        //                image.LoadFile(CheckImageFile(wxString::Format(wxT("types/type%d.png"), i+1)), wxBITMAP_TYPE_PNG);
        bmp = wxBitmap(image);
        m_pTglBmpBondTypes[i] = new ToggleBitmapButton(m_pScrolWin, CTRL_ID_TGL_BOND_TYPE_START + i, wxDefaultPosition, wxSize(50, 30));
        m_pTglBmpBondTypes[i]->SetBitmapNormal(bmp);
    }
    pBsBondType->Add(m_pTglBmpBondTypes[0], 0, wxALIGN_CENTER_HORIZONTAL, 5);
    pBsBondType->Add(m_pTglBmpBondTypes[3], 0, wxALIGN_CENTER_HORIZONTAL, 5);
    pBsBondType->Add(m_pTglBmpBondTypes[1], 0, wxALIGN_CENTER_HORIZONTAL, 5);
    pBsBondType->Add(m_pTglBmpBondTypes[2], 0, wxALIGN_CENTER_HORIZONTAL, 5);
    pBsBondType->Add(m_pTglBmpBondTypes[4], 0, wxALIGN_CENTER_HORIZONTAL, 5);
    //
    wxStaticLine* line = new wxStaticLine(m_pScrolWin, wxID_STATIC, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
    pBsTop->Add(pGsElems, 0, wxALL | wxEXPAND, 2);
    pBsTop->AddSpacer(3);
    pBsTop->Add(pBsBondPos, 0, wxALL, 2);
    pBsTop->Add(line, 0, wxALL | wxEXPAND, 5);
    pBsTop->Add(pBsBondType, 0, wxALL, 2);
    //
    //
    return m_pScrolWin;
}
/*wxPanel* ElemPnl::CreateElementPnl(wxBookCtrlBase *parent) {
        int i,j;
        pPnlRet11 = new wxPanel(parent,wxID_ANY,wxPoint(0,20),wxSize(260, 650));
    m_pScrolWin=new wxScrolledWindow(pPnlRet11,wxID_ANY,wxPoint(0,0),wxSize(260, 650),wxSUNKEN_BORDER|wxScrolledWindowStyle );

//        wxPanel* pPnlRet = new wxPanel(parent,wxID_ANY,wxDefaultPosition, wxSize(260, 1500));
    m_pScrolWin->SetScrollRate( 10, 10 );
        m_pScrolWin->SetVirtualSize( 280, 650 );
    wxPanel* pPnlRet = new wxPanel(m_pScrolWin);
    wxBoxSizer* pBsTop = new wxBoxSizer(wxVERTICAL);
        pPnlRet->SetSizer(pBsTop);

        wxFlexGridSizer *pGsElems= new wxFlexGridSizer(17, 10, 0, 0);
//wxToggleButton fontBtn;
//        wxFont font = fontBtn.GetFont();
//        font.SetPointSize(font.GetPointSize() - 1);

    CreateCategoryElems(pPnlRet, wxT("Non Metal"), wxColour(0,255,0), NON_METALs);
    CreateCategoryElems(pPnlRet, wxT("Inert Gas"), wxColour(205,154,154), INERT_GASSES);
    CreateCategoryElems(pPnlRet, wxT("Main Group Metal"), wxColour(204,255,153), MAIN_GROUP_METALS);
    CreateCategoryElems(pPnlRet, wxT("Alkaline Metal"), wxColour(102,153,255), ALKALINE_METALS);
    CreateCategoryElems(pPnlRet, wxT("Alkaline Earth Metal"), wxColour(153,204,255), ALKALINE_EARTH_METALS);
    CreateCategoryElems(pPnlRet, wxT("Transition Metal"), wxColour(255,153,204), TRANSITION_ELEMS);
    CreateCategoryElems(pPnlRet, wxT("LaAc Metal"), wxColour(192,192,192), LAAC_ELEMS);
    CreateDummyElem(pPnlRet);*/

/*for(i = 0; i < ELEM_TOGGLE_BTN_COUNT; i++) {
m_pTglElems[i] = new wxToggleButton(pPnlRet, CTRL_ID_TGL_ELEM_START + i, ELEM_NAMES[i], wxDefaultPosition, wxSize(25,25));
        m_pTglElems[i]->SetOwnFont(font);
}*/
//Main group elements
/*        for(i = 0; i < ELEM_Ca; i++) {
                if(i == ELEM_H) {
                        for(j = 0; j < 3; j++) {
                                pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
                        }
                        pGsElems->Add(m_pTglElems[ELEM_DUMMY - 1], 0, wxALIGN_CENTER_HORIZONTAL, 0);
                        //pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
                        for(j = 0; j < 4; j++) {
                                pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
                        }
                }
                if(i == ELEM_Be || i == ELEM_Mg) {
            for(j = 0; j < 2; j++) {
                                pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
                        }
        }
                pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
        }
        for(j = 0; j < 2; j++) {
        pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
    }
    for(i=ELEM_Ga-1;i<ELEM_Sr;i++){
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for(j = 0; j < 2; j++) {
        pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
    }
    for(i=ELEM_In-1;i<ELEM_Ba;i++){
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for(j = 0; j < 2; j++) {
        pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
    }
    for(i=ELEM_Tl-1;i<ELEM_Ra;i++){
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for(j = 0; j < 8; j++) {
        pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
    }
    //Vice-group elements
        for(i=ELEM_Sc-1;i<ELEM_Zn;i++){
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for(i=ELEM_Y-1;i<ELEM_Cd;i++){
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    pGsElems->Add(m_pTglElems[ELEM_La-1], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    for(i=ELEM_Hf-1;i<ELEM_Hg;i++){
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    pGsElems->Add(m_pTglElems[ELEM_Ac-1], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    for(i=ELEM_Rf-1;i<ELEM_Mt;i++){
        pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
    }
    for(j = 0; j < 3; j++) {
        pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
    }
    //
    for(i =ELEM_Ce-1; i < ELEM_Lu; i++) {
                pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
        }
    for(i = 0; i < 6; i++) {
        pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
    }
    for(i =ELEM_Th-1; i < ELEM_Lr; i++) {
                pGsElems->Add(m_pTglElems[i], 0, wxALIGN_CENTER_HORIZONTAL, 0);
        }
    for(i = 0; i < 6; i++) {
        pGsElems->Add(new wxStaticText(pPnlRet, wxID_ANY, wxT("")));
    }

    //
    wxImage image;
        wxBitmap bmp;
        wxGridSizer *pBsBondPos = new wxGridSizer(2, 5, 0, 0);
        for(i = 0; i < BOND_POS_COUNT; i++) {
//                EnvUtil::GetMolcasDir()+wxT("/gui")+ wxT("/res/mol/images/") + wxT("tadd.png")

//                image.LoadFile(CheckImageFile(wxString::Format(wxT("positions/pos%d.png"), i + 1)), wxBITMAP_TYPE_PNG);
                image.LoadFile(EnvUtil::GetPrgRootDir()+wxTRUNK_MOL_IMG_PATH+wxString::Format(wxT("positions/pos%d.png"), i + 1), wxBITMAP_TYPE_PNG);

                bmp = wxBitmap(image);
                m_pTglBmpBondPoss[i] = new ToggleBitmapButton(pPnlRet, CTRL_ID_TGL_BOND_POS_START + i, wxDefaultPosition, wxSize(50,30));
                m_pTglBmpBondPoss[i]->SetBitmapNormal(bmp);
                pBsBondPos->Add(m_pTglBmpBondPoss[i], 0, wxALIGN_CENTER_HORIZONTAL, 5);
        }
    //
    wxGridSizer *pBsBondType = new wxGridSizer(1, BOND_TYPE_COUNT, 0, 0);
        for(i = 0; i < BOND_TYPE_COUNT; i++) {

//                image.LoadFile(CheckImageFile(wxString::Format(wxT("positions/pos%d.png"), i + 1)), wxBITMAP_TYPE_PNG);
                image.LoadFile(EnvUtil::GetPrgRootDir()+wxTRUNK_MOL_IMG_PATH+wxString::Format(wxT("types/type%d.png"), i + 1), wxBITMAP_TYPE_PNG);

//                image.LoadFile(CheckImageFile(wxString::Format(wxT("types/type%d.png"), i+1)), wxBITMAP_TYPE_PNG);
                bmp = wxBitmap(image);
                m_pTglBmpBondTypes[i] = new ToggleBitmapButton(pPnlRet, CTRL_ID_TGL_BOND_TYPE_START + i, wxDefaultPosition, wxSize(50, 30));
                m_pTglBmpBondTypes[i]->SetBitmapNormal(bmp);
        }
        pBsBondType->Add(m_pTglBmpBondTypes[0], 0, wxALIGN_CENTER_HORIZONTAL, 5);
        pBsBondType->Add(m_pTglBmpBondTypes[3], 0, wxALIGN_CENTER_HORIZONTAL, 5);
        pBsBondType->Add(m_pTglBmpBondTypes[1], 0, wxALIGN_CENTER_HORIZONTAL, 5);
        pBsBondType->Add(m_pTglBmpBondTypes[2], 0, wxALIGN_CENTER_HORIZONTAL, 5);
        pBsBondType->Add(m_pTglBmpBondTypes[4], 0, wxALIGN_CENTER_HORIZONTAL, 5);
    //
    wxStaticLine* line = new wxStaticLine(pPnlRet, wxID_STATIC, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
    pBsTop->Add(pGsElems, 0, wxALL | wxEXPAND, 2);
    pBsTop->AddSpacer(3);
        pBsTop->Add(pBsBondPos, 0, wxALL, 2);
        pBsTop->Add(line, 0, wxALL | wxEXPAND, 5);
        pBsTop->Add(pBsBondType, 0, wxALL, 2);
    //
    //
        return pPnlRet;
}*/

void ElemPnl::CreateCategoryElems(wxScrolledWindow* parent, wxString category, wxColour color, const ElemId* elems)
{
    int i = 0, j = 0;
    //begin : hurukun :17/12/2009    comment the lines bellow to stop warnings.
    //    wxToggleButton fontBtn;
    //        wxFont font = fontBtn.GetFont();
    //        font.SetPointSize(font.GetPointSize() - 1);
    //end   : hurukun : 17/12/2009
    wxFont font(8, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    while (elems[i] != ELEM_NULL && elems[i] <= ELEM_DUMMY)
    {
        j = elems[i] - 1;
        //m_pTglElems[j] = new wxToggleButton(parent, CTRL_ID_TGL_ELEM_START + j, ELEM_NAMES[j], wxDefaultPosition, wxSize(25,25));
        m_pTglElems[j] = new ToggleBitmapButton(parent, CTRL_ID_TGL_ELEM_START + j, wxDefaultPosition, wxSize(25, 25));
        m_pTglElems[j]->SetLabel(ELEM_NAMES[j]);
        m_pTglElems[j]->SetOwnFont(font);
        m_pTglElems[j]->SetToolTip(ELEM_NAMES[j] + wxString::Format(wxT(":%d"), (int)elems[i]));
        m_pTglElems[j]->SetBackgroundColour(color);
        i++;
    }
}

void ElemPnl::CreateDummyElem(wxScrolledWindow* parent)
{
    //begin : hurukun :17/12/2009    comment the lines bellow to stop warnings.
    //    wxToggleButton fontBtn;
    //        wxFont font = fontBtn.GetFont();
    //        font.SetPointSize(font.GetPointSize() - 1);
    //end   : hurukun : 17/12/2009
    wxFont font(8, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    int j = ELEM_DUMMY - 1;
    wxBitmap bitmap;
    m_pTglElems[j] = new ToggleBitmapButton(parent, CTRL_ID_TGL_ELEM_START + j, wxDefaultPosition, wxSize(25, 25));
    m_pTglElems[j]->SetLabel(wxT("X"));
    m_pTglElems[j]->SetOwnFont(font);
    m_pTglElems[j]->SetToolTip(wxT("Dummy Atom"));
    m_pTglElems[j]->SetBackgroundColour(wxColour(255, 255, 255));
}

//wangyuanlong     2010/11/23    end
void ElemPnl::InitDefaultAtomPosition(void)
{
    int i = 0;
    for (i = 0; i < ELEM_TOGGLE_BTN_COUNT; i++)
        m_defaultAtomBondPoss[i] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_H - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_He - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Li - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Be - 1] = BOND_POS_2;
    m_defaultAtomBondPoss[ELEM_B - 1] = BOND_POS_3;
    m_defaultAtomBondPoss[ELEM_C - 1] = BOND_POS_6;
    m_defaultAtomBondPoss[ELEM_N - 1] = BOND_POS_5;
    m_defaultAtomBondPoss[ELEM_O - 1] = BOND_POS_4;
    m_defaultAtomBondPoss[ELEM_F - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Ne - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Na - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Mg - 1] = BOND_POS_2;
    m_defaultAtomBondPoss[ELEM_Al - 1] = BOND_POS_3;
    m_defaultAtomBondPoss[ELEM_Si - 1] = BOND_POS_6;
    m_defaultAtomBondPoss[ELEM_P - 1] = BOND_POS_5;
    m_defaultAtomBondPoss[ELEM_S - 1] = BOND_POS_4;
    m_defaultAtomBondPoss[ELEM_Cl - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Ar - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_K - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Ca - 1] = BOND_POS_2;
    m_defaultAtomBondPoss[ELEM_Ga - 1] = BOND_POS_3;
    m_defaultAtomBondPoss[ELEM_Ge - 1] = BOND_POS_6;
    m_defaultAtomBondPoss[ELEM_As - 1] = BOND_POS_5;
    m_defaultAtomBondPoss[ELEM_Se - 1] = BOND_POS_4;
    m_defaultAtomBondPoss[ELEM_Br - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Kr - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Rb - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Sr - 1] = BOND_POS_2;
    m_defaultAtomBondPoss[ELEM_In - 1] = BOND_POS_3;
    m_defaultAtomBondPoss[ELEM_Sn - 1] = BOND_POS_6;

    m_defaultAtomBondPoss[ELEM_Sb - 1] = BOND_POS_5;
    m_defaultAtomBondPoss[ELEM_Te - 1] = BOND_POS_4;
    m_defaultAtomBondPoss[ELEM_I - 1] = BOND_POS_1;
    m_defaultAtomBondPoss[ELEM_Xe - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Sc - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Ti - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_V - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Cr - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Mn - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Fe - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Co - 1] = BOND_POS_10;

    m_defaultAtomBondPoss[ELEM_Ni - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Cu - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Zn - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Y - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Zr - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Nb - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Mo - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Tc - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Ru - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Rh - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Pd - 1] = BOND_POS_10;

    m_defaultAtomBondPoss[ELEM_Ag - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Cd - 1] = BOND_POS_10;

    m_defaultAtomBondPoss[ELEM_La - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Hf - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Ta - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_W - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Re - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Os - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Ir - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Pt - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Au - 1] = BOND_POS_10;
    m_defaultAtomBondPoss[ELEM_Hg - 1] = BOND_POS_10;

    m_defaultAtomBondPoss[ELEM_DUMMY - 1] = BOND_POS_NULL;
}

void ElemPnl::SetDefaultValues(void)
{
    int i;
    ElemPnlData::Get()->SetElemId(ELEM_C);
    ElemPnlData::Get()->SetBondPos((BondPos)(BOND_POS_6));
    ElemPnlData::Get()->SetBondType(BOND_TYPE_1);
    //
    m_pTglElems[ELEM_C - 1]->SetValue(true);
    //UnselectedOtherElemButtons(ELEM_C - 1);
    m_pTglBmpBondPoss[BOND_POS_6 - 1]->SetValue(true);
    UnselectedOtherBondPositionButtons(BOND_POS_6 - 1);
    m_pTglBmpBondTypes[0]->SetValue(true);
    for (i = 1; i < BOND_TYPE_COUNT; i++)
    {
        m_pTglBmpBondTypes[i]->SetValue(false);
    }
    RefreshGraphics();
}

void ElemPnl::RefreshGraphics(void)
{
    //        super->RefreshGraphics();
    m_molData.GetActiveCtrlInst()->LoadPreviewData();
    m_pGLPreview->Draw();
}

void ElemPnl::OnToggleButton(wxCommandEvent& event)
{
    //   wxMessageBox(wxT("ElemPnl::OnToggleButton"));
    //   int    x=0,y=0;
    //  m_pScrolWin->GetViewStart(&x,&y);
    //   tog_btn_clk=true;
    int btnId = event.GetId();
    if (btnId >= CTRL_ID_TGL_ELEM_START && btnId <= CTRL_ID_TGL_ELEM_END)
    {
        btnId -= CTRL_ID_TGL_ELEM_START;
        if (!m_pTglElems[btnId]->GetValue())
        {
            //when click a selected button, cannot change its status
            m_pTglElems[btnId]->SetValue(true);
            if (btnId == ELEM_DUMMY - 1)
            {
                ElemPnlData::Get()->SetBondPos((BondPos)m_defaultAtomBondPoss[btnId]);
                UnselectedOtherBondPositionButtons(BP_NULL_VALUE);
                ElemPnlData::Get()->ClearFragment();
                RefreshGraphics();
            }
        }
        else
        {
            UnselectedOtherElemButtons(btnId);
            ElemPnlData::Get()->SetElemId((ElemId)(btnId + 1));
            if (m_defaultAtomBondPoss[btnId] != BP_NULL_VALUE)
            {
                ElemPnlData::Get()->SetBondPos((BondPos)m_defaultAtomBondPoss[btnId]);
                UnselectedOtherBondPositionButtons(BP_NULL_VALUE);
                if (btnId != ELEM_DUMMY - 1)
                {
                    m_pTglBmpBondPoss[m_defaultAtomBondPoss[btnId] - 1]->SetValue(true);
                }
            }
            ElemPnlData::Get()->ClearFragment();
            RefreshGraphics();
            //if(btnId == ELEM_DUMMY - 1 && ElemPnlData::Get()->GetActionType() != ACTION_INSERT_FRAGMENT) {
            //        wxGetApp().GetMainFrame()->DoMoleculeBuildCommon(MENU_ID_BUILD_INSERT_FRAGMENT, false, true);
            //        wxGetApp().GetMainFrame()->SetActionType(ACTION_INSERT_FRAGMENT, BP_NULL_VALUE);
            //}
        }
    }
    else if (btnId >= CTRL_ID_TGL_BOND_POS_START && btnId <=  CTRL_ID_TGL_BOND_POS_END)
    {
        btnId -= CTRL_ID_TGL_BOND_POS_START;
        if (!m_pTglBmpBondPoss[btnId]->GetValue())
        {
            //when click a selected button, cannot change its status
            m_pTglBmpBondPoss[btnId]->SetValue(true);
        }
        else
        {
            UnselectedOtherBondPositionButtons(btnId);
            ElemPnlData::Get()->SetBondPos((BondPos)(btnId + 1));
            ElemPnlData::Get()->ClearFragment();
            RefreshGraphics();
        }
    }
    else if (btnId >= CTRL_ID_TGL_BOND_TYPE_START && btnId <=  CTRL_ID_TGL_BOND_TYPE_END)
    {
        btnId -= CTRL_ID_TGL_BOND_TYPE_START;
        if (!m_pTglBmpBondTypes[btnId]->GetValue())
        {
            //when click a selected button, cannot change its status
            m_pTglBmpBondTypes[btnId]->SetValue(true);
        }
        else
        {
            for (int i = 0; i < BOND_TYPE_COUNT; i++)
            {
                if (i != btnId)
                {
                    m_pTglBmpBondTypes[i]->SetValue(false);
                }
            }
            ElemPnlData::Get()->SetBondType((BondType)(btnId + 1));
        }
    }
    //if(!(ACTION_ADD_ATOM == ElemPnlData::Get()->GetActionType() || ACTION_INSERT_ATOM == ElemPnlData::Get()->GetActionType())) {
    //        wxGetApp().GetMainFrame()->DoMoleculeBuildCommon(MENU_ID_MOLECULE_ADD, false, true);
    //}
    //        m_pScrolWin->Scroll(x,y);
}

void ElemPnl::UnselectedOtherElemButtons(int btnId)
{
    for (int i = 0; i < ELEM_TOGGLE_BTN_COUNT; i++)
    {
        if (i != btnId && NULL != m_pTglElems[i])
        {
            m_pTglElems[i]->SetValue(false);
        }
    }
}

void ElemPnl::UnselectedOtherBondPositionButtons(int btnId)
{
    for (int i = 0; i < BOND_POS_COUNT; i++)
    {
        if ( i != btnId)
        {
            m_pTglBmpBondPoss[i]->SetValue(false);
        }
    }
}

void ElemPnl::OnListBox(wxCommandEvent& event)
{
    //   int    x=0,y=0;
    //   m_pScrolWin->GetViewStart(&x,&y);
    if (m_PageChanged)
    {
        //                m_pScrolWin->Scroll(0,0);    //wangyuanlong     2010/11/5    end
        m_PageChanged = false;
    }
    if (event.IsSelection())
    {
        DoFragmentCommon(event.GetId() - CTRL_ID_LB_GROUPS);
    }

}

void ElemPnl::DoFragmentCommon(int listBoxIndex)
{
    if (m_pLbFragments[listBoxIndex]->GetSelection()  == wxNOT_FOUND) return;

    wxString selectdFragment = m_pLbFragments[listBoxIndex]->GetStringSelection();
    if (!StringUtil::IsEmptyString(selectdFragment))
    {
        selectdFragment.Replace(wxT(" "), wxT("_"));
        ElemPnlData::Get()->SetFragment(CatenateDataFile(listBoxIndex, selectdFragment));
        RefreshGraphics();
    }
}

void ElemPnl::OnNotebookPageChanged(wxNotebookEvent& event)
{
    //   if(event.GetSelection()==2)
    //      m_pScrolWin->SetScrollbars(10,10,26,110);
    //  else
    //      m_pScrolWin->SetScrollbars(10,10,26,55);
    int i = 0;
    if (event.GetSelection() == 0)
    {
        //element fragment page
        for (i = 0; i < ELEM_TOGGLE_BTN_COUNT; i++)
        {
            if (m_pTglElems[i]->GetValue())
            {
                ElemPnlData::Get()->SetElemId((ElemId)(i + 1));
                break;
            }
        }
        for (i = 0; i < BOND_POS_COUNT; i++)
        {
            if (m_pTglBmpBondPoss[i]->GetValue())
            {
                ElemPnlData::Get()->SetBondPos((BondPos)(i + 1));
                break;
            }
        }
        ElemPnlData::Get()->ClearFragment();
        RefreshGraphics();
    }
    else
    {
        //other pages
        DoFragmentCommon(event.GetSelection() - 1);
    }
    //  m_pScrolWin->Scroll(0,0);//wangyuanlong     2010/11/5    end
    m_PageChanged = true;
}

bool ElemPnl::AddCustomTemplate(wxString name, AtomBondArray& atomBondArray)
{
    if (wxNOT_FOUND != m_pLbFragments[CUSTOM_FRAGMENT_INDEX]->FindString(name))
    {
        return false;
    }
    else
    {
        MolFile::CreateTemplateFile(CatenateDataFile(CUSTOM_FRAGMENT_INDEX, name) + wxT(".dll"), atomBondArray);
        m_pLbFragments[CUSTOM_FRAGMENT_INDEX]->Append(name);
        //                m_pScrolWin->Scroll(0,0);//wangyuanlong     2010/11/5    end
    }
    return true;
}

wxString ElemPnl::DeleteSelectedCustomTemplate(bool isEnquiry)
{
    wxString tempName = wxEmptyString;
    int oldpos = 0;
    oldpos = m_pLbFragments[CUSTOM_FRAGMENT_INDEX]->GetSelection();
    if (m_pLbFragments[CUSTOM_FRAGMENT_INDEX]->IsShown())
    {
        tempName = m_pLbFragments[CUSTOM_FRAGMENT_INDEX]->GetStringSelection();
        if (isEnquiry)
        {
            return tempName;
        }
        else if (!tempName.IsEmpty())
        {
            if (wxRemoveFile(CatenateDataFile(CUSTOM_FRAGMENT_INDEX, tempName)  + wxT(".dll")))
            {
                m_pLbFragments[CUSTOM_FRAGMENT_INDEX]->Delete(oldpos);
                oldpos = oldpos - 1;
                if (oldpos >= 0)
                {
                    m_pLbFragments[CUSTOM_FRAGMENT_INDEX]->SetSelection(oldpos);
                    DoFragmentCommon(CUSTOM_FRAGMENT_INDEX);
                }
                //                                m_pScrolWin->Scroll(0,0);//wangyuanlong     2010/11/5    end
            }
        }
    }
    return tempName;
}
