/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "auisharedwidgets.h"

#include "simuprjdef.h"

#include "tools.h"
#include "envutil.h"
#include "uiprops.h"
#include "ctrlids.h"
#include "resource.h"
#include "manager.h"

const wxString STR_TB_FILE = wxT("TbFile");
const wxString STR_TB_BUILD = wxT("TbBuild");
const wxString STR_TB_GEOMETRY = wxT("TbGeometry");
const wxString STR_TB_CALCULATE = wxT("TbCalculate");

const wxString STR_PNL_FRAGMENTS = wxT("PnlFragments");
const wxString STR_PNL_GLCANVASMAIN = wxT("GLCanvasMain");
const wxString STR_PNL_DELETE = wxT("PnlDelete");
const wxString STR_PNL_MEASURE = wxT("PnlMeasure");
// const wxString STR_PNL_SYMMETRIZE = wxT("PnlSymmetrize");
const wxString STR_PNL_PROGRESSBAR = wxT("PnlProgressBar");
//const wxString STR_PNL_CONSTRAIN = wxT("PnlConstrain");
const wxString STR_PNL_MIRROR = wxT("PnlMirror");
const wxString STR_PNL_DESIGNBOND=wxT("PnlDesignBond");
static AuiSharedWidgets* auiSharedWidgets = NULL;

AuiSharedWidgets::AuiSharedWidgets() {
        m_pAuiMgr = NULL;
        m_pMenuBar = NULL;
        m_winptrHash.clear();
        m_pPnlElem = NULL;
        m_pPnlMeasure = NULL;
        m_pPnlMirror = NULL;
        m_pPnlDelete = NULL;
        m_pPnlProgressBar=NULL;
}

AuiSharedWidgets::~AuiSharedWidgets() {
}

AuiSharedWidgets* AuiSharedWidgets::Get(void) {
        if(!auiSharedWidgets)
                auiSharedWidgets = new AuiSharedWidgets;
    return auiSharedWidgets;
}

void AuiSharedWidgets::Free(void) {
    delete auiSharedWidgets;
    auiSharedWidgets = NULL;
}

void AuiSharedWidgets::SetAuiManager(wxAuiManager* pAuiMgr) {
        m_pAuiMgr = pAuiMgr;
}

void AuiSharedWidgets::BuildWidgets(wxWindow* parent) {
    if(parent==NULL)
        return;
        m_pPnlElem = new ElemPnl(parent, wxID_ANY, wxPoint(0,0), wxSize(275,550));
        m_pPnlElem->SetDefaultValues();
        m_pPnlProgressBar=new ProgressBarPnl(parent,wxID_ANY,wxPoint(0,0),wxSize(800,50));
        m_pPnlProgressBar->InitPanel();
        m_pPnlMeasure = new MeasurePnl(parent, wxID_ANY, wxPoint(0,0), wxSize(800, 50));
        m_pPnlMeasure->InitPanel();
        m_pPnlDesignBond = new DesignBondPnl(parent, wxID_ANY, wxPoint(0,0), wxSize(800, 50));
        m_pPnlDesignBond->InitPanel();

        // m_pPnlSymmetrize = new SymmetrizePnl(parent, wxID_ANY, wxPoint(0, 0), wxSize(800, 50));
        // m_pPnlSymmetrize->InitPanel();

        m_pPnlMirror = new MirrorPnl(parent, wxID_ANY, wxPoint(0,0), wxSize(300, 50));
        m_pPnlDelete = new DeletePnl(parent, wxID_ANY, wxPoint(0,0), wxSize(800, 50));
        m_pPnlDelete->UpdateControls(ACTION_DELETE_FRAGMENT);
    //begin : hurukun : 06/11/2009
//
//        m_pAuiMgr->AddPane(m_pPnlElem, wxAuiPaneInfo().Name(STR_PNL_FRAGMENTS).Caption(wxT("Building Blocks")). //SONG changed the fragment to blocks for workshop2009
//                        Right().CloseButton(true).MaximizeButton(true).TopDockable(false).BottomDockable(false).LeftDockable(platform::windows).RightDockable(platform::windows).MinSize(470,595).Float().FloatingPosition(800,85).Hide());
        m_pAuiMgr->AddPane(m_pPnlElem, wxAuiPaneInfo().Name(STR_PNL_FRAGMENTS).Caption(wxT("Building Blocks")). //SONG changed the fragment to blocks for workshop2009
                        BestSize(wxSize(280, 730)).CloseButton(false).TopDockable(false).BottomDockable(false).MinSize(275,400).MaxSize(280,600).Dock().Right().Hide().Layer(1));
        //end   : hurukun : 06/11/2009
//                        Right().CloseButton(true).MaximizeButton(true).TopDockable(false).BottomDockable(false).LeftDockable(platform::windows).RightDockable(platform::windows).MinSize(470,595).Dock().Hide()); //SONG add this in order this panel is dock on the screen
//

        m_pAuiMgr->AddPane(m_pPnlMeasure, wxAuiPaneInfo().Name(STR_PNL_MEASURE).Caption(wxT("Measure Panel")).
            Bottom().CloseButton(false).MaximizeButton(false).LeftDockable(false).RightDockable(false).MinSize(800,50).FloatingSize(800,50).Hide());
        // m_pAuiMgr->AddPane(m_pPnlSymmetrize, wxAuiPaneInfo().Name(STR_PNL_SYMMETRIZE).Caption(wxT("Symmetrize Panel")).
        //     Bottom().CloseButton(false).MaximizeButton(false).LeftDockable(false).RightDockable(false).MinSize(800,50).FloatingSize(800,50).Hide());
        m_pAuiMgr->AddPane(m_pPnlDesignBond, wxAuiPaneInfo().Name(STR_PNL_DESIGNBOND).Caption(wxT("DesignBond Panel")).
            Bottom().CloseButton(false).MaximizeButton(false).LeftDockable(false).RightDockable(false).MinSize(800,50).FloatingSize(800,50).Hide());

        m_pAuiMgr->AddPane(m_pPnlProgressBar, wxAuiPaneInfo().Name(STR_PNL_PROGRESSBAR).Caption(wxT("ProgressBar Panel")).
            Bottom().CloseButton(false).MaximizeButton(false).LeftDockable(false).RightDockable(false).MinSize(800,50).FloatingSize(800,50).Hide());
        m_pAuiMgr->AddPane(m_pPnlMirror, wxAuiPaneInfo().Name(STR_PNL_MIRROR).Caption(wxT("Mirror Panel")).
            Bottom().CloseButton(false).MaximizeButton(false).MinSize(1024,50).FloatingSize(1024,50).Hide());
    m_pAuiMgr->AddPane(m_pPnlDelete, wxAuiPaneInfo().Name(STR_PNL_DELETE).Caption(wxT("Delete Panel")).
            Bottom().CloseButton(false).MaximizeButton(false).MinSize(700,50).FloatingSize(700,50).Hide());
    //m_pAuiMgr->AddPane(m_pPnlVibration, wxAuiPaneInfo().Name(STR_PNL_VIBRATION).Caption(wxT("Vibration")).
        //                Right().CloseButton(true).MaximizeButton(false).TopDockable(false).BottomDockable(false).MinSize(300,195).Hide());
    //m_pAuiMgr->AddPane(m_pPnlSurface, wxAuiPaneInfo().Name(STR_PNL_SURFACE).Caption(wxT("Surface/MO")).
        //                Right().CloseButton(true).MaximizeButton(false).TopDockable(false).BottomDockable(false).MinSize(330,195).Hide());
}

wxWindow* AuiSharedWidgets::GetWindow(const wxString name) const {
        if(m_winptrHash.find(name) != m_winptrHash.end()) {
                return m_winptrHash.find(name)->second;
        }
        return NULL;
}

void AuiSharedWidgets::AddWindow(const wxString name, wxString caption, wxWindow* pWin, int minSizeX, int minSizeY) {
        m_winptrHash[name] = pWin;
        m_pAuiMgr->AddPane(pWin, wxAuiPaneInfo().Name(name).Caption(caption).Right().CloseButton(false).
                MaximizeButton(false).TopDockable(false).BottomDockable(false).MinSize(minSizeX, minSizeY).FloatingSize(minSizeX, minSizeY).Hide());
}

void AuiSharedWidgets::BuildMenus(wxMenuBar* pMenuBar) {
    if(pMenuBar == NULL) return;
    m_pMenuBar = pMenuBar;
        //wxMenu* menu = NULL;
        //unsigned i;
    int pos = wxNOT_FOUND;

        /*if(GetType() == VIEW_GL_BUILD) {
                pos = m_pMenuBar->FindMenu(wxT("&File"));
                if(pos != wxNOT_FOUND) {
                        menu = m_pMenuBar->GetMenu(pos);
                wxMenuItemList& items = menu->GetMenuItems();
                for(i = 0; i < items.GetCount(); i++) {
                                if(items[i]->GetLabel().Cmp(wxT("Save All Views")) == 0) {
                                        menu->Insert(i, wxID_SAVE, wxT("&Save"));
                                        break;
                                }
                        }
                }else {
                        Tools::ShowError(wxT("Could not find File menu!"));
                }
        }*/
                wxMenu* viewMenu = new wxMenu;
                viewMenu->AppendCheckItem(MENU_ID_BUILD_VIEW, wxT("&View Structure"));
        viewMenu->Append(MENU_ID_VIEW_CAPTURE_IMAGE, wxT("Capture Image"));      // mayingbo
//        viewMenu->Enable(MENU_ID_VIEW_CAPTURE_IMAGE, false);

        viewMenu->AppendSeparator();
        viewMenu->AppendRadioItem(MENU_ID_MODEL_WIRE, wxT("&Wire"));
        viewMenu->AppendRadioItem(MENU_ID_MODEL_TUBE, wxT("&Tube"));
        viewMenu->AppendRadioItem(MENU_ID_MODEL_BALLSPOKE, wxT("Ball and S&poke"));
        viewMenu->AppendRadioItem(MENU_ID_MODEL_BALLWIRE, wxT("&Ball and Wire"));
        viewMenu->AppendRadioItem(MENU_ID_MODEL_SPACEFILLING, wxT("&Space Filling"));
        viewMenu->AppendSeparator();
        viewMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_HYDROGENS, wxT("&Hydrogens"));
        viewMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_GLOBAL_LABELS, wxT("Global &Labels"));
        viewMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_ATOM_LABELS, wxT("Atom L&abels"));
        viewMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_AXES, wxT("A&xes"));
        viewMenu->AppendSeparator();
        //Symmetry tool using Molcas GV
        //viewMenu->Append(MENU_ID_CALC_SYMMETRY, wxT("Symmetry"));

        wxMenu* layersMenu = new wxMenu;
        layersMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_LAYERS_SHOW, wxT("Show"));
        layersMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_LAYERS_HIGH, wxT("High"));
        layersMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_LAYERS_MEDIUM, wxT("Medium"));
        layersMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_LAYERS_LOW, wxT("Low"));
   //---------------XIA for workshop2009---------
        // modelMenu->Append(MENU_ID_MODEL_SHOW_LAYERS, wxT("La&yers"), layersMenu);
  //  modelMenu->AppendCheckItem(MENU_ID_MODEL_SHOW_HYDROGEN_BONDS, wxT("Hydrogen Bon&ds"));

        wxMenu *buildMenu = new wxMenu;
        buildMenu->Append(MENU_ID_EDIT_UNDO, wxT("&Undo\tCtrl-Z"));
        buildMenu->Append(MENU_ID_EDIT_REDO, wxT("&Redo\tCtrl-Y"));
        buildMenu->AppendSeparator();
    buildMenu->AppendCheckItem(MENU_ID_BUILD_ADD_FRAGMENT, wxT("&Add Blocks")); //SONG changed the fragment to blocks for workshop2009
    buildMenu->AppendCheckItem(MENU_ID_BUILD_INSERT_FRAGMENT, wxT("&Insert Blocks")); //SONG changed the fragment to blocks for workshop2009
    buildMenu->AppendCheckItem(MENU_ID_BUILD_DELETE_FRAGMENT, wxT("&Delete Blocks")); //SONG changed the fragment to blocks for workshop2009
    buildMenu->AppendCheckItem(MENU_ID_BUILD_MAKE_BOND, wxT("&Make Bond"));
    buildMenu->AppendCheckItem(MENU_ID_BUILD_BREAK_BOND, wxT("&Break Bond"));
    buildMenu->AppendCheckItem(MENU_ID_BUILD_DESIGN_BOND, wxT("Design Bond"));
    buildMenu->AppendCheckItem(MENU_ID_GEOMETRY_DEFINE_DUMMY, wxT("Define Dummy"));
    buildMenu->AppendSeparator();
    buildMenu->Append(MENU_ID_IMPORT_STRUCTURE, wxT("Import Structure"));
    buildMenu->AppendCheckItem(MENU_ID_GEOMETRY_MIRROR, wxT("Mirror"));
    buildMenu->Append(MENU_ID_EDIT_CLEAR, wxT("Clear"));
    buildMenu->AppendSeparator();
    buildMenu->Append(MENU_ID_BUILD_CREATE_TEMPLATE, wxT("Create Custom Blocks...")); //SONG changed the fragment to blocks for workshop2009
    buildMenu->Append(MENU_ID_BUILD_DELETE_TEMPLATE, wxT("Delete Custom Blocks")); //SONG changed the fragment to blocks for workshop2009

    wxMenu *geoMenu = new wxMenu;
    geoMenu->AppendCheckItem(MENU_ID_GEOMETRY_MEASURE, wxT("&Measure"));
    geoMenu->AppendCheckItem(MENU_ID_GEOMETRY_CONSTRAIN, wxT("Constrain"));
    // geoMenu->AppendCheckItem(MENU_ID_GEOMETRY_SYMMETRIZE, wxT("Symmetrize"));
    geoMenu->Append(MENU_ID_EDIT_ATOM_LIST, wxT("Coordinate"));
    //------------XIA 09workshop------
        //geoMenu->AppendCheckItem(MENU_ID_GEOMETRY_CONSTRAIN, wxT("&Constrain"));
        //-----------------------
//    geoMenu->AppendCheckItem(MENU_ID_GEOMETRY_DEFINE_DUMMY, wxT("Define Dummy"));
    geoMenu->AppendSeparator();

    // geoMenu->Append(MENU_ID_CALC_MM_OPTIMIZATION, wxT("Minimize"));
    geoMenu->Append(MENU_ID_CALC_MOPAC_OPTIMIZATION, wxT("Mopac Optimization"));


    pos = pMenuBar->FindMenu(wxT("&Window"));
    if(pos == wxNOT_FOUND) {
        pos = pMenuBar->FindMenu(wxT("&Job"));
    }
    if(pos != wxNOT_FOUND) {
        pMenuBar->Insert(pos, buildMenu, wxT("&Build"));
        pMenuBar->Insert(pos + 1, geoMenu, wxT("&Geometry"));
        pMenuBar->Insert(pos + 2, viewMenu, wxT("&View"));
    }
}

void AuiSharedWidgets::BuildToolBars(wxWindow* parent, bool isStandAlone) {
    wxBitmap bmp;

    long style = GetUIProps()->menuProps.isToolbarStyleText | GetUIProps()->menuProps.isToolbarStyleFlat;
    // if(GetUIProps()->menuProps.isToolbarStyleText) {
    //     style |= wxTB_TEXT;
    // }
    // if(GetUIProps()->menuProps.isToolbarStyleFlat) {
    //     style |= wxTB_FLAT;
    // }

    m_pToolbars[0] = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("new.png"), wxBITMAP_TYPE_PNG);
    m_pToolbars[0]->AddTool(TOOL_ID_FILE_NEW, wxT("New"), bmp, wxT("New Molecule"));
//    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("open.png"), wxBITMAP_TYPE_PNG);
//    m_pToolbars[0]->AddTool(TOOL_ID_FILE_OPEN, wxT("Open"), bmp, wxT("Open Molecule File"));
//    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("save.png"), wxBITMAP_TYPE_PNG);
//    m_pToolbars[0]->AddTool(TOOL_ID_FILE_SAVE, wxT("Save"), bmp, wxT("Save Molecule File"));
//    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_PRJ_IMG_PATH+ wxT("close.png"), wxBITMAP_TYPE_PNG);
//    m_pToolbars[0]->AddTool(TOOL_ID_FILE_CLOSE, wxT("Close"), bmp);
    m_pToolbars[0]->Realize();

    //the second toolbar must be build tool, as has been used in UpdateMenuBuild;
/*    m_pToolbars[1] = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
    bmp = wxBitmap(CheckImageFile(wxT("tview.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_VIEW, wxT("View"), bmp, wxT("View Molecule"), wxITEM_CHECK);
    m_pToolbars[1]->AddSeparator();
    bmp = wxBitmap(CheckImageFile(wxT("tadd.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_ADD_FRAGMENT, wxT("Add"), bmp,wxT("Add Blocks"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("tinsert.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_INSERT_FRAGMENT, wxT("Insert"), bmp,wxT("Insert Blocks"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("tdelete.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_DELETE_FRAGMENT, wxT("Delete"), bmp,wxT("Delete Blocks"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("tmakebond.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_MAKE_BOND, wxT("Make"), bmp,wxT("Make Bond"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("tbreakbond.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_BREAK_BOND, wxT("Break"), bmp,wxT("Break Bond"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("tlayer.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[1]->AddSeparator();
    //----------XIA--------
        m_pToolbars[1]->AddTool(TOOL_ID_BUILD_SELECT_LAYER, wxT("Layer"), bmp,wxT("Select Layer"), wxITEM_CHECK);
//        EnableTool(TOOL_ID_BUILD_SELECT_LAYER, false);
    m_pToolbars[1]->Realize();

    m_pToolbars[2] = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
    bmp = wxBitmap(CheckImageFile(wxT("measure.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[2]->AddTool(TOOL_ID_GEOMETRY_MEASURE, wxT("Measure"), bmp, wxT("Measure position, distance, angle, and dihedral"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("constrain.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[2]->AddTool(TOOL_ID_GEOMETRY_CONSTRAIN, wxT("Constrain"), bmp, wxT("Constrain distance, angle, and dihedral"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("definedummy.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[2]->AddTool(TOOL_ID_GEOMETRY_DEFINE_DUMMY, wxT("Dummy"), bmp, wxT("Define Dummy"), wxITEM_CHECK);
    bmp = wxBitmap(CheckImageFile(wxT("mirror.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[2]->AddTool(TOOL_ID_GEOMETRY_MIRROR, wxT("Mirror"), bmp, wxT("Mirror Molecule"), wxITEM_CHECK);
    m_pToolbars[2]->Realize();

    m_pToolbars[3] = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
    bmp = wxBitmap(CheckImageFile(wxT("energy.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[3]->AddTool(TOOL_ID_CALC_MM_OPTIMIZATION, wxT("MM"), bmp, wxT("Optimize Geometry"));
    bmp = wxBitmap(CheckImageFile(wxT("symmetry.png")), wxBITMAP_TYPE_PNG);
    m_pToolbars[3]->AddTool(TOOL_ID_CALC_SYMMETRY, wxT("Symm"), bmp, wxT("Symmetry Calculation"));
    m_pToolbars[3]->Realize();
*/
    m_pToolbars[1] = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tview.png"), wxBITMAP_TYPE_PNG);
    

//  bmp = wxBitmap(CheckImageFile(wxT("tview.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_VIEW, wxT("View"), bmp, wxT("View Molecule"), wxITEM_CHECK);
        m_pToolbars[1]->AddTool(TOOL_ID_BUILD_VIEW, wxEmptyString, bmp, wxT("View Molecule"), wxITEM_CHECK);
    m_pToolbars[1]->AddSeparator();
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tadd.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("tadd.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_ADD_FRAGMENT, wxT("Add"), bmp,wxT("Add Blocks"), wxITEM_CHECK);
        m_pToolbars[1]->AddTool(TOOL_ID_BUILD_ADD_FRAGMENT, wxEmptyString, bmp,wxT("Add Blocks"), wxITEM_CHECK);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tinsert.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("tinsert.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_INSERT_FRAGMENT, wxT("Insert"), bmp,wxT("Insert Blocks"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_INSERT_FRAGMENT, wxEmptyString, bmp,wxT("Insert Blocks"), wxITEM_CHECK);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tdelete.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("tdelete.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_DELETE_FRAGMENT, wxT("Delete"), bmp,wxT("Delete Blocks"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_DELETE_FRAGMENT, wxEmptyString, bmp,wxT("Delete Blocks"), wxITEM_CHECK);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tmakebond.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("tmakebond.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_MAKE_BOND, wxT("Make"), bmp,wxT("Make Bond"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_MAKE_BOND, wxEmptyString, bmp,wxT("Make Bond"), wxITEM_CHECK);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tbreakbond.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("tbreakbond.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_BREAK_BOND, wxT("Break"), bmp,wxT("Break Bond"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_BREAK_BOND, wxEmptyString, bmp,wxT("Break Bond"), wxITEM_CHECK);


    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tdesignbond.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("tbreakbond.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_BREAK_BOND, wxT("Break"), bmp,wxT("Break Bond"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_BUILD_DESIGN_BOND, wxEmptyString, bmp,wxT("Design Bond"), wxITEM_CHECK);



    
//    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("tlayer.png"), wxBITMAP_TYPE_PNG);
    


//    bmp = wxBitmap(CheckImageFile(wxT("tlayer.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddSeparator();
    //----------XIA--------
//        m_pToolbars[1]->AddTool(TOOL_ID_BUILD_SELECT_LAYER, wxT("Layer"), bmp,wxT("Select Layer"), wxITEM_CHECK);
//        EnableTool(TOOL_ID_BUILD_SELECT_LAYER, false);
//    m_pToolbars[1]->Realize();

//    m_pToolbars[1] = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("measure.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("measure.png")), wxBITMAP_TYPE_PNG);
  //  m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_MEASURE, wxT("Measure"), bmp, wxT("Measure position, distance, angle, and dihedral"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_MEASURE, wxEmptyString, bmp, wxT("Measure position, distance, angle, and dihedral"), wxITEM_CHECK);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("constrain.png"), wxBITMAP_TYPE_PNG);
    
    m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_CONSTRAIN, wxEmptyString, bmp, wxT("Constrain position, distance, angle, and dihedral"), wxITEM_CHECK);

    
    // bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("symmetrize.png"), wxBITMAP_TYPE_PNG);
    
    // m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_SYMMETRIZE, wxEmptyString, bmp, wxT("Symmetrize"), wxITEM_CHECK);

//    bmp = wxBitmap(CheckImageFile(wxT("constrain.png")), wxBITMAP_TYPE_PNG);
 //   m_pToolbars[2]->AddTool(TOOL_ID_GEOMETRY_CONSTRAIN, wxT("Constrain"), bmp, wxT("Constrain distance, angle, and dihedral"), wxITEM_CHECK);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("definedummy.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("definedummy.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_DEFINE_DUMMY, wxT("Dummy"), bmp, wxT("Define Dummy"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_DEFINE_DUMMY, wxEmptyString, bmp, wxT("Define Dummy"), wxITEM_CHECK);
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("mirror.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("mirror.png")), wxBITMAP_TYPE_PNG);
//   m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_MIRROR, wxT("Mirror"), bmp, wxT("Mirror Molecule"), wxITEM_CHECK);
    m_pToolbars[1]->AddTool(TOOL_ID_GEOMETRY_MIRROR, wxEmptyString, bmp, wxT("Mirror Molecule"), wxITEM_CHECK);
    m_pToolbars[1]->Realize();

    m_pToolbars[2] = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
    
    // bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("energy.png"), wxBITMAP_TYPE_PNG);    
    // m_pToolbars[2]->AddTool(TOOL_ID_CALC_MM_OPTIMIZATION, wxEmptyString, bmp, wxT("Minimize Energy"));

        //begin : hurukun 06/11/2009
    
    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("mopac.png"), wxBITMAP_TYPE_PNG);
    
//        m_pToolbars[2]->AddTool(TOOL_ID_CALC_MOPAC_OPTIMIZATION, wxT("MOPAC"), bmp, wxT("Optimize Geometry"));
        m_pToolbars[2]->AddTool(TOOL_ID_CALC_MOPAC_OPTIMIZATION, wxEmptyString, bmp, wxT("Optimize Geometry By Mopac"));
        //end   : hurukun 06/11/2009


    
//    bmp = wxBitmap(EnvUtil::GetPrgResDir()+ wxTRUNK_MOL_IMG_PATH + wxT("symmetry.png"), wxBITMAP_TYPE_PNG);
    

//    bmp = wxBitmap(CheckImageFile(wxT("symmetry.png")), wxBITMAP_TYPE_PNG);
//    m_pToolbars[2]->AddTool(TOOL_ID_CALC_SYMMETRY, wxT("Symm"), bmp, wxT("Symmetry Calculation"));
    // m_pToolbars[2]->AddTool(TOOL_ID_CALC_SYMMETRY, wxEmptyString, bmp, wxT("Symmetry Calculation"));
    m_pToolbars[2]->Realize();

        m_pAuiMgr->AddPane(m_pToolbars[0], wxAuiPaneInfo().Name(STR_TB_FILE).Caption(wxT("File Tools")).
                        ToolbarPane().Top().Row(0).LeftDockable(false).Position(0).RightDockable(false));
    m_pAuiMgr->AddPane(m_pToolbars[1], wxAuiPaneInfo().Name(STR_TB_BUILD).Caption(wxT("Build Tools")).
                        ToolbarPane().Top().Row(0).LeftDockable(false).Position(2).RightDockable(false));
 //   m_pAuiMgr->AddPane(m_pToolbars[2], wxAuiPaneInfo().Name(STR_TB_GEOMETRY).Caption(wxT("Geometry Tools")).
//                        ToolbarPane().Top().Row(0).LeftDockable(false).Position(3).RightDockable(false));
    m_pAuiMgr->AddPane(m_pToolbars[2], wxAuiPaneInfo().Name(STR_TB_CALCULATE).Caption(wxT("Calculate Tools")).
                        ToolbarPane().Top().Row(0).LeftDockable(false).Position(4).RightDockable(false));

        m_pAuiMgr->GetPane(STR_TB_FILE).Show(isStandAlone);
        m_pAuiMgr->GetPane(STR_TB_BUILD).Show(true);
//        m_pAuiMgr->GetPane(STR_TB_GEOMETRY).Show(true);
        m_pAuiMgr->GetPane(STR_TB_CALCULATE).Show(true);
}

ElemPnl* AuiSharedWidgets::GetElemPnl(void) const {
        return m_pPnlElem;
}

MirrorPnl* AuiSharedWidgets::GetMirrorPnl(void) const {
        return m_pPnlMirror;
}

DeletePnl* AuiSharedWidgets::GetDeletePnl(void) const {
        return m_pPnlDelete;
}

DesignBondPnl* AuiSharedWidgets::GetDesignBondPnl(void) const {
    return m_pPnlDesignBond;
}

MeasurePnl* AuiSharedWidgets::GetMeasurePnl(void) const {
    return m_pPnlMeasure;
}
// SymmetrizePnl* AuiSharedWidgets::GetSymmetrizePnl(void) const {
//         return m_pPnlSymmetrize;
// }
ProgressBarPnl* AuiSharedWidgets::GetProgressBarPnl(void) const {
    return m_pPnlProgressBar;
}

wxAuiManager* AuiSharedWidgets::GetAuiManager(void) const {
        return m_pAuiMgr;
}

void AuiSharedWidgets::EnableMenuTool(bool isEnable) {
        int i;
        for(i = MENU_ID_MOL_START + 1; i < MENU_ID_MOL_END; i++) {
                Tools::EnableMenu(m_pMenuBar, i, isEnable);
        }
        for(i = TOOL_ID_MOL_START + 1; i < TOOL_ID_MOL_END; i++) {
                EnableTool(i, isEnable);
        }
//        Manager::Get()->GetAppFrame()->GetMenuBar()->Enable(MENU_ID_VIEW_CAPTURE_IMAGE, true);
}

void AuiSharedWidgets::EnableTool(int toolId, bool enabled) {
    int i = 0;
    for(i = 0; i < TOOLBARS_COUNT; i++) {
        if(m_pToolbars[i]->FindById(toolId)) {
            if (m_pToolbars[i]->GetToolEnabled(toolId) != enabled) {
                m_pToolbars[i]->EnableTool(toolId, enabled);
            }
            break;
        }
    }

}

void AuiSharedWidgets::ToggleTool(int toolId, bool toggle) {
    int i = 0;
    for(i = 0; i < TOOLBARS_COUNT; i++) {
        if(m_pToolbars[i]->FindById(toolId)) {
            //if (m_pToolbars[i]->GetToolState(toolId) != toggle) {
                m_pToolbars[i]->ToggleTool(toolId, toggle);
            //}
            break;
        }
    }
}

bool AuiSharedWidgets::GetToolState(int toolId) {
    int i;
    for(i = 0; i < TOOLBARS_COUNT; i++) {
        if(m_pToolbars[i]->FindById(toolId)) {
            return m_pToolbars[i]->GetToolState(toolId);
        }
    }
    return false;
}
wxToolBar* AuiSharedWidgets::GetToolBar(const unsigned int id)
{
        if(id>=TOOLBARS_COUNT)
                return NULL;

        return m_pToolbars[id];
}
void AuiSharedWidgets::ShowAuiWidgets(const wxString& name, bool isShown) {
        bool needUpdate = false;
        if(isShown) {
                if(!m_pAuiMgr->GetPane(name).IsShown()) {
                        needUpdate = true;
                }
        }else if(!isShown && m_pAuiMgr->GetPane(name).IsShown()) {
                needUpdate = true;
        }
        if(needUpdate) {
                m_pAuiMgr->GetPane(name).Show(isShown);
                m_pAuiMgr->Update();
        }
}

void AuiSharedWidgets::ShowExclusiveAuiWidget(const wxString& name) {
        WindowPtrHash::const_iterator  cit;
        bool isShown = false;
        wxAuiPaneInfo paneInfo;
        for(cit = m_winptrHash.begin(); cit != m_winptrHash.end(); cit++){
                isShown = false;
                if(cit->first.Cmp(name) == 0) {
                        isShown = true;
                }
                paneInfo = m_pAuiMgr->GetPane(cit->first);
        if(paneInfo.IsOk() && paneInfo.IsShown() != isShown) {
            m_pAuiMgr->GetPane(cit->first).Show(isShown);
        }
        }
        m_pAuiMgr->Update();
}

void AuiSharedWidgets::ShowExclusivePanel(const wxString& pnlName) {
    int i;
    bool isShown;
    wxAuiPaneInfo paneInfo;
        // const wxString EXCLUSIVE_PNL_NAMES[] = {STR_PNL_FRAGMENTS, STR_PNL_DELETE, STR_PNL_MEASURE, STR_PNL_SYMMETRIZE, STR_PNL_MIRROR,STR_PNL_PROGRESSBAR,STR_PNL_DESIGNBOND};
    const wxString EXCLUSIVE_PNL_NAMES[] = {STR_PNL_FRAGMENTS, STR_PNL_DELETE, STR_PNL_MEASURE, STR_PNL_MIRROR,STR_PNL_PROGRESSBAR,STR_PNL_DESIGNBOND};
    const int EXCLUSIVE_PNL_COUNT = sizeof(EXCLUSIVE_PNL_NAMES)/sizeof(wxString);

    for (i = 0; i < EXCLUSIVE_PNL_COUNT; i++) {
        isShown = false;
        if (EXCLUSIVE_PNL_NAMES[i].Cmp(pnlName) == 0) {
            isShown = true;
        }
        paneInfo = m_pAuiMgr->GetPane(EXCLUSIVE_PNL_NAMES[i]);
        if(paneInfo.IsOk() && paneInfo.IsShown() != isShown) {
            m_pAuiMgr->GetPane(EXCLUSIVE_PNL_NAMES[i]).Show(isShown);
        }
    }
    m_pAuiMgr->Update();
        //process event
        /*wxPaneChangeEvent event(wxEVT_COMMAND_PANE_CHANGED, wxID_ANY);
        event.SetText(pnlName);
        event.SetEventObject(this);
        GetEventHandler()->ProcessEvent(event);*/
}

bool AuiSharedWidgets::IsShown(wxString pnlName) const {
        bool isShown = false;
        if(m_pAuiMgr->GetPane(pnlName).IsOk() && m_pAuiMgr->GetPane(pnlName).IsShown()) {
                isShown = true;
        }
        return isShown;
}
