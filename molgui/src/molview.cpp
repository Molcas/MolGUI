/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "molview.h"

#include <wx/filename.h>

#include "stringutil.h"
#include "tools.h"
#include "appglobals.h"
#include "auisharedwidgets.h"
#include "ctrlids.h"
#include "molvieweventhandler.h"
#include "simuprjdef.h"
#include "fileutil.h"
#include "manager.h"
#include "surfacepnl.h"
#include "vibrationpnl.h"

int CTRL_ID_GLCANVAS = wxNewId();

BEGIN_EVENT_TABLE(MolView, AbstractView)
        //EVT_UPDATE_UI(CTRL_ID_GLCANVAS, MolView::OnMolViewUpdateUI)
END_EVENT_TABLE()

MolView::MolView(wxWindow* parent, wxEvtHandler* evtHandler, ViewType viewType, long style) : AbstractView(parent, evtHandler, viewType, style) {
        m_pPopupMenu = NULL;
        m_pMolViewData = new MolViewData();
        m_pGLCanvas = new MolGLCanvas(this, viewType, wxID_ANY, wxDefaultPosition, wxSize(600,600));
        m_pGLCanvas->SetMolData(m_pMolViewData);
        wxBoxSizer* bs = new wxBoxSizer(wxVERTICAL);
        bs->Add(m_pGLCanvas, 1, wxEXPAND);
        SetSizer(bs);
        if (viewType==VIEW_GL_BUILD)m_pMolViewData->SetActionType(ACTION_ADD_FRAGMENT);
        m_curmo=-1;

}

MolView::~MolView() {
        delete m_pMolViewData;
        m_pMolViewData=NULL;
        delete m_pGLCanvas;
        m_pGLCanvas=NULL;
        MolViewEventHandler::Get()->Free();
        //Tools::ShowError(wxT("MolView Deconstructor"), true);
}


int MolView::GetCurrentMO(void){
    return m_curmo;
}

void MolView::SetCurrentMO(int curmo){
    m_curmo=curmo;
}


MolViewData* MolView::GetData(void) const {
        return m_pMolViewData;
}

MolGLCanvas* MolView::GetGLCanvas(void) const {
        return m_pGLCanvas;
}

void MolView::RefreshGraphics(void) {
//        m_pGLCanvas->InitFont();
    m_pGLCanvas->Draw();
}

void MolView::InitSurfaceOrVib(void){
//        wxMessageBox(wxT("before newView0"));
        SimuProject* pProject = Manager::Get()->GetProjectManager()->GetProject(GetProjectId());
        if(pProject!=NULL)
         Manager::Get()->GetProjectManager()->SetActiveProject(pProject);
        if(GetType()== VIEW_GL_DISPLAY_SURFACE && pProject) {
//        wxMessageBox(wxT("before newView1"));
                SurfacePnl* pSurfacePnl = static_cast<SurfacePnl*>(AuiSharedWidgets::Get()->GetWindow(STR_PNL_SURFACE));
                if(pSurfacePnl) {
//        wxMessageBox(wxT("before newView2"));
                        wxArrayString desFiles = pProject->GetFiles(GetTitle())->GetSurfaceDataFiles();
//        wxMessageBox(wxT("before newView3"));
//        if(desFiles.GetCount()>0)wxMessageBox(desFiles[0]);
//        else
//        wxMessageBox(GetTitle());
                        pSurfacePnl->AppendDenFile(desFiles);
                        if(m_curmo>=0)pSurfacePnl->SetSelectRow(m_curmo);
//        wxMessageBox(wxT("before newView4"));
                }
//                                AuiSharedWidgets::Get()->ShowExclusiveAuiWidget(STR_PNL_SURFACE);
               Manager::Get()->GetAppFrame()->GetMenuBar()->Enable(MENU_ID_VIEW_CAPTURE_IMAGE, true);


        }else if(GetType() == VIEW_GL_DISPLAY_VIBRATION && pProject) {
                VibrationPnl* pVibPnl = static_cast<VibrationPnl*>(AuiSharedWidgets::Get()->GetWindow(STR_PNL_VIBRATION));
                if(pVibPnl) {
                        AbstractProjectFiles* pFiles = pProject->GetFiles(GetTitle());
        //                                wxMessageBox(pFiles->GetVibOutputFileName());
                        pFiles->GetIRDatas()->LoadFile(pFiles->GetVibOutputFileName());
                        pVibPnl->AddVibDisplayData(pFiles->GetIRDatas()->GetIRDataArray());//pProject->GetFiles(newView->GetTitle())->GetVibrationData());
                }
//                                AuiSharedWidgets::Get()->ShowExclusiveAuiWidget(STR_PNL_VIBRATION);
//                                timer = true;
        }
        RefreshGraphics();
}
void MolView::InitView(void) {
        CtrlInst* pCtrlInst = new CtrlInst(GetData(),m_viewId);
        wxString fileName = GetFileName();
        if(GetData() != NULL) {
                GetData()->AddCtrlInst(pCtrlInst);
                GetData()->SetActiveCtrlInstIndex(m_pMolViewData->GetCtrlInstCount() - 1);
                if (!StringUtil::IsEmptyString(fileName) && wxFileExists(fileName)) {
                    if(!GetData()->GetActiveCtrlInst()->LoadFile(fileName)) {
                        Tools::ShowError(wxT("Error when loading ") + fileName);
                    }
                }
        }
        SetMenuToolCtrlInst();
//wxString    msg=wxString::Format(wxT("%d"),GetProjectId());
//wxMessageBox(msg);
        RefreshGraphics();
}

void MolView::OnMolViewUpdateUI(wxUpdateUIEvent& event) {
        RefreshGraphics();
}

wxMenu* MolView::GetPopupMenu(void) {
        if(m_pPopupMenu == NULL) {
                m_pPopupMenu = new wxMenu;
            m_pPopupMenu->Append(MENU_ID_BUILD_RESET, wxT("&Center Molecule"));
            m_pPopupMenu->AppendSeparator();
            m_pPopupMenu->AppendCheckItem(POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS, wxT("Global &Labels"));
            m_pPopupMenu->AppendCheckItem(POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS, wxT("Atom L&abels"));
            m_pPopupMenu->AppendSeparator();
            m_pPopupMenu->Append(MENU_ID_EDIT_ATOM_LIST, wxT("Edit Coordinate..."));
            m_pPopupMenu->Append(MENU_ID_EDIT_BACKGROUND_COLOR, wxT("Background Color"));
    //        m_pPopupMenu->Append(MENU_ID_EDIT_CONSTRAIN_LIST, wxT("Constraint List..."));
        }
    return m_pPopupMenu;
}

void MolView::EnableMenuTool(bool isEnable) {
        if(GetType() != VIEW_GL_BUILD) {
                //always disable menus and tools
                isEnable = false;
        }
        AuiSharedWidgets::Get()->EnableMenuTool(isEnable);
        if(!isEnable) AuiSharedWidgets::Get()->ShowExclusivePanel(wxEmptyString);
}

void MolView::UpdateMenuTool(void) {
        MolViewEventHandler::Get()->SetMolView(this);
        if(GetType() == VIEW_GL_BUILD) {
                EnableMenuToolCtrlInst();
                SetMenuToolCtrlInst();
                MolViewEventHandler::Get()->UpdateMenuToolExclusive(false, GetBuildGeometryMenuId(GetData()->GetActionType()), true);
                MolViewEventHandler::Get()->SetActionType(GetData()->GetActionType(), BP_NULL_VALUE);
        }
}

int MolView::GetBuildGeometryMenuId(ActionType actionType) {
    int menuId = MENU_ID_BUILD_VIEW;
    switch (actionType) {
    case ACTION_VIEW:
        menuId = MENU_ID_BUILD_VIEW;
        break;
    case ACTION_ADD_FRAGMENT:
        menuId = MENU_ID_BUILD_ADD_FRAGMENT;
        break;
    case ACTION_INSERT_FRAGMENT:
        menuId = MENU_ID_BUILD_INSERT_FRAGMENT;
        break;
    case ACTION_DELETE_FRAGMENT:
        menuId = MENU_ID_BUILD_DELETE_FRAGMENT;
        break;
    case ACTION_MAKE_BOND:
        menuId = MENU_ID_BUILD_MAKE_BOND;
        break;
    case ACTION_BREAK_BOND:
        menuId = MENU_ID_BUILD_BREAK_BOND;
        break;
        case ACTION_LAYER:
        menuId = MENU_ID_BUILD_SELECT_LAYER;
        break;
    case ACTION_MEASURE:
        menuId = MENU_ID_GEOMETRY_MEASURE;
        break;
    case ACTION_CONSTRAIN:
        menuId = MENU_ID_GEOMETRY_CONSTRAIN;
        break;
    case ACTION_SYMMETRIZE:
        menuId = MENU_ID_GEOMETRY_SYMMETRIZE;
        break;
    case ACTION_MIRROR:
        menuId = MENU_ID_GEOMETRY_MIRROR;
        break;
    case ACTION_DEFINE_DUMMY:
        menuId = MENU_ID_GEOMETRY_DEFINE_DUMMY;
        break;
    default:
        break;
    }
    return menuId;
}

int MolView::GetModeTypeMenuId(ModelType modelType) {
    int menuId = MENU_ID_MODEL_WIRE;
    switch (modelType) {
    case MODEL_WIRE:
        menuId = MENU_ID_MODEL_WIRE;
        break;
    case MODEL_TUBE:
        menuId = MENU_ID_MODEL_TUBE;
        break;
    case MODEL_BALLSPOKE:
        menuId = MENU_ID_MODEL_BALLSPOKE;
        break;
    case MODEL_BALLWIRE:
        menuId = MENU_ID_MODEL_BALLWIRE;
        break;
    case MODEL_SPACEFILLING:
        menuId = MENU_ID_MODEL_SPACEFILLING;
        break;
    case MODEL_RIBBON:
        menuId = MENU_ID_MODEL_RIBBON;
        break;
    default:
        break;
    }
    return menuId;
}

void MolView::SetMenuToolCtrlInst(void) {
        int i = 0;
        CtrlInst* pCtrlInst = GetData()->GetActiveCtrlInst();
    if (pCtrlInst) {
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_HYDROGENS, pCtrlInst->isShowHydrogens);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_GLOBAL_LABELS, pCtrlInst->isShowGlobalLabels);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_ATOM_LABELS, pCtrlInst->isShowAtomLabels);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_LAYERS_SHOW, pCtrlInst->isShowLayers);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_LAYERS_HIGH, pCtrlInst->isShowLayerHigh);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_LAYERS_MEDIUM, pCtrlInst->isShowLayerMedium);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_LAYERS_LOW, pCtrlInst->isShowLayerLow);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_HYDROGEN_BONDS, pCtrlInst->isShowHydrogenBonds);
        Tools::CheckMenu(GetMenuBar(), MENU_ID_MODEL_SHOW_AXES, pCtrlInst->isShowAxes);
        Tools::CheckMenu(GetMenuBar(), GetModeTypeMenuId(pCtrlInst->modelType), true);
        //
        GetPopupMenu()->Check(POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS, pCtrlInst->isShowGlobalLabels);
        GetPopupMenu()->Check(POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS, pCtrlInst->isShowAtomLabels);
        //
        for(i = MENU_ID_MODEL_SHOW_LAYERS_HIGH; i <= MENU_ID_MODEL_SHOW_LAYERS_LOW; i++) {
                        EnableMenu(i, pCtrlInst->isShowLayers);
                }
                for(i = MENU_ID_MODEL_WIRE; i <= MENU_ID_MODEL_RIBBON; i++) {
                        EnableMenu(i, !pCtrlInst->isShowLayers);
                }
    }
}


void MolView::EnableMenuToolCtrlInst(void) {
    bool enabled = false;
    CtrlInst* pCtrlInst = GetData()->GetActiveCtrlInst();

    enabled = false;
    if (pCtrlInst != NULL && pCtrlInst->GetRenderingData()->GetAtomCount() > 0) {
        enabled = true;
    }
    // EnableMenu(MENU_ID_CALC_MM_OPTIMIZATION, enabled);
    // AuiSharedWidgets::Get()->EnableTool(TOOL_ID_CALC_MM_OPTIMIZATION, enabled);
    EnableMenu(MENU_ID_CALC_MOPAC_OPTIMIZATION, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_CALC_MOPAC_OPTIMIZATION, enabled);
    EnableMenu(MENU_ID_CALC_SYMMETRY, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_CALC_SYMMETRY, enabled);

    enabled = false;
    if (pCtrlInst != NULL && pCtrlInst->GetRenderingData()->GetAtomCount() > 0) {
        enabled = true;
    }
    //-----------xia-------------------------
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_FILE_SAVE, enabled);
    EnableMenu(wxID_SAVE, enabled);
     EnableMenu(wxID_SAVEAS, enabled);
    //-----------------------------------------
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_DELETE_FRAGMENT, enabled);
    EnableMenu(MENU_ID_BUILD_DELETE_FRAGMENT, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_BREAK_BOND, enabled);
    EnableMenu(MENU_ID_BUILD_BREAK_BOND, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_SELECT_LAYER, false);
    EnableMenu(MENU_ID_BUILD_SELECT_LAYER, enabled);
    EnableMenu(MENU_ID_BUILD_CREATE_TEMPLATE, enabled);
    EnableMenu(MENU_ID_BUILD_DELETE_TEMPLATE, true);


    EnableMenu(MENU_ID_EDIT_CLEAR, enabled);
    EnableMenu(MENU_ID_EDIT_CONSTRAIN_LIST, enabled);
    EnableMenu(MENU_ID_EDIT_ATOM_LIST, pCtrlInst != NULL);
    EnableMenu(MENU_ID_EDIT_BACKGROUND_COLOR, enabled);

    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_GEOMETRY_MEASURE, enabled);
    EnableMenu(MENU_ID_GEOMETRY_MEASURE, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_GEOMETRY_CONSTRAIN, enabled);
    EnableMenu(MENU_ID_GEOMETRY_CONSTRAIN, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_GEOMETRY_SYMMETRIZE, enabled);
    EnableMenu(MENU_ID_GEOMETRY_SYMMETRIZE, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_GEOMETRY_MIRROR, enabled);
    EnableMenu(MENU_ID_GEOMETRY_MIRROR, enabled);

    enabled = false;
    EnableMenu(wxID_FIND, enabled);
    EnableMenu(MENU_ID_MODEL_RIBBON, enabled);
    EnableMenu(MENU_ID_MODEL_SHOW_HYDROGEN_BONDS, enabled);

    enabled = false;
    if (pCtrlInst != NULL && pCtrlInst->GetRenderingData()->GetAtomCount() >= 2){
        enabled = true;
        }
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_MAKE_BOND, enabled);
    EnableMenu(MENU_ID_BUILD_MAKE_BOND, enabled);

    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_BUILD_DESIGN_BOND, enabled);
    EnableMenu(MENU_ID_BUILD_DESIGN_BOND, enabled);

    EnableMenu(MENU_ID_GEOMETRY_DEFINE_DUMMY, enabled);
    AuiSharedWidgets::Get()->EnableTool(TOOL_ID_GEOMETRY_DEFINE_DUMMY, enabled);
//------------------xia---------------
    enabled = false;
    if (pCtrlInst !=NULL && pCtrlInst->CanUndo()) {
        /*switch (GetGLCanvas()->GetMolData()->GetActionType()) {
        case ACTION_VIEW:
        case ACTION_ADD_FRAGMENT:
        case ACTION_INSERT_FRAGMENT:
        case ACTION_DELETE_FRAGMENT:
        case ACTION_MAKE_BOND:
        case ACTION_BREAK_BOND:
        case ACTION_MIRROR:
                case ACTION_DEFINE_DUMMY:
                case ACTION_CHANGE_BOND:
                case ACTION_LAYER:
            enabled = true;
            break;
        case ACTION_CONSTRAIN:
        case ACTION_MEASURE:
                //case ACTION_CALCULATION:
       // case ACTION_VIBRATION:
        //case ACTION_SURFACE:
            break;
        }*/
        enabled = true;
    }
    EnableMenu(MENU_ID_EDIT_UNDO, enabled);

    enabled = false;
    if (pCtrlInst !=NULL && pCtrlInst->CanRedo()) {
        /*switch (GetGLCanvas()->GetMolData()->GetActionType()) {
        case ACTION_VIEW:
        case ACTION_ADD_FRAGMENT:
        case ACTION_INSERT_FRAGMENT:
        case ACTION_DELETE_FRAGMENT:
        case ACTION_MAKE_BOND:
        case ACTION_BREAK_BOND:
        case ACTION_MIRROR:
                case ACTION_DEFINE_DUMMY:
                case ACTION_CHANGE_BOND:
                case ACTION_LAYER:
            enabled = true;
            break;
        case ACTION_CONSTRAIN:
        case ACTION_MEASURE:
                //case ACTION_CALCULATION:
        //case ACTION_VIBRATION:
        //case ACTION_SURFACE:
            break;
        }*/
        enabled = true;
    }
    EnableMenu(MENU_ID_EDIT_REDO, enabled);
/*
    enabled = false;
    if (pCtrlInst !=NULL && pCtrlInst->CanUndo()) {
        enabled=true;
    }
    EnableMenu(wxID_UNDO, enabled);
    EnableMenu(wxID_REDO, enabled);*/
    //-------------------
    //enabled = true;
    /*
    if (pCtrlInst !=NULL) {
        switch (GetUIData()->GetActionType()) {
        case ACTION_VIEW:
        case ACTION_ADD_FRAGMENT:
        case ACTION_INSERT_FRAGMENT:
        case ACTION_DELETE_FRAGMENT:
        case ACTION_MAKE_BOND:
        case ACTION_BREAK_BOND:
        case ACTION_MIRROR:
                case ACTION_DEFINE_DUMMY:
                case ACTION_CHANGE_BOND:
                case ACTION_LAYER:
            enabled = true;
            break;
        case ACTION_CONSTRAIN:
        case ACTION_MEASURE:
                case ACTION_CALCULATION:
        case ACTION_VIBRATION:
        case ACTION_SURFACE:
            break;
        }
    }*/
    //EnableMenu(wxID_UNDO, enabled && pCtrlInst->CanUndo());
    //EnableMenu(wxID_REDO, enabled && pCtrlInst->CanRedo());
}

int MolView::NeedSaveRenderingData(CtrlInst* pCtrlInst) {
    if (pCtrlInst && !pCtrlInst->GetRenderingData()->IsEmpty()) {
        if (pCtrlInst->IsDirty()) {
            wxMessageDialog dialog(this, wxT("Would you like to save changes in view: ")+GetTitle()+wxT(" ?"), appglobals::appName,
                                   wxYES_DEFAULT | wxYES_NO | wxICON_INFORMATION);
            return dialog.ShowModal();
        } // hurukun 0426
    }
    return wxID_NO;
}

bool MolView::SaveRenderingData(wxString fileName, CtrlInst* pCtrlInst) {
    if (!pCtrlInst || pCtrlInst->GetRenderingData()->IsEmpty()) {
                if(::wxFileExists(fileName)){
                        wxRemoveFile(fileName);
                }
                fileName=FileUtil::ChangeFileExt(fileName,wxMCD_FILE_SUFFIX);
                if(::wxFileExists(fileName)){
                        wxRemoveFile(fileName);
                }
                pCtrlInst->SetDirty(false);
        return true;
    }
        wxString newFileName = fileName;
        if (StringUtil::IsEmptyString(newFileName)) {
                newFileName = Tools::GetFilePathFromUser(this, wxT("Save File"), wxT("SimuGui Molecule Files (*.sim)|*.sim|XYZ Files (*.xyz)|*.xyz"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    }

    if (!StringUtil::IsEmptyString(newFileName)) {
        pCtrlInst->SaveFile(newFileName, false);
        pCtrlInst->SetDirty(false);
        return true;
    }else {
                Tools::ShowError(wxT("Empty File Name"));
        }
    return false;
}
/*
bool MolView::SaveRenderingData(wxString fileName, CtrlInst* pCtrlInst, bool isSaveAs) {
    if (!pCtrlInst || pCtrlInst->GetRenderingData()->IsEmpty()) {
        return true;
    }
        wxString newFileName = fileName;
        if (isSaveAs || StringUtil::IsEmptyString(newFileName)) {
                newFileName = Tools::GetFilePathFromUser(this, wxT("Save File"), wxT("SimuGui Molecule Files (*.sim)|*.sim|XYZ Files (*.xyz)|*.xyz"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    }

    if (!StringUtil::IsEmptyString(newFileName)) {
        pCtrlInst->SaveFile(newFileName, false);
        pCtrlInst->SetDirty(false);
        return true;
    }else {
                Tools::ShowError(wxT("Empty File Name"));
        }
    return false;
}
*/

bool MolView::Close(bool isAskForSave,bool default_save) {
        bool isClose = DoCloseCtrlInst(GetFileName(), GetData()->GetActiveCtrlInst(),isAskForSave,default_save);
    if (!isClose) return false;
        //Tools::ShowError(wxT("MolView Close"), true);
    return true;
}

bool MolView::DoCloseCtrlInst(wxString fileName, CtrlInst* pCtrlInst,bool isAskForSave,bool default_save) {
    bool isClose = true;
    if(isAskForSave==false&&default_save==true){
        isClose = SaveRenderingData(fileName, pCtrlInst);
    }else if(isAskForSave==true){
            switch (NeedSaveRenderingData(pCtrlInst)) {
                case wxID_YES:
                isClose = SaveRenderingData(fileName, pCtrlInst);
                break;
                case wxID_NO:
                break;
                case wxID_CANCEL:
                isClose = false;
                   break;
           default:
                break;
            }
    }
    return isClose;
}

bool MolView::Save() {
        return SaveRenderingData(GetFileName(), GetData()->GetActiveCtrlInst());
}

bool MolView::SaveAs(wxString nFileName) {
        CtrlInst* pCtrlInst = GetData()->GetActiveCtrlInst();
        if(!pCtrlInst || pCtrlInst->GetRenderingData()->IsEmpty())
        return false;
    wxString tmp=FileUtil::ChangeFileExt(nFileName,wxMSD_FILE_SUFFIX);

        return SaveRenderingData(tmp, pCtrlInst);
}

void MolView::ImportFile(wxString fileName) {
        if(!StringUtil::IsEmptyString(fileName)) {
                GetData()->GetActiveCtrlInst()->LoadFile(fileName);
                GetData()->GetActiveCtrlInst()->SetDirty(true);
        }
}
