/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "navindicator.h"
#include "matrix4x4.h"

#define RIBBON_WIDTH 0.05f
#define TESS_LEVEL 30.0f
#define CENTROID_RADIUS 1.0f
#define MINIMUN_RADIUS 0.2f
#define RADIUS_DISP 0.2f

NavIndicator::NavIndicator(CtrlInst* pCtrlInst) {
        m_pCtrlInst = pCtrlInst;

        m_startAngle = (float)(PI / 8);
        m_endAngle = 2 * PI - m_startAngle;
        m_angleStep = (m_endAngle - m_startAngle) / TESS_LEVEL;

}

void NavIndicator::Start(void) {
        glPushMatrix();
        glColor3f(1.0f, 0.5f, 0.25f);
        AtomPosition3F center = m_pCtrlInst->GetRotationCenter(m_ribbonRadius);
        m_ribbonWidth = RIBBON_WIDTH;
        if(m_ribbonRadius == 0.0f) {
                m_ribbonRadius = CENTROID_RADIUS;
                m_ribbonWidth *= 1.5;
        }else if(m_ribbonRadius < MINIMUN_RADIUS) {
                m_ribbonRadius = MINIMUN_RADIUS;
        }
        m_ribbonRadius += RADIUS_DISP;
        glTranslated((GLfloat)center.x, (GLfloat)center.y, (GLfloat)center.z);
        // std::cout << "NavIndicator::Start: m_ribbonWidth: " << m_ribbonWidth << "\n m_ribbonRadius" << m_ribbonRadius << std::endl;
}

void NavIndicator::End(void) {
        glPopMatrix();
}

void NavIndicator::Painting(void) {
        Start();
        switch(m_pCtrlInst->moveType) {
        case MOVE_ROTATE_XY:
                //Draw Ribbon rotated with X and Y axes
                DrawRotationRibbonX();
                DrawRotationRibbonY();
                break;
        case MOVE_ROTATE_XZ:
                //Draw Ribbon rotated with X and Z axes
                DrawRotationRibbonX();
                DrawRotationRibbonZ();
                break;
        default:
                break;
        }
        End();
}

/**
 * Draw Ribbon rotated with X axis
 */
void NavIndicator::DrawRotationRibbonX(void) {
        AtomPosition3F pos;
        AtomPosition3F vec = m_pCtrlInst->camera.GetZAxis();
        vec.Normalize();
        AtomPosition3F xAxis = m_pCtrlInst->camera.GetXAxis();
        xAxis.Normalize();

        Matrix4x4 matrix;
        matrix.SetIdentity();
        matrix.RotateAxis(xAxis, m_pCtrlInst->navIndicatorAngle.x);

        float arrowWidth = m_ribbonWidth * 2;
        //draw top arrow
        glBegin(GL_TRIANGLES);
                matrix.RotateAxis(xAxis, m_angleStep);
                AtomPosition3F tempPos = matrix.Multiply(vec);
                tempPos.Normalize();
                tempPos.Scale(m_ribbonRadius);
                matrix.RotateAxis(xAxis, m_angleStep);
                pos = matrix.Multiply(vec);
                pos.Normalize();
                pos.Scale(m_ribbonRadius);

                glNormal3f((GLfloat)pos.x, (GLfloat)pos.y, (GLfloat)pos.z);
                glVertex3f((GLfloat)tempPos.x, (GLfloat)tempPos.y, (GLfloat)tempPos.z);
                glVertex3f((GLfloat)pos.x - arrowWidth*xAxis.x, (GLfloat)pos.y - arrowWidth*xAxis.y, (GLfloat)pos.z - arrowWidth*xAxis.z);
                glVertex3f((GLfloat)pos.x + arrowWidth*xAxis.x, (GLfloat)pos.y + arrowWidth*xAxis.y, (GLfloat)pos.z + arrowWidth*xAxis.z);
    glEnd();


        glBegin(GL_QUAD_STRIP);
                for(int i = 0; i <= TESS_LEVEL; i++) {
                        if(i != 0) {
                                matrix.RotateAxis(xAxis, m_angleStep);
                        }
                        pos = matrix.Multiply(vec);
                        pos.Normalize();
                        pos.Scale(m_ribbonRadius);
                        glNormal3f((GLfloat)pos.x, (GLfloat)pos.y, (GLfloat)pos.z);
                        glVertex3f((GLfloat)pos.x - m_ribbonWidth*xAxis.x, (GLfloat)pos.y - m_ribbonWidth*xAxis.y, (GLfloat)pos.z - m_ribbonWidth*xAxis.z);
                        glVertex3f((GLfloat)pos.x + m_ribbonWidth*xAxis.x, (GLfloat)pos.y + m_ribbonWidth*xAxis.y, (GLfloat)pos.z + m_ribbonWidth*xAxis.z);
                }
        glEnd();

        //draw bottom arrow
        glBegin(GL_TRIANGLES);
                glVertex3f((GLfloat)pos.x - arrowWidth*xAxis.x, (GLfloat)pos.y - arrowWidth*xAxis.y, (GLfloat)pos.z - arrowWidth*xAxis.z);
                glVertex3f((GLfloat)pos.x + arrowWidth*xAxis.x, (GLfloat)pos.y + arrowWidth*xAxis.y, (GLfloat)pos.z + arrowWidth*xAxis.z);
                matrix.RotateAxis(xAxis, m_angleStep);
                pos = matrix.Multiply(vec);
                pos.Normalize();
                pos.Scale(m_ribbonRadius);
                glVertex3f((GLfloat)pos.x, (GLfloat)pos.y, (GLfloat)pos.z);
    glEnd();
}

/**
 * Draw Ribbon rotated with Y axis
 */
void NavIndicator::DrawRotationRibbonY(void) {
        AtomPosition3F pos;
        AtomPosition3F vec = m_pCtrlInst->camera.GetZAxis();
        AtomPosition3F yAxis = m_pCtrlInst->camera.GetYAxis();
        vec.Normalize();
        yAxis.Normalize();

        Matrix4x4 matrix;
        matrix.SetIdentity();
        matrix.RotateAxis(yAxis, m_pCtrlInst->navIndicatorAngle.y);

        float arrowWidth = m_ribbonWidth * 2;

        //draw right arrow
        glBegin(GL_TRIANGLES);
                matrix.RotateAxis(yAxis, m_angleStep);
                AtomPosition3F tempPos = matrix.Multiply(vec);
                tempPos.Normalize();
                tempPos.Scale(m_ribbonRadius);
                matrix.RotateAxis(yAxis, m_angleStep);
                pos = matrix.Multiply(vec);
                pos.Normalize();
                pos.Scale(m_ribbonRadius);

                glNormal3f((GLfloat)pos.x, (GLfloat)pos.y, (GLfloat)pos.z);
                glVertex3f((GLfloat)tempPos.x, (GLfloat)tempPos.y, (GLfloat)tempPos.z);
                glVertex3f((GLfloat)pos.x - arrowWidth*yAxis.x, (GLfloat)pos.y - arrowWidth*yAxis.y, (GLfloat)pos.z - arrowWidth*yAxis.z);
                glVertex3f((GLfloat)pos.x + arrowWidth*yAxis.x, (GLfloat)pos.y + arrowWidth*yAxis.y, (GLfloat)pos.z + arrowWidth*yAxis.z);
    glEnd();


        glBegin(GL_QUAD_STRIP);
                for(int i = 0; i <= TESS_LEVEL; i++) {
                        if(i != 0) {
                                matrix.RotateAxis(yAxis, m_angleStep);
                        }
                        pos = matrix.Multiply(vec);
                        pos.Normalize();
                        pos.Scale(m_ribbonRadius);

                        glNormal3f((GLfloat)pos.x, (GLfloat)pos.y, (GLfloat)pos.z);
                        glVertex3f((GLfloat)pos.x - m_ribbonWidth*yAxis.x, (GLfloat)pos.y - m_ribbonWidth*yAxis.y, (GLfloat)pos.z - m_ribbonWidth*yAxis.z);
                        glVertex3f((GLfloat)pos.x + m_ribbonWidth*yAxis.x, (GLfloat)pos.y + m_ribbonWidth*yAxis.y, (GLfloat)pos.z + m_ribbonWidth*yAxis.z);
                }
        glEnd();

        //draw left arrow
        glBegin(GL_TRIANGLES);
                glVertex3f((GLfloat)pos.x - arrowWidth*yAxis.x, (GLfloat)pos.y - arrowWidth*yAxis.y, (GLfloat)pos.z - arrowWidth*yAxis.z);
                glVertex3f((GLfloat)pos.x + arrowWidth*yAxis.x, (GLfloat)pos.y + arrowWidth*yAxis.y, (GLfloat)pos.z + arrowWidth*yAxis.z);
                matrix.RotateAxis(yAxis, m_angleStep);
                pos = matrix.Multiply(vec);
                pos.Normalize();
                pos.Scale(m_ribbonRadius);
                glVertex3f((GLfloat)pos.x, (GLfloat)pos.y, (GLfloat)pos.z);
    glEnd();
}

/**
 * Draw Ribbon rotated with Z axis
 */
void NavIndicator::DrawRotationRibbonZ(void) {
        AtomPosition3F pos1, pos2;
        AtomPosition3F vec = m_pCtrlInst->camera.GetXAxis();
        vec.Normalize();
        AtomPosition3F zAxis = m_pCtrlInst->camera.GetZAxis();
        zAxis.Normalize();

        Matrix4x4 matrix;
        matrix.SetIdentity();
        matrix.RotateAxis(zAxis, m_pCtrlInst->navIndicatorAngle.z);

        float arrowWidth = m_ribbonWidth * 2;
        //draw top arrow
        glBegin(GL_TRIANGLES);
                matrix.RotateAxis(zAxis, m_angleStep);
                AtomPosition3F tempPos = matrix.Multiply(vec);
                tempPos.Normalize();
                tempPos.Scale(m_ribbonRadius);
                matrix.RotateAxis(zAxis, m_angleStep);
                pos1 = matrix.Multiply(vec);
                pos1.Normalize();
                pos2 = pos1;
                pos1.Scale(m_ribbonRadius - arrowWidth);
                pos2.Scale(m_ribbonRadius + arrowWidth);

                glNormal3f((GLfloat)pos1.x, (GLfloat)pos1.y, (GLfloat)pos1.z);
                glVertex3f((GLfloat)tempPos.x, (GLfloat)tempPos.y, (GLfloat)tempPos.z);
                glVertex3f((GLfloat)pos1.x, (GLfloat)pos1.y, (GLfloat)pos1.z);
                glVertex3f((GLfloat)pos2.x, (GLfloat)pos2.y, (GLfloat)pos2.z);
    glEnd();


        glBegin(GL_QUAD_STRIP);
                for(int i = 0; i <= TESS_LEVEL; i++) {
                        if(i != 0) {
                                matrix.RotateAxis(zAxis, m_angleStep);
                        }
                        pos1 = matrix.Multiply(vec);
                        pos1.Normalize();
                        pos2 = pos1;
                        pos1.Scale(m_ribbonRadius - m_ribbonWidth);
                        pos2.Scale(m_ribbonRadius + m_ribbonWidth);
                        glNormal3f((GLfloat)pos1.x, (GLfloat)pos1.y, (GLfloat)pos1.z);
                        glVertex3f((GLfloat)pos1.x, (GLfloat)pos1.y, (GLfloat)pos1.z);
                        glVertex3f((GLfloat)pos2.x, (GLfloat)pos2.y, (GLfloat)pos2.z);
                }
        glEnd();

        //draw bottom arrow
        glBegin(GL_TRIANGLES);
                pos1.Normalize();
                pos2 = pos1;
                pos1.Scale(m_ribbonRadius - arrowWidth);
                pos2.Scale(m_ribbonRadius + arrowWidth);
                glVertex3f((GLfloat)pos1.x, (GLfloat)pos1.y, (GLfloat)pos1.z);
                glVertex3f((GLfloat)pos2.x, (GLfloat)pos2.y, (GLfloat)pos2.z);
                matrix.RotateAxis(zAxis, m_angleStep);
                pos1 = matrix.Multiply(vec);
                pos1.Normalize();
                pos1.Scale(m_ribbonRadius);
                glVertex3f((GLfloat)pos1.x, (GLfloat)pos1.y, (GLfloat)pos1.z);
    glEnd();
}
