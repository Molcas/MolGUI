/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "ctrlinst.h"

#include <wx/filename.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>

#include "stringutil.h"
#include "fileutil.h"
#include "envutil.h"
#include "tools.h"

#include "uiprops.h"
#include "molfile.h"
#include "xyzfile.h"
#include "atomlistfrm.h"
#include "molviewdata.h"
#include "molvieweventhandler.h"
#include <wx/arrimpl.cpp>

#include <fstream>
#include <string>
#include <vector>
using namespace std;

AbstractSurface::AbstractSurface() {
        m_isRendering = false;
        m_renderingStyle = 1;
}

void AbstractSurface::SetRendering(bool isRendering) {
        m_isRendering = isRendering;
}

bool AbstractSurface::IsRendering(void) const {
        return m_isRendering;
}

void AbstractSurface::SetRenderingStyle(int style) {
        m_renderingStyle = style;
}

int AbstractSurface::GetRenderingStyle(void) const {
        return m_renderingStyle;
}

CtrlInst::CtrlInst(MolViewData* pViewData,int id) {
    m_id=id;//related this object to the view.
        mouseClickedAtomIndex = BP_NULL_VALUE;
        mouseButton = MOUSE_BUTTON_NONE;
        modelType = MODEL_BALLSPOKE;
        moveType = MOVE_NONE;

        isShowHydrogens = true;
        isShowGlobalLabels = false;
        isShowAtomLabels = false;
        isShowLayers = false;
         isShowLayerHigh = true;
        isShowLayerMedium = true;
        isShowLayerLow = true;
        isShowHydrogenBonds = false;
        isShowSurface = false;
        isShowAxes = false;
        isShowSymmetry=false;
        //atomList = NULL;

        m_isDirty = false;
        m_isPreview = false;
        m_strFileName = wxEmptyString;

        m_selUtil.SetCtrlInst(this);
        m_pRenderingData = new RenderingData(pViewData);
        m_currentHisFileIndex = 0;
        m_hisFileArray.Clear();


        m_surface = NULL;
        m_pAtomListFrm = NULL;
        Empty();
}

CtrlInst::~CtrlInst() {
        m_hisFileArray.Clear();
        Empty();
        /*if(m_pRenderingData) {
                delete m_pRenderingData;
                m_pRenderingData = NULL;
        }*/
        if(m_pAtomListFrm != NULL) {
        //        m_pAtomListFrm->Close(true);
                m_pAtomListFrm->Show(false);
                m_pAtomListFrm->Destroy();
                m_pAtomListFrm = NULL;
        }
//        FreeAtomListFrm();
}

RenderingData* CtrlInst::GetRenderingData(void) {
        return m_pRenderingData;
}

void CtrlInst::Empty(void) {
        m_atomAngleArray.Clear();
        ClearSelectedAtoms();
        m_pRenderingData->Empty();
        ResetGLPositions();
        if(m_surface) {
                delete m_surface;
                m_surface = NULL;
        }
}

void CtrlInst::ResetGLPositions(void) {
        camera.SetIdentity();
        if(m_isPreview) {
        camera.Scale(2.0f);
    }else {
        camera.Scale(0.8f);
    }

        posCoord.x = posCoord.y = posCoord.z = 0;
        navIndicatorAngle.x = navIndicatorAngle.y = navIndicatorAngle.z = 0;
}

SelectionUtil* CtrlInst::GetSelectionUtil(void) {
    return &m_selUtil;
}

bool CtrlInst::IsDirty(void) const {
        return m_isDirty;
}

void CtrlInst::SetDirty(bool isDirty){
        this->m_isDirty = isDirty;
        if(isDirty) {
                ////ResetOutputList(false);
        }
}

bool CtrlInst::IsPreviewCtrlInst(void) const {
        return m_isPreview;
}

void CtrlInst::SetPreviewCtrlInst(bool isPreview){
        this->m_isPreview = isPreview;
        this->isShowAxes = false;
        ResetGLPositions();
}

wxString CtrlInst::GetFileName(void) const {
    return m_strFileName;
}

bool CtrlInst::LoadFile(wxString& strFileName) {
    if (StringUtil::IsEmptyString(strFileName)) {
        return false;
    }
        if(!wxFileExists(strFileName))
                return false;
        Empty();
    wxString lowerCaseFile = strFileName.Lower();
        if(lowerCaseFile.EndsWith(wxT(".sim"))) {
        MolFile::LoadFile(strFileName, GetRenderingData());
    }else if(lowerCaseFile.EndsWith(wxT(".xyz"))) {
        XYZFile::LoadFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
        }else if(lowerCaseFile.EndsWith(wxT(".in"))) {
                if(XYZFile::IsQChemXYZFile(strFileName)==true) //if true, read the Q-Chem xyz format file
                        XYZFile::LoadQchemFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
                else if(XYZFile::IsQChemInnerFile(strFileName)==true) //if true, read the Q-chem inner format file
                        XYZFile::LoadQChemTestFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
                else {
                        Tools::ShowError(wxT("Sorry, we cannot recognize this file format now!"));
                        return false;
                }
        }else if(lowerCaseFile.EndsWith(wxT(".dens"))){
                XYZFile::LoadFromDen(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
        }else if(lowerCaseFile.EndsWith(wxT(".pdb"))) {
        wxMessageDialog dialog(NULL, wxT("Would you like to add Hydrogen or not?"),
                wxT("Hydrogen adding"), wxYES_DEFAULT | wxYES_NO | wxICON_INFORMATION);
                wxString xyzFileName,xyzHFileName;
                xyzFileName=FileUtil::ChangeFileExt(strFileName,wxT(".xyz"));
                xyzHFileName=FileUtil::ChangeFileExt(strFileName,wxT("_h.xyz"));
        if(dialog.ShowModal() == wxID_YES) {
                        XYZFile::LoadFile(xyzHFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
        }else {
                        XYZFile::LoadFile(xyzFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
        }
                ////AddOutputFile(xyzFileName,true);
                ////AddOutputFile(xyzHFileName,true);
        }else {
        if(XYZFile::IsXYZFile(strFileName)) {
                        XYZFile::LoadFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
                }else if(XYZFile::IsQChemXYZFile(strFileName)==true){
                        XYZFile::LoadQchemFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
                }else if(XYZFile::IsQChemInnerFile(strFileName)==true){
                        XYZFile::LoadQChemTestFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
                }else {
                        Tools::ShowError(wxT("Sorry, cannot recognize the file format!"));
                        return false;
                }
    }
    GetRenderingData()->TagConnectionGroup();
    m_strFileName = FileUtil::NormalizeFileName(strFileName);
    return true;
}

void CtrlInst::SaveFile(const wxString& strFileName, bool isExport) {
        wxString filename = strFileName;
        if (StringUtil::IsEmptyString(filename)) {
                return ;
        }
        wxString lowerCaseFile = strFileName.Lower();
        if (lowerCaseFile.EndsWith(wxT(".sim"))) {
                MolFile::SaveFile(strFileName, GetRenderingData(), false);
        }else if (lowerCaseFile.EndsWith(wxT(".xyz"))) {
                XYZFile::SaveFile(strFileName, GetRenderingData()->GetAtomBondArray(), false);
        }else if(lowerCaseFile.EndsWith(wxT(".pdb"))) {

        }
        if(!isExport) {
            m_strFileName = FileUtil::NormalizeFileName(strFileName);
        }
        wxString xyzfilename = FileUtil::ChangeFileExt(strFileName, wxT(".xyz"));
    XYZFile::SaveFile(xyzfilename, GetRenderingData()->GetAtomBondArray(), false);
}



bool CtrlInst::AddSelectedAtom(int selectedAtomIndex, bool isAppend) {
        return m_selUtil.AddSelectedIndex(selectedAtomIndex, isAppend);
}

//bool CtrlInst::AddSelectedIndexLimited(int selectedMax, int selectedIndex) {
//    return m_selUtil.AddSelectedIndexLimited(selectedMax, selectedIndex);
//}

void CtrlInst::ClearSelectedAtoms(void) {
        m_selUtil.ClearAll();
}

void CtrlInst::LoadPreviewData(void) {
    ClearSelectedAtoms();
    ResetGLPositions();
    m_pRenderingData->LoadPreviewData();
    if(m_pRenderingData->GetAtomCount() > 1) {
        //set the second atom to be selected by default
                AddSelectedAtom(1, false);
                AtomPosition3F center = m_pRenderingData->GetGroupCentroid(BP_NULL_VALUE);
                center.x = -center.x;
                center.y = -center.y;
                center.z = -center.z;
                camera.Translate(center);
        }
}

AtomBondArray CtrlInst::GetPreviewData(void) {
        AtomBondArray prevData = m_pRenderingData->CloneAtomBondArray();
        AtomBondArray chemData = m_pRenderingData->GetAtomBondArray();
        int k;
        unsigned i, j;
        int selectedIndex = m_selUtil.GetSelectedIndex(0);

        if( selectedIndex == BP_NULL_VALUE)
                return prevData;

        if(!prevData[selectedIndex].atom.IsDefaultHydrogen())
                return prevData;

        if(selectedIndex == 1 || prevData[selectedIndex].bondCount != 1)
                return prevData;

        int changeBase = prevData[selectedIndex].bonds[0].atomIndex;
        AtomPosition pos;
        pos.x = prevData[changeBase].atom.pos.x;
        pos.y = prevData[changeBase].atom.pos.y;
        pos.z = prevData[changeBase].atom.pos.z;

        wxIntArray list;
        list.Add(changeBase);
        list.Add(selectedIndex);

        for(k = 0; k < prevData[changeBase].bondCount; k++) {
                if(prevData[changeBase].bonds[k].atomIndex != selectedIndex) {
                        list.Add(prevData[changeBase].bonds[k].atomIndex);
                }
        }

        bool found;
        for(i = 0; i < prevData.GetCount(); i++) {
                found = false;
                for(j = 0; j < list.GetCount(); j++){
                        if(list[j] == (int)i) {
                                found = true;
                                break;
                        }
                }
                if(!found) {
                        list.Add(i);
                }
        }

        wxIntHash tran;
        for(i = 0; i < list.GetCount(); i++) {
                tran[list[i]]=i;
                prevData[i] = chemData[list[i]];
                //prevData[i].atom.selected=false;
                prevData[i].atom.pos.x -= pos.x;
                prevData[i].atom.pos.y -= pos.y;
                prevData[i].atom.pos.z -= pos.z;
        }

        for(i = 0; i < prevData.GetCount(); i++) {
                for(k = 0; k < prevData[i].bondCount; k++) {
                        prevData[i].bonds[k].atomIndex = tran.find(prevData[i].bonds[k].atomIndex)->second;
                }
        }
        //DumpData(ret);
        return prevData;
}

AtomPosition3F CtrlInst::GetRotationCenter(void) {
        float centerRadius;
        return GetRotationCenter(centerRadius);
}

AtomPosition3F CtrlInst::GetRotationCenter(float& centerRadius) {
        AtomPosition3F center;
        float radius = 0.0f;
        if(mouseClickedAtomIndex >= 0 && mouseClickedAtomIndex < GetRenderingData()->GetAtomCount()) {
                AtomPosition atomPos = GetRenderingData()->GetAtomBondArray()[mouseClickedAtomIndex ].atom.pos;
                center.x = (float)atomPos.x;
                center.y = (float)atomPos.y;
                center.z = (float)atomPos.z;
                radius = GetRenderingData()->GetAtomBondArray()[mouseClickedAtomIndex ].atom.GetDispRadius(modelType);
        }else {
                center = GetRenderingData()->GetGroupCentroid(BP_NULL_VALUE);
        }
        centerRadius = radius;
        return center;
}

bool CtrlInst::CanUndo(void) {
    if(m_hisFileArray.IsEmpty() || m_currentHisFileIndex <= 0)
                return false;
        return m_currentHisFileIndex <= (int)m_hisFileArray.GetCount();
}

bool CtrlInst::CanRedo(void) {
    if(m_hisFileArray.IsEmpty() || m_currentHisFileIndex >= (int)m_hisFileArray.GetCount()-1 )
                return false;
        return (m_currentHisFileIndex < (int)m_hisFileArray.GetCount()-1) ;
}

void CtrlInst::RedoMolecule(void) {
        if(m_currentHisFileIndex>= (int)m_hisFileArray.GetCount())
                //return false;
                return ;
        m_currentHisFileIndex++;
        m_pRenderingData->Empty();
        MolFile::LoadFile(m_hisFileArray[m_currentHisFileIndex], GetRenderingData());
        m_pRenderingData->TagConnectionGroup();
        m_selUtil.ClearSelection();
        ////wxGetApp().GetMainFrame()->SetMenuToolStatusCtrlInst();
        //return true;
}
void CtrlInst::UndoMolecule(void) {
        if(m_currentHisFileIndex == (int)m_hisFileArray.GetCount()){
                wxString tempFileName = wxFileName::CreateTempFileName(EnvUtil::GetHistoryDir() + wxT("/"));
                MolFile::SaveFile(tempFileName, GetRenderingData(), true);
                m_hisFileArray.Add(tempFileName);
        }
        m_pRenderingData->Empty();
        MolFile::LoadFile(m_hisFileArray[m_currentHisFileIndex - 1], GetRenderingData());
        m_pRenderingData->TagConnectionGroup();
        m_currentHisFileIndex--;
        m_selUtil.ClearSelection();
        // std::cout << "UndoMolecule: " << m_hisFileArray.Count() << ";" << std::endl;
////        wxGetApp().GetMainFrame()->SetMenuToolStatusCtrlInst();
        //return true;
}

void CtrlInst::SaveHistroyFile(void) {
        wxString tempFileName = wxFileName::CreateTempFileName(EnvUtil::GetHistoryDir() + wxT("/"));
        MolFile::SaveFile(tempFileName, GetRenderingData(), true);
        m_hisFileArray.Add(tempFileName);
        m_currentHisFileIndex = m_hisFileArray.GetCount();
////        wxGetApp().GetMainFrame()->SetMenuToolStatusCtrlInst();
}

bool CtrlInst::GetSelectedPosition(int atomIndex, AtomPosition& atomPos) {
    int i = 0;
        if(GetSelectionUtil()->GetSelectedCount() > atomIndex) {
                AtomBondArray& chemArray = GetRenderingData()->GetAtomBondArray();
                for(i = 0; i < 3; i++) {
            atomPos[i] = chemArray[GetSelectionUtil()->GetSelectedIndex(atomIndex)].atom.pos[i];
        }
        return true;
        }
        return false;
}

int CtrlInst::GetOldAngleValue(int atomStill, int atomAxis, int atomMove, double& value) {
    unsigned i = 0;
    AtomBondArray atomBondArray = GetRenderingData()->GetAtomBondArray();

        for(i = 0; i < m_atomAngleArray.GetCount(); i++) {
        if(m_atomAngleArray[i].atomAxisUniqueId == atomBondArray[atomAxis].atom.uniqueId) {
            if( (m_atomAngleArray[i].atomStillUniqueId == atomBondArray[atomStill].atom.uniqueId
                && m_atomAngleArray[i].atomMoveUniqueId == atomBondArray[atomMove].atom.uniqueId)
                || (m_atomAngleArray[i].atomMoveUniqueId == atomBondArray[atomStill].atom.uniqueId
                && m_atomAngleArray[i].atomStillUniqueId == atomBondArray[atomMove].atom.uniqueId)) {
                    value = m_atomAngleArray[i].angle;
                  //  wxMessageBox(wxString::Format(wxT("get %d %d %d, %f"), atomStill, atomAxis, atomMove, value));
                    return i;
                }
        }
    }
    value = -1.0f;
    return BP_NULL_VALUE;
}

void CtrlInst::SetOldAngleValue(int atomStill, int atomAxis, int atomMove, double value) {
    AtomBondArray atomBondArray = GetRenderingData()->GetAtomBondArray();
    double oldValue = 0;
    int index = GetOldAngleValue(atomStill, atomAxis, atomMove, oldValue);

    if(index != BP_NULL_VALUE) {
        m_atomAngleArray[index].angle = value;
    }else {
        AtomAngle atomAngle;
        atomAngle.atomStillUniqueId = atomBondArray[atomStill].atom.uniqueId;
        atomAngle.atomAxisUniqueId = atomBondArray[atomAxis].atom.uniqueId;
        atomAngle.atomMoveUniqueId = atomBondArray[atomMove].atom.uniqueId;
        atomAngle.angle = value;
        m_atomAngleArray.Add(atomAngle);
    }
    //wxMessageBox(wxString::Format(wxT("set %d %d %d, %f"), atomStill, atomAxis, atomMove, value));
}

bool CtrlInst::MakeBond(int selectedAtomIndex) {
        bool isSucc = true;
        int selected[2];
        if(!m_pRenderingData->GetAtomBondArray()[selectedAtomIndex].atom.IsDefaultHydrogen()) {
                return isSucc;
        }
        if(m_selUtil.GetSelectedCount() == 1) {
        selected[0] = m_selUtil.GetSelectedIndex(0);
                if(m_pRenderingData->GetAtomBondArray()[selected[0]].bonds[0].atomIndex == m_pRenderingData->GetAtomBondArray()[selectedAtomIndex].bonds[0].atomIndex)
                        return isSucc;
        }
        if(m_selUtil.AddSelectedIndexLimited(2, selectedAtomIndex)) {
                selected[1] = m_selUtil.GetSelectedIndex(1);
                isSucc = m_pRenderingData->MakeBond(selected);
                ClearSelectedAtoms();
        }
        return isSucc;
}

void CtrlInst::LoadOptFile(wxString &strFileName)
{
    if (StringUtil::IsEmptyString(strFileName)) {
                wxMessageBox(_("IsEmpty"));
        return ;
    }
        if(!wxFileExists(strFileName)){
                wxMessageBox(strFileName+_("No Exists File"));
                return;
        }
    wxString lowerCaseFile = strFileName.Lower();

        if(lowerCaseFile.EndsWith(wxT(".sim"))) {
        //        wxMessageBox(_("Begin load sim"));
        MolFile::LoadFile(strFileName, GetRenderingData());
    }else if(lowerCaseFile.EndsWith(wxT(".xyz"))) {
        XYZFile::LoadFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
        }else {
        if(XYZFile::IsXYZFile(strFileName)) {
                        XYZFile::LoadFile(strFileName, m_pRenderingData->GetAtomBondArray(), m_pRenderingData->GetAtomLabels());
                }else {
                        Tools::ShowError(wxT("Sorry, cannot recognize the file format!"));
                        return ;
                }
    }
    //DumpRenderingData(m_atomBondArray, GetLogDir() + wxT("/gmfile.dat"));
    GetRenderingData()->TagConnectionGroup();
}

bool CtrlInst::GetReturn(wxString &strFileName)
{
        long flag;
    if (StringUtil::IsEmptyString(strFileName)) {
        return false;
    }
        if(!wxFileExists(strFileName))
                return false;
    wxString lowerCaseFile = strFileName.Lower();
    wxString strLine, value;
        wxFileInputStream fis(strFileName);
        wxTextInputStream tis(fis);
        wxString FILE_RETURN_COOR = wxT("$BEGIN RETURN");
        flag=0;
        do{
                strLine=tis.ReadLine();
        }while(FILE_RETURN_COOR.Cmp(strLine)!=0);
        if(FILE_RETURN_COOR.Cmp(strLine) == 0)
        {
        strLine=tis.ReadLine();
                strLine.ToLong(&flag);
         }
        if (flag!=0)
                return true;
        else
                return false;

}

AbstractSurface* CtrlInst::GetSurface(void) const {
        return m_surface;
}

void CtrlInst::SetSurface(AbstractSurface* surface) {
        if(m_surface) {
                delete m_surface;
        }
        m_surface = surface;
}

AtomListFrm* CtrlInst::GetAtomListFrm(bool createIt) {
        if(m_pAtomListFrm == NULL && createIt) {
                m_pAtomListFrm = new AtomListFrm(NULL, wxID_ANY,wxT("Atom List Editor"));
                //m_pAtomListFrm = new AtomListFrm(NULL, wxID_ANY);
        //                m_pAtomListFrm = new AtomListFrm(this, wxID_ANY);
        //m_pAtomListFrm = new AtomListFrm(NULL, wxID_ANY, wxT("Atom List Editor"),wxDefaultPosition, wxDefaultSize, wxCAPTION | wxMINIMIZE_BOX | wxMAXIMIZE_BOX  |wxCLOSE_BOX | wxRESIZE_BORDER | wxRESIZE_BORDER);

        }
        return m_pAtomListFrm;
}

void CtrlInst::FreeAtomListFrm(void) {
        if(m_pAtomListFrm != NULL) {
                m_pAtomListFrm->Show(false);
                m_pAtomListFrm->Destroy();
                delete m_pAtomListFrm;
                m_pAtomListFrm = NULL;
        }
}

wxString CtrlInst::GetFullPathName(void) {
    if(StringUtil::IsEmptyString(m_strProjectPath) || StringUtil::IsEmptyString(m_strLoadedFileName)) {
        return wxEmptyString;
    }
    return FileUtil::NormalizeFileName(m_strProjectPath + wxFileName::GetPathSeparator() + m_strLoadedFileName);
}

void CtrlInst::DoSymme(wxString file) {

        wxString tempFileName = wxFileName::CreateTempFileName(EnvUtil::GetHistoryDir() + wxT("/"));

        ifstream in(file.ToAscii());
        string s;
        vector<string> v;

        while (getline(in, s)) {
                v.push_back(s);
        }
        ofstream out(tempFileName.ToAscii());
        for (unsigned i=0; i<v.size(); ++i) {
                out << v[i] << endl;
        }
        out.close();

        in.close();



//        MolFile::SaveFile(tempFileName, GetRenderingData(), true);

        m_hisFileArray.Add(tempFileName);
        m_currentHisFileIndex = m_hisFileArray.GetCount();

        m_pRenderingData->Empty();
        MolFile::LoadFile(m_hisFileArray[m_currentHisFileIndex - 1], GetRenderingData());
        m_pRenderingData->TagConnectionGroup();
        m_currentHisFileIndex--;


//        GetUIProps()->LoadProps();
//        openGLProps.mainDisableBgColor.ToColor(wxT("0,0,0"));
//        GetUIProps()->openGLProps.previewBgColor.ToColor(wxT("0,0,0"));


//        wxMemoryDC dc;
//
//        wxSize size = MolViewEventHandler::Get()->GetMolView()->GetGLCanvas()->GetSize();
//
//        wxBitmap bmp(size.x, size.y);
//
//        dc.SelectObject(bmp);
//
//        MolViewEventHandler::Get()->GetMolView()->GetGLCanvas()->PaintCanvas(dc);
//
//        dc.SelectObject(wxNullBitmap);
//
//        bmp.SaveFile(wxT("/home/allan/a.bmp"),wxBITMAP_TYPE_BMP);
////MolViewEventHandler::Get()->GetMolView()->GetGLCanvas()->

        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();

}
