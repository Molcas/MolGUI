/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>

#include "xyzfile.h"
#include "measure.h"
#include "commons.h"
#include "tools.h"
#include "stringutil.h"

#include "envutil.h"
#include "fileutil.h"

// fenglai: we extend the bond limit so that under all
// cases the bond can be "visulized" formed
// Nov. 2ed, 2010
#define BOND_LIMIT              1.17
#define SINGLE_BOND_LIMIT       0.94
#define PI_BOND_LIMIT           0.87
#define DOUBLE_BOND_LIMIT       0.77
#define TRIPLE_BOND_LIMIT       0.60
#define R529 0.529177249f

/**
 * here we use -1 to end the array, and the element in the array is the atom id
 * and now we are not define the fourth bond list
 */
const int PI_BOND_LIST[] = {6, 7, 14, 16, BP_NULL_VALUE};
const int DOUBLE_BOND_LIST[] = {6, 7, 8, 14, 15, 16, BP_NULL_VALUE };
const int TRIPLE_BOND_LIST[] = {6, 7, 14, BP_NULL_VALUE};

/**
 * The format xyz file is:
 *      Line 1: 5                   // the atom number
 *      Line 2:                     // comment, may be a blank line
 *      Line 3: H x y z             // element symbol, followed by its x, y, and z coordinate
 */
void XYZFile::LoadFromDen(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels){
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
        ElemInfo* eleInfoOption = NULL;
        int i = 0, atom2Index;
        BondType bondType = BOND_TYPE_1;
        wxString atomSymbol = wxEmptyString;
    int atomId;
    double x, y, z;
    long atomNumber;
    long lAtomIndex;

        int atomCountBase = appendAtomBondArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;
        strLine = tis.ReadLine();
        strLine = tis.ReadLine();
        strLine = tis.ReadLine();
    wxStringTokenizer tzk(strLine);
        tzk.GetNextToken();
        tzk.GetNextToken().ToLong(&atomNumber); // the number of atoms
    for(i = 0; i < atomNumber; i++) {
        strLine = tis.ReadLine();       // read one line
                wxStringTokenizer tzk(strLine);
        AtomBond atomBond = AtomBond();
        atomSymbol = tzk.GetNextToken();
        if(atomSymbol.ToLong(&lAtomIndex)) {
                        atomId = (int)lAtomIndex;
                }else {
                atomId = GetElemId(atomSymbol);
                }
        tzk.GetNextToken().ToDouble(&x);
        tzk.GetNextToken().ToDouble(&y);
        tzk.GetNextToken().ToDouble(&z);
        atomBond.atom.elemId = (int)atomId;
        if(atomBond.atom.elemId == ELEM_H) {
            atomBond.atom.elemId = (ElemId)DEFAULT_HYDROGEN_ID;
        }
        atomBond.atom.pos.x = x*R529;
        atomBond.atom.pos.y = y*R529;
        atomBond.atom.pos.z = z*R529;
        appendAtomBondArray.Add(atomBond);
    }

    //create bond type
   for(i = atomCountBase; i < atomCountBase + atomNumber; i++) {
        eleInfoOption = GetElemInfo((ElemId)appendAtomBondArray[i].atom.elemId);

        appendAtomBondArray[i].atom.SetDispRadius(eleInfoOption->dispRadius);
        appendAtomBondArray[i].atom.label = ++atomLabels[ELEM_PREFIX + appendAtomBondArray[i].atom.elemId];

            for(atom2Index = i + 1; atom2Index < atomCountBase + atomNumber; atom2Index++) {
            if(appendAtomBondArray[i].bondCount >= ATOM_MAX_BOND_COUNT) break;
            if(appendAtomBondArray[atom2Index].bondCount >= ATOM_MAX_BOND_COUNT) continue;
            bondType = CalculateBondType(i, atom2Index, appendAtomBondArray);

            if (bondType == BOND_TYPE_0) {
            }else if (bondType == BOND_TYPE_1 || bondType == BOND_TYPE_2 || bondType == BOND_TYPE_3 || bondType == BOND_TYPE_4) {
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].atomIndex = atom2Index;
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].bondType = bondType;

                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].atomIndex = i;
                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].bondType = bondType;

                appendAtomBondArray[i].bondCount++;
                appendAtomBondArray[atom2Index].bondCount++;
            }else if (bondType == BOND_TYPE_5) {
                Tools::ShowError(wxT("Until now we have not supported the fourth bond list!"));
            }else {
                Tools::ShowError(wxT("The bond type is wrong!!!"));
            }
        }
    }
}

void XYZFile::LoadFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels) {
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
        ElemInfo* eleInfoOption = NULL;
        int i = 0, atom2Index;
        BondType bondType = BOND_TYPE_1;
        wxString atomSymbol = wxEmptyString;
    int atomId;
    double x, y, z;
    long atomNumber;
    long lAtomIndex;

        int atomCountBase = appendAtomBondArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;
        strLine = tis.ReadLine();
    strLine.ToLong(&atomNumber); // the number of atoms
    tis.ReadLine();         // omit the comment line

    for(i = 0; i < atomNumber; i++) {
        strLine = tis.ReadLine();       // read one line
        wxStringTokenizer tzk(strLine);
        AtomBond atomBond = AtomBond();
        atomSymbol = tzk.GetNextToken();
        if(atomSymbol.ToLong(&lAtomIndex)) {
                        atomId = (int)lAtomIndex;
                }else {
                atomId = GetElemId(atomSymbol);
                }
        tzk.GetNextToken().ToDouble(&x);
        tzk.GetNextToken().ToDouble(&y);
        tzk.GetNextToken().ToDouble(&z);
        atomBond.atom.elemId = (int)atomId;
        if(atomBond.atom.elemId == ELEM_H) {
            atomBond.atom.elemId = (ElemId)DEFAULT_HYDROGEN_ID;
        }
        atomBond.atom.pos.x = x;
        atomBond.atom.pos.y = y;
        atomBond.atom.pos.z = z;
        appendAtomBondArray.Add(atomBond);
    }

    //create bond type
  for(i = atomCountBase; i < atomCountBase + atomNumber; i++) {
        eleInfoOption = GetElemInfo((ElemId)appendAtomBondArray[i].atom.elemId);

        appendAtomBondArray[i].atom.SetDispRadius(eleInfoOption->dispRadius);
        appendAtomBondArray[i].atom.label = ++atomLabels[ELEM_PREFIX + appendAtomBondArray[i].atom.elemId];

            for(atom2Index = i + 1; atom2Index < atomCountBase + atomNumber; atom2Index++) {
            if(appendAtomBondArray[i].bondCount >= ATOM_MAX_BOND_COUNT) break;
            if(appendAtomBondArray[atom2Index].bondCount >= ATOM_MAX_BOND_COUNT) continue;
            bondType = CalculateBondType(i, atom2Index, appendAtomBondArray);

            if (bondType == BOND_TYPE_0) {
            }else if (bondType == BOND_TYPE_1 || bondType == BOND_TYPE_2 || bondType == BOND_TYPE_3 || bondType == BOND_TYPE_4) {
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].atomIndex = atom2Index;
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].bondType = bondType;

                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].atomIndex = i;
                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].bondType = bondType;

                appendAtomBondArray[i].bondCount++;
                appendAtomBondArray[atom2Index].bondCount++;
            }else if (bondType == BOND_TYPE_5) {
                Tools::ShowError(wxT("Until now we have not supported the fourth bond list!"));
            }else {
                Tools::ShowError(wxT("The bond type is wrong!!!"));
            }
        }
    }
}

BondType XYZFile::CalculateBondType(int atom1Index, int atom2Index, AtomBondArray& atomBondArray) {
    BondType  bondType = BOND_TYPE_0;

    ElemId atom1Id = (ElemId)atomBondArray[atom1Index].atom.elemId;
    ElemId atom2Id = (ElemId)atomBondArray[atom2Index].atom.elemId;

    double covalentBondLength = GetElemInfo(atom1Id)->covalentRadius + GetElemInfo(atom2Id)->covalentRadius;
    double bondLength = MeasureBondDistance(atom1Index, atom2Index, atomBondArray);


    if (bondLength > covalentBondLength*BOND_LIMIT) {
        bondType = BOND_TYPE_0;
    } else if (bondLength > covalentBondLength*SINGLE_BOND_LIMIT && bondLength <= covalentBondLength*BOND_LIMIT) {
        bondType = BOND_TYPE_1;
    } else if (bondLength > covalentBondLength*PI_BOND_LIMIT && bondLength <= covalentBondLength*SINGLE_BOND_LIMIT) {
        bondType = BOND_TYPE_4;
    } else if (bondLength > covalentBondLength*DOUBLE_BOND_LIMIT && bondLength <= covalentBondLength*PI_BOND_LIMIT) {
        bondType = BOND_TYPE_2;
    } else if (bondLength > covalentBondLength*TRIPLE_BOND_LIMIT && bondLength <= covalentBondLength*DOUBLE_BOND_LIMIT) {
        bondType = BOND_TYPE_3;
    } else {
        bondType = BOND_TYPE_5;
    }
    //
    //wxLogError(wxString::Format("%f, %f, %d", bondLength, covalentBondLength, (int)bondType));

    switch(bondType) {
        case BOND_TYPE_0:
            break;
        case BOND_TYPE_1:
            break;
        case BOND_TYPE_2:
            if(SearchBondList(atom1Id, atom2Id, DOUBLE_BOND_LIST)) {
            }else {
                bondType = BOND_TYPE_1;
            }
            break;
        case BOND_TYPE_3:
            if(SearchBondList(atom1Id, atom2Id, TRIPLE_BOND_LIST)) {
            }else if(SearchBondList(atom1Id, atom2Id, DOUBLE_BOND_LIST)) {
                bondType = BOND_TYPE_2;
            }else {
                bondType = BOND_TYPE_1;
            }
            break;
        case BOND_TYPE_4:
            if(SearchBondList(atom1Id, atom2Id, PI_BOND_LIST)) {
            }else if(SearchBondList(atom1Id, atom2Id, DOUBLE_BOND_LIST)) {
                bondType = BOND_TYPE_2;
            }else {
                bondType = BOND_TYPE_1;
            }
            break;
        default:
            break;

    }
    //if(bondType > 0)
    //wxLogError(wxString::Format("===%f, %f, %d", bondLength, covalentBondLength, (int)bondType));

    return bondType;
}
bool XYZFile::ChangePDBFile(const wxString& pdbFile,const wxString& nh_xyzFile,const wxString& h_xyzFile)
{
 printf("pdb handling is not implemented");
 return false;

        wxString        dirName = wxT("modules/pdb");
        wxString        exeName=EnvUtil::GetProgramName(wxT("pdb_xyz_h"));
        exeName=FileUtil::CheckFile(dirName,exeName,true);

         exeName=exeName+wxT(" ")+pdbFile+wxT(" ")+nh_xyzFile+wxT(" ")+h_xyzFile;
//wxMessageBox(exeName);
        wxString tmpStr=EnvUtil::GetWorkDir()+platform::PathSep()+wxT("tmp.tmp");;
        if(Tools::ExecModule(exeName, tmpStr, tmpStr)==true){
        }
        return true;
}
bool XYZFile::SearchBondList(int atom1Id, int atom2Id, const int* bondList) {
    int i;
    bool atom1 = false;
    bool atom2 = false;

    for (i = 0; bondList[i] != BP_NULL_VALUE; i++) {
        if (atom1Id == bondList[i]) {
            atom1 = true;
            break;
        }
    }

    for (i = 0; bondList[i] != BP_NULL_VALUE; i++) {
        if (atom2Id == bondList[i]) {
            atom2 = true;
            break;
        }
    }

    return (atom1 && atom2);
}

void XYZFile::SaveFile(const wxString& fileName, AtomBondArray& bondArray, bool useTab) {
    unsigned int i;
    int atomId,atomNum;


    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        wxString format = wxEmptyString;
        atomNum=bondArray.GetCount();
    for(i = 0; i < bondArray.GetCount(); i++) {
        if(bondArray[i].atom.elemId == DEFAULT_HYDROGEN_ID) {
            atomId = (int)ELEM_H;
        }else{
            atomId = bondArray[i].atom.elemId ;
        }
                if(atomId >= ELEM_COUNT || atomId < 1) atomNum--;
        }
    tos << wxString::Format(wxT("%d"), atomNum) << endl;
    tos << wxString(wxT("#Generated with MolGUI")) << endl;

    for(i = 0; i < bondArray.GetCount(); i++) {
        if(bondArray[i].atom.elemId == DEFAULT_HYDROGEN_ID) {
            atomId = (int)ELEM_H;
        }else{
            atomId = bondArray[i].atom.elemId ;
        }
        if(atomId >= ELEM_COUNT || atomId < 1) {
            //wxLogError(wxString::Format(wxT("AtomId %d has not corresponding symboles in current version"), atomId));
        }else{
            if(useTab)
                    format = ELEM_NAMES[atomId - 1] + wxT("\t%15.8f\t%15.8f\t%15.8f\n");
            else
                    format = ELEM_NAMES[atomId - 1] + wxT(" %15.8f %15.8f %15.8f\n");
            tos << wxString::Format(format, bondArray[i].atom.pos.x, bondArray[i].atom.pos.y, bondArray[i].atom.pos.z);
        }
    }
}

void XYZFile::SaveCoord(const wxString& fileName, AtomBondArray& bondArray, bool useTab) {
    unsigned int i;
    int atomId;

    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        wxString format = wxEmptyString;

//    tos << wxString::Format(wxT("%d"), bondArray.GetCount()) << endl;
//    tos << wxString(wxT("#Generated with MolGUI")) << endl;

    for(i = 0; i < bondArray.GetCount(); i++) {
        if(bondArray[i].atom.elemId == DEFAULT_HYDROGEN_ID) {
            atomId = (int)ELEM_H;
        }else{
            atomId = bondArray[i].atom.elemId ;
        }
        if(atomId >= ELEM_COUNT || atomId < 1) {
            //wxLogError(wxString::Format(wxT("AtomId %d has not corresponding symboles in current version"), atomId));
        }else{
            if(useTab)
                    format = ELEM_NAMES[atomId - 1] + wxT("\t%12.5f\t%12.5f\t%12.5f\n");
            else
                    format = ELEM_NAMES[atomId - 1] + wxT(" %f %f %f\n");
            tos << wxString::Format(format, bondArray[i].atom.pos.x, bondArray[i].atom.pos.y, bondArray[i].atom.pos.z);
        }
    }
}

bool XYZFile::IsXYZFile(const wxString &fileName)
{
        wxString strLine;
        wxArrayString strArr;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
    long atomId;
    long atomNumber;
        strLine = tis.ReadLine();
        //wxMessageBox(strLine);
    strLine.ToLong(&atomNumber); // the number of atoms
        if(atomNumber<=0)
                return false;
    tis.ReadLine();         // omit the comment line
    for(long i = 0; i < atomNumber; i++)
        {
        strLine = tis.ReadLine();       // read one line
                //wxMessageBox(strLine);
                strArr=wxStringTokenize(strLine,wxT(" \t\n\r"));
                if(strArr.GetCount()!=4)
                        return false;
        atomId = GetElemId(strArr[0]);
                if(atomId == ELEM_NULL)
                        return false;
                if(fis.Eof())
                        return false;
    }
        return true;
}


void XYZFile::LoadQchemFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels)
{
        wxString strLine;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
        ElemInfo* eleInfoOption = NULL;
        int i = 0, atom2Index;
        BondType bondType = BOND_TYPE_1;
        wxString atomSymbol = wxEmptyString;
//    int atomId;
//    double x, y, z;
//    long atomNumber;
    long lAtomIndex;
        long atomNumber=0;

        int atomCountBase = appendAtomBondArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;
        strLine = tis.ReadLine(); //read the first line of Q-Chem file

//        while(FILE_QCHEM_BEGIN_COOR.Cmp(strLine) != 0) //jump over the lines before the $molecule begin
        while(!strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
                strLine=tis.ReadLine();
        }

//        if(FILE_QCHEM_BEGIN_COOR.Cmp(strLine) == 0)
        if(strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
        //load atom info
                strLine = tis.ReadLine();  //read the second line of Q-chem file
        long atomId;
        double x, y, z;
//        while( FILE_QCHEM_END_COOR.Cmp(strLine = tis.ReadLine()) != 0)
                strLine=tis.ReadLine(); //add
                while(!strLine.Lower().Contains(FILE_QCHEM_END_COOR))
                {
 //       strLine = tis.ReadLine();       // read one line
                        wxStringTokenizer tzk(strLine);
                        AtomBond atomBond = AtomBond();
                        atomSymbol = tzk.GetNextToken();
/*                        if(atomSymbol.ToLong(&lAtomIndex))
                        {
                                atomId = (int)lAtomIndex;
                        }
                        else
                        {
                        atomId = GetElemId(atomSymbol);
                        }
*/
                wxString last=atomSymbol.Last();
                wxString findnumber;
                if((last.IsNumber())==false)  //the situation of C, H
                {
            atomId=(int)GetElemId(atomSymbol);
                }
                else if(atomSymbol.IsNumber()) //the situation of 6 8
                {
                        atomSymbol.ToLong(&lAtomIndex);
                        atomId = (int)lAtomIndex;
                }

                else  //the situation of H1,H3
                {
                           for(int j=1;j<10;j++)
                        {
                          findnumber=atomSymbol.GetChar(j);
                          if((findnumber.IsNumber())==true)
                                        break;
                        }
                        wxChar firstnum;
                        firstnum=findnumber.GetData()[0];
                        wxString atomSymbolchar = atomSymbol.BeforeLast(firstnum);
                        atomId=(int)GetElemId(atomSymbolchar);
                }


                        tzk.GetNextToken().ToDouble(&x);
                        tzk.GetNextToken().ToDouble(&y);
                        tzk.GetNextToken().ToDouble(&z);
                        atomBond.atom.elemId = (int)atomId;
                        if(atomBond.atom.elemId == ELEM_H)   //ensure to add the following 3lines, in order to add the molecular
                        {
            atomBond.atom.elemId = (ElemId)DEFAULT_HYDROGEN_ID;
                        }

                        atomBond.atom.pos.x = x;
                        atomBond.atom.pos.y = y;
                        atomBond.atom.pos.z = z;
                        appendAtomBondArray.Add(atomBond);
                        atomNumber=atomNumber+1;
                        strLine=tis.ReadLine();
                }
//                strLine = tis.ReadLine();
    }
                //create bond type
  for(i = atomCountBase; i < atomCountBase + atomNumber; i++)
  {
        eleInfoOption = GetElemInfo((ElemId)appendAtomBondArray[i].atom.elemId);

        appendAtomBondArray[i].atom.SetDispRadius(eleInfoOption->dispRadius);
        appendAtomBondArray[i].atom.label = ++atomLabels[ELEM_PREFIX + appendAtomBondArray[i].atom.elemId];

            for(atom2Index = i + 1; atom2Index < atomCountBase + atomNumber; atom2Index++)
                {
            if(appendAtomBondArray[i].bondCount >= ATOM_MAX_BOND_COUNT) break;
            if(appendAtomBondArray[atom2Index].bondCount >= ATOM_MAX_BOND_COUNT) continue;
            bondType = XYZFile::CalculateBondType(i, atom2Index, appendAtomBondArray);

            if (bondType == BOND_TYPE_0)
                        {
            }
                        else if (bondType == BOND_TYPE_1 || bondType == BOND_TYPE_2 || bondType == BOND_TYPE_3 || bondType == BOND_TYPE_4)
                        {
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].atomIndex = atom2Index;
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].bondType = bondType;

                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].atomIndex = i;
                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].bondType = bondType;

                appendAtomBondArray[i].bondCount++;
                appendAtomBondArray[atom2Index].bondCount++;
            }
                        else if (bondType == BOND_TYPE_5)
                        {
                Tools::ShowError(wxT("Until now we have not supported the fourth bond list!"));
            }else
                        {
                Tools::ShowError(wxT("The bond type is wrong!!!"));
            }
        }
    }
}

#ifndef STRING_HASH
#define STRING_HASH
    WX_DECLARE_HASH_MAP(wxString, wxString, wxStringHash, wxStringEqual, StringHash);
#endif

/*void GetZMatrixArrayFromTextArray(wxArrayString& strArr,AtomBondArray& appendAtomBondArray, ZMatrixArray& zmatrixArr)
{
        int i,start=-1;
        wxArrayString tokens;
        for( i= strArr.GetCount()-1; i>=0 ; i--)
        {
                if(StringUtil::IsEmptyString(strArr[i]))
                {
                        start=i+1;
                        break;
                }
        }

        //find the empty line in order to get the unknown value
        wxString key,value,zlabel, findnumber;
        wxString delim=" \t,=\n";
//        wxString delim=" =";
        StringHash vhash,ehash;
        long lvalue;
        double dvalue;
        //get the unknown value
        for( i=start; i < (int)strArr.GetCount(); i++)
        {
                tokens=wxStringTokenize(strArr[i],delim,wxTOKEN_STRTOK);
                key=tokens[0];
                value=tokens[1];
                vhash[key]=value;
        }
        //get the index
        for(i=1; i < start-1; i++)
        {
                tokens=wxStringTokenize(strArr[i],delim,wxTOKEN_STRTOK);
                key=tokens[0];
                value=wxString::Format(wxT("%d"),i-1);
                ehash[key]=value;
        }
        for(i=1; i< start-1; i++)
        {
                tokens=wxStringTokenize(strArr[i],delim,wxTOKEN_STRTOK);
                ZMatrix zmatrix = ZMatrix();
                AtomBond atomBond = AtomBond();
                int atomnumber=0;

        //get the label from the zmatrix
                zmatrix.label=tokens[0];
                zlabel=zmatrix.label;
               for(int j=1;j<10;j++)
                {
                  findnumber=zlabel.GetChar(j);
                  if((findnumber.IsNumber())==true)
                            break;
                }
                wxChar firstnum;
                firstnum=findnumber.GetData()[0];
                wxString atomSymbol = zlabel.BeforeLast(firstnum);
            atomBond.atom.elemId=(int)GetElemId(atomSymbol);

                if(atomBond.atom.elemId == ELEM_H)   //ensure to add the following 3lines, in order to add the molecular
                {
           atomBond.atom.elemId = (ElemId)DEFAULT_HYDROGEN_ID;
                }

                atomBond.atom.pos.x = 0.0;
                atomBond.atom.pos.y = 0.0;
                atomBond.atom.pos.z = 0.0;

                //The first atom zmatrix is null
                if(tokens.GetCount()==1)
                {
                        zmatrix.bond.atomIndex = BP_NULL_VALUE;
                        zmatrix.bond.value = BP_NULL_VALUE;
                        zmatrix.angle.atomIndex = BP_NULL_VALUE;
                        zmatrix.angle.value = BP_NULL_VALUE;
                        zmatrix.dihedral.atomIndex = BP_NULL_VALUE;
                        zmatrix.dihedral.value = BP_NULL_VALUE;
                }

                if(tokens.GetCount()>=3)  //read the "c2 c1 cc" line
                {
                        value=ehash.find(tokens[1])->second; //this sentence will cause crash
                        value.ToLong(&lvalue);
                        zmatrix.bond.atomIndex=(int)lvalue;
                        wxMessageBox(value);
                        if(StringUtil::IsDigital(tokens[2]))
                                value=tokens[2];
                        else
                                value=vhash.find(tokens[2])->second;
                        value.ToDouble(&dvalue);
                        zmatrix.bond.value=dvalue;
                        wxMessageBox(value);
                }
                if(tokens.GetCount()>=5)
                {
                        value=ehash.find(tokens[3])->second;
                        value.ToLong(&lvalue);
                        zmatrix.angle.atomIndex=(int)lvalue;
                                                wxMessageBox(value);

                        if(StringUtil::IsDigital(tokens[4]))
                                value=tokens[4];
                        else
                                value=vhash.find(tokens[4])->second;
                        value.ToDouble(&dvalue);
                        zmatrix.angle.value=dvalue;
                                                wxMessageBox(value);

                }
                if(tokens.GetCount()>=7)
                {
                        value=ehash.find(tokens[5])->second;
                        value.ToLong(&lvalue);
                        zmatrix.dihedral.atomIndex=(int)lvalue;
                                                wxMessageBox(value);

                        if(StringUtil::IsDigital(tokens[6]))
                                value=tokens[6];
                        else
                                value=vhash.find(tokens[6])->second;
                        value.ToDouble(&dvalue);
                        zmatrix.dihedral.value=dvalue;
                                                wxMessageBox(value);

                }
                zmatrixArr.Add(zmatrix);
                appendAtomBondArray.Add(atomBond);
        }
}
*/

void XYZFile::LoadQChemTestFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels)
{
        wxString strLine,strSubLine;
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
//        wxTextInputStream tissub(fis);
        ElemInfo* eleInfoOption = NULL;
        int i = 0, atom2Index;
        BondType bondType = BOND_TYPE_1;
        wxString atomSymbol = wxEmptyString;
//    long lAtomIndex;
        long atomNumber=0;

        strLine = tis.ReadLine();
//        while(FILE_QCHEM_BEGIN_COOR.Cmp(strLine) != 0)
        while(!strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
                strLine=tis.ReadLine();
        }
//        if(FILE_QCHEM_BEGIN_COOR.Cmp(strLine) == 0)
        if(strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
                wxArrayString strArray;
// there may be something wrong,wxWidget can't get the empty using the IsNull()
//        while(strLine=tis.ReadLine(),(strLine.IsEmpty())==false)

//                while(strLine=tis.ReadLine(),strLine.Cmp(wxT(" "))!=0)   //this line is work
//((strLine=tis.ReadLine()).Cmp(FILE_QCHEM_EMPTYLINE)!=0)||
                strLine=tis.ReadLine();
                while(!strLine.IsEmpty()&&(strLine.Cmp(FILE_QCHEM_EMPTYLINE)!=0)&&(strLine.Cmp(FILE_QCHEM_END_COOR)!=0))
                {
                        strArray.Add(strLine);
                        atomNumber=atomNumber+1;
                        strLine=tis.ReadLine();
                }
//                if(strLine.Lower().Cmp(FILE_QCHEM_END_COOR)!=0)
                if(!strLine.Lower().Contains(FILE_QCHEM_END_COOR))
                {
                        strArray.Add(strLine);//add the empty line
                        //while((strLine=tis.ReadLine()).Cmp(FILE_QCHEM_END_COOR)!=0)
                        strLine=tis.ReadLine();//add
                        while(!strLine.Lower().Contains(FILE_QCHEM_END_COOR))
                        {
                                strArray.Add(strLine);
                                strLine=tis.ReadLine();//add
                        }
                }
                ZMatrixArray appendZMatrixArray;
//                GetZMatrixArrayFromTextArray(strArray,appendAtomBondArray,appendZMatrixArray);

        int i,start=-1;
        wxArrayString tokens;
        for( i= strArray.GetCount()-1; i>=0 ; i--)
        {
                if(StringUtil::IsEmptyString(strArray[i]))
                {
                        start=i+1;
                        break;
                }
        }
//wxMessageBox("hello");
        //find the empty line in order to get the unknown value
/*        for( i= 0; i< strArr.GetCount() ; i++)
        {
//                wxString temp=strArr[i];
//wxMessageBox(temp);
                if(StringUtil::IsEmptyString(strArr[i]))
                {
                        start=i+1;
                        break;
                }
        }
*/
        wxString key,value,zlabel, findnumber;
        wxString delim=wxT(" \t,=\n");
//        wxString delim=" =";
        StringHash vhash,ehash;
        long lvalue;
        double dvalue;
        //get the unknown value
        if(start!=-1)
        for( i=start; i < (int)strArray.GetCount(); i++)
        {
                tokens=wxStringTokenize(strArray[i],delim,wxTOKEN_STRTOK);
                key=tokens[0];  //if the character is Upper, using MakeLower() to change it .
                value=tokens[1];
                vhash[key]=value;
        }
        //get the index
        if(start!=-1)
        for(i=1; i < start-1; i++)
//        for(i=1; i < atomNumber-1; i++)
        {
                wxString test=strArray[i];
                tokens=wxStringTokenize(strArray[i],delim,wxTOKEN_STRTOK);
                key=tokens[0];
                value=wxString::Format(wxT("%d"),i-1);
                ehash[key]=value;
        }

        if(start==-1)//add
                for(i=1;i<atomNumber;i++)
        {
                wxString test=strArray[i];
                tokens=wxStringTokenize(strArray[i],delim,wxTOKEN_STRTOK);
                key=tokens[0];
                value=wxString::Format(wxT("%d"),i-1);
                ehash[key]=value;
        }



//        for(i=1; i< start-1; i++)
        for(i=1; i< atomNumber; i++)
        {
                tokens=wxStringTokenize(strArray[i],delim,wxTOKEN_STRTOK);
                ZMatrix zmatrix = ZMatrix();
                AtomBond atomBond = AtomBond();

        //get the label from the zmatrix
                zmatrix.label=tokens[0].MakeLower();
                zlabel=zmatrix.label;
                wxString last=zlabel.Last();
                if((last.IsNumber())==false)
                {
            atomBond.atom.elemId=(int)GetElemId(zlabel);
                }
                else
                {
                           for(int j=1;j<10;j++)
                        {
                          findnumber=zlabel.GetChar(j);
                          if((findnumber.IsNumber())==true)
                                        break;
                        }
                        wxChar firstnum;
                        firstnum=findnumber.GetData()[0];
                        wxString atomSymbol = zlabel.BeforeLast(firstnum);
                        atomBond.atom.elemId=(int)GetElemId(atomSymbol);
                }
                if(atomBond.atom.elemId == ELEM_H)   //ensure to add the following 3lines, in order to add the molecular
                {
           atomBond.atom.elemId = (ElemId)DEFAULT_HYDROGEN_ID;
                }

                atomBond.atom.pos.x = 0.0;
                atomBond.atom.pos.y = 0.0;
                atomBond.atom.pos.z = 0.0;

                //The first atom zmatrix is null
                if(tokens.GetCount()==1)
                {
                        zmatrix.bond.atomIndex = BP_NULL_VALUE;
                        zmatrix.bond.value = BP_NULL_VALUE;
                        zmatrix.angle.atomIndex = BP_NULL_VALUE;
                        zmatrix.angle.value = BP_NULL_VALUE;
                        zmatrix.dihedral.atomIndex = BP_NULL_VALUE;
                        zmatrix.dihedral.value = BP_NULL_VALUE;
                }
//                        value=tokens[1];
                wxString lastchar;
                if(value.Len()>=2)  // to determine whether the row 1 is the format of C2
                lastchar=value.Last();

                if(tokens.GetCount()>=3)  //read the "c2 c1 cc" line
                {
//                        wxString lastchar=value.Last();
//                        if(StringUtil::IsDigital(tokens[1])) //the second maybe is the number or the label
                        if((StringUtil::IsDigital(tokens[1]))||(StringUtil::EndsWith(tokens[1],wxT("."),true)))
                        {
                                value=tokens[1];
                                value.ToLong(&lvalue);
                                zmatrix.bond.atomIndex=(int)lvalue-1;  //our program should begin from 0
                        }
/*                        else if(!last.IsNumber())
                        {
                                value=ehash.find(tokens[1])->first; //this sentence will cause crash
                                value.ToLong(&lvalue);
                                zmatrix.bond.atomIndex=(int)lvalue;
                        }
                        */
                        else if(lastchar.IsNumber()) //the format is C2
                        {
//                                value=ehash.find(tokens[1])->second; //this sentence will cause crash
                                value=ehash.find(tokens[1])->second;//add
                                value.ToLong(&lvalue);
                                zmatrix.bond.atomIndex=(int)lvalue;
//                                wxMessageBox(value);
                        }
                        else  //the format is S, H. so we should find the number in the first row
                        {
                                value=vhash.find(tokens[1])->second; //this sentence will cause crash
                                value.ToLong(&lvalue);
                                zmatrix.bond.atomIndex=(int)lvalue;
                        }



//                        value=ehash.find(tokens[1])->second; //this sentence will cause crash
//                        value.ToLong(&lvalue);
//                        zmatrix.bond.atomIndex=(int)lvalue;
//                        wxMessageBox(value);

//                        if(StringUtil::IsDigital(tokens[2])) //orginal
                        if((StringUtil::IsDigital(tokens[2]))||(StringUtil::EndsWith(tokens[2],wxT("."),true)))
                                value=tokens[2];
                        else
                                value=vhash.find(tokens[2])->second;
                        value.ToDouble(&dvalue);
                        zmatrix.bond.value=dvalue;
//                        wxMessageBox(value);
                        zmatrix.angle.atomIndex = BP_NULL_VALUE;
                        zmatrix.angle.value = BP_NULL_VALUE;
                        zmatrix.dihedral.atomIndex = BP_NULL_VALUE;
                        zmatrix.dihedral.value = BP_NULL_VALUE;

                }
                if(tokens.GetCount()>=5)
                {
//                        if(StringUtil::IsDigital(tokens[3])) //orginal
                        if((StringUtil::IsDigital(tokens[3]))||(StringUtil::EndsWith(tokens[3],wxT("."),true)))

                        {
                                value=tokens[3];
                                value.ToLong(&lvalue);
                                zmatrix.angle.atomIndex=(int)lvalue-1;
                        }
/*                        else  //orginal
                        {
                                value=ehash.find(tokens[3])->second; //this sentence will cause crash
                                value.ToLong(&lvalue);
                                zmatrix.angle.atomIndex=(int)lvalue;
                        }
*/
                        else if(lastchar.IsNumber()) //the format is C2
                        {
//                                value=ehash.find(tokens[1])->second; //this sentence will cause crash
                                value=ehash.find(tokens[3])->second;//add
                                value.ToLong(&lvalue);
                                zmatrix.angle.atomIndex=(int)lvalue;
                        }
                        else  //the format is S, H. so we should find the number in the first row
                        {
                                value=vhash.find(tokens[3])->second; //this sentence will cause crash
                                value.ToLong(&lvalue);
                                zmatrix.angle.atomIndex=(int)lvalue;
                        }


//                        value=ehash.find(tokens[3])->second;
//                        value.ToLong(&lvalue);
//                        zmatrix.angle.atomIndex=(int)lvalue;
//                                                wxMessageBox(value);

//                        if(StringUtil::IsDigital(tokens[4]))  //orgianl
                        if((StringUtil::IsDigital(tokens[4]))||(StringUtil::EndsWith(tokens[4],wxT("."),true)))
                                value=tokens[4];
                        else
                                value=vhash.find(tokens[4])->second;
                        value.ToDouble(&dvalue);
                        zmatrix.angle.value=dvalue;
//                                                wxMessageBox(value);
                        zmatrix.dihedral.atomIndex = BP_NULL_VALUE;
                        zmatrix.dihedral.value = BP_NULL_VALUE;


                }
                if(tokens.GetCount()>=7)
                {

//                        if(StringUtil::IsDigital(tokens[5]))  //orginal
                        if((StringUtil::IsDigital(tokens[5]))||(StringUtil::EndsWith(tokens[5],wxT("."),true)))
                        {
                                value=tokens[5];
                                value.ToLong(&lvalue);
                                zmatrix.dihedral.atomIndex=(int)lvalue-1;
                        }
/*                        else //orginal
                        {
                                value=ehash.find(tokens[5])->second; //this sentence will cause crash
                                value.ToLong(&lvalue);
                                zmatrix.dihedral.atomIndex=(int)lvalue;
                        }
*/
                        else if(lastchar.IsNumber()) //the format is C2
                        {
//                                value=ehash.find(tokens[1])->second; //this sentence will cause crash
                                value=ehash.find(tokens[5])->second;//add
                                value.ToLong(&lvalue);
                                zmatrix.dihedral.atomIndex=(int)lvalue;
                        }
                        else  //the format is S, H. so we should find the number in the first row
                        {
                                value=vhash.find(tokens[5])->second; //this sentence will cause crash
                                value.ToLong(&lvalue);
                                zmatrix.dihedral.atomIndex=(int)lvalue;
                        }



//                        value=ehash.find(tokens[5])->second;
//                        value.ToLong(&lvalue);
//                        zmatrix.dihedral.atomIndex=(int)lvalue;
//                                                wxMessageBox(value);

//                        if(StringUtil::IsDigital(tokens[6]))
                        if((StringUtil::IsDigital(tokens[6]))||(StringUtil::EndsWith(tokens[6],wxT("."),true)))
                                value=tokens[6];
                        else
                                value=vhash.find(tokens[6])->second;
                        value.ToDouble(&dvalue);
                        zmatrix.dihedral.value=dvalue;
//                                                wxMessageBox(value);

                }
                appendZMatrixArray.Add(zmatrix);
                appendAtomBondArray.Add(atomBond);
        }

//                wxMessageBox("finish my code");
                IntToXYZ(appendAtomBondArray,appendZMatrixArray);
//                wxMessageBox("finish Liu code");
        }
//wxMessageBox("hello");
        //create bond type
//  int atomCountBase = appendAtomBondArray.GetCount();        //original atom count before loading this gm file
//  if(atomCountBase < 0) atomCountBase = 0;
int atomCountBase = 0;
  atomNumber=atomNumber-1;
  for(i = atomCountBase; i < atomCountBase + atomNumber; i++)
  {
        eleInfoOption = GetElemInfo((ElemId)appendAtomBondArray[i].atom.elemId);

        appendAtomBondArray[i].atom.SetDispRadius(eleInfoOption->dispRadius);
        appendAtomBondArray[i].atom.label = ++atomLabels[ELEM_PREFIX + appendAtomBondArray[i].atom.elemId];

            for(atom2Index = i + 1; atom2Index < atomCountBase + atomNumber; atom2Index++)
                {
            if(appendAtomBondArray[i].bondCount >= ATOM_MAX_BOND_COUNT) break;
            if(appendAtomBondArray[atom2Index].bondCount >= ATOM_MAX_BOND_COUNT) continue;
            bondType = XYZFile::CalculateBondType(i, atom2Index, appendAtomBondArray);

            if (bondType == BOND_TYPE_0)
                        {
            }
                        else if (bondType == BOND_TYPE_1 || bondType == BOND_TYPE_2 || bondType == BOND_TYPE_3 || bondType == BOND_TYPE_4)
                        {
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].atomIndex = atom2Index;
                appendAtomBondArray[i].bonds[appendAtomBondArray[i].bondCount].bondType = bondType;

                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].atomIndex = i;
                appendAtomBondArray[atom2Index].bonds[appendAtomBondArray[atom2Index].bondCount].bondType = bondType;

                appendAtomBondArray[i].bondCount++;
                appendAtomBondArray[atom2Index].bondCount++;
            }
                        else if (bondType == BOND_TYPE_5)
                        {
                Tools::ShowError(wxT("Until now we have not supported the fourth bond list!"));
            }else
                        {
                Tools::ShowError(wxT("The bond type is wrong!!!"));
            }
        }
    }

}

bool XYZFile::IsQChemXYZFile(const wxString &fileName)
{
        wxString strLine;
        wxArrayString strArr;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
//    long atomId;
//    long atomNumber;

        strLine=tis.ReadLine();
//        while(strLine.Cmp(FILE_QCHEM_BEGIN_COOR)!=0) //read the useless lines before meet the $molecule
        while(!strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
                strLine=tis.ReadLine();
        }
        //wxMessageBox(strLine);
        if(strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
        strLine = tis.ReadLine();       // read 0 1 line
                strLine = tis.ReadLine();
                if(strLine.Contains(wxT("--")))
                        return false;
        strArr=wxStringTokenize(strLine,wxT(" \t\n\r"));
                if(strArr.GetCount()!=4)
                        return false;
/*        atomId = GetElemId(strArr[0]);
                if(atomId == ELEM_NULL)
                        return false;
                if(fis.Eof())
                        return false;*/
                return true;
    }
        return false;
}


bool XYZFile::IsQChemInnerFile(const wxString &fileName)
{
        wxString strLine;
        wxArrayString strArr;
        wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
//    long atomId;
//    long atomNumber;

        strLine=tis.ReadLine();
//        while((strLine=tis.ReadLine()).Cmp(FILE_QCHEM_BEGIN_COOR)!=0) //read the useless lines before meet the $molecule
        while(!strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
                strLine=tis.ReadLine();
        }
        //wxMessageBox(strLine);
        if(strLine.Lower().Contains(FILE_QCHEM_BEGIN_COOR))
        {
        strLine = tis.ReadLine();       // read 0 1 line
                strLine = tis.ReadLine();
                if(strLine.Contains(wxT("--")))
                        return false;
        strArr=wxStringTokenize(strLine,wxT(" \t\n\r"));
                if(strArr.GetCount()!=1)
                        return false;
                if(fis.Eof())
                        return false;
                return true;
    }
        return false;
}
