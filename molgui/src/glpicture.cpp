/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "glpicture.h"

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/filename.h>
#include <stdlib.h>

#include "stringutil.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

GLPicture::GLPicture(wxGLCanvas* canvas)
{
        glCanvas=canvas;
        glContext = new wxGLContext(glCanvas);
        image=NULL;
        imageData=NULL;
}

GLPicture::~GLPicture()
{
        if(imageData)
                free(imageData);
        delete glContext;
}

bool GLPicture::ReadGLPixels()
{
        unsigned char* buffer;
        int width, height;
        int x,y;
        //long pixel;
        int viewport[4];
        if(glCanvas)
        {
                glCanvas->SetCurrent(*glContext);
                //glCanvas->GetClientSize(&width, &height);
                glGetIntegerv(GL_VIEWPORT,viewport);
                width=viewport[2];
                height=viewport[3];
                //width-=width%4;
                //buffer=new GLbyte[width*height*3];
                //buffer=(unsigned char*)malloc(width*height*3*sizeof(unsigned char));
                buffer=(unsigned char*)malloc(4*((width*24+31)/32)*height*sizeof(unsigned char));
                //buffer=(unsigned char*)malloc(3*width*height*sizeof(unsigned char));
                if(!buffer)
                        return false;
                //memset(buffer,0,3*width*height*sizeof(unsigned char));
                memset(buffer,0,4*((width*24+31)/32)*height*sizeof(unsigned char));
                glReadBuffer(GL_BACK);
                //glReadPixels(viewport[0],viewport[1], viewport[2],viewport[3],GL_RGB,GL_UNSIGNED_BYTE,buffer);
                //glReadPixels(0,0,width,height,GL_BGR_EXT,GL_UNSIGNED_BYTE,buffer);
                glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,buffer);
                //WriteBits(wxT("buffer.txt"),buffer,4*((width*24+31)/32)*height);
                imageData=(unsigned char*)malloc(3*width*height*sizeof(unsigned char));
                if(!imageData)
                        return false;
                memset(imageData,0,3*width*height*sizeof(unsigned char));
/*                for (y = height-1; y >= 0; y--)
                {
            for ( x = 0; x < width; x++ )
            {
                pixel = 3*(y*width + x);

                imageData[3*x    ] = buffer[pixel+2];
                imageData[3*x + 1] = buffer[pixel+1];
                imageData[3*x + 2] = buffer[pixel];
            }
        }
*/
                for(x=0;x<width*3;x++)
                {
                        for(y=0;y<height;y++)
                        {
                                imageData[y*width*3+x]=buffer[(height-y-1)*4*((width*24+31)/32)+x];
                        }
                }
                if(image)
                        delete image;
                //image=new wxImage(width,height,buffer);
                //WriteBits(wxT("getdata.txt"),image->GetData(),4*((width*24+31)/32)*height);
                image=new wxImage(width,height,imageData);
                //WriteBits(wxT("getdata.txt"),image->GetData(),3*width*height);
                //image=new wxImage(4*((width*24+31)/32)/3,height,buffer);
                //image=new wxImage(width,height);
                //image->SetData(buffer);
                free(buffer);
                //free(imageData);
                //buffer=image->GetData();
                //wxBitmap bitmap;
                //        bitmap=image->ConvertToBitmap();
                //bitmap.LoadFile(wxT("realscreen.bmp"),wxBITMAP_TYPE_BMP);
                //WriteBits(wxT("savescreen.txt"),bitmap.ConvertToImage().GetData(),3*width*height);
                return true;
        }
        return false;
}

bool GLPicture::SaveFile(const wxString &name, int type,wxRect rect) const
{
        if(!image)
                return false;
        else
        {
                if(rect.width==0 || rect.height==0)
                {
                        return image->SaveFile(name,(wxBitmapType)type);
                }
                else
                {
                        wxBitmap bmp=ConvertToBitmap();
                        int width,height;
                        width=bmp.GetWidth();
                        height=bmp.GetHeight();
                        if(rect.x+rect.width>width)
                                rect.width=width-rect.x;
                        if(rect.x<0)
                        {
                                rect.width+=rect.x;
                                rect.x=0;
                        }
                        if(rect.y+rect.height>height)
                                rect.height=height-rect.y;
                        if(rect.y<0)
                        {
                                rect.height+=rect.y;
                                rect.y=0;
                        }
                        wxBitmap subBmp=bmp.GetSubBitmap(rect);
                        return subBmp.SaveFile(name,(wxBitmapType)type,NULL);
                }
        }
}

bool GLPicture::SaveFile(const wxString &name, const wxString &mimetype) const
{
        if(!image)
                return false;
        else
                return image->SaveFile(name,mimetype);
}

bool GLPicture::SaveFile(const wxString &name) const
{
        if(!image)
                return false;
        else
                return image->SaveFile(name);
}

bool GLPicture::SaveFile(wxOutputStream &stream, int type) const
{
        if(!image)
                return false;
        else
                return image->SaveFile(stream, (wxBitmapType)type);
}

bool GLPicture::SaveFile(wxOutputStream &stream, const wxString &mimetype) const
{
        if(!image)
                return false;
        else
                return image->SaveFile(stream,mimetype);
}

void GLPicture::InitImage()
{
        if(!image)
                ReadGLPixels();
}

void GLPicture::InitImage(wxGLCanvas *canvas)
{
        glCanvas=canvas;
        if(glContext)
            delete glContext;
        glContext = new wxGLContext(glCanvas);
        ReadGLPixels();
}

wxBitmap GLPicture::ConvertToBitmap() const
{
        wxBitmap bitmap;
        wxString tempFileName=wxFileName::CreateTempFileName(wxT("~$"));
        if(image)
        {
                image->SaveFile(tempFileName,wxBITMAP_TYPE_BMP);
        //        bitmap=image->ConvertToBitmap();
                bitmap.LoadFile(tempFileName,wxBITMAP_TYPE_BMP);
                if(wxFileExists(tempFileName)) wxRemoveFile(tempFileName);
        }
        return bitmap;
}

void GLPicture::WriteBits(wxString fileName, unsigned char *data,long total)
{
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
        long i;
        for(i=0;i<total;i++)
                tos<<wxString::Format(wxT("%d"),data[i])<<endl;
}


void GLPicture::SetMask(bool hasMask)
{
        if(image)
                image->SetMask(hasMask);
}

void GLPicture::SetMaskColour(unsigned char red, unsigned char green, unsigned char blue)
{
        if(image)
                image->SetMaskColour(red,green,blue);
}

bool GLPicture::SetMaskFromImage(const wxImage &mask, unsigned char mr, unsigned char mg, unsigned char mb)
{
        if(!image)
                return false;
        else
                return image->SetMaskFromImage(mask,mr,mg,mb);
}

void GLPicture::SetOption(const wxString &name, const wxString &value)
{
        if(image)
                image->SetOption(name,value);
}

void GLPicture::SetOption(const wxString &name, int value)
{
        if(image)
                image->SetOption(name,value);
}

void GLPicture::SetPalette(const wxPalette &palette)
{
        if(image)
                image->SetPalette(palette);
}

void GLPicture::SetRGB(int x, int y, unsigned char red, unsigned char green, unsigned char blue)
{
        if(image)
                image->SetRGB(x,y,red,green,blue);
}

void GLPicture::SetRGB(wxRect &rect, unsigned char red, unsigned char green, unsigned char blue)
{
        if(image)
                image->SetRGB(rect,red,green,blue);
}

wxImage* GLPicture::GetImage()
{
        return image;
}

int GLPicture::GetHeight() const
{
        if(image)
                return image->GetHeight();
        else
                return 0;
}

unsigned char GLPicture::GetMaskBlue() const
{
        if(image)
                return image->GetMaskBlue();
        else
                return 255;
}

unsigned char GLPicture::GetMaskGreen() const
{
        if(image)
                return image->GetMaskGreen();
        else
                return 255;
}

unsigned char GLPicture::GetMaskRed() const
{
        if(image)
                return image->GetMaskRed();
        else
                return 255;
}

bool GLPicture::GetOrFindMaskColour(unsigned char *r, unsigned char *g, unsigned char *b) const
{
        if(image)
                return image->GetOrFindMaskColour(r,g,b);
        else
                return false;
}

//DEL const wxPalette& GLPicture::GetPalette() const
//DEL {
//DEL         if(image)
//DEL                 return image->GetPalette();
//DEL         else
//DEL                 return false;
//DEL
//DEL }

wxString AppendPictureFileType(wxString fileName, int filterIndex) {
        wxString appendedFile = wxEmptyString;
        wxString lowerCaseFile = wxEmptyString;
    if(!StringUtil::IsEmptyString(fileName)) {
        appendedFile = fileName;
                lowerCaseFile = fileName.Lower();
                if(!(lowerCaseFile.EndsWith(wxT(".bmp")) || lowerCaseFile.EndsWith(wxT(".jpg")) || lowerCaseFile.EndsWith(wxT(".xpm"))||lowerCaseFile.EndsWith(wxT(".png")))) {
        switch(filterIndex) {
            case PICTURE_FORMAT_BMP:
                                if(!(lowerCaseFile.EndsWith(wxT(".bmp")))) {
                                    appendedFile += wxT(".bmp");
                }
                break;
            case PICTURE_FORMAT_XPM:
                                if(!(lowerCaseFile.EndsWith(wxT(".xpm")))) {
                                    appendedFile += wxT(".xpm");
                }
                break;
                        case PICTURE_FORMAT_JPEG:
                                if(!(lowerCaseFile.EndsWith(wxT(".jpg")))) {
                                    appendedFile += wxT(".jpg");
                }
                                break;
                        case PICTURE_FORMAT_PNG:
                                if(!(lowerCaseFile.EndsWith(wxT(".png")))) {
                                    appendedFile += wxT(".png");
                }
                                break;
            case PICTURE_FORMAT_ALL:
                                if(!(lowerCaseFile.EndsWith(wxT(".bmp")))) {
                                    appendedFile += wxT(".bmp");
                }
                break;
                        }
                }
    }
    return appendedFile;
}
