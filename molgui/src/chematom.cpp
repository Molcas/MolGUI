/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "chematom.h"
#include "uiprops.h"

#include <wx/arrimpl.cpp>

long ChemAtom::GLOBAL_UNIQUE_ID = 0;

ChemAtom::ChemAtom() {
        elemId = -999;         // index of atom
        label = -999;
        dispRadius = -999.0f;
        pos.x = pos.y = pos.z = 0.0f;
        layer = LAYER_HIGH;
        connGroup = BP_NULL_VALUE;
        ResetUniqueId();
}

void ChemAtom::ResetUniqueId(void) {
    uniqueId = ++GLOBAL_UNIQUE_ID;
}

bool ChemAtom::IsDefaultHydrogen() const {
    return elemId == DEFAULT_HYDROGEN_ID;
}

bool ChemAtom::IsDummy(void) const {
    return elemId == DUMMY_ATOM_ID;
}

float ChemAtom::GetDispRadius(ModelType modelType) const {
        float dr = dispRadius;
        if(MODEL_SPACEFILLING == modelType) {
                //dr += 0.85f;
                dr = GetElemInfo((ElemId)elemId)->vanderRadius;
        }else if(MODEL_BALLWIRE == modelType) {
                dr /= 2.0f;
        }else if(MODEL_TUBE == modelType || MODEL_WIRE == modelType) {
        dr = GetUIProps()->openGLProps.bondRadius;
    }

        //if(selected) {
        //        dr += 0.05f;
        //}
        return dr;
}

float ChemAtom::GetOriginalDispRadius(void) const {
    return dispRadius;
}

float ChemAtom::GetSelectRadius(ModelType modelType) const {
        return GetDispRadius(modelType) + 0.2f;
}

void ChemAtom::SetDispRadius(float dr){
        dispRadius = dr;
}

void ChemAtom::Clone(ChemAtom atom) {
    elemId = atom.elemId;
    //label = -999;
    SetDispRadius(atom.GetOriginalDispRadius());
    pos.x = atom.pos.x;
    pos.y = atom.pos.y;
    pos.z = atom.pos.z;
    layer = atom.layer;
    //connGroup = BP_NULL_VALUE;
}

void ChemAtom::Translate(ChemAtom& atom, float transX, float transY, float transZ) {
        atom.pos.x += transX;
        atom.pos.y += transY;
        atom.pos.z += transZ;
}

void ChemAtom::Translate(ChemAtom& atom, AtomPosition3F posTrans) {
        atom.pos.x += posTrans.x;
        atom.pos.y += posTrans.y;
        atom.pos.z += posTrans.z;
}

void ChemAtom::Translate(AtomPosition3F& atomPos, AtomPosition3F posTrans) {
        atomPos.x += posTrans.x;
        atomPos.y += posTrans.y;
        atomPos.z += posTrans.z;
}

void ChemAtom::Translate(AtomPosition3F& atomPos, float transX, float transY, float transZ) {
        atomPos.x += transX;
        atomPos.y += transY;
        atomPos.z += transZ;
}

void ChemAtom::RotateWithX(ChemAtom& atom, float angle) {
        float tempY, tempZ;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempY = atom.pos.y * cosAlpha - atom.pos.z * sinAlpha;
        tempZ = atom.pos.y * sinAlpha + atom.pos.z * cosAlpha;

        atom.pos.y = tempY;
        atom.pos.z = tempZ;
}

void ChemAtom::RotateWithX(AtomPosition& atomPos, float angle) {
        float tempY, tempZ;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempY = atomPos.y * cosAlpha - atomPos.z * sinAlpha;
        tempZ = atomPos.y * sinAlpha + atomPos.z * cosAlpha;

        atomPos.y = tempY;
        atomPos.z = tempZ;
}

void ChemAtom::RotateWithX(AtomPosition3F& atomPos, float angle) {
        float tempY, tempZ;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempY = atomPos.y * cosAlpha - atomPos.z * sinAlpha;
        tempZ = atomPos.y * sinAlpha + atomPos.z * cosAlpha;

        atomPos.y = tempY;
        atomPos.z = tempZ;
}

void ChemAtom::RotateWithY(ChemAtom& atom, float angle) {
        float tempX, tempZ;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempX = atom.pos.x * cosAlpha + atom.pos.z * sinAlpha;
        tempZ = atom.pos.x * (-sinAlpha) + atom.pos.z * cosAlpha;

        atom.pos.x = tempX;
        atom.pos.z = tempZ;
}

void ChemAtom::RotateWithY(AtomPosition& atomPos, float angle) {
        float tempX, tempZ;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempX = atomPos.x * cosAlpha + atomPos.z * sinAlpha;
        tempZ = atomPos.x * (-sinAlpha) + atomPos.z * cosAlpha;

        atomPos.x = tempX;
        atomPos.z = tempZ;
}

void ChemAtom::RotateWithY(AtomPosition3F& atomPos, float angle) {
        float tempX, tempZ;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempX = atomPos.x * cosAlpha + atomPos.z * sinAlpha;
        tempZ = atomPos.x * (-sinAlpha) + atomPos.z * cosAlpha;

        atomPos.x = tempX;
        atomPos.z = tempZ;
}

void ChemAtom::RotateWithZ(ChemAtom& atom, float angle) {
        float tempX, tempY;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempX = atom.pos.x * cosAlpha - atom.pos.y * sinAlpha;
        tempY = atom.pos.x * sinAlpha + atom.pos.y * cosAlpha;

        atom.pos.x = tempX;
        atom.pos.y = tempY;
}

void ChemAtom::RotateWithZ(AtomPosition& atomPos, float angle) {
        float tempX, tempY;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempX = atomPos.x * cosAlpha - atomPos.x * sinAlpha;
        tempY = atomPos.y * sinAlpha + atomPos.y * cosAlpha;

        atomPos.x = tempX;
        atomPos.y = tempY;
}

void ChemAtom::RotateWithZ(AtomPosition3F& atomPos, float angle) {
        float tempX, tempY;
        float cosAlpha = cos(angle * RADIAN);
        float sinAlpha = sin(angle * RADIAN);

        tempX = atomPos.x * cosAlpha - atomPos.x * sinAlpha;
        tempY = atomPos.y * sinAlpha + atomPos.y * cosAlpha;

        atomPos.x = tempX;
        atomPos.y = tempY;
}

ChemBond::ChemBond() {
        Clear();
}

void ChemBond::Clear(void) {
        atomIndex = BP_NULL_VALUE;
        bondType = BP_NULL_VALUE;
        dispType = BP_NULL_VALUE;
}

void ChemBond::Clone(ChemBond chemBond) {
    atomIndex = chemBond.atomIndex;
        bondType = chemBond.bondType;
        dispType = chemBond.dispType;
}

bool ChemBond::ChangeBondTypeToNext(void) {
        bool isError = false;
        switch(bondType) {
                case BOND_TYPE_1:
                        bondType = BOND_TYPE_2;
                        break;
                case BOND_TYPE_2:
                        bondType = BOND_TYPE_3;
                        break;
                case BOND_TYPE_3:
                        bondType = BOND_TYPE_5;
                        break;
                case BOND_TYPE_4:
                        bondType = BOND_TYPE_2;
                        break;
                case BOND_TYPE_5:
                        isError = true;
                        break;
                default:
                        bondType = BOND_TYPE_1;

        }
        return isError;
}

AtomBond::AtomBond() {
    for(int i = 0; i < ATOM_MAX_BOND_COUNT; i++) {
        bonds[i] = ChemBond();
    }
    bondCount = 0;
}

void AtomBond::Clone(AtomBond atomBond) {
    bondCount = atomBond.bondCount;
    atom.Clone(atomBond.atom);
    for(int i = 0; i < bondCount; i++) {
        bonds[i].Clone(atomBond.bonds[i]);
    }
}

void AtomBond::RemoveBond(int bondIndex) {
        int i = 0;
        if(bondIndex < bondCount) {
                for(i = bondIndex; i < bondCount - 1; i++) {
                        bonds[i].Clone(bonds[i + 1]);
                }
                bonds[i].Clear();
                bondCount--;
        }
}

AtomAngle::AtomAngle() {
    atomStillUniqueId = BP_NULL_VALUE;
    atomAxisUniqueId = BP_NULL_VALUE;
    atomMoveUniqueId = BP_NULL_VALUE;
    angle = 0;
}

ChemLigand::ChemLigand(){
        dummyAtomId = BP_NULL_VALUE;
}

wxString ChemLigand::ToString(void)        {
        wxString str = wxString::Format(wxT("%ld"), dummyAtomId);
        for(unsigned i = 0; i < linkedAtomIds.GetCount(); i++)        {
                str += wxString::Format(wxT(" %ld"), linkedAtomIds[i]);
        }
        return str;
}


WX_DEFINE_OBJARRAY(AtomPositionArray);
WX_DEFINE_OBJARRAY(AtomPosition3FArray);
WX_DEFINE_OBJARRAY(AtomBondArray);
WX_DEFINE_OBJARRAY(AtomAngleArray);
WX_DEFINE_OBJARRAY(ChemLigandArray);
