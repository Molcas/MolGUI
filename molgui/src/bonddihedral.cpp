/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "measure.h"
#include "measureutils.h"
#include "atommanuutils.h"
#include "renderingdata.h"


/********************************************************************
 *to judge wether three atoms is lining
 *the atom is in order of:4 3 (r4) 2 (a4) 1 (d4)
 *here the -1.0f indicates that the oldAngle is null, we should
 *calculate its value
 *the bond angle can not be 180.0 or 0.0!
 ********************************************************************/
MeasureInfo JudgeDihedral(int atom1Index, int atom2Index, int atom3Index, int atom4Index, const AtomBondArray& atomBondArray)
{

  //  printf ("the bond angle between 1 2 3 is: %f\n", MeasureBondAngle(-1.0f, atom1Index,atom2Index,atom3Index,atomBondArray));

  //  printf ("the bond angle between 2 3 4 is: %f\n", MeasureBondAngle(-1.0f, atom2Index,atom3Index,atom4Index,atomBondArray));

  double angle;


  angle = MeasureBondAngle(-1.0, atom1Index,atom2Index,atom3Index,atomBondArray);
  if ( fabs(180.0 - angle) < MEASURE_PREC )  {
    return DIHEDRAL_CAN_NOT_DEFINE;
  } else if ( fabs(0.0 -  angle)< MEASURE_PREC ) {
    return DIHEDRAL_CAN_NOT_DEFINE;
  }


  angle = MeasureBondAngle(-1.0, atom2Index,atom3Index,atom4Index,atomBondArray);
  if ( fabs(180.0 - angle)< MEASURE_PREC ) {
    return DIHEDRAL_CAN_NOT_DEFINE;
  } else if ( fabs(0.0 -  angle)< MEASURE_PREC ) {
    return DIHEDRAL_CAN_NOT_DEFINE;
  }

  return MEASURE_SUCCESS;
  /* if we calculate the dihedral, there must not has three atoms lining together  */
}




MeasureInfo MeasureDihedral(double & dihedral, int atom1Index, int atom2Index, int atom3Index, int atom4Index, const AtomBondArray& atomBondArray)
{
  double vec123_x = (atomBondArray[atom2Index].atom.pos.y-atomBondArray[atom1Index].atom.pos.y)
    *(atomBondArray[atom3Index].atom.pos.z-atomBondArray[atom2Index].atom.pos.z) -
    (atomBondArray[atom2Index].atom.pos.z-atomBondArray[atom1Index].atom.pos.z)
    *(atomBondArray[atom3Index].atom.pos.y-atomBondArray[atom2Index].atom.pos.y);

  double vec123_y =  (atomBondArray[atom2Index].atom.pos.z-atomBondArray[atom1Index].atom.pos.z) *(atomBondArray[atom3Index].atom.pos.x-atomBondArray[atom2Index].atom.pos.x) - (atomBondArray[atom2Index].atom.pos.x-atomBondArray[atom1Index].atom.pos.x)*(atomBondArray[atom3Index].atom.pos.z-atomBondArray[atom2Index].atom.pos.z);

  double vec123_z =  (atomBondArray[atom2Index].atom.pos.x-atomBondArray[atom1Index].atom.pos.x)
    *(atomBondArray[atom3Index].atom.pos.y-atomBondArray[atom2Index].atom.pos.y) -
    (atomBondArray[atom2Index].atom.pos.y-atomBondArray[atom1Index].atom.pos.y)
    *(atomBondArray[atom3Index].atom.pos.x-atomBondArray[atom2Index].atom.pos.x);
  /* the plane formed is define by vec 21 and vec 32 */

  double cosalfa = 0;
  double sinalfa = 0;                /* alfa is formed by vec34 and vec32 */

  double cosbeta = 0;
  double sinbeta = 0;                /* beta is formed by vec32 and vec31 */

  double costheta = 0;                /* theta is formed by vec34 and  vec 31*/

  double cos_dihedral = 0;        /* now here the cos_dihedral can not be eliminated */

  MeasureInfo returnInfo;

  returnInfo = JudgeDihedral(atom1Index,atom2Index,atom3Index,atom4Index,atomBondArray);
  if (returnInfo != MEASURE_SUCCESS)
    return DIHEDRAL_CAN_NOT_DEFINE;


  cosalfa = cos (MeasureBondAngle(-1.0, atom2Index,atom3Index,atom4Index,atomBondArray)*RADIAN);
  sinalfa = sqrt (1 - cosalfa*cosalfa);

  cosbeta = cos (MeasureBondAngle(-1.0, atom1Index,atom3Index,atom2Index,atomBondArray)*RADIAN);
  sinbeta = sqrt (1 - cosbeta*cosbeta);

  costheta =cos (MeasureBondAngle(-1.0, atom1Index,atom3Index,atom4Index,atomBondArray)*RADIAN);

  cos_dihedral = (costheta - cosalfa*cosbeta)/(sinalfa*sinbeta);

  /* for some calculation, the cos_dihedral may be larger than 1. so we make some change */
  if (fabs(cos_dihedral) > 1)
    {
      if (cos_dihedral < 0)
        cos_dihedral = -1;
      else
        cos_dihedral = 1;
    }


  dihedral = acos(cos_dihedral)/RADIAN;

  if (((atomBondArray[atom4Index].atom.pos.x - atomBondArray[atom3Index].atom.pos.x)*vec123_x
       + (atomBondArray[atom4Index].atom.pos.y - atomBondArray[atom3Index].atom.pos.y)*vec123_y
       + (atomBondArray[atom4Index].atom.pos.z - atomBondArray[atom3Index].atom.pos.z)*vec123_z ) >= 0 )
    {
      dihedral = dihedral*1;
    }
  else
    {
      dihedral = dihedral*(-1);
    }
  return MEASURE_SUCCESS;

}



/******************************************************
 to calculate the coordinate made by dihedral change
*******************************************************/

MeasureInfo ChangeDihedral(double dihedral, int atomStill, int atomAxis1, int atomAxis2, int atomMove, AtomBondArray& atomBondArray, bool methodSwitch)
{

  int i;

  //all of the moving atom's information
  //is here. including the position of label. see the common.h
  MovingAtoms * movingAtoms = NULL;
  int movingAtomArrayLength = 0;

  MeasureInfo  returnInfo;  /* return information */
  ChemBond bondArrayStoreOfAtom[ATOM_MAX_BOND_COUNT];
  //the first axis atom's information is deleted



  // decide how to change the corrdinate
  // if methodSwitch is true, the corrdinate change only done to 2,3,4 atoms
  if (methodSwitch) {
    ChangeDihedralCalculation (dihedral, atomStill, atomAxis1, atomAxis2, atomMove, movingAtoms, movingAtomArrayLength, atomBondArray);
    return   MEASURE_SUCCESS;
  }


  // if there are three atoms lining together, the dihedral can not change!
  returnInfo = JudgeDihedral(atomStill,atomAxis1,atomAxis2,atomMove,atomBondArray);
  if (returnInfo != MEASURE_SUCCESS)
    return DIHEDRAL_CAN_NOT_DEFINE;



  /* it requests that there should not have bond linking atom1 and atom4  */
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {
      if ( atomBondArray[atomStill].bonds[i].atomIndex== atomMove)
        {
          return MEASURE_ERROR_IN_DIHEDRAL;
        }
    }


  /* when the atom2 and atom4 is bonding,  dihedral can not changed */
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {

      if ( atomBondArray[atomAxis1].bonds[i].atomIndex== atomMove)
        {
          return MEASURE_ERROR_IN_DIHEDRAL;
        }
      else
        {
          continue;
        }
    }



  // copy the axis atom's bond information
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {
      bondArrayStoreOfAtom[i] = atomBondArray[atomAxis1].bonds[i];
    }


  //  RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata0.dat"));



  // delete all the information related to the atomAxis1
  for (i=0; i<atomBondArray[atomAxis1].bondCount; i++)
    {
      atomBondArray[atomAxis1].bonds[i].atomIndex = BP_NULL_VALUE;
    }


  // RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata1.dat"));





  /* search the connection*/
  movingAtoms = SearchConnectedAtoms(atomBondArray, atomMove, &movingAtomArrayLength);



 // finally restore the information of the delete atom
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {
      atomBondArray[atomAxis1].bonds[i] = bondArrayStoreOfAtom[i];
    }


  if ( movingAtoms == NULL)
    {
      return MEASURE_ERROR_IN_MEMORY_ALLOCATION;
    }




  //RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata2.dat"));


  /* if atom1 and atom4 is bonding into a circle, the dihedral can not change! */
  for (i=0; i<movingAtomArrayLength; i++)
    {
      if ( movingAtoms[i].atomLabel == atomStill)
        {

          // finally free the space used by the movingAtom
          if (movingAtoms != NULL) {
            free (movingAtoms);
            movingAtoms = NULL;
          }

          return MEASURE_ERROR_IN_DIHEDRAL;
        }
    }



  /* calculate the change of coordinate */
  ChangeDihedralCalculation(dihedral,atomStill, atomAxis1, atomAxis2, atomMove,  movingAtoms, movingAtomArrayLength, atomBondArray);


  // finally free the space used by the movingAtom
  if (movingAtoms != NULL) {
    free (movingAtoms);
    movingAtoms = NULL;
  }


  return   MEASURE_SUCCESS;

}



/******************************************************************
really calculate the coordinate change made by the dihedral change
*******************************************************************/

void ChangeDihedralCalculation(double dihedral,int atomStill, int atomAxis1, int atomAxis2, int atomMove,  MovingAtoms * movingAtoms, int movingAtomArrayLength, AtomBondArray& atomBondArray)
{

  double angleZ  = 0;
  double cosgama = 0;
  int labelZ = 0;

  double angleY  = 0;
  double cosbeta = 0;
  int labelY = 0;

  int i;
  double x2;        /* the coordinate of the atomAxis1 */
  double y2;
  double z2;
  double x3;        /* the coordinate of the atomAxis2 */
  double y3;
  double z3;
  double x4;    /* the coordinate of the atomMove  */
  double y4;
  double z4;

  double dihedralOld;                // the old dihedral value
  double dihedralR;                // the value change between old and new


  /* initialize the coordinate */
  x2 = atomBondArray[atomAxis1].atom.pos.x;
  y2 = atomBondArray[atomAxis1].atom.pos.y;
  z2 = atomBondArray[atomAxis1].atom.pos.z;
  x3 = atomBondArray[atomAxis2].atom.pos.x;
  y3 = atomBondArray[atomAxis2].atom.pos.y;
  z3 = atomBondArray[atomAxis2].atom.pos.z;
  x4 = atomBondArray[atomMove].atom.pos.x;
  y4 = atomBondArray[atomMove].atom.pos.y;
  z4 = atomBondArray[atomMove].atom.pos.z;


  /*  move the atomAxis1 to the origin */
    x4 -= x2;
    y4 -= y2;
    z4 -= z2;
    x3 -= x2;
    y3 -= y2;
    z3 -= z2;
  if (movingAtomArrayLength != 0 && movingAtoms != NULL) {
    for (i=0; i< movingAtomArrayLength; i++) {
      movingAtoms[i].x -= x2;
      movingAtoms[i].y -= y2;
      movingAtoms[i].z -= z2;
    }
  }



  /* rotate around the z; move vector between stomAxis1 and atomAxis2
     to the xz plane*/
  if(fabs(x3) > MEASURE_PREC || fabs(y3) > MEASURE_PREC) {
    cosgama = x3/sqrt(x3*x3 + y3*y3);
    if (y3<0) //counter-clockwise
      angleZ =  acos(cosgama);
    else      //clockwise
      angleZ = -acos(cosgama);
    RotationByZ (&x3, &y3, angleZ);

    if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
      RotationByZ (&x4, &y4, angleZ);
    }
    else {
      GroupRotationByZ (movingAtoms,movingAtomArrayLength, angleZ);
    }
  }
  else
    {
      labelZ = 1;
    }



  /* rotate around the y; move vector between stomAxis1 and atomAxis2
     to the z axis*/
  if(fabs(x3) > MEASURE_PREC || fabs(z3) > MEASURE_PREC) {
    cosbeta = z3/sqrt (x3*x3 + z3*z3);
    if (x3<0) //counter-clockwise
      angleY =  acos(cosbeta);
    else      //clockwise
      angleY = -acos(cosbeta);
    RotationByY (&x3, &z3, angleY);

    if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
      RotationByY (&x4, &z4, angleY);
    }
    else {
      GroupRotationByY (movingAtoms,movingAtomArrayLength, angleY);
    }
  }
  else
    {
      labelY = 1;
    }



  /* calculate the dihedral change, around the z axis rotate  */
  MeasureDihedral(dihedralOld,atomStill,atomAxis1,atomAxis2,atomMove, atomBondArray);


  dihedralR = (dihedral - dihedralOld)*RADIAN;
  if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
    RotationByZ (&x4, &y4, dihedralR);
  }
  else {
    GroupRotationByZ (movingAtoms,movingAtomArrayLength, dihedralR);
  }




  /* restore the rotation around the y*/
  if (labelY != 1) {
    angleY = (-angleY);
    if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
      RotationByY (&x4, &z4, angleY);
    }
    else {
      GroupRotationByY (movingAtoms,movingAtomArrayLength, angleY);
    }
  }



  /* restore the rotation around the Z*/
  if (labelZ != 1) {
    angleZ = (-angleZ);
    if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
      RotationByZ (&x4, &y4, angleZ);
    }
    else {
      GroupRotationByZ (movingAtoms,movingAtomArrayLength, angleZ);
    }
  }



  /*  move the atomAxis1 back */
  if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
    x4 += x2;
    y4 += y2;
    z4 += z2;
  }
  else {
    for (i=0; i< movingAtomArrayLength; i++) {
      movingAtoms[i].x += x2;
      movingAtoms[i].y += y2;
      movingAtoms[i].z += z2;
    }
  }


  //value to write back
  if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
    atomBondArray[atomMove].atom.pos.x = x4;
    atomBondArray[atomMove].atom.pos.y = y4;
    atomBondArray[atomMove].atom.pos.z = z4;
  }
  else {
    for (i=0; i< movingAtomArrayLength; i++) {
      atomBondArray[movingAtoms[i].atomLabel].atom.pos.x = movingAtoms[i].x;
      atomBondArray[movingAtoms[i].atomLabel].atom.pos.y = movingAtoms[i].y;
      atomBondArray[movingAtoms[i].atomLabel].atom.pos.z = movingAtoms[i].z;
    }
  }


}
