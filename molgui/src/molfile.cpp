/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>
#include <wx/filename.h>

#include "molfile.h"
#include "commons.h"

#include "tools.h"
#include "stringutil.h"
#include "fileutil.h"

const wxString FILE_BEGIN_COOR = wxT("$BEGIN COOR");
const wxString FILE_END_COOR = wxT("$END COOR");
const wxString FILE_BEGIN_BOND = wxT("$BEGIN BOND");
const wxString FILE_END_BOND = wxT("$END BOND");
const wxString FILE_BEGIN_TYPE = wxT("$BEGIN TYPE");
const wxString FILE_END_TYPE = wxT("$END TYPE");
const wxString FILE_BEGIN_CONSTRAIN = wxT("$BEGIN CONSTRAINTS");
const wxString FILE_END_CONSTRAIN = wxT("$END CONSTRAINTS");
const wxString FILE_CONSTRAIN_POSITION_FLAG = wxT("P");
const wxString FILE_CONSTRAIN_LENGTH_FLAG = wxT("L");
const wxString FILE_CONSTRAIN_ANGLE_FLAG = wxT("A");
const wxString FILE_CONSTRAIN_DIHEDRAL_FLAG = wxT("D");
const wxString FILE_BEGIN_LIGAND = wxT("$BEGIN LIGAND");
const wxString FILE_END_LIGAND = wxT("$END LIGAND");

bool MolFile::IsMolFile(const wxString& fileName) {
    return true;
}

void MolFile::CreateTemplateFile(const wxString& fileName, AtomBondArray& bondArray) {
        unsigned int i, total;
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);

    total = bondArray.GetCount(); //Avoid a format specifer assert failure
    tos << wxString::Format(wxT("%d"),total) << endl;
    int atomId;
    for(i = 0; i < bondArray.GetCount(); i++) {
        atomId = bondArray[i].atom.elemId ;
        tos << wxString::Format(wxT("%d\t%.8f\t%.8f\t%.8f\n"), atomId, bondArray[i].atom.pos.x,
            bondArray[i].atom.pos.y, bondArray[i].atom.pos.z);
    }
    for(i = 0, total = 0; i < bondArray.GetCount(); i++)  {
                total += bondArray[i].bondCount;
        }
        total /= 2;
    tos << wxString::Format(wxT("%d"),total) << endl;
    for(i = 0; i < bondArray.GetCount(); i++) {
           for(int j = 0; j < bondArray[i].bondCount; j++) {
                        if(bondArray[i].bonds[j].atomIndex > (int)i) {
                tos << wxString::Format(wxT("%d\t%d\t%d\n"), i + 1, bondArray[i].bonds[j].atomIndex + 1, bondArray[i].bonds[j].bondType);
            }
           }
    }
}

void MolFile::LoadFile(const wxString &strLoadedFileName, RenderingData* pData) {
        int i;
    wxString strLine, value;
    ElemInfo* eleInfoOption = NULL;
        if(pData == NULL){
                wxMessageBox(_("pData Null in molfile::loadfile"));
                return;
        }
        wxFileInputStream fis(strLoadedFileName);
        wxTextInputStream tis(fis);
        AtomBondArray& appendAtomBondArray = pData->GetAtomBondArray();
        int* atomLabels = pData->GetAtomLabels();
        ConstrainData& constrainData = pData->GetConstrainData();
        wxArrayString tokens;

        int atomCountBase = appendAtomBondArray.GetCount();        //original atom count before loading this gm file
        if(atomCountBase < 0) atomCountBase = 0;
        strLine = tis.ReadLine();

        if(FILE_BEGIN_COOR.Cmp(strLine.Left(FILE_BEGIN_COOR.Len())) == 0)
        {
        //load atom info
        long atomId;
        double x, y, z;
        strLine = tis.ReadLine();
        while( FILE_END_COOR.Cmp(strLine.Left(FILE_END_COOR.Len())) != 0)
                {
            wxStringTokenizer tzk(strLine);
            AtomBond atomBond = AtomBond();
            tzk.GetNextToken().ToLong(&atomId);
            tzk.GetNextToken().ToDouble(&x);
            tzk.GetNextToken().ToDouble(&y);
            tzk.GetNextToken().ToDouble(&z);
            atomBond.atom.elemId = (int)atomId;
            atomBond.atom.pos.x = x;
            atomBond.atom.pos.y = y;
            atomBond.atom.pos.z = z;
            appendAtomBondArray.Add(atomBond);
            strLine = tis.ReadLine();
        }
                strLine = tis.ReadLine();
    }

    if(FILE_BEGIN_BOND.Cmp(strLine.Left(FILE_BEGIN_BOND.Len())) == 0) {
        //load bond info
        int atom1, atom2;
        short bondType;
        long atom1Long, atom2Long, bondTypeLong;
        strLine = tis.ReadLine();
        while(FILE_END_BOND.Cmp(strLine.Left(FILE_END_BOND.Len())) != 0) {
            wxStringTokenizer tzk(strLine);
            tzk.GetNextToken().ToLong(&atom1Long);
            tzk.GetNextToken().ToLong(&atom2Long);
            tzk.GetNextToken().ToLong(&bondTypeLong);
            atom1 = (int)atom1Long;
            atom2 = (int)atom2Long;
            bondType = (short)bondTypeLong;

            atom1--;
            atom2--;
            atom1 += atomCountBase;
            atom2 += atomCountBase;

            appendAtomBondArray[atom1].bonds[appendAtomBondArray[atom1].bondCount].atomIndex = atom2;
            appendAtomBondArray[atom1].bonds[appendAtomBondArray[atom1].bondCount].bondType = bondType;

            appendAtomBondArray[atom2].bonds[appendAtomBondArray[atom2].bondCount].atomIndex = atom1;
            appendAtomBondArray[atom2].bonds[appendAtomBondArray[atom2].bondCount].bondType = bondType;

            appendAtomBondArray[atom1].bondCount++;
            appendAtomBondArray[atom2].bondCount++;
            strLine = tis.ReadLine();
        }
                strLine = tis.ReadLine();
    }

    if(FILE_BEGIN_TYPE.Cmp(strLine.Left(FILE_BEGIN_TYPE.Len())) == 0) {
        int type;
        long typeLong;
        i = atomCountBase;
        strLine = tis.ReadLine();
        while(FILE_END_TYPE.Cmp(strLine.Left(FILE_END_TYPE.Len())) != 0) {
            strLine.ToLong(&typeLong);
            type = (int)typeLong;
            if( type == DEFAULT_HYDROGEN_ID) {
                appendAtomBondArray[i].atom.elemId = type;
            }
            eleInfoOption = GetElemInfo((ElemId)appendAtomBondArray[i].atom.elemId);
            appendAtomBondArray[i].atom.SetDispRadius(eleInfoOption->dispRadius);
                        appendAtomBondArray[i].atom.label = ++atomLabels[ELEM_PREFIX + appendAtomBondArray[i].atom.elemId];
                        i++;
                        strLine = tis.ReadLine();
        }
                strLine = tis.ReadLine();
    }
        ConsElem ce;
        long indexLong;
        double valueDouble;
        wxString consFlags[3]={        FILE_CONSTRAIN_LENGTH_FLAG,
                                                                                        FILE_CONSTRAIN_ANGLE_FLAG,
                                                                                        FILE_CONSTRAIN_DIHEDRAL_FLAG};
        ConsType consTypes[3]={CONS_BOND,CONS_ANGLE,CONS_DIHEDRAL};
        ConsType consType=CONS_NULL;
        if(FILE_BEGIN_CONSTRAIN.Cmp(strLine.Left(FILE_BEGIN_CONSTRAIN.Len()))==0)
        {
            strLine=tis.ReadLine();
                while(FILE_END_CONSTRAIN.Cmp(strLine.Left(FILE_END_CONSTRAIN.Len()))!=0)        {
                        //wxMessageBox(strLine);
                        tokens=wxStringTokenize(strLine,wxT(" \t\r\n"),wxTOKEN_STRTOK);
                        //DisplayArrayString(tokens);
                        //wxMessageBox(tokens[0]);
                        consType=CONS_NULL;
                        for(i=0; i <3 ;i ++)
                        {
                                if(consFlags[i].Cmp(tokens[0])==0)
                                {
                                        consType=consTypes[i];
                                        break;
                                }
                        }
                        if(consType==CONS_NULL){
                            strLine=tis.ReadLine();
                                continue;
                        }
            if((int)tokens.GetCount()<consType+2){
                strLine=tis.ReadLine();
                                continue;
            }
                        ce=ConsElem();
                        for(i=0;i<consType;i++)
                        {
                                //wxMessageBox(tokens[i+1]);
                                tokens[i+1].ToLong(&indexLong);
                                indexLong--;
                                ce.elemInfos.Add(appendAtomBondArray[indexLong].atom.uniqueId);
                        }
                        //wxMessageBox(tokens[tokens.GetCount()-1]);
                        tokens[tokens.GetCount()-1].ToDouble(&valueDouble);
                        ce.value=valueDouble;
                        constrainData.AppendConstrainData(ce);
                        strLine=tis.ReadLine();
                }
                strLine = tis.ReadLine();
        }
}

void MolFile::SaveFile(const wxString &fileName, RenderingData* pData, bool useTab) {
        if(pData == NULL || pData->GetAtomCount()<=0)
                return;
    wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
    SaveFile(tos, pData, useTab);
}

void MolFile::SaveFile(wxTextOutputStream &tos, RenderingData* pData, bool useTab) {
    if(pData == NULL || pData->GetAtomCount()<=0)
                return;
        AtomBondArray &chemArray = pData->GetAtomBondArray();
        ChemLigandArray& ligandArray = pData->GetLigandArray();

        SaveAtomBondArray(tos, chemArray, useTab);
        SaveLigandInfo(tos,chemArray,ligandArray,useTab);
        SaveConstrain(tos,chemArray, pData->GetConstrainData(), useTab);
}

void MolFile::SaveAtomBondArray(const wxString& fileName, AtomBondArray &chemArray, bool useTab) {
        wxFileOutputStream fos(fileName);
    wxTextOutputStream tos(fos);
    SaveAtomBondArray(tos, chemArray, useTab);
}

void MolFile::SaveAtomBondArray(wxTextOutputStream &tos,AtomBondArray &chemArray, bool useTab) {
        unsigned i;
    int elemId;
        wxString format;
    tos << FILE_BEGIN_COOR << endl;
    for(i = 0; i < chemArray.GetCount(); i++) {
        if(chemArray[i].atom.elemId == DEFAULT_HYDROGEN_ID) {
            elemId = (int)ELEM_H;
        }else{
            elemId = chemArray[i].atom.elemId ;
        }
        if(useTab)
                format = wxT("\t%12d\t%12.4f\t%12.4f\t%12.4f\n");
        else
                format = wxT(" %12d %12.4f %12.4f %12.4f\n");
        tos << wxString::Format(format, elemId, chemArray[i].atom.pos.x,
            chemArray[i].atom.pos.y, chemArray[i].atom.pos.z);
    }
    tos << FILE_END_COOR << endl;

    tos << FILE_BEGIN_BOND << endl;
    for(i = 0; i < chemArray.GetCount(); i++) {
           for(int j = 0; j < chemArray[i].bondCount; j++) {
            //if(BP_NULL_VALUE == bondArray[i].bonds[j].atomIndex) {
                //break;
            //}
                        if(useTab)
                        format = wxT("\t%12d\t%12d\t%12d\n");
                else
                        format = wxT(" %12d %12d %12d\n");
            if(chemArray[i].bonds[j].atomIndex > (int)i) {
                tos << wxString::Format(format, i + 1, chemArray[i].bonds[j].atomIndex + 1, chemArray[i].bonds[j].bondType);
            }
           }
    }
    tos << FILE_END_BOND << endl;

    tos << FILE_BEGIN_TYPE << endl;
    int type = 1;
    for(i = 0; i < chemArray.GetCount(); i++) {
        type = 1;
        if (chemArray[i].atom.elemId == DEFAULT_HYDROGEN_ID) {
            type = -1;
        }
                if(useTab)
                format = wxT("\t%12d\n");
        else
                format = wxT(" %12d\n");
        tos << wxString::Format(format, type);
    }
    tos << FILE_END_TYPE << endl;
}

void MolFile::SaveConstrain(wxTextOutputStream &tos, AtomBondArray &chemArray, ConstrainData &constrainData, bool useTab) {
        unsigned i;
        tos << FILE_BEGIN_CONSTRAIN <<endl;
        wxArrayString lenArray = constrainData.GetConstrainLengthArray(chemArray, useTab, false);
        wxArrayString angArray = constrainData.GetConstrainAngleArray(chemArray, useTab, false);
        wxArrayString dihArray = constrainData.GetConstrainDihedralArray(chemArray, useTab, false);
        for(i = 0; i < lenArray.GetCount(); i++) {
                tos << wxT("    ")
                        << FILE_CONSTRAIN_LENGTH_FLAG
                        << lenArray[i]
                        << endl;
        }
        for(i = 0; i < angArray.GetCount(); i++) {
                tos << wxT("    ")
                        << FILE_CONSTRAIN_ANGLE_FLAG
                        << angArray[i]
                        << endl;
        }
        for(i = 0; i < dihArray.GetCount(); i++) {
                tos << wxT("    ")
                        << FILE_CONSTRAIN_DIHEDRAL_FLAG
                        << dihArray[i]
                        << endl;
        }
        tos << FILE_END_CONSTRAIN << endl;
}

void MolFile::SaveLigandInfo(wxTextOutputStream &tos, AtomBondArray &chemArray, ChemLigandArray &ligandArray, bool useTab)
{
        LngLHash indexHash;
        unsigned i,j,atomIndex;
        wxString format;
        if(ligandArray.GetCount()<=0)
                return;
        format= useTab ? wxT("\t%12d") : wxT(" %12d");
        tos<< FILE_BEGIN_LIGAND <<endl;
        for(i=0 ; i < chemArray.GetCount() ; i++)
                indexHash[chemArray[i].atom.uniqueId]=i;
        for(i=0 ; i< ligandArray.GetCount() ; i++)
        {
                atomIndex=indexHash.find(ligandArray[i].dummyAtomId)->second;
                tos << wxString::Format(wxT("%d"),atomIndex+1);
                for(j=0 ; j< ligandArray[i].linkedAtomIds.GetCount(); j++)
                {
                        atomIndex=indexHash.find(ligandArray[i].linkedAtomIds[j])->second;
                        tos<<wxString::Format(format,atomIndex+1);
                }
                tos<<endl;
        }
        tos << FILE_END_LIGAND <<endl;
}
