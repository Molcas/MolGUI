/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "commons.h"
#include "chematom.h"
#include "atommanuutils.h"

#define VISITED  1
#define OCCUPY_STATUS  -10

MovingAtoms * SearchConnectedAtoms(const AtomBondArray & atomBondArray, int atomForConnection, int * length)
{

    int  * tmpResult = NULL;             /* tmp store the  element */
    int * visitStatus = NULL;   /* store the information wether the element is visited */
    int layerBegin;                /* label to show the search layer begin */
    int layerEnd;                /* label to show the search layer end */
    int moveInLayer;        /* label to move in a given search layer */
    int tmpResultEnd;        /*  label to show the changing end of tmp_result */
    int atomCount = atomBondArray.GetCount();
    int numberOfAtomsInLayer;

    int i;                        /* label for loop */
    int tmp;                        /* temprarily store some value */
    MovingAtoms * movingAtoms = NULL;                /* the result */


    /* distribute the memory */

    if ((tmpResult = (int *) malloc(sizeof(int)*(atomCount+1))) == NULL) {
        return NULL;
    }
    else if((visitStatus = (int *) malloc(sizeof(int)*(atomCount+1))) == NULL) {
        free (tmpResult);
        tmpResult = NULL;
        return NULL ;
    }


    /* initialization */
    for (i=0; i<(atomCount+1); i++) {
        tmpResult[i] = OCCUPY_STATUS;
        visitStatus[i] = BP_NULL_VALUE;
    }

    tmpResult[0] = atomForConnection;
    tmpResultEnd = 1;
    /* to put the atomForConnection in the tmpResult*/

    visitStatus[atomForConnection] = VISITED;
/* the correspoding element in the visitStatus  */



    /* here we employ a algorithm to calculate the connection graph */
    layerBegin = 0;
    layerEnd = tmpResultEnd - 1;
    while (1)
    {
        for (moveInLayer = layerBegin; moveInLayer <= layerEnd; moveInLayer++)
        {
            numberOfAtomsInLayer = atomBondArray[tmpResult[moveInLayer]].bondCount;
            for (i=0; i< numberOfAtomsInLayer; i++)
            {
                tmp = atomBondArray[tmpResult[moveInLayer]].bonds[i].atomIndex;

                if (tmp == BP_NULL_VALUE)
                    continue;          /* here it is the breaking bond
                                          or deleting atom or end flag */
                else if (visitStatus[tmp] == BP_NULL_VALUE)
                {
                    tmpResult[tmpResultEnd]= tmp;
                    tmpResultEnd++; /* to store the element if it's not visited */
                    visitStatus[tmp] = VISITED;
                    continue;
                }
                else
                    continue;
            }

        }

        // here it indicates that we have no new elements added in
        if (layerEnd == (tmpResultEnd -1))
            break;
        else
        {
            layerBegin = layerEnd + 1;
            layerEnd = tmpResultEnd - 1; /* initialize the loop */
        }
    }




    if ((movingAtoms = (MovingAtoms *) malloc (sizeof(MovingAtoms)*tmpResultEnd)) == NULL) {
        free (visitStatus);
        visitStatus = NULL;
        free (tmpResult);
        tmpResult = NULL;
        return NULL;
    }
    else {
        *length = tmpResultEnd;
        for (i=0; i<tmpResultEnd; i++) {
            movingAtoms[i].atomLabel = tmpResult[i];
            movingAtoms[i].x = atomBondArray[tmpResult[i]].atom.pos.x;
            movingAtoms[i].y = atomBondArray[tmpResult[i]].atom.pos.y;
            movingAtoms[i].z = atomBondArray[tmpResult[i]].atom.pos.z;
        }
    }

    /* free the memory occupied */
    free (visitStatus);
    visitStatus = NULL;
    free (tmpResult);
    tmpResult = NULL;


    return movingAtoms;
}





void GroupRotationByZ (MovingAtoms * movingAtoms, int movingAtomArrayLength, double angle)
{
    double tmpx;
    double tmpy;
    double cosgama = cos (angle);
    double singama = sin (angle);
    int i;

    // counter-clockwise
    for (i=0; i<movingAtomArrayLength; i++) {
        tmpx = cosgama*(movingAtoms[i].x) - singama*(movingAtoms[i].y);
        tmpy = singama*(movingAtoms[i].x) + cosgama*(movingAtoms[i].y);
        movingAtoms[i].x = tmpx;
        movingAtoms[i].y = tmpy;
    }
}

void GroupRotationByY (MovingAtoms * movingAtoms, int movingAtomArrayLength, double angle)
{
    double tmpx;
    double tmpz;
    double cosbeta = cos (angle);
    double sinbeta = sin (angle);
    int i;

    // counter-clockwise
    for (i=0; i<movingAtomArrayLength; i++) {
        tmpx = cosbeta*(movingAtoms[i].x) + sinbeta*(movingAtoms[i].z);
        tmpz =-sinbeta*(movingAtoms[i].x) + cosbeta*(movingAtoms[i].z);
        movingAtoms[i].x = tmpx;
        movingAtoms[i].z = tmpz;
    }

}

void GroupRotationByX (MovingAtoms * movingAtoms, int movingAtomArrayLength, double angle)
{
    double tmpy;
    double tmpz;
    double cosalfa = cos (angle);
    double sinalfa = sin (angle);
    int i;

    // counter-clockwise
    for (i=0; i<movingAtomArrayLength; i++) {
        tmpy = cosalfa*(movingAtoms[i].y) - sinalfa*(movingAtoms[i].z);
        tmpz = sinalfa*(movingAtoms[i].y) + cosalfa*(movingAtoms[i].z);
        movingAtoms[i].y = tmpy;
        movingAtoms[i].z = tmpz;
    }

}




void GroupReflection (MovingAtoms * movingAtoms, int movingAtomArrayLength)
{
    int i;

    for (i=0; i<movingAtomArrayLength; i++) {
        movingAtoms[i].x = -(movingAtoms[i].x);
        movingAtoms[i].y = -(movingAtoms[i].y);
        movingAtoms[i].z = -(movingAtoms[i].z);
    }
}


/************************************************************************
 * this function provides selection to some specific fragment
 * so this function is called FragSelect
 * atom1 and atom2 are two selected atoms, if the user choose "select all",
 * one of them is set to "BP_NULL_VALUE". else we will search the connected
 * atoms with atom2
 ************************************************************************/
FragmentInfo FragSelect(AtomBondArray & atomBondArray, SelectionUtil* pSelUtil, AtomSelectType atomSelectType, int atom1, int atom2)
{
    MovingAtoms * fragList;
    int length;

    int i;
    ChemBond bondArrayStoreAtom1[ATOM_MAX_BOND_COUNT];
    ChemBond bondArrayStoreAtom2[ATOM_MAX_BOND_COUNT];
    bool linkState = false;

    if (atom1 == BP_NULL_VALUE) {
        fragList = SearchConnectedAtoms(atomBondArray,atom2,&length);
        if (fragList == NULL) {
            return ERROR_IN_MEMORY_ALLOCATION;
        }
        for (i=0; i<length; i++) {
    pSelUtil->AddManuSelectedIndex(fragList[i].atomLabel, atomSelectType);
        }
        free (fragList);
        fragList = NULL;
    }
    else if (atom2 == BP_NULL_VALUE) {
        fragList = SearchConnectedAtoms(atomBondArray,atom1,&length);
        if (fragList == NULL) {
            return ERROR_IN_MEMORY_ALLOCATION;
        }
        for (i=0; i<length; i++) {
    pSelUtil->AddManuSelectedIndex(fragList[i].atomLabel, atomSelectType);
        }
        free (fragList);
        fragList = NULL;
    }
    else {

        // test that whether atom1 and atom2 are linking
         for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
             if ( atomBondArray[atom1].bonds[i].atomIndex == atom2) {
                 linkState = true;
                 break;
             }
             else {
                 continue;
             }
         }

         if (linkState == false) {
             return NO_BOND_LINK_TWO_SELECT_ATOMS;
         }


        // store the bond array information of atom1 and atom2
        for (i=0; i<ATOM_MAX_BOND_COUNT; i++) {
            bondArrayStoreAtom1[i] = atomBondArray[atom1].bonds[i];
            bondArrayStoreAtom2[i] = atomBondArray[atom2].bonds[i];
        }


        // then break the bond information
        for (i=0; i<atomBondArray[atom1].bondCount; i++)
        {
            if ( atomBondArray[atom1].bonds[i].atomIndex == atom2 )
            {
                atomBondArray[atom1].bonds[i].atomIndex = BP_NULL_VALUE;
                break;
            }
            else
                continue;
        }


        for (i=0; i<atomBondArray[atom2].bondCount; i++)
        {
            if ( atomBondArray[atom2].bonds[i].atomIndex == atom1 )
            {
                atomBondArray[atom2].bonds[i].atomIndex = BP_NULL_VALUE;
                break;
            }
            else
                continue;
        }


        fragList = SearchConnectedAtoms(atomBondArray,atom2,&length);

        // restore the information of breaking bond
        for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
        {
            atomBondArray[atom1].bonds[i] = bondArrayStoreAtom1[i];
            atomBondArray[atom2].bonds[i] = bondArrayStoreAtom2[i];
        }

        // write the result
        if (fragList == NULL) {
            return ERROR_IN_MEMORY_ALLOCATION;
        }
        else {
            for (i=0; i<length; i++) {
    pSelUtil->AddManuSelectedIndex(fragList[i].atomLabel, atomSelectType);
            }
        }
        free (fragList);
        fragList = NULL;

    }


    return SUCCESS;

}










// liufenglai: the function below is abandoned.
// but nay be in the future we can use it again. so only comment it out


// /************************************************************************
//  * this function provides selection to some specific fragment
//  * so this function is called FragSelect
//  * the result returns the fragment which has been seleted, in the form of
//  * movingatoms struct array(see above), and this array's length is the
//  * "length" in the parameters list
//  * atomSelect is one of the selected atom, if it's BP_NULL_VALUE, then
//  * actually we only search for the connection atoms for the
//  * atomForConnection, then returns the result; else we will detect that
//  * whether there's a bond linking atomSelect and atomForConnection,if not;
//  * we will return an error message
//  * we will always search the connection atoms which directly or indirectly
//  * connected to the atomForConnection
//  * if the user choose to break more bonds while atomSelect and
//  * atomForConnection are still linking together, then we use the
//  * bondbreak1 and bondbreak2 to indicate the bond location. In other mode,
//  * both of the two values should be set to BP_NULL_VALUE
//  ************************************************************************/
// /**********************************************************************************
// FragmentInfo FragSelection(AtomBondArray & atomBondArray, MovingAtoms * fragList, int * length, int atomSelect, int atomForConnection, int bondBreak1, int bondBreak2)
// {

//     int typeOfSearch;
//     int i;
//     bool linkState = false;

//     // used to store the bond information related to atomSelect,
//     // atomForConnection, bondBreak1 and bondBreak2
//     ChemBond bondArrayStoreOfAtomSelect[ATOM_MAX_BOND_COUNT];
//     ChemBond bondArrayStoreOfAtomConnect[ATOM_MAX_BOND_COUNT];
//     ChemBond bondArrayStoreOfBondBreak1[ATOM_MAX_BOND_COUNT];
//     ChemBond bondArrayStoreOfBondBreak2[ATOM_MAX_BOND_COUNT];

//     fragList = NULL;
//     length = NULL;

// /**************************************************************************
//      determine the search type
//      if type of search is set to 0; then the atomSelect is set to
//      BP_NULL_VALUE
//      if type of search is set to 1; an initial search is started between
//      atomSelect and atomForConnection
//      if type of search is set to 2; we are going to break more bonds and
//      continue the search. here we will use the bondbreak1 and bondbreak2
// ***************************************************************************/
//     if (atomSelect == BP_NULL_VALUE) {
//         typeOfSearch = 0;
//     }
//     else if (bondBreak1 == BP_NULL_VALUE && bondBreak2 == BP_NULL_VALUE) {
//         typeOfSearch = 1;
//     }
//     else if (bondBreak1 != BP_NULL_VALUE && bondBreak2 != BP_NULL_VALUE) {
//         typeOfSearch = 2;
//     }
//     else {
//         return ERROR_IN_PARAMETER;
//     }

//     if (typeOfSearch == 0) {
//         fragList = SearchConnectedAtoms(atomBondArray,atomForConnection,length);
//         if (fragList == NULL) {
//             return ERROR_IN_MEMORY_ALLOCATION;
//         }
//         else {
//             return  SUCCESS;
//         }
//     }

//     if (typeOfSearch == 1) {
//         // check whether the atomSelect and atomForConnection are linking
//         for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
//         {
//             if ( atomBondArray[atomSelect].bonds[i].atomIndex == atomForConnection)
//             {
//                 linkState = true;
//                 break;
//             }
//             else
//             {
//                 continue;
//             }
//         }

//         if (linkState == false) {
//             return NO_BOND_LINK_TWO_SELECT_ATOMS;
//         }
//     }

//     // copy the bond information between the
//     // atomSelect and atomForConnection
//     for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
//     {
//         bondArrayStoreOfAtomSelect[i] = atomBondArray[atomSelect].bonds[i];
//         bondArrayStoreOfAtomConnect[i]  = atomBondArray[atomForConnection].bonds[i];
//     }

//     // then break the bond information
//     for (i=0; i<atomBondArray[atomSelect].bondCount; i++)
//     {
//         if ( atomBondArray[atomSelect].bonds[i].atomIndex == atomForConnection )
//         {
//             atomBondArray[atomSelect].bonds[i].atomIndex = BP_NULL_VALUE;
//             break;
//         }
//         else
//             continue;
//     }


//     for (i=0; i<atomBondArray[atomForConnection].bondCount; i++)
//     {
//         if ( atomBondArray[atomForConnection].bonds[i].atomIndex == atomSelect )
//         {
//             atomBondArray[atomForConnection].bonds[i].atomIndex = BP_NULL_VALUE;
//             break;
//         }
//         else
//             continue;
//     }

//     if (typeOfSearch == 1) {

//         fragList = SearchConnectedAtoms(atomBondArray,atomForConnection,length);

//         // restore the information of breaking bond
//         for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
//         {
//             atomBondArray[atomSelect].bonds[i] = bondArrayStoreOfAtomSelect[i];
//             atomBondArray[atomForConnection].bonds[i] = bondArrayStoreOfAtomConnect[i];
//         }


//         if (fragList == NULL) {
//             return ERROR_IN_MEMORY_ALLOCATION;
//         }

//         /* if two atoms bonding into a circle, return this information*/
//         for (i=0; i<(*length); i++)
//         {
//             if ( fragList[i].atomLabel == atomSelect)
//             {

//                 if (fragList != NULL) {
//                     free (fragList);
//                     fragList = NULL;
//                 }
//                 return MORE_BOND_HAS_TO_BE_BREAK;
//             }
//         }

//         return SUCCESS;

//     }
//     else {

//         // copy the bond information between the
//         // bondbreak1 and bondbreak2
//         for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
//         {
//             bondArrayStoreOfBondBreak1[i] = atomBondArray[bondBreak1].bonds[i];
//             bondArrayStoreOfBondBreak2[i]  = atomBondArray[bondBreak2].bonds[i];
//         }


//         // break the bond information between bondbreak1 and bondbreak2
//         for (i=0; i<atomBondArray[bondBreak1].bondCount; i++)
//         {
//             if ( atomBondArray[bondBreak1].bonds[i].atomIndex == bondBreak2)
//             {
//                 atomBondArray[bondBreak1].bonds[i].atomIndex = BP_NULL_VALUE;
//                 break;
//             }
//             else
//                 continue;
//         }


//         for (i=0; i<atomBondArray[bondBreak2].bondCount; i++)
//         {
//             if ( atomBondArray[bondBreak2].bonds[i].atomIndex == bondBreak1)
//             {
//                 atomBondArray[bondBreak2].bonds[i].atomIndex = BP_NULL_VALUE;
//                 break;
//             }
//             else
//                 continue;
//         }


//         fragList = SearchConnectedAtoms(atomBondArray,atomForConnection,length);

//         // restore the information of breaking bond
//         for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
//         {
//             atomBondArray[atomSelect].bonds[i] = bondArrayStoreOfAtomSelect[i];
//             atomBondArray[atomForConnection].bonds[i] = bondArrayStoreOfAtomConnect[i];
//             atomBondArray[bondBreak1].bonds[i] = bondArrayStoreOfBondBreak1[i];
//             atomBondArray[bondBreak2].bonds[i] = bondArrayStoreOfBondBreak2[i];
//         }


//         if (fragList == NULL) {
//             return ERROR_IN_MEMORY_ALLOCATION;
//         }

//         /* if two atoms bonding into a circle, return this information*/
//         for (i=0; i<(*length); i++)
//         {
//             if ( fragList[i].atomLabel == atomSelect)
//             {

//                 if (fragList != NULL) {
//                     free (fragList);
//                     fragList = NULL;
//                 }
//                 return MORE_BOND_HAS_TO_BE_BREAK;
//             }
//         }

//         return SUCCESS;

//     }





// }


#undef VISITED
#undef OCCUPY_STATUS
