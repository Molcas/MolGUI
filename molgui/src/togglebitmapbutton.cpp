/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "togglebitmapbutton.h"

//DEFINE_EVENT_TYPE(wxEVT_COMMAND_TOGGLEBUTTON_CLICKED);

BEGIN_EVENT_TABLE(ToggleBitmapButton, wxWindow)
    EVT_PAINT(ToggleBitmapButton::OnPaint)
    EVT_MOUSE_EVENTS(ToggleBitmapButton::OnMouseEvent)
    EVT_SIZE(ToggleBitmapButton::OnSizeEvent)
    EVT_ERASE_BACKGROUND(ToggleBitmapButton::OnEraseBackground)
    EVT_CHAR(ToggleBitmapButton::OnChar)
    EVT_SET_FOCUS(ToggleBitmapButton::OnSetFocus)
    EVT_KILL_FOCUS(ToggleBitmapButton::OnKillFocus)
END_EVENT_TABLE()
IMPLEMENT_DYNAMIC_CLASS(ToggleBitmapButton, wxWindow);

#define wxMB_COLOR_OVER wxColour(0xE8,0xE8,0xE8)
#define wxMB_COLOR_BG wxColour(0xD7,0xD7,0xD7)
#define wxMB_TEXT_MARGIN 8

ToggleBitmapButton::ToggleBitmapButton(wxWindow *parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator,
                   const wxString& name): wxWindow(parent, id, pos, size, style, name) {
        m_strLabel = wxEmptyString;
        m_buttonState = STATE_BUTTON_UP;
        m_buttonStatePrev = STATE_BUTTON_UP;
        m_painted = false;
        m_enabled = true;
        m_hasFocus = false;

        if(size == wxDefaultSize) {
               m_width = 32;
               m_height = 32;
        }else {
               m_width = size.x;
               m_height = size.y;
        }

        SetSize(m_width, m_height);
        m_bitmap = new wxBitmap(m_width, m_height);
}

void ToggleBitmapButton::SetBitmapToggled(const wxBitmap& bitmap) {
    m_buttonBitmapToggled = bitmap;
}

void ToggleBitmapButton::SetBitmapNormal(const wxBitmap& bitmap) {
    m_buttonBitmapNormal = bitmap;
}

void ToggleBitmapButton::SetLabel(const wxString& label) {
        m_strLabel = label;
}

void ToggleBitmapButton::OnPaint(wxPaintEvent & event) {
    wxPaintDC dc(this);

    if(!m_painted) {
           DrawOnBitmap();
           m_painted = true;
    }
    dc.DrawBitmap(*m_bitmap, 0, 0, true);
}


void ToggleBitmapButton::OnSizeEvent(wxSizeEvent &event) {
    // Redraw();
    event.Skip();
}

void ToggleBitmapButton::OnEraseBackground(wxEraseEvent &event) {
#ifdef __WXMSW__
    Redraw();
    event.Skip();
#else
    // at this time, the background image isn't repaint by the toolbar,
    // so defer the redraw for later
    wxSizeEvent ev(GetSize(),GetId());
    AddPendingEvent(ev);
    event.Skip();
#endif
}

void ToggleBitmapButton::OnSetFocus(wxFocusEvent& event) {
        /*if(!m_hasFocus) {
                m_hasFocus = true;
                    Redraw();
                    event.Skip();
        }*/
}

void ToggleBitmapButton::OnKillFocus(wxFocusEvent& event) {
        /*if(m_hasFocus) {
                m_hasFocus = false;
                    Redraw();
                    event.Skip();
        }*/
}

void ToggleBitmapButton::Redraw() {
    if(m_painted) {
           DrawOnBitmap();
           wxClientDC dc(this);
           dc.DrawBitmap(*m_bitmap, 0, 0, true);
    }
}

void ToggleBitmapButton::DispathEvent(void) {
        // Generate a wx event.
        wxCommandEvent event(wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, GetId());
        event.SetInt(GetValue());
        event.SetEventObject(this);
        this->GetEventHandler()->ProcessEvent(event);

}

void ToggleBitmapButton::DrawOnBitmap()
{
    wxMemoryDC dc;
    dc.SelectObject(*m_bitmap);

    //wxBrush brush(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE), wxSOLID);
    //dc.SetBackground(brush);
    //dc.Clear();

    //dc.SetPen(*wxLIGHT_GREY_PEN);
    //// separator is a vertical relief line
    //dc.DrawLine(m_width>>1, 1, m_width>>1, m_height-3);
    //dc.SetPen(*wxMEDIUM_GREY_PEN);
    //dc.DrawLine((m_width>>1)+1,2,(m_width>>1)+1,m_height-2);

    switch(m_buttonState) {
        case STATE_BUTTON_UP:
            DrawBorder(dc, BORDER_HIGH);
            break;
        case STATE_BUTTON_OVER:
            DrawBorder(dc, BORDER_OVER);
            break;
        case STATE_BUTTON_DOWN:
            DrawBorder(dc, BORDER_SUNKEN);
            break;
        case STATE_BUTTON_DISABLE:
            DrawBorder(dc, BORDER_FLAT);
            break;
        default:
            break;
    }

}

void ToggleBitmapButton::DrawBorder(wxDC& dc, BorderType border) {
    //wxColour bg = GetParent()->GetBackgroundColour();
    wxColour bg = GetBackgroundColour();
    if(border == BORDER_SUNKEN) {
        bg = wxColour(wxT("white"));
    }
          //wxBrush brush_over(wxMB_COLOR_OVER, wxSOLID);
        wxBrush brush_over(bg, wxBRUSHSTYLE_SOLID);
        dc.SetBrush(brush_over);
        dc.SetPen(*wxTRANSPARENT_PEN);
        //dc.SetPen(wxPen(bg, 1));
        dc.DrawRectangle(0,  0,  m_width, m_height);

    wxPen lightPen(wxColour(0xFF,0xFF,0xFF), 1, wxPENSTYLE_SOLID);
    wxPen grayPen(wxColour(0xC0,0xC0,0xC0), 1, wxPENSTYLE_SOLID);
    wxPen darkPen(wxColour(0x80,0x80,0x80), 1, wxPENSTYLE_SOLID);
        wxPen focusPen(wxColour(0x00,0x00,0x00), 1, wxPENSTYLE_SHORT_DASH);
    //wxPen corner(wxMB_COLOR_BG, 1, wxSOLID);

        wxSize sz = GetClientSize();
        int width = 0, height = 0;
        int x = 0, y = 0;
        if(m_strLabel.IsEmpty()) {
            wxBitmap bitmap = m_buttonBitmapNormal;
            if(STATE_BUTTON_DOWN == m_buttonState && m_buttonBitmapToggled.IsOk()) {
                bitmap = m_buttonBitmapToggled;
            }
            width = bitmap.GetWidth();
            height =  bitmap.GetHeight();
            x = wxMax(0, (sz.x - width)/2);
            y = wxMax(0, (sz.y - height)/2);
            dc.DrawBitmap(bitmap, x, y, true);
        }else {
                dc.GetTextExtent(m_strLabel, &width, &height);
            x = wxMax(0, (sz.x - width)/2);
            y = wxMax(0, (sz.y - height)/2);
                dc.SetFont(GetFont());
            dc.DrawText(m_strLabel, x, y);
        }
    /*if(m_hasFocus) {
           dc.SetPen(focusPen);
           dc.DrawLine(2, 2, m_width-2,2);
           dc.DrawLine(2, 2, 2, m_height-2);
           dc.DrawLine(2, m_height-2, m_width-2, m_height-2);
           dc.DrawLine(m_width-2, 2, m_width-2, m_height-2);
     }*/
    switch(border) {
    case BORDER_HIGH:
           dc.SetPen(lightPen);
           dc.DrawLine(0, 0, m_width - 1, 0);
           dc.DrawLine(0, 0, 0, m_height - 1);
           dc.SetPen(darkPen);
           dc.DrawLine(0, m_height - 1, m_width - 1, m_height - 1);
           dc.DrawLine(m_width - 1, 0, m_width - 1, m_height - 1);
           dc.SetPen(grayPen);
           dc.DrawLine(1, m_height - 2, m_width - 2, m_height - 2);
           dc.DrawLine(m_width - 2, 1, m_width - 2, m_height - 2);
           break;
    case BORDER_SUNKEN:
           dc.SetPen(darkPen);
           dc.DrawLine(0, 0, m_width - 1, 0);
           dc.DrawLine(0, 0, 0, m_height - 1);
           dc.SetPen(grayPen);
           dc.DrawLine(1, 1, m_width - 2, 1);
           dc.DrawLine(1, 1, 1, m_height - 2);
       dc.SetPen(lightPen);
           dc.DrawLine(1, m_height - 1, m_width - 1, m_height - 1);
           dc.DrawLine(m_width - 1, 1, m_width - 1, m_height-1);
           break;
        case BORDER_FLAT:
        break;
    default:
           break;
    }
}

void ToggleBitmapButton::OnMouseEvent(wxMouseEvent & event)
{
    if(!IsEnabled()) return;

    //wxCommandEvent ev(wxEVT_COMMAND_MENU_SELECTED, GetId());
    //ev.SetEventObject(GetParent());

    if(event.LeftDown()) {
        //SetFocus();
        SetValue(!GetValue());
        DispathEvent();
    }

    /*if(event.ButtonDown()) {
           m_buttonState = STATE_BUTTON_DOWN;
           Redraw();
    }
    else if(event.ButtonUp()) {
           m_buttonState = STATE_BUTTON_OVER;
           wxPoint pos = event.GetPosition();
           if((pos.x < GetSize().GetWidth()) && (pos.y < GetSize().GetHeight())) {
            GetParent()->ProcessEvent(ev);
           }
           Redraw();
    }*/
    else if (event.Entering()) {
        if(m_buttonState == STATE_BUTTON_UP) {
            m_buttonState = STATE_BUTTON_OVER;
            Redraw();
        }
    }
    else if(event.Leaving()) {
           if(m_buttonState == STATE_BUTTON_OVER) {
            m_buttonState = STATE_BUTTON_UP;
            Redraw();
           }
    }
    event.Skip(false);
}

void ToggleBitmapButton::OnChar(wxKeyEvent& event) {
    if(!IsEnabled()) return;
    if(event.GetKeyCode() == WXK_SPACE) {
        SetValue(!GetValue());
        DispathEvent();
    }
    event.Skip();
}

bool ToggleBitmapButton::GetValue() const {
    return m_buttonState == STATE_BUTTON_DOWN ;
}

void ToggleBitmapButton::SetValue(const bool state) {
    if(GetValue() != state) {
        if(state)  {
            m_buttonState = STATE_BUTTON_DOWN;
        }else {
            m_buttonState = STATE_BUTTON_UP;
        }
        Redraw();
    }
}

bool ToggleBitmapButton::Enable(bool enable) {
    if(m_enabled != enable) {
        if(enable) {
            m_buttonState = m_buttonStatePrev;
        }else{
            m_buttonStatePrev = m_buttonState;
            m_buttonState = STATE_BUTTON_DISABLE;
        }
        m_enabled = enable;
        Redraw();
    }
    return m_enabled == true;
}

bool ToggleBitmapButton::IsEnabled() const {
    return m_enabled;
}

void ToggleBitmapButton::SetSize(int x, int y, int width, int height, int sizeFlags) {
    //SetPosition(wxPoint(x, y));
    SetSize(width, height);
    wxWindow::SetSize(x, y, width, height, sizeFlags);
}

void ToggleBitmapButton::SetSize(const wxRect& rect) {
    SetSize(rect.GetWidth(), rect.GetHeight());
}

void ToggleBitmapButton::SetSize(int width, int height) {
    m_height = height;
    m_width = width;
    Redraw();
}

void ToggleBitmapButton::SetSize(const wxSize& size) {
    SetSize(size.GetWidth(), size.GetHeight());
}
