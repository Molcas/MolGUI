/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "mirrorpnl.h"
#include "molvieweventhandler.h"

#define BP_SLIDER_PRECISION 1000.0

////////////////////////////////////////////////////////////////////
//enum
////////////////////////////////////////////////////////////////////
enum{
        ID_CTRL_TEXT_VALUE = 1,
        ID_CTRL_BTN_MIRROR,
        ID_CTRL_SLIDER,
        ID_CTRL_CKB_SELECT,
        ID_CTRL_RB_SINGLE,
        ID_CTRL_RB_CONNECTED,
        //ID_CTRL_BTN_CLEAR
};
//////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE(MirrorPnl, wxPanel)
    EVT_TEXT_ENTER(ID_CTRL_TEXT_VALUE, MirrorPnl::OnTextEnter)
        EVT_COMMAND_SCROLL(ID_CTRL_SLIDER, MirrorPnl::OnSliderScroll)
        EVT_BUTTON(ID_CTRL_BTN_MIRROR, MirrorPnl::OnBtnMirror)
        //EVT_BUTTON(ID_CTRL_BTN_CLEAR, MirrorPnl::OnBtnClear)
        EVT_CHECKBOX(ID_CTRL_CKB_SELECT, MirrorPnl::OnCkbSelect)
        EVT_RADIOBUTTON(wxID_ANY, MirrorPnl::OnRbSelectType)
END_EVENT_TABLE()

MirrorPnl::MirrorPnl(wxWindow* parent, wxWindowID id, const wxPoint& pos,
            const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name) {
        m_pCtrlInst = NULL;
        m_pBsAngle = NULL;
        m_atomSelectType = ATOM_SELECT_SINGLE;
        InitPanel();
}

void MirrorPnl::InitPanel(void) {
        int height = wxDefaultSize.GetHeight();

    wxBoxSizer* pBsMain = new wxBoxSizer(wxHORIZONTAL);
        this->SetSizer(pBsMain);
    m_pBSelect = new wxBoxSizer(wxHORIZONTAL);
        m_pBsAngle = new wxBoxSizer(wxHORIZONTAL);


        m_pCkbSelect = new wxCheckBox(this, ID_CTRL_CKB_SELECT, wxT("Select Mirrored Atoms"), wxDefaultPosition, wxSize(140, height));

        wxStaticBox *sbBox = new wxStaticBox(this, wxID_ANY, wxEmptyString);
    wxBoxSizer* bsSelectType = new wxStaticBoxSizer(sbBox, wxHORIZONTAL);
    m_pRbTypeSingle = new wxRadioButton(this, ID_CTRL_RB_SINGLE, wxT("Single"), wxDefaultPosition, wxSize(80, height), wxRB_GROUP);
    m_pRbTypeConnected = new wxRadioButton(this, ID_CTRL_RB_CONNECTED, wxT("Connected"), wxDefaultPosition, wxSize(80, height));
    bsSelectType->Add(m_pRbTypeSingle, 0, wxALL, 5);
    bsSelectType->Add(m_pRbTypeConnected, 0, wxALL, 5);
    //wxString selectType[2] = {wxT("One by one"), wxT("One with all its connected")};
        //wxRadioBox* rbSelectType = new wxRadioBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(300, height), 2, selectType, 1, wxRA_SPECIFY_ROWS);

    //m_pBtnClear = new wxButton(this, ID_CTRL_BTN_CLEAR, wxT("Select None"), wxDefaultPosition, wxSize(100, 30));

    m_pBSelect->Add(m_pCkbSelect, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
    m_pBSelect->Add(bsSelectType, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);

        wxStaticText* pStAngle = new wxStaticText(this, wxID_ANY, wxT("Angle"), wxDefaultPosition, wxSize(80, height), wxALIGN_RIGHT | wxST_NO_AUTORESIZE);
        m_pTcValue = new wxTextCtrl(this, ID_CTRL_TEXT_VALUE, wxT("180"), wxDefaultPosition, wxSize(60, 30), wxTE_RIGHT | wxTE_PROCESS_ENTER);
        wxStaticText* pStMin = new wxStaticText(this, wxID_ANY, wxT("0"), wxDefaultPosition, wxSize(20, height),wxALIGN_RIGHT | wxST_NO_AUTORESIZE);
        //m_pSlider = new wxSlider(this, ID_CTRL_SLIDER, 50, 0, 100, wxDefaultPosition, wxSize(200, 30), wxSL_HORIZONTAL|wxSL_AUTOTICKS);
        m_pSlider = new wxSlider(this, ID_CTRL_SLIDER, 50, 0, 100, wxDefaultPosition, wxSize(300, 30), wxSL_HORIZONTAL|wxSL_AUTOTICKS);
        wxStaticText* pStMax = new wxStaticText(this, wxID_ANY, wxT("360"), wxDefaultPosition, wxSize(40, height), wxALIGN_LEFT | wxST_NO_AUTORESIZE);
        m_pSlider->SetTickFreq(10);

        m_pBsAngle->Add(pStAngle, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
        m_pBsAngle->Add(m_pTcValue, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
        m_pBsAngle->Add(pStMin, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
        m_pBsAngle->Add(m_pSlider, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
        m_pBsAngle->Add(pStMax, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);

        m_pSlider->SetRange((long)0, (long)(BP_SLIDER_PRECISION * 360.0f));
    m_pSlider->SetTickFreq((long)(BP_SLIDER_PRECISION * 360.0f)/10);
    SetDefaultValues();

        m_pBtnMirror = new wxButton(this, ID_CTRL_BTN_MIRROR, wxT("Mirror"), wxDefaultPosition, wxSize(80, 30));

        //pBsMain->Add(m_pCkbSelect, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
    //pBsMain->Add(bsSelectType, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
   //pBsMain->Add(m_pBtnClear, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);

    pBsMain->Add(m_pBSelect, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
    //pBsMain->AddStretchSpacer(1);
        pBsMain->Add(m_pBsAngle, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);
        pBsMain->AddStretchSpacer(1);
        pBsMain->Add(m_pBtnMirror, 0, wxALIGN_CENTER_VERTICAL |wxALL, 5);

        DoCkbSelect(false);
        ShowAnglePanel(false);

    GetSizer()->Show(m_pBSelect, false);
        m_pBtnMirror->Enable(false);
}

void MirrorPnl::ShowAnglePanel(bool isAngle) {
    GetSizer()->Show(m_pBsAngle, isAngle);

    Layout();
        Refresh();
}

void MirrorPnl::SetCtrlInst(CtrlInst* pCtrlInst) {
    m_pCtrlInst = pCtrlInst;
}

void MirrorPnl::ClearSelection(void) {
    if(m_pCtrlInst) {
           m_pCtrlInst->GetSelectionUtil()->ClearSelection();
    }
        ShowAnglePanel(false);
}

void MirrorPnl::AddSelectedIndex(int selectedAtomIndex) {
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        if(m_pCkbSelect->GetValue()) {
        pSelUtil->AddManuSelectedIndex(selectedAtomIndex, m_atomSelectType);
        }else {
                if(m_pCtrlInst->GetRenderingData()->GetAtomBond(selectedAtomIndex).bondCount <= ATOM_MAX_BOND_COUNT / 2 ) {
                        if(pSelUtil->GetSelectedCount() > 2 || pSelUtil->IsSelected(selectedAtomIndex)) {
                                pSelUtil->ClearSelection();
                        }
                        pSelUtil->AddSelectedIndex(selectedAtomIndex, true);
                        m_pBtnMirror->Enable(pSelUtil->GetSelectedCount() > 0);
                        ShowAnglePanel(pSelUtil->GetSelectedCount() == 2);
                }
        }
}

void MirrorPnl::SetDefaultValues(void) {
        m_pTcValue->SetValue(wxT("180.0"));
        m_pSlider->SetValue((long)(180 * BP_SLIDER_PRECISION));
}

void MirrorPnl::OnTextEnter(wxCommandEvent& event) {
        double newValue = 0;
        long longValue = 0;
        wxString strValue = m_pTcValue->GetValue();
        strValue.ToDouble(&newValue);
        longValue = (long)(newValue * BP_SLIDER_PRECISION);
        if(longValue <= m_pSlider->GetMax() && longValue >= m_pSlider->GetMin()) {
                m_pSlider->SetValue(longValue);
        }else {
                SetDefaultValues();
        }
}

void MirrorPnl::OnSliderScroll(wxScrollEvent& event) {
        float value = event.GetInt() / BP_SLIDER_PRECISION;
        if(value < 0) value = 0;
        else if(value > 360.0f) value = 360.0;
        m_pTcValue->SetValue(wxString::Format(wxT("%.1f"), value));
}

void MirrorPnl::OnBtnMirror(wxCommandEvent& event) {
        double newValue = 0;
        wxString strValue = m_pTcValue->GetValue();
        strValue.ToDouble(&newValue);
        if(m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() > 0) {
                m_pCtrlInst->SaveHistroyFile();
                m_pCtrlInst->GetRenderingData()->Mirror(m_pCtrlInst->GetSelectionUtil(), !m_pCkbSelect->GetValue(), newValue);
                 //--------------original code--------------
         /*
        ShowAnglePanel(false);
                m_pBtnMirror->Enable(false);
                m_pCkbSelect->SetValue(false);
                DoCkbSelect(false);
                */
                ShowAnglePanel(true);
        m_pBtnMirror->Enable(true);
                m_pCkbSelect->SetValue(false);
                DoCkbSelect(true);
                //---------end-------------------------------
                m_pCtrlInst->GetSelectionUtil()->ClearAll();
                MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
                MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        }

}
/*

void MirrorPnl::OnBtnClear(wxCommandEvent& event) {
        DoClear();
}
*/
void MirrorPnl::DoClear(void) {
        if(m_pCtrlInst) {
                m_pCtrlInst->GetSelectionUtil()->ClearManuSelection();
                MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        }
}

void MirrorPnl::OnCkbSelect(wxCommandEvent& event){
        DoCkbSelect(event.IsChecked());
}

void MirrorPnl::DoCkbSelect(bool isSelected) {
        m_pRbTypeSingle->Enable(isSelected);
        m_pRbTypeConnected->Enable(isSelected);
        //m_pBtnClear->Enable(isSelected);
        if(!isSelected) {
                DoClear();
        }else {
                if(m_pRbTypeSingle->GetValue()) {
                        m_atomSelectType = ATOM_SELECT_SINGLE;
                }else {
                        m_atomSelectType = ATOM_SELECT_CONNECTED;
                }
        }
}

void MirrorPnl::OnRbSelectType(wxCommandEvent& event){
        if(event.GetId() == ID_CTRL_RB_SINGLE) {
                m_atomSelectType = ATOM_SELECT_SINGLE;
        }else if(event.GetId() == ID_CTRL_RB_CONNECTED) {
                m_atomSelectType = ATOM_SELECT_CONNECTED;
        }
}
