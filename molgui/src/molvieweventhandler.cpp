/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "molvieweventhandler.h"
#include "tools.h"
#include "envutil.h"
#include "envsettings.h"
#include "fileutil.h"
#include "stringutil.h"
#include "ctrlids.h"
#include "auisharedwidgets.h"
#include "molfile.h"
#include "atomlistfrm.h"
//begin : hurukun : 12/11/2009
#include "mopacparadlg.h"
#include "mopacfile.h"
#include "xyzfile.h"
#include "simuproject.h"
#include "manager.h"
#include "projectmanager.h"
#include "viewmanager.h"
//end   : hurukun : 12/11/2009
#include "simuprjfrm.h"
#include "dlgoutput.h"

#include <wx/utils.h> 

BEGIN_EVENT_TABLE(MolViewEventHandler, wxEvtHandler)
    //EVT_UPDATE_UI(idViewFullScreen, MainFrame::OnViewMenuUpdateUI)
END_EVENT_TABLE()

static MolViewEventHandler *viewEventHandler = NULL;

MolViewEventHandler *MolViewEventHandler::Get(void) {
    if(viewEventHandler == NULL) {
        viewEventHandler = new MolViewEventHandler;
    }
    return viewEventHandler;
}

void MolViewEventHandler::Free(void) {
    if(viewEventHandler)delete viewEventHandler;
    viewEventHandler = NULL;
}

MolViewEventHandler::MolViewEventHandler() {
    m_pMolView = NULL;
    m_pWinMenu = NULL;
    //begin : hurukun : 13/11/2009
    m_strModualName = wxEmptyString;
}        //end   : hurukun : 13/11/2009

MolViewEventHandler::~MolViewEventHandler() {
}

void MolViewEventHandler::SetMolView(MolView *pMolView) {
    m_pMolView = pMolView;
}

MolView *MolViewEventHandler::GetMolView(void) const {
    return m_pMolView;
}

void MolViewEventHandler::SetWinMenu(wxMenu *pWinMenu) {
    m_pWinMenu = pWinMenu;
}

wxMenu *MolViewEventHandler::GetWinMenu(void) const {
    return m_pWinMenu;
}

void MolViewEventHandler::ProcessMenuEvent(wxCommandEvent &event) {
    //   wxMessageBox("ProcessMenuEvent");
    if(event.GetId() >= MENU_ID_MODEL_WIRE && event.GetId() <= MENU_ID_MODEL_RIBBON) {
        OnMenuModelType(event);
    } else if(event.GetId() >= MENU_ID_BUILD_VIEW && event.GetId() <= MENU_ID_BUILD_SELECT_LAYER) {
        OnMenuMoleculeBuild(event);
    } else if(event.GetId() >= MENU_ID_MODEL_SHOW_HYDROGENS && event.GetId() <= MENU_ID_MODEL_SHOW_AXES) {
        OnMenuModelShows(event);
    } else if(event.GetId() >= MENU_ID_MODEL_SHOW_LAYERS_SHOW && event.GetId() <= MENU_ID_MODEL_SHOW_LAYERS_LOW) {
        OnMenuModelShowLayers(event);
    } else if(event.GetId() >= POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS && event.GetId() <= POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS) {
        OnPopupMenuModelShows(event);
    } else if(event.GetId() == MENU_ID_BUILD_CREATE_TEMPLATE) {
        OnCreateTemplate(event);
    } else if(event.GetId() == MENU_ID_BUILD_DELETE_TEMPLATE) {
        OnDeleteTemplate(event);
    } else if(event.GetId() == MENU_ID_BUILD_RESET) {
        OnPopupReset(event);
    } else if(event.GetId() >= MENU_ID_GEOMETRY_MEASURE && event.GetId() <= MENU_ID_GEOMETRY_MIRROR) {
        OnMenuGeometry(event);
    } else if(event.GetId() == MENU_ID_EDIT_CLEAR) {
        OnEditClear(event);
    } else if(event.GetId() == MENU_ID_EDIT_REDO) {
        OnEditRedo(event);
    } else if(event.GetId() == MENU_ID_EDIT_ATOM_LIST) {
        OnEditAtomList(event);
    } else if(event.GetId() == MENU_ID_EDIT_UNDO) {
        OnEditUndo(event);
    } else if(event.GetId() == MENU_ID_CALC_MM_OPTIMIZATION) {
        OnCalcMMOptimization(event);
        //begin : hurukun : 12/11/2009
    } else if(event.GetId() == MENU_ID_CALC_MOPAC_OPTIMIZATION) {
        OnCalcMopacOptimization(event);
        //end   : hurukun : 12/11/2009
    } else if(event.GetId() == MENU_ID_CALC_SYMMETRY) {
        OnCalcSymmetry(event);
    } else if(event.GetId() == MENU_ID_IMPORT_STRUCTURE) {
        OnImportStructure(event);
        //-----------grp
    } else if(event.GetId() == MENU_ID_EDIT_BACKGROUND_COLOR) {
        ChangeColor changecolour;
        changecolour.OnChangeBackgroundColor(event);
        //------------grp
    }
}
// void MolViewEventHandler::ChangeBC(wxCommandEvent &event){
//     ChangeColor::OnChangeBackgroundColor(event);
// };

void MolViewEventHandler::ProcessToolEvent(wxCommandEvent &event) {
    //    wxMessageBox(wxString::Format(wxT("%d"),event.GetId()));
    if(event.GetId() >= TOOL_ID_BUILD_VIEW && event.GetId() <= TOOL_ID_BUILD_SELECT_LAYER) {
        OnToolMoleculeBuild(event);
    } else if(event.GetId() >= TOOL_ID_GEOMETRY_MEASURE && event.GetId() <= TOOL_ID_GEOMETRY_MIRROR) {
        OnToolGeometry(event);
    } else if(event.GetId() == TOOL_ID_CALC_MM_OPTIMIZATION) {
        OnCalcMMOptimization(event);
        //begin : hurukun : 06/11/2009
    } else if(event.GetId() == TOOL_ID_CALC_MOPAC_OPTIMIZATION) {
        OnCalcMopacOptimization(event);
        //end   : hurukun : 06/11/2009
    } else if(event.GetId() == TOOL_ID_CALC_SYMMETRY) {
        OnCalcSymmetry(event);
    }
}

void MolViewEventHandler::OnMenuMoleculeBuild(wxCommandEvent &event) {
    DoMoleculeBuildCommon(event.GetId(), true, true);
}

void MolViewEventHandler::OnToolMoleculeBuild(wxCommandEvent &event)
{
    DoMoleculeBuildCommon(event.GetId(), true, false);
}
void MolViewEventHandler::DoMoleculeBuildCommon(int eventId, bool isUIEvent, bool isMenu)
{
    //wxMessageBox(wxT("MolViewEventHandler::DoMoleculeBuildCommon"));
    if(GetMolView() == NULL) return;
    if (GetMolView()->GetData()->GetCtrlInstCount() > 0) {
        if (isUIEvent) {
            // if has been checked, dose not allow to be unchecked
            if (isMenu && !Tools::IsMenuChecked(GetMolView()->GetMenuBar(), eventId)) {
                Tools::CheckMenu(GetMolView()->GetMenuBar(), eventId, true);
                //return;
            } else if (!isMenu && !AuiSharedWidgets::Get()->GetToolState(eventId)) {
                AuiSharedWidgets::Get()->ToggleTool(eventId, true);
                //return;
            }
        }
        ActionType actionType = UpdateMenuToolExclusive(isUIEvent, eventId, isMenu);

        /*if(ACTION_DELETE_FRAGMENT == actionType) {
                        AuiSharedWidgets::Get()->GetDeletePnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
                        AuiSharedWidgets::Get()->GetDeletePnl()->UpdateControls(ACTION_DELETE_FRAGMENT);
                        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(STR_PNL_DELETE).Caption(wxT("Delete Panel")).MinSize(700,50).FloatingSize(700,50);
                }else if(ACTION_LAYER == actionType) {
                        AuiSharedWidgets::Get()->GetDeletePnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
                        AuiSharedWidgets::Get()->GetDeletePnl()->UpdateControls(ACTION_LAYER);
                        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(STR_PNL_DELETE).Caption(wxT("Layer Selection Panel")).MinSize(850,50).FloatingSize(850,50);
                }*/
        SetActionType(actionType, eventId);
    }
    //wxMessageBox(wxT("MolViewEventHandler::DoMoleculeBuildCommon"));
}
void MolViewEventHandler::OnMenuGeometry(wxCommandEvent &event)
{
    DoGeometryCommon(event.GetId(), true, true);
}
void MolViewEventHandler::OnToolGeometry(wxCommandEvent &event)
{
    DoGeometryCommon(event.GetId(), true, false);
}
void MolViewEventHandler::DoGeometryCommon(int eventId, bool isUIEvent, bool isMenu)
{
    if(GetMolView() == NULL) return;
    ActionType actionType = UpdateMenuToolExclusive(isUIEvent, eventId, isMenu);
    bool isShow = false;
    if(isMenu) {
        isShow = Tools::IsMenuChecked(GetMolView()->GetMenuBar(), eventId);
    } else {
        isShow = AuiSharedWidgets::Get()->GetToolState(eventId);
    }
    if(isShow) {
        /*if(actionType == ACTION_MEASURE || actionType == ACTION_CONSTRAIN) {
                AuiSharedWidgets::Get()->GetMeasurePnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
            AuiSharedWidgets::Get()->GetMeasurePnl()->SetMeasureOrConstrain(actionType == ACTION_MEASURE);
                AuiSharedWidgets::Get()->GetMeasurePnl()->ClearSelection();
                AuiSharedWidgets::Get()->GetMeasurePnl()->ResetControls();
                if(actionType == ACTION_MEASURE) {
                        AuiSharedWidgets::Get()->GetMeasurePnl()->EnableControls(ACTION_MEASURE);
                        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(STR_PNL_MEASURE).Caption(wxT("Measure Panel")).MinSize(800,50).FloatingSize(800,50);
                }else {
                        AuiSharedWidgets::Get()->GetMeasurePnl()->EnableControls(ACTION_CONSTRAIN);
                        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(STR_PNL_MEASURE).Caption(wxT("Constraint Panel")).MinSize(600,50).FloatingSize(600,50);
                }
        }*/
        SetActionType(actionType, BP_NULL_VALUE);
    } else {
        UpdateMenuToolExclusive(false, MENU_ID_BUILD_VIEW, true);
        SetActionType(ACTION_VIEW, BP_NULL_VALUE);
    }
    GetMolView()->EnableMenuToolCtrlInst();
}
ActionType MolViewEventHandler::UpdateMenuToolExclusive(bool isUIEvent, int id, bool isMenu)
{
    ActionType actionType = ACTION_VIEW;
    int ids[12][2] = {{MENU_ID_BUILD_VIEW, TOOL_ID_BUILD_VIEW},
        {MENU_ID_BUILD_ADD_FRAGMENT, TOOL_ID_BUILD_ADD_FRAGMENT},
        {MENU_ID_BUILD_INSERT_FRAGMENT, TOOL_ID_BUILD_INSERT_FRAGMENT},
        {MENU_ID_BUILD_DELETE_FRAGMENT, TOOL_ID_BUILD_DELETE_FRAGMENT},
        {MENU_ID_BUILD_MAKE_BOND, TOOL_ID_BUILD_MAKE_BOND},
        {MENU_ID_BUILD_BREAK_BOND, TOOL_ID_BUILD_BREAK_BOND},
        {MENU_ID_BUILD_DESIGN_BOND, TOOL_ID_BUILD_DESIGN_BOND},
        {MENU_ID_GEOMETRY_MEASURE, TOOL_ID_GEOMETRY_MEASURE},
        {MENU_ID_GEOMETRY_CONSTRAIN, TOOL_ID_GEOMETRY_CONSTRAIN},
        {MENU_ID_GEOMETRY_SYMMETRIZE, TOOL_ID_GEOMETRY_SYMMETRIZE},
        {MENU_ID_GEOMETRY_DEFINE_DUMMY, TOOL_ID_GEOMETRY_DEFINE_DUMMY},
        {MENU_ID_GEOMETRY_MIRROR, TOOL_ID_GEOMETRY_MIRROR}
    };
    int col = isMenu ? 0 : 1;

    for (int i = 0; i < 12; i++) {
        if ( id != ids[i][col]) {
            Tools::CheckMenu(GetMolView()->GetMenuBar(), ids[i][0], false);
            AuiSharedWidgets::Get()->ToggleTool(ids[i][1], false);
        } else {
            if (isUIEvent) {
                if (isMenu) {
                    AuiSharedWidgets::Get()->ToggleTool(ids[i][1], Tools::IsMenuChecked(GetMolView()->GetMenuBar(), ids[i][0]));
                } else {
                    Tools::CheckMenu(GetMolView()->GetMenuBar(), ids[i][0], AuiSharedWidgets::Get()->GetToolState(ids[i][1]));
                }
            } else {
                Tools::CheckMenu(GetMolView()->GetMenuBar(), ids[i][0], true);
                AuiSharedWidgets::Get()->ToggleTool(ids[i][1], true);
            }
            //wxMessageBox(wxT("MolViewEventHandler::UpdateMenuToolExclusive"));
            if(ids[i][0] ==  MENU_ID_BUILD_VIEW) {
                actionType = ACTION_VIEW;
            } else if(ids[i][0] ==  MENU_ID_BUILD_ADD_FRAGMENT) {
                actionType = ACTION_ADD_FRAGMENT;
            } else if(ids[i][0] ==  MENU_ID_BUILD_INSERT_FRAGMENT) {
                actionType = ACTION_INSERT_FRAGMENT;
            } else if(ids[i][0] ==  MENU_ID_BUILD_DELETE_FRAGMENT) {
                actionType = ACTION_DELETE_FRAGMENT;
            } else if(ids[i][0] ==  MENU_ID_BUILD_MAKE_BOND) {
                actionType = ACTION_MAKE_BOND;
            } else if(ids[i][0] ==  MENU_ID_BUILD_BREAK_BOND) {
                actionType = ACTION_BREAK_BOND;
            } else if(ids[i][0] ==  MENU_ID_BUILD_SELECT_LAYER) {
                actionType = ACTION_LAYER;
            } else if(ids[i][0] ==  MENU_ID_GEOMETRY_MEASURE) {
                actionType = ACTION_MEASURE;
            } else if(ids[i][0] ==  MENU_ID_GEOMETRY_CONSTRAIN) {
                actionType = ACTION_CONSTRAIN;
            } else if(ids[i][0] ==  MENU_ID_GEOMETRY_SYMMETRIZE) {
                actionType = ACTION_SYMMETRIZE;
            } else if(ids[i][0] ==  MENU_ID_GEOMETRY_DEFINE_DUMMY) {
                actionType = ACTION_DEFINE_DUMMY;
            } else if(ids[i][0] ==  MENU_ID_GEOMETRY_MIRROR) {
                actionType = ACTION_MIRROR;
            } else if(ids[i][0] ==  MENU_ID_BUILD_DESIGN_BOND) {
                actionType = ACTION_DESIGN_BOND;
            }
        }
    }


    if (ViewManager::Get()->GetViewCount() > 1) {
        Manager::Get()->GetAppFrame()->GetMenuBar()->Enable(
            MENU_ID_VIEW_CAPTURE_IMAGE, true);
    } else {
        Manager::Get()->GetAppFrame()->GetMenuBar()->Enable(
            MENU_ID_VIEW_CAPTURE_IMAGE, false);
    }


    //    if (ACTION_VIEW  ==  actionType) {
    //                  if ( Tools::IsMenuChecked(GetMolView()->GetMenuBar(), MENU_ID_BUILD_VIEW) )
    //                        Manager::Get()->GetAppFrame()->GetMenuBar()->Enable(MENU_ID_VIEW_CAPTURE_IMAGE, true);
    //                  } else {
    //                        if ( !Tools::IsMenuChecked(GetMolView()->GetMenuBar(), MENU_ID_BUILD_VIEW) )
    //                        Manager::Get()->GetAppFrame()->GetMenuBar()->Enable(MENU_ID_VIEW_CAPTURE_IMAGE, false);
    //                }

    return actionType;
}
void MolViewEventHandler::SetActionType(ActionType actionType, int eventId)
{
    wxString paneName = wxEmptyString;
    bool isClearSelectedAtoms = true;
    switch (actionType) {
    case ACTION_VIEW:
        paneName = wxEmptyString;
        break;
    case ACTION_ADD_FRAGMENT:
    case ACTION_INSERT_FRAGMENT:
        paneName = STR_PNL_FRAGMENTS;
        break;
    case ACTION_DELETE_FRAGMENT:
        paneName = STR_PNL_DELETE;
        AuiSharedWidgets::Get()->GetDeletePnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
        AuiSharedWidgets::Get()->GetDeletePnl()->UpdateControls(ACTION_DELETE_FRAGMENT);
        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(paneName).Caption(wxT("Delete Panel")).MinSize(700, 50).FloatingSize(700, 50);
        break;
    case ACTION_BREAK_BOND:
    case ACTION_MAKE_BOND:
        paneName = wxEmptyString;
        break;
    case ACTION_DESIGN_BOND:
        paneName = STR_PNL_DESIGNBOND;
        AuiSharedWidgets::Get()->GetDesignBondPnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
        AuiSharedWidgets::Get()->GetDesignBondPnl()->ClearSelection();
        //AuiSharedWidgets::Get()->GetDesignBondPnl()->UpdateControls(ACTION_DESIGN_BOND);
        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(paneName).Caption(wxT("Design Bond Panel")).MinSize(850, 50).FloatingSize(850, 50);

        break;
    case ACTION_LAYER:
        paneName = STR_PNL_DELETE;
        AuiSharedWidgets::Get()->GetDeletePnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
        AuiSharedWidgets::Get()->GetDeletePnl()->UpdateControls(ACTION_LAYER);
        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(paneName).Caption(wxT("Layer Selection Panel")).MinSize(850, 50).FloatingSize(850, 50);
        break;
    case ACTION_MEASURE:
    case ACTION_CONSTRAIN:
        AuiSharedWidgets::Get()->GetMeasurePnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
        AuiSharedWidgets::Get()->GetMeasurePnl()->SetMeasureOrConstrain(actionType == ACTION_MEASURE);
        AuiSharedWidgets::Get()->GetMeasurePnl()->ClearSelection();
        AuiSharedWidgets::Get()->GetMeasurePnl()->ResetControls();
        if(actionType == ACTION_MEASURE) {
            paneName = STR_PNL_MEASURE;
            AuiSharedWidgets::Get()->GetMeasurePnl()->EnableControls(ACTION_MEASURE);
            AuiSharedWidgets::Get()->GetAuiManager()->GetPane(paneName).Caption(wxT("Measure Panel")).MinSize(800, 50).FloatingSize(800, 50);
        } else {
            paneName = STR_PNL_MEASURE;
            AuiSharedWidgets::Get()->GetMeasurePnl()->EnableControls(ACTION_CONSTRAIN);
            AuiSharedWidgets::Get()->GetAuiManager()->GetPane(paneName).Caption(wxT("Constraint Panel")).MinSize(600, 50).FloatingSize(600, 50);
        }
        break;
    // case ACTION_SYMMETRIZE:
    //     paneName = STR_PNL_SYMMETRIZE;
    //     AuiSharedWidgets::Get()->GetSymmetrizePnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
    //     AuiSharedWidgets::Get()->GetSymmetrizePnl()->SetMeasureOrConstrain(actionType == ACTION_MEASURE);

    //     AuiSharedWidgets::Get()->GetSymmetrizePnl()->ClearSelection();

    //     AuiSharedWidgets::Get()->GetAuiManager()->GetPane(paneName).Caption(wxT("Symmetrize Panel")).MinSize(600, 50).FloatingSize(600, 50);
    //     break;

    case ACTION_MIRROR:
        paneName = STR_PNL_MIRROR;
        AuiSharedWidgets::Get()->GetMirrorPnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
        AuiSharedWidgets::Get()->GetMirrorPnl()->ClearSelection();
        break;
    case ACTION_DEFINE_DUMMY:
        paneName = wxEmptyString;
        break;
    case ACTION_CHANGE_BOND:
        isClearSelectedAtoms = false;
        break;
    case ACTION_MM:
    //begin : hurukun : 10/11/2009
    case ACTION_MOPAC:
        //end   : hurukun : 10/11/2009
        //                AuiSharedWidgets::Get()->GetProgressBarPnl()->SetCtrlInst(GetMolView()->GetData()->GetActiveCtrlInst());
        paneName = STR_PNL_PROGRESSBAR;
        AuiSharedWidgets::Get()->GetAuiManager()->GetPane(paneName).Caption(wxT("ProgressBar Panel")).MinSize(800, 50).FloatingSize(800, 50);
        break;
    default:
        break;
    }

    /*if(actionType == ACTION_VIEW && GetMolView()->GetMenuBar()->FindItem(MENU_ID_BUILD_VIEW)) {
            if(!AuiSharedWidgets::Get()->GetToolState(TOOL_ID_BUILD_VIEW)) {
                    AuiSharedWidgets::Get()->ToggleTool(TOOL_ID_BUILD_VIEW, true);
            }
            if(!Tools::IsMenuChecked(GetMolView()->GetMenuBar(), MENU_ID_BUILD_VIEW)) {
                    Tools::CheckMenu(GetMolView()->GetMenuBar(), MENU_ID_BUILD_VIEW, true);
            }
    }*/
    if (GetMolView()->GetData()->GetActiveCtrlInst()) {
        GetMolView()->GetData()->SetActionType(actionType);
        if(isClearSelectedAtoms) {
            GetMolView()->GetData()->GetActiveCtrlInst()->ClearSelectedAtoms();
        }
    }
    AuiSharedWidgets::Get()->ShowExclusivePanel(paneName);
    //wxMessageBox(wxT("MolViewEventHandler::SetActionType"));
    GetMolView()->RefreshGraphics();
}
void MolViewEventHandler::OnMenuModelType(wxCommandEvent &event)
{
    ModelType modelType = MODEL_BALLSPOKE;
    if(event.GetId() == MENU_ID_MODEL_WIRE) {
        modelType = MODEL_WIRE;
    } else if(event.GetId() == MENU_ID_MODEL_TUBE) {
        modelType = MODEL_TUBE;
    } else if(event.GetId() == MENU_ID_MODEL_BALLSPOKE) {
        modelType = MODEL_BALLSPOKE;
    } else if(event.GetId() == MENU_ID_MODEL_BALLWIRE) {
        modelType = MODEL_BALLWIRE;
    } else if(event.GetId() == MENU_ID_MODEL_SPACEFILLING) {
        modelType = MODEL_SPACEFILLING;
    } else if(event.GetId() == MENU_ID_MODEL_RIBBON) {
        modelType = MODEL_RIBBON;
    }
    if (!Tools::IsMenuChecked(GetMolView()->GetMenuBar(), event.GetId())) {
        //cannot deselect itself
        Tools::CheckMenu(GetMolView()->GetMenuBar(), event.GetId(), true);
    }
    if (GetMolView()->GetData()->GetActiveCtrlInst()) {
        GetMolView()->GetData()->GetActiveCtrlInst()->modelType = modelType;
        GetMolView()->RefreshGraphics();
    }
}
void MolViewEventHandler::OnMenuModelShows(wxCommandEvent &event)
{
    // wxMessageBox(wxT("OnMenuMoldelShows"));
    CtrlInst *ctrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    if (ctrlInst) {
        bool isChecked = Tools::IsMenuChecked(GetMolView()->GetMenuBar(), event.GetId());
        if(event.GetId() == MENU_ID_MODEL_SHOW_HYDROGENS) {
            ctrlInst->isShowHydrogens = isChecked;
        } else if(event.GetId() == MENU_ID_MODEL_SHOW_GLOBAL_LABELS) {
            ctrlInst->isShowGlobalLabels = isChecked;
            GetMolView()->GetPopupMenu()->Check(POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS, isChecked);
            if (isChecked && ctrlInst->isShowAtomLabels) {
                Tools::CheckMenu(GetMolView()->GetMenuBar(), MENU_ID_MODEL_SHOW_ATOM_LABELS, false);
                ctrlInst->isShowAtomLabels = false;
            }
        } else if(event.GetId() == MENU_ID_MODEL_SHOW_ATOM_LABELS) {
            ctrlInst->isShowAtomLabels = isChecked;
            GetMolView()->GetPopupMenu()->Check(POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS, isChecked);
            if (isChecked && ctrlInst->isShowGlobalLabels) {
                Tools::CheckMenu(GetMolView()->GetMenuBar(), MENU_ID_MODEL_SHOW_GLOBAL_LABELS, false);
                ctrlInst->isShowGlobalLabels = false;
            }
        } else if(event.GetId() == MENU_ID_MODEL_SHOW_HYDROGEN_BONDS) {
            ctrlInst->isShowHydrogenBonds = isChecked;
        } else if(event.GetId() == MENU_ID_MODEL_SHOW_AXES) {
            ctrlInst->isShowAxes = isChecked;
        }
        GetMolView()->RefreshGraphics();
    }
}
void MolViewEventHandler::OnMenuModelShowLayers(wxCommandEvent &event)
{
    bool isChecked = Tools::IsMenuChecked(GetMolView()->GetMenuBar(), event.GetId());
    int id;
    CtrlInst *ctrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    if(event.GetId() == MENU_ID_MODEL_SHOW_LAYERS_SHOW) {
        for(id = MENU_ID_MODEL_SHOW_LAYERS_HIGH; id <= MENU_ID_MODEL_SHOW_LAYERS_LOW; id++) {
            GetMolView()->GetMenuBar()->Enable(id, isChecked);
        }
        for(id = MENU_ID_MODEL_WIRE; id <= MENU_ID_MODEL_RIBBON; id++) {
            GetMolView()->GetMenuBar()->Enable(id, !isChecked);
        }
    }

    if (ctrlInst) {
        if(event.GetId() == MENU_ID_MODEL_SHOW_LAYERS_SHOW) {
            ctrlInst->isShowLayers = isChecked;
        } else if(event.GetId() == MENU_ID_MODEL_SHOW_LAYERS_HIGH) {
            ctrlInst->isShowLayerHigh = isChecked;
        } else if(event.GetId() == MENU_ID_MODEL_SHOW_LAYERS_MEDIUM) {
            ctrlInst->isShowLayerMedium = isChecked;
        } else if(event.GetId() == MENU_ID_MODEL_SHOW_LAYERS_LOW) {
            ctrlInst->isShowLayerLow = isChecked;
        }
    }
    GetMolView()->RefreshGraphics();
}
void MolViewEventHandler::OnPopupMenuModelShows(wxCommandEvent &event)
{
    CtrlInst *ctrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    bool isChecked = GetMolView()->GetPopupMenu()->IsChecked(event.GetId());
    if (ctrlInst) {
        if(event.GetId() == POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS) {
            Tools::CheckMenu(GetMolView()->GetMenuBar(), MENU_ID_MODEL_SHOW_GLOBAL_LABELS, isChecked);
            ctrlInst->isShowGlobalLabels = isChecked;
            if(isChecked && ctrlInst->isShowAtomLabels) {
                Tools::CheckMenu(GetMolView()->GetMenuBar(), MENU_ID_MODEL_SHOW_ATOM_LABELS, false);
                GetMolView()->GetPopupMenu()->Check(POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS, false);
                ctrlInst->isShowAtomLabels = false;
            }
        } else if(event.GetId() == POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS) {
            Tools::CheckMenu(GetMolView()->GetMenuBar(), MENU_ID_MODEL_SHOW_ATOM_LABELS, isChecked);
            ctrlInst->isShowAtomLabels = isChecked;
            if (isChecked && ctrlInst->isShowGlobalLabels) {
                Tools::CheckMenu(GetMolView()->GetMenuBar(), MENU_ID_MODEL_SHOW_GLOBAL_LABELS, false);
                GetMolView()->GetPopupMenu()->Check(POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS, false);
                ctrlInst->isShowGlobalLabels = false;
            }
        }
        GetMolView()->RefreshGraphics();
    }
}
void MolViewEventHandler::OnCreateTemplate(wxCommandEvent &event)
{
    if (!GetMolView()->GetData()->GetActiveCtrlInst() || GetMolView()->GetData()->GetActiveCtrlInst()->GetRenderingData()->GetAtomCount() <= 0)
        return;
    wxString value = wxT("Default");
    wxTextEntryDialog *dialog = new wxTextEntryDialog(GetMolView(), wxT("Please enter the template name"), wxT("Template name"), value, wxOK | wxCANCEL);
    while (dialog->ShowModal() == wxID_OK) {
        value = dialog->GetValue();
        if (AuiSharedWidgets::Get()->GetElemPnl()->AddCustomTemplate(value, GetMolView()->GetData()->GetActiveCtrlInst()->GetRenderingData()->GetAtomBondArray())) {
            break;
        }
        delete dialog;
        dialog = new wxTextEntryDialog(GetMolView(), wxT("Please enter another template name"), wxT("Template name"), value + wxT("1"), wxOK | wxCANCEL);
    }
    delete dialog;
}
void MolViewEventHandler::OnDeleteTemplate(wxCommandEvent &event)
{
    wxString tempName = AuiSharedWidgets::Get()->GetElemPnl()->DeleteSelectedCustomTemplate(true);
    if(tempName.IsEmpty()) {
        wxMessageBox(wxT("Please select a template first."));
    } else {
        wxMessageDialog dialog(GetMolView(), wxT("Are you sure to delete the selected template: ") + tempName, wxT("Delete template"),
                               wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_INFORMATION);
        if(dialog.ShowModal() == wxID_YES) {
            AuiSharedWidgets::Get()->GetElemPnl()->DeleteSelectedCustomTemplate(false);
        }
    }
}
void MolViewEventHandler::OnPopupReset(wxCommandEvent &event)
{
    CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    if(pCtrlInst) pCtrlInst->ResetGLPositions();
    GetMolView()->RefreshGraphics();
}
void MolViewEventHandler::OnEditClear(wxCommandEvent &event)
{
    CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    if(pCtrlInst && pCtrlInst->GetRenderingData()->GetAtomCount() > 0) {
        pCtrlInst->Empty();
        pCtrlInst->SetDirty(true);
        UpdateMenuToolExclusive(false, MENU_ID_BUILD_ADD_FRAGMENT, true);
        SetActionType(ACTION_ADD_FRAGMENT, BP_NULL_VALUE);
        GetMolView()->EnableMenuToolCtrlInst();
    }
}
//--------------------added by xia-----------------------------
void MolViewEventHandler::OnEditUndo(wxCommandEvent &event)
{
    CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    pCtrlInst->UndoMolecule();
    GetMolView()->EnableMenuToolCtrlInst();
    GetMolView()->RefreshGraphics();

}
void MolViewEventHandler::OnEditRedo(wxCommandEvent &event)
{
    CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    pCtrlInst->RedoMolecule();
    GetMolView()->EnableMenuToolCtrlInst();
    GetMolView()->RefreshGraphics();

}
//----------------------------------------------------
void MolViewEventHandler::OnEditAtomList(wxCommandEvent &event)
{
    CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    if(pCtrlInst != NULL) {
        AtomListFrm *pAtomList = pCtrlInst->GetAtomListFrm(false);
        if(pAtomList != NULL) {
            pAtomList->Raise();
        } else {
            pAtomList = pCtrlInst->GetAtomListFrm(true);
            pAtomList->InitTable(GetMolView(), pCtrlInst);
            pAtomList->UpdateFrameTitle(GetMolView()->GetProjectTitle());

            if(GetWinMenu() != NULL) {
                int count = MENU_ID_WIN_START + GetMolView()->GetData()->GetCtrlInstCount();
                for (int i = MENU_ID_WIN_START; i < count; i++) {
                    if(GetWinMenu()->IsChecked(i)) {
                        pAtomList->UpdateFrameTitle(GetWinMenu()->GetLabel(i));
                    }
                }
            }

            pAtomList->Show(true);
        }
    }
}
//--------------------grp
#include "wx/colordlg.h"
#include "uiprops.h"
void ChangeColor::OnChangeBackgroundColor(wxCommandEvent &event)
{   
//-----------------
    wxColour oldColor = GetUIProps()->openGLProps.mainBgColor.GetColor();
    colour = wxGetColourFromUser(this, oldColor, wxT("Background Color"));
    if (colour.IsOk()) {
        GetUIProps()->openGLProps.mainBgColor.SetColor(colour);
        GetUIProps()->openGLProps.previewBgColor.SetColor(colour);

        GetUIProps()->SaveColour(colour);
        if (MolViewEventHandler::Get()->GetMolView()) {
            AuiSharedWidgets::Get()->GetElemPnl()->RefreshGraphics();
            MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        }
        // bgShow->SetBackgroundColour(colour);
        // bgShow->ClearBackground();
    }
}
//----------------grp
void MolViewEventHandler::OnCalcSymmetry(wxCommandEvent &event)
{
    DoCalcSymmetry();
}
void MolViewEventHandler::OnCalcMMOptimization(wxCommandEvent &event)
{
    DoCalcMMOptimization();
}
//begin : hurukun : 06/11/2009
void MolViewEventHandler::OnCalcMopacOptimization(wxCommandEvent &event)
{
    MOPAC_CHECK mopacCheck = EnvSettingsData::Get()->CheckMopac();
    if(mopacCheck != MOPAC_OK)
        wxMessageBox(wxT("Unable to run MOPAC. Please check your Environment Settings."));
    else
        DoCalcMopacOptimization();
}
//end   : hurukun : 06/11/2009
void MolViewEventHandler::DoCalcMMOptimization(void)
{
    wxString outputFileName, inputFileName, tempFileName, tempFileDir, dirName, paraFileName;
    wxString procName, execProcName, outputResultFileName, errorResultFileName;
    CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    if (pCtrlInst == NULL)
        return;
    RenderingData *renderingData = pCtrlInst ->GetRenderingData();

    if(renderingData->GetAtomCount() <= 0) {
        Tools::ShowStatus(wxT("Please build a molecule at first!"));
        return;
    }

    printf("This part is not finished. A new MM optimizer?");
    return;
    /*
            SetActionType(ACTION_MM, BP_NULL_VALUE);
    //        wxMessageBox(_("do Begin"));
            //tempFileDir = EnvUtil::GetWorkDir() + wxT("/modules/mm");
            tempFileDir=Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
        //FileUtil::CreateDirectory(tempFileDir);
        //FileUtil::RemoveDirectory(tempFileDir, false);
        inputFileName = tempFileDir + wxT("MM_INPUT.sim");
    //        wxMessageBox(_("Save File"));
        MolFile::SaveFile(inputFileName, pCtrlInst->GetRenderingData(), false);
            pCtrlInst->SetDirty(false);
        outputFileName = tempFileDir + wxT("MM_OUTPUT.sim");
        tempFileName = tempFileDir + wxT("MM_TEMP");

        dirName = wxT("modules/mm");
        paraFileName = FileUtil::CheckFile(dirName, wxT("PARAMS.MMFF"), true);
        if (StringUtil::IsEmptyString(paraFileName)) {
            return;
        }
        procName = EnvUtil::GetProgramName(wxT("MMFF_MINE"));
        execProcName = FileUtil::CheckFile(dirName, procName, true);
        if (StringUtil::IsEmptyString(execProcName)) {
            return;
        }
        execProcName += wxT(" ") + tempFileName + wxT(" ")+inputFileName + wxT(" ") + outputFileName + wxT(" ") + paraFileName ;



    //wxMessageBox(execProcName);



        outputResultFileName = tempFileDir + wxT("MM_EXEC.log");
        errorResultFileName = tempFileDir + wxT("MM_EXEC_ERROR.log");

            //begin : hurukun : 13/11/2009
            m_strModualName=wxT("MM");
            //end   : hurukun : 13/11/2009
    //        wxMessageBox(execProcName);
            AuiSharedWidgets::Get()->GetProgressBarPnl()->RunProcess(execProcName);
            //GetUIData()->GetActiveCtrlInst()->SaveHistroyFile();

        //if(Tools::ExecModule(execProcName, outputFileName, errorResultFileName)) {
            //this->Raise();
      //  }
      */
}
//begin : hurukun : 06/11/2009
void MolViewEventHandler::DoCalcMopacOptimization(void)
{
    wxString sInputFileName, mInputFileName, tempFileDir, dirName, paraMethod, paraMulti, paraOptions;
    long        ncharge;
    wxString execProcName = EnvUtil::GetMopacDir();
    wxString msg = wxEmptyString;
    if(execProcName.IsEmpty())//To check if the mopac programme is installed;
    {
        msg = wxT("Please install the mopac programme and set full-path in environment setting!");
        ::wxMessageBox(msg, wxT("ERROR"));
        return;
    }
    //begin: hurukun : 2010/3/2
    if(GetMolView() == NULL)
        return;
    /*    SimuProject* pProject=Manager::Get()->GetProjectManager()->GetProject(GetMolView()->GetProjectId());
        if(pProject==NULL)
            return;
        if(pProject->Save()==false)
            return;
    */    //end : hurukun : 2010/3/2
    CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
    if (pCtrlInst == NULL)
        return;
    RenderingData *renderingData = pCtrlInst ->GetRenderingData();

    if(renderingData->GetAtomCount() <= 0) {
        Tools::ShowStatus(wxT("Please build a molecule at first!"));
        return;
    }
    SetActionType(ACTION_MOPAC, BP_NULL_VALUE);
    CMopacParaDlg        paraDlg(NULL);
    if(paraDlg.ShowModal() == wxID_OK) {
        paraMethod = paraDlg.GetMethod();
        paraMulti = paraDlg.GetMultiplicity();
        ncharge = paraDlg.GetCharge();
        paraOptions = paraDlg.GetOptions();
    } else { //no parameter,no mopac caculation.
        SetActionType(ACTION_VIEW, BP_NULL_VALUE);
        return;
    }

    //        tempFileDir = EnvUtil::GetWorkDir() + platform::PathSep()+wxT("modules")+platform::PathSep()+wxT("mopac");
    tempFileDir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
    //   FileUtil::CreateDirectory(tempFileDir);
    //   FileUtil::RemoveDirectory(tempFileDir, false);
    sInputFileName = tempFileDir + wxT("MOPAC_INPUT.sim");
    MolFile::SaveFile(sInputFileName, pCtrlInst->GetRenderingData(), false);
    mInputFileName = tempFileDir + wxT("MOPAC.dat");
    // Convert the form of input file.
    //wxMessageBox(sInputFileName);
    //wxMessageBox(mInputFileName);
    //wxMessageBox(paraMethod);
    //wxMessageBox(paraMulti);
    //wxMessageBox(wxString::Format(wxT("%d"),ncharge));
    //wxMessageBox(execProcName);
    msg = MopacFile::SimuToMopac(sInputFileName, mInputFileName, paraMethod, paraMulti, paraOptions, ncharge);
    if(!msg.IsEmpty())
    {
        wxMessageBox(msg, wxT("ERROR"));
        return;
    }
    //run mopacRunProcess
    wxSetEnv(wxT("LD_LIBRARY_PATH"), EnvSettingsData::Get()->GetMopacLibPath());
    wxString mopacDir = wxPathOnly(EnvSettingsData::Get()->GetMopacPath());
    wxSetEnv(wxT("MOPAC_LICENSE"), "./");
    wxString cwd = wxGetCwd();
    wxSetWorkingDirectory(mopacDir);

    execProcName += wxT(" ") + mInputFileName.BeforeLast(wxT('.'));
    m_strModualName = wxT("Mopac-") + paraMethod;
    //run the mopac programme
    AuiSharedWidgets::Get()->GetProgressBarPnl()->RunProcess(execProcName);
    
    wxSetWorkingDirectory(cwd);

}
//end   : hurukun : 10/11/2009
void MolViewEventHandler::DoCalcSymmetry()
{
    //#ifdef  __WINDOWS__
    if(Manager::Get()->GetProjectManager()->GetActiveProject()->GetType() != PROJECT_MOLCAS) {
        CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
        if (pCtrlInst == NULL)
            return;
        RenderingData *renderingData = pCtrlInst ->GetRenderingData();
        if (renderingData->GetAtomCount() <= 0) {
            Tools::ShowStatus(wxT("Please build a molecule at first!"));
            return;
        }
        //    wxString tempFileDir = EnvUtil::GetWorkDir() + wxT("/modules/sy");
        //    FileUtil::CreateDirectory(tempFileDir);
        //    FileUtil::RemoveDirectory(tempFileDir, false);
        wxString tempFileDir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
        wxString inputFileName = tempFileDir + wxT("SY_INPUT.sim");
        MolFile::SaveFile(inputFileName, pCtrlInst->GetRenderingData(), false);
        wxString outputFileName = tempFileDir + wxT("SY_OUTPUT.txt");

        wxString procName = EnvUtil::GetProgramName(wxT("symmetry"));
        wxString execProcName = FileUtil::CheckFile(wxT("modules/sy"), procName, true);
        if (StringUtil::IsEmptyString(execProcName)) {
            return;
        }
        execProcName += wxT(" \"") + inputFileName + wxT("\" \"") + outputFileName + wxT("\"");
        wxString outputResultFileName = tempFileDir + wxT("SY_EXEC.log");
        wxString errorResultFileName = tempFileDir + wxT("SY_EXEC_ERROR.log");

        if(Tools::ExecModule(execProcName, outputResultFileName, errorResultFileName)) {
            wxArrayString result = FileUtil::LoadStringLineFile(outputFileName);
            if (result.GetCount() > 0 ) {
                Tools::ShowStatus(result[0].Trim(), 2);
            }
        }
    } else {
        //#else
        if(EnvUtil::GetMolcasGVDir().IsEmpty()) {
            wxMessageBox(wxT("Can't find molcas-gv, please install it at first."));
            return;
        }
        MolView        *pView = (MolView *)(Manager::Get()->GetViewManager()->GetActiveView());
        if(pView == NULL)
            return;
        if(pView->GetData() == NULL)
            return;
        CtrlInst *pCtrlInst = pView->GetData()->GetActiveCtrlInst();
        //wxMessageBox(wxT("OK"));
        if (pCtrlInst == NULL)
            return;
        RenderingData *renderingData = pCtrlInst ->GetRenderingData();
        if (renderingData->GetAtomCount() <= 0) {
            Tools::ShowStatus(wxT("Please build a molecule at first!"));
            return;
        }
        //    wxString tempFileDir = EnvUtil::GetWorkDir() + platform::PathSep()+wxT("modules")+platform::PathSep()+wxT("sy");
        wxString tempFileDir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
        wxString inputFileName = wxGetCwd() + wxT("/findsym.inp");
        if(wxFileExists(inputFileName))
            wxRemoveFile(inputFileName);
        XYZFile::SaveFile(inputFileName, pCtrlInst->GetRenderingData()->GetAtomBondArray(), false);
        wxString outputFileName = tempFileDir + platform::PathSep() + wxT("findsym.out");
        wxString errorResultFileName = tempFileDir + platform::PathSep() + wxT("findsym.err");

        wxString        gvFile = EnvUtil::GetMolcasGVDir();
        if(gvFile.IsEmpty()) {
            wxMessageBox(wxT("Please install gv and modify the settings at first."));
            return ;
        }

        wxString execProcName = gvFile + wxT(" --findsym");

        if(Tools::ExecModule(execProcName, outputFileName, errorResultFileName) == true) {
            wxCopyFile(wxGetCwd() + wxT("/findsym.out"), outputFileName);
            //       wxRemoveFile(EnvUtil::GetBaseDir() +wxT("/findsym.out"));
            wxArrayString result = FileUtil::LoadStringLineFile(wxGetCwd() + wxT("/findsym.out"));
            //        wxRemoveFile(EnvUtil::GetBaseDir() +wxT("/findsym.out"));
            //wxMessageBox(execProcName);
            if (result.GetCount() > 0 ) {
                Tools::ShowStatus(wxT("Symmetry: ") + result[0].AfterFirst(wxT(' ')).BeforeFirst(wxT(' ')), 2);
                //        pCtrlInst->isShowSymmetry=true;
            }
        }
        //    wxRemoveFile(inputFileName);
    }
    //#endif
}

void MolViewEventHandler::OnUpdateProgress(int status)
{
    wxString tempFileDir, outputFileName, msg;
    //        tempFileDir.Printf(wxT("%d"),status);
    //        wxMessageBox(tempFileDir);
    //begin : hurukun : 13/11/2009
    //                tempFileDir = EnvUtil::GetWorkDir() + wxT("/modules/mm");
    //                outputFileName = tempFileDir + wxT("/MM_OUTPUT.sim");

    wxString sInputFileName, mOutputFileName, sOutputFileName;

    if(!m_strModualName.Cmp(wxT("MM")))
    {
        //                tempFileDir = EnvUtil::GetWorkDir() + platform::PathSep()+wxT("modules")+platform::PathSep()+wxT("mm");
        tempFileDir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
        outputFileName = tempFileDir + wxT("MM_OUTPUT.sim");
    } else if(m_strModualName.Left(5).IsSameAs(wxT("Mopac")))
    {
        //                tempFileDir = EnvUtil::GetWorkDir() + platform::PathSep()+wxT("modules")+platform::PathSep()+wxT("mopac");
        tempFileDir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
        //wxMessageBox(tempFileDir);
        outputFileName = tempFileDir +  wxT("MOPAC_OUTPUT.sim");
        sInputFileName = tempFileDir + wxT("MOPAC_INPUT.sim");
        mOutputFileName = tempFileDir + wxT("MOPAC.out");
        // std::cout << "MolViewEventHandler::OnUpdateProgress " << mOutputFileName << std::endl;
        if(!wxFile::Exists(mOutputFileName))
            mOutputFileName.Replace(_T(".out"), _T(".OUT"));
        sOutputFileName = tempFileDir + wxT("MOPAC_OUTPUT.sim");
        //Convert the form of output file from mopac to simu.
        //wxMessageBox(wxT("Please check if the mopac.out exist"));
        msg = MopacFile::MopacToSimu(sInputFileName, mOutputFileName, sOutputFileName);
        if(!msg.IsEmpty())
        {
            wxMessageBox(msg, wxT("ERROR::"));
            if(wxFile::Exists(mOutputFileName))
            {
                DlgOutput *pdlg = new DlgOutput(Manager::Get()->GetAppWindow(), wxID_ANY);
                pdlg->AppendFile(mOutputFileName, mOutputFileName);
                pdlg->SetLabel(mOutputFileName);
                pdlg->Show(true);
                pdlg->ScrollToBottom();
            }
            SetActionType(ACTION_VIEW, BP_NULL_VALUE);
            return;
        }
    }
    // wxMessageBox(outputFileName);
    //end   : hurukun : 13/11/2009
    if(wxFileExists(outputFileName))
    {
        //         wxMessageBox(outputFileName);
        CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
        if (pCtrlInst == NULL)
            return;
        if(m_strModualName.IsSameAs(wxT("MM")) && pCtrlInst->GetReturn(outputFileName) >= 0)
        {
            RenderingData *renderingData = pCtrlInst ->GetRenderingData();
            //            wxMessageBox(_("Before Empty"));
            renderingData->Empty();
            //        wxMessageBox(outputFileName);
            pCtrlInst->LoadOptFile(outputFileName);
            //
        }
        SimuProject    *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
        if(m_strModualName.Left(5).IsSameAs(wxT("Mopac")) && pProject != NULL)
        {
            wxString    mopacFileName = pProject->GetMopacOutFilePath(m_strModualName);
            if(wxCopyFile(mOutputFileName, mopacFileName))
            {
                RenderingData *renderingData = pCtrlInst ->GetRenderingData();
                //            wxMessageBox(_("Before Empty"));
                renderingData->Empty();
                //        wxMessageBox(outputFileName);
                pCtrlInst->LoadOptFile(outputFileName);
                pProject->AddMopacNode(m_strModualName);
            }
        }
        //wxMessageBox(m_strModualName);
        //
        //                wxMessageBox(_("After Load"));
        //        if(pCtrlInst->GetReturn(outputFileName)==2)                DoCalcMMOptimization();
        if(m_strModualName.Left(5).IsSameAs(wxT("Mopac")))
        {
            SetActionType(ACTION_VIEW, BP_NULL_VALUE);
            pCtrlInst->SetDirty(true);
            GetMolView()->SetMenuToolCtrlInst();
            GetMolView()->RefreshGraphics();
        } else if(m_strModualName.IsSameAs(wxT("MM")) && (pCtrlInst->GetReturn(outputFileName) > 0) && (status != 1))
        {
            DoCalcMMOptimization();
        } else {
            SetActionType(ACTION_VIEW, BP_NULL_VALUE);
            pCtrlInst->SetDirty(true);
            GetMolView()->SetMenuToolCtrlInst();
            GetMolView()->RefreshGraphics();

        }

    }
}

void MolViewEventHandler::DoUpdateMM(void)
{
    wxString tempFileDir, outputFileName, msg;

    wxString sInputFileName, mOutputFileName, sOutputFileName;
    int mmflagold = 0, mmflagnew = 0;
    if(!m_strModualName.Cmp(wxT("MM")))
    {
        //                tempFileDir = EnvUtil::GetWorkDir() + platform::PathSep()+wxT("modules")+platform::PathSep()+wxT("mm");
        tempFileDir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
        outputFileName = tempFileDir + wxT("MM_OUTPUT.sim");
        if(wxFileExists(outputFileName))
        {
            CtrlInst *pCtrlInst = GetMolView()->GetData()->GetActiveCtrlInst();
            if (pCtrlInst == NULL)return;
            mmflagnew = pCtrlInst->GetReturn(outputFileName);
            if(mmflagnew != mmflagold) {
                RenderingData *renderingData = pCtrlInst ->GetRenderingData();
                renderingData->Empty();
                pCtrlInst->LoadOptFile(outputFileName);
                pCtrlInst->SetDirty(true);
                GetMolView()->SetMenuToolCtrlInst();
                GetMolView()->RefreshGraphics();
                mmflagold = mmflagnew;
            }
        }

    }
}
void MolViewEventHandler::OnImportStructure(wxCommandEvent &event)
{
    //wxMessageBox(wxT("MolViewEventHandler::OnImportStructure"));
    SimuProject    *pProject = Manager::Get()->GetProjectManager()->GetActiveProject();
    if(pProject == NULL) {
        wxMessageBox(wxT("Please choose a project first."));;
        return;
    }
    pProject->Import();
    AuiSharedWidgets::Get()->EnableMenuTool(true);
}
