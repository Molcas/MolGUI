/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

// constrain.cpp: implementation of the ConsElem class.
//
//////////////////////////////////////////////////////////////////////
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>
#include "constraindata.h"
#include "envutil.h"

/////////////////////////////////////////////////////////////////////
// const vars
static const wxString CONSTRAIN_BOND_FLAG = wxT("B");
static const wxString CONSTRAIN_ANGLE_FLAG = wxT("A");
static const wxString CONSTRAIN_DIHEDRAL_FLAG = wxT("D");
static const double CONS_NULL_VALUE=-361.0;
static const int STR_CONS_ELEM_ADD_COUNT=1;
static const int STR_CONS_ELEM_BOND_COUNT=CONS_BOND+STR_CONS_ELEM_ADD_COUNT;
static const int STR_CONS_ELEM_ANGLE_COUNT=CONS_ANGLE+STR_CONS_ELEM_ADD_COUNT;
static const int STR_CONS_ELEM_DIHEDRAL_COUNT=CONS_DIHEDRAL+STR_CONS_ELEM_ADD_COUNT;
//////////////////////////////////////////////////////////////////////
//static functions
static wxArrayLong SortConsElemInfos(const wxArrayLong& src,bool order=true)
{//order ��С����Not order,�Ӵ�С
        wxArrayLong result,begin,ends;
        unsigned i;
        bool Reverse=false;
        for(i=0;i< src.GetCount()/2; i++)
        {
                begin.Add(src[i]);
                ends.Add(src[src.GetCount()-i-1]);
        }
        for(i=0 ; i < begin.GetCount() ; i++)
        {
                if(begin[i]==ends[i])
                        continue;
                Reverse=order ? (begin[i] > ends[i]) : (begin[i] < ends[i]);
        }
        if(Reverse)
        {
                for(i=0;i<src.GetCount(); i++)
                {
                        result.Add(src[src.GetCount()-i-1]);
                }
        }
        else
        {
                for(i=0;i<src.GetCount(); i++)
                {
                        result.Add(src[i]);
                }
        }
        return result;
}

static wxString FormatConsElem(const ConsElem& elem)
{
        wxString str=wxEmptyString;
        unsigned i;
        wxArrayLong ids=SortConsElemInfos(elem.elemInfos);
        switch(elem.GetConsType())
        {
        case CONS_BOND:
                str=CONSTRAIN_BOND_FLAG+wxT("(");
                for(i=0;i<ids.GetCount();i++)
                        str+=wxString::Format(wxT("%ld"),ids[i])+wxT(",");
                str=str.Left(str.Len()-1)+wxT(")");
                break;
        case CONS_ANGLE:
                str=CONSTRAIN_ANGLE_FLAG+wxT("(");
                for(i=0;i<ids.GetCount();i++)
                        str+=wxString::Format(wxT("%ld"),ids[i])+wxT(",");
                str=str.Left(str.Len()-1)+wxT(")");
                break;
        case CONS_DIHEDRAL:
                str=CONSTRAIN_DIHEDRAL_FLAG+wxT("(");
                for(i=0;i<ids.GetCount();i++)
                        str+=wxString::Format(wxT("%ld"),ids[i])+wxT(",");
                str=str.Left(str.Len()-1)+wxT(")");
                break;
        default:
                break;
        }
        return str;
}

/*
static bool TestElemStr(const wxString& src)
{
        wxArrayString strArr=wxStringTokenize(src,wxT("(),="),wxTOKEN_STRTOK);
        int count = strArr.GetCount();
        if(count<STR_CONS_ELEM_BOND_COUNT|| count>STR_CONS_ELEM_DIHEDRAL_COUNT)
                return false;
        if(strArr[0].Cmp(CONSTRAIN_BOND_FLAG)==0 && count==STR_CONS_ELEM_BOND_COUNT)
                return true;
        else if(strArr[0].Cmp(CONSTRAIN_ANGLE_FLAG)==0 && count==STR_CONS_ELEM_ANGLE_COUNT)
                return true;
        else if(strArr[0].Cmp(CONSTRAIN_DIHEDRAL_FLAG)==0 && count==STR_CONS_ELEM_DIHEDRAL_COUNT)
                return true;
        return false;
}

*/
static ConsElem ConvertStrToConsElem(const wxString& src)
{
        // wxASSERT_MSG(TestElemStr(src),wxT("The string '")+src+wxT("' can not convert to ConsElem!"));
        ConsElem elem;
        wxString delim=wxT(",=()");
        wxArrayString tokens=wxStringTokenize(src,delim,wxTOKEN_STRTOK);
        long lvalue;
        //double dvalue;
        unsigned i;
        for(i=1;i<tokens.GetCount();i++)
        {
                tokens[i].ToLong(&lvalue);
                elem.elemInfos.Add(lvalue);
        }
        //tokens[tokens.GetCount()-1].ToDouble(&dvalue);
        elem.value=CONS_NULL_VALUE;
        return elem;
}
//////////////////////////////////////////////////////////////////////
// class FreezeAtom
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
FreezeAtom::FreezeAtom()
{
        uniqueId=BP_NULL_VALUE;
}
FreezeAtom::FreezeAtom(const AtomBond &atomBond)
{
        uniqueId=atomBond.atom.uniqueId;
        pos=atomBond.atom.pos;
}

FreezeAtom::FreezeAtom(const ChemAtom &chemAtom)
{
        uniqueId=chemAtom.uniqueId;
        pos=chemAtom.pos;
}
FreezeAtom& FreezeAtom::operator =(const AtomBond &atomBond)
{
        uniqueId=atomBond.atom.uniqueId;
        pos=atomBond.atom.pos;
        return *this;
}

FreezeAtom& FreezeAtom::operator =(const ChemAtom &chemAtom)
{
        uniqueId=chemAtom.uniqueId;
        pos=chemAtom.pos;
        return *this;
}
//////////////////////////////////////////////////////////////////////
// class ConsElem
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ConsElem::ConsElem()
{
        value=CONS_NULL_VALUE;
}

ConsElem::ConsElem(wxArrayLong infos,double val)
{
        WX_APPEND_ARRAY(elemInfos,infos);
        value=val;
}
ConsElem::ConsElem(long info1,long info2,double length)
{
        elemInfos.Add(info1);
        elemInfos.Add(info2);
        value=length;
}
ConsElem::ConsElem(long info1,long info2,long info3,double angle)
{
        elemInfos.Add(info1);
        elemInfos.Add(info2);
        elemInfos.Add(info3);
        value=angle;
}
ConsElem::ConsElem(long info1,long info2,long info3,long info4,double dihedral)
{
        elemInfos.Add(info1);
        elemInfos.Add(info2);
        elemInfos.Add(info3);
        elemInfos.Add(info4);
        value=dihedral;
}

ConsElem::~ConsElem()
{
        elemInfos.Clear();
        value=CONS_NULL_VALUE;
}

wxString ConsElem::ToString()
{
        return FormatConsElem(*this);
}


ConsType ConsElem::GetConsType()const
{
        if(elemInfos.GetCount()>=2 && elemInfos.GetCount()<=4)
                return (ConsType)elemInfos.GetCount();
        else
                return CONS_NULL;
}

ConsElem& ConsElem::operator =(const wxString &consInfo)
{
        ConsElem ce=ConvertStrToConsElem(consInfo);
        elemInfos.Clear();
        elemInfos=ce.elemInfos;
        value=ce.value;
        //wxMessageBox(wxT("operator =(const wxString &consInfo):")+consInfo+wxT("\n")+ce.ToString()+wxT("\n")+ToString());
        return *this;
}

ConsElem& ConsElem::operator =(const ConsElem &ce)
{
        elemInfos.Clear();
        elemInfos=ce.elemInfos;
        value=ce.value;
        return *this;
}

bool ConsElem::operator ==(const ConsElem &ce) const
{
        if(elemInfos.GetCount()!=ce.elemInfos.GetCount())
                return false;
        unsigned i;
        bool Order=true,Reverse=true;
        for(i=0; i < elemInfos.GetCount() ;i++)//˳��Ƚ�
        {
                Order = Order && (elemInfos[i]==ce.elemInfos[i]);
        }
        for(i=0; i < elemInfos.GetCount() ;i++)//˳��Ƚ�
        {
                Reverse = Reverse && (elemInfos[i]==ce.elemInfos[elemInfos.GetCount()-i-1]);
        }
        return (Order||Reverse);
}

bool ConsElem::operator !=(const ConsElem &ce) const
{
        if(elemInfos.GetCount()!=ce.elemInfos.GetCount())
                return true;
        unsigned i;
        bool Order=true,Reverse=true;
        for(i=0; i < elemInfos.GetCount() ;i++)//˳��Ƚ�
        {
                Order = Order && (elemInfos[i]==ce.elemInfos[i]);
        }
        for(i=0; i < elemInfos.GetCount() ;i++)//˳��Ƚ�
        {
                Reverse = Reverse && (elemInfos[i]==ce.elemInfos[elemInfos.GetCount()-i-1]);
        }
        return (!Order && !Reverse);
}
////////////////////////////////////////////////////////////////////////
//class ConstrainData
////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
ConstrainData::ConstrainData()
{

}

ConstrainData::~ConstrainData()
{

}

void ConstrainData::UpdateConstrainList(AtomBondArray &chemArray)
{
//wxMessageBox(wxT("ConstrainData::UpdateConstrainList"));
        if(lengthHash.size()>0)
        {
                UpdateConstrainList(chemArray,CONS_BOND);
//wxMessageBox(wxT("UpdateConstrainAngleList"));
        }
        if(angleHash.size()>0)
        {
                UpdateConstrainList(chemArray,CONS_ANGLE);
//wxMessageBox(wxT("UpdateConstrainDihedralList"));
        }
        if(dihedralHash.size()>0)
        {
                UpdateConstrainList(chemArray,CONS_DIHEDRAL);
//wxMessageBox(wxT("UpdateConstrainDihedralList"));
        }
//wxMessageBox(wxT("ConstrainData::UpdateConstrainList Finish"));
}

void ConstrainData::UpdateConstrainList(AtomBondArray &chemArray,ConsType type)
{
        StrDHash::iterator itStart,itEnd;
        wxSetLong chemSet;
        StrDHash tempHash;
        ConsElem ce;
        unsigned int index,elemIndex;
        bool exist;
        switch(type)
        {
        case CONS_BOND:
                itStart=lengthHash.begin();
                itEnd=lengthHash.end();
                break;
        case CONS_ANGLE:
                itStart=angleHash.begin();
                itEnd=angleHash.end();
                break;
        case CONS_DIHEDRAL:
                itStart=dihedralHash.begin();
                itEnd=dihedralHash.end();
                break;
        default:
                return;
        }
        for(index=0;index<chemArray.GetCount();index++)
        {
                chemSet.insert(chemArray[index].atom.uniqueId);
        }
        while(itStart!=itEnd)
        {
                ce=itStart->first;
                exist=true;
                for(elemIndex=0;elemIndex<ce.elemInfos.GetCount();elemIndex++)
                {
                        if(chemSet.find(ce.elemInfos[elemIndex])==chemSet.end())
                        {
                                exist=false;
                                break;
                        }
                }
                if(exist)
                        tempHash[itStart->first]=itStart->second;
                itStart++;
        }
        switch(type)
        {
        case CONS_BOND:
                lengthHash.clear();
                lengthHash=tempHash;
                break;
        case CONS_ANGLE:
                angleHash.clear();
                angleHash=tempHash;
                break;
        case CONS_DIHEDRAL:
                dihedralHash.clear();
                dihedralHash=tempHash;
                break;
        default:
                return;
        }
}

bool ConstrainData::AppendConstrainData(ConsElem ce)
{
        //wxMessageBox(wxT("Append Constrain Data:")+ce.ToString());
        switch(ce.GetConsType())
        {
        case CONS_BOND:
                lengthHash[ce.ToString()]=ce.value;
                break;
        case CONS_ANGLE:
                angleHash[ce.ToString()]=ce.value;
                break;
        case CONS_DIHEDRAL:
                dihedralHash[ce.ToString()]=ce.value;
                break;
        default:
                return false;
        }
        return true;
}

bool ConstrainData::HasConstrainData(ConsElem &ce) const
{
        switch(ce.GetConsType())
        {
        case CONS_BOND:
                return (lengthHash.find(ce.ToString())!=lengthHash.end());
        case CONS_ANGLE:
                return (angleHash.find(ce.ToString())!=angleHash.end());
        case CONS_DIHEDRAL:
                return (dihedralHash.find(ce.ToString())!=dihedralHash.end());
        default:
                return false;
        }
}

bool ConstrainData::RemoveConstrainData(ConsElem &ce)
{
        switch(ce.GetConsType())
        {
        case CONS_BOND:
                lengthHash.erase(ce.ToString());
                break;
        case CONS_ANGLE:
                angleHash.erase(ce.ToString());
                break;
        case CONS_DIHEDRAL:
                dihedralHash.erase(ce.ToString());
                break;
        default:
                return false;
        }
        return true;
}

bool ConstrainData::UpdateConstrainData(ConsElem &ce)
{
        switch(ce.GetConsType())
        {
        case CONS_BOND:
                lengthHash[ce.ToString()]=ce.value;
                break;
        case CONS_ANGLE:
                angleHash[ce.ToString()]=ce.value;
                break;
        case CONS_DIHEDRAL:
                dihedralHash[ce.ToString()]=ce.value;
                break;
        default:
                return false;
        }
        return true;
}

void ConstrainData::ClearAllData()
{
        posHash.clear();
        angleHash.clear();
        lengthHash.clear();
        dihedralHash.clear();
}

wxArrayString ConstrainData::GetConstrainLengthArray(AtomBondArray &chemArray, bool useTab,bool withType)//  const
{
        return GetConstrainDataArray(chemArray,CONS_BOND,useTab,withType);
}

wxArrayString ConstrainData::GetConstrainAngleArray(AtomBondArray &chemArray, bool useTab,bool withType)//  const
{
        return GetConstrainDataArray(chemArray,CONS_ANGLE,useTab,withType);
}

wxArrayString ConstrainData::GetConstrainDihedralArray(AtomBondArray &chemArray, bool useTab,bool withType)//  const
{
        return GetConstrainDataArray(chemArray,CONS_DIHEDRAL,useTab,withType);
}

void ConstrainData::DumpData(AtomBondArray &chemArray)
{
    wxFileOutputStream fos(EnvUtil::GetLogDir() + wxT("/constraindata.txt"));
    wxTextOutputStream tos(fos);
        //StrDHash::iterator vhiDih=dihedralHash.begin();
        //StrDHash::iterator vhiAng=angleHash.begin();
        //StrDHash::iterator vhiLen=lengthHash.begin();
        unsigned int i;
        wxArrayString lenArray=GetConstrainLengthArray(chemArray,true,false);
        wxArrayString angArray=GetConstrainAngleArray(chemArray,true,false);
        wxArrayString dihArray=GetConstrainDihedralArray(chemArray,true,false);
        for(i = 0; i < lenArray.GetCount(); i++)
        {
                tos << CONSTRAIN_BOND_FLAG
                        << lenArray[i]
                        << endl;
        }
        for(i = 0; i < angArray.GetCount(); i++)
        {
                tos << CONSTRAIN_ANGLE_FLAG
                        << angArray[i]
                        << endl;
        }
        for(i = 0; i < dihArray.GetCount(); i++)
        {
                tos << CONSTRAIN_DIHEDRAL_FLAG
                        << dihArray[i]
                        << endl;
        }
}

wxString ConstrainData::ToString(AtomBondArray &chemArray) // const
{
        unsigned int i;
        wxArrayString lenArray=GetConstrainLengthArray(chemArray,true,true);
        wxArrayString angArray=GetConstrainAngleArray(chemArray,true,true);
        wxArrayString dihArray=GetConstrainDihedralArray(chemArray,true,true);
        wxString retStr=wxEmptyString;
        for(i = 0; i < lenArray.GetCount(); i++)
        {
                retStr +=CONSTRAIN_BOND_FLAG + lenArray[i] + wxT("\n");
        }
        for(i = 0; i < angArray.GetCount(); i++)
        {
                retStr += CONSTRAIN_ANGLE_FLAG + angArray[i] + wxT("\n");
        }
        for(i = 0; i < dihArray.GetCount(); i++)
        {
                retStr += CONSTRAIN_DIHEDRAL_FLAG + dihArray[i] + wxT("\n");
        }
        return retStr;
}
/*
wxString ConstrainData::OutputForMolcasConstrain(AtomBondArray &chemArray)
{
        unsigned int i,j;
        wxArrayString lenArray=GetConstrainLengthArray(chemArray,true);
        wxArrayString angArray=GetConstrainAngleArray(chemArray,true);
        wxArrayString dihArray=GetConstrainDihedralArray(chemArray,true);
        wxArrayString tokens;
        wxString endl=wxT("\n");
        wxString retStr=MOLCAS_FILE_CONSTRAIN_BEGIN + endl;
        for(i=0;i< lenArray.GetCount();i++)
        {
                tokens=wxStringTokenize(lenArray[i],wxT("\t"),wxTOKEN_STRTOK);
                retStr += MOLCAS_FILE_CONSTRAIN_BOND_PREFIX
                        + wxString::Format(wxT("%d"),i+1)
                        + MOLCAS_FILE_CONSTRAIN_OPERATOR
                        + MOLCAS_FILE_CONSTRAIN_BOND_FLAG;
                for(j=0;j<2;j++)
                        retStr += wxT(" ")+tokens[j];
                retStr += endl;
        }
        for(i=0;i< angArray.GetCount();i++)
        {
                tokens=wxStringTokenize(angArray[i],wxT("\t"),wxTOKEN_STRTOK);
                retStr += MOLCAS_FILE_CONSTRAIN_ANGLE_PREFIX
                        + wxString::Format(wxT("%d"),i+1)
                        + MOLCAS_FILE_CONSTRAIN_OPERATOR
                        + MOLCAS_FILE_CONSTRAIN_ANGLE_FLAG;
                for(j=0;j<3;j++)
                        retStr +=wxT(" ")+tokens[j];
                retStr +=endl;
        }
        for(i=0;i< dihArray.GetCount(); i++)
        {
                tokens=wxStringTokenize(dihArray[i],wxT("\t"),wxTOKEN_STRTOK);
                retStr += MOLCAS_FILE_CONSTRAIN_DIHEDRAL_PREFIX
                        + wxString::Format(wxT("%d"),i+1)
                        + MOLCAS_FILE_CONSTRAIN_OPERATOR
                        + MOLCAS_FILE_CONSTRAIN_DIHEDRAL_FLAG;
                for(j=0;j<4;j++)
                        retStr += wxT(" ")+tokens[j];
                retStr += endl;
        }
        retStr += MOLCAS_FILE_CONSTRAIN_VALUE + endl;
        for(i=0;i< lenArray.GetCount();i++)
        {
                tokens=wxStringTokenize(lenArray[i],wxT("\t"),wxTOKEN_STRTOK);
                retStr += MOLCAS_FILE_CONSTRAIN_BOND_PREFIX
                        +wxString::Format(wxT("%d"),i+1)
                        + MOLCAS_FILE_CONSTRAIN_OPERATOR
                        + tokens[2]
                        + wxT(" ")
                        + MOLCAS_FILE_CONSTRAIN_BOND_MEASURE
                        + endl;
        }
        for(i=0;i< angArray.GetCount();i++)
        {
                tokens=wxStringTokenize(angArray[i],wxT("\t"),wxTOKEN_STRTOK);
                retStr += MOLCAS_FILE_CONSTRAIN_ANGLE_PREFIX
                        + wxString::Format(wxT("%d"),i+1)
                        + MOLCAS_FILE_CONSTRAIN_OPERATOR
                        + tokens[3]
                        + wxT(" ")
                        + MOLCAS_FILE_CONSTRAIN_ANGLE_MEASURE
                        + endl;
        }
        for(i=0;i< dihArray.GetCount(); i++)
        {
                tokens=wxStringTokenize(dihArray[i],wxT("\t"),wxTOKEN_STRTOK);
                retStr += MOLCAS_FILE_CONSTRAIN_DIHEDRAL_PREFIX
                        + wxString::Format(wxT("%d"),i+1)
                        + MOLCAS_FILE_CONSTRAIN_OPERATOR
                        + tokens[4]
                        + wxT(" ")
                        + MOLCAS_FILE_CONSTRAIN_DIHEDRAL_MEASURE
                        + endl;
        }
        retStr += MOLCAS_FILE_CONSTRAIN_END +endl;
        return retStr;
}
*/
wxArrayString ConstrainData::GetConstrainDataArray(AtomBondArray &chemArray, ConsType consType, bool useTab,bool withType)
{
        StrDHash::iterator itStart,itEnd;
        LngLHash chemHash;
        ConsElem ce;
        unsigned int index,elemIndex;
        bool exist;
        wxString format,tempStr,type,prefix,valueformat=wxT("\t%.4f");
        wxArrayString retArr;
        switch(consType)
        {
        case CONS_BOND:
                itStart=lengthHash.begin();
                itEnd=lengthHash.end();
                break;
        case CONS_ANGLE:
                itStart=angleHash.begin();
                itEnd=angleHash.end();
                break;
        case CONS_DIHEDRAL:
                itStart=dihedralHash.begin();
                itEnd=dihedralHash.end();
                break;
        default:
                return retArr;
        }
        if(useTab)
        {
                format=wxT("%d\t");//\t%d\t%.4f");
                prefix=wxT("\t");
        }
        else
        {
                format=wxT("%d ");// %d %.4f");
                prefix=wxT(" ");
        }
        for(index=0;index<chemArray.GetCount();index++)
        {
                chemHash[chemArray[index].atom.uniqueId]=index;
        }
        while(itStart!=itEnd)
        {
                ce=itStart->first;
                //wxMessageBox(wxT("GetArray,ce=itStart->first:\t")+ce.ToString());
                exist=true;
                for(elemIndex=0;elemIndex<ce.elemInfos.GetCount();elemIndex++)
                {
                        if(chemHash.find(ce.elemInfos[elemIndex])==chemHash.end())
                        {
                                exist=false;
                                break;
                        }
                }
                if(exist)
                {
                        tempStr=prefix;
                        for(elemIndex=0;elemIndex<ce.elemInfos.GetCount();elemIndex++)
                        {
                                index=chemHash.find(ce.elemInfos[elemIndex])->second;
                                if(chemArray[index].atom.IsDefaultHydrogen())
                                        type=ELEM_NAMES[ELEM_H-1];
                                else
                                        type=ELEM_NAMES[chemArray[index].atom.elemId-1];
                                if(withType)
                                        tempStr+=type+wxString::Format(format,index+1);
                                else
                                        tempStr+=wxString::Format(format,index+1);
                        }
                        tempStr+=wxString::Format(valueformat,itStart->second);
                        retArr.Add(tempStr);
                        //wxMessageBox(wxT("GetArray, tempStr=")+tempStr);
                }
                itStart++;
        }
        return retArr;
}

void ConstrainData::UpdateFreezeList(AtomBondArray &chemArray)
{
        LngSHash result;
        FreezeAtom fa;
        unsigned i;
        for(i=0; i < chemArray.GetCount() ; i++)
        {
                if(posHash.find(chemArray[i].atom.uniqueId)!=posHash.end())
                {
                        fa.uniqueId=chemArray[i].atom.uniqueId;
                        fa.pos=posHash.find(fa.uniqueId)->second;
                        result[fa.uniqueId]=fa.pos.ToString();
                }
        }
        posHash.clear();
        posHash=result;
        result.clear();
}

void ConstrainData::AppendFreezeData(FreezeAtom &fa)
{
        posHash[fa.uniqueId]=fa.pos.ToString();
}

bool ConstrainData::HasFreezeData(long uniqueId) const
{
        return (posHash.find(uniqueId)!=posHash.end());
}

void ConstrainData::RemoveFreezeData(long uniqueId)
{
        posHash.erase(uniqueId);
}

void ConstrainData::UpdateDataList(AtomBondArray &chemArray)
{
        UpdateConstrainList(chemArray);
        UpdateFreezeList(chemArray);
}

void ConstrainData::UpdateFreezeAtom(FreezeAtom &fa)
{
        posHash[fa.uniqueId]=fa.pos.ToString();
}
