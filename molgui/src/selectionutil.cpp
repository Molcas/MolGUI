/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "selectionutil.h"
#include "chematom.h"
#include "ctrlinst.h"
#include "molviewdata.h"

SelectionUtil::SelectionUtil() {
    m_pCtrlInst = NULL;
}

void SelectionUtil::SetCtrlInst(CtrlInst* pCtrlInst) {
    this->m_pCtrlInst = pCtrlInst;
        ClearAll();
}

CtrlInst* SelectionUtil::GetCtrlInst(void) const {
    return m_pCtrlInst;
}

void SelectionUtil::ClearAll(void) {
        ClearSelection();
    ClearManuSelection();
}

bool SelectionUtil::AddSelectedIndexLimited(int selectedMax, int selectedAtomIndex, bool isSelectTwoAtoms) {
        int i = 0;
        if(selectedAtomIndex < 0) {
                ClearSelection();
        }else {
                if(GetSelectedCount() == selectedMax)
                        ClearSelection();
                   if(isSelectTwoAtoms && GetSelectedCount() >= 1) {
                        if(IsSelected(selectedAtomIndex)) {
                                RemoveSelected(selectedAtomIndex);
                        }else if(GetSelectedCount() == 1) {
                                //should be connected with the first selected atom
                                AtomBond atomBond = m_pCtrlInst->GetRenderingData()->GetAtomBondArray()[GetSelectedIndex(0)];
                                for(i = 0; i < atomBond.bondCount; i++) {
                                        if(atomBond.bonds[i].atomIndex == selectedAtomIndex) {
                                                AddSelectedIndex(selectedAtomIndex, true);
                                                break;
                                        }
                                }
                        }
                }else {
                        AddSelectedIndex(selectedAtomIndex, true);
                }
        }
        return (GetSelectedCount() == selectedMax);
}

bool SelectionUtil::AddSelectedIndex(int selectedAtomIndex, bool isAppend) {
    AtomBondArray& atomBondarray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
    if(selectedAtomIndex >= 0 && selectedAtomIndex < m_pCtrlInst->GetRenderingData()->GetAtomCount()) {
        if(m_intSelAry.Index(selectedAtomIndex) == wxNOT_FOUND
            && ((m_pCtrlInst->IsPreviewCtrlInst() && atomBondarray[selectedAtomIndex].atom.IsDefaultHydrogen())
                || !m_pCtrlInst->IsPreviewCtrlInst())){
            if(!isAppend) {
                ClearSelection();
            }
            m_intSelAry.Add(selectedAtomIndex);
            return true;
        }
    }
    return false;
}

bool SelectionUtil::AddSelectedIndex(int selectedMin, int selectedMax, int selectedAtomIndex[2]) {
        if(selectedAtomIndex[0] < 0 || selectedAtomIndex[1] < 0){
                ClearSelection();
        }else{
                if(GetSelectedCount() == selectedMax){
                        ClearSelection();
                }
                if(!IsSelected(selectedAtomIndex[0]) && !IsSelected(selectedAtomIndex[1])) {
            AddSelectedIndexLimited(selectedMax, selectedAtomIndex[0]);
            AddSelectedIndexLimited(selectedMax, selectedAtomIndex[1]);
                }else if(IsSelected(selectedAtomIndex[0]) && !IsSelected(selectedAtomIndex[1])){
                        AddSelectedIndexLimited(selectedMax, selectedAtomIndex[1]);
                }else if(!IsSelected(selectedAtomIndex[0]) && IsSelected(selectedAtomIndex[1])){
                        AddSelectedIndexLimited(selectedMax, selectedAtomIndex[0]);
                }else{
                        RemoveSelected(selectedAtomIndex[0]);
                        RemoveSelected(selectedAtomIndex[1]);
                }
        }
        return (GetSelectedCount() <= selectedMax && GetSelectedCount() >= selectedMin);
}


bool SelectionUtil::IsSelected(int selectedAtomIndex) const {
    for(unsigned i = 0; i < m_intSelAry.GetCount(); i++) {
        if(m_intSelAry[i] == selectedAtomIndex) {
            return true;
        }
    }
    return false;
}

void SelectionUtil::ClearSelection(void) {
    /*if(m_pCtrlInst) {
        AtomBondArray& atomBondarray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        for(unsigned i = 0; i < m_intSelAry.GetCount(); i++) {
            atomBondarray[m_intSelAry[i]].atom.selected = false;
        }
    }*/
    m_intSelAry.Clear();
}

void SelectionUtil::RemoveSelected(int selectedAtomIndex) {
    //m_intSelAry.Clear();
    unsigned i;
    for(i = 0; i < m_intSelAry.GetCount(); i++) {
        if(m_intSelAry[i] == selectedAtomIndex) {
            m_intSelAry.RemoveAt(i);
            /*if(m_pCtrlInst) {
                m_pCtrlInst->GetRenderingData()->GetAtomBondArray()[selectedAtomIndex].atom.selected = false;
            }*/
            break;
        }
    }
}

int SelectionUtil::GetSelectedCount(void) const{
    return (int)m_intSelAry.GetCount();
}

int SelectionUtil::GetSelectedIndex(int selSequenceNum) const {
    if(GetSelectedCount() <= 0 || GetSelectedCount() <= selSequenceNum) {
        return BP_NULL_VALUE;
    }else {
        return m_intSelAry[selSequenceNum];
    }
}

wxString SelectionUtil::GetSelectionLabel(int selectedMax) {
        wxString label = wxEmptyString;
        for(int i = 0; i < selectedMax; i++) {
                if(i < GetSelectedCount()) {
                        label += Index2DisplayLabel(m_pCtrlInst, m_intSelAry[i]);
                }
                if(i != selectedMax - 1 ) {
                        label += wxT(", ");
                }
        }
        return label;
}

int SelectionUtil::GetAtomCountLessThan(int atomIndex) const {
        int i, count = 0, index = BP_NULL_VALUE;
        for(i = 0; i < GetSelectedCount(); i++) {
                index = GetSelectedIndex(i);
                if(index < atomIndex && index != BP_NULL_VALUE) {
                        count++;
                }
        }
        return count;
}

void SelectionUtil::GetSelectedPositions(double points[][3], int length) const {
        int j = BP_NULL_VALUE;
        AtomBondArray& atomBondarray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        for(int i = 0; i < GetSelectedCount() && i < length; i++) {
                j = GetSelectedIndex(i);
                points[i][0] = atomBondarray[j].atom.pos.x;
                points[i][1] = atomBondarray[j].atom.pos.y;
                points[i][2] = atomBondarray[j].atom.pos.z;
        }
}

int SelectionUtil::GetManuSelectedCount(void) const{
    return (int)m_intManuSelAry.GetCount();
}

bool SelectionUtil::IsManuSelected(int atomIndex) const {
        return m_intManuSelAry.Index(atomIndex) != wxNOT_FOUND;
}

bool SelectionUtil::AddManuSelectedIndex(int selectedAtomIndex, AtomSelectType atomSelectType) {
        unsigned i = 0;
        bool added = false;
        if(atomSelectType == ATOM_SELECT_ALL) {
                ClearManuSelection();
                for(i = 0; i < m_pCtrlInst->GetRenderingData()->GetAtomBondArray().GetCount(); i++) {
                        m_intManuSelAry.Add(i);
                }
                added = true;
        }else if(selectedAtomIndex >= 0 && selectedAtomIndex < m_pCtrlInst->GetRenderingData()->GetAtomCount() && (!IsSelected(selectedAtomIndex) || atomSelectType == ATOM_SELECT_TWO_ATOMS)) {
                int index = m_intManuSelAry.Index(selectedAtomIndex);
        if(index == wxNOT_FOUND){
                        if(atomSelectType == ATOM_SELECT_SINGLE || atomSelectType == ATOM_SELECT_TWO_ATOMS) {
                    m_intManuSelAry.Add(selectedAtomIndex);
                        }else if(atomSelectType == ATOM_SELECT_CONNECTED){
                                AtomBondArray& atomBondarray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
                                short connGroup = atomBondarray[selectedAtomIndex].atom.connGroup;
                                for(i = 0; i < atomBondarray.GetCount(); i++) {
                                        if(connGroup == atomBondarray[i].atom.connGroup) {
                                                if(m_intManuSelAry.Index(i) == wxNOT_FOUND && !IsSelected(i)){
                                                        m_intManuSelAry.Add(i);
                                                }
                                        }
                                }
                        }
            added = true;
        }else {
                        if(atomSelectType == ATOM_SELECT_SINGLE || atomSelectType == ATOM_SELECT_TWO_ATOMS) {
                                m_intManuSelAry.RemoveAt(index);
                        }else if(atomSelectType == ATOM_SELECT_CONNECTED){
                                AtomBondArray& atomBondarray = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
                                short connGroup = atomBondarray[selectedAtomIndex].atom.connGroup;
                                for(i = 0; i < atomBondarray.GetCount(); i++) {
                                        if(connGroup == atomBondarray[i].atom.connGroup) {
                                                index = m_intManuSelAry.Index(i);
                                                if(index != wxNOT_FOUND && !IsSelected(i)){
                                                        m_intManuSelAry.RemoveAt(index);
                                                }
                                        }
                                }
                        }
                }
    }
    return added;
}

void SelectionUtil::ClearManuSelection(void) {
        m_intManuSelAry.Clear();
}

int SelectionUtil::GetManuSelectedIndex(int selSequenceNum) const {
    if(GetManuSelectedCount() <= 0 || GetManuSelectedCount() <= selSequenceNum) {
        return BP_NULL_VALUE;
    }else {
        return m_intManuSelAry[selSequenceNum];
    }
}

int SelectionUtil::GetManuAtomCountLessThan(int atomIndex) const {
        int i, count = 0, index = BP_NULL_VALUE;
        for(i = 0; i < GetManuSelectedCount(); i++) {
                index = GetManuSelectedIndex(i);
                if(index < atomIndex && index != BP_NULL_VALUE) {
                        count++;
                }
        }
        return count;
}


void SelectionUtil::RemoveManuSelected(int atomIndex) {
    for(int i = 0; i < GetManuSelectedCount(); i++) {
                if(GetManuSelectedIndex(i) == atomIndex) {
                        m_intManuSelAry.RemoveAt(i);
                }
        }
}
