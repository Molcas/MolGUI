/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "designbondpnl.h"
#include <wx/statline.h>
#include "tools.h"
#include "molvieweventhandler.h"
#include "envutil.h"  //SONG add the GetMolcasDir function to compile Molcas
#include "resource.h"
#include "dlgjobname.h"

#include <fstream>
#include <string>
#include <vector>
using namespace std;
//////////////////////////////////////////////////////////////////////
int ID_CTRL_BTN_RESET=wxNewId();

BEGIN_EVENT_TABLE(DesignBondPnl, wxPanel)
EVT_TOGGLEBUTTON(wxID_ANY, DesignBondPnl::OnToggleButton)
EVT_BUTTON(ID_CTRL_BTN_RESET, DesignBondPnl::OnReset)
END_EVENT_TABLE()

DesignBondPnl::DesignBondPnl(wxWindow* parent, wxWindowID id, const wxPoint& pos,
                const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name) {
        m_pBtnReset = NULL;
        m_isSelectionChanged = false;
        m_pCtrlInst = NULL;
}


void DesignBondPnl::InitPanel(void) {

        wxBoxSizer* pBsMain = new wxBoxSizer(wxHORIZONTAL);
        this->SetSizer(pBsMain);
    wxImage image;
        wxBitmap bmp;
        int i,height = wxDefaultSize.GetHeight();
        wxBoxSizer* pBsState=new wxBoxSizer(wxVERTICAL);
        m_pLbTitle = new wxStaticText(this, wxID_ANY, wxT("Please select two atoms!"),
                        wxDefaultPosition, wxSize(250, height), wxALIGN_LEFT);
    pBsState->Add(m_pLbTitle,0,wxALL|wxEXPAND,5);
    pBsMain->Add(pBsState,0,wxCENTER|wxEXPAND,5);
    wxGridSizer *pBsBondType = new wxGridSizer(1,  BOND_TYPE_COUNT_NEW+1, 0, 0);

        for(i = 0; i <  BOND_TYPE_COUNT_NEW; i++) {
            //image.LoadFile(CheckImageFile(wxString::Format(wxT("positions/pos%d.png"), i + 1)), wxBITMAP_TYPE_PNG);
            image.LoadFile(EnvUtil::GetPrgResDir()+wxTRUNK_MOL_IMG_PATH+wxString::Format(wxT("types/type%d.png"), i ), wxBITMAP_TYPE_PNG);

            //image.LoadFile(CheckImageFile(wxString::Format(wxT("types/type%d.png"), i+1)), wxBITMAP_TYPE_PNG);
            bmp = wxBitmap(image);
            m_pTglBmpBondTypes[i] = new ToggleBitmapButton(this, CTRL_ID_TGL_BOND_TYPE_START + i, wxDefaultPosition, wxSize(50, 30));
            m_pTglBmpBondTypes[i]->SetBitmapNormal(bmp);
            m_pTglBmpBondTypes[i]->Enable(false);
            pBsBondType->Add(m_pTglBmpBondTypes[i], 0, wxALIGN_CENTER_HORIZONTAL, 5);
        }
    wxStaticLine* line = new wxStaticLine(this, wxID_STATIC, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
    pBsBondType->Add(line, 0, wxALL | wxEXPAND, 5);
    pBsMain->Add(pBsBondType,0,wxCENTER|wxEXPAND,5);
    pBsMain->SetSizeHints(this);
}
/*
void DesignBondPnl::SetMeasureOrConstrain(bool isMeasure) {
        m_isMeasure = isMeasure;
        GetSizer()->Show(m_pCkbMoveSingleAtom, isMeasure);
        if (m_pCtrlInst) {
                GetSizer()->Show(m_pBsPosition,
                                m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() <= 1);
                GetSizer()->Show(m_pBsBond,
                                m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() > 1);
                GetSizer()->Show(m_pStMCType,
                                m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() > 1 || isMeasure);
        }
        GetSizer()->Show(m_pBtnConstrain, !isMeasure);
        GetSizer()->Show(m_pImportBtn, !isMeasure);
        GetSizer()->Show(m_pShowCons, !isMeasure);
        GetSizer()->Show(m_pClearConsBtn, !isMeasure);
        GetSizer()->Show(m_pBtnReset, isMeasure);
        if (!isMeasure) {
                m_pBsBond->Show(m_pStMin, false);
                m_pBsBond->Show(m_pSlider, false);
                m_pBsBond->Show(m_pStMax, false);
        }
        //EnableControls(isMeasure ? ACTION_MEASURE:ACTION_CONSTRAIN);
        Layout();
        Refresh();
}
*/

void DesignBondPnl::SetCtrlInst(CtrlInst* pCtrlInst) {
        m_pCtrlInst = pCtrlInst;
}

void DesignBondPnl::ClearSelection(void) {
        if (m_pCtrlInst) {
                m_pCtrlInst->GetSelectionUtil()->ClearSelection();
        }
        for(int i = 0; i <  BOND_TYPE_COUNT_NEW; i++) {
                m_pTglBmpBondTypes[i]->Enable(false);
        }
    m_pLbTitle->SetLabel(wxT("Please select two atoms!"));
}


void DesignBondPnl::AddSelectedIndex(int selectedAtomIndex) {
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        int selectedMax = pSelUtil->GetSelectedCount() + 1;
        //m_mcLabel = wxEmptyString;

        if (selectedMax > 2 || pSelUtil->IsSelected(selectedAtomIndex)) {
                pSelUtil->ClearSelection();
                selectedMax = 1;
        }

        if (pSelUtil->AddSelectedIndexLimited(selectedMax, selectedAtomIndex)
                        && pSelUtil->GetSelectedCount() ==2) {
                        for(int i = 0; i <  BOND_TYPE_COUNT_NEW; i++) {
                                        m_pTglBmpBondTypes[i]->Enable(true);
                                        m_pTglBmpBondTypes[i]->SetValue(false);
                                }
            wxString msg=wxT("The atoms you have selected:");
            msg+=pSelUtil->GetSelectionLabel(selectedMax);
            m_pLbTitle->SetLabel(msg);

        }else{
            if(pSelUtil->GetSelectedCount()==0){
                m_pLbTitle->SetLabel(wxT("Please select two atoms!"));
            }else{
                wxString msg=wxT("The atoms you have selected:");
                msg+=pSelUtil->GetSelectionLabel(selectedMax);
                m_pLbTitle->SetLabel(msg);
            }
                        for(int i = 0; i <  BOND_TYPE_COUNT_NEW; i++) {
                                        m_pTglBmpBondTypes[i]->Enable(false);
                                }
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

/*
void DesignBondPnl::AddSelectedIndex(int selectedAtomIndex[2]) {
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        int selectedMax = pSelUtil->GetSelectedCount() + 2;
        int selectedMin = 1;
        if (selectedMax > 4) {
                selectedMax = 4;
        }
        bool addSuccess = pSelUtil->AddSelectedIndex(selectedMin, selectedMax,
                        selectedAtomIndex);
        m_mcLabel = wxEmptyString;
        switch (pSelUtil->GetSelectedCount()) {
        case 0:
        case 1:
                m_mcType = m_isMeasure ? (int) MEASURE_POSITION
                                : (int) CONSTRAIN_POSITION;
                //m_mcLabel = m_isMeasure ? wxT("Position(") : wxT("Constraint(");
                m_mcLabel = wxT("Position(");
                break;
        case 2:
                m_mcType = m_isMeasure ? (int) MEASURE_DISTANCE
                                : (int) CONSTRAIN_DISTANCE;
                //m_mcLabel = m_isMeasure ? wxT("Distance(") : wxT("Constraint(");
                m_mcLabel = wxT("Distance(");
                break;
        case 3:
                m_mcType = m_isMeasure ? (int) MEASURE_ANGLE : (int) CONSTRAIN_ANGLE;
                //m_mcLabel = m_isMeasure ? wxT("Angel(") : wxT("Constraint(");
                m_mcLabel = wxT("Angel(");
                break;
        case 4:
                m_mcType = m_isMeasure ? (int) MEASURE_DIHEDRAL
                                : (int) CONSTRAIN_DIHEDRAL;
                //m_mcLabel = m_isMeasure ? wxT("Dihedral(") : wxT("Constraint(");
                m_mcLabel = wxT("Dihedral(");
                break;
        }
        if (addSuccess && pSelUtil->GetSelectedCount() > 0) {
                DoMeasure();
                if (m_isMeasure)
                        m_pBtnReset->Enable(true);
        }
        if (pSelUtil->GetSelectedCount() <= 0) {
                SetPositionEnable(false);
        }
        selectedMax
                        = pSelUtil->GetSelectedCount() > 0 ? pSelUtil->GetSelectedCount()
                                        : 1;
        UpdateMCType(selectedMax);
        SetMeasureOrConstrain(m_isMeasure);
        if (!m_isMeasure && pSelUtil->GetSelectedCount() > 0) {
                UpdateConsFreezeStatus();
                //wxMessageBox(wxString::Format(wxT("m_cfStatus=%d"),m_cfStatus));
                m_pBtnConstrain->SetValue(m_cfStatus);
                EnableEditors(m_cfStatus);
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}


// here we note that the angle is accurate at 10-5 precision
MeasureInfo DesignBondPnl::DoBondChange(float value) {
        double oldValue = BP_NULL_VALUE;
        double newValue = value;
        MeasureInfo mInfo = MEASURE_SUCCESS;
        bool isMoveSingle = IsMoveSingleAtom();
        SelectionUtil* selUtil = m_pCtrlInst->GetSelectionUtil();

        switch (m_mcType) {
        case MEASURE_DISTANCE:
                mInfo = ChangeBondDistance(newValue, selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                isMoveSingle);
                break;
        case MEASURE_ANGLE:
                SetAngleRange(newValue);
                if (newValue < 0.00001) {
                        newValue = 0.00001;
                } else if (newValue > 179.99999 && newValue <= 180.0) {
                        newValue = 179.99999;
                } else if (newValue > 180.0 && newValue < 180.00001) {
                        newValue = 180.00001;
                } else if (newValue > 359.99999) {
                        newValue = 359.99999;
                }

                m_pCtrlInst->GetOldAngleValue(selUtil->GetSelectedIndex(2),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(0),
                                oldValue);
                mInfo = ChangeBondAngle(newValue, oldValue,
                                selUtil->GetSelectedIndex(2), selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                isMoveSingle);
                if (mInfo == MEASURE_SUCCESS) {
                        m_pCtrlInst->SetOldAngleValue(selUtil->GetSelectedIndex(2),
                                        selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(0),
                                        newValue);
                }
                break;
        case MEASURE_DIHEDRAL:
                mInfo = ChangeDihedral(newValue, selUtil->GetSelectedIndex(3),
                                selUtil->GetSelectedIndex(2), selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                isMoveSingle);
                break;
        default:
                break;
        }

        return mInfo;
}

void DesignBondPnl::ChangeBondValueByPageKey(bool isUp) {
        double newValue = 0;
        long longValue = 0;

        if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() <= 1)
                return;
        wxString strValue = m_pTcValue->GetValue();
        strValue.ToDouble(&newValue);

        switch (m_mcType) {
        case MEASURE_DISTANCE:
                newValue += isUp ? 0.1 : -0.1;
                break;
        case MEASURE_ANGLE:
        case MEASURE_DIHEDRAL:
                newValue += isUp ? 1 : -1;
                break;
        default:
                break;
        }
        longValue = (long) (newValue * BP_SLIDER_PRECISION);
        if (longValue < m_pSlider->GetMin()) {
                newValue = m_pSlider->GetMin() / BP_SLIDER_PRECISION;
        } else if (longValue > m_pSlider->GetMax()) {
                newValue = m_pSlider->GetMax() / BP_SLIDER_PRECISION;
        }
        m_pTcValue->SetValue(wxString::Format(wxT("%.3f"), newValue));
        DoTextValueChanged();
}

*/

void DesignBondPnl::OnReset(wxCommandEvent& event) {
/*        double offset = 0;
        if (GetSizer()->IsShown(m_pBsPosition)) {
                SetInitPositionValue(m_initPos);
                if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() == 1) {
                        for (int i = 0; i < 3; i++) {
                                offset
                                                = m_initPos[i]
                                                                - m_pCtrlInst->GetRenderingData()->GetAtomBondArray()[m_pCtrlInst->GetSelectionUtil()->GetSelectedIndex(
                                                                                0)].atom.pos[i];
                                if (IsMoveSingleAtom()) {
                                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                                        m_pCtrlInst->GetSelectionUtil()->GetSelectedIndex(0),
                                                        i, offset);
                                } else {
                                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                                        BP_NULL_VALUE, i, offset);
                                }
                        }
                        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
                        if (!m_isMeasure && m_cfStatus) {
                                AtomPosition pos;
                                m_pCtrlInst->GetSelectedPosition(0, pos);
                                UpdateFreezeAtom(pos);
                        }
                }
        } else {
                m_pTcValue->SetValue(wxString::Format(wxT("%.3f"), m_initBondValue));
                DoTextValueChanged();
        }
*/
}

void DesignBondPnl::OnToggleButton(wxCommandEvent& event)
{
    int i;
    bool chgbond=false;
    BondType bondtype=BOND_TYPE_0;
         int btnId = event.GetId();
    if(btnId >= CTRL_ID_TGL_BOND_TYPE_START && btnId <=  CTRL_ID_TGL_BOND_TYPE_END) {
                btnId -= CTRL_ID_TGL_BOND_TYPE_START;
                if(!m_pTglBmpBondTypes[btnId]->GetValue()) {
                        //when click a selected button, cannot change its status
                        m_pTglBmpBondTypes[btnId]->SetValue(true);
                }else {
                        for(i = 0; i <  BOND_TYPE_COUNT_NEW; i++) {
                                if(i != btnId) {
                                        m_pTglBmpBondTypes[i]->SetValue(false);
                                }
                        }
                }

        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        if(pSelUtil->GetSelectedCount()<=0){
            wxMessageBox(wxT("Please select tow atoms! "));
            return;
        }
        int atomIndex[2];
        atomIndex[0]=pSelUtil->GetSelectedIndex(0);
        atomIndex[1]=pSelUtil->GetSelectedIndex(1);
   //    if(btnId==0){
    //        m_pCtrlInst->GetRenderingData()->BreakBond(atomIndex,true);
     //   }else{
            switch(btnId){
            case 0:
            bondtype=BOND_TYPE_0;
            break;
            case 1:
            bondtype=BOND_TYPE_1;
            break;
            case 2:
            bondtype=BOND_TYPE_2;
            break;
            case 3:
            bondtype=BOND_TYPE_3;
            break;
            case 4:
            bondtype=BOND_TYPE_4;
            break;
            case 5:
            bondtype=BOND_TYPE_5;

            }
            chgbond=m_pCtrlInst->GetRenderingData()->ChangeBondType(atomIndex,bondtype);
            if(!chgbond){
              m_pCtrlInst->GetRenderingData()->AddBond(atomIndex,bondtype);
            }
            m_pCtrlInst->SetDirty(true);
            MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
            MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
       // }
        }

}
