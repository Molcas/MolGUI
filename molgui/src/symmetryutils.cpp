/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "symmetryutils.h"
#include "measure.h"

/*******************************************************
within this file, the functions used in the symmetry
operation are defined.
********************************************************/

// liufenglai: just write this function only use in this file. so I do
// not hope that other routines know this.
static double MeasureLining(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3 );

/********************************************************
target:
      moving the atom to the given point
input:
      atomIndex: the atom which should move required
                 by user
      symCoord:  the final coordinate of atomIndex, this
                 array has dimension of 3
      isSingle: determine to move in the group way,
                 or only move the single atom
*********************************************************/
SymInfor SymmetryTranslation (int atomIndex, double symCoord[], AtomBondArray & atomBondArray, bool isSingle) {
    MovingAtoms * movingAtoms = NULL;        // record the information of
    // moving atoms
    int movingArrayLength = 0;                // the length of movingAtoms array

    int i;

    if (isSingle) {
        atomBondArray[atomIndex].atom.pos.x -= symCoord[0];
        atomBondArray[atomIndex].atom.pos.y -= symCoord[1];
        atomBondArray[atomIndex].atom.pos.z -= symCoord[2];
        return SYMMETRY_SUCCESS;
    }

    //search the moving atoms
    movingAtoms = SearchConnectedAtoms(atomBondArray, atomIndex, &movingArrayLength);
    if ( movingAtoms == NULL || movingArrayLength == 0) {
        return SYMMETRY_ERROR_IN_MEMORY_ALLOCATION;
    }

    //moving the group to the given point
    for (i=0; i<movingArrayLength; i++) {
        movingAtoms[i].x -= symCoord[0];
        movingAtoms[i].y -= symCoord[1];
        movingAtoms[i].z -= symCoord[2];
    }

    //value write back
    for (i=0; i< movingArrayLength; i++) {
        atomBondArray[movingAtoms[i].atomLabel].atom.pos.x = movingAtoms[i].x;
        atomBondArray[movingAtoms[i].atomLabel].atom.pos.y = movingAtoms[i].y;
        atomBondArray[movingAtoms[i].atomLabel].atom.pos.z = movingAtoms[i].z;
    }

    // finally free the space used by the movingAtom
    if (movingAtoms != NULL) {
        free (movingAtoms);
        movingAtoms = NULL;
    }

    return SYMMETRY_SUCCESS;
}

/*************************************************************************
target:
      inverse the atom coordinates according to the given point
input:
      atomIndex: the atom which should move required
                 by user
      symCoord:  the coordinates of the inversion point , this
                 array has dimension of 3
      isSingle: determine to move in the group way,
                 or only move the single atom
**************************************************************************/
SymInfor SymmetryInversion (int atomIndex, double symCoord[], AtomBondArray & atomBondArray, bool isSingle) {
    MovingAtoms * movingAtoms = NULL;        // record the information of
    // moving atoms
    int movingArrayLength = 0;                // the length of movingAtoms array

    int i;

    if (isSingle) {
        atomBondArray[atomIndex].atom.pos.x = 2*symCoord[0] - atomBondArray[atomIndex].atom.pos.x;
        atomBondArray[atomIndex].atom.pos.y = 2*symCoord[1] - atomBondArray[atomIndex].atom.pos.y;
        atomBondArray[atomIndex].atom.pos.z = 2*symCoord[2] - atomBondArray[atomIndex].atom.pos.z;
        return SYMMETRY_SUCCESS;
    }

    //search the moving atoms
    movingAtoms = SearchConnectedAtoms(atomBondArray, atomIndex, &movingArrayLength);
    if ( movingAtoms == NULL || movingArrayLength == 0) {
        return SYMMETRY_ERROR_IN_MEMORY_ALLOCATION;
    }

    //moving the group to the given point
    for (i=0; i<movingArrayLength; i++) {
        movingAtoms[i].x = 2*symCoord[0] - movingAtoms[i].x;
        movingAtoms[i].y = 2*symCoord[1] - movingAtoms[i].y;
        movingAtoms[i].z = 2*symCoord[2] - movingAtoms[i].z;
    }

    //value write back
    for (i=0; i< movingArrayLength; i++) {
        atomBondArray[movingAtoms[i].atomLabel].atom.pos.x = movingAtoms[i].x;
        atomBondArray[movingAtoms[i].atomLabel].atom.pos.y = movingAtoms[i].y;
        atomBondArray[movingAtoms[i].atomLabel].atom.pos.z = movingAtoms[i].z;
    }

    // finally free the space used by the movingAtom
    if (movingAtoms != NULL) {
        free (movingAtoms);
        movingAtoms = NULL;
    }

    return SYMMETRY_SUCCESS;
}





/*************************************************************************
target:
      rotate the atom coordinates according to the given axis. this
      axis can be any line in the space
input:
      atomIndex: the atom which should move required
                 by user
      symCoord:  the coordinates of the given axis, this
                 array has dimension of 2*3
      angle:     the rotating angle the user wish
      isSingle: determine to move in the group way,
                 or only move the single atom
**************************************************************************/
SymInfor SymmetryRotation (int atomIndex, double symCoord[][3], double angle, AtomBondArray & atomBondArray, bool isSingle) {
    MovingAtoms * movingAtoms = NULL;        // record the information of
    // moving atoms
    int movingArrayLength = 0;                // the length of movingAtoms array

    int i;

    double x1 = symCoord[0][0];        // the first point coordinate on the axis
    double y1 = symCoord[0][1];
    double z1 = symCoord[0][2];
    double x2 = symCoord[1][0];        // the second point coordinate on the axis
    double y2 = symCoord[1][1];
    double z2 = symCoord[1][2];

    // the coordinates of the atomIndex
    double x = atomBondArray[atomIndex].atom.pos.x;
    double y = atomBondArray[atomIndex].atom.pos.y;
    double z = atomBondArray[atomIndex].atom.pos.z;

    double angleZ  = 0;
    double cosgama = 0;
    int labelZ = 0;                        // values used in rotating around z

    double angleY  = 0;
    double cosbeta = 0;
    int labelY = 0;                        // values used in rotating around y



    //search the moving atoms
    //here in this function, we always do this step!!
    movingAtoms = SearchConnectedAtoms(atomBondArray, atomIndex, &movingArrayLength);
    if ( movingAtoms == NULL || movingArrayLength == 0) {
        return SYMMETRY_ERROR_IN_MEMORY_ALLOCATION;
    }



    //moving the first point to the origin
    x2 -= x1;
    y2 -= y1;
    z2 -= z1;

    if (isSingle) {
        x -= x1;
        y -= y1;
        z -= z1;
    } else {
        for (i=0; i< movingArrayLength; i++) {
            movingAtoms[i].x -= x1;
            movingAtoms[i].y -= y1;
            movingAtoms[i].z -= z1;
        }
    }

    //    printf ("the first step: x2=%f, y2=%f, z2=%f\n", x2, y2, z2);
    //  printf ("the first step: x=%f, y=%f, z=%f\n", x, y, z);

    /* rotate around the z; move the axis to the xz plane*/
    if (fabs(x2) > MEASURE_PREC || fabs(y2) > MEASURE_PREC) {
      cosgama = x2/sqrt(x2*x2 + y2*y2);
      if (y2<0) //counter-clockwise
        angleZ =  acos(cosgama);
      else      //clockwise
        angleZ = -acos(cosgama);

      RotationByZ (&x2, &y2, angleZ);
      if (isSingle) {
        RotationByZ (&x, &y, angleZ);
      } else {
        GroupRotationByZ (movingAtoms,movingArrayLength, angleZ);
      }
    } else {
      labelZ = 1;
    }

    //  printf ("the second step: x2=%f, y2=%f, z2=%f\n", x2, y2, z2);
    //  printf ("the second step: x=%f, y=%f, z=%f\n", x, y, z);



    /* rotate around the Y; move the axis to the Z axis, positive direction*/
    if (fabs(x2) > MEASURE_PREC || fabs(z2) > MEASURE_PREC) {
      cosbeta = z2/sqrt(x2*x2 + z2*z2);
      if (x2<0) //counter-clockwise
        angleY =  acos(cosbeta);
      else      //clockwise
        angleY = -acos(cosbeta);

      RotationByY (&x2, &z2, angleY);
      if (isSingle) {
        RotationByY (&x, &z, angleY);
      } else {
        GroupRotationByY (movingAtoms,movingArrayLength, angleY);
      }
    } else {
      labelY = 1;
    }

    //    printf ("the third step: x2=%f, y2=%f, z2=%f\n", x2, y2, z2);
    //    printf ("the third step: x=%f, y=%f, z=%f\n", x, y, z);



    // rotate around the z, to the angle you want
    angle = angle*RADIAN;
    if (isSingle) {
        RotationByZ (&x, &y, angle);
    } else {
        GroupRotationByZ (movingAtoms,movingArrayLength, angle);
    }



    /* restore the rotation around the y*/
    if (labelY != 1) {
        angleY = (-angleY);
        if (isSingle) {
            RotationByY (&x, &z, angleY);
        } else {
            GroupRotationByY (movingAtoms,movingArrayLength, angleY);
        }
    }



    /* restore the rotation around the Z*/
    if (labelZ != 1) {
        angleZ = (-angleZ);
        if (isSingle) {
            RotationByZ (&x, &y, angleZ);
        } else {
            GroupRotationByZ (movingAtoms,movingArrayLength, angleZ);
        }
    }


    //moving the axis back
    if (isSingle) {
        x += x1;
        y += y1;
        z += z1;
    } else {
        for (i=0; i< movingArrayLength; i++) {
            movingAtoms[i].x += x1;
            movingAtoms[i].y += y1;
            movingAtoms[i].z += z1;
        }
    }


    //value write back
    if (isSingle) {
        atomBondArray[atomIndex].atom.pos.x = x;
        atomBondArray[atomIndex].atom.pos.y = y;
        atomBondArray[atomIndex].atom.pos.z = z;
    } else {
        for (i=0; i< movingArrayLength; i++) {
            atomBondArray[movingAtoms[i].atomLabel].atom.pos.x = movingAtoms[i].x;
            atomBondArray[movingAtoms[i].atomLabel].atom.pos.y = movingAtoms[i].y;
            atomBondArray[movingAtoms[i].atomLabel].atom.pos.z = movingAtoms[i].z;
        }
    }


    // finally free the space used by the movingAtom
    if (movingAtoms != NULL) {
        free (movingAtoms);
        movingAtoms = NULL;
    }

    return SYMMETRY_SUCCESS;

}








/*************************************************************************
target:
      reflect the atom coordinates according to the given plane. this
      plane can be any plane in the space
input:
      atomIndex: the atom which should move required
                 by user
      symCoord:  the coordinates which define the plane, furthermore the
                 lining situation is checked and hope to avoided.this
                 array has dimension of 3*3.
      isSingle: determine to move in the group way,
                 or only move the single atom
**************************************************************************/
SymInfor SymmetryReflection (int atomIndex, double symCoord[][3], AtomBondArray & atomBondArray, bool isSingle) {
    MovingAtoms * movingAtoms = NULL;        // record the information of
    // moving atoms
    int movingArrayLength = 0;                // the length of movingAtoms array

    int i;

    double x1 = symCoord[0][0];        // the first point coordinate on the plane
    double y1 = symCoord[0][1];
    double z1 = symCoord[0][2];
    double x2 = symCoord[1][0];        // the second point coordinate on the plane
    double y2 = symCoord[1][1];
    double z2 = symCoord[1][2];
    double x3 = symCoord[2][0];        // the third point coordinate on the plane
    double y3 = symCoord[2][1];
    double z3 = symCoord[2][2];


    // the coordinates of the atomIndex
    double x = atomBondArray[atomIndex].atom.pos.x;
    double y = atomBondArray[atomIndex].atom.pos.y;
    double z = atomBondArray[atomIndex].atom.pos.z;

    double angleZ  = 0;
    double cosgama = 0;
    int labelZ = 0;                        // values used in rotating around z

    double angleY  = 0;
    double cosbeta = 0;
    int labelY = 0;                        // values used in rotating around y

    double angleX  = 0;
    double cosalfa = 0;
    int labelX = 0;                       // values used in rotating around x

    double testingResult;

    // before doing any thing, let's check whether these three points
    // are lining!!! or they can not defining the real plane!!!
    testingResult = MeasureLining (x1,y1,z1,x2,y2,z2,x3,y3,z3);
    if ( fabs(1-fabs(testingResult))<MEASURE_PREC) {
        return   SYMMETRY_ERROR_IN_REFLECTION;
    }


    //search the moving atoms
    //here in this function, we always do this step!!
    movingAtoms = SearchConnectedAtoms(atomBondArray, atomIndex, &movingArrayLength);
    if ( movingAtoms == NULL || movingArrayLength == 0) {
        return SYMMETRY_ERROR_IN_MEMORY_ALLOCATION;
    }



    //moving the second point to the origin
    x1 -= x2;
    y1 -= y2;
    z1 -= z2;
    x3 -= x2;
    y3 -= y2;
    z3 -= z2;

    if (isSingle) {
        x -= x2;
        y -= y2;
        z -= z2;
    } else {
        for (i=0; i< movingArrayLength; i++) {
            movingAtoms[i].x -= x2;
            movingAtoms[i].y -= y2;
            movingAtoms[i].z -= z2;
        }
    }



    /* rotate around the z; move the first to the xz plane*/
    if (fabs(x1) > MEASURE_PREC || fabs(y1) > MEASURE_PREC) {
        cosgama = x1/sqrt(x1*x1 + y1*y1);
        if (y1<0) //counter-clockwise
            angleZ =  acos(cosgama);
        else      //clockwise
            angleZ = -acos(cosgama);
        RotationByZ (&x1, &y1, angleZ);
        RotationByZ (&x3, &y3, angleZ);

        if (isSingle) {
            RotationByZ (&x, &y, angleZ);
        } else {
            GroupRotationByZ (movingAtoms,movingArrayLength, angleZ);
        }
    } else {
        labelZ = 1;
    }
    //    printf ("the x1 and y1 and z1 value is: %f, %f, %f\n", x1, y1, z1);
    //    printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);




    //rotate around the Y; move the first point
    //to the x axis, positive direction
    if (fabs(x1) > MEASURE_PREC || fabs(z1) > MEASURE_PREC) {
        cosbeta = x1/sqrt(x1*x1 + z1*z1);
        if (z1>0) //counter-clockwise
            angleY =  acos(cosbeta);
        else      //clockwise
            angleY = -acos(cosbeta);
        RotationByY (&x1, &z1, angleY);
        RotationByY (&x3, &z3, angleY);

        if (isSingle) {
            RotationByY (&x, &z, angleY);
        } else {
            GroupRotationByY (movingAtoms,movingArrayLength, angleY);
        }
    } else {
        labelY = 1;
    }
    //    printf ("the x1 and y1 and z1 value is: %f, %f, %f\n", x1, y1, z1);
    //    printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);




    /* rotate around the x; move the third point to the
       to the xy plane*/
    if (fabs(y3) > MEASURE_PREC || fabs(z3) > MEASURE_PREC) {
        cosalfa = y3/sqrt (y3*y3 + z3*z3);
        if (z3<0) //counter-clockwise
            angleX =  acos(cosalfa);
        else      //clockwise
            angleX = -acos(cosalfa);
        RotationByX (&y3, &z3, angleX);

        if (isSingle) {
            RotationByX (&y, &z, angleX);
        } else {
            GroupRotationByX (movingAtoms,movingArrayLength, angleX);
        }
    } else {
        labelX = 1;
    }

    //    printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);



    //now the plane is move the xy plane
    //reflection operation is done here!!!!
    if (isSingle) {
        z= -z;
    } else {
        for (i=0; i< movingArrayLength; i++) {
            movingAtoms[i].z = - movingAtoms[i].z ;
        }
    }

    /* restore the rotation around the x*/
    if (labelX != 1) {
        angleX = (-angleX);
        if (isSingle) {
            RotationByX (&y, &z, angleX);
        } else {
            GroupRotationByY (movingAtoms,movingArrayLength, angleX);
        }
    }



    /* restore the rotation around the y*/
    if (labelY != 1) {
        angleY = (-angleY);
        if (isSingle) {
            RotationByY (&x, &z, angleY);
        } else {
            GroupRotationByY (movingAtoms,movingArrayLength, angleY);
        }
    }



    /* restore the rotation around the Z*/
    if (labelZ != 1) {
        angleZ = (-angleZ);
        if (isSingle) {
            RotationByZ (&x, &y, angleZ);
        } else {
            GroupRotationByZ (movingAtoms,movingArrayLength, angleZ);
        }
    }


    //moving the second point back
    if (isSingle) {
        x += x2;
        y += y2;
        z += z2;
    } else {
        for (i=0; i< movingArrayLength; i++) {
            movingAtoms[i].x += x2;
            movingAtoms[i].y += y2;
            movingAtoms[i].z += z2;
        }
    }


    //value write back
    if (isSingle) {
        atomBondArray[atomIndex].atom.pos.x = x;
        atomBondArray[atomIndex].atom.pos.y = y;
        atomBondArray[atomIndex].atom.pos.z = z;
    } else {
        for (i=0; i< movingArrayLength; i++) {
            atomBondArray[movingAtoms[i].atomLabel].atom.pos.x = movingAtoms[i].x;
            atomBondArray[movingAtoms[i].atomLabel].atom.pos.y = movingAtoms[i].y;
            atomBondArray[movingAtoms[i].atomLabel].atom.pos.z = movingAtoms[i].z;
        }
    }


    // finally free the space used by the movingAtom
    if (movingAtoms != NULL) {
        free (movingAtoms);
        movingAtoms = NULL;
    }

    return SYMMETRY_SUCCESS;

}





/*******************************************************************
 * for calculate the angle between three points
 ********************************************************************/

double MeasureLining (double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3) {
    double vec23_x = x3 - x2;
    double vec23_y = y3 - y2;
    double vec23_z = z3 - z2;

    double vec21_x = x1 - x2;
    double vec21_y = y1 - y2;
    double vec21_z = z1 - z1;

    double result;
    result = (vec23_x*vec21_x + vec23_y*vec21_y + vec23_z*vec21_z)/( sqrt( vec23_x*vec23_x + vec23_y*vec23_y + vec23_z*vec23_z ) * sqrt (vec21_x*vec21_x + vec21_y*vec21_y + vec21_z*vec21_z) );
    /* result is the cosine of bond_angle value betwwen vec23 and vec21 */

    // here we add some protection to the return value
    if (fabs(result)<MEASURE_PREC) {
        result = 0.0;
    } else if ( fabs(1-fabs(result))<MEASURE_PREC) {
        if (result > 0)
            result = 1.0;
        else
            result = -1.0;
    }

    return result;
}
