/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "progressbarpnl.h"

#include "tools.h"

#include "molvieweventhandler.h"

////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
int ID_CTRL_TEXT_STATUS = wxNewId();
int ID_CTRL_BTN_STOP= wxNewId();
int ID_CTRL_GAUGE= wxNewId();
int MMTIMER_ID_IDLE = wxNewId();
static const wxChar *errorText[] = {
        wxT(""), // no error
        wxT("signal not supported"),
        wxT("permission denied"),
        wxT("no such process"),
        wxT("unspecified error"),
};
//////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE(ProgressBarPnl, wxPanel)
        EVT_BUTTON(ID_CTRL_BTN_STOP, ProgressBarPnl::OnBtnStop)
        EVT_TIMER(MMTIMER_ID_IDLE, ProgressBarPnl::OnTimerIdle)
END_EVENT_TABLE()

ProgressBarPnl::ProgressBarPnl (wxWindow* parent, wxWindowID id, const wxPoint& pos,
            const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name),timerIdleWakeUp(this, MMTIMER_ID_IDLE) {
        m_pStatus = NULL;
        m_pGaugeProgress = NULL;
        m_pBtnStop = NULL;
        m_pCtrlInst = NULL;
        m_processId=0;
        m_pipedProcess=NULL;
}

void ProgressBarPnl::InitPanel(void) {
//        int i = 0;
        int height = wxDefaultSize.GetHeight();

    wxBoxSizer* pBsMain = new wxBoxSizer(wxHORIZONTAL);
        this->SetSizer(pBsMain);
         m_pStatus = new wxStaticText(this, wxID_ANY, wxT("Task Progress:"), wxDefaultPosition, wxSize(200, height), wxALIGN_RIGHT | wxST_NO_AUTORESIZE);
        m_pGaugeProgress = new wxGauge(this, ID_CTRL_GAUGE, 100, wxDefaultPosition, wxSize(400, 30), wxGA_HORIZONTAL,wxDefaultValidator,_("gauge"));
        m_pBtnStop = new wxButton(this,ID_CTRL_BTN_STOP, wxT("Stop"), wxDefaultPosition, wxSize(60, 30));
        m_pBtnStop->Enable(true);
//        pBsMain->AddStretchSpacer(1);
        pBsMain->Add(m_pStatus, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL |wxALL, 5);
        pBsMain->Add(m_pGaugeProgress, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL |wxALL, 5);
        pBsMain->Add(m_pBtnStop, 0, wxALIGN_LEFT |wxALIGN_CENTER_VERTICAL |wxALL, 5);

}


void ProgressBarPnl::SetCtrlInst(CtrlInst* pCtrlInst) {
    m_pCtrlInst = pCtrlInst;
}

void ProgressBarPnl::OnBtnStop(wxCommandEvent& event) {
    m_break=1;
        wxKillError rc = wxProcess::Kill(m_processId, (wxSignal)wxSIGKILL, wxKILL_CHILDREN);
        if (rc == wxKILL_OK) {
                Tools::ShowStatus(wxString::Format(wxT("Process %ld killed."), m_processId), 1);
                if (timerIdleWakeUp.IsRunning())timerIdleWakeUp.Stop();
        }else{
                Tools::ShowStatus(wxString::Format(wxT("Failed to kill process %ld: %s"), m_processId, errorText[rc]), 1);
        }
}

void ProgressBarPnl::OnTimerIdle(wxTimerEvent& WXUNUSED(event)){
        wxWakeUpIdle();
        m_pGaugeProgress->Pulse();
//        MolViewEventHandler::Get()->DoUpdateMM();
}

void ProgressBarPnl::OnProcessTerminated(int status){
        MolViewEventHandler::Get()->OnUpdateProgress(m_break);
        //if (timerIdleWakeUp.IsRunning())timerIdleWakeUp.Stop();
}

bool ProgressBarPnl::RunProcess(wxString cmd){
        m_pipedProcess = new MMExecPipedProcess(this);
        m_processId = wxExecute(cmd,wxEXEC_ASYNC, m_pipedProcess); //| wxEXEC_MAKE_GROUP_LEADER
        // std::cout << "ProgressBarPnl::RunProcess: " << cmd << std::endl;
        bool started = true;
        m_break=0;
        if (!m_processId) {
                wxLogError(_T("Execution of '%s' failed."), cmd.c_str());
                delete m_pipedProcess;
                m_pipedProcess = NULL;
                started=false;
        }else{
                if (!timerIdleWakeUp.IsRunning())timerIdleWakeUp.Start(300);
        }
        return started;
};

MMExecPipedProcess::MMExecPipedProcess(MMIProcessListener* listener): wxProcess(){
        m_listener = listener;
        Redirect();
}

MMExecPipedProcess::~MMExecPipedProcess() {
        m_listener=NULL;
}

void MMExecPipedProcess::OnTerminate(int pid, int status){
        // show the rest of the output
        m_listener-> OnProcessTerminated(status);
        // we're not needed any more
        //delete this;
}
