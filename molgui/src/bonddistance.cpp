/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "measure.h"
#include "atommanuutils.h"
#include "renderingdata.h"

/***************************************************
 * for calculate two atom's distance
 ***************************************************/
double MeasureBondDistance(int atom1Index, int atom2Index, const AtomBondArray& atomBondArray)
{
  double d1 = atomBondArray[atom1Index].atom.pos.x - atomBondArray[atom2Index].atom.pos.x;
  double d2 = atomBondArray[atom1Index].atom.pos.y - atomBondArray[atom2Index].atom.pos.y;
  double d3 = atomBondArray[atom1Index].atom.pos.z - atomBondArray[atom2Index].atom.pos.z;
  return  sqrt (d1*d1 + d2*d2 +d3*d3);
}


/*********************************************************************
   this function changes the coordinates of the atoms, while the bond
   distance is changing. two ways supported: first, there's group
   linking with the moving atom also moveing consistently; the other
   way, only the moving atom ssigned by the user moves. the switch of
   both of the two ways, is defined by bool methodSwitch.
**********************************************************************/
MeasureInfo ChangeBondDistance(double distance, int atomStill, int atomMove, AtomBondArray& atomBondArray, bool methodSwitch)
{
  int i;

  //all of the moving atom's information
  //is here. including the position of label. see the common.h
  MovingAtoms * movingAtoms = NULL;
  int movingAtomArrayLength = 0;

  //MeasureInfo  returnInfo;  /* return information */

  // both of the two arrays used to preserve the bond infor temporily
  ChemBond bondArrayStoreOfAtomStill[ATOM_MAX_BOND_COUNT];
  ChemBond bondArrayStoreOfAtomMove[ATOM_MAX_BOND_COUNT];

  //RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata0.dat"));



  // first decide how to change the corrdinate
  // if methodSwitch is true, the corrdinate change only done to 2,3,4 atoms
  if (methodSwitch) {
    ChangeBondDistanceCalculation (distance, atomStill, atomMove, movingAtoms, movingAtomArrayLength, atomBondArray);
    return   MEASURE_SUCCESS;
  }


  // second copy the information of atoms which will break their bond
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {
      bondArrayStoreOfAtomStill[i] = atomBondArray[atomStill].bonds[i];
      bondArrayStoreOfAtomMove[i]  = atomBondArray[atomMove].bonds[i];
    }


  //RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata1.dat"));


  /* third change the connection to fit for the bond length change */
  for (i=0; i<atomBondArray[atomStill].bondCount; i++)
    /* we have to break the bond between atom1 and atom2 */
    {
      if ( atomBondArray[atomStill].bonds[i].atomIndex == atomMove )
        {
          atomBondArray[atomStill].bonds[i].atomIndex = BP_NULL_VALUE;
          break;
        }
      else
        continue;
    }


  for (i=0; i<atomBondArray[atomMove].bondCount; i++)
    /* we have to break the bond between atom1 and atom2 */
    {
      if ( atomBondArray[atomMove].bonds[i].atomIndex == atomStill )
        {
          atomBondArray[atomMove].bonds[i].atomIndex = BP_NULL_VALUE;
          break;
        }
      else
        continue;
    }

  //RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata2.dat"));




  /* search the connection*/
  movingAtoms = SearchConnectedAtoms(atomBondArray, atomMove, &movingAtomArrayLength);


  // restore the information of breaking bond
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {
      atomBondArray[atomStill].bonds[i] = bondArrayStoreOfAtomStill[i];
      atomBondArray[atomMove].bonds[i] =  bondArrayStoreOfAtomMove[i];
    }

  if ( movingAtoms == NULL)
    {
      return MEASURE_ERROR_IN_MEMORY_ALLOCATION;
    }



  //RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata3.dat"));



  /* if atom1 and atom2 is bonding into a circle, the bond length can not change! */
  for (i=0; i<movingAtomArrayLength; i++)
    {
      if ( movingAtoms[i].atomLabel == atomStill)
        {
          if (movingAtoms != NULL) {
            free (movingAtoms);
            movingAtoms = NULL;
          }

          return MEASURE_ERROR_IN_BONDLENGTH;
        }
    }



  /* calculate the coordinate */
  ChangeBondDistanceCalculation (distance, atomStill, atomMove, movingAtoms, movingAtomArrayLength, atomBondArray);

  // finally free the space used by the movingAtom
  if (movingAtoms != NULL) {
    free (movingAtoms);
    movingAtoms = NULL;
  }

  return   MEASURE_SUCCESS;

}






/******************************************************************************
 *calculate the coordinate change made by the distance change between two atoms
 ******************************************************************************/

void  ChangeBondDistanceCalculation (double distance, int atomStill, int atomMove,  MovingAtoms * movingAtoms, int movingAtomArrayLength, AtomBondArray& atomBondArray)
{

  double bond;                        // the original bond length
  double normal;                // nornalized parameter
  double tmpx;
  double tmpy;
  double tmpz;                /* tmp store some value */
  int i;
  int tmpLabel;                        // the moving atom label

  double x1;                         // atomStill 's coordinate
  double y1;
  double z1;
  double x2;                        // atomMove's coordinate
  double y2;
  double z2;


    x1 = atomBondArray[atomStill].atom.pos.x;
    y1 = atomBondArray[atomStill].atom.pos.y;
    z1 = atomBondArray[atomStill].atom.pos.z;
    x2 = atomBondArray[atomMove].atom.pos.x;
    y2 = atomBondArray[atomMove].atom.pos.y;
    z2 = atomBondArray[atomMove].atom.pos.z;



    /* calculate the change */
    bond = sqrt((x2 - x1)*(x2 - x1)+(y2 - y1)*(y2 - y1)+(z2 - z1)*(z2 - z1));
    normal = distance/bond;


    /* we have all the moving atoms to move in such difference  */
    tmpx = x1 + normal*(x2 - x1) - x2;
    tmpy = y1 + normal*(y2 - y1) - y2;
    tmpz = z1 + normal*(z2 - z1) - z2;


  if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
        atomBondArray[atomMove].atom.pos.x += tmpx;
        atomBondArray[atomMove].atom.pos.y += tmpy;
        atomBondArray[atomMove].atom.pos.z += tmpz;
    }
  else {
    for (i=0; i< movingAtomArrayLength; i++) {
      tmpLabel = movingAtoms[i].atomLabel;
      atomBondArray[tmpLabel].atom.pos.x += tmpx;
      atomBondArray[tmpLabel].atom.pos.y += tmpy;
      atomBondArray[tmpLabel].atom.pos.z += tmpz;
    }
  }


}
