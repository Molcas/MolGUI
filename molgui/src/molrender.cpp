/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "molrender.h"
#include "uiprops.h"
#include "molviewdata.h"
#include "freetypefont.h"

//FreeTypeFont* globalFreeTypeFont = NULL;

//void MolRender::InitLabelFont(void) {
//        if(globalFreeTypeFont == NULL) {
//                globalFreeTypeFont = new FreeTypeFont();
//                wxString fontFilename=CheckFontFile(GetUIProps()->openGLProps.labelFont);
        //        wxMessageBox(fontFilename);
//                globalFreeTypeFont->Init(fontFilename, 16);
//        }
//}
//void MolRender::FreeLabelFont(void) {
//        if(globalFreeTypeFont ){
//                globalFreeTypeFont->Clean();
//                delete globalFreeTypeFont;
//        }
//}
//----------print test
void MolRender::PrintLabelCenter(CtrlInst* pCtrlInst, MolGLCanvas * pMolGLCanvas, wxString text){
        //------printing test fixed labels temp
        AtomPosition tempAtom;
        long dispRadius = 0.2f;
        //MolGLCanvas* pMolGLCanvas;
        tempAtom.x = 0;
        tempAtom.y = 0;
        tempAtom.z = 0;
        //wxString psome = wxString::Format(wxT("x: %0.2f y: %0.2f"), xCoord,yCoord);
        AtomPosition3F zAxis = pCtrlInst->camera.GetZAxis();
        pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, tempAtom, dispRadius, text);
        //--------
//------------
}

void MolRender::DrawAtom(CtrlInst* pCtrlInst, int atomIndex, ActionType actionType, GLenum mode,MolGLCanvas * pMolGLCanvas) {
    ChemAtom* atom = &(pCtrlInst->GetRenderingData()->GetAtomBondArray()[atomIndex].atom);
        ElemInfo *eleInfoOption = NULL;
        ChemAtom hydrogenAtom;
        hydrogenAtom.elemId = (ElemId)ELEM_H;
        GLUquadricObj* sphere = NULL;
        GLUquadricObj* sphereSelected = NULL;
        ModelType modelType = pCtrlInst->modelType;
        bool isMoving = pCtrlInst->moveType != MOVE_NONE;

        if(pCtrlInst->isShowLayers) {
                if((pCtrlInst->isShowLayerHigh && LAYER_HIGH == (LayerType)atom->layer)
                                || (pCtrlInst->isShowLayerMedium && LAYER_MEDIUM == (LayerType)atom->layer)
                                || (pCtrlInst->isShowLayerLow && LAYER_LOW == (LayerType)atom->layer)) {
                        modelType = GetUIProps()->chemProps.GetLayerModelType((LayerType)atom->layer);
                }else {
                        return;
                }
        }
        float dispRadius = atom->GetDispRadius(modelType);

        glPushMatrix();
        glTranslatef(atom->pos.x, atom->pos.y, atom->pos.z);

        if(MODEL_BALLSPOKE == modelType || MODEL_WIRE == modelType || MODEL_BALLWIRE == modelType ||MODEL_SPACEFILLING == modelType
                        || GL_SELECT == mode || MODEL_TUBE == modelType) {

                if(!pCtrlInst->isShowHydrogens && atom->IsDefaultHydrogen()) {
                }else {
                    if((actionType == ACTION_VIEW || actionType == ACTION_MEASURE || actionType == ACTION_CONSTRAIN
                                        || actionType == ACTION_LAYER||actionType==ACTION_DELETE_FRAGMENT || actionType == ACTION_SYMMETRIZE|| actionType == ACTION_DESIGN_BOND)&& atom->IsDefaultHydrogen()) {
                            eleInfoOption = GetElemInfo(ELEM_H);
                            hydrogenAtom.SetDispRadius(eleInfoOption->dispRadius);
                            dispRadius = hydrogenAtom.GetDispRadius(modelType);
                    }else {
                            if(atom->IsDefaultHydrogen() && GL_SELECT == mode) {
                                    dispRadius = atom->GetSelectRadius(modelType);
                            }
                            eleInfoOption = GetElemInfo((ElemId)atom->elemId);
                    }
                    if(MODEL_WIRE != modelType || GL_SELECT == mode) {
                            glColor3f(eleInfoOption->colorR, eleInfoOption->colorG, eleInfoOption->colorB);

                                //GLfloat matDif[] = {eleInfoOption->colorR, eleInfoOption->colorG, eleInfoOption->colorB, 1.0f};
                                //glMaterialfv(GL_FRONT, GL_DIFFUSE, matDif);

                                sphere = gluNewQuadric();
                                // gluQuadricDrawStyle(sphere, GLU_FILL);
                                // gluQuadricNormals(sphere, GLU_SMOOTH);
                        if(isMoving) {
                          gluSphere(sphere, dispRadius, GetUIProps()->openGLProps.movingObjectSlices, GetUIProps()->openGLProps.movingObjectStacks);
                          //gluSphere(sphere, dispRadius, GetUIProps()->openGLProps.staticObjectSlices, GetUIProps()->openGLProps.staticObjectStacks);
                        }else {
                          gluSphere(sphere, dispRadius, GetUIProps()->openGLProps.staticObjectSlices, GetUIProps()->openGLProps.staticObjectStacks);
                          //gluSphere(sphere, dispRadius, GetUIProps()->openGLProps.movingObjectSlices, GetUIProps()->openGLProps.movingObjectStacks);
                        }
                    }
                        if(pCtrlInst->GetSelectionUtil()->IsSelected(atomIndex)) {
                                glColor3f(GetUIProps()->openGLProps.selectionColor.r, GetUIProps()->openGLProps.selectionColor.g, GetUIProps()->openGLProps.selectionColor.b);
                                sphereSelected = gluNewQuadric();
                                gluQuadricDrawStyle(sphereSelected, (GLenum)GLU_LINE);
                                gluSphere(sphereSelected, dispRadius + 0.03, 16, 16);
                        }
                        if(pCtrlInst->GetSelectionUtil()->IsManuSelected(atomIndex)) {
                                glColor3f(GetUIProps()->openGLProps.manuSelectionColor.r, GetUIProps()->openGLProps.manuSelectionColor.g, GetUIProps()->openGLProps.manuSelectionColor.b);
                                if(sphereSelected == NULL) {
                                        sphereSelected = gluNewQuadric();
                                        gluQuadricDrawStyle(sphereSelected, (GLenum)GLU_LINE);
                                }
                                gluSphere(sphereSelected, dispRadius + 0.03, 20, 20);
                        }
                }
        }

        PrintLabel(pCtrlInst, atomIndex, atom,pMolGLCanvas);
        //------printing test fixed labels temp
        //pMolGLCanvas->m_FreeTypeFont->Print(1.0f,1.0f,"coords: ");//not working
        // AtomPosition tempAtom;
        // tempAtom.x = 0;
        // tempAtom.y = 0;
        // tempAtom.z = 0;
        // wxString psome = wxString::Format(wxT("Rad: %0.2f x: %0.2f y: %0.2f"), dispRadius,atom->pos.x,atom->pos.y);
        // pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, tempAtom, dispRadius, psome);
        //--------
        glPopMatrix();

        if(sphere)
                gluDeleteQuadric(sphere);
        if(sphereSelected)
                gluDeleteQuadric(sphereSelected);
}

void MolRender::PrintLabel(CtrlInst* pCtrlInst, int atomIndex, ChemAtom* atom,MolGLCanvas *pMolGLCanvas) {
        if(pCtrlInst->moveType == MOVE_NONE && (pCtrlInst->isShowGlobalLabels || pCtrlInst->isShowAtomLabels)) {
                if(!pCtrlInst->isShowHydrogens && atom->IsDefaultHydrogen()) {
                }else {
                        if(pCtrlInst->isShowGlobalLabels)
                                glColor3f(GetUIProps()->openGLProps.globalLabelColor.r, GetUIProps()->openGLProps.globalLabelColor.g, GetUIProps()->openGLProps.globalLabelColor.b);
                        else
                                glColor3f(GetUIProps()->openGLProps.atomLabelColor.r, GetUIProps()->openGLProps.atomLabelColor.g, GetUIProps()->openGLProps.atomLabelColor.b);
                        float dispRadius = atom->GetDispRadius(pCtrlInst->modelType);
                        if(MODEL_TUBE == pCtrlInst->modelType) {
                                // some displacement needed by the tube type
                                dispRadius += 0.03f;
                        }
                        //globalFreeTypeFont->PrintString(pCtrlInst, atom->pos, dispRadius, Index2DisplayLabel(atom, atomIndex, pCtrlInst->isShowGlobalLabels).mb_str());
                        //----------- printing test
                        // wxString fromcoord;
                        // fromcoord = wxString::Format(wxT("x: %0.1f y: %0.1f z: %0.1f disprad: %0.1f" ), atom->pos.x,atom->pos.y,atom->pos.z,dispRadius);
                        // pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, atom->pos, dispRadius, fromcoord);
                        //------------------------
                        pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, atom->pos, dispRadius, Index2DisplayLabel(atom, atomIndex, pCtrlInst->isShowGlobalLabels).mb_str());
                }
        }
}

void MolRender::DrawBond(AtomBondArray& bondArray, int atomIndex, int bondIndex, bool colorWithDefaultHydrogen, bool isMoving) {
        int i;
        int atom1Index, atom2Index, atom3Index;

        short bondType = bondArray[atomIndex].bonds[bondIndex].bondType;
        atom1Index = atomIndex;
        atom2Index = bondArray[atomIndex].bonds[bondIndex].atomIndex;
        atom3Index = BP_NULL_VALUE;
        bool foundBondType4WithAtom1 = false;
        if(bondType != BOND_TYPE_1) {
                for(i = 0; i < bondArray[atom1Index].bondCount; i++) {
                        if(i != bondIndex && bondArray[atom1Index].bonds[i].bondType != BOND_TYPE_1) {
                                atom3Index = bondArray[atom1Index].bonds[i].atomIndex;
                                if(bondArray[atom1Index].bonds[i].bondType == BOND_TYPE_4) {
                                        foundBondType4WithAtom1 = true;
                                        break;
                                }
                        }
                }
                if(!foundBondType4WithAtom1) {
                        atom1Index = bondArray[atomIndex].bonds[bondIndex].atomIndex;
                        atom2Index = atomIndex;
                        for(i = 0; i < bondArray[atom1Index].bondCount; i++) {
                                if(atom2Index !=  bondArray[atom1Index].bonds[i].atomIndex
                                                 && bondArray[atom1Index].bonds[i].bondType != BOND_TYPE_1) {
                                        atom3Index = bondArray[atom1Index].bonds[i].atomIndex;
                                        if(bondArray[atom1Index].bonds[i].bondType == BOND_TYPE_4) {
                                                break;
                                        }
                                }
                        }
                }
        }
        
        DrawBond(bondArray, atom1Index, atom2Index, atom3Index, (BondType)bondType, isMoving);
        // if(!isMoving) wxMessageBox(wxT("MolRender::DrawBond1: at1: "+std::to_string(atom1Index)+" at2: "+std::to_string(atom2Index)+" at3: "+std::to_string(atom3Index)+ " bondType "+std::to_string(bondType)), wxT("Warning"));
}

void MolRender::DrawBond(AtomBondArray& bondArray, int atom1Index, int atom2Index, int atom3Index, const BondType bondType, bool isMoving) {
    float r, rxz, rxy, rotY, rotZ, rotZ2, lenAxis[3];
        float atom1X, atom1Y, atom1Z;

        ChemAtom atom1 = bondArray[atom1Index].atom;
        ChemAtom atom2 = bondArray[atom2Index].atom;
        ChemAtom atom3;

        atom1X = atom1.pos.x;
        atom1Y = atom1.pos.y;
        atom1Z = atom1.pos.z;

        lenAxis[0] = atom2.pos.x - atom1.pos.x;
        lenAxis[1] = atom2.pos.y - atom1.pos.y;
        lenAxis[2] = atom2.pos.z - atom1.pos.z;
        r = lenAxis[0] * lenAxis[0] + lenAxis[1] * lenAxis[1] + lenAxis[2] * lenAxis[2];
        if (r <= 0.0f)
                return; // early exit if cylinder is of length 0
        r = sqrtf(r);

        // determine rotY rotation, amount to rotate about y
        rxz = r;
        if (rxz <= 0.0f) {
                rotY = 0.0f;
        } else {
                rotY = acosf(lenAxis[2] / rxz);
                // already on xz 1st quadrant
        }

        // determine rotZ rotation, amount to rotate about z
        rxy = sqrtf(lenAxis[0] * lenAxis[0] + lenAxis[1] * lenAxis[1]);
        if (rxy <= 0.0f) {
                rotZ = 0.0f;
        } else {
                rotZ = acosf(lenAxis[0] / rxy);
                if (lenAxis[1] < 0.0f)
                        rotZ = (float) (2.0f * PI) - rotZ;
        }

        //rotate atom1-atom3 to the xz plane
        rotZ2 = (float)(PI / 2.0f);
        if(atom3Index != BP_NULL_VALUE) {
                atom3 = bondArray[atom3Index].atom;

                ChemAtom::Translate(atom1, -atom1X, -atom1Y, -atom1Z);
                ChemAtom::Translate(atom3, -atom1X, -atom1Y, -atom1Z);
                ChemAtom::RotateWithZ(atom3, 360 - rotZ/RADIAN);
                ChemAtom::RotateWithY(atom3, 360 - rotY/RADIAN);

                lenAxis[0] = atom3.pos.x - atom1.pos.x;
                lenAxis[1] = atom3.pos.y - atom1.pos.y;
                // determine rotZ2 rotation, amount to rotate about z
                rxy = sqrtf(lenAxis[0] * lenAxis[0] + lenAxis[1] * lenAxis[1]);
                if (rxy <= 0.0f) {
                        rotZ2 = 0.0f;
                } else {
                        rotZ2 = acosf(lenAxis[0] / rxy);
                        if (lenAxis[1] < 0.0f)
                                rotZ2 = (float) (2.0f * PI) - rotZ2;
                }
        }

        glPushMatrix();
        glTranslatef((GLfloat)(atom1X), (GLfloat)(atom1Y), (GLfloat)(atom1Z));
        if(rotZ != 0.0f)
                glRotatef((GLfloat) (rotZ/RADIAN), 0.0f, 0.0f, 1.0f);
        if(rotY != 0.0f)
                glRotatef((GLfloat) (rotY/RADIAN), 0.0f, 1.0f, 0.0f);
        if(atom3Index != BP_NULL_VALUE && rotZ2 != 0.0f) {
                glRotatef((GLfloat) (rotZ2/RADIAN), 0.0f, 0.0f, 1.0f);
        }
        //let the user setting change the color of the bond
        glColor3f(GetUIProps()->openGLProps.bondColor.r,GetUIProps()->openGLProps.bondColor.g,GetUIProps()->openGLProps.bondColor.b);

        //GLfloat matDif[] = {GetUIProps()->openGLProps.bondColor.r, GetUIProps()->openGLProps.bondColor.g, GetUIProps()->openGLProps.bondColor.b, 1.0f};
        //glMaterialfv(GL_FRONT, GL_DIFFUSE, matDif);

        DrawCylinders(bondType, r, isMoving);
        glPopMatrix();
}

void MolRender::DrawCylinders(BondType bondType, GLdouble distance, bool isMoving) {
        GLUquadric * pObjQuadric = gluNewQuadric();
        gluQuadricDrawStyle(pObjQuadric, (GLenum)GLU_FILL);

        GLint slices = 1;
        GLint stacks = 1;
        if(isMoving) {
        slices = GetUIProps()->openGLProps.movingObjectSlices;
        stacks = GetUIProps()->openGLProps.movingObjectStacks;
    }else {
        slices = GetUIProps()->openGLProps.staticObjectSlices;
        stacks = GetUIProps()->openGLProps.staticObjectStacks;
    }

        GLdouble radius = GetUIProps()->openGLProps.bondRadius;
        int i;

        switch (bondType) {
                case BOND_TYPE_1:
                        //radius = 0.12f;
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        break;
                case BOND_TYPE_2:
                        radius = 0.07f;
                        glTranslatef(-0.08f, 0.0f, 0.0f);
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        glTranslatef(0.16f, 0.0f, 0.0f);
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        break;
                case BOND_TYPE_3:
                        radius = 0.045f;
                        glTranslatef(-0.10f, 0.0f, 0.0f);
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        glTranslatef(0.10f, 0.0f, 0.0f);
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        glTranslatef(0.10f, 0.0f, 0.0f);
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        break;
                case BOND_TYPE_4:
                        radius = 0.07f;
                        glTranslatef(-0.08f, 0.0f, 0.0f);
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        glTranslatef(0.16f, 0.0f, distance/16);
                        gluCylinder(pObjQuadric, radius, radius, distance / 8, slices, stacks);
                        for(i = 0; i < 4; i++) {
                                glTranslatef(0.0f, 0.0f, 3 * distance/16);
                                gluCylinder(pObjQuadric, radius, radius, distance / 8, slices, stacks);
                        }
                        break;
                case BOND_TYPE_5:
                        radius = 0.03f;
                        glTranslatef(-0.12f, 0.0f, 0.0f);
                        gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        for(i = 0; i < 3; i++) {
                                glTranslatef(0.08f, 0.0f, 0.0f);
                                gluCylinder(pObjQuadric, radius, radius, distance, slices, stacks);
                        }
                        break;
                case BOND_TYPE_6:
                        radius = 0.06f;
                        glColor3f(0.6f, 1.0f, 0.7f);
                        for(i = 0; i < 4; i++) {
                                glTranslatef(0.0f, 0.0f, 3 * distance/16);
                                gluCylinder(pObjQuadric, radius, radius, distance / 8, slices, stacks);
                        }
                        break;
                default:
            break;
        }
        gluDeleteQuadric(pObjQuadric);
}

void MolRender::DrawViewVolume(AtomPosition3F minPos, AtomPosition3F maxPos) {
    /*float disp = 0.5f;
        minPos.x -= disp;
        minPos.y -= disp;
        minPos.z -= disp;
        maxPos.x += disp;
        maxPos.y += disp;
        maxPos.z += disp;
        */
        glDisable(GL_LIGHTING);
        glColor3f(0.5f, 0.5f, 0.5f);
        //glLineStipple(1, 0xAAAA);
        //glEnable(GL_LINE_STIPPLE);
        glBegin(GL_LINE_LOOP);
                glVertex3f(minPos.x, minPos.y, minPos.z);
                glVertex3f(maxPos.x, minPos.y, minPos.z);
                glVertex3f(maxPos.x, maxPos.y, minPos.z);
                glVertex3f(minPos.x, maxPos.y, minPos.z);
        glEnd();
        glBegin(GL_LINE_LOOP);
                glVertex3f(minPos.x, minPos.y, maxPos.z);
                glVertex3f(maxPos.x, minPos.y, maxPos.z);
                glVertex3f(maxPos.x, maxPos.y, maxPos.z);
                glVertex3f(minPos.x, maxPos.y, maxPos.z);
        glEnd();
        glBegin(GL_LINE_LOOP);
                glVertex3f(minPos.x, minPos.y, minPos.z);
                glVertex3f(minPos.x, maxPos.y, minPos.z);
                glVertex3f(minPos.x, maxPos.y, maxPos.z);
                glVertex3f(minPos.x, minPos.y, maxPos.z);
        glEnd();
        glBegin(GL_LINE_LOOP);
                glVertex3f(maxPos.x, minPos.y, minPos.z);
                glVertex3f(maxPos.x, maxPos.y, minPos.z);
                glVertex3f(maxPos.x, maxPos.y, maxPos.z);
                glVertex3f(maxPos.x, minPos.y, maxPos.z);
        glEnd();
        glBegin(GL_LINE_LOOP);
                glVertex3f(minPos.x, minPos.y, minPos.z);
                glVertex3f(maxPos.x, minPos.y, minPos.z);
                glVertex3f(maxPos.x, minPos.y, maxPos.z);
                glVertex3f(minPos.x, minPos.y, maxPos.z);
        glEnd();
        glBegin(GL_LINE_LOOP);
                glVertex3f(minPos.x, maxPos.y, minPos.z);
                glVertex3f(maxPos.x, maxPos.y, minPos.z);
                glVertex3f(maxPos.x, maxPos.y, maxPos.z);
                glVertex3f(minPos.x, maxPos.y, maxPos.z);
        glEnd();
        glEnable(GL_LIGHTING);
}

void MolRender::DrawWire(const AtomBond* const atomBond1, const AtomBond* const atomBond2) {
        GLfloat lenAxis[3];
        ElemInfo* elemInfo = NULL;
        lenAxis[0] = (GLfloat)(atomBond1->atom.pos.x + (atomBond2->atom.pos.x - atomBond1->atom.pos.x)/2.0);
        lenAxis[1] = (GLfloat)(atomBond1->atom.pos.y + (atomBond2->atom.pos.y - atomBond1->atom.pos.y)/2.0);
        lenAxis[2] = (GLfloat)(atomBond1->atom.pos.z + (atomBond2->atom.pos.z - atomBond1->atom.pos.z)/2.0);

        glDisable(GL_LIGHTING);
        glLineWidth((GLfloat)2.0);
        //glBegin(GL_LINE_STRIP);
        glBegin(GL_LINES);
                elemInfo = GetElemInfo((ElemId)atomBond1->atom.elemId);
                   glColor3f(elemInfo->colorR, elemInfo->colorG, elemInfo->colorB);
                //glColor3f(atomBond1->atom.colorR, atomBond1->atom.colorG, atomBond1->atom.colorB);
                glVertex3f((GLfloat)(atomBond1->atom.pos.x), (GLfloat)(atomBond1->atom.pos.y), (GLfloat)(atomBond1->atom.pos.z));
                glVertex3f(lenAxis[0], lenAxis[1], lenAxis[2]);
                elemInfo = GetElemInfo((ElemId)atomBond2->atom.elemId);
                glColor3f(elemInfo->colorR, elemInfo->colorG, elemInfo->colorB);
                //glColor3f(atomBond2->atom.colorR, atomBond2->atom.colorG, atomBond2->atom.colorB);
                glVertex3f(lenAxis[0], lenAxis[1], lenAxis[2]);
                glVertex3f((GLfloat)(atomBond2->atom.pos.x), (GLfloat)(atomBond2->atom.pos.y), (GLfloat)(atomBond2->atom.pos.z));
        glEnd();
        glLineWidth((GLfloat)1.0);
        glEnable(GL_LIGHTING);
}

void MolRender::DrawTube(const AtomBond* const atomBond1, const AtomBond* const atomBond2, bool isTube, bool isMoving) {
        float r, rxy, phi, theta, lenAxis[3];
        ElemInfo * elemInfo = NULL;
        // need to do some preprocessing ... find length of vector
        lenAxis[0] = atomBond2->atom.pos.x - atomBond1->atom.pos.x;
        lenAxis[1] = atomBond2->atom.pos.y - atomBond1->atom.pos.y;
        lenAxis[2] = atomBond2->atom.pos.z - atomBond1->atom.pos.z;

        r = lenAxis[0] * lenAxis[0] + lenAxis[1] * lenAxis[1] + lenAxis[2] * lenAxis[2];

        if (r <= 0.0)
                return; // early exit if cylinder is of length 0

        r = sqrtf(r);
        // determine phi rotation angle, amount to rotate about y
        phi = acosf(lenAxis[2] / r);

        // determine theta rotation, amount to rotate about z
        rxy = sqrtf(lenAxis[0] * lenAxis[0] + lenAxis[1] * lenAxis[1]);
        if (rxy <= 0.0f) {
                theta = 0.0f;
        } else {
                theta = acosf(lenAxis[0] / rxy);
                if (lenAxis[1] < 0.0f)
                        theta = (float) (2.0f * PI) - theta;
        }

        glPushMatrix();
        glTranslatef((GLfloat)(atomBond1->atom.pos.x), (GLfloat)(atomBond1->atom.pos.y), (GLfloat)(atomBond1->atom.pos.z));
        if (theta != 0.0f)
                glRotatef((GLfloat) ((theta / PI) * 180.0f), 0.0f, 0.0f, 1.0f);
        if (phi != 0.0f)
                glRotatef((GLfloat) ((phi / PI) * 180.0f), 0.0f, 1.0f, 0.0f);

        if(isTube) {
                elemInfo = GetElemInfo((ElemId)atomBond1->atom.elemId);
                glColor3f(elemInfo->colorR, elemInfo->colorG, elemInfo->colorB);
                //glColor3f(atomBond1->atom.colorR, atomBond1->atom.colorG, atomBond1->atom.colorB);
                //if(atomBond1->bondCount < 4) {
                        //DrawDisk((GLenum)GLU_INSIDE);
                //        DrawSphere();
                //}

                DrawCylinders(BOND_TYPE_1, r / 2.0, isMoving);
                glTranslatef(0, 0, r/2.0);
                elemInfo = GetElemInfo((ElemId)atomBond2->atom.elemId);
                glColor3f(elemInfo->colorR, elemInfo->colorG, elemInfo->colorB);
                //glColor3f(atomBond2->atom.colorR, atomBond2->atom.colorG, atomBond2->atom.colorB);
                DrawCylinders(BOND_TYPE_1, r / 2.0, isMoving);
                /*if(atomBond2->bondCount == 1) {
                        glTranslatef(0, 0, r/2.0);
                        //DrawDisk((GLenum)GLU_OUTSIDE);
                        DrawSphere();
                }else if(atomBond2->bondCount == 2){
                        glTranslatef(0, 0, r/2.0);
                        DrawSphere();
                }        */
        }else {
                if(atomBond1->atom.IsDefaultHydrogen()){
                        elemInfo = GetElemInfo((ElemId)atomBond1->atom.elemId);
                        glColor3f(elemInfo->colorR, elemInfo->colorG, elemInfo->colorB);
                        //glColor3f(atomBond1->atom.colorR, atomBond1->atom.colorG, atomBond1->atom.colorB);
                }else {
                        glColor3f(0.7f, 0.7f, 0.7f);
                }
                DrawCylinders(BOND_TYPE_1, r / 2.0, isMoving);
                if(atomBond2->atom.IsDefaultHydrogen()){
                        elemInfo = GetElemInfo((ElemId)atomBond2->atom.elemId);
                        glColor3f(elemInfo->colorR, elemInfo->colorG, elemInfo->colorB);
                        //glColor3f(atomBond2->atom.colorR, atomBond2->atom.colorG, atomBond2->atom.colorB);
                }else {
                        glColor3f(0.7f, 0.7f, 0.7f);
                }
                glTranslatef(0, 0, r/2.0);
                DrawCylinders(BOND_TYPE_1, r / 2.0, isMoving);
        }
        glPopMatrix();
}

void MolRender::DrawAxes(CtrlInst* pCtrlInst,MolGLCanvas* pMolGLCanvas) {
        glDisable(GL_LIGHTING);
        glLineWidth((GLfloat)1.0);
        GLfloat len = 5.0f;
        //GLfloat arrowWidth = 0.3f;
        //GLfloat arrowHeight = 0.1f;

        AtomPosition pos;

        glBegin(GL_LINES);
            //glColor3f(GetUIProps()->openGLProps.axesColor.r, GetUIProps()->openGLProps.axesColor.g, GetUIProps()->openGLProps.axesColor.b);
            glColor3f(1.0f, 0.0f, 0.0f);
            glVertex3f((GLfloat)-len, (GLfloat)0.0f, (GLfloat)0.0f);
            glVertex3f(len, (GLfloat)0.0f, (GLfloat)0.0f);
         glEnd();
        glPushMatrix();
                pos.x = len + 0.2f;
                pos.y = 0.0f;
                pos.z = 0.0f;
                glTranslatef(pos.x, pos.y, pos.z);
                pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, pos, 0, "X");
        glPopMatrix();

        glBegin(GL_LINES);
            len--;
            glColor3f(0.0f, 1.0f, 0.0f);
            glVertex3f((GLfloat)0.0f, (GLfloat)-len, (GLfloat)0.0f);
            glVertex3f((GLfloat)0.0f, len, (GLfloat)0.0f);
         glEnd();
        glPushMatrix();
                pos.x = 0.0f;
                pos.y = len + 0.2f;
                pos.z = 0.0f;
                glTranslatef(pos.x, pos.y, pos.z);
                pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, pos, 0, "Y");
        glPopMatrix();

        glBegin(GL_LINES);
            len--;
            glColor3f(0.0f, 0.0f, 1.0f);
            glVertex3f((GLfloat)0.0f, (GLfloat)0.0f, (GLfloat)-len);
            glVertex3f((GLfloat)0.0f, (GLfloat)0.0f, len);
        glEnd();
        glPushMatrix();
                pos.x = 0.0f;
                pos.y = 0.0f;
                pos.z = len + 0.2f;
                glTranslatef(pos.x, pos.y, pos.z);
                pMolGLCanvas->m_FreeTypeFont->PrintString(pCtrlInst, pos, 0, "Z");
        glPopMatrix();

        /*glBegin(GL_TRIANGLES);
                glVertex3f(len, (GLfloat)0.0f, (GLfloat)0.0f);
                glVertex3f(len - arrowWidth, arrowHeight, (GLfloat)0.0f);
                glVertex3f(len - arrowWidth, -arrowHeight, (GLfloat)0.0f);

                glVertex3f((GLfloat)0.0f, len, (GLfloat)0.0f);
                glVertex3f(-arrowHeight, len - arrowWidth, (GLfloat)0.0f);
                glVertex3f(arrowHeight, len - arrowWidth, (GLfloat)0.0f);

                glVertex3f((GLfloat)0.0f, (GLfloat)0.0f, len);
                glVertex3f(arrowHeight, (GLfloat)0.0f, len - arrowWidth);
                glVertex3f(-arrowHeight, (GLfloat)0.0f, len - arrowWidth);
        glEnd();
        */
        glLineWidth((GLfloat)1.0);
        glEnable(GL_LIGHTING);
}

void MolRender::DrawMirrorPlain(CtrlInst* pCtrlInst) {
    int i = 0;
    AtomPosition3F pos;
    ChemAtom* atom = NULL;
    int count = pCtrlInst->GetSelectionUtil()->GetSelectedCount();
    if(count <= 1) return;

    glEnable(GL_BLEND);
    glDisable(GL_LIGHTING);
    glColor4f(1.0f, 1.0f, 1.0f, 0.7f);
    //glColor4f(0.75f, 0.75f, 0.75f, 0.5f);
    if(count == 2) {
        glLineWidth((GLfloat)4.0);
            glBegin(GL_LINES);
    }else {
        glBegin(GL_TRIANGLES);
    }
        glNormal3f(0, 0, 1);
        for(i = 0; i < pCtrlInst->GetSelectionUtil()->GetSelectedCount(); i++) {
            atom = &(pCtrlInst->GetRenderingData()->GetAtomBondArray()[pCtrlInst->GetSelectionUtil()->GetSelectedIndex(i)].atom);
            glVertex3f(atom->pos.x, atom->pos.y, atom->pos.z);
        }
    glEnd();

    if(count == 2) {
       glLineWidth((GLfloat)1.0);
    }
    glEnable(GL_LIGHTING);
    glDisable(GL_BLEND);
}

void MolRender::GetWorldCoord3(double& xWorld, double& yWorld, double& zWorld) {
        GLdouble modelview[16];
        GLdouble projection[16];
        GLint viewport[4];
        GLdouble xWin, yWin;
        GLdouble x0, y0, z0, x1, y1, z1;
        double x2, y2, z2;
        double x3, y3, z3;

        glGetIntegerv(GL_VIEWPORT, viewport);
        glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
        glGetDoublev(GL_PROJECTION_MATRIX, projection);

        xWin = xWorld;
        //yWin = viewport[3] - yWorld - 1;
        yWin = viewport[3] - yWorld;

        gluUnProject((GLdouble)xWin, (GLdouble)yWin, (GLdouble)0.0, modelview, projection, viewport, &x0, &y0, &z0);
        gluUnProject((GLdouble)xWin, (GLdouble)yWin, (GLdouble)1.0, modelview, projection, viewport, &x1, &y1, &z1);

        x3 = x1 - x0;
        y3 = y1 - y0;
        z3 = z1 - z0;

        z2 = -(x0 * x3 - z0 * x3 * x3 / z3 + y0 * y3 - z0 * y3 * y3 / z3 )/((x3*x3+y3*y3+z3*z3)/z3);
        x2 = x0 + (z2 - z0) * x3 / z3;
        y2 = y0 + (z2 - z0) * y3 / z3;

        xWorld = x2;
        yWorld = y2;
        zWorld = z2;

}

void MolRender::GetWorldCoord(double& xWorld, double& yWorld, double& zWorld) {
        GLdouble modelview[16];
        GLdouble projection[16];
        GLint viewport[4];
        GLdouble xWin, yWin;
        GLdouble x1, y1, z1, x2, y2, z2;
        double x0, y0, z0;

        glGetIntegerv(GL_VIEWPORT, viewport);
        glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
        glGetDoublev(GL_PROJECTION_MATRIX, projection);

        xWin = xWorld;
        //yWin = viewport[3] - yWorld - 1;
        yWin = viewport[3] - yWorld;

        gluUnProject((GLdouble)xWin, (GLdouble)yWin, (GLdouble)0.0, modelview, projection, viewport, &x1, &y1, &z1);
        gluUnProject((GLdouble)xWin, (GLdouble)yWin, (GLdouble)1.0, modelview, projection, viewport, &x2, &y2, &z2);

        //wxLogError(wxString::Format(wxT("x1 xWorld:%f, yWorld:%f, zWorld:%f"), x1, y1, z1));
        //wxLogError(wxString::Format(wxT("x2 xWorld:%f, yWorld:%f, zWorld:%f"), x2, y2, z2));

        x0 = x2 - x1;
        y0 = y2 - y1;
        z0 = z2 - z1;
        double t = x0 * x0 + y0 * y0 + z0 * z0;
        if(fabs(t) > BP_PREC) {
                t = -(x0 * x1 + y0 * y1 + z0 * z1) / t;
                xWorld = x0 * t + x1;
                yWorld = y0 * t + y1;
                zWorld = z0 * t + z1;
                wxLogError(wxString::Format(wxT("xw xWorld:%f, yWorld:%f, zWorld:%f"), (xWorld - x1)/x0, (yWorld - y1)/y0, (zWorld - z1)/z0));
        }else {
                xWorld = x1;
                yWorld = y1;
                zWorld = z1;
        }
}


void MolRender::GetWorldCoord2(double& xWorld, double& yWorld, double& zWorld) {
        GLdouble modelview[16];
        GLdouble projection[16];
        GLint viewport[4];
        GLdouble xWin, yWin;
        GLdouble xTemp0, yTemp0, zTemp0, xTemp1, yTemp1, zTemp1;

        glGetIntegerv(GL_VIEWPORT, viewport);
        glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
        glGetDoublev(GL_PROJECTION_MATRIX, projection);

        double xTemp2, yTemp2, zTemp2;

        xWin = xWorld;
        //yWin = viewport[3] - yWorld - 1;
        yWin = viewport[3] - yWorld;
        gluUnProject((GLdouble)xWin, (GLdouble)yWin,(GLdouble)0.0, modelview, projection, viewport, &xTemp0, &yTemp0, &zTemp0);
        gluUnProject((GLdouble)xWin, (GLdouble)yWin,(GLdouble)1.0, modelview, projection, viewport, &xTemp1, &yTemp1, &zTemp1);

        zTemp2 = zWorld;
        if(zTemp1 != zTemp0) {
                xTemp2 = xTemp0 + (zTemp2 - zTemp0) * (xTemp1 - xTemp0)/ (zTemp1 - zTemp0);
                yTemp2 = yTemp0 + (zTemp2 - zTemp0) * (yTemp1 - yTemp0)/ (zTemp1 - zTemp0);
                //wxLogError(wxString::Format(wxT("y:%f, xWorld:%f, yWorld:%f, zWorld:%f"), rotAngle.y, xWorld, yWorld, zTemp2));

                xWorld = (float)xTemp2;
                yWorld = (float)yTemp2;
                zWorld = (float)zTemp2;
        }else {
                xWorld = (float)(xTemp0);
                yWorld = (float)(yTemp0);
                zWorld = (float)(zTemp0);
        }
}

void MolRender::GetWindowCoord(double &xCoord, double &yCoord, double &zCoord)
{
        GLdouble modelview[16];
        GLdouble projection[16];
        GLint viewport[4];
        GLdouble xTemp, yTemp, zTemp;

        glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
        glGetDoublev(GL_PROJECTION_MATRIX, projection);
        glGetIntegerv(GL_VIEWPORT, viewport);

        gluProject(xCoord, yCoord, zCoord, modelview, projection, viewport, &xTemp, &yTemp, &zTemp);
        xCoord = xTemp;
        yCoord = viewport[3] - yTemp- 1;
        zCoord = zTemp;
}

AtomPosition MolRender::Project(AtomPosition atomPos) {
        GLdouble modelview[16];
        GLdouble projection[16];
        GLint viewport[4];
        GLdouble xWin, yWin, zWin;
        AtomPosition winPos;

        glGetIntegerv(GL_VIEWPORT, viewport);
        glGetDoublev(GL_PROJECTION_MATRIX, projection);
        //pCtrlInst->camera.Transpose(modelView);
        glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

        gluProject((GLdouble)atomPos.x, (GLdouble)atomPos.y, (GLdouble)atomPos.z, modelview, projection, viewport, &xWin, &yWin, &zWin);

        yWin = viewport[3] - yWin;
        winPos.x = (double)xWin;
        winPos.y = (double)yWin;
        winPos.z = (double)zWin;

        return winPos;
}

AtomPosition MolRender::UnProject(AtomPosition3L winCoord) {
        GLdouble modelview[16];
        GLdouble projection[16];
        GLint viewport[4];
        GLdouble xAtom, yAtom, zAtom;
        AtomPosition atomPos;

        glGetIntegerv(GL_VIEWPORT, viewport);
        glGetDoublev(GL_PROJECTION_MATRIX, projection);
        //pCtrlInst->camera.Transpose(modelView);
        glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

        winCoord.y = viewport[3] - winCoord.y;

        gluUnProject((GLdouble)winCoord.x, (GLdouble)winCoord.y, (GLdouble)winCoord.z, modelview, projection, viewport, &xAtom, &yAtom, &zAtom);

        atomPos.x = (double)xAtom;
        atomPos.y = (double)yAtom;
        atomPos.z = (double)zAtom;

        return atomPos;
}
