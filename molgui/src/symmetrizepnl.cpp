/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

/*
 * symmetrizepnl.cpp
 *
 *  Created on: 2010-8-2
 *      Author: allan
 */

/* measurepnl.cpp $Revision: 7.5 Patch(7.5): 591_new_structre $ */
#include "symmetrizepnl.h"

#include "tools.h"
#include "manager.h"
#include "molvieweventhandler.h"
#include "constraindata.h"
#include "envutil.h"  //SONG add the GetMolcasDir function to compile Molcas
#include "resource.h"
#include "dlgjobname.h"
#include "molfile.h"
#include "fileutil.h"
#include "stringutil.h"

#ifndef SSDLG_H
#define SSDLG_H
#include "envutil.h"
int BT_OK = wxNewId();
int BT_CANCLE = wxNewId();

class ssdlg : public wxDialog {
public :
        ssdlg (wxWindow * parent, wxWindowID id, const wxString& title)
                                        : wxDialog(parent, id, title, wxDefaultPosition, wxSize(280, 230)) {
                btnOk = new wxButton(this, BT_OK, wxT("Ok"));

                btnOk->Enable(false);

                btnCancel = new wxButton(this, BT_CANCLE, wxT("Cancel"));

                wxBoxSizer* bs=new wxBoxSizer(wxVERTICAL);

                wxStaticText* label=new wxStaticText(this,wxID_ANY,wxT("Approximate higher-order point group :"), wxDefaultPosition, wxSize(270, 20));
                bs->Add(label, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);

                m_lcJobName = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxSize(270, 120));
                bs->Add(m_lcJobName, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);

                wxBoxSizer * hbs = new wxBoxSizer(wxHORIZONTAL);
                hbs->AddStretchSpacer(1);

                hbs->Add(btnOk, 0, wxALIGN_CENTER_HORIZONTAL | wxALL);
                hbs->Add(btnCancel, 0, wxALIGN_CENTER_HORIZONTAL | wxALL);

                bs->Add(hbs, wxALIGN_CENTER_HORIZONTAL);

                SetSizer(bs);

        }

        void AddFile() {

                wxString tmp;
                wxArrayString exist;

                wxString dir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();

//                for (int i=0; i<filenum; ++i) {
//                        if( wxFileExists( dir+argFiles[i] ) )
//                                exist.Add(arg[i]);
//                }


                wxString f = wxFindFirstFile(dir + wxT("arg.arg_*"));

                while(!f.IsEmpty()) {

                        exist.Add(f.AfterLast('_'));
                        f = wxFindNextFile();
                }

                wxString* str = new wxString[exist.GetCount()];
                for(unsigned int i=0;i<exist.GetCount();i++) {
                        str[i] = exist[i];
                }
                m_lcJobName->InsertItems(exist.GetCount(), str, 0);

                if (0 != exist.GetCount()) {
                        m_lcJobName->SetSelection(0);

                        name = dir + wxT("arg.arg_") + m_lcJobName->GetString(0);

                        btnOk->Enable(true);
                }

        }

        void Remove() {
                wxString dir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();

                wxString f = wxFindFirstFile(dir + wxT("arg.arg*"));
                while(!f.IsEmpty()) {
                        wxRemoveFile(f);
                        f = wxFindNextFile();
                }
        }

        wxString GetName() {
                return name;
        }

        void OnButton(wxCommandEvent& event) {

                int id = event.GetId();
                if (id == BT_OK) {
                        wxString jobName = name;
                        if (jobName.IsEmpty()) {
                                        return;
                        }
                        EndModal(wxID_OK);
                } else {
                        EndModal(wxID_CANCEL);
                }
        }

        void OnChoose(wxCommandEvent& event) {
                wxString dir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();
                name = dir + wxT("arg.arg_") + m_lcJobName->GetString(event.GetSelection());
                btnOk->Enable(true);
        }

    DECLARE_EVENT_TABLE();

private :
        wxButton * btnOk;
        wxButton * btnCancel;
        wxListBox * m_lcJobName;
        wxString name;
};
#endif

BEGIN_EVENT_TABLE(ssdlg,wxDialog)
                                EVT_LISTBOX(wxID_ANY, ssdlg::OnChoose)
                                EVT_BUTTON(wxID_ANY, ssdlg::OnButton)
END_EVENT_TABLE()

const int toln = 5;

const wxString tols[]= {
                wxT("Very tight (0.0001)"),
                wxT("Tight (0.001)"),
                wxT("Default (0.01)"),
                wxT("Loose (0.1)"),
                wxT("Very loose (0.3)")
};
const string tolf[] = {
                "0.0001" ,
                "0.001",
                "0.01",
                "0.1",
                "0.3"
};

int ID_CTRL_CHECK_GLOBAL = wxNewId();
int ID_CTRL_CHECK_LOCAL = wxNewId();
int ID_CTRL_COMBOX_TOLARENCE = wxNewId();
int ID_CTRL_COMBOX_HIGHER_ORDER = wxNewId();
// int ID_CTRL_BTN_SYMMETRIZE = wxNewId();

BEGIN_EVENT_TABLE(SymmetrizePnl, wxPanel)
        // EVT_BUTTON(ID_CTRL_BTN_SYMMETRIZE, SymmetrizePnl::OnBtnSym)
        EVT_CHECKBOX(ID_CTRL_CHECK_GLOBAL, SymmetrizePnl::OnChkGlo)
        EVT_CHECKBOX(ID_CTRL_CHECK_LOCAL, SymmetrizePnl::OnChkLocl)
        EVT_COMBOBOX(ID_CTRL_COMBOX_TOLARENCE, SymmetrizePnl::OnCombTol)
        EVT_COMMAND_LEFT_CLICK(ID_CTRL_COMBOX_HIGHER_ORDER, SymmetrizePnl::OnButton)
END_EVENT_TABLE()

SymmetrizePnl::SymmetrizePnl(wxWindow* parent, wxWindowID id, const wxPoint& pos,
                const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name) {

        m_isError = false;
        m_isSelectionChanged = false;
        m_isMeasure = true;
        m_pCtrlInst = NULL;
}
wxString kong[] = { wxT("") };

void SymmetrizePnl::InitPanel(void) {

        wxBoxSizer* pBsMain = new wxBoxSizer(wxHORIZONTAL);
        this->SetSizer(pBsMain);

        wxStaticText* pStTol = new wxStaticText(this, -1, wxT("Tolerance:"));
//        wxStaticText* pStType = new wxStaticText(this, -1, wxT("Constrain to subgroup:"));


        m_pCkboxGlobal = new wxCheckBox(this, ID_CTRL_CHECK_GLOBAL, wxT("Global"));
        m_pCkboxLocal = new wxCheckBox(this, ID_CTRL_CHECK_LOCAL, wxT("Local"));
        m_pCkboxLocal->SetValue(true);

        m_pComboxTol = new wxComboBox(this, ID_CTRL_COMBOX_TOLARENCE, tols[2], wxDefaultPosition,
                        wxSize(200, 30), 5, tols, wxCB_READONLY);

//        m_pComboxHigher = new wxComboBox(this, ID_CTRL_COMBOX_TOLARENCE, wxT(""), wxDefaultPosition,
//                        wxSize(100, 30), 1, kong, wxCB_READONLY);

        // m_pBtnSubmit = new wxButton(this, ID_CTRL_BTN_SYMMETRIZE, wxT("Symmetrize"));

        wxBoxSizer * bs = new wxBoxSizer(wxHORIZONTAL);

        bs->Add(m_pCkboxGlobal);
        bs->Add(m_pCkboxLocal);

        pBsMain->Add(bs, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        pBsMain->AddStretchSpacer(1);
        pBsMain->Add(pStTol, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        pBsMain->Add(m_pComboxTol, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

//        pBsMain->Add(pStType, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
//        pBsMain->Add(m_pComboxHigher, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        // pBsMain->Add(m_pBtnSubmit, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        tolerance = "0.01";
}

void SymmetrizePnl::SetMeasureOrConstrain(bool isMeasure) {

        m_isMeasure = isMeasure;
        //EnableControls(isMeasure ? ACTION_MEASURE:ACTION_CONSTRAIN);
        Layout();
        Refresh();
}


void SymmetrizePnl::SetCtrlInst(CtrlInst* pCtrlInst) {
        m_pCtrlInst = pCtrlInst;
}

void SymmetrizePnl::ClearSelection(void) {
        if (m_pCtrlInst) {
                m_pCtrlInst->GetSelectionUtil()->ClearSelection();
        }

        m_mcType = m_isMeasure ? (int) MEASURE_POSITION : (int) CONSTRAIN_POSITION;
        //m_mcLabel = m_isMeasure ? wxT("Position(") : wxT("Constraint(");
        m_mcLabel = wxT("Position(");
        if (m_pCkboxGlobal->GetValue()) {
                m_pCkboxGlobal->SetValue(false) ;
                m_pCkboxLocal->SetValue(true);
        }
        UpdateMCType(1);
        SetMeasureOrConstrain(m_isMeasure);
}

void SymmetrizePnl::AddSelectedIndex(int selectedAtomIndex) {

        if (m_pCkboxGlobal->GetValue()) {
                m_pCkboxGlobal->SetValue(false) ;
                m_pCkboxLocal->SetValue(true);
        }



        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        int selectedMax = pSelUtil->GetSelectedCount() + 1;
        m_mcLabel = wxEmptyString;

        if (pSelUtil->IsSelected(selectedAtomIndex)) {
                pSelUtil->RemoveSelected(selectedAtomIndex);
        } else if (pSelUtil->AddSelectedIndexLimited(selectedMax, selectedAtomIndex)
                        && pSelUtil->GetSelectedCount() > 0) {
                DoMeasure();
        }

        if (pSelUtil->GetSelectedCount() <= 0) {

        }

        UpdateMCType(selectedMax);
        SetMeasureOrConstrain(m_isMeasure);
        if (!m_isMeasure && pSelUtil->GetSelectedCount() > 0) {
                UpdateConsFreezeStatus();
                //wxMessageBox(wxString::Format(wxT("m_cfStatus=%d"),m_cfStatus));
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

void SymmetrizePnl::AddSelectedIndex(int selectedAtomIndex[2]) {

        if (m_pCkboxGlobal->GetValue()) {
                m_pCkboxGlobal->SetValue(false) ;
                m_pCkboxLocal->SetValue(true);
        }

        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        int selectedMax = pSelUtil->GetSelectedCount() + 2;

        bool addSuccess = false;
        if (pSelUtil->IsSelected(selectedAtomIndex[0])) {
                pSelUtil->RemoveSelected(selectedAtomIndex[0]);
        } else if (pSelUtil->AddSelectedIndexLimited(selectedMax, selectedAtomIndex[0])
                        && pSelUtil->GetSelectedCount() > 0) {
                addSuccess = true;
                DoMeasure();
        }
        if (pSelUtil->IsSelected(selectedAtomIndex[1])) {
                pSelUtil->RemoveSelected(selectedAtomIndex[1]);
        } else if (pSelUtil->AddSelectedIndexLimited(selectedMax, selectedAtomIndex[1])
                        && pSelUtil->GetSelectedCount() > 0) {
                addSuccess = true;
                DoMeasure();
        }


        m_mcLabel = wxEmptyString;

        if (addSuccess && pSelUtil->GetSelectedCount() > 0) {
                DoMeasure();
        }
        if (pSelUtil->GetSelectedCount() <= 0) {
        }
        selectedMax
                        = pSelUtil->GetSelectedCount() > 0 ? pSelUtil->GetSelectedCount()
                                        : 1;
        UpdateMCType(selectedMax);
        SetMeasureOrConstrain(m_isMeasure);
        if (!m_isMeasure && pSelUtil->GetSelectedCount() > 0) {
                UpdateConsFreezeStatus();
                //wxMessageBox(wxString::Format(wxT("m_cfStatus=%d"),m_cfStatus));
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

void SymmetrizePnl::DoPositionChanged(int ctrlId, bool isSpinBtn) {
        int index = 0;
        double offset = 0;
        double currValue = 0, oldValue = 0;
        wxString str = wxEmptyString;
        static bool busy = false;
        if (busy)
                return;
        busy = true;
        if (isSpinBtn) {

                str.ToDouble(&oldValue);
                //
        } else {
//                str = m_pTcPoss[index]->GetValue();

        }
        //wxMessageBox(wxString::Format(wxT("curr %.3f  = old %.3f"), currValue, oldValue));
        offset = currValue - oldValue;

        if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() == 1) {
                if (1) {
                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                        m_pCtrlInst->GetSelectionUtil()->GetSelectedIndex(0),
                                        index, offset);
                } else {
                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                        BP_NULL_VALUE, index, offset);
                }
                MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        }
        AtomPosition position;
        if (!m_isMeasure && m_cfStatus) {
                m_pCtrlInst->GetSelectedPosition(0, position);
                UpdateFreezeAtom(position);
        }
        busy = false;
}

// here we note that the angle is accurate at 10-5 precision
MeasureInfo SymmetrizePnl::DoBondChange(float value) {
        double oldValue = BP_NULL_VALUE;
        double newValue = value;
        MeasureInfo mInfo = MEASURE_SUCCESS;
        SelectionUtil* selUtil = m_pCtrlInst->GetSelectionUtil();

        switch (m_mcType) {
        case MEASURE_DISTANCE:
                mInfo = ChangeBondDistance(newValue, selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                1);
                break;
        case MEASURE_ANGLE:
                SetAngleRange(newValue);
                if (newValue < 0.00001) {
                        newValue = 0.00001;
                } else if (newValue > 179.99999 && newValue <= 180.0) {
                        newValue = 179.99999;
                } else if (newValue > 180.0 && newValue < 180.00001) {
                        newValue = 180.00001;
                } else if (newValue > 359.99999) {
                        newValue = 359.99999;
                }

                m_pCtrlInst->GetOldAngleValue(selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(2),
                                oldValue);
                mInfo = ChangeBondAngle(newValue, oldValue,
                                selUtil->GetSelectedIndex(0), selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(2),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                1);
                if (mInfo == MEASURE_SUCCESS) {
                        m_pCtrlInst->SetOldAngleValue(selUtil->GetSelectedIndex(0),
                                        selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(2),
                                        newValue);
                }
                break;
        case MEASURE_DIHEDRAL:
                mInfo = ChangeDihedral(newValue, selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(2),
                                selUtil->GetSelectedIndex(3),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                1);
                break;
        default:
                break;
        }

        return mInfo;
}


void SymmetrizePnl::UpdateMCType(int selectedMax) {
        m_mcLabel
                        += m_pCtrlInst->GetSelectionUtil()->GetSelectionLabel(selectedMax)
                                        + wxT(")");
}

void SymmetrizePnl::DoMeasure(void) {
        float value = 0.0;
        double newValue = 0.0;
        double oldValue = 0;
        MeasureInfo mInfo = MEASURE_SUCCESS;
        //------------xia--------------
        m_pCtrlInst->SaveHistroyFile();
        //---------------------------
        AtomBondArray & atomBondArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* selUtil = m_pCtrlInst->GetSelectionUtil();

        switch (m_mcType) {
        case MEASURE_POSITION:
                m_initPos[0] = m_initPos[1] = m_initPos[2] = 0;
                if (m_pCtrlInst->GetSelectedPosition(0, m_initPos)) {
                        SetInitPositionValue(m_initPos);
                }
                break;
        case MEASURE_DISTANCE:
                newValue = MeasureBondDistance(selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1), atomBondArray);
                break;
        case MEASURE_ANGLE:
                m_pCtrlInst->GetOldAngleValue(selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(2),
                                oldValue);
                newValue = MeasureBondAngle(oldValue, selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(2),
                                atomBondArray);
                m_pCtrlInst->SetOldAngleValue(selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(2),
                                newValue);
                break;
        case MEASURE_DIHEDRAL:
                mInfo = MeasureDihedral(newValue, selUtil->GetSelectedIndex(0),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(2),
                                selUtil->GetSelectedIndex(3), atomBondArray);
                if (mInfo != MEASURE_SUCCESS) {
                        ShowMeasureInfo(mInfo);
                        return;
                }
                break;
        }
        value = (float) newValue;
        if (m_mcType != MEASURE_POSITION) {
                SetInitBondValue(value);
        }
}

void SymmetrizePnl::SetInitPositionValue(AtomPosition& atomPos) {

}

void SymmetrizePnl::SetInitBondValue(float value) {

}

void SymmetrizePnl::ShowMeasureInfo(MeasureInfo mInfo) {
        switch (mInfo) {
        case MEASURE_SUCCESS:
                break;
        case MEASURE_ERROR_IN_MEMORY_ALLOCATION:
                Tools::ShowError(wxT("Error in memory allocaiton!"));
                break;
        case MEASURE_ERROR_IN_BONDLENGTH:
                Tools::ShowError(wxT("Cannot change bond length!"));
                break;
        case MEASURE_ERROR_IN_BONDANGLE:
                Tools::ShowError(wxT("Cannot change bond angle!"));
                break;
        case MEASURE_ERROR_IN_DIHEDRAL:
                Tools::ShowError(wxT("Cannot change dihedral!"));
                break;
        case DIHEDRAL_CAN_NOT_DEFINE:
                Tools::ShowError(wxT("It's impossible to define the dihedral!"));
                break;
        default:
                break;
        }
}

void SymmetrizePnl::UpdateConsFreezeStatus() {
        ConstrainData& constrain =
                        m_pCtrlInst->GetRenderingData()->GetConstrainData();
        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        ConsElem ce;
        int i, index;
        if (pSelUtil->GetSelectedCount() <= 0)
                return;
        if (pSelUtil->GetSelectedCount() == 1) {
                index = pSelUtil->GetSelectedIndex(0);
                m_cfStatus = constrain.HasFreezeData(chemArray[index].atom.uniqueId);
        } else {
                for (i = 0; i < pSelUtil->GetSelectedCount(); i++) {
                        index = pSelUtil->GetSelectedIndex(i);
                        ce.elemInfos.Add(chemArray[index].atom.uniqueId);
                }
                m_cfStatus = constrain.HasConstrainData(ce);
        }
}

void SymmetrizePnl::UpdateConstrainData(double value) {
        ConstrainData& constrain =
                        m_pCtrlInst->GetRenderingData()->GetConstrainData();
        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        ConsElem ce;
        int i, index;
        if (pSelUtil->GetSelectedCount() <= 1)
                return;
        for (i = 0; i < pSelUtil->GetSelectedCount(); i++) {
                index = pSelUtil->GetSelectedIndex(i);
                ce.elemInfos.Add(chemArray[index].atom.uniqueId);
        }
        constrain.UpdateConstrainData(ce);
}

void SymmetrizePnl::UpdateFreezeAtom(AtomPosition pos) {
        ConstrainData& constrain =
                        m_pCtrlInst->GetRenderingData()->GetConstrainData();
        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        FreezeAtom fa;
        int index;
        if (pSelUtil->GetSelectedCount() != 1)
                return;
        index = pSelUtil->GetSelectedIndex(0);
        fa.uniqueId = chemArray[index].atom.uniqueId;
        fa.pos = pos;
        constrain.UpdateFreezeAtom(fa);
}

// setting the angle into the range of 0-360
void SymmetrizePnl::SetAngleRange(double & angle) {


        while (1) {

                if (angle >= 0.0 && angle <= 360.0) {
                        return;
                } else if (angle < 0.0) {
                        angle = angle + 360.0;
                } else {
                        angle = angle - 360.0;
                }
        }
}

void SymmetrizePnl::OnBtnSym(wxCommandEvent & event) {

    // int count;

    // SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();

    // RenderingData* renderingData = m_pCtrlInst ->GetRenderingData();

    // if(renderingData->GetAtomCount() <= 0) {
    //     Tools::ShowStatus(wxT("Please build a molecule at first!"));
    //     return;
    // }
    wxMessageBox(wxT("This part is not coded, or should be removed!"));
    return;

////////////////////////////////////////////////////////////////////////
///     Removed code
///
////////////////////////////////////////////////////////////////////////
//     count = pSelUtil->GetSelectedCount();

//     if ( count < 1) {
//         wxMessageBox(wxT("Please select atoms first!"));
//         return ;
//     }

//     int *p = new int[count];

//     for (int i = 0; i < count; i++) {

//         p[i]        = pSelUtil->GetSelectedIndex(i);

//     }

//     m_pCtrlInst->SetDirty(true);

//     MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();

//     wxString defDir;

//     defDir = Manager::Get()->GetProjectManager()->GetActiveProject()->GetPathWithSep();

//     wxString argName = defDir + wxT("arg.arg");

// //        wxMessageBox(argName);

//     MolFile::SaveFile(argName, m_pCtrlInst->GetRenderingData(), false);

//     ofstream out(argName.ToAscii(), ios::app);

//     out << "$BEGIN SYMMETRIZE" << endl;
//     if (m_pCkboxLocal->GetValue()) {
//         out << "LOCAL " << endl;
//         for (int i=0; i<count; ++i) {
//             out << " " << p[i]+1 << endl;
//         }
//         out << "END_LOCAL" << endl;
//     }

//     out << "SY_TOL " << tolerance << endl;

//     out << "$END SYMMETRIZE" << endl;
//     out.close();

//     wxString dirName = wxT("modules/symmetrize");

//     wxString procName = EnvUtil::GetProgramName(wxT("make_sy"));
//     wxString execProcName = FileUtil::CheckFile(dirName, procName, true);
//     if (StringUtil::IsEmptyString(execProcName)) {
//         return;
//     }
//     execProcName += wxT(" ") + argName;

//     wxExecute(execProcName, wxEXEC_SYNC);

//     wxString retName;

//     ssdlg dlg(this, -1, wxT(""));

//     dlg.AddFile();

//     if (wxID_OK == dlg.ShowModal()) {
//         retName = dlg.GetName();
//     }         else {
//         dlg.Remove();
//         return ;
//     }

//     m_pCtrlInst->DoSymme(retName);

//     dlg.Remove();

}
void SymmetrizePnl::DoGlo() {

        m_pCtrlInst->SaveHistroyFile();

        m_pCkboxLocal->SetValue(false);
        m_pCkboxGlobal->SetValue(true);

        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();

        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();

        int count;

        count = chemArray.GetCount();

        pSelUtil->ClearAll();

        for (int i=0; i<count; ++i) {

                pSelUtil->AddSelectedIndex(i, true);

        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();

}
void SymmetrizePnl::OnChkGlo(wxCommandEvent & event) {
        DoGlo();
}

void SymmetrizePnl::OnChkLocl(wxCommandEvent & event) {

        if (!m_pCkboxGlobal->GetValue()) {
                m_pCkboxLocal->SetValue(true);
                m_pCkboxGlobal->SetValue(false);
                return ;
        }

        m_pCtrlInst->SaveHistroyFile();

        m_pCkboxLocal->SetValue(true);
        m_pCkboxGlobal->SetValue(false);

        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();

        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();

        int count;

        count = chemArray.GetCount();

        pSelUtil->ClearAll();

        for (int i=0; i<count; ++i) {

                pSelUtil->RemoveSelected(i);

        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

void SymmetrizePnl::OnCombTol(wxCommandEvent & event) {
        tolerance = tolf[m_pComboxTol->GetCurrentSelection()];
}
void SymmetrizePnl::OnCombHigh(wxCommandEvent & event) {
}
void SymmetrizePnl::OnButton(wxCommandEvent & event) {
}
