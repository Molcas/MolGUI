/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "commons.h"

#include "fileutil.h"
#include "envutil.h"

#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/tokenzr.h>
#include <wx/arrimpl.cpp>
#include "resource.h"

ElemInfo aryElemInfo[ELEM_INFO_COUNT];

const wxString ELEM_NAMES[ELEM_COUNT+1] = {
        wxT("H"), wxT("He"),
        wxT("Li"), wxT("Be"),wxT("B"), wxT("C"), wxT("N"), wxT("O"), wxT("F"), wxT("Ne"),
        wxT("Na"), wxT("Mg"), wxT("Al"), wxT("Si"), wxT("P"), wxT("S"), wxT("Cl"), wxT("Ar"),

    wxT("K"), wxT("Ca"),
    wxT("Sc"), wxT("Ti"), wxT("V"), wxT("Cr"), wxT("Mn"), wxT("Fe"), wxT("Co"), wxT("Ni"), wxT("Cu"), wxT("Zn"),
    wxT("Ga"), wxT("Ge"), wxT("As"), wxT("Se"), wxT("Br"), wxT("Kr"),

        wxT("Rb"), wxT("Sr"),
    wxT("Y"), wxT("Zr"), wxT("Nb"), wxT("Mo"), wxT("Tc"), wxT("Ru"), wxT("Rh"), wxT("Pd"), wxT("Ag"), wxT("Cd"),
    wxT("In"), wxT("Sn"), wxT("Sb"), wxT("Te"), wxT("I"), wxT("Xe"),

    wxT("Cs"), wxT("Ba"),
        wxT("La"),
        wxT("Ce"), wxT("Pr"), wxT("Nd"), wxT("Pm"), wxT("Sm"), wxT("Eu"), wxT("Gd"),
        wxT("Tb"), wxT("Dy"), wxT("Ho"), wxT("Er"), wxT("Tm"), wxT("Yb"), wxT("Lu"),
    wxT("Hf"), wxT("Ta"), wxT("W"), wxT("Re"), wxT("Os"), wxT("Ir"), wxT("Pt"), wxT("Au"), wxT("Hg"),
        wxT("Tl"), wxT("Pb"), wxT("Bi"),  wxT("Po"), wxT("At"), wxT("Rn"),
        wxT("Fr"), wxT("Ra"),
        wxT("Ac"),
        wxT("Th"), wxT("Pa"), wxT("U"), wxT("Np"), wxT("Pu"), wxT("Am"), wxT("Cm"),
        wxT("Bk"), wxT("Cf"), wxT("Es"), wxT("Fm"), wxT("Md"), wxT("No"), wxT("Lr"),
        wxT("Rf"), wxT("Db"), wxT("Sg"), wxT("Bh"), wxT("Hs"), wxT("Mt"),wxT("X")
};

const ElemId NON_METALs[] = {
    ELEM_H, ELEM_B, ELEM_C, ELEM_N, ELEM_O, ELEM_F,
    ELEM_Si, ELEM_P, ELEM_S, ELEM_Cl,
    ELEM_As, ELEM_Se, ELEM_Br,
    ELEM_Te, ELEM_I,
    ELEM_At,
    ELEM_NULL
};

const ElemId INERT_GASSES[] = {
    ELEM_He, ELEM_Ne, ELEM_Ar, ELEM_Kr, ELEM_Xe, ELEM_Rn,
    ELEM_NULL
};

const ElemId MAIN_GROUP_METALS[] = {
    ELEM_Al, ELEM_Ga, ELEM_Ge,
    ELEM_In, ELEM_Sn, ELEM_Sb,
    ELEM_Tl, ELEM_Pb, ELEM_Bi, ELEM_Po,
    ELEM_NULL
};

const ElemId ALKALINE_METALS[] = {
    ELEM_Li, ELEM_Na, ELEM_K, ELEM_Rb, ELEM_Cs, ELEM_Fr,
    ELEM_NULL
};

const ElemId ALKALINE_EARTH_METALS[] = {
    ELEM_Be, ELEM_Mg, ELEM_Ca, ELEM_Sr, ELEM_Ba, ELEM_Ra,
    ELEM_NULL
};

const ElemId TRANSITION_ELEMS[] = {
    ELEM_Sc, ELEM_Ti, ELEM_V, ELEM_Cr, ELEM_Mn, ELEM_Fe, ELEM_Co, ELEM_Ni, ELEM_Cu, ELEM_Zn,
    ELEM_Y, ELEM_Zr, ELEM_Nb, ELEM_Mo, ELEM_Tc, ELEM_Ru, ELEM_Rh, ELEM_Pd, ELEM_Ag, ELEM_Cd,
    ELEM_La,
    ELEM_Hf, ELEM_Ta, ELEM_W, ELEM_Re, ELEM_Os, ELEM_Ir, ELEM_Pt, ELEM_Au, ELEM_Hg,
    ELEM_Ac,
        ELEM_Rf, ELEM_Db, ELEM_Sg, ELEM_Bh, ELEM_Hs, ELEM_Mt,
    ELEM_NULL
};

const ElemId LAAC_ELEMS[] = {
    ELEM_Ce, ELEM_Pr, ELEM_Nd, ELEM_Pm, ELEM_Sm, ELEM_Eu, ELEM_Gd, ELEM_Tb,
    ELEM_Dy, ELEM_Ho, ELEM_Er, ELEM_Tm, ELEM_Yb, ELEM_Lu,
        ELEM_Th, ELEM_Pa, ELEM_U, ELEM_Np, ELEM_Pu, ELEM_Am, ELEM_Cm, ELEM_Bk,
        ELEM_Cf, ELEM_Es, ELEM_Fm, ELEM_Md, ELEM_No, ELEM_Lr,ELEM_NULL
};

/**
 * wx version
 * slower than C version
 */

void InitElemsInfo(void) {

    wxString fileFullName=EnvUtil::GetPrgResDir()+wxTRUNK_MOL_DAT_PATH+wxT("elements.dat");


    wxFileInputStream fis(fileFullName);
    wxTextInputStream tis(fis);
        for(int i = 0; i < ELEM_INFO_COUNT; i++) {
        tis >> aryElemInfo[i].elemId;
        tis >> aryElemInfo[i].name;
        tis >> aryElemInfo[i].dispRadius;
                aryElemInfo[i].dispRadius *= 1.3f;
        tis >> aryElemInfo[i].colorR;
        tis >> aryElemInfo[i].colorG;
        tis >> aryElemInfo[i].colorB;
        tis >> aryElemInfo[i].covalentRadius;
        tis >> aryElemInfo[i].vanderRadius;
     }
         //DumpElemsInfo();
}

void DumpElemsInfo(void) {
        wxFileOutputStream fos(EnvUtil::GetLogDir() + wxT("/logs/elementsinfo.txt"));
        wxTextOutputStream tos(fos);

        for(int i = 0 ; i < ELEM_INFO_COUNT; i++) {
                tos << wxString::Format(wxT("%2d  %-3s  %.2f  %.2f  %.2f  %.2f  %.2f  %.2f\n"), aryElemInfo[i].elemId, aryElemInfo[i].name.c_str(),
                                aryElemInfo[i].dispRadius, aryElemInfo[i].colorR,
                                aryElemInfo[i].colorG, aryElemInfo[i].colorB, aryElemInfo[i].covalentRadius, aryElemInfo[i].vanderRadius);
        }
}

ElemInfo* GetElemInfo(ElemId elemId){
    return &aryElemInfo[elemId + ELEM_PREFIX];
}

ElemId GetElemId(wxString elemSymbol) {
    int i = 0;
    for(i = 0; i < ELEM_COUNT; i++) {
        if((ELEM_NAMES[i].Cmp(elemSymbol) == 0)||(ELEM_NAMES[i].Lower().Cmp(elemSymbol) == 0)) {
            return (ElemId)(i + 1);
        }
    }
    return ELEM_NULL;
}

AtomPosition3F::AtomPosition3F() {
        x = y = z = 0.0f;
}

float& AtomPosition3F::operator [](int i) {
    if(i == 0) {
        return x;
    }else if(i == 1) {
        return y;
    }else {
        return z;
    }
}

AtomPosition3F& AtomPosition3F::operator =(const AtomPosition3F& pos) {
        x = pos.x;
        y = pos.y;
        z = pos.z;
        return *this;
}

void AtomPosition3F::Normalize(void) {
        float squareRoot = sqrt(x * x + y * y + z * z);
        if(squareRoot - 0 > BP_PREC) {
                x /= squareRoot;
                y /= squareRoot;
                z /= squareRoot;
        }
}

void AtomPosition3F::Scale(float scale) {
        x *= scale;
        y *= scale;
        z *= scale;
}

wxString AtomPosition3F::ToString() {
        return wxString::Format(wxT("%.4f\t %.4f\t %.4f"), x, y, z);
}

AtomPosition3L::AtomPosition3L() {
        x = y = z = 0l;
}


long& AtomPosition3L::operator [](int i) {
    if(i == 0) {
        return x;
    }else if(i == 1) {
        return y;
    }else {
        return z;
    }
}

AtomPosition3L& AtomPosition3L::operator =(const AtomPosition3L& pos) {
        x = pos.x;
        y = pos.y;
        z = pos.z;
        return *this;
}

AtomPosition3L& AtomPosition3L::operator -(const AtomPosition3L& pos) {
        x -= pos.x;
        y -= pos.y;
        z -= pos.z;
        return *this;
}

wxString AtomPosition3L::ToString() {
        return wxString::Format(wxT("%ld\t %ld\t %ld"), x, y, z);
}

AtomPosition::AtomPosition() {
        x = y = z = 0.0;
}

double& AtomPosition::operator [](int i) {
    if(i == 0) {
        return x;
    }else if(i == 1) {
        return y;
    }else {
        return z;
    }
}

AtomPosition& AtomPosition::operator =(const AtomPosition& pos) {
        x = pos.x;
        y = pos.y;
        z = pos.z;
        return *this;
}

AtomPosition& AtomPosition::operator =(const wxString &info) {
        wxArrayString strArr = wxStringTokenize(info, wxT(" \t"), wxTOKEN_STRTOK);
        wxASSERT_MSG(strArr.GetCount() >= 3, wxT("The string cannot be converted to AtomPosition - ") + info);
        strArr[0].ToDouble(&x);
        strArr[1].ToDouble(&y);
        strArr[2].ToDouble(&z);
        return *this;
}

AtomPosition& AtomPosition::operator -(const AtomPosition& pos) {
        x -= pos.x;
        y -= pos.y;
        z -= pos.z;
        return *this;
}

wxString AtomPosition::ToString() {
        return wxString::Format(wxT("%.4f\t %.4f\t %.4f"), x, y, z);
}

wxString CheckImageFile(const wxString& fileName, bool needReport) {
    wxString baseDir= EnvUtil::GetPrgResDir() + platform::PathSep()+wxT("res")+platform::PathSep()+wxT("mol")+platform::PathSep()+wxT("images");
    return FileUtil::CheckFile(baseDir, fileName, needReport);
}

wxString CheckFontFile(const wxString& fileName) {
        wxString baseDir = EnvUtil::GetPrgResDir() + platform::PathSep()+wxT("res")+platform::PathSep()+wxT("mol")+platform::PathSep()+wxT("font");
        return FileUtil::CheckFile(baseDir,  fileName, true);
}
