/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include <wx/dialog.h>
#include "mopacparadlg.h"
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/object.h>
#include <wx/hyperlink.h>
#include <wx/msgdlg.h>
int        CTRL_DLGMOPAC_ID_CB_METHOD = wxNewId();
int        CTRL_DLGMOPAC_ID_CB_MULTI = wxNewId();
int        CTRL_DLGMOPAC_ID_TC_CHARGE = wxNewId();
int        CTRL_DLGMOPAC_ID_TC_OPTIONS = wxNewId();
//....................................................
BEGIN_EVENT_TABLE(CMopacParaDlg, wxDialog)
    EVT_BUTTON(wxID_OK, CMopacParaDlg::OnOK)
END_EVENT_TABLE()

CMopacParaDlg::CMopacParaDlg(wxWindow *parent, wxWindowID id, const wxString &caption,
                             const wxPoint &pos, const wxSize &size, long style, const wxString &name)
{
    InitParameter();
    m_pcbMethod = NULL;
    m_strMethodVal = m_strMethod[5]; // PM7
    m_pcbMulti = NULL;
    m_strMultiVal = m_strMulti[0]; // Singlet
    m_ptcCharge = NULL;
    m_ptcOptions = NULL;
    m_strOptionsVal = wxEmptyString;
    m_nCharge = 0;
    wxDialog::Create(parent, id, caption, pos, wxSize(300, 320), style, name);
    InitPanel();
}
void CMopacParaDlg::InitParameter()
{
    m_strMethod.Add(wxT("AM1"));
    m_strMethod.Add(wxT("MNDO"));
    m_strMethod.Add(wxT("MNDOD"));
    m_strMethod.Add(wxT("PM3"));
    m_strMethod.Add(wxT("PM6"));
    m_strMethod.Add(wxT("PM7"));
    m_strMethod.Add(wxT("RM1"));
    //...........................................
    m_strMulti.Add(wxT("Singlet"));
    m_strMulti.Add(wxT("Doublet"));
    m_strMulti.Add(wxT("Triplet"));
    m_strMulti.Add(wxT("Quartet"));
    m_strMulti.Add(wxT("Quintet"));
    m_strMulti.Add(wxT("Sextet"));
}
void CMopacParaDlg::InitPanel()
{
    wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);

    // parameter options.
    wxBoxSizer *boxMethodSizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *labelMethod = new wxStaticText( this, wxID_STATIC, wxT("Method:"), wxDefaultPosition, wxSize(80, 20), 0 );
    m_pcbMethod = new wxComboBox(this, CTRL_DLGMOPAC_ID_CB_METHOD, m_strMethodVal, wxDefaultPosition, wxSize(130, 25), m_strMethod, wxCB_DROPDOWN | wxCB_READONLY);
    boxMethodSizer->Add(labelMethod, 0, wxALIGN_LEFT | wxALL, 5);
    boxMethodSizer->Add(m_pcbMethod, 0, wxALIGN_LEFT | wxALL, 5);
    topSizer->Add(boxMethodSizer, 0, wxALIGN_LEFT | wxALL, 5);

    wxBoxSizer *boxMultiSizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *labelMulti = new wxStaticText( this, wxID_STATIC, wxT("Multiplicity:"), wxDefaultPosition, wxSize(80, 20), 0 );
    m_pcbMulti = new wxComboBox(this, CTRL_DLGMOPAC_ID_CB_MULTI, m_strMultiVal, wxDefaultPosition, wxSize(130, 25), m_strMulti, wxCB_DROPDOWN | wxCB_READONLY);
    boxMultiSizer->Add(labelMulti, 0, wxALIGN_LEFT | wxALL, 5);
    boxMultiSizer->Add(m_pcbMulti, 0, wxALIGN_LEFT | wxALL, 5);
    topSizer->Add(boxMultiSizer, 0, wxALIGN_LEFT | wxALL, 5);

    wxBoxSizer *boxChargeSizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *labelCharge = new wxStaticText( this, wxID_STATIC, wxT("Charge:"), wxDefaultPosition, wxSize(80, 20), 0 );
    m_ptcCharge = new wxTextCtrl(this, CTRL_DLGMOPAC_ID_TC_CHARGE, wxT("0"), wxDefaultPosition, wxSize(130, 25));
    boxChargeSizer->Add(labelCharge, 0, wxALIGN_LEFT | wxALL, 5);
    boxChargeSizer->Add(m_ptcCharge, 0, wxALIGN_LEFT | wxALL, 5);
    topSizer->Add(boxChargeSizer, 0, wxALIGN_LEFT | wxALL, 5);

    wxBoxSizer *boxOptionsSizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *labelOptions = new wxStaticText( this, wxID_STATIC, wxT("Options:"), wxDefaultPosition, wxSize(80, 20), 0 );
    m_ptcOptions = new wxTextCtrl(this, CTRL_DLGMOPAC_ID_TC_CHARGE, wxEmptyString, wxDefaultPosition, wxSize(130, 25));
    boxOptionsSizer->Add(labelOptions, 0, wxALIGN_LEFT | wxALL, 5);
    boxOptionsSizer->Add(m_ptcOptions, 0, wxALIGN_LEFT | wxALL, 5);
    topSizer->Add(boxOptionsSizer, 0, wxALIGN_LEFT | wxALL, 5);

    wxStaticText *copyright = new wxStaticText(this, wxID_STATIC, wxT("MOPAC (R) Stewart Computational Chemistry"));
    topSizer->Add(copyright, 0, wxALIGN_LEFT | wxALL, 7);

    wxHyperlinkCtrl *weblink = new wxHyperlinkCtrl(this, wxID_STATIC, wxT("http://openmopac.net"), wxT("http://openmopac.net"));
    topSizer->Add(weblink, 0, wxALIGN_LEFT | wxALL, 7);

    topSizer->Add(CreateButtonSizer(wxOK | wxCANCEL), wxSizerFlags().Right().Border(wxALL,5));

    SetSizer(topSizer);
    topSizer->SetSizeHints(this);
    Fit();
}
CMopacParaDlg::~CMopacParaDlg()
{
}
//**************************************************************************//
//Event handle functions
//***************************************************************************/
void CMopacParaDlg::OnOK(wxCommandEvent &event)
{
    m_strMethodVal = m_pcbMethod->GetValue();
    m_strMultiVal = m_pcbMulti->GetValue();
    wxString str = m_ptcCharge->GetValue();
    m_strOptionsVal = m_ptcOptions->GetValue();
    if(!str.IsEmpty()) {
        if(!str.ToLong(&m_nCharge, 10)) {
            ::wxMessageBox(wxT("The \'Charge\' must be integer value."));
            m_nCharge = 0;
            return;
        }
    }
    m_strOptionsVal.Replace(wxT(";"), wxT(" "));
    m_strOptionsVal.Replace(wxT(","), wxT(" "));
    //wxMessageBox(m_strOptionsVal);
    EndModal(wxID_OK);
    return;
}
