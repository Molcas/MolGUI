/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "measure.h"
#include "atommanuutils.h"
#include "renderingdata.h"
#include "measureutils.h"
/*******************************************************************
 * for calculate the angle between three atoms
 * the atom 's order is showed by the z-matirx: 3  2  (r3) 1 (a3)
 * return float: bond angle value in ordinary
 * so here the oldAngle should be in ordinary not the arc value
 ********************************************************************/
double MeasureBondAngle(double oldAngle, int atom1Index, int atom2Index, int atom3Index, const AtomBondArray& atomBondArray)
{
  double vec23_x;
  double vec23_y;
  double vec23_z;
  double vec21_x;
  double vec21_y;
  double vec21_z;
  double result;

  // if oldAngle is -1.0f, that means the oldAngle is in the initial state
  if ( fabs(1.0 + oldAngle) > MEASURE_PREC ) {
    return oldAngle;
  }

  vec23_x = atomBondArray[atom3Index].atom.pos.x - atomBondArray[atom2Index].atom.pos.x;
  vec23_y = atomBondArray[atom3Index].atom.pos.y - atomBondArray[atom2Index].atom.pos.y;
  vec23_z = atomBondArray[atom3Index].atom.pos.z - atomBondArray[atom2Index].atom.pos.z;

  vec21_x = atomBondArray[atom1Index].atom.pos.x - atomBondArray[atom2Index].atom.pos.x;
  vec21_y = atomBondArray[atom1Index].atom.pos.y - atomBondArray[atom2Index].atom.pos.y;
  vec21_z = atomBondArray[atom1Index].atom.pos.z - atomBondArray[atom2Index].atom.pos.z;


  result = (vec23_x*vec21_x + vec23_y*vec21_y + vec23_z*vec21_z)/( sqrt( vec23_x*vec23_x + vec23_y*vec23_y + vec23_z*vec23_z ) * sqrt (vec21_x*vec21_x + vec21_y*vec21_y + vec21_z*vec21_z) );
  /* result is the cosine of bond_angle value betwwen vec23 and vec21 */

  // here we add some protection to the return value
  if (fabs(result)<MEASURE_PREC)
    {
      result = 0.0;
    }
  else if ( fabs(1-fabs(result))<MEASURE_PREC)
    {
      if (result > 0)
        result = 1.0;
      else
        result = -1.0;
    }

  return acos(result)/RADIAN;

}



/*********************************************************************
   this function changes the coordinates of the atoms, while the bond
   angle is changing. two ways supported: first, there's group
   linking with the moving atom also moveing consistently; the other
   way, only the moving atom ssigned by the user moves. the switch of
   both of the two ways, is defined by bool methodSwitch.
**********************************************************************/
MeasureInfo ChangeBondAngle(double angle, double oldAngle, int atomStill, int atomAxis, int atomMove, AtomBondArray& atomBondArray, bool methodSwitch)
{

  int i;

  //all of the moving atom's information
  //is here. including the position of label. see the common.h
  MovingAtoms * movingAtoms = NULL;
  int movingAtomArrayLength = 0;


  //used to save the atomAxis' bond information
  ChemBond bondArrayStoreOfAtomAxis[ATOM_MAX_BOND_COUNT];



  // decide how to change the corrdinate
  // if methodSwitch is true, the corrdinate change only done to 2,3,4 atoms
  if (methodSwitch) {
    ChangeBondAngleCalculation (angle, oldAngle, atomStill, atomAxis, atomMove, movingAtoms, movingAtomArrayLength, atomBondArray);
    return   MEASURE_SUCCESS;
  }


  // copy the axis atom's bond information
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {
      bondArrayStoreOfAtomAxis[i] = atomBondArray[atomAxis].bonds[i];
    }


  // RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata0.dat"));



  /* change the connection to fit for the bond angle change */
  for (i=0; i<atomBondArray[atomAxis].bondCount; i++)
    {
      atomBondArray[atomAxis].bonds[i].atomIndex = BP_NULL_VALUE;
    }

  // RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata1.dat"));


  /* search the connection*/
  movingAtoms = SearchConnectedAtoms(atomBondArray, atomMove, &movingAtomArrayLength);



  // finally restore the information of breaking bond
  for (i=0; i<ATOM_MAX_BOND_COUNT; i++)
    {
      atomBondArray[atomAxis].bonds[i] = bondArrayStoreOfAtomAxis[i];
    }


  if ( movingAtoms == NULL)
    {
      return MEASURE_ERROR_IN_MEMORY_ALLOCATION;
    }




  // RenderingData::DumpRenderingData(atomBondArray, wxT("logs/dumprenderingdata2.dat"));


  /* if atomStill and atomMove is bonding into a circle, the bond angle can not change! */
  for (i=0; i<movingAtomArrayLength; i++)
    {
      if ( movingAtoms[i].atomLabel == atomStill)
        {

          // finally free the space used by the movingAtom
          if (movingAtoms != NULL) {
            free (movingAtoms);
            movingAtoms = NULL;
          }

          return MEASURE_ERROR_IN_BONDANGLE;
        }
    }



  /* calculate the coordinate */
  ChangeBondAngleCalculation (angle, oldAngle, atomStill, atomAxis, atomMove, movingAtoms, movingAtomArrayLength, atomBondArray);


  // finally free the space used by the movingAtom
  if (movingAtoms != NULL) {
    free (movingAtoms);
    movingAtoms = NULL;
  }

  return   MEASURE_SUCCESS;

}







/******************************************************************************
 *calculate the coordinate change made by the angle change between three atoms
 ******************************************************************************/
void ChangeBondAngleCalculation(double angle, double oldAngle, int atomStill, int atomAxis,int atomMove,  MovingAtoms * movingAtoms, int movingAtomArrayLength, AtomBondArray& atomBondArray)
{

  double angleX  = 0;
  double cosalfa = 0;
  int labelX = 0;

  double angleY  = 0;
  double cosbeta = 0;
  int labelY = 0;

  double angleZ  = 0;
  double cosgama = 0;
  int labelZ = 0;                // used in rotation around axis

  double angleOld;
  double angleR;                /*  value for rotation to change the angle*/

  int i;                        /* for counting the loop */
  double x1;                         /* the still atom */
  double y1;
  double z1;
  double x2;                        /* the axis atom */
  double y2;
  double z2;
  double x3;                        /* the moving atom*/
  double y3;
  double z3;

  x1 = atomBondArray[atomStill].atom.pos.x;
  y1 = atomBondArray[atomStill].atom.pos.y;
  z1 = atomBondArray[atomStill].atom.pos.z;
  x2 = atomBondArray[atomAxis].atom.pos.x;
  y2 = atomBondArray[atomAxis].atom.pos.y;
  z2 = atomBondArray[atomAxis].atom.pos.z;
  x3 = atomBondArray[atomMove].atom.pos.x;
  y3 = atomBondArray[atomMove].atom.pos.y;
  z3 = atomBondArray[atomMove].atom.pos.z;



  /*  move the atomAxis to the origin */
  x1 -= x2;
  y1 -= y2;
  z1 -= z2;
  x3 -= x2;
  y3 -= y2;
  z3 -= z2;
  if (movingAtomArrayLength != 0 && movingAtoms != NULL) {
    for (i=0; i< movingAtomArrayLength; i++) {
      movingAtoms[i].x -= x2;
      movingAtoms[i].y -= y2;
      movingAtoms[i].z -= z2;
    }
  }


  /* rotate around the z; move vector between stomStill and atomAxis
     to the xz plane*/
  if(fabs(x1) > MEASURE_PREC || fabs(y1) > MEASURE_PREC) {
    cosgama = x1/sqrt(x1*x1 + y1*y1);
    if (y1<0) //counter-clockwise
      angleZ =  acos(cosgama);
    else      //clockwise
      angleZ = -acos(cosgama);
    RotationByZ (&x1, &y1, angleZ);
    RotationByZ (&x3, &y3, angleZ);

    if (movingAtomArrayLength != 0 && movingAtoms != NULL) {
      GroupRotationByZ (movingAtoms,movingAtomArrayLength, angleZ);
    }
  }
  else
    {
      labelZ = 1;
    }

  //   printf ("around the Z:\n");
  //  printf ("the x1 and y1 and z1 value is: %f, %f, %f\n", x1, y1, z1);
  //  printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);



  /* rotate around the y; move vector between stomStill and atomAxis
     to the x axis*/
  if(fabs(x1) > MEASURE_PREC || fabs(z1) > MEASURE_PREC) {
    cosbeta = x1/sqrt (x1*x1 + z1*z1);
    if (z1>0) //counter-clockwise
      angleY =  acos(cosbeta);
    else      //clockwise
      angleY = -acos(cosbeta);
    RotationByY (&x1, &z1, angleY);
    RotationByY (&x3, &z3, angleY);
    if (movingAtomArrayLength != 0 && movingAtoms != NULL) {
      GroupRotationByY (movingAtoms,movingAtomArrayLength, angleY);
    }
  }
  else
    {
      labelY = 1;
    }

  //   printf ("around the Y:\n");
  //  printf ("the x1 and y1 and z1 value is: %f, %f, %f\n", x1, y1, z1);
  //  printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);


  /* rotate around the x; move vector between atomMove and atomAxis
     to the xy plane*/
  if(fabs(y3) > MEASURE_PREC || fabs(z3) > MEASURE_PREC) {
    cosalfa = y3/sqrt (y3*y3 + z3*z3);
    if (z3<0) //counter-clockwise
      angleX =  acos(cosalfa);
    else      //clockwise
      angleX = -acos(cosalfa);

    RotationByX (&y3, &z3, angleX);
    if (movingAtomArrayLength != 0 && movingAtoms != NULL) {
      GroupRotationByX (movingAtoms,movingAtomArrayLength, angleX);
    }
  }
  else
    {
      labelX = 1;
    }

  //  printf ("around the X:\n");
  //  printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);


  /* decide the position of atomMove according to the oldAngle
     here the oldAngle is set to 0-360, or -1.0. if oldAngle is larger
     than 180.0, then move it to the -y*/
    if (oldAngle > 180.0 && oldAngle < 360.0) {
        y3 = -y3;
        if (movingAtomArrayLength != 0 && movingAtoms != NULL) {
            for (i=0; i<movingAtomArrayLength; i++) {
                movingAtoms[i].y = -(movingAtoms[i].y);
            }
        }
    }


  /* rotate the angle around the z axis to what you want*/
  /* the angle here is restricted to 0-360*/

  // the oldAngle is in its original state,
  // so we strik up some calculation
  angleOld = oldAngle;
  if ( fabs(1.0 + angleOld) < MEASURE_PREC ) {
    angleOld = MeasureBondAngle(oldAngle, atomStill, atomAxis, atomMove, atomBondArray);
  }

  angleR = (angle - angleOld)*RADIAN;
//     printf ("angleOld , angle and angleR is:%f, %f, %f\n", angleOld, angle, angleR/RADIAN);

  if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
    RotationByZ (&x3, &y3, angleR);
  }
  else {
    GroupRotationByZ (movingAtoms,movingAtomArrayLength, angleR);
  }

//  printf ("after changing with angleR:\n");
//  printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);


  /* restore the operation of changing y*/
  if (oldAngle > 180.0 && oldAngle < 360.0) {
      y3 = -y3;
      if (movingAtomArrayLength != 0 && movingAtoms != NULL) {
          for (i=0; i<movingAtomArrayLength; i++) {
              movingAtoms[i].y = -(movingAtoms[i].y);
          }
      }
  }

  /* restore the rotation around the x*/
  if (labelX != 1) {
    angleX = (-angleX);
    if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
      RotationByX (&y3, &z3, angleX);
    }
    else {
      GroupRotationByX (movingAtoms,movingAtomArrayLength, angleX);
    }
  }

  //  printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);

  /* restore the rotation around the y*/
  if (labelY != 1) {
    angleY = (-angleY);
    if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
      RotationByY (&x3, &z3, angleY);
    }
    else {
      GroupRotationByY (movingAtoms,movingAtomArrayLength, angleY);
    }
  }

  //    printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);

  /* restore the rotation around the Z*/
  if (labelZ != 1) {
    angleZ = (-angleZ);
    if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
      RotationByZ (&x3, &y3, angleZ);
    }
    else {
      GroupRotationByZ (movingAtoms,movingAtomArrayLength, angleZ);
    }
  }

  //    printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);

  /*  move the atomAxis back */
  if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
    x3 += x2;
    y3 += y2;
    z3 += z2;
  }
  else {
    for (i=0; i< movingAtomArrayLength; i++) {
      movingAtoms[i].x += x2;
      movingAtoms[i].y += y2;
      movingAtoms[i].z += z2;
    }
  }

  //    printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);

  //value to write back
  if (movingAtomArrayLength == 0 || movingAtoms == NULL) {
    atomBondArray[atomMove].atom.pos.x = x3;
    atomBondArray[atomMove].atom.pos.y = y3;
    atomBondArray[atomMove].atom.pos.z = z3;
  }
  else {
    for (i=0; i< movingAtomArrayLength; i++) {
      atomBondArray[movingAtoms[i].atomLabel].atom.pos.x = movingAtoms[i].x;
      atomBondArray[movingAtoms[i].atomLabel].atom.pos.y = movingAtoms[i].y;
      atomBondArray[movingAtoms[i].atomLabel].atom.pos.z = movingAtoms[i].z;
    }
  }


//  printf ("the end of this function:\n");
//  printf ("the x1 and y1 and z1 value is: %f, %f, %f\n", x1, y1, z1);
//  printf ("the x3 and y3 and z3 value is: %f, %f, %f\n", x3, y3, z3);


}
