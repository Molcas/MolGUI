/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "hbonds.h"
#include "measure.h"

static  int InitialHBlists (const AtomBondArray & atomBondArray, HBondsInfo * HBondsList);


/********************************************************************
 since we use the list to present the data structure, so here we
 provide some functions to deal with the hadling to the list
 ********************************************************************/
static int Initial (HBondsInfo * HBondsList);
static int Insert (HBondsInfo * HBondsList, int acceptor, int hydrogen, int donator);


// used to initialize the head of HBonds List
int Initial (HBondsInfo * HBondsList)
{

    HBondsInfo * newPtr;
    newPtr = (HBondsInfo *)malloc(sizeof(HBondsInfo));
    if (newPtr == NULL) {
        return 1;
    } else {
        newPtr->acceptor = BP_NULL_VALUE;
        newPtr->hydrogen = BP_NULL_VALUE;
        newPtr->donator  = BP_NULL_VALUE;
        newPtr->next     = NULL;
    }

    HBondsList = newPtr;

    return 0;


}

// insert the element into the node. node can be any, head until the last node
int Insert (HBondsInfo * HBondsList, int acceptor, int hydrogen, int donator)
{

    HBondsInfo * currentPtr;
    HBondsInfo * newPtr;
    currentPtr = HBondsList;

    // search for the last node
    // head node
    if (currentPtr->next == NULL) {
        currentPtr->acceptor = acceptor;
        currentPtr->hydrogen = hydrogen;
        currentPtr->donator  = donator;
    }
    // other nodes
    else {
        while (currentPtr->next != NULL) {
            currentPtr = currentPtr->next;
        }

        // stuff the values inside the new node
        newPtr = (HBondsInfo *)malloc(sizeof(HBondsInfo));
        if (newPtr == NULL) {
            return 1;
        } else {
            newPtr->acceptor = acceptor;
            newPtr->hydrogen = hydrogen;
            newPtr->donator  = donator;
            newPtr->next     = NULL;
            currentPtr->next = newPtr;
        }
    }

    return 0;

}


/********************************************************************
 this function used to fetch the basic list for the hydrogen bonds
 it searchs all hydrogen atoms for those which has such connectivity:
   H-X (X = O, F or N). X is the donator, H is the hydrogen
 these hydrogen atoms will be stored in a list of HBondsInfo kind.
*********************************************************************/
int InitialHBlists (const AtomBondArray & atomBondArray, HBondsInfo * HBondsList)
{

    int natoms;
    int i;
    int j;
    int linkAtom;

    // initilize
    natoms = atomBondArray.GetCount();
    // memory is not enough
    if (Initial (HBondsList) == 1 ) {
        return 1;
    }

    // decide that if the H atom is what we want
    for (i=0; i<natoms; i++) {
        if ((ElemId)atomBondArray[i].atom.elemId == ELEM_H) {
            for (j=0; j<ATOM_MAX_BOND_COUNT; j++) {
                linkAtom = atomBondArray[i].bonds[j].atomIndex;
                if(linkAtom == BP_NULL_VALUE) {
                    break;
                }
                if ((ElemId)atomBondArray[linkAtom].atom.elemId == ELEM_N || (ElemId)atomBondArray[linkAtom].atom.elemId == ELEM_O || (ElemId)atomBondArray[linkAtom].atom.elemId == ELEM_F) {
                    // if memory is not enough, return
                    if (Insert (HBondsList, BP_NULL_VALUE, i, linkAtom) == 1) {
                        return 1;
                    }
                }
            }
        }

    }

    // check that if there's element in the list
    if (HBondsList->next == NULL && HBondsList->hydrogen == BP_NULL_VALUE) {
      return 2;
    }

    return 0;

}



/**************************************************************************
 this function is used to calculate the hydrogen bonds based on the list
 we have been gotten. search the acceptor for each node
 **************************************************************************/
HBondsReturn HBondsCal (const AtomBondArray & atomBondArray, HBondsInfo * list)
{

    int i;
    int Hatom;                        // H atom for each node
    int don;                        // donator for each node
    int natoms;
    double vdwAcc;                // van der walls radius for acceptor
    double vdwH;                // van der walls radius for H
    double covH;                // covelent length for H
    double covAcc;                // covelent length for donator
    double bondLength;
    double angle;
    HBondsInfo * current;
    bool flag;
    int result;

    // get the basical list containing the information of hydrogen and donator
    // also do the initilization
    // it's possible that we can not find any H atoms in this list
    natoms = atomBondArray.GetCount();
    vdwH = 1.20;
    covH = 0.30;
    result = InitialHBlists (atomBondArray, list);
    if ( result == 2) {
      return THERE_IS_NO_HYGROGEN_BOND;
    } else if (result == 1) {
      return MEMORY_ALLOCATION_FAILED;
    } else {
      current = list;
    }


    // loop over each node in the list
    while (current != NULL) {

      Hatom = current->hydrogen;
      don   = current->donator;
      for (i=0; i<natoms; i++) {
        if ((ElemId)atomBondArray[i].atom.elemId == ELEM_N || (ElemId)atomBondArray[i].atom.elemId == ELEM_O || (ElemId)atomBondArray[i].atom.elemId == ELEM_F) {


          // the i should not be the donator
          if (i == don) {
            continue;
          }

          // check the bond length
          covAcc = GetElemInfo((ElemId)atomBondArray[i].atom.elemId)->covalentRadius;
          vdwAcc = GetElemInfo((ElemId)atomBondArray[i].atom.elemId)->vanderRadius;
          bondLength = MeasureBondDistance(Hatom, i, atomBondArray);
          if (bondLength<(covAcc + covH)*HBONDS_LOWER_LIMIT || bondLength>(vdwAcc + vdwH)*HBONDS_UPPER_LIMIT) {
            continue;
          }

          // check the angle
          angle = MeasureBondAngle(-1.0,i,Hatom,don,atomBondArray);
          if (fabs(180.0-angle) > HBONDS_ANGLE_LIMIT) {
            continue;
          }

          // now we can write the result
          current->acceptor = i;
        }

      }

      // move to the next node
      current = current->next;
    }


    // search that if there's any H bonds paired
    // true is to find some paired H bonds
    current = list;
    flag = false;
    while (current != NULL) {
        if (current->acceptor != BP_NULL_VALUE) {
            flag = true;
            break;
        }
        current = current->next;
    }
    if (!flag) {
        return THERE_IS_NO_HYGROGEN_BOND;
    }


    return SUCCESS;


}
