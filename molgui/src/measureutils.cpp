/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "measureutils.h"

void RotationByZ (double *x, double *y, double angle) {
    double tmpx;
    double tmpy;
    double cosgama = cos (angle);
    double singama = sin (angle);

    // counter-clockwise
    tmpx = cosgama*(*x) - singama*(*y);
    tmpy = singama*(*x) + cosgama*(*y);
    *x = tmpx;
    *y = tmpy;
}

void RotationByY (double *x, double *z, double angle) {
    double tmpx;
    double tmpz;
    double cosbeta = cos (angle);
    double sinbeta = sin (angle);

    // counter-clockwise
    tmpx = cosbeta*(*x) + sinbeta*(*z);
    tmpz =-sinbeta*(*x) + cosbeta*(*z);
    *x = tmpx;
    *z = tmpz;
}

void RotationByX (double *y, double *z, double angle) {
    double tmpy;
    double tmpz;
    double cosalfa = cos (angle);
    double sinalfa = sin (angle);

    // counter-clockwise
    tmpy = cosalfa*(*y) - sinalfa*(*z);
    tmpz = sinalfa*(*y) + cosalfa*(*z);
    *y = tmpy;
    *z = tmpz;
}


/* reflection on an atom */
void Reflection (double *x, double *y, double *z) {
    *x = -(*x);
    *y = -(*y);
    *z = -(*z);
}
