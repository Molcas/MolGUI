/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "molviewdata.h"
#include "ctrlids.h"
#include "tools.h"

MolViewData::MolViewData() {
    m_actionType = ACTION_VIEW;
    m_activeCtrlInstIndex = BP_NULL_VALUE;
    m_intUntitledWinCount = 0;
    m_ctrlInstArray.Clear();
}

MolViewData::~MolViewData() {
        CtrlInst* pCtrlInst = NULL;
        for(int i = (int)m_ctrlInstArray.GetCount() - 1; i >= 0; i--) {
                pCtrlInst = m_ctrlInstArray[i];
                delete pCtrlInst;
                m_ctrlInstArray.RemoveAt(i);
        }
}

wxString Index2DisplayLabel(CtrlInst* pCtrlInst, int atomIndex){
    wxString label = wxEmptyString;
    if(!pCtrlInst)
        return label;

    int elemId = pCtrlInst->GetRenderingData()->GetAtomBond(atomIndex).atom.elemId;
    if(elemId == DEFAULT_HYDROGEN_ID) {
        label = wxT("H");
    }else {
        label = GetElemInfo((ElemId)elemId)->name;
    }
    int index = atomIndex + 1;
    if(!pCtrlInst->isShowGlobalLabels) {
        index = pCtrlInst->GetRenderingData()->GetAtomBond(atomIndex).atom.label;
    }
    label += wxString::Format(wxT("%d"), index);

    return label;
}

wxString Index2DisplayLabel(const ChemAtom* const atom, int atomIndex, bool isGlobal){
    wxString label;

    int index = atomIndex + 1;
    if(!isGlobal) {
        index = atom->label;
    }
    int elemId = atom->elemId;
    if(elemId == DEFAULT_HYDROGEN_ID) {
        label = wxT("H");
    }else {
        label = GetElemInfo((ElemId)elemId)->name;
    }
    label += wxString::Format(wxT("%d"), index);

    return label;
}

ActionType MolViewData::GetActionType(void) const {
    return m_actionType;
}

void MolViewData::SetActionType(ActionType actType){
    this->m_actionType = actType;
}

/*
MeasureType MolViewData::GetMeasureType(void) const{
    return m_measureType;
}

void MolViewData::SetMeasureType(MeasureType mType) {
    this->m_measureType = mType;
}

void MolViewData::SetConstrainType(ConstrainType cType) {
        this->m_constrainType=cType;
}

ConstrainType MolViewData::GetConstrainType(void) const {
        return m_constrainType;
}
*/

void MolViewData::SetActiveCtrlInstIndex(int ctrlIndex) {
    this->m_activeCtrlInstIndex = ctrlIndex;
}

int MolViewData::GetActiveCtrlInstIndex(void) const {
        return m_activeCtrlInstIndex;
}

CtrlInst* MolViewData::GetActiveCtrlInst(void) {
        return GetCtrlInst(GetActiveCtrlInstIndex());
}

int MolViewData::GetCtrlInstCount(void) const {
    return (int) m_ctrlInstArray.GetCount();
}

CtrlInst* MolViewData::GetCtrlInst(int index) {
    if( index < 0 || index >= GetCtrlInstCount()) {
        return NULL;
    }
    //return &(m_ctrlInstArray[index]);
        return m_ctrlInstArray[index];
}
CtrlInst* MolViewData::GetCtrlInstByID(int viewId)
{
    unsigned int count=m_ctrlInstArray.GetCount();
    CtrlInst*   tmp=NULL;
    for(unsigned int i=0;i<count;i++){
        tmp=m_ctrlInstArray[i];
        if(tmp->GetId()==viewId)
            return tmp;
    }
    return NULL;
}

void MolViewData::AddCtrlInst(CtrlInst* pCtrlInst){
        m_ctrlInstArray.Add(pCtrlInst);
    CtrlInst* aCtrlInst = GetCtrlInst(GetCtrlInstCount() - 1);
        aCtrlInst->GetSelectionUtil()->SetCtrlInst(aCtrlInst);
}

void MolViewData::RemoveCtrlInst(int index) {
        CtrlInst* pCtrlInst = GetCtrlInst(index);
        m_ctrlInstArray.RemoveAt(index);
        if(pCtrlInst!=NULL)
                delete pCtrlInst;
}

bool MolViewData::IsAllowAddCtrlInst(void) {
    if(GetCtrlInstCount() >= MENU_ID_WIN_END - MENU_ID_WIN_START) {
        Tools::ShowInfo(wxT("You have reached the maximum number of windows opend concurrently."));
        return false;
    }else {
        return true;
    }
}

int MolViewData::GetUntitledWinCount(void) const {
    return m_intUntitledWinCount;
}

void MolViewData::IncreaseUntitledWinCount(void) {
    m_intUntitledWinCount++;
}

void MolViewData::ResetUntitledWinCount(void) {
    m_intUntitledWinCount = 0;
}

bool MolViewData::IsFileLoaded(const wxString& strFileName, bool exceptActive) {
        for(unsigned i=0 ; i < m_ctrlInstArray.GetCount(); i++) {
                if(exceptActive && i == (unsigned)GetActiveCtrlInstIndex()) continue;
                if(strFileName.Cmp(GetCtrlInst(i)->GetFileName()) == 0) return true;
        }
        return false;
}
