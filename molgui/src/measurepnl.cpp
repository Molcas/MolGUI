/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include "measurepnl.h"

#include "tools.h"
#include "manager.h"
#include "molvieweventhandler.h"
#include "constraindata.h"
#include "envutil.h"  //SONG add the GetMolcasDir function to compile Molcas
#include "resource.h"
#include "dlgjobname.h"

#include <fstream>
#include <string>
#include <vector>
using namespace std;
////////////////////////////////////////////////////////////////////
//enum
////////////////////////////////////////////////////////////////////
enum {
        ID_CTRL_TEXT_VALUE = 1,
        ID_CTRL_TEXT_POS_X,
        ID_CTRL_TEXT_POS_Y,
        ID_CTRL_TEXT_POS_Z,
        ID_CTRL_SPINBTN_POS_X,
        ID_CTRL_SPINBTN_POS_Y,
        ID_CTRL_SPINBTN_POS_Z,
        ID_CTRL_BTN_CONSTRAIN = 9,
        ID_CTRL_BTN_RESET,
        ID_CTRL_SLIDER,
        ID_CTRL_CKB_MOVE_SINGLE_ATOM
};

int ID_CTRL_BTN_IMPORT_CONSTRAIN = wxNewId();
int ID_CTRL_BTN_CLEAR_CONSTRAIN = wxNewId();
int ID_CTRL_BTN_SHOW_CONSTRAIN = wxNewId();

//////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE(MeasurePnl, wxPanel)
EVT_SPIN(ID_CTRL_SPINBTN_POS_X, MeasurePnl::OnSpinButton)
EVT_SPIN(ID_CTRL_SPINBTN_POS_Y, MeasurePnl::OnSpinButton)
EVT_SPIN(ID_CTRL_SPINBTN_POS_Z, MeasurePnl::OnSpinButton)

EVT_TEXT_ENTER(ID_CTRL_TEXT_POS_X, MeasurePnl::OnTextEnter)
EVT_TEXT_ENTER(ID_CTRL_TEXT_POS_Y, MeasurePnl::OnTextEnter)
EVT_TEXT_ENTER(ID_CTRL_TEXT_POS_Z, MeasurePnl::OnTextEnter)

EVT_TEXT_ENTER(ID_CTRL_TEXT_VALUE, MeasurePnl::OnTextEnter)

EVT_COMMAND_SCROLL(ID_CTRL_SLIDER, MeasurePnl::OnSliderScroll)
EVT_TOGGLEBUTTON(ID_CTRL_BTN_CONSTRAIN, MeasurePnl::OnBtnConstrain)
EVT_BUTTON(ID_CTRL_BTN_RESET, MeasurePnl::OnReset)
EVT_BUTTON(ID_CTRL_BTN_IMPORT_CONSTRAIN, MeasurePnl::OnImportCons)
EVT_BUTTON(ID_CTRL_BTN_CLEAR_CONSTRAIN, MeasurePnl::OnClearCons)
EVT_BUTTON(ID_CTRL_BTN_SHOW_CONSTRAIN, MeasurePnl::OnShowCons)
END_EVENT_TABLE()

MeasurePnl::MeasurePnl(wxWindow* parent, wxWindowID id, const wxPoint& pos,
                const wxSize& size, long style, const wxString& name ) : wxPanel(parent, id, pos, size, style, name) {
        m_pStMCType = NULL;
        m_pTcValue = NULL;
        m_pStMin = NULL;
        m_pStMax = NULL;
        m_pSlider = NULL;
        m_pBtnConstrain = NULL;
        m_pBtnReset = NULL;
        m_pCkbMoveSingleAtom = NULL;

        m_pBsPosition = NULL;
        m_pBsBond = NULL;

        m_initBondValue = 0;
        m_initPos[0] = m_initPos[1] = m_initPos[2] = 0;

        m_isError = false;
        m_isSelectionChanged = false;
        m_isMeasure = true;
        m_pCtrlInst = NULL;
}

void MeasurePnl::InitPanel(void) {
        int i = 0;
        int height = wxDefaultSize.GetHeight();

        wxBoxSizer* pBsMain = new wxBoxSizer(wxHORIZONTAL);
        this->SetSizer(pBsMain);
        m_pCkbMoveSingleAtom = new wxCheckBox(this, ID_CTRL_CKB_MOVE_SINGLE_ATOM,
                        wxT("Move Single Atom"), wxDefaultPosition, wxSize(150, height));
        m_pCkbMoveSingleAtom->SetValue(false);

        m_pBsPosition = new wxBoxSizer(wxHORIZONTAL);
        wxString posLabels[] = { wxT("X"), wxT("Y"), wxT("Z") };
        wxStaticText* pStPos = NULL;
        // move position of the atoms
        for (i = 0; i < 3; i++) {
                pStPos = new wxStaticText(this, wxID_ANY, posLabels[i],
                                wxDefaultPosition, wxSize(20, height), wxALIGN_RIGHT);
                m_pTcPoss[i] = new wxTextCtrl(this, ID_CTRL_TEXT_POS_X + i, wxT(""),
                                wxDefaultPosition, wxSize(60, 30), wxTE_RIGHT
                                                | wxTE_PROCESS_ENTER);
                m_pSbPoss[i] = new wxSpinButton(this, ID_CTRL_SPINBTN_POS_X + i,
                                wxDefaultPosition, wxSize(20, height), wxSP_VERTICAL);
                m_pSbPoss[i]->SetRange(-300000, 300000);
                m_pSbPoss[i]->SetValue(0);

                m_pBsPosition->Add(pStPos, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
                m_pBsPosition->Add(m_pTcPoss[i], 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
                m_pBsPosition->Add(m_pSbPoss[i], 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
        }

        m_pBsBond = new wxBoxSizer(wxHORIZONTAL);

        m_pStMCType = new wxStaticText(this, wxID_ANY, wxT("Position()"),
                        wxDefaultPosition, wxSize(170, height), wxALIGN_RIGHT
                                        | wxST_NO_AUTORESIZE);
        m_pTcValue = new wxTextCtrl(this, ID_CTRL_TEXT_VALUE, wxT(""),
                        wxDefaultPosition, wxSize(80, 30), wxTE_RIGHT | wxTE_PROCESS_ENTER);
        m_pStMin = new wxStaticText(this, wxID_ANY, wxT("0"), wxDefaultPosition,
                        wxSize(40, height), wxALIGN_RIGHT | wxST_NO_AUTORESIZE);
        m_pSlider = new wxSlider(this, ID_CTRL_SLIDER, 50, 0, 100,
                        wxDefaultPosition, wxSize(100, 30), wxSL_HORIZONTAL
                                        | wxSL_AUTOTICKS);
        m_pStMax = new wxStaticText(this, wxID_ANY, wxT("100"), wxDefaultPosition,
                        wxSize(40, height), wxALIGN_LEFT | wxST_NO_AUTORESIZE);

        m_pSlider->SetTickFreq(20);
        // std::cout << "m_pTcValue " << m_pTcValue->GetLabel() << std::endl;
        m_pBsBond->Add(m_pTcValue, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        m_pBsBond->Add(m_pStMin, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        m_pBsBond->Add(m_pSlider, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        m_pBsBond->Add(m_pStMax, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);


        wxBitmap bmp = wxBitmap(EnvUtil::GetPrgResDir() + wxTRUNK_MOL_IMG_PATH + wxT(
                        "unlocked.png"), wxBITMAP_TYPE_PNG);


        //    wxBitmap bmp = wxBitmap(CheckImageFile(wxT("unlocked.png")), wxBITMAP_TYPE_PNG);
        m_pBtnConstrain = new ToggleBitmapButton(this, ID_CTRL_BTN_CONSTRAIN,
                        wxDefaultPosition, wxSize(50, 26));
        m_pBtnConstrain->SetBitmapNormal(bmp);

        bmp = wxBitmap(EnvUtil::GetPrgResDir() + wxTRUNK_MOL_IMG_PATH + wxT(
                        "locked.png"), wxBITMAP_TYPE_PNG);


        //        bmp = wxBitmap(CheckImageFile(wxT("locked.png")), wxBITMAP_TYPE_PNG);
        m_pBtnConstrain->SetBitmapToggled(bmp);

        m_pBtnReset = new wxButton(this, ID_CTRL_BTN_RESET, wxT("Reset"),
                        wxDefaultPosition, wxSize(60, 30));

        pBsMain->Add(m_pCkbMoveSingleAtom, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_pImportBtn = new wxButton(this, ID_CTRL_BTN_IMPORT_CONSTRAIN , wxT("Export to exist jobs..."));
        pBsMain->Add(m_pImportBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        m_pShowCons = new wxButton(this, ID_CTRL_BTN_SHOW_CONSTRAIN , wxT("Current constraints"));
        pBsMain->Add(m_pShowCons, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);


        m_pClearConsBtn = new wxButton(this, ID_CTRL_BTN_CLEAR_CONSTRAIN , wxT("Clear all constraints"));
        pBsMain->AddStretchSpacer(1);
        pBsMain->Add(m_pStMCType, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        pBsMain->Add(m_pBsPosition, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        pBsMain->Add(m_pBsBond, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        pBsMain->Add(m_pBtnConstrain, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        pBsMain->Add(m_pBtnReset, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
        pBsMain->Add(m_pClearConsBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
}

void MeasurePnl::SetMeasureOrConstrain(bool isMeasure) {
        m_isMeasure = isMeasure;
        GetSizer()->Show(m_pCkbMoveSingleAtom, isMeasure);
        if (m_pCtrlInst) {
                GetSizer()->Show(m_pBsPosition,
                                m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() <= 1);
                GetSizer()->Show(m_pBsBond,
                                m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() > 1);
                GetSizer()->Show(m_pStMCType,
                                m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() > 1 || isMeasure);
        }
        GetSizer()->Show(m_pBtnConstrain, !isMeasure);
        GetSizer()->Show(m_pImportBtn, !isMeasure);
        GetSizer()->Show(m_pShowCons, !isMeasure);
        GetSizer()->Show(m_pClearConsBtn, !isMeasure);
        GetSizer()->Show(m_pBtnReset, isMeasure);
        if (!isMeasure) {
                m_pBsBond->Show(m_pStMin, false);
                m_pBsBond->Show(m_pSlider, false);
                m_pBsBond->Show(m_pStMax, false);
        }
        //EnableControls(isMeasure ? ACTION_MEASURE:ACTION_CONSTRAIN);
        Layout();
        Refresh();
}

bool MeasurePnl::IsMeasureOrConstrain(void) const {
        return m_isMeasure;
}

void MeasurePnl::SetCtrlInst(CtrlInst* pCtrlInst) {
        m_pCtrlInst = pCtrlInst;
}

void MeasurePnl::ClearSelection(void) {
        if (m_pCtrlInst) {
                m_pCtrlInst->GetSelectionUtil()->ClearSelection();
        }

        m_mcType = m_isMeasure ? (int) MEASURE_POSITION : (int) CONSTRAIN_POSITION;
        //m_mcLabel = m_isMeasure ? wxT("Position(") : wxT("Constraint(");
        m_mcLabel = wxT("Position(");
        UpdateMCType(1);
        SetMeasureOrConstrain(m_isMeasure);
        SetPositionEnable(false);
}

bool MeasurePnl::IsMoveSingleAtom(void) const {
        return m_pCkbMoveSingleAtom->IsChecked();
}

void MeasurePnl::AddSelectedIndex(int selectedAtomIndex) {
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        int selectedMax = pSelUtil->GetSelectedCount() + 1;
        m_mcLabel = wxEmptyString;

        if (selectedMax > 4 || pSelUtil->IsSelected(selectedAtomIndex)) {
                pSelUtil->ClearSelection();
                selectedMax = 1;
        }

        switch (pSelUtil->GetSelectedCount()) {
        case 0:
                m_mcType = m_isMeasure ? (int) MEASURE_POSITION
                                : (int) CONSTRAIN_POSITION;
                m_mcLabel = m_isMeasure ? wxT("Position(") : wxT("CPosition(");
                break;
        case 1:
                m_mcType = m_isMeasure ? (int) MEASURE_DISTANCE
                                : (int) CONSTRAIN_DISTANCE;
                m_mcLabel = m_isMeasure ? wxT("Distance(") : wxT("CDistance(");
                break;
        case 2:
                m_mcType = m_isMeasure ? (int) MEASURE_ANGLE : (int) CONSTRAIN_ANGLE;
                m_mcLabel = m_isMeasure ? wxT("Angel(") : wxT("CAngle(");
                break;
        case 3:
                m_mcType = m_isMeasure ? (int) MEASURE_DIHEDRAL
                                : (int) CONSTRAIN_DIHEDRAL;
                m_mcLabel = m_isMeasure ? wxT("Dihedral(") : wxT("CDihedral(");
                break;
        }

        if (pSelUtil->AddSelectedIndexLimited(selectedMax, selectedAtomIndex)
                        && pSelUtil->GetSelectedCount() > 0) {
                DoMeasure();
        }

        if (pSelUtil->GetSelectedCount() <= 0) {
                SetPositionEnable(false);
        }

        UpdateMCType(selectedMax);
        SetMeasureOrConstrain(m_isMeasure);
        if (!m_isMeasure && pSelUtil->GetSelectedCount() > 0) {
                UpdateConsFreezeStatus();
                //wxMessageBox(wxString::Format(wxT("m_cfStatus=%d"),m_cfStatus));
                m_pBtnConstrain->SetValue(m_cfStatus);
                EnableEditors(m_cfStatus);
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

void MeasurePnl::AddSelectedIndex(int selectedAtomIndex[2]) {
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        int selectedMax = pSelUtil->GetSelectedCount() + 2;
        int selectedMin = 1;
        if (selectedMax > 4) {
                selectedMax = 4;
        }
        bool addSuccess = pSelUtil->AddSelectedIndex(selectedMin, selectedMax,
                        selectedAtomIndex);
        m_mcLabel = wxEmptyString;
        switch (pSelUtil->GetSelectedCount()) {
        case 0:
        case 1:
                m_mcType = m_isMeasure ? (int) MEASURE_POSITION
                                : (int) CONSTRAIN_POSITION;
                //m_mcLabel = m_isMeasure ? wxT("Position(") : wxT("Constraint(");
                m_mcLabel = wxT("Position(");
                break;
        case 2:
                m_mcType = m_isMeasure ? (int) MEASURE_DISTANCE
                                : (int) CONSTRAIN_DISTANCE;
                //m_mcLabel = m_isMeasure ? wxT("Distance(") : wxT("Constraint(");
                m_mcLabel = wxT("Distance(");
                break;
        case 3:
                m_mcType = m_isMeasure ? (int) MEASURE_ANGLE : (int) CONSTRAIN_ANGLE;
                //m_mcLabel = m_isMeasure ? wxT("Angel(") : wxT("Constraint(");
                m_mcLabel = wxT("Angel(");
                break;
        case 4:
                m_mcType = m_isMeasure ? (int) MEASURE_DIHEDRAL
                                : (int) CONSTRAIN_DIHEDRAL;
                //m_mcLabel = m_isMeasure ? wxT("Dihedral(") : wxT("Constraint(");
                m_mcLabel = wxT("Dihedral(");
                break;
        }
        if (addSuccess && pSelUtil->GetSelectedCount() > 0) {
                DoMeasure();
                if (m_isMeasure)
                        m_pBtnReset->Enable(true);
        }
        if (pSelUtil->GetSelectedCount() <= 0) {
                SetPositionEnable(false);
        }
        selectedMax
                        = pSelUtil->GetSelectedCount() > 0 ? pSelUtil->GetSelectedCount()
                                        : 1;
        UpdateMCType(selectedMax);
        SetMeasureOrConstrain(m_isMeasure);
        if (!m_isMeasure && pSelUtil->GetSelectedCount() > 0) {
                UpdateConsFreezeStatus();
                //wxMessageBox(wxString::Format(wxT("m_cfStatus=%d"),m_cfStatus));
                m_pBtnConstrain->SetValue(m_cfStatus);
                EnableEditors(m_cfStatus);
        }
        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
}

void MeasurePnl::OnTextEnter(wxCommandEvent& event) {
        if (event.GetId() == ID_CTRL_TEXT_VALUE) {
               DoTextValueChanged();
               // std::cout << "MeasurePnl::OnTextEnter DoTextValueChanged " << std::endl;
        } else {
                DoPositionChanged(event.GetId(), false);
                // std::cout << "MeasurePnl::OnTextEnter DoPositionChanged " << std::endl;
        }
}

void MeasurePnl::DoTextValueChanged(void) {
        double newValue = 0;
        long longValue = 0;
        wxString strValue = m_pTcValue->GetValue();
        strValue.ToDouble(&newValue);
        longValue = (long) (newValue * BP_SLIDER_PRECISION);
        // std::cout << "MeasurePnl::DoTextValueChanged new value " << strValue << std::endl;
        if (longValue <= m_pSlider->GetMax() && longValue >= m_pSlider->GetMin()) {
                if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() > 0) {
                        MeasureInfo mInfo = DoBondChange(newValue);
                        if (mInfo != MEASURE_SUCCESS) {
                                ShowMeasureInfo(mInfo);
                        } else {
                                m_pSlider->SetValue(longValue);
                                if (!m_isMeasure && m_cfStatus) {
                                        UpdateConstrainData(newValue);
                                }
                                if (!m_isMeasure && !m_pBtnConstrain->GetValue()) {
                                        UpdateConstrainData(newValue);
                                }
                                if (!m_pCtrlInst->IsDirty()) {
                                        m_pCtrlInst->SetDirty(true);
                                        MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
                                }
                                MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
                        }
                }
        }
}

void MeasurePnl::DoPositionChanged(int ctrlId, bool isSpinBtn) {
        int index = 0;
        double offset = 0;
        double currValue = 0, oldValue = 0;
        wxString str = wxEmptyString;
        static bool busy = false;
        // std::cout << "MeasurePnl::DoPositionChanged isSpinBtn " << isSpinBtn << std::endl;
        if (busy)
                return;
        busy = true;
        if (isSpinBtn) {
                index = ctrlId - ID_CTRL_SPINBTN_POS_X;
                currValue = m_initPos[index] + m_pSbPoss[index]->GetValue()
                                / BP_SLIDER_PRECISION;
                str = m_pTcPoss[index]->GetValue();
                str.ToDouble(&oldValue);
                //
                m_pTcPoss[index]->SetValue(wxString::Format(wxT("%.3f"), currValue));
        } else {
                index = ctrlId - ID_CTRL_TEXT_POS_X;
                str = m_pTcPoss[index]->GetValue();
                str.ToDouble(&currValue);
                oldValue = m_initPos[index] + m_pSbPoss[index]->GetValue()
                                / BP_SLIDER_PRECISION;
                //
                m_pSbPoss[index]->SetValue((int) ((currValue - m_initPos[index])
                                * BP_SLIDER_PRECISION));
        }
        //wxMessageBox(wxString::Format(wxT("curr %.3f  = old %.3f"), currValue, oldValue));
        offset = currValue - oldValue;

        if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() == 1) {
                if (IsMoveSingleAtom()) {
                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                        m_pCtrlInst->GetSelectionUtil()->GetSelectedIndex(0),
                                        index, offset);
                } else {
                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                        BP_NULL_VALUE, index, offset);
                }
                MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
        }
        AtomPosition position;
        if (!m_isMeasure && m_cfStatus) {
                m_pCtrlInst->GetSelectedPosition(0, position);
                UpdateFreezeAtom(position);
        }
        busy = false;
}

// here we note that the angle is accurate at 10-5 precision
MeasureInfo MeasurePnl::DoBondChange(float value) {
        double oldValue = BP_NULL_VALUE;
        double newValue = value;
        MeasureInfo mInfo = MEASURE_SUCCESS;
        bool isMoveSingle = IsMoveSingleAtom();
        SelectionUtil* selUtil = m_pCtrlInst->GetSelectionUtil();

        switch (m_mcType) {
        case MEASURE_DISTANCE:
                mInfo = ChangeBondDistance(newValue, selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                isMoveSingle);
                break;
        case MEASURE_ANGLE:
                SetAngleRange(newValue);
                if (newValue < 0.00001) {
                        newValue = 0.00001;
                } else if (newValue > 179.99999 && newValue <= 180.0) {
                        newValue = 179.99999;
                } else if (newValue > 180.0 && newValue < 180.00001) {
                        newValue = 180.00001;
                } else if (newValue > 359.99999) {
                        newValue = 359.99999;
                }

                m_pCtrlInst->GetOldAngleValue(selUtil->GetSelectedIndex(2),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(0),
                                oldValue);
                mInfo = ChangeBondAngle(newValue, oldValue,
                                selUtil->GetSelectedIndex(2), selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                isMoveSingle);
                if (mInfo == MEASURE_SUCCESS) {
                        m_pCtrlInst->SetOldAngleValue(selUtil->GetSelectedIndex(2),
                                        selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(0),
                                        newValue);
                }
                break;
        case MEASURE_DIHEDRAL:
                mInfo = ChangeDihedral(newValue, selUtil->GetSelectedIndex(3),
                                selUtil->GetSelectedIndex(2), selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0),
                                m_pCtrlInst->GetRenderingData()->GetAtomBondArray(),
                                isMoveSingle);
                break;
        default:
                break;
        }

        return mInfo;
}

void MeasurePnl::ChangeBondValueByPageKey(bool isUp) {
        double newValue = 0;
        long longValue = 0;

        if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() <= 1)
                return;
        wxString strValue = m_pTcValue->GetValue();
        strValue.ToDouble(&newValue);

        switch (m_mcType) {
        case MEASURE_DISTANCE:
                newValue += isUp ? 0.1 : -0.1;
                break;
        case MEASURE_ANGLE:
        case MEASURE_DIHEDRAL:
                newValue += isUp ? 1 : -1;
                break;
        default:
                break;
        }
        longValue = (long) (newValue * BP_SLIDER_PRECISION);
        if (longValue < m_pSlider->GetMin()) {
                newValue = m_pSlider->GetMin() / BP_SLIDER_PRECISION;
        } else if (longValue > m_pSlider->GetMax()) {
                newValue = m_pSlider->GetMax() / BP_SLIDER_PRECISION;
        }
        m_pTcValue->SetValue(wxString::Format(wxT("%.3f"), newValue));
        DoTextValueChanged();
}

void MeasurePnl::OnSpinButton(wxSpinEvent& event) {
        DoPositionChanged(event.GetId(), true);
}

void MeasurePnl::OnSliderScroll(wxScrollEvent& event) {
        static bool busy = false;
        float value = event.GetInt() / BP_SLIDER_PRECISION;
        if (busy)
                return;
        if (m_isError && !m_isSelectionChanged)
                return;
        if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() <= 1)
                return;
        busy = true;
        MeasureInfo mInfo = DoBondChange(value);

        if (mInfo != MEASURE_SUCCESS) {
                ShowMeasureInfo(mInfo);
                m_isError = true;
                m_isSelectionChanged = false;
        } else {
                m_pTcValue->SetValue(wxString::Format(wxT("%.3f"), value));
                if (!m_pCtrlInst->IsDirty()) {
                        m_pCtrlInst->SetDirty(true);
                        MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
                }
                MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
                m_isError = false;
        }
        busy = false;
}

void MeasurePnl::OnReset(wxCommandEvent& event) {
        double offset = 0;
        if (GetSizer()->IsShown(m_pBsPosition)) {
                SetInitPositionValue(m_initPos);
                if (m_pCtrlInst->GetSelectionUtil()->GetSelectedCount() == 1) {
                        for (int i = 0; i < 3; i++) {
                                offset
                                                = m_initPos[i]
                                                                - m_pCtrlInst->GetRenderingData()->GetAtomBondArray()[m_pCtrlInst->GetSelectionUtil()->GetSelectedIndex(
                                                                                0)].atom.pos[i];
                                if (IsMoveSingleAtom()) {
                                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                                        m_pCtrlInst->GetSelectionUtil()->GetSelectedIndex(0),
                                                        i, offset);
                                } else {
                                        m_pCtrlInst->GetRenderingData()->TranslateAtomsAxis(
                                                        BP_NULL_VALUE, i, offset);
                                }
                        }
                        MolViewEventHandler::Get()->GetMolView()->RefreshGraphics();
                        if (!m_isMeasure && m_cfStatus) {
                                AtomPosition pos;
                                m_pCtrlInst->GetSelectedPosition(0, pos);
                                UpdateFreezeAtom(pos);
                        }
                }
        } else {
                m_pTcValue->SetValue(wxString::Format(wxT("%.3f"), m_initBondValue));
                DoTextValueChanged();
        }
}

void MeasurePnl::OnBtnConstrain(wxCommandEvent& event) {
    DoTextValueChanged();
    DoMeasure();
        ConstrainData& constrain =
                        m_pCtrlInst->GetRenderingData()->GetConstrainData();
        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        ConsElem ce;
        FreezeAtom fa;
        int i, index;
        switch (m_mcType) {
        case CONSTRAIN_POSITION:
                if (pSelUtil->GetSelectedCount() < 1)
                        m_pBtnConstrain->SetValue(false);
                        return;
                index = pSelUtil->GetSelectedIndex(0);
                fa = chemArray[index];
                if (!m_pCtrlInst->GetSelectedPosition(0, fa.pos)) {
                        return;
                }
                break;
        case CONSTRAIN_DISTANCE:
        case CONSTRAIN_ANGLE:
        case CONSTRAIN_DIHEDRAL:
                if (pSelUtil->GetSelectedCount() < (m_mcType + 1))
                        return;
                for (i = 0; i <= m_mcType; i++) {
                        index = pSelUtil->GetSelectedIndex(i);
                        ce.elemInfos.Add(chemArray[index].atom.uniqueId);
                }
                ce.value = m_initBondValue;
                break;
        }
        m_cfStatus = m_pBtnConstrain->GetValue();
        EnableEditors(m_pBtnConstrain->GetValue());
        if (m_pBtnConstrain->GetValue()) {
                //SetValue(true);
                switch (m_mcType) {
                case CONSTRAIN_POSITION:
                        constrain.AppendFreezeData(fa);
                        break;
                case CONSTRAIN_DISTANCE:
                case CONSTRAIN_ANGLE:
                case CONSTRAIN_DIHEDRAL:
                        constrain.AppendConstrainData(ce);
                        break;
                }
        } else {
                switch (m_mcType) {
                case CONSTRAIN_POSITION:
                        constrain.RemoveFreezeData(fa.uniqueId);
                        //DoPositionChanged(ID_CTRL_TEXT_POS_X, false);
                        //DoPositionChanged(ID_CTRL_TEXT_POS_Y, false);
                        //DoPositionChanged(ID_CTRL_TEXT_POS_Z, false);
                        break;
                case CONSTRAIN_DISTANCE:
                case CONSTRAIN_ANGLE:
                case CONSTRAIN_DIHEDRAL:
                        constrain.RemoveConstrainData(ce);
//                        DoTextValueChanged();
                        break;
                }
        }
        m_pCtrlInst->SetDirty(true);
        MolViewEventHandler::Get()->GetMolView()->EnableMenuToolCtrlInst();
        Manager::Get()->GetProjectManager()->GetActiveProject()->Save();
}

void MeasurePnl::UpdateMCType(int selectedMax) {
        m_mcLabel
                        += m_pCtrlInst->GetSelectionUtil()->GetSelectionLabel(selectedMax)
                                        + wxT(")");
        m_pStMCType->SetLabel(m_mcLabel);
}

void MeasurePnl::DoMeasure(void) {
        float value = 0.0;
        double newValue = 0.0;
        double oldValue = 0;
        MeasureInfo mInfo = MEASURE_SUCCESS;
        //------------xia--------------
        m_pCtrlInst->SaveHistroyFile();
        //---------------------------
        AtomBondArray & atomBondArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* selUtil = m_pCtrlInst->GetSelectionUtil();

        switch (m_mcType) {
        case MEASURE_POSITION:
                m_initPos[0] = m_initPos[1] = m_initPos[2] = 0;
                if (m_pCtrlInst->GetSelectedPosition(0, m_initPos)) {
                        SetInitPositionValue(m_initPos);
                        SetPositionEnable(selUtil->GetSelectedCount() == 1);
                }
                break;
        case MEASURE_DISTANCE:
                newValue = MeasureBondDistance(selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0), atomBondArray);
                break;
        case MEASURE_ANGLE:
                m_pCtrlInst->GetOldAngleValue(selUtil->GetSelectedIndex(2),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(0),
                                oldValue);
                newValue = MeasureBondAngle(oldValue, selUtil->GetSelectedIndex(2),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(0),
                                atomBondArray);
                m_pCtrlInst->SetOldAngleValue(selUtil->GetSelectedIndex(2),
                                selUtil->GetSelectedIndex(1), selUtil->GetSelectedIndex(0),
                                newValue);
                break;
        case MEASURE_DIHEDRAL:
                mInfo = MeasureDihedral(newValue, selUtil->GetSelectedIndex(3),
                                selUtil->GetSelectedIndex(2), selUtil->GetSelectedIndex(1),
                                selUtil->GetSelectedIndex(0), atomBondArray);
                if (mInfo != MEASURE_SUCCESS) {
                        ShowMeasureInfo(mInfo);
                        return;
                }
                break;
        }
        value = (float) newValue;
        if (m_mcType != MEASURE_POSITION) {
                SetInitBondValue(value);
        }
}

void MeasurePnl::SetInitPositionValue(AtomPosition& atomPos) {
        int i = 0;
        for (i = 0; i < 3; i++) {
                m_pTcPoss[i]->SetValue(wxString::Format(wxT("%.3f"), atomPos[i]));
                m_initPos[i] = atomPos[i];
                m_pSbPoss[i]->SetValue(0);
        }
}

void MeasurePnl::SetPositionEnable(bool enable) {
        if (GetSizer()->IsShown(m_pBsPosition) && m_pTcPoss[0]->IsEnabled()
                        != enable) {
                for (int i = 0; i < 3; i++) {
                        m_pTcPoss[i]->Enable(enable);
                        m_pSbPoss[i]->Enable(enable);
                }
                m_pBtnReset->Enable(enable);
        }
        m_pBtnConstrain->Enable(enable);
}

void MeasurePnl::SetInitBondValue(float value) {
        wxString minLabel, maxLabel, sliderLabel;
        long sliderValue = 0, sliderMin = 0, sliderMax = 0;
        m_isSelectionChanged = true;
        m_isError = false;
        switch (m_mcType) {
        case MEASURE_DISTANCE:
                minLabel.Printf(wxT("%.3f"), value / 2.0f);
                maxLabel.Printf(wxT("%.3f"), value * 2.0f);
                sliderMin = (long) (BP_SLIDER_PRECISION * value / 2.0f);
                sliderMax = (long) (BP_SLIDER_PRECISION * value * 2.0f);
                sliderValue = (long) (BP_SLIDER_PRECISION * value);
                sliderLabel.Printf(wxT("%.3f"), value);
                break;
        case MEASURE_ANGLE:
                minLabel.Printf(wxT("0"));
                maxLabel.Printf(wxT("360"));
                sliderMin = (long) 0;
                sliderMax = (long) (BP_SLIDER_PRECISION * 360.0f);
                sliderValue = (long) (BP_SLIDER_PRECISION * value);
                sliderLabel.Printf(wxT("%.3f"), value);
                break;
        case MEASURE_DIHEDRAL:
                minLabel.Printf(wxT("-180"));
                maxLabel.Printf(wxT("180"));
                sliderMin = (long) (BP_SLIDER_PRECISION * -180.0f);
                sliderMax = (long) (BP_SLIDER_PRECISION * 180.0f);
                sliderValue = (long) (BP_SLIDER_PRECISION * value);
                sliderLabel.Printf(wxT("%.3f"), value);
                break;
        default:
                break;
        }

        m_pStMin->SetLabel(minLabel);
        m_pStMax->SetLabel(maxLabel);
        m_pSlider->SetRange(sliderMin, sliderMax);
        m_pSlider->SetValue(sliderValue);
        m_pSlider->SetTickFreq((sliderMax - sliderMin) / 10);
        m_pTcValue->SetValue(sliderLabel);

        m_initBondValue = value;
}

void MeasurePnl::ShowMeasureInfo(MeasureInfo mInfo) {
        switch (mInfo) {
        case MEASURE_SUCCESS:
                break;
        case MEASURE_ERROR_IN_MEMORY_ALLOCATION:
                Tools::ShowError(wxT("Error in memory allocaiton!"));
                break;
        case MEASURE_ERROR_IN_BONDLENGTH:
                Tools::ShowError(wxT("Cannot change bond length!"));
                break;
        case MEASURE_ERROR_IN_BONDANGLE:
                Tools::ShowError(wxT("Cannot change bond angle!"));
                break;
        case MEASURE_ERROR_IN_DIHEDRAL:
                Tools::ShowError(wxT("Cannot change dihedral!"));
                break;
        case DIHEDRAL_CAN_NOT_DEFINE:
                Tools::ShowError(wxT("It's impossible to define the dihedral!"));
                break;
        default:
                break;
        }
}

void MeasurePnl::EnableControls(ActionType actionType) {
        bool enabled = false;
        if (actionType == ACTION_MEASURE) {
                enabled = true;
        }
        EnableEditors(enabled);
        enabled = false;
        if (actionType == ACTION_CONSTRAIN) {
                enabled = true;
        }
        m_pBtnConstrain->Enable(enabled);
        m_cfStatus = false;
        m_pBtnConstrain->SetValue(false);
}

void MeasurePnl::ResetControls() {
        m_pTcValue->SetValue(wxEmptyString);
        m_pSlider->SetTick(0);
        for (int i = 0; i < 3; i++) {
                m_pTcPoss[i]->SetValue(wxEmptyString);
                m_pSbPoss[i]->SetValue(0);
        }
}

void MeasurePnl::EnableEditors(bool enabled) {
        if (!m_pBtnConstrain->IsEnabled()) {
                m_pTcValue->SetEditable(enabled);
        } else {
                m_pTcValue->SetEditable(false);
                if (!m_pBtnConstrain->GetValue()) {
                        m_pTcValue->SetEditable(true);
                }
        }
        m_pSlider->Enable(enabled);
        for (int i = 0; i < 3; i++) {
                m_pTcPoss[i]->SetEditable(enabled);
                m_pSbPoss[i]->Enable(enabled);
        }
        m_pBtnReset->Enable(enabled);
}

void MeasurePnl::UpdateConsFreezeStatus() {
        ConstrainData& constrain =
                        m_pCtrlInst->GetRenderingData()->GetConstrainData();
        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        ConsElem ce;
        int i, index;
        if (pSelUtil->GetSelectedCount() <= 0)
                return;
        if (pSelUtil->GetSelectedCount() == 1) {
                index = pSelUtil->GetSelectedIndex(0);
                m_cfStatus = constrain.HasFreezeData(chemArray[index].atom.uniqueId);
        } else {
                for (i = 0; i < pSelUtil->GetSelectedCount(); i++) {
                        index = pSelUtil->GetSelectedIndex(i);
                        ce.elemInfos.Add(chemArray[index].atom.uniqueId);
                }
                m_cfStatus = constrain.HasConstrainData(ce);
        }
}

void MeasurePnl::UpdateConstrainData(double value) {
        ConstrainData& constrain =
                        m_pCtrlInst->GetRenderingData()->GetConstrainData();
        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        ConsElem ce;
        int i, index;
        // std::cout << "MeasurePnl::UpdateConstrainData new value " << value << std::endl;
        if (pSelUtil->GetSelectedCount() <= 1)
                return;
        for (i = 0; i < pSelUtil->GetSelectedCount(); i++) {
                index = pSelUtil->GetSelectedIndex(i);
                ce.elemInfos.Add(chemArray[index].atom.uniqueId);
        }
        constrain.UpdateConstrainData(ce);
}

void MeasurePnl::UpdateFreezeAtom(AtomPosition pos) {
        ConstrainData& constrain =
                        m_pCtrlInst->GetRenderingData()->GetConstrainData();
        AtomBondArray & chemArray
                        = m_pCtrlInst->GetRenderingData()->GetAtomBondArray();
        SelectionUtil* pSelUtil = m_pCtrlInst->GetSelectionUtil();
        FreezeAtom fa;
        int index;
        // std::cout << "MeasurePnl::UpdateFreezeAtom new value " << pos.x << std::endl;
        if (pSelUtil->GetSelectedCount() != 1)
                return;
        index = pSelUtil->GetSelectedIndex(0);
        fa.uniqueId = chemArray[index].atom.uniqueId;
        fa.pos = pos;
        constrain.UpdateFreezeAtom(fa);
}

// setting the angle into the range of 0-360
void MeasurePnl::SetAngleRange(double & angle) {


        while (1) {

                if (angle >= 0.0 && angle <= 360.0) {
                        return;
                } else if (angle < 0.0) {
                        angle = angle + 360.0;
                } else {
                        angle = angle - 360.0;
                }
        }

}

void MeasurePnl::OnImportCons(wxCommandEvent & event) {

        wxString jobName;

        ConsDlg d(Manager::Get()->GetAppFrame(), Manager::Get()->GetProjectManager()->GetSelectedProject(),
                                                                        wxID_ANY, wxT("Select a job"),
             wxDefaultPosition, wxDefaultSize,
             wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER);

        if (wxID_OK != d.ShowModal()) {
                return ;
        }

        jobName = d.GetJobName();
        wxString inputName;

        inputName = Manager::Get()->GetProjectManager()->GetActiveProject()->GetFiles(jobName)->GetInputFileName();

        if (inputName.AfterLast('.').IsSameAs(wxT("in"))) {

                wxString name = Manager::Get()->GetProjectManager()->GetActiveProject()->GetResultDir(name);

                name.RemoveLast();

                wxString simName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".sim");

                ifstream in(inputName.ToAscii());

                string s;
                vector<string> vs;
        cout << " " ;
                while(getline(in, s)) {

                        if (0 == s.compare("$opt")) {
                                while (getline(in, s)) {
                                        if (0 == s.compare("$end")) {
                                                break;
                                        }
                                }
                                continue;
                        }
                        vs.push_back(s);
                }
                in.close();
        cout << " " ;
                in.open(simName.ToAscii());
        cout << " " ;
                vector<string> vec;
                //bool has = false;
                while(getline(in, s)) {

                        if (0 == s.compare("$BEGIN CONSTRAINTS")) {

                                while (getline(in, s)){

                                        if (0 != s.compare("$END CONSTRAINTS")) {
                                                //has = true;
                                                string::size_type pos=0;

                                                if( (pos=s.find("L", pos)) != string::npos){
                                                         s.erase(pos, 1);
                                                         s.insert(pos, "stre");
                                                }
                                                pos=0;
                                                if( (pos=s.find("A", pos)) != string::npos){
                                                         s.erase(pos, 1);
                                                         s.insert(pos, "bend");
                                                }
                                                pos=0;
                                                if( (pos=s.find("D", pos)) != string::npos){
                                                         s.erase(pos, 1);
                                                         s.insert(pos, "tors");
                                                }

                                                vec.push_back(s);

                                        } else {
                                                break;
                                        }

                                }
                                continue;
                        }
                }
                in.close();
//                if (!has) {
//                        wxMessageBox(wxT("No constraints!"));
//                        return;
//                }
        cout << " \n" ;
                ofstream out(inputName.ToAscii());

                for (unsigned i=0; i<vs.size(); ++i) {
                        out << vs[i] << endl;
                }

                out << "$opt" << endl;
                out << "CONSTRAINT" << endl;

                for (unsigned i=0; i<vec.size(); ++i) {
                        out << vec[i] << endl;
                }

                out << "ENDCONSTRAINT" << endl;
                out << "$end" << endl;

                out.close();

                wxMessageBox(wxT("Import Constraints Completed!"));

        } else {

                vector<string> temp;

                temp.push_back("Constraints");

                wxString name;

                name = Manager::Get()->GetProjectManager()->GetActiveProject()->GetResultDir(name);

                name.RemoveLast();

                wxString simName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".sim");

                wxString xyzName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".xyz");

                const int lennum = 2;
                const int angnum = 3;
                const int dihnum = 4;

                int len = 0;
                int ang = 0;
                int dih = 0;

                vector<string> v;

                ifstream in(xyzName.ToAscii());

                int count = 0;

                in >> count;

                string s;

                getline(in, s);
                getline(in, s);

                v.push_back("");

                for (int i=0; i<count; ++i) {

                        in >> s;
                        v.push_back(s);
                        getline(in, s);

                }
                in.close();

                in.open(simName.ToAscii());

                vector<string> vec;

                int f = 0;

                while(getline(in, s)) {

                        if (0 == s.compare("$BEGIN CONSTRAINTS")) {



                                while (in >> s){

                                        if (0 == s.compare("$END")) break;

                                        string elem;

                                        string value;

                                        int num;

                                        char a[20];

                                        float res;

                                        switch(s.at(0)) {

                                        case 'L':
                                                len++;

                                                sprintf(a, "%d", len);

                                                elem += "r";

                                                elem += a;

                                                elem += " = Bond ";

                                                for (int i=0; i<lennum; ++i) {

                                                        in >> num;

                                                        elem += v[num];
                                                        sprintf(a, "%d", num);
                                                        elem += a;
                                                        elem += " ";
                                                }


                                                sprintf(a, "%d", len);

                                                value += "r";

                                                value += a;

                                                value += " = ";

                                                in >> res;
                                                sprintf(a, "%0.4f", res);

                                                value += a;

                                                vec.push_back(value);

                                                f = 1;

                                                break;

                                        case 'A':
                                                ang++;

                                                sprintf(a, "%d", ang);

                                                elem += "a";

                                                elem += a;

                                                elem += " = Angle ";

                                                for (int i=0; i<angnum; ++i) {

                                                        in >> num;

                                                        elem += v[num];
                                                        sprintf(a, "%d", num);
                                                        elem += a;
                                                        elem += " ";
                                                }


                                                sprintf(a, "%d", ang);

                                                value += "a";

                                                value += a;
                                                value += " = ";
                                                in >> res;
                                                sprintf(a, "%0.4f", res);

                                                value += a;
                                                vec.push_back(value);

                                                f = 1;

                                                break;

                                        case 'D':
                                                dih++;

                                                sprintf(a, "%d", dih);

                                                elem += "d";

                                                elem += a;

                                                elem += " = Dihedral ";

                                                for (int i=0; i<dihnum; ++i) {

                                                        in >> num;

                                                        elem += v[num];
                                                        sprintf(a, "%d", num);
                                                        elem += a;
                                                        elem += " ";
                                                }

                                                sprintf(a, "%d", dih);

                                                value += "d";

                                                value += a;
                                                value += " = ";
                                                in >> res;
                                                sprintf(a, "%0.4f", res);

                                                value += a;
                                                vec.push_back(value);

                                                f = 1;

                                                break;

                                        default :
                                                break;
                                        }
                                        temp.push_back(elem);
                                }
                                if (1 == f) {
                                        temp.push_back("value");
                                        for (unsigned int i=0; i<vec.size(); ++i) {
                                                temp.push_back(vec[i]);
                                        }
                                }
                        }
                }

                if (1 == f) {
                        temp.push_back("End of constraints");
                } else {
                        wxMessageBox(wxT("No constraints!"));
                        temp.pop_back();
                }
                in.close();

                in.open(inputName.ToAscii());

                int noSlapaf = 1;

                vector<string> vs;
                int wt = 0;
                while (getline(in, s)) {
                        vs.push_back(s);
                        if (0 == s.compare(" &GATEWAY")) {
                                noSlapaf = 0;
                                while(getline(in, s)){
                                        if(0 == s.compare("Constraints")) {
                                                for (; 0 != s.compare("End of constraints");getline(in, s));
                                                if ( 0 == wt ) {
                                                        int ddd = 0;
                                                        for (unsigned int i=0; i<temp.size(); ++i) {
                                                                vs.push_back(temp[i]);
                                                                ddd ++;
                                                        }
                                                        wt = 1;
                                                }
                                        } else if (0 == s.compare(">>>>>>>> ENDDO <<<<<<<<")) {
                                                if (0 == wt) {
                                                        int ddd = 0;
                                                        for (unsigned int i = 0; i < temp.size(); ++i) {
                                                                vs.push_back(temp[i]);
                                                                ddd ++;
                                                        }
                                                        wt = 1;
                                                }
                                                vs.push_back(s);
                                                break;
                                        } else {
                                                if (0 == wt) {
                                                        int ddd = 0;
                                                        for (unsigned int i = 0; i < temp.size(); ++i) {
                                                                vs.push_back(temp[i]);
                                                                ddd ++;
                                                        }
                                                        wt = 1;
                                                }
                                                vs.push_back(s);
                                        }
                                }
                        }
                }

                in.close();

                if (noSlapaf) {
                        wxMessageBox(wxT("No GATEWAY found in input!"));
                        return ;
                }

                ofstream out(inputName.ToAscii());
                for (unsigned int i=0; i<vs.size(); ++i) {
                        out << vs[i] << endl;
                }
                out.close();
                if (1 == f) {
                        wxMessageBox(wxT("Import Constraints Completed!"));
                }
        }

}

void MeasurePnl::OnClearCons(wxCommandEvent & event) {

        wxMessageDialog mdlg(this, wxT("Clear All Constraints?"), wxT("Attention!"), wxYES_NO);
        if (wxID_YES != mdlg.ShowModal()) {
                return ;
        }

        wxString name;

        name = Manager::Get()->GetProjectManager()->GetActiveProject()->GetResultDir(name);

        name.RemoveLast();

        wxString simName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".sim");

        ifstream in(simName.ToAscii());

        vector<string> vec;

        string s;

        while(getline(in, s)) {

                vec.push_back(s);

                if (0 == s.compare("$BEGIN CONSTRAINTS")) {

                        while (getline(in, s)){

                                if (0 == s.compare("$END CONSTRAINTS")) {
                                        vec.push_back(s);
                                        break;
                                }

                        }
                }
        }
        in.close();

        ofstream out(simName.ToAscii());
        for (unsigned int i=0; i<vec.size(); ++i) {
                out << vec[i] << endl;
        }
        out.close();

        ConstrainData& cd = m_pCtrlInst->GetRenderingData()->GetConstrainData();
        cd.ClearAllData();
        m_pBtnConstrain->SetValue(false);
}

void MeasurePnl::OnShowCons(wxCommandEvent & event) {

        vector<string> temp;

        temp.push_back("Constraints");

        wxString name;

        name = Manager::Get()->GetProjectManager()->GetActiveProject()->GetResultDir(name);

        name.RemoveLast();

        wxString simName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".sim");

        wxString xyzName = name + platform::PathSep() + name.AfterLast(platform::PathSep().GetChar(0)) + wxT(".xyz");

        const int lennum = 2;
        const int angnum = 3;
        const int dihnum = 4;

        int len = 0;
        int ang = 0;
        int dih = 0;

        vector<string> v;

        ifstream in(xyzName.ToAscii());

        int count = 0;

        in >> count;

        string s;

        getline(in, s);
        getline(in, s);

        v.push_back("");

        for (int i=0; i<count; ++i) {

                in >> s;
                v.push_back(s);
                getline(in, s);

        }
        in.close();

        in.open(simName.ToAscii());

        vector<string> vec;

        int f = 0;

        while(getline(in, s)) {

                if (0 == s.compare("$BEGIN CONSTRAINTS")) {



                        while (in >> s){

                                if (0 == s.compare("$END")) break;

                                string elem;

                                string value;

                                int num;

                                char a[20];

                                float res;

                                switch(s.at(0)) {

                                case 'L':
                                        len++;

                                        sprintf(a, "%d", len);

                                        elem += "r";

                                        elem += a;

                                        elem += " = Bond ";

                                        for (int i=0; i<lennum; ++i) {

                                                in >> num;

                                                elem += v[num];
                                                sprintf(a, "%d", num);
                                                elem += a;
                                                elem += " ";
                                        }


                                        sprintf(a, "%d", len);

                                        value += "r";

                                        value += a;

                                        value += " = ";

                                        in >> res;
                                        sprintf(a, "%0.4f", res);

                                        value += a;

                                        vec.push_back(value);

                                        f = 1;

                                        break;

                                case 'A':
                                        ang++;

                                        sprintf(a, "%d", ang);

                                        elem += "a";

                                        elem += a;

                                        elem += " = Angle ";

                                        for (int i=0; i<angnum; ++i) {

                                                in >> num;

                                                elem += v[num];
                                                sprintf(a, "%d", num);
                                                elem += a;
                                                elem += " ";
                                        }


                                        sprintf(a, "%d", ang);

                                        value += "a";

                                        value += a;
                                        value += " = ";
                                        in >> res;
                                        sprintf(a, "%0.4f", res);

                                        value += a;
                                        vec.push_back(value);

                                        f = 1;

                                        break;

                                case 'D':
                                        dih++;

                                        sprintf(a, "%d", dih);

                                        elem += "d";

                                        elem += a;

                                        elem += " = Dihedral ";

                                        for (int i=0; i<dihnum; ++i) {

                                                in >> num;

                                                elem += v[num];
                                                sprintf(a, "%d", num);
                                                elem += a;
                                                elem += " ";
                                        }

                                        sprintf(a, "%d", dih);

                                        value += "d";

                                        value += a;
                                        value += " = ";
                                        in >> res;
                                        sprintf(a, "%0.4f", res);

                                        value += a;
                                        vec.push_back(value);

                                        f = 1;

                                        break;

                                default :
                                        break;
                                }
                                temp.push_back(elem);
                        }
                        if (1 == f) {
                                temp.push_back("value");
                                for (unsigned int i=0; i<vec.size(); ++i) {
                                        temp.push_back(vec[i]);
                                }
                        }
                }
        }

        if (1 == f) {
                temp.push_back("End of constraints");
        } else {
                wxMessageBox(wxT("No constraints!"));
                temp.pop_back();
                return ;
        }
        in.close();

        wxDialog * textd = new wxDialog(this, -1, wxT("Current constraints"), wxDefaultPosition, wxSize(250, 300));

        wxListCtrl * text = new wxListCtrl(textd, -1, wxPoint(30, 20), wxSize(200, 250), wxLC_LIST|wxLC_SINGLE_SEL|wxBORDER_SUNKEN );

        for (unsigned int i = 0; i<temp.size(); ++i) {
                text->InsertItem(i, wxString::FromAscii(temp[i].c_str()));
        }
        textd->ShowModal();
}

#include <wx/dir.h>
#include "fileutil.h"

int CTRL_CONS_ID_TXT_JOBNAME=wxNewId();
int CTRL_CONS_ID_LC_JOBNAME=wxNewId();
int CTRL_CONS_ID_BT_OK=wxNewId();
int CTRL_CONS_ID_BT_CANCLE=wxNewId();

BEGIN_EVENT_TABLE(ConsDlg,wxDialog)
                                        EVT_LISTBOX(wxID_ANY, ConsDlg::OnText)
        EVT_LIST_ITEM_ACTIVATED(wxID_ANY, ConsDlg::OnLcActiveItem)
//        EVT_TEXT(wxID_ANY,ConsDlg::OnText)
        EVT_BUTTON(wxID_ANY, ConsDlg::OnButton)
                EVT_TEXT_ENTER(CTRL_CONS_ID_TXT_JOBNAME,ConsDlg::OnEnter)
END_EVENT_TABLE()

ConsDlg::ConsDlg(wxWindow* parent,SimuProject* pProject,wxWindowID id,const wxString& title,const wxPoint& pos,const wxSize& size,
                long style)
{
        wxDialog::Create(parent,id,title,pos,wxSize(280,290),style);
        m_pProject=pProject;

        //        wxString jobName=wxString::Format(wxT("%s%d"),wxT("Job"),::wxGetUTCTime());
        wxBoxSizer* bs=new wxBoxSizer(wxVERTICAL);

        wxStaticText* label=new wxStaticText(this,wxID_ANY,wxT("Please select a job name:"),wxPoint(10,10),wxSize(200,25));
        bs->Add(label,1,wxEXPAND);

        m_lcJobName = new wxListBox(this,CTRL_CONS_ID_LC_JOBNAME,wxPoint(10,40),wxSize(250,160));
        bs->Add(m_lcJobName,1,wxEXPAND);
        
        wxBoxSizer* bsBt = new wxBoxSizer(wxHORIZONTAL);
        wxButton* bt = new wxButton(this,CTRL_CONS_ID_BT_OK,wxT("Ok"),wxPoint(120,230),wxSize(60,25));
        bsBt->Add(bt,1,wxEXPAND);
        bt = new wxButton(this,CTRL_CONS_ID_BT_CANCLE,wxT("Cancel"),wxPoint(190,230),wxSize(60,25));
        bsBt->Add(bt,1,wxEXPAND);
        bs->Add(bsBt,1,wxEXPAND);
        // std::cout << "consdlg 1.2: " << std::endl;
        if(m_pProject==NULL)
        return;

        wxString dir=m_pProject->GetFileName().BeforeLast(platform::PathSep().GetChar(0));
        wxDir theDir(dir);
        wxString fileName=wxEmptyString;
        // std::cout << "consdlg 1.3: " << std::endl;
        bool cont = theDir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
        wxString tmp;
        wxArrayString existJob;
        // std::cout << "consdlg 1.4: " << std::endl;
        while ( cont ) {
                tmp=m_pProject->GetFiles(fileName)->GetInputFileName();//dir+platform::PathSep()+fileName+platform::PathSep()+fileName+wxMINPUT_FILE_SUFFIX;
                if(wxFileExists(tmp))
                existJob.Add(fileName);
                cont = theDir.GetNext(&fileName);
        }
        // std::cout << "consdlg 1.5: " << std::endl;
        existJob.Sort();
        wxString* str = new wxString[existJob.GetCount()];
        for(unsigned int i=0;i<existJob.GetCount();i++) {
                str[i] = existJob[i];
                //        int index=m_lcJobName->InsertItem(i, tmp);
        }
        // std::cout << "consdlg 1.6: " << str << "get count " << existJob.GetCount() << std::endl;
        if (existJob.GetCount() > 0)
                m_lcJobName->InsertItems(existJob.GetCount(), str, 0);
        // std::cout << "consdlg 2: " << std::endl;

}
ConsDlg::~ConsDlg() {
}

wxString ConsDlg::GetJobName() {
        return m_txtJobName;
}
///////////////////////////////////////////////////////////////
void ConsDlg::OnButton(wxCommandEvent& event) {

        int id = event.GetId();
        if (id == CTRL_CONS_ID_BT_OK) {
                wxString jobName = m_txtJobName;

                if (jobName.IsEmpty()) {
                                wxMessageBox(wxT("Please select a job first!"));
                                return;
                }

                wxString dir=m_pProject->GetFileName().BeforeLast(platform::PathSep().GetChar(0))+platform::PathSep()+jobName;
                jobName=m_pProject->GetFiles(jobName)->GetInputFileName();
        //dir+platform::PathSep()+jobName+wxMINPUT_FILE_SUFFIX;

                if(!wxFileExists(jobName) ) {
                        wxMessageDialog mdlg(this,wxT("The job file dosn't exists?"),wxT("warning"),wxYES_NO);
                                return;
                }

 //       FileUtil::CreateDirectory(dir);
                EndModal(wxID_OK);
        } else {
                EndModal(wxID_CANCEL);
        }
}
void ConsDlg::OnEnter(wxCommandEvent& event) {
        event.SetId(CTRL_CONS_ID_BT_OK);
        OnButton(event);;
}
void ConsDlg::OnLcActiveItem(wxListEvent& event) {
        wxString name = event.GetText();
        m_txtJobName = name;
}

void ConsDlg::OnText(wxCommandEvent& event) {
        m_txtJobName = m_lcJobName->GetString(event.GetSelection());
}
