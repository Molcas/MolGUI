/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#include "ctrlids.h"
int MENU_ID_WIN_START = wxNewId();
int MENU_ID_WIN1 = wxNewId();
int MENU_ID_WIN2 = wxNewId();
int MENU_ID_WIN3 = wxNewId();
int MENU_ID_WIN4 = wxNewId();
int MENU_ID_WIN5 = wxNewId();
int MENU_ID_WIN6 = wxNewId();
int MENU_ID_WIN7 = wxNewId();
int MENU_ID_WIN8 = wxNewId();
int MENU_ID_WIN_END = wxNewId();
//int MENU_ID_BUILD_VIEW = wxNewId();
/*
int MENU_ID_FILE_RECENT_FILES = wxNewId();
int MENU_ID_FILE_RECENT_FILE0 = wxNewId();
int MENU_ID_FILE_RECENT_FILE1 = wxNewId();
int MENU_ID_FILE_RECENT_FILE2 = wxNewId();
int MENU_ID_FILE_RECENT_FILE3 = wxNewId();
int MENU_ID_FILE_RECENT_FILE4 = wxNewId();
int MENU_ID_FILE_RECENT_FILE5 = wxNewId();
int MENU_ID_FILE_RECENT_FILE6 = wxNewId();
int MENU_ID_FILE_RECENT_FILE7 = wxNewId();
int MENU_ID_FILE_RECENT_FILE8 = wxNewId();
int MENU_ID_FILE_RECENT_FILE9 = wxNewId();
int MENU_ID_FILE_CLEAR_RECENT_FILES = wxNewId();
//
int MENU_ID_FILE_SAVE_PROJECT_AS = wxNewId();
int MENU_ID_FILE_IMPORT = wxNewId();
int MENU_ID_FILE_EXPORT = wxNewId();
int MENU_ID_FILE_EXPORT_FILE = wxNewId();
int MENU_ID_FILE_EXPORT_IMAGE = wxNewId();


int MENU_ID_MOL_START = wxNewId();

int MENU_ID_EDIT_CLEAR = wxNewId();
int MENU_ID_EDIT_UNDO = wxNewId();
int MENU_ID_EDIT_REDO = wxNewId();
int MENU_ID_EDIT_ATOM_LIST = wxNewId();
int MENU_ID_EDIT_CONSTRAIN_LIST = wxNewId();

int MENU_ID_EDIT_TOOLBARS = wxNewId();
int MENU_ID_EDIT_TOOLBARS_FILE = wxNewId();
int MENU_ID_EDIT_TOOLBARS_BUILD = wxNewId();
int MENU_ID_EDIT_TOOLBARS_GEOMETRY = wxNewId();
int MENU_ID_EDIT_TOOLBARS_CALC = wxNewId();
int MENU_ID_EDIT_VIEWSETTINGS = wxNewId();
//
int MENU_ID_MODEL_WIRE = wxNewId();
int MENU_ID_MODEL_TUBE = wxNewId();
int MENU_ID_MODEL_BALLSPOKE = wxNewId();
int MENU_ID_MODEL_BALLWIRE = wxNewId();
int MENU_ID_MODEL_SPACEFILLING = wxNewId();
int MENU_ID_MODEL_RIBBON = wxNewId();

int MENU_ID_MODEL_SHOW_HYDROGENS = wxNewId();
int MENU_ID_MODEL_SHOW_GLOBAL_LABELS = wxNewId();
int MENU_ID_MODEL_SHOW_ATOM_LABELS = wxNewId();
int MENU_ID_MODEL_SHOW_HYDROGEN_BONDS = wxNewId();
int MENU_ID_MODEL_SHOW_AXES = wxNewId();
int MENU_ID_MODEL_SHOW_LAYERS = wxNewId();
int MENU_ID_MODEL_SHOW_LAYERS_SHOW = wxNewId();
int MENU_ID_MODEL_SHOW_LAYERS_HIGH = wxNewId();
int MENU_ID_MODEL_SHOW_LAYERS_MEDIUM = wxNewId();
int MENU_ID_MODEL_SHOW_LAYERS_LOW = wxNewId();
//


int MENU_ID_BUILD_ADD_FRAGMENT = wxNewId();
int MENU_ID_BUILD_INSERT_FRAGMENT = wxNewId();
int MENU_ID_BUILD_DELETE_FRAGMENT = wxNewId();
int MENU_ID_BUILD_MAKE_BOND = wxNewId();
int MENU_ID_BUILD_BREAK_BOND = wxNewId();
int MENU_ID_BUILD_SELECT_LAYER = wxNewId();

int MENU_ID_BUILD_CREATE_TEMPLATE = wxNewId();
int MENU_ID_BUILD_DELETE_TEMPLATE = wxNewId();

int MENU_ID_BUILD_RESET = wxNewId();
//
//int MENU_ID_GEOMETRY_MEASURE = wxNewId();
int MENU_ID_GEOMETRY_CONSTRAIN = wxNewId();
int MENU_ID_GEOMETRY_SYMMETRIZE = wxNewId();
int MENU_ID_GEOMETRY_DEFINE_DUMMY = wxNewId();
int MENU_ID_GEOMETRY_MIRROR = wxNewId();
//
//int MENU_ID_JOB_NEW = wxNewId();
//int MENU_ID_JOB_SETTING = wxNewId();
//int MENU_ID_JOB_SUBMIT = wxNewId();
//int MENU_ID_JOB_MONITOR = wxNewId();
//
int MENU_ID_CALC_JOB_SIMUCALC = wxNewId();
int MENU_ID_CALC_JOB_MING = wxNewId();
int MENU_ID_CALC_JOB_GAUSSIAN = wxNewId();
int MENU_ID_CALC_SUBMIT = wxNewId();

//int MENU_ID_CALC_SYMMETRIZE=wxNewId();
int MENU_ID_IMPORT_STRUCTURE=wxNewId();

int MENU_ID_CALC_MM_OPTIMIZATION = wxNewId();
//begin : hurukun : 12/11/2009
int MENU_ID_CALC_MOPAC_OPTIMIZATION = wxNewId();
//end   : hurukun : 12/11/2009
int MENU_ID_CALC_SYMMETRY = wxNewId();
//
int MENU_ID_DISPLAY_NORMAL = wxNewId();
int MENU_ID_DISPLAY_CALCULATED = wxNewId();
int MENU_ID_DISPLAY_OUTPUT = wxNewId();
int MENU_ID_DISPLAY_SUMMARY = wxNewId();
int MENU_ID_DISPLAY_SURFACES_MO = wxNewId();
int MENU_ID_DISPLAY_VIBRATION = wxNewId();
int MENU_ID_DISPLAY_JOB_MONITOR = wxNewId();

int POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS = wxNewId();
int POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS = wxNewId();

int MENU_ID_MOL_END = wxNewId();

int MENU_ID_WORKER_EVENT=wxNewId();

int TOOL_ID_FILE_NEW = wxNewId();
int TOOL_ID_FILE_OPEN = wxNewId();
int TOOL_ID_FILE_SAVE = wxNewId();
int TOOL_ID_FILE_SAVE_AS = wxNewId();
int TOOL_ID_FILE_CLOSE = wxNewId();
int TOOL_ID_FILE_EXPORT = wxNewId();
int TOOL_ID_FILE_EXIT = wxNewId();

int TOOL_ID_MOL_START = wxNewId();

int TOOL_ID_BUILD_VIEW = wxNewId();
int TOOL_ID_BUILD_ADD_FRAGMENT = wxNewId();
int TOOL_ID_BUILD_INSERT_FRAGMENT = wxNewId();
int TOOL_ID_BUILD_DELETE_FRAGMENT = wxNewId();
int TOOL_ID_BUILD_MAKE_BOND = wxNewId();
int TOOL_ID_BUILD_BREAK_BOND = wxNewId();
int TOOL_ID_BUILD_SELECT_LAYER = wxNewId();

int TOOL_ID_GEOMETRY_MEASURE = wxNewId();
int TOOL_ID_GEOMETRY_CONSTRAIN = wxNewId();
int TOOL_ID_GEOMETRY_SYMMETRIZE = wxNewId();
int TOOL_ID_GEOMETRY_DEFINE_DUMMY = wxNewId();
int TOOL_ID_GEOMETRY_MIRROR = wxNewId();
//
int TOOL_ID_CALC_JOB_SIMUCALC = wxNewId();
int TOOL_ID_CALC_JOB_GAUSSIAN = wxNewId();
int TOOL_ID_CALC_JOB_MING = wxNewId();

int TOOL_ID_CALC_MM_OPTIMIZATION = wxNewId();
//begin : hurukun : 06/11/2009
int TOOL_ID_CALC_MOPAC_OPTIMIZATION= wxNewId();
//end   : hurukun : 06/11/2009
int TOOL_ID_CALC_SYMMETRY = wxNewId();

int TOOL_ID_MOL_END = wxNewId();
*/
int TIMER_ID = wxNewId();

