/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#include <wx/wx.h>
#include "uiprops.h"
#include <wx/tokenzr.h>
#include <wx/filename.h>
#include "molvieweventhandler.h"
#include "stringutil.h"
#include "fileutil.h"
#include "ctrlids.h" //SONG add it for workshop2009
#include "envutil.h" //SONG add it 1006
#include "resource.h"
#include <wx/stdpaths.h>

#include <fstream>
#include <string>
using namespace std;

UIProps* pUIProps = new UIProps;

UIData* pUIData = new UIData;  //SONG add it for workshop2009

//SONG add it for workshop2009
UIData* GetUIData() {
    return pUIData;
}

//SONG add it for workshop2009
void FreeUIData(){
        if(pUIData)delete pUIData;
}

UIProps* GetUIProps(void) {
    return pUIProps;
}

UIColor::UIColor() {
    r = g = b = 0.0f;
    alpha = 1.0f;
}

UIColor::UIColor(float cr, float cg, float cb) {
    r = cr;
    g = cg;
    b = cb;
    alpha = -1000.0f;
}

UIColor::UIColor(float cr, float cg, float cb, float calpha) {
    r = cr;
    g = cg;
    b = cb;
    alpha = calpha;
}

void UIColor::SetColor(const wxColour& color) {
    r = color.Red() / 255.0;
    g = color.Green() / 255.0;
    b = color.Blue() / 255.0;
}

//change the RGB to wxColour
wxColour UIColor::GetColor(void) const {
    return wxColour((unsigned char)(r*255.0), (unsigned char)(g*255.0), (unsigned char)(b*255.0));
}


void UIColor::ToColor(wxString colorStr) {
    double doubleValue = 0;
    wxArrayString strArrTokens = wxStringTokenize(colorStr, wxT(","));
    strArrTokens[0].ToDouble(&doubleValue);
    r = (float) doubleValue;
    strArrTokens[1].ToDouble(&doubleValue);
    g = (float) doubleValue;
    strArrTokens[2].ToDouble(&doubleValue);
    b = (float) doubleValue;
    if(strArrTokens.GetCount() > 3) {
        strArrTokens[3].ToDouble(&doubleValue);
        alpha = (float) doubleValue;
    }
}

wxString UIColor::ToString(void) const{
    if(alpha < -100) {
        return wxString::Format(wxT("%.1f, %.1f, %.1f"), r, g, b);
    }else {
        return wxString::Format(wxT("%.1f, %.1f, %.1f, %.1f"), r, g, b, alpha);
    }
}


void UIProps::SaveColour(wxColour colour) {
#ifdef __WINDOWS__
    string sep = "\\";
#else
    string sep = "/";
#endif
    // char cfile[100];  // EnvUtil::GetBaseDir().ToAscii();
    // getcwd(cfile, 100);
    // string colrfile = cfile + sep + "res" + sep + "ming" + sep + "data" + sep + "colr.set";
    wxString COLR_FILE = EnvUtil::GetPrgResDir() + wxTRUNK_MING_DAT_PATH + wxT("colr.set");

    ofstream out(COLR_FILE.mb_str());
    out << (int)colour.Red() << endl << (int)colour.Green() << endl << (int)colour.Blue();
    out.close();
}

wxColour OpenGLProps::LoadColour() {
    #ifdef __WINDOWS__
            string sep = "\\";
    #else
            string sep = "/";
    #endif

    wxColour colour(0, 153, 255);
    // wxString COLR_FILE = EnvUtil::GetPrgResDir()+ wxTRUNK_MING_DAT_PATH + wxT("colr.set");
    // fstream  test(COLR_FILE.mb_str(), ios::in);
    // if (!test) {
    //             ofstream out(COLR_FILE.mb_str());
    //             out << (int)colour.Red() << endl << (int)colour.Green() << endl << (int)colour.Blue();
    //             out.close();
    //     }
    //     ifstream in(COLR_FILE.mb_str());
    //     int  r, g, b;
    //     in >> r >> g >> b;
    //     in.close();
    //     colour.Set(r, g, b);

    previewBgColor.SetColor(colour);
    mainBgColor.SetColor(colour);
        return colour;
}

OpenGLProps::OpenGLProps() {
LoadColour();
    int i = 0, j = 0;
    mainDisableBgColor = UIColor(0.56f, 0.6f, 0.68f, 1.0f);
    lightSourceCount = 1;
    lightPositions[0][0]= -1.0f;
    lightPositions[0][1]= 0.1f;
    lightPositions[0][2]= 1.0f;
    lightPositions[0][3]= 0.0f;
    for(i = 1; i < LIGHT_COUNT_MAX; i++) {
        for(j = 0; j < 4; j++) {
            lightPositions[i][j] = 0.0f;
        }
    }
    staticObjectSlices = 100;
    staticObjectStacks = 100;
    movingObjectSlices = 50;
    movingObjectStacks = 50;
    isMovingWithBonds = 1;
    globalLabelColor = UIColor(1.0f, 1.0f, 1.0f);
    atomLabelColor = UIColor(1.0f, 0.8f, 0.0f);
    bondColor = UIColor(0.7f, 0.7f, 0.7f);
    bondRadius = 0.12f;
    selectionColor = UIColor(1.0f, 1.0f, 1.0f);
    manuSelectionColor = UIColor(0.5f, 1.0f, 0.0f);
    labelFont = wxT("LiberationMono-Regular.ttf");
    axesColor = UIColor(1.0f, 1.0f, 1.0f);
}

wxString OpenGLProps::LoadProps(wxFileInputStream& fis, wxTextInputStream& tis) {
    wxString strLine;
    wxArrayString strArrTokens;
    int i, j;
    long longValue = 0;
    double doubleValue = 0.0;
    while(!fis.Eof()){
                strLine = tis.ReadLine();
                if(!StringUtil::IsEmptyString(strLine)) {
            if(strLine.StartsWith(wxT("["), NULL)) {
                break;
            }
            strArrTokens = wxStringTokenize(strLine, wxT("="));
            if(strArrTokens.GetCount() != 2) continue;
            if(strArrTokens[0].Cmp(wxT("previewBgColor")) == 0) {
                previewBgColor.SetColor(wxColour(0, 153, 255));
            }else if(strArrTokens[0].Cmp(wxT("mainBgColor")) == 0) {
                mainBgColor.SetColor(wxColour(0, 153, 255));
            }else if(strArrTokens[0].Cmp(wxT("mainDisableBgColor")) == 0) {
                mainDisableBgColor.ToColor(strArrTokens[1]);
            }else if(strArrTokens[0].Cmp(wxT("lightSourceCount")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                lightSourceCount = (int)longValue;
                for(i = 0; i < lightSourceCount; i++) {
                    strLine = tis.ReadLine();
                    strArrTokens = wxStringTokenize(strLine, wxT("="));
                    strArrTokens = wxStringTokenize(strArrTokens[1], wxT(","));
                    for(j = 0; j < 4; j++) {
                        strArrTokens[j].ToDouble(&doubleValue);
                        lightPositions[i][j] = (float) doubleValue;
                    }
                }
            }else if(strArrTokens[0].Cmp(wxT("staticObjectSlices")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                staticObjectSlices = (int)longValue;
            }else if(strArrTokens[0].Cmp(wxT("staticObjectStacks")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                staticObjectStacks = (int)longValue;
            }else if(strArrTokens[0].Cmp(wxT("movingObjectSlices")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                movingObjectSlices = (int)longValue;
            }else if(strArrTokens[0].Cmp(wxT("movingObjectStacks")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                movingObjectStacks = (int)longValue;
            }else if(strArrTokens[0].Cmp(wxT("isMovingWithBonds")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                isMovingWithBonds = (int)longValue;
            }else if(strArrTokens[0].Cmp(wxT("globalLabelColor")) == 0) {
                globalLabelColor.ToColor(strArrTokens[1]);
            }else if(strArrTokens[0].Cmp(wxT("atomLabelColor")) == 0) {
                atomLabelColor.ToColor(strArrTokens[1]);
            }else if(strArrTokens[0].Cmp(wxT("bondColor")) == 0) {
                bondColor.ToColor(strArrTokens[1]);
            }else if(strArrTokens[0].Cmp(wxT("bondRadius")) == 0) {
                strArrTokens[1].ToDouble(&doubleValue);
                bondRadius = (float)doubleValue;
            }else if(strArrTokens[0].Cmp(wxT("selectionColor")) == 0) {
                selectionColor.ToColor(strArrTokens[1]);
            }else if(strArrTokens[0].Cmp(wxT("labelFont")) == 0) {
                labelFont = strArrTokens[1];
            }else if(strArrTokens[0].Cmp(wxT("axesColor")) == 0) {
                axesColor.ToColor(strArrTokens[1]);
            }
        }
    }
    //wxLogError(mainBgColor.ToString());
    if(fis.Eof()) {
        strLine = wxEmptyString;
    }
    return strLine;
}

MenuProps::MenuProps() {
    isToolbarStyleText = 0;
    isToolbarStyleFlat = 1;
}

wxString MenuProps::LoadProps(wxFileInputStream& fis, wxTextInputStream& tis) {
    wxString strLine;
    wxArrayString strArrTokens;
    long longValue = 0;
    while(!fis.Eof()){
                strLine = tis.ReadLine();
                if(!StringUtil::IsEmptyString(strLine)) {
            if(strLine.StartsWith(wxT("["), NULL)) {
                break;
            }
            strArrTokens = wxStringTokenize(strLine, wxT("="));
            if(strArrTokens.GetCount() != 2) continue;

            if(strArrTokens[0].Cmp(wxT("isToolbarStyleText")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                isToolbarStyleText = (int)longValue;
            }else if(strArrTokens[0].Cmp(wxT("isToolbarStyleFlat")) == 0) {
                strArrTokens[1].ToLong(&longValue);
                isToolbarStyleFlat = (int)longValue;
            }
        }
    }
    if(fis.Eof()) {
        strLine = wxEmptyString;
    }
    return strLine;
}

PanelProps::PanelProps() {
}

wxString PanelProps::LoadProps(wxFileInputStream& fis, wxTextInputStream& tis) {
    wxString strLine;
    wxArrayString strArrTokens;
    //long longValue = 0;
    while(!fis.Eof()){
                strLine = tis.ReadLine();
                if(!StringUtil::IsEmptyString(strLine)) {
            if(strLine.StartsWith(wxT("["), NULL)) {
                break;
            }
            if(strArrTokens.GetCount() != 2) continue;
        }
    }
    if(fis.Eof()) {
        strLine = wxEmptyString;
    }
    return strLine;
}

ChemProps::ChemProps() {
        layerModelLow = MODEL_WIRE;
        layerModelMedium = MODEL_TUBE;
        layerModelHigh = MODEL_BALLSPOKE;
}

wxString ChemProps::LoadProps(wxFileInputStream& fis, wxTextInputStream& tis) {
        wxString strLine;
    wxArrayString strArrTokens;
    while(!fis.Eof()){
                strLine = tis.ReadLine();
                if(!StringUtil::IsEmptyString(strLine)) {
            if(strLine.StartsWith(wxT("["), NULL)) {
                break;
            }
            strArrTokens = wxStringTokenize(strLine, wxT("="));
            if(strArrTokens.GetCount() != 2) continue;

            if(strArrTokens[0].Cmp(wxT("layerModelLow")) == 0) {
                                layerModelLow = GetLayerModelType(strArrTokens[1]);
            }else if(strArrTokens[0].Cmp(wxT("layerModelMedium")) == 0) {
                layerModelMedium = GetLayerModelType(strArrTokens[1]);
            }else if(strArrTokens[0].Cmp(wxT("layerModelHigh")) == 0) {
                layerModelHigh = GetLayerModelType(strArrTokens[1]);
            }
        }
    }
    if(fis.Eof()) {
        strLine = wxEmptyString;
    }
    return strLine;
}

ModelType ChemProps::GetLayerModelType(wxString strModel) {
        ModelType layer = MODEL_BALLSPOKE;
        if(strModel.Cmp(wxT("WIRE")) == 0) {
                layer = MODEL_WIRE;
        }else if(strModel.Cmp(wxT("TUBE")) == 0) {
                layer = MODEL_TUBE;
        }else if(strModel.Cmp(wxT("BALLSPOKE")) == 0) {
                layer = MODEL_BALLSPOKE;
        }else if(strModel.Cmp(wxT("BALLWIRE")) == 0) {
                layer = MODEL_BALLWIRE;
        }else if(strModel.Cmp(wxT("SPACEFILLING")) == 0) {
                layer = MODEL_SPACEFILLING;
        }

        return layer;
}

ModelType ChemProps::GetLayerModelType(LayerType layerType) {
        ModelType layer = MODEL_BALLSPOKE;
        switch(layerType) {
                case LAYER_HIGH:
                        layer = layerModelHigh;
                        break;
                case LAYER_MEDIUM:
                        layer = layerModelMedium;
                        break;
                case LAYER_LOW:
                        layer = layerModelLow;
                        break;
                default:
                        break;
        }
        return layer;
}

void UIProps::LoadProps(void) {
    wxString strLine = wxEmptyString;

    wxString fileName = FileUtil::CheckFile(wxT("usr"), wxT("uiprops.dat"), false);
    if(fileName == wxEmptyString) {
        fileName = FileUtil::CheckFile(EnvUtil::GetPrgResDir()+platform::PathSep()+wxT("res/mol"), wxT("uiprops.dat"), true);
    }

    wxFileInputStream fis(fileName);
        wxTextInputStream tis(fis);
        while(!fis.Eof()) {
        if(strLine == wxEmptyString) {
            strLine = tis.ReadLine();
        }
        if(strLine.Cmp(wxT("[OPENGL]")) == 0) {
            strLine = openGLProps.LoadProps(fis, tis);
        }else if(strLine.Cmp(wxT("[MENU]")) == 0) {
            strLine = menuProps.LoadProps(fis, tis);
        }else if(strLine.Cmp(wxT("[PANELS]")) == 0) {
            strLine = panelProps.LoadProps(fis, tis);;
        }else if(strLine.Cmp(wxT("[CHEM]")) == 0) {
            strLine = chemProps.LoadProps(fis, tis);;
        }
        if(strLine == wxEmptyString) {
            break;
        }
    }
}

int UIData::GetCtrlInstCount(void) const {
    return (int) m_ctrlInstArray.GetCount();
}

bool UIData::IsAllowAddCtrlInst(void) {
    if(GetCtrlInstCount() >= MENU_ID_WIN_END - MENU_ID_WIN_START) {
        wxMessageBox(wxT("You have reached the maximum number of windows opend concurrently."));
        return false;
    }else {
        return true;
    }
}

bool UIData::IsFileLoaded(const wxString& strFileName)
{
        for(unsigned i=0 ; i < m_ctrlInstArray.GetCount(); i++)
        {
                if(strFileName.Cmp(GetCtrlInst(i) ->GetFullPathName())==0)
                        return true;
        }
        return false;

}

CtrlInst* UIData::GetCtrlInst(int index) {
    if( index < 0 || index >= GetCtrlInstCount()) {
        return NULL;
    }
    return m_ctrlInstArray[index];
}
