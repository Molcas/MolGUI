/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

/***************************************************************************
 the functions in this file is resposible to calculate the symmetry
 by the given coordinates. all the source codes may be added step by step
 the work is from Oct. 17th, 2008
 10-17:
   create the source files.
****************************************************************************/


#include "symmetrygene.h"







// this is the interface to the GUI
SymGeneInfor SymmetryGene (const AtomBondArray & atomBondArray)
{

  // head of the lists
  //SymGene * list;

  return SUCCESS;

}
