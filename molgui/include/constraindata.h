/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_CONSTRAINDATA_H__
#define __BP_MOL_CONSTRAINDATA_H__

#include "wx.h"
#include "datatype.h"

#include "chematom.h"
#include "commons.h"

typedef enum _consType {
        CONS_BOND = 2,
        CONS_ANGLE,
        CONS_DIHEDRAL,
        CONS_NULL = 0
}ConsType;

class FreezeAtom {
        public:
                long uniqueId;
                AtomPosition pos;
        public:
                FreezeAtom();
                FreezeAtom(const ChemAtom& chemAtom);
                FreezeAtom(const AtomBond& atomBond);
                FreezeAtom& operator =(const AtomBond &atomBond);
                FreezeAtom& operator =(const ChemAtom &chemAtom);
};

class ConsElem {
        public:
                wxArrayLong elemInfos;
                double value;

        public:
                ConsElem();
                ConsElem(wxArrayLong infos, double val);
                ConsElem(long info1, long info2, double length);
                ConsElem(long info1, long info2, long info3, double angle);
                ConsElem(long info1, long info2, long info3, long info4, double dihedral);
                virtual ~ConsElem();

                wxString ToString(void);
                ConsType GetConsType(void)const;
                ConsElem& operator =(const wxString &consInfo);
                ConsElem& operator =(const ConsElem &ce);
                bool operator==(const ConsElem &ce) const;
                bool operator!=(const ConsElem &ce) const;
};

class ConstrainData {
        public:
                ConstrainData();
                virtual ~ConstrainData();

                void UpdateFreezeAtom(FreezeAtom& fa);
                void UpdateDataList(AtomBondArray& chemArray);
                wxString ToString(AtomBondArray &chemArray) ;
                void DumpData(AtomBondArray& chemArray);
                wxArrayString GetConstrainDihedralArray(AtomBondArray &chemArray, bool useTab, bool withType);
                wxArrayString GetConstrainAngleArray(AtomBondArray &chemArray, bool useTab, bool withType) ;
                wxArrayString GetConstrainLengthArray(AtomBondArray &chemArray, bool useTab, bool withType);
                void ClearAllData(void);
                bool UpdateConstrainData(ConsElem &ce) ;
                bool HasConstrainData(ConsElem &ce) const;
                bool RemoveConstrainData(ConsElem &ce)  ;
                bool AppendConstrainData(ConsElem ce);
                void UpdateConstrainList(AtomBondArray& chemArray);
                //freeze
                void RemoveFreezeData(long uniqueId);
                bool HasFreezeData(long uniqueId)const;
                void AppendFreezeData(FreezeAtom& fa);
                void UpdateFreezeList(AtomBondArray& chemArray);
                //wxString OutputForMolcasConstrain(AtomBondArray& chemArray);

        private:
                LngSHash posHash;
                StrDHash lengthHash;
                StrDHash angleHash;
                StrDHash dihedralHash;

        private:
                wxArrayString GetConstrainDataArray(AtomBondArray &chemArray, ConsType consType, bool useTab, bool withType);
                void UpdateConstrainList(AtomBondArray &chemArray, ConsType type);

};

#endif
