/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_CHEMATOM_H__
#define __BP_MOL_CHEMATOM_H__

#include "commons.h"

class ChemAtom {

public:
    ChemAtom();
    float GetDispRadius(ModelType modelType = MODEL_BALLSPOKE) const;
    float GetOriginalDispRadius(void) const;
    void SetDispRadius(float dr);
    float GetSelectRadius(ModelType modelType = MODEL_BALLSPOKE) const;
    void ResetUniqueId(void);
    bool IsDefaultHydrogen(void) const;
    bool IsDummy(void) const;
    void Clone(ChemAtom atom);

    static void Translate(ChemAtom& atom, float transX, float transY, float transZ);
    static void Translate(ChemAtom& atom, AtomPosition3F posTrans);
    static void Translate(AtomPosition3F& atomPos, AtomPosition3F posTrans);
    static void Translate(AtomPosition3F& atomPos, float transX, float transY, float transZ);
    static void RotateWithX(ChemAtom& atom, float angle);
    static void RotateWithY(ChemAtom& atom, float angle);
    static void RotateWithZ(ChemAtom& atom, float angle);
    static void RotateWithX(AtomPosition& atomPos, float angle);
    static void RotateWithY(AtomPosition& atomPos, float angle);
    static void RotateWithZ(AtomPosition& atomPos, float angle);
    static void RotateWithX(AtomPosition3F& atomPos, float angle);
    static void RotateWithY(AtomPosition3F& atomPos, float angle);
    static void RotateWithZ(AtomPosition3F& atomPos, float angle);

public:
    long uniqueId;
    int elemId;
    int label;
    AtomPosition pos;
    short layer;
    short connGroup;        //connect group index, if atoms with the same connGroup,
                        // all these atoms are connnected
private:
    float dispRadius;
    static long GLOBAL_UNIQUE_ID;
};

class ChemBond {
public:
    int atomIndex;      // connected atom index in AtomBondArray with this atom
    short bondType;     // corresponding connected bond type
    short dispType;     // bond display type

public:
    ChemBond();
    void Clone(ChemBond chemBond);
    void Clear(void);
    bool ChangeBondTypeToNext(void);
};

class AtomBond {
public:
    ChemAtom atom;
    ChemBond bonds[ATOM_MAX_BOND_COUNT];
    int bondCount;

public:
    AtomBond();
    void Clone(AtomBond atomBond);
    void RemoveBond(int bondIndex);
};

class AtomAngle {
public:
    int atomStillUniqueId;
    int atomAxisUniqueId;
    int atomMoveUniqueId;
    double angle;

public:
    AtomAngle();
};

class ChemLigand {
public:
        long dummyAtomId;
        wxArrayLong linkedAtomIds;

public:
        ChemLigand();
        wxString ToString();
};

WX_DECLARE_OBJARRAY(AtomPosition, AtomPositionArray);
//WX_DECLARE_OBJARRAY(AtomPosition3F, AtomPosition3FArray);
WX_DECLARE_OBJARRAY(AtomBond, AtomBondArray);
WX_DECLARE_OBJARRAY(AtomAngle, AtomAngleArray);
WX_DECLARE_OBJARRAY(ChemLigand, ChemLigandArray);

#endif
