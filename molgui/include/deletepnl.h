/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_DELETEPANEL_H__
#define __BP_MOL_DELETEPANEL_H__

#include "wx.h"
#include "ctrlinst.h"


class DeletePnl : public wxPanel {

private:
        DECLARE_EVENT_TABLE();

public:
    DeletePnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30), long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panelmirror"));
    void SetCtrlInst(CtrlInst* pCtrlInst);
    void ClearSelection(void);
    void AddSelectedIndex(int selectedAtomIndex, bool isDoubleClicked);
        void UpdateControls(ActionType actionType);

private:
        void InitPanel(void);
        void OnBtnDelete(wxCommandEvent& event);
        void OnBtnClear(wxCommandEvent& event);
        void OnBtnSelectAll(wxCommandEvent& event);
        void DoClear(void);
        LayerType GetSelectedLayer(void);

private:
    wxButton* m_pBtnDelete;
    wxButton* m_pBtnClear;
    wxRadioBox* m_pRbSelectType;
        CtrlInst* m_pCtrlInst;
        wxStaticText* m_pStSetLayer;
        wxComboBox* m_pCbbLayerType;
        ActionType m_actionType;
};

#endif
