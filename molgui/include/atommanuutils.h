/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_ATOMMANUUTILS_H__
#define __BP_MOL_ATOMMANUUTILS_H__

#include "commons.h"
#include "selectionutil.h"
/***************************************************************************
this function is for searching the atoms connected to the
specified atom- atomForConnection, by employing the
connection table defined in the  ChemAtom.h. in that data structure,
the connection table is the
      ChemBond bonds[ATOM_MAXIMUM_BOND_NUMBER]
in the ChemBond structure, connection table is made clear
****************************************************************************
the connection table instructions:
   the connection array is N*6(N is the number of atoms), 6 indicates
   that there's only 6 maxmum bonds linking to this atom. and actually
   it is the ATOM_MAXIMUM_BOND_NUMBER.

   the content of this array (example):
   atom_11(connected with atom of 1) atom_12 atom_13 ... -1 -1
   atom_21(connected with atom of 2) atom_22 atom_23 ... -1 -1
   ................................

   rules to establish the array:
       1 -1 always ends the row
       2 the row number is the atom number-1, and it's element
         in the row is the bonding atom number
       3 if the bond is breaking or the atom is deleting beacause
         of bond, its element is -2
*****************************************************************************
the specification of the moving atoms:

   typedef struct _movingAtoms {
  int atomLabel;
  double x;
  double y;
  double z;
} MovingAtoms;

   this structure is defined in the bp/commons.h. in the way of group
   moving, we use this structrue array to contains the necessary
   information of the moving atoms. the atomLabel is the
   atom which moves, and we use the label to locate its position in
   atombondarray. the x, y,z is its cordinate used in the calculation.
*****************************************************************************/
/****************************************************************************
 * this functions returns the connection atoms with the "atomForConnection",
 * which use the struct array of movingatom (see above) to store the result
 * the length of this array is the parameter of length
 *****************************************************************************/
MovingAtoms * SearchConnectedAtoms(const AtomBondArray & atomBondArray, int atomForConnection, int * length);



/**********************************************************************
 this groups of function used to rotate a group around the axis
 to some angle
***********************************************************************/
void GroupRotationByZ (MovingAtoms * movingAtoms, int movingAtomArrayLength, double angle);
void GroupRotationByY (MovingAtoms * movingAtoms, int movingAtomArrayLength, double angle);
void GroupRotationByX (MovingAtoms * movingAtoms, int movingAtomArrayLength, double angle);

/* reflection on the group*/
void GroupReflection (MovingAtoms * movingAtoms, int movingAtomArrayLength);



typedef enum _fragmentInfo {
    ERROR_IN_MEMORY_ALLOCATION,
    NO_BOND_LINK_TWO_SELECT_ATOMS,
    SUCCESS,
} FragmentInfo;



/************************************************************************
 * this function provides selection to some specific fragment
 * so this function is called FragSelect
 * atom1 and atom2 are two selected atoms, if the user choose "select all",
 * one of them is set to "BP_NULL_VALUE". else we will search the connected
 * atoms with atom2
 ************************************************************************/
FragmentInfo FragSelect(AtomBondArray & atomBondArray, SelectionUtil* pSelUtil, AtomSelectType atomSelectType, int atom1, int atom2);

#endif
