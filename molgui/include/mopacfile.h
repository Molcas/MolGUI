/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOPACFILE_H__
#define __BP_MOPACFILE_H__

#include "wx.h"

class MopacFile {
public:
        //begin : hurukun : 12/11/2009
        //generate a mopac-input file from a simu-input file
        static wxString SimuToMopac(const wxString simufile,const wxString mopacfile,const wxString& paraMethod,const wxString& paraMulti,const wxString& paraOptions,const int ncharge);
        //generate a simu-output file from a mopac-output file.
        static wxString MopacToSimu(const wxString mopacinfile,const wxString mopacoutfile,const wxString simufile);
        //end   : hurukun : 12/11/2009
};
#endif
