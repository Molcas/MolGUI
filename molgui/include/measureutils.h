/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MEASUREUTILS_H__
#define __BP_MOL_MEASUREUTILS_H__

#include "commons.h"

/******************************************************
 this groups of function used to rotate around the axis
*******************************************************/
void RotationByX (double *y, double *z, double angle);
void RotationByY (double *x, double *z, double angle);
void RotationByZ (double *x, double *y, double angle);

/* reflection on an atom */
void Reflection (double *x, double *y, double *z);

#endif
