/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_ATOMLISTFRM_H__
#define __BP_MOL_ATOMLISTFRM_H__

#include "wx.h"
#include "ctrlinst.h"
#include "molview.h"
#include <wx/grid.h>

class AtomGridTable : public wxGridTableBase {
public:
    AtomGridTable(MolView* pMolView, CtrlInst* pCtrlInst);

    virtual int GetNumberRows();
    virtual int GetNumberCols();
    virtual bool IsEmptyCell( int row, int col );
    virtual wxString GetValue( int row, int col );
    virtual void SetValue( int row, int col, const wxString& value );

    virtual wxString GetColLabelValue( int col );

private:
        MolView* m_pMolView;
        CtrlInst* m_pCtrlInst;
};

// ----------------------------------------------------------------------------
// custom attr provider: this one makes all odd rows appear light blue
// ----------------------------------------------------------------------------
class RowColorGridCellAttrProvider : public wxGridCellAttrProvider {
public:
    RowColorGridCellAttrProvider();
    virtual ~RowColorGridCellAttrProvider();

    virtual wxGridCellAttr *GetAttr(int row, int col, wxGridCellAttr::wxAttrKind  kind) const;

private:
    wxGridCellAttr *m_attrForOddRows;
    wxGridCellAttr *m_attrForEmptyCell;
};

class AtomListFrm : public wxFrame {
        public:
                // class constructor
                AtomListFrm(wxFrame *parent,
                                 wxWindowID id = wxID_ANY,
                                 const wxString &title = wxT("Atom List Editor"),
                                 const wxPoint& pos = wxDefaultPosition,
                                 const wxSize& size = wxDefaultSize,
                 //long style = wxCAPTION | wxMINIMIZE_BOX | wxMAXIMIZE_BOX  |wxCLOSE_BOX | wxRESIZE_BORDER | wxRESIZE_BORDER);
                        long style = wxDEFAULT_FRAME_STYLE | wxRESIZE_BORDER|wxFRAME_FLOAT_ON_PARENT);
                ~AtomListFrm();
                void InitTable(MolView* pMolView, CtrlInst* pCtrlInst);
                void UpdateFrameTitle(wxString title);

        protected:
                void CreateMenu(void);
                void InitToolBar(void);

        private:
                DECLARE_EVENT_TABLE()
                //void OnClose(wxCloseEvent& event);
                void OnClose(wxCloseEvent& event);
                void OnQuit(wxCommandEvent& event);
                void OnEditReconnect(wxCommandEvent& event);
                void OnMenuShowCols(wxCommandEvent& event);
                void OnToolShowCols(wxCommandEvent& event);
                void UpdateMenuShowCols(int evnetId, bool isMenu);
                void OnRefresh(wxCommandEvent& event);

                void OnRangeSelected(wxGridRangeSelectEvent& event);

                void SetColAttrs(void);
                void UpdateColSizes(void);

        private:
                wxGrid *m_grid;
                AtomGridTable* m_atomGridTable;
                MolView* m_pMolView;
                CtrlInst* m_pCtrlInst;
};

class wxGridCellBitmapRenderer : public wxGridCellRenderer {
        public:
                wxGridCellBitmapRenderer();
                virtual ~wxGridCellBitmapRenderer();

                virtual wxGridCellRenderer * Clone() const;
                virtual wxSize GetBestSize(wxGrid& grid, wxGridCellAttr& attr, wxDC& dc, int row, int col);
                virtual void Draw(wxGrid& grid, wxGridCellAttr& attr, wxDC& dc, const wxRect& rect, int row, int col, bool isSelected);
};

#endif
