/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_TOGGLEBITMAPBUTTON_H__
#define __BP_MOL_TOGGLEBITMAPBUTTON_H__

#include "wx.h"
#include <wx/tglbtn.h>

class ToggleBitmapButton : public wxWindow {
public:
    DECLARE_DYNAMIC_CLASS (ToggleBitmapButton);

private:
    DECLARE_EVENT_TABLE();

public:
    ToggleBitmapButton() {}
    ToggleBitmapButton(wxWindow *parent,
                   wxWindowID id,
                   const wxPoint& pos = wxDefaultPosition,
                   const wxSize& size = wxDefaultSize,
                   long style = 0,
                   const wxValidator& validator = wxDefaultValidator,
                   const wxString& name = wxT("ToggleBitmapButton"));

    bool GetValue() const;
    void SetValue(const bool state);

    void SetBitmapToggled(const wxBitmap& bitmap);
    void SetBitmapNormal(const wxBitmap& bitmap);
    void SetLabel(const wxString& label);

    void OnPaint(wxPaintEvent &event);
    void OnMouseEvent(wxMouseEvent& event);
    void OnEraseBackground(wxEraseEvent &event);
    void OnSizeEvent(wxSizeEvent& event);
    void OnChar(wxKeyEvent& event);
    void OnSetFocus(wxFocusEvent& event);
    void OnKillFocus(wxFocusEvent& event);

    virtual bool Enable(bool enable = true);
    virtual bool IsEnabled() const;
    virtual void SetSize(int x, int y, int width, int height, int sizeFlags = wxSIZE_AUTO);
    virtual void SetSize(const wxRect& rect);
    virtual void SetSize(int width, int height);
    virtual void SetSize(const wxSize& size);


protected:
    typedef enum  tglButtonState{
        STATE_BUTTON_UP,
        STATE_BUTTON_DOWN,
        STATE_BUTTON_OVER,
        STATE_BUTTON_DISABLE,
        STATE_BUTTON_NEW
    }ButtonState;

    typedef enum tglBorderType {
        BORDER_SUNKEN,
        BORDER_FLAT,
        BORDER_OVER,
        BORDER_HIGH
    }BorderType;

protected:
    void DrawOnBitmap();
    void DrawBorder(wxDC& dc, BorderType border);
    void Redraw();
    void DispathEvent(void);

private:
    wxBitmap* m_bitmap;
    wxBitmap m_buttonBitmapNormal;
    wxBitmap m_buttonBitmapToggled;
    wxString m_strLabel;

    ButtonState m_buttonState;
    ButtonState m_buttonStatePrev;
    bool m_painted;
    bool m_enabled;
    bool m_hasFocus;

    int m_width;
    int m_height;
    int m_dx;
    int m_dy;
};

#endif
