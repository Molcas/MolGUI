/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

/*
 * symmetrizepnl.h
 *
 *  Created on: 2010-8-2
 *      Author: allan
 */
#ifndef __BP_MOL_SYMMETRIZEPNL_H__
#define __BP_MOL_SYMMETRIZEPNL_H__

#include "wx.h"

#include "commons.h"
#include "measure.h"
#include "togglebitmapbutton.h"
#include "ctrlinst.h"

#include <fstream>
#include <string>
#include <vector>
using namespace std;

class SymmetrizePnl : public wxPanel {

private:
        DECLARE_EVENT_TABLE();

public:
    SymmetrizePnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30), long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panelmeasure"));
    void InitPanel(void);

    void SetMeasureOrConstrain(bool isMeasure);

    void SetCtrlInst(CtrlInst* pCtrlInst);
    void ClearSelection(void);

    void AddSelectedIndex(int selectedAtomIndex);
    void AddSelectedIndex(int selectedAtomIndex[2]);

    static void ShowMeasureInfo(MeasureInfo mInfo);

    void ChangeBondValueByPageKey(bool isUp);
                        void DoGlo();
        bool CanClear() const{
                return !m_pCkboxGlobal->GetValue();
        }
private:
    void DoPositionChanged(int ctrlId, bool isSpinBtn);
    MeasureInfo DoBondChange(float value);

        void DoMeasure(void);
        void SetInitPositionValue(AtomPosition& atomPos);
        void SetPositionEnable(bool enable);
        void SetInitBondValue(float value);
        void UpdateMCType(int selectedMax);

        void UpdateConsFreezeStatus(void);
        void UpdateFreezeAtom(AtomPosition pos);
        void UpdateConstrainData(double value);

        void SetAngleRange(double & angle);

        void OnBtnSym(wxCommandEvent & event);
        void OnChkGlo(wxCommandEvent & event);
        void OnChkLocl(wxCommandEvent & event);
        void OnCombTol(wxCommandEvent & event);
        void OnCombHigh(wxCommandEvent & event);
        void OnButton(wxCommandEvent & event);

private:

        wxCheckBox* m_pCkboxGlobal;
        wxCheckBox* m_pCkboxLocal;

        wxButton *m_pBtnSubmit;
        wxComboBox *m_pComboxTol;
        wxComboBox *m_pComboxHigher;

        bool m_isError;
        bool m_isSelectionChanged;
        int m_mcType;
        wxString m_mcLabel;
        float m_initBondValue;
        AtomPosition m_initPos;
        bool m_isMeasure;
        bool m_cfStatus;
        CtrlInst* m_pCtrlInst;

        string tolerance;
};

#endif /* SYMMETRIZEPNL_H_ */
