/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_HBONDS_H__
#define __BP_MOL_HBONDS_H__

#include "commons.h"
#include "chematom.h"


/*****************************************************************
 the specification of the data type of HBondsInfo:
 HydrogenBondsInfo is used to contain the information about the
 hydorgen bonds. here contains three type of data, acceptor,
 hydrogen and donator. acceptor is the atom which gives the
 hydrogen atom, and the donator is the atom which accept the
 hygrogen atom. that is:
                     X-H....Y
 X is the donator, and Y is the acceptor, H is the hydrogen.
 *****************************************************************/
/*here we use list to store the result.*/
typedef struct _hBondsInfo {
    int acceptor;
    int hydrogen;
    int donator;
    struct _hBondsInfo * next;
}HBondsInfo;


/********************************************************
 other arguments used in the H bonds determination:
  the HBONDS_LOWER_LIMIT is used to determine the
  lower limit of the hydrogen bond. it should be larger
  than (sum of covelent bond length)*HBONDS_LOWER_LIMIT
  also the hydrogen bond should be less than
  (sum of van der waals radius)*HBONDS_UPPER_LIMIT
  the calculation is for the acceptor and hydrogen
  the HBONDS_ANGLE_LIMIT defines the hydrogen bonds angle
  they should be within 180(+-)HBONDS_ANGLE_LIMIT
*********************************************************/
#define HBONDS_LOWER_LIMIT  1.7
#define HBONDS_UPPER_LIMIT  1.2
#define HBONDS_ANGLE_LIMIT  30.0


/* the return information */
typedef  enum _hBondsReturn {
  SUCCESS,
  THERE_IS_NO_HYGROGEN_BOND,
  MEMORY_ALLOCATION_FAILED,
}HBondsReturn;


HBondsReturn HBondsCal (const AtomBondArray & atomBondArray, HBondsInfo * list);


#endif
