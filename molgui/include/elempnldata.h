/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_ELEMPNLDATA_H__
#define __BP_MOL_ELEMPNLDATA_H__

#include "commons.h"
#include "ctrlinst.h"

class ElemPnlData {
public:
        static ElemPnlData* Get(void);
        static void Free(void);
        void SetElemId(ElemId elem);
        ElemId GetElemId(void) const;
        void SetBondType(BondType bondType);
        BondType GetBondType(void) const;
        void SetBondPos(BondPos bondPos);
        BondPos GetBondPos(void) const;
        void ClearElemId(void);
        bool IsValidElemId(void) const;
        bool IsValidBondType(void) const;
        bool IsValidBondPos(void) const;
        void SetFragment(wxString name);       //groups, rings, ligands, or custom
        wxString GetFragment(void) const;
        void ClearFragment(void);
        bool IsValidFragment(void);

        CtrlInst* GetPreviewCtrlInst(void) const;
        void SetPreviewCtrlInst(CtrlInst* ctrlInst);

private:
        ElemPnlData();
        ~ElemPnlData();

private:
        ElemId m_elemId;
        BondPos m_bondPos;
        BondType m_bondType;
        wxString m_fragmentName;
        CtrlInst* m_previewCtrlInst;
};

#endif
