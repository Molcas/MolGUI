/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_RENDERINGDATA_H__
#define __BP_MOL_RENDERINGDATA_H__

#include "commons.h"
#include "chematom.h"
#include "constraindata.h"
#include "selectionutil.h"
#include "matrix4x4.h"

class MolViewData;

class RenderingData {
public:
    RenderingData(MolViewData* molViewData);
        ~RenderingData();

        void Empty();
        AtomBondArray& GetAtomBondArray(void);
        AtomBondArray CloneAtomBondArray(void) const;
        AtomBond GetAtomBond(int index) const;
        int GetAtomCount(void) const;
        bool IsEmpty() const;
        int* GetAtomLabels(void);

        AtomPosition3FArray& GetGroupViewVolumeArray(void);

        //bool LoadFile(const wxString& strFileName);
        //void SaveFile(const wxString& strFileName);
        //void SaveHistroyFile(const wxString& tempFileName);
        //void LoadHistroyFile(const wxString& tempFileName);
        void LoadPreviewData(void);


        void AttachRenderingDataEx(int selHydrogenIndex, AtomBondArray &chemArray);
        void AttachRenderingDataEx(float xWorld, float yWorld, float zWorld, AtomBondArray &chemArray);
        void ReplaceRenderingData(int selectedIndex, AtomBond &chemAtom);

        bool ChangeBondType(int atomIndex[2], BondType bondType);
        bool MakeBond(int atomIndex[2]);
        bool AddBond(int atomIndex[2],BondType bondtype);
        void DeleteAtom(int atomIndex, bool tagConn);
        bool DeleteAtoms(SelectionUtil* pSelUtil);
        bool BreakBond(int atomIndex[2], bool tagConn);
        bool DefineDummy(SelectionUtil* pSelUtil);
        int GetAtomIndex(long uniqueId);

        void TagConnectionGroup(void);
        void ClearGroupCentroid(void);
        int GetConnectionGroupCount(void) const;
        AtomPosition3F GetGroupCentroid(int groupId);

        void TranslateAtomsAxis(int atomIndex, int axisIndex, double offset);
        //void TranslateConnectionGroup(int groupId, AtomPosition3F posTrans);
        //void RotateConnectionGroup(int groupId, AtomPosition3F rotAngle);
        void TranslateConnectionGroup(int groupId, AtomPosition3F posTrans);
        void RotateConnectionGroup(int groupId, AtomPosition3F center, Matrix4x4 camera);

        void Mirror(SelectionUtil* pSelUtil, bool isAll, double rotateAngle);

        static void LoadPositionFile(AtomBondArray& localAtomBondArray, int elemId, const wxString fileName);
        static void DumpRenderingData(AtomBondArray& bondArray, const wxString fileName);

        void SetAtomArray(AtomPosition3FArray &atomarray,int opmode,float enhanceswing);

        void GetMoleculeSize(AtomPosition3F& inLeftTop, AtomPosition3F& outRightBottom);

        ChemLigandArray& GetLigandArray(void);
        void ResetLigandArray(void);
        void ReCalcDummyAtomPositions(void);
        ConstrainData& GetConstrainData(void);

private:
    void ResetUniqueIds(AtomBondArray & chemArray);
    void AttachRenderingData(int selHydrogenIndex, AtomBondArray& loadedArray);

    void RemoveArrayElement(AtomBondArray& bondArray, int removeIndex, int replaceIndex);

    void TagConnectionGroup(int atomIndex, int connGroupIndex);
    void RenewGroupViewVolume(int atomIndex, int connGroupIndex);


    void GetHydrogenPosition(int atom1Index, int atom2Index, AtomPosition& pos);

private:
        MolViewData* m_pMolViewData;
    AtomBondArray m_atomBondArray;
        AtomPosition3FArray m_groupViewVolumeArray; // save the min and max x, y, z for a connection group
    // for index 0 store the current maximum label for the default Hydrogen
        // for index > 0 store the current maximum lable for atom with id index
        int m_atomLabels[ELEM_COUNT];
        int m_connGroupCount;
        bool m_isValidCentroid;

        AtomPosition3F m_posCentroid;
        ChemLigandArray m_ligandArray;
        ConstrainData m_constrainData;
};

#endif
