/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MOLFILE_H__
#define __BP_MOL_MOLFILE_H__

#include "wx.h"
#include "renderingdata.h"

extern const wxString FILE_BEGIN_COOR;
extern const wxString FILE_END_COOR;
extern const wxString FILE_BEGIN_BOND;
extern const wxString FILE_END_BOND;
extern const wxString FILE_BEGIN_TYPE;
extern const wxString FILE_END_TYPE;
extern const wxString FILE_BEGIN_CONSTRAIN;
extern const wxString FILE_END_CONSTRAIN;
extern const wxString FILE_CONSTRAIN_POSITION_FLAG;
extern const wxString FILE_CONSTRAIN_LENGTH_FLAG;
extern const wxString FILE_CONSTRAIN_ANGLE_FLAG;
extern const wxString FILE_CONSTRAIN_DIHEDRAL_FLAG;
extern const wxString FILE_BEGIN_LIGAND;
extern const wxString FILE_END_LIGAND;

class MolFile {
public:
        static void SaveFile(const wxString& fileName, RenderingData* pData,  bool useTab);
        static void SaveFile(wxTextOutputStream &tos, RenderingData* pData,  bool useTab);
        static void LoadFile(const wxString& strLoadedFileName, RenderingData* pData);
        static bool IsMolFile(const wxString& fileName);
    static void CreateTemplateFile(const wxString& fileName, AtomBondArray& bondArray);
    static void SaveAtomBondArray(const wxString& fileName, AtomBondArray &chemArray, bool useTab);

private:
        static void SaveAtomBondArray(wxTextOutputStream &tos, AtomBondArray &chemArray, bool useTab);
        static void SaveConstrain(wxTextOutputStream &tos, AtomBondArray &chemArray, ConstrainData &constrainData, bool useTab);
        static void SaveLigandInfo(wxTextOutputStream& tos,AtomBondArray& chemArray,ChemLigandArray& ligandArray, bool useTab);
};
#endif
