/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_COMMONS_H__
#define __BP_MOL_COMMONS_H__

#include "wx.h"
#include "datatype.h"

#define ELEM_COUNT 109      //109 elements

#define ELEM_PREFIX 5
#define ELEM_INFO_COUNT (ELEM_PREFIX + ELEM_COUNT + 1) // with 109 atoms and 1 blank line

#define ATOM_MAX_BOND_COUNT 6
#define DEFAULT_HYDROGEN_ID -1
#define DUMMY_ATOM_ID -5

#ifndef ELEM_IDS
#define ELEM_IDS
typedef enum _elemId {
        ELEM_H = 1, ELEM_He,
        ELEM_Li, ELEM_Be, ELEM_B, ELEM_C, ELEM_N, ELEM_O, ELEM_F, ELEM_Ne,
        ELEM_Na, ELEM_Mg, ELEM_Al, ELEM_Si, ELEM_P, ELEM_S, ELEM_Cl, ELEM_Ar,
        ELEM_K, ELEM_Ca,
    ELEM_Sc, ELEM_Ti, ELEM_V, ELEM_Cr, ELEM_Mn, ELEM_Fe, ELEM_Co, ELEM_Ni, ELEM_Cu, ELEM_Zn,
    ELEM_Ga, ELEM_Ge, ELEM_As, ELEM_Se, ELEM_Br, ELEM_Kr,
        ELEM_Rb, ELEM_Sr,
    ELEM_Y, ELEM_Zr, ELEM_Nb, ELEM_Mo, ELEM_Tc, ELEM_Ru, ELEM_Rh, ELEM_Pd, ELEM_Ag, ELEM_Cd,
    ELEM_In, ELEM_Sn, ELEM_Sb, ELEM_Te, ELEM_I, ELEM_Xe,
        ELEM_Cs, ELEM_Ba, ELEM_La,
    ELEM_Ce, ELEM_Pr, ELEM_Nd, ELEM_Pm, ELEM_Sm, ELEM_Eu, ELEM_Gd, ELEM_Tb,
    ELEM_Dy, ELEM_Ho, ELEM_Er, ELEM_Tm, ELEM_Yb, ELEM_Lu,
    ELEM_Hf, ELEM_Ta, ELEM_W, ELEM_Re, ELEM_Os, ELEM_Ir, ELEM_Pt, ELEM_Au, ELEM_Hg,
        ELEM_Tl, ELEM_Pb, ELEM_Bi, ELEM_Po, ELEM_At, ELEM_Rn, ELEM_Fr, ELEM_Ra, ELEM_Ac,
        ELEM_Th, ELEM_Pa, ELEM_U, ELEM_Np, ELEM_Pu, ELEM_Am, ELEM_Cm, ELEM_Bk, ELEM_Cf, ELEM_Es,
        ELEM_Fm, ELEM_Md, ELEM_No, ELEM_Lr,
        ELEM_Rf, ELEM_Db, ELEM_Sg, ELEM_Bh, ELEM_Hs, ELEM_Mt, ELEM_DUMMY, ELEM_NULL = 0
}ElemId;        //ELEM_DUMMY is for GUI button id.

typedef struct _elemInfo{
        int elemId;
        wxString name;
        float dispRadius;                                // display radius
        float colorR, colorG, colorB;
        float covalentRadius;                        // chemistry radius for connecting atoms
        float vanderRadius;                                // for space filling, van der Waals radius

        _elemInfo() {
                elemId = ELEM_NULL;
                dispRadius = 0.0f;
                colorR = colorG = colorB = 0.0f;
                covalentRadius = 0.0f;
                vanderRadius = 0.0f;
        }
}ElemInfo;

typedef enum _bondPos {
    BOND_POS_NULL = 0,
           BOND_POS_1 = 1, BOND_POS_2, BOND_POS_3, BOND_POS_4, BOND_POS_5,
        BOND_POS_6, BOND_POS_7, BOND_POS_8, BOND_POS_9, BOND_POS_10
}BondPos;

/**
 * BOND_TYPE_4 is bond pi
 * BOND_TYPE_5 is bond 4
 * BOND_TYPE_6 is bond dummy
 */
typedef enum _bondType {
        BOND_TYPE_0 = BP_NULL_VALUE, BOND_TYPE_1 = 1, BOND_TYPE_2, BOND_TYPE_3, BOND_TYPE_4, BOND_TYPE_5, BOND_TYPE_6
}BondType;

typedef enum _actionType {
    ACTION_VIEW,
    ACTION_ADD_FRAGMENT,
    ACTION_INSERT_FRAGMENT,
    ACTION_DELETE_FRAGMENT,
    ACTION_MAKE_BOND,
    ACTION_BREAK_BOND,
    ACTION_DESIGN_BOND,
    ACTION_LAYER,
    ACTION_MEASURE,
    ACTION_CONSTRAIN,
    ACTION_SYMMETRIZE,
        ACTION_DEFINE_DUMMY,
        ACTION_MIRROR,
    ACTION_CHANGE_BOND,
        ACTION_MM,
        //begin : hurukun : 10/11/2009
        ACTION_MOPAC,
        //end   : hurukun : 10/11/2009
}ActionType;

typedef enum _mouseButton {
    MOUSE_BUTTON_NONE = 0,
    MOUSE_BUTTON_LEFT = 1,
    MOUSE_BUTTON_RIGHT = 2
}MouseButton;

typedef enum _modelType {
        MODEL_WIRE = 0,
        MODEL_TUBE,
        MODEL_BALLSPOKE,
        MODEL_BALLWIRE,
        MODEL_SPACEFILLING,
        MODEL_RIBBON,
        MODEL_HIDE
}ModelType;

typedef enum _measureType {
        MEASURE_POSITION = 0,
    MEASURE_DISTANCE,
    MEASURE_ANGLE,
    MEASURE_DIHEDRAL
}MeasureType;

typedef enum _constrainType {
        CONSTRAIN_POSITION = 0,
    CONSTRAIN_DISTANCE,
        CONSTRAIN_ANGLE,
    CONSTRAIN_DIHEDRAL
}ConstrainType;

typedef enum _layerType {
        LAYER_LOW,
        LAYER_MEDIUM,
        LAYER_HIGH
}LayerType;

typedef enum _moveType {
        MOVE_NONE,        //static
        MOVE_ROTATE_XY,
        MOVE_ROTATE_XZ,
        MOVE_TRANS
}MoveType;

#endif

extern const wxString ELEM_NAMES[];
extern const ElemId NON_METALs[];
extern const ElemId INERT_GASSES[];
extern const ElemId MAIN_GROUP_METALS[];
extern const ElemId ALKALINE_METALS[];
extern const ElemId ALKALINE_EARTH_METALS[];
extern const ElemId TRANSITION_ELEMS[];
extern const ElemId LAAC_ELEMS[];

void InitElemsInfo(void);
void DumpElemsInfo(void);
ElemInfo* GetElemInfo(ElemId elemId);
ElemId GetElemId(wxString elemSymbol);

class AtomPosition3F {
public:
        AtomPosition3F();
    float& operator [](int i);
    AtomPosition3F& operator =(const AtomPosition3F& pos);
    void Normalize(void);
    void Scale(float scale);
        wxString ToString(void);

public:
        float x, y, z;
};
WX_DECLARE_OBJARRAY(AtomPosition3F, AtomPosition3FArray);
class AtomPosition3L {
public:
        AtomPosition3L();
    long& operator [](int i);
    AtomPosition3L& operator =(const AtomPosition3L& pos);
    AtomPosition3L& operator -(const AtomPosition3L& pos);
        wxString ToString(void);

public:
        long x, y, z;
};

class AtomPosition {
public:
        AtomPosition();
    double& operator [](int i);
    AtomPosition& operator =(const AtomPosition& pos);
        AtomPosition& operator =(const wxString &info);
        AtomPosition& operator -(const AtomPosition& pos);
        wxString ToString(void);

public:
        double x, y, z;
};

/*
 * the specification of the Moving Atoms specified by the user:
 * this structure is defined in the bp/commons.h. in the way of group
 * moving, we use this structrue array to contains the necessary
 * information of the moving atoms. the atomLabel is the
 * atom which moves, and we use the label to locate its position in
 * atombondarray. the x, y,z is its cordinate used in the calculation.
 */
typedef struct _movingAtoms {
    int atomLabel;
    double x;
    double y;
    double z;
} MovingAtoms;

wxString CheckImageFile(const wxString& fileName, bool needReport = true);
wxString CheckFontFile(const wxString& fileName);

#endif
