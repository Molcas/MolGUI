/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_SELECTION_UTIL_H__
#define __BP_SELECTION_UTIL_H__

#include "datatype.h"

class CtrlInst;

typedef enum _atomSelectType {
    ATOM_SELECT_ALL,
    ATOM_SELECT_SINGLE,
    ATOM_SELECT_CONNECTED,
    ATOM_SELECT_TWO_ATOMS
}AtomSelectType;

class SelectionUtil {
        public:
        SelectionUtil();
        void SetCtrlInst(CtrlInst* pCtrlInst);
        CtrlInst* GetCtrlInst(void) const;
        void ClearAll(void);

                bool AddSelectedIndex(int selectedAtomIndex, bool isAppend);
                bool AddSelectedIndexLimited(int selectedMax, int selectedAtomIndex, bool isSelectTwoAtoms = false);
                bool AddSelectedIndex(int selectedMin, int selectedMax, int selectedAtomIndex[2]);
                void RemoveSelected(int selectedAtomIndex);

                bool IsSelected(int selectedAtomIndex) const;
                void ClearSelection(void);
                int GetSelectedCount(void) const;
                int GetSelectedIndex(int selSequenceNum) const;
                wxString GetSelectionLabel(int selectedMax) ;
                int GetAtomCountLessThan(int atomIndex) const;
                void GetSelectedPositions(double points[][3], int length) const;

                bool AddManuSelectedIndex(int selectedAtomIndex, AtomSelectType atomSelectType);
                int GetManuSelectedCount(void) const;
                bool IsManuSelected(int atomIndex) const;
                void ClearManuSelection(void);
                int GetManuAtomCountLessThan(int atomIndex) const;
                int GetManuSelectedIndex(int selSequenceNum) const;
                void RemoveManuSelected(int atomIndex);

        private:
                wxIntArray m_intSelAry;
                wxIntArray m_intManuSelAry;
                CtrlInst* m_pCtrlInst;
};

#endif
