/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_FREETYPEFONT_H__
#define __BP_MOL_FREETYPEFONT_H__

//FreeType Headers
#include <ft2build.h>
#include <freetype.h>
#include <ftglyph.h>
#include <ftoutln.h>
#include <fttrigon.h>

//OpenGL Headers
#include "wx.h"
#ifdef _DARWIN_
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
#else
    #include <GL/gl.h>
    #include <GL/glu.h>
#endif
#include "ctrlinst.h"
#include "chematom.h"

/**
 *This holds all of the information related to any
 *freetype font that we want to create.
 */
class FreeTypeFont {
public:
        /**
         * The init function will create a font of of the height h from the file fname.
         */
        void Init(wxString fname, unsigned int h);
        //Free all the resources assosiated with the font.
        void Clean();

        /**
         * The flagship function of the library -
         * this thing will print out text at window coordinates x,y,
         * using the font ft_font.
         * The current modelview matrix will also be applied to the text.
         */
        void Print(float x, float y, const char *fmt, ...);
        void PrintString(CtrlInst* pCtrlInst, AtomPosition atomPos, float dispRadius, const char *string);
public:
        float h;                         ///< Holds the height of the font.
        GLuint * textures;         ///< Holds the texture id's
        GLuint list_base;         ///< Holds the first display list id
};




#endif
