/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MATRIX4X4_H__
#define __BP_MOL_MATRIX4X4_H__

#include "commons.h"
#ifdef _DARWIN_
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif
class Matrix4x4 {
public:
        Matrix4x4();
        void SetIdentity(void);
        GLdouble* GetMatrix(void) const;
        void Transpose(GLdouble* trans) const;
        void PreMultiply(Matrix4x4 mat);
        void Multiply(Matrix4x4 mat);
        AtomPosition3F Multiply(AtomPosition3F pos);
        AtomPosition Multiply(AtomPosition pos);
        void Translate(AtomPosition3F trans);
        void PreTranslate(AtomPosition3F trans);
        void Rotate(AtomPosition3F center, AtomPosition3F angle);
        void RotateAxis(AtomPosition3F axis, float delta);
        void Scale(float ratio);
        void Normalize(void);

        Matrix4x4& operator =(const Matrix4x4& matrix);

        //Get back transformed axes;
        AtomPosition3F GetXAxis(void);
        AtomPosition3F GetYAxis(void);
        AtomPosition3F GetZAxis(void);

        wxString ToString(void);
public:
        GLdouble m_mat[4][4];

private:
        AtomPosition3F GetAxis(int index);

};

#endif
