/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_CTRLIDS_H__
#define __BP_MOL_CTRLIDS_H__

#include "wx.h"
extern int MENU_ID_WIN_START;
extern int MENU_ID_WIN_END;
//extern int MENU_ID_BUILD_VIEW;

/*
extern int MENU_ID_FILE_RECENT_FILES;
extern int MENU_ID_FILE_RECENT_FILE0;
extern int MENU_ID_FILE_RECENT_FILE1;
extern int MENU_ID_FILE_RECENT_FILE2;
extern int MENU_ID_FILE_RECENT_FILE3;
extern int MENU_ID_FILE_RECENT_FILE4;
extern int MENU_ID_FILE_RECENT_FILE5;
extern int MENU_ID_FILE_RECENT_FILE6;
extern int MENU_ID_FILE_RECENT_FILE7;
extern int MENU_ID_FILE_RECENT_FILE8;
extern int MENU_ID_FILE_RECENT_FILE9;
extern int MENU_ID_FILE_CLEAR_RECENT_FILES;
//
extern int MENU_ID_FILE_SAVE_PROJECT_AS;
extern int MENU_ID_FILE_IMPORT;
extern int MENU_ID_FILE_EXPORT;
extern int MENU_ID_FILE_EXPORT_FILE;
extern int MENU_ID_FILE_EXPORT_IMAGE;


///////////////////////////
extern int MENU_ID_MOL_START;

extern int MENU_ID_EDIT_CLEAR;
extern int MENU_ID_EDIT_UNDO;
extern int MENU_ID_EDIT_REDO;
extern int MENU_ID_EDIT_ATOM_LIST;
extern int MENU_ID_EDIT_CONSTRAIN_LIST;
extern int MENU_ID_IMPORT_STRUCTURE;

extern int MENU_ID_EDIT_TOOLBARS;
extern int MENU_ID_EDIT_TOOLBARS_FILE;
extern int MENU_ID_EDIT_TOOLBARS_BUILD;
extern int MENU_ID_EDIT_TOOLBARS_GEOMETRY;
extern int MENU_ID_EDIT_TOOLBARS_CALC;

extern int MENU_ID_EDIT_VIEWSETTINGS;
extern int MENU_ID_MODEL_WIRE;
extern int MENU_ID_MODEL_TUBE;
extern int MENU_ID_MODEL_BALLSPOKE;
extern int MENU_ID_MODEL_BALLWIRE;
extern int MENU_ID_MODEL_SPACEFILLING;
extern int MENU_ID_MODEL_RIBBON;

extern int MENU_ID_MODEL_SHOW_HYDROGENS;
extern int MENU_ID_MODEL_SHOW_GLOBAL_LABELS;
extern int MENU_ID_MODEL_SHOW_ATOM_LABELS;
extern int MENU_ID_MODEL_SHOW_HYDROGEN_BONDS;
extern int MENU_ID_MODEL_SHOW_AXES;
extern int MENU_ID_MODEL_SHOW_LAYERS;
extern int MENU_ID_MODEL_SHOW_LAYERS_SHOW;
extern int MENU_ID_MODEL_SHOW_LAYERS_HIGH;
extern int MENU_ID_MODEL_SHOW_LAYERS_MEDIUM;
extern int MENU_ID_MODEL_SHOW_LAYERS_LOW;
//

extern int MENU_ID_BUILD_ADD_FRAGMENT;
extern int MENU_ID_BUILD_INSERT_FRAGMENT;
extern int MENU_ID_BUILD_DELETE_FRAGMENT;
extern int MENU_ID_BUILD_MAKE_BOND;
extern int MENU_ID_BUILD_BREAK_BOND;
extern int MENU_ID_BUILD_SELECT_LAYER;

extern int MENU_ID_BUILD_CREATE_TEMPLATE;
extern int MENU_ID_BUILD_DELETE_TEMPLATE;

extern int MENU_ID_BUILD_RESET;
//
//extern int MENU_ID_GEOMETRY_MEASURE;
extern int MENU_ID_GEOMETRY_CONSTRAIN;
extern int MENU_ID_GEOMETRY_SYMMETRIZE;
extern int MENU_ID_GEOMETRY_DEFINE_DUMMY;
extern int MENU_ID_GEOMETRY_MIRROR;
//
//extern int MENU_ID_JOB_NEW;
//extern int MENU_ID_JOB_SETTING;
//extern int MENU_ID_JOB_SUBMIT;
//extern int MENU_ID_JOB_MONITOR;
//
extern int MENU_ID_CALC_JOB_SIMUCALC;
extern int MENU_ID_CALC_JOB_MING;
extern int MENU_ID_CALC_JOB_GAUSSIAN;
extern int MENU_ID_CALC_SUBMIT;

extern int MENU_ID_CALC_SYMMETRIZE;

extern int MENU_ID_CALC_MM_OPTIMIZATION;
//begin : hurukun : 12/11/2009
extern int MENU_ID_CALC_MOPAC_OPTIMIZATION;
//end   : hurukun : 12/11/2009
extern int MENU_ID_CALC_SYMMETRY;
//
extern int MENU_ID_DISPLAY_NORMAL;
extern int MENU_ID_DISPLAY_CALCULATED;
extern int MENU_ID_DISPLAY_OUTPUT;
extern int MENU_ID_DISPLAY_SUMMARY;
extern int MENU_ID_DISPLAY_SURFACES_MO;
extern int MENU_ID_DISPLAY_VIBRATION;
extern int MENU_ID_DISPLAY_JOB_MONITOR;

extern int POPUPMENU_ID_MODEL_SHOW_GLOBAL_LABELS;
extern int POPUPMENU_ID_MODEL_SHOW_ATOM_LABELS;

extern int MENU_ID_MOL_END;
extern int MENU_ID_WORKER_EVENT;
///////////////////////////

extern int TOOL_ID_FILE_NEW;
extern int TOOL_ID_FILE_OPEN;
extern int TOOL_ID_FILE_SAVE;
extern int TOOL_ID_FILE_SAVEALL;
extern int TOOL_ID_FILE_SAVE_AS;
extern int TOOL_ID_FILE_CLOSE;
extern int TOOL_ID_FILE_EXPORT;
extern int TOOL_ID_FILE_EXIT;

///////////////////////////
extern int TOOL_ID_MOL_START;

extern int TOOL_ID_BUILD_VIEW;
extern int TOOL_ID_BUILD_ADD_FRAGMENT;
extern int TOOL_ID_BUILD_INSERT_FRAGMENT;
extern int TOOL_ID_BUILD_DELETE_FRAGMENT;
extern int TOOL_ID_BUILD_MAKE_BOND;
extern int TOOL_ID_BUILD_BREAK_BOND;
extern int TOOL_ID_BUILD_SELECT_LAYER;

extern int TOOL_ID_GEOMETRY_MEASURE;
extern int TOOL_ID_GEOMETRY_CONSTRAIN;
extern int TOOL_ID_GEOMETRY_SYMMETRIZE;
extern int TOOL_ID_GEOMETRY_DEFINE_DUMMY;
extern int TOOL_ID_GEOMETRY_MIRROR;
//
extern int TOOL_ID_CALC_JOB_SIMUCALC;
extern int TOOL_ID_CALC_JOB_GAUSSIAN;
extern int TOOL_ID_CALC_JOB_MING;

extern int TOOL_ID_CALC_MM_OPTIMIZATION;
//begin : hurukun : 06/11/2009
extern int TOOL_ID_CALC_MOPAC_OPTIMIZATION;
//end   : hurukun : 06/11/2009
extern int TOOL_ID_CALC_SYMMETRY;

extern int TOOL_ID_MOL_END;                        //last id for tools
///////////////////////////
*/
extern int TIMER_ID;

#endif
