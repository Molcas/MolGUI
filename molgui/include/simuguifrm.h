/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

//---------------------------------------------------------------------------
//
// Name:        simuguigrm.h
// Author:      xychen
// Created:     2008-12-19 21:36:26
// Description: SimuGUIFrm class declaration
//
//---------------------------------------------------------------------------

#ifndef __BP_MOL_SIMUGUIFRM_H__
#define __BP_MOL_SIMUGUIFRM_H__

#include "wx.h"

#include <wx/aui/aui.h>
#include "molview.h"

//begin : hurukun 05/11/2009
#include <wx/html/helpctrl.h>
//end   : hurukun 05/11/2009
//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
////Header Include End

////Dialog Style Start
#define SimuGuiFrm_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxRESIZE_BORDER | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX
//#define SimuGuiFrm_STYLE wxCAPTION | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class SimuGuiFrm : public wxFrame
{
        private:
                DECLARE_EVENT_TABLE();

        public:
                SimuGuiFrm(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("SimuGui"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = SimuGuiFrm_STYLE);
                virtual ~SimuGuiFrm();
                void InitFrame(void);
                void AfterCreation(void);
                MolViewData* GetMolData(void) const;
                void EnableMenuToolCtrlInst(void);
                void DoCheckMenuWindow(int winId);

        protected:
                void InitWindowState(void);
                void SaveWindowState(void);

                void UpdateFrameTitle(const wxString& fileName);
                bool CreateCtrlInst(wxString fileName);
                void EnableMenuTool(bool enabled);

                /**
                 * Set the fileName as the most recent files.
                 * @param fileName wxEmptyString means to load history file, or when clearing history
                 * @param icClearHis True to clear history.
                 * @param isRemove True to remove the fileName from the history.
                 */
                void UpdateRecentFiles(const wxString& fileName, bool isClearHis = false, bool isRemove = false);

        private:
                //Do not add custom control declarations between
                //GUI Control Declaration Start and GUI Control Declaration End.
                //wxDev-C++ will remove them. Add custom code after the block.
                ////GUI Control Declaration Start
                ////GUI Control Declaration End
                void OnClose(wxCloseEvent& event);
                void OnMenuToolCommand(wxCommandEvent& event);
                //void OnToolCommand(wxCommandEvent& event);
                void OnFileNew(wxCommandEvent& event);
                void OnFileOpen(wxCommandEvent& event);
                bool DoFileOpen(wxString& fileName);
                void OnFileClose(wxCommandEvent& event);
                void OnFileExit(wxCommandEvent& event);
                bool DoFileCloseAll(void);
                bool DoFileCloseWithMenuUpdate(void);
                void OnFileSave(wxCommandEvent& event);
                void DoFileSave(bool isSaveAs);
                void OnFileSaveAs(wxCommandEvent& event);
                void OnFileRecentFiles(wxCommandEvent& event);
                void OnFileClearRecentFiles(wxCommandEvent& event);
                void OnMolMenuCommand(wxCommandEvent& event);
                void OnMolToolCommand(wxCommandEvent& event);
                void OnMenuWinNavigation(wxCommandEvent& event);
                void DoMenuWinNavigationCommon(int eventId);
                void OnMenuWindows(wxCommandEvent& event);
                //void OnStatusShowInfo(wxCommandEvent& event);
                //-----------------------
                void OnMenuEditUndo(wxCommandEvent &event);
                void OnMenuEditRedo(wxCommandEvent &event);
                //--------------------------------
                //begin : hurukun 05/11/2009
                void OnMenuHelp(wxCommandEvent &event);
                void OnMenuAbout(wxCommandEvent &event);
                //end   : hurukun 05/11/2009
        void CreateGUIControls(void);
                void CreateMenu(void);
                void CreateGUI(void);

        private:
                //Note: if you receive any error with these enum IDs, then you need to
                //change your old form code that are based on the #define control IDs.
                //#defines may replace a numeric value for the enum names.
                //Try copy and pasting the below block in your old form header files.
                enum
                {
                        ////GUI Enum Control ID Start
                        ////GUI Enum Control ID End
                        ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
                };

                //begin : hurukun 05/11/2009
                wxHtmlHelpController* m_helpCtrl;
                //end   : hurukun 05/11/2009
                wxAuiManager m_auiManager;
                wxMenu *m_pRecentFilesMenu;
                MolView* m_pMolView;
};

#endif
