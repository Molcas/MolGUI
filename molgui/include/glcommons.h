/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_GLCOMMONS_H__
#define __BP_MOL_GLCOMMONS_H__

#define KEY_DELTA_TRNASLATE 0.05f
#define KEY_DELTA_ROTATE 0.1f
#define KEY_DELTA_SCALE 0.05f

#define MOUSE_PREC_TRANSLATE 50.0f
#define MOUSE_PREC_ROTATE 200.0f

#define SELECT_BUF_SIZE        512
#define SELECT_PREC 5.0f

#endif
