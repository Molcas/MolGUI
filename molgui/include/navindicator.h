/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_NAVINDICATOR_H__
#define __BP_MOL_NAVINDICATOR_H__

#include "commons.h"
#include "ctrlinst.h"

/**
 * Draw nagivation arrows etc. inside the molglcanvas scene
 */
class NavIndicator {
public:
        NavIndicator(CtrlInst* pCtrlInst);
        void Painting(void);

private:
        void Start(void);
        void End(void);

        void DrawRotationRibbonX(void);
        void DrawRotationRibbonY(void);
        void DrawRotationRibbonZ(void);

private:
        float m_startAngle;
        float m_endAngle;
        float m_angleStep;
        float m_ribbonRadius;
        float m_ribbonWidth;

        CtrlInst* m_pCtrlInst;
};

#endif
