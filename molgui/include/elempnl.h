/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_ELEMPNL_H__
#define __BP_MOL_ELEMPNL_H__

#include "wx.h"

#include <wx/tglbtn.h>
#include <wx/bookctrl.h>
#include <wx/colour.h>
#include <wx/scrolwin.h>

#include "commons.h"
#include "togglebitmapbutton.h"
#include "molglcanvas.h"

#define ELEM_TOGGLE_BTN_COUNT (ELEM_COUNT + 1) // dummy atom
#define BOND_POS_COUNT 10
#define BOND_TYPE_COUNT 5        // does not count bond dummy
#define FRAGMENT_CATEGORY_COUNT 4


class ElemPnl : public wxWindow
{
public:
    ElemPnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = wxT("ElemPnl"));
    virtual ~ElemPnl();

    void InitPanel(wxSize& szParentClient);
    void SetDefaultValues(void);
    void RefreshGraphics(void);

    bool AddCustomTemplate(wxString name, AtomBondArray& atomBondArray);
    wxString DeleteSelectedCustomTemplate(bool isEnquiry);

private:
    DECLARE_EVENT_TABLE();
    wxScrolledWindow* CreateElementPnl(wxBookCtrlBase* parent);//wangyuanlong     2010/11/23
    void OnToggleButton(wxCommandEvent& event);
    void OnListBox(wxCommandEvent& event);
    void OnNotebookPageChanged(wxNotebookEvent& event);
    void OnGLCanvasUpdateUI(wxUpdateUIEvent& event);
    void DoFragmentCommon(int listBoxIndex);
    void CreateCategoryElems(wxScrolledWindow* parent, wxString category, wxColour color, const ElemId* elems);//wangyuanlong     2010/11/23
    void CreateDummyElem(wxScrolledWindow* parent);//wangyuanlong     2010/11/23
    void InitDefaultAtomPosition(void);
    void UnselectedOtherElemButtons(int btnId);
    void UnselectedOtherBondPositionButtons(int btnId);
    wxString CatenateDataFile(int listBoxIndex, wxString fileName);
    void OnPaint(wxPaintEvent& event);//wangyuanlong     2010/11/5
    void OnSize(wxSizeEvent& event);

private:
    enum
    {
        CTRL_ID_TGL_ELEM_START = 1001,
        CTRL_ID_TGL_ELEM_END = 1130,
        CTRL_ID_TGL_BOND_POS_START = 1201,
        CTRL_ID_TGL_BOND_POS_END = 1220,
        CTRL_ID_TGL_BOND_TYPE_START = 1221,
        CTRL_ID_TGL_BOND_TYPE_END = 1230,

        CTRL_ID_LB_GROUPS,
        CTRL_ID_LB_RINGS,
        CTRL_ID_LB_LIGANDS,
        CTRL_ID_LB_CUSTOM,
        CTRL_ID_CVS_GL
    };
    ToggleBitmapButton* m_pTglElems[ELEM_TOGGLE_BTN_COUNT];
    int m_defaultAtomBondPoss[ELEM_TOGGLE_BTN_COUNT];
    ToggleBitmapButton* m_pTglBmpBondPoss[BOND_POS_COUNT];
    ToggleBitmapButton* m_pTglBmpBondTypes[BOND_TYPE_COUNT];
    wxScrolledWindow*    m_pScrolWin;
    wxListBox* m_pLbFragments[FRAGMENT_CATEGORY_COUNT];
    wxBookCtrlBase* m_pBookElements;
    //        bool    tog_btn_clk;
    MolGLCanvas* m_pGLPreview;
    MolViewData m_molData;
    bool m_PageChanged;


};

#endif
