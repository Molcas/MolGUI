/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_XYZFILE_H__
#define __BP_MOL_XYZFILE_H__

#include "wx.h"
#include "chematom.h"
#include "zmatrix.h"

const wxString FILE_QCHEM_BEGIN_COOR = wxT("$molecule");
const wxString FILE_QCHEM_END_COOR = wxT("$end");
const wxString FILE_QCHEM_EMPTYLINE = wxT(" ");
const wxString FILE_QCHEM_EMPTYLINE2 = wxT("");


class XYZFile {
public:
        static bool ChangePDBFile(const wxString& pdbFile,const wxString& nh_xyzFile,const wxString& h_xyzFile);
        static bool IsXYZFile(const wxString& fileName);
        static bool IsQChemXYZFile(const wxString& fileName);
        static bool IsQChemInnerFile(const wxString& fileName);
    static void LoadFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels);
    static void SaveFile(const wxString& fileName, AtomBondArray& bondArray, bool useTab);
        static void LoadQchemFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels);
    static void SaveCoord(const wxString& fileName, AtomBondArray& bondArray, bool useTab);
    static bool SearchBondList(int atom1Id, int atom2Id, const int* bondList);
    static BondType CalculateBondType(int atom1Index, int atom2Index, AtomBondArray& atomBondArray);
        static void LoadFromDen(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels);
        static void LoadQChemTestFile(const wxString& strLoadedFileName, AtomBondArray& appendAtomBondArray, int* atomLabels); //SONG add it for workshop2009

};

#endif
