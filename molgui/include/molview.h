/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MOLVIEW_H__
#define __BP_MOL_MOLVIEW_H__

#include "abstractview.h"
#include "molglcanvas.h"
#include "molviewdata.h"

class MolView : public AbstractView {
public:
                MolView(wxWindow* parent, wxEvtHandler* evtHandler, ViewType viewType, long style = wxTAB_TRAVERSAL);
                virtual ~MolView();
public:
                virtual bool Close(bool isAskForSave=true,bool default_save=false);
                virtual void InitView(void);

                virtual bool Save();
                virtual bool SaveAs(wxString nFileName);

                virtual void EnableMenuTool(bool isEnable);
                virtual void UpdateMenuTool(void);

                void ImportFile(wxString fileName);

                MolViewData* GetData(void) const;
                MolGLCanvas* GetGLCanvas(void) const;
                void RefreshGraphics(void);

                void SetMenuToolCtrlInst(void);
                void EnableMenuToolCtrlInst(void);
                wxMenu* GetPopupMenu(void);
                void InitSurfaceOrVib(void);
        int GetCurrentMO(void);
        void SetCurrentMO(int curmo);
        private:
                DECLARE_EVENT_TABLE();

                void UpdateToolBar(bool isShown);
                void OnMolViewUpdateUI(wxUpdateUIEvent& event);

                int NeedSaveRenderingData(CtrlInst* pCtrlInst);
                bool SaveRenderingData(wxString fileName, CtrlInst* pCtrlInst);
                bool DoCloseCtrlInst(wxString fileName, CtrlInst* pCtrlInst,bool isAskForSave=true,bool default_save=false);

                int GetModeTypeMenuId(ModelType modelType);
                int GetBuildGeometryMenuId(ActionType actionType);

        private:
                MolGLCanvas* m_pGLCanvas;
                MolViewData* m_pMolViewData;
                wxMenu *m_pPopupMenu;
                int m_curmo;
};

#endif
