/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef        _MOLPAC_PARA_SETTING_H_
#define _MOLPAC_PARA_SETTING_H_
#include <wx/window.h>
#include <wx/textctrl.h>
#include <wx/combobox.h>

class CMopacParaDlg : public wxDialog{
private:
          wxComboBox                *m_pcbMethod;
        wxComboBox                *m_pcbMulti;
        wxTextCtrl                *m_ptcCharge;
        wxTextCtrl                *m_ptcOptions;
        wxArrayString         m_strMethod;
        wxString                m_strMethodVal;
    wxArrayString        m_strMulti;
        wxString                m_strMultiVal;
        long                        m_nCharge;
        wxString                m_strOptionsVal;
public:
        CMopacParaDlg(wxWindow * parent,wxWindowID id = wxID_ANY,
                      const wxString& caption = wxT("Mopac Parameters"),  
                      const wxPoint& pos = wxDefaultPosition,
                      const wxSize& size = wxDefaultSize,long  style = wxCAPTION|wxSYSTEM_MENU|wxCLOSE_BOX,
                      const wxString& name=wxT("Mopac Parameters"));
        ~CMopacParaDlg();
private:
        void InitParameter();
        void InitPanel();
public:
        wxString GetMethod()const{return m_strMethodVal;}
        wxString GetMultiplicity()const {return m_strMultiVal;}
        long         GetCharge()const{return m_nCharge;}
    wxString GetOptions()const{return m_strOptionsVal;}
private:
        void        OnOK(wxCommandEvent& event);
        DECLARE_EVENT_TABLE();
};

#endif
