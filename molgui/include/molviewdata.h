/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MOLVIEWDATA_H__
#define __BP_MOL_MOLVIEWDATA_H__

#include "commons.h"
#include "ctrlinst.h"

class MolViewData {
public:
        MolViewData();
        ~MolViewData();

        ActionType GetActionType(void) const;
        void SetActionType(ActionType actType);

        int GetCtrlInstCount(void) const;
        bool IsAllowAddCtrlInst(void);
        void AddCtrlInst(CtrlInst* pCtrlInst);
        void RemoveCtrlInst(int index);
        CtrlInst* GetCtrlInst(int index);
        CtrlInst* GetCtrlInstByID(int viewId);
        CtrlInst* GetActiveCtrlInst(void);
        void SetActiveCtrlInstIndex(int ctrlIndex);
        int GetActiveCtrlInstIndex(void) const;

        int GetUntitledWinCount(void) const;
        void IncreaseUntitledWinCount(void);
        void ResetUntitledWinCount(void);

        bool IsFileLoaded(const wxString& strFileName, bool exceptActive);

private:
        ActionType m_actionType;
        MeasureType m_measureType;
        CtrlInstArray m_ctrlInstArray;
        int m_activeCtrlInstIndex;
        int m_intUntitledWinCount;
};

wxString Index2DisplayLabel(CtrlInst* pCtrlInst, int atomIndex);
wxString Index2DisplayLabel(const ChemAtom* const atom, int atomIndex, bool isGlobal);

#endif
