/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MIRRORPNL_H__
#define __BP_MOL_MIRRORPNL_H__

#include<wx/spinctrl.h>

#include "wx.h"
#include "ctrlinst.h"

class MirrorPnl : public wxPanel {

private:
        DECLARE_EVENT_TABLE();

public:
    MirrorPnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30), long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panelmirror"));
    void InitPanel(void);

    void SetCtrlInst(CtrlInst* pCtrlInst);
    void ClearSelection(void);
   void AddSelectedIndex(int selectedAtomIndex);

private:
    void OnTextEnter(wxCommandEvent& event);
    void OnSliderScroll(wxScrollEvent& event);
        void OnBtnMirror(wxCommandEvent& event);
        //void OnBtnClear(wxCommandEvent& event);
        void OnCkbSelect(wxCommandEvent& event);
        void OnRbSelectType(wxCommandEvent& event);

        void DoClear(void);
        void DoCkbSelect(bool isSelected);

        void ShowAnglePanel(bool isAngle);
        void SetDefaultValues(void);

private:
        wxTextCtrl* m_pTcValue;
        wxSlider* m_pSlider;
    wxBoxSizer* m_pBsAngle;
    wxBoxSizer* m_pBSelect;
    wxButton* m_pBtnMirror;
    wxCheckBox* m_pCkbSelect;
    wxRadioButton* m_pRbTypeSingle;
    wxRadioButton* m_pRbTypeConnected;
    //wxButton* m_pBtnClear;
    AtomSelectType m_atomSelectType;

        CtrlInst* m_pCtrlInst;
};

#endif
