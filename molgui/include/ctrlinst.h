/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_CTRLINST_H__
#define __BP_MOL_CTRLINST_H__

#include "wx.h"

#include <wx/ffile.h>

#include "commons.h"
#include "renderingdata.h"
#include "selectionutil.h"
#include "matrix4x4.h"


class MolViewData;
class AtomListFrm;

class AbstractSurface {
        public:
                AbstractSurface();
                virtual ~AbstractSurface() {}
                virtual void Init(const wxString fileName) = 0;
                virtual void RenderSurface(void) = 0;
                void SetRendering(bool isRendering);
                bool IsRendering(void) const;

                int GetRenderingStyle(void) const;
                void SetRenderingStyle(int style);

                virtual void SetCurmo(int mo) = 0;
                virtual int GetCurmo(void) const = 0;

                virtual void LoadGrid(int mo, wxFFile * outf) = 0;

        protected:
                bool m_isRendering;
                int m_renderingStyle;
};
/**
 * CtrlInst represents an opened file.
 */
class CtrlInst {
public:
        Matrix4x4 camera;                                                //GL modelview matrix
        int mouseClickedAtomIndex;
        AtomPosition3L posCoord;                                // mouse pressed position
        AtomPosition3L posCursor;                                // current mouse position (used when mouse is moving)
        AtomPosition3F navIndicatorAngle;                // GL navigation indicator angle

        MouseButton mouseButton;                                //currently clicked button
        ModelType modelType;                                        //Displayed model type
        MoveType moveType;


        bool isShowHydrogens;
        bool isShowGlobalLabels;
        bool isShowAtomLabels;
        bool isShowLayers;
        bool isShowLayerHigh;
        bool isShowLayerMedium;
        bool isShowLayerLow;
        bool isShowHydrogenBonds;
        bool isShowSurface;
        bool isShowAxes;
        bool isShowSymmetry;

public:
        CtrlInst(MolViewData* pViewData,int id);
        ~CtrlInst();
public:
    int GetId()const{return m_id;}
        RenderingData* GetRenderingData(void);
    bool AddSelectedAtom(int selectedAtomIndex, bool isAppend);
    void ClearSelectedAtoms(void);
    void Empty(void);
        bool IsDirty(void) const;
        void SetDirty(bool isDirty);
        bool IsPreviewCtrlInst(void) const;
        void SetPreviewCtrlInst(bool isPreview);
        void ResetGLPositions(void);
        SelectionUtil* GetSelectionUtil(void);
        bool GetSelectedPosition(int atomIndex, AtomPosition& atomPos);
        AtomPosition3F GetRotationCenter(void);
        AtomPosition3F GetRotationCenter(float& centerRadius);

        bool CanUndo(void);
    bool CanRedo(void);
        //bool RedoMolecule(void);
        //bool UndoMolecule(void);
        void UndoMolecule(void);
        void RedoMolecule(void);
        void SaveHistroyFile(void);

        bool MakeBond(int selectedAtomIndex);

        wxString GetFileName(void) const;

        bool LoadFile(wxString& strFileName);
        void SaveFile(const wxString& strFileName, bool isExport);
        void LoadOptFile(wxString& strFileName);
        bool GetReturn(wxString& strFileName);

        int GetOldAngleValue(int atomStill, int atomAxis, int atomMove, double& value);
        void SetOldAngleValue(int atomStill, int atomAxis, int atomMove, double value);

    void LoadPreviewData(void);
    AtomBondArray GetPreviewData(void);

        AbstractSurface* GetSurface(void) const;
        void SetSurface(AbstractSurface* surface);

        AtomListFrm* GetAtomListFrm(bool createIt);
        void FreeAtomListFrm(void);

        wxString GetFullPathName(void);  //SONG add it for workshop2009

        void DoSymme(wxString file);                                //mayingbo

private:
    int                       m_id;//same to the viewId of the related view.
    RenderingData*        m_pRenderingData;
        wxArrayString                m_hisFileArray;
        int                                                m_currentHisFileIndex;
        wxString                                m_strFileName;
        bool                                            m_isDirty;
        bool                                            m_isPreview;
    SelectionUtil                        m_selUtil;
    AtomAngleArray                        m_atomAngleArray;                // save angle value before change it.
    AbstractSurface*                m_surface;
    AtomListFrm*                         m_pAtomListFrm;
        wxString m_strProjectPath;  //SONG add it for workshop2009
        wxString m_strLoadedFileName;  //SONG add it for workshop2009

};

WX_DEFINE_ARRAY(CtrlInst*, CtrlInstArray);
//WX_DECLARE_OBJARRAY(CtrlInst, CtrlInstArray);

#endif
