/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#ifndef __BP_MOL_MOLVIEWEVENTHANDLER_H__
#define __BP_MOL_MOLVIEWEVENTHANDLER_H__

#include "wx.h"

#include "molview.h"

class MolViewEventHandler : public wxEvtHandler {
public:
        void DoCalcSymmetry(void);
        void DoCalcMMOptimization(void);
        //begin : hurukun : 05/11/2009
        void DoCalcMopacOptimization(void);
        //end   : hurukun : 16/11/2009
        static MolViewEventHandler* Get(void);
        static void Free(void);

        void SetMolView(MolView* pMolView);
        MolView* GetMolView(void) const;

        wxMenu* GetWinMenu(void) const;
        void SetWinMenu(wxMenu* pWinMenu);

        void SetActionType(ActionType actionType, int eventId);
        ActionType UpdateMenuToolExclusive(bool isUIEvent, int id, bool isMenu);

        void ProcessMenuEvent(wxCommandEvent& event);
        void ProcessToolEvent(wxCommandEvent& event);

        void DoMoleculeBuildCommon(int eventId, bool isUIEvent, bool isMenu);
        void OnUpdateProgress(int status);
        void DoUpdateMM(void);
        // void ChangeBC(wxCommandEvent& event);
        
private:
        MolViewEventHandler();
        ~MolViewEventHandler();

        DECLARE_EVENT_TABLE();

        void OnEditClear(wxCommandEvent& event);
        void OnEditAtomList(wxCommandEvent& event);
        //---------------------GRP-------------------
        // void OnChangeBackgroundColor(wxCommandEvent& event);
        // wxColour colour;
        // wxButton *bgBtn;
        // wxWindow *bgShow;
        //---------------------xia-----------------
        void OnEditUndo(wxCommandEvent& event);
        void OnEditRedo(wxCommandEvent& event);
        //-----------------------------------------
        void OnMenuMoleculeBuild(wxCommandEvent& event);
        void OnToolMoleculeBuild(wxCommandEvent& event);

        void OnMenuModelType(wxCommandEvent& event);
        void OnMenuModelShows(wxCommandEvent& event);
        void OnMenuModelShowLayers(wxCommandEvent& event);

        void OnCreateTemplate(wxCommandEvent& event);
        void OnDeleteTemplate(wxCommandEvent& event);

        void OnMenuGeometry(wxCommandEvent& event);
        void OnToolGeometry(wxCommandEvent& event);
        void DoGeometryCommon(int eventId, bool isUIEvent, bool isMenu);

        void OnPopupMenuModelShows(wxCommandEvent& event);
        void OnPopupReset(wxCommandEvent& event);

        void OnImportStructure(wxCommandEvent& event);
        void OnCalcSymmetry(wxCommandEvent& event);
        void OnCalcMMOptimization(wxCommandEvent& event);
        //begin : hurukun : 06/11/2009
        void OnCalcMopacOptimization(wxCommandEvent& event);
        //end   : hurukun : 06/11/2009

private:
        MolView*        m_pMolView;
        wxMenu *        m_pWinMenu;
        //begin : hurukun : 13/11/2009
        wxString        m_strModualName;
        //end   : hurukun : 13/11/2009
};

// };
class ChangeColor: public wxFrame{
        public:

        void OnChangeBackgroundColor(wxCommandEvent &event);
        wxColour colour;
};

#endif
