/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MOLRENDER_H__
#define __BP_MOL_MOLRENDER_H__

#include "wx.h"
#ifdef _DARWIN_
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif
#include "chematom.h"
#include "ctrlinst.h"
#include "molglcanvas.h"
//#include "freetypefont.h"
//#include "uidata.h"

class MolRender {
public:
 //        static void InitLabelFont(void);
 //   static void FreeLabelFont(void);

        static void DrawAtom(CtrlInst* pCtrlInst, int atomIndex, ActionType actionType, GLenum mode,MolGLCanvas * pMolGLCanvas);
        static void PrintLabelCenter(CtrlInst* pCtrlInst, MolGLCanvas* pMolGLCanvas, wxString text);
        static void DrawBond(AtomBondArray& bondArray, int atomIndex, int bondIndex, bool colorWithDefaultHydrogen, bool isMoving);
        static void DrawWire(const AtomBond* const atomBond1, const AtomBond* const atomBond2);
        static void DrawTube(const AtomBond* const atomBond1, const AtomBond* const atomBond2, bool isTube, bool isMoving);

        static void DrawViewVolume(AtomPosition3F minPos, AtomPosition3F maxPos);
        static void DrawAxes(CtrlInst* pCtrlInst,MolGLCanvas* pMolGLCanvas);
        static void DrawMirrorPlain(CtrlInst* pCtrlInst);

        static void GetWorldCoord(double& xWorld, double& yWorld, double& zWorld);
        static void GetWorldCoord2(double& xWorld, double& yWorld, double& zWorld);
        static void GetWorldCoord3(double& xWorld, double& yWorld, double& zWorld);
        static void GetWindowCoord(double &xCoord, double &yCoord, double &zCoord);
        static AtomPosition Project(AtomPosition atomPos);
        static AtomPosition UnProject(AtomPosition3L winCoord);
private:
    static void DrawCylinders(BondType bondType, GLdouble distance, bool isMoving);
    static void DrawBond(AtomBondArray& bondArray, int atom1Index, int atom2Index, int atom3Index, const BondType bondType, bool isMoving);
    static void PrintLabel(CtrlInst* pCtrlInst, int atomIndex, ChemAtom* atom,MolGLCanvas* pMolGLCanvas);

};
#endif
