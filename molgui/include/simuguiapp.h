/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

//---------------------------------------------------------------------------
//
// Name:        simuguiapp.h
// Author:      xychen
// Created:     2008-12-19 21:36:26
// Description:
//
//---------------------------------------------------------------------------

#ifndef __BP_MOL_SIMUGUIAPP_H__
#define __BP_MOL_SIMUGUIAPP_H__

#include "wx.h"
#include "simuguifrm.h"

class SimuGuiApp : public wxApp
{
        public:
                bool OnInit();
                int OnExit();

                SimuGuiFrm* GetAppFrame(void);

        private:
        SimuGuiFrm* m_pAppFrame;
};

DECLARE_APP(SimuGuiApp);

#endif
