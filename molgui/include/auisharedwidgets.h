/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_AUISHAREDWIDGETS_H__
#define __BP_MOL_AUISHAREDWIDGETS_H__

#define TOOLBARS_COUNT 3

#include "wx.h"

#include <wx/aui/aui.h>

#include "abstractview.h"
#include "datatype.h"
#include "elempnl.h"
#include "measurepnl.h"
#include "symmetrizepnl.h"
#include "deletepnl.h"
#include "mirrorpnl.h"
#include "progressbarpnl.h"
#include "designbondpnl.h"

extern const wxString STR_TB_FILE;
extern const wxString STR_TB_BUILD;
extern const wxString STR_TB_GEOMETRY;
extern const wxString STR_TB_CALCULATE;

extern const wxString STR_PNL_FRAGMENTS;
extern const wxString STR_PNL_GLCANVASMAIN;
extern const wxString STR_PNL_DELETE;
extern const wxString STR_PNL_MEASURE;
extern const wxString STR_PNL_SYMMETRIZE;
extern const wxString STR_PNL_MIRROR;
extern const wxString STR_PNL_PROGRESSBAR;
extern const wxString STR_PNL_DESIGNBOND;
class AuiSharedWidgets {
public:
        static AuiSharedWidgets* Get(void);
        static void Free(void);
        void SetAuiManager(wxAuiManager* pAuiMgr);
        void BuildMenus(wxMenuBar* pMenuBar);
        void BuildWidgets(wxWindow* parent);
        void BuildToolBars(wxWindow* parent, bool isStandAlone);

        void ShowAuiWidgets(const wxString& name, bool isShown);
        bool IsShown(wxString pnlName) const;
        void ShowExclusiveAuiWidget(const wxString& name);


        void EnableMenuTool(bool isEnable);
        void EnableTool(int toolId, bool enabled);
        void ToggleTool(int toolId, bool toggle);
        bool GetToolState(int toolId);
        wxToolBar* GetToolBar(const unsigned int id);
        void ShowExclusivePanel(const wxString& pnlName);

        ElemPnl* GetElemPnl(void) const;
        MeasurePnl* GetMeasurePnl(void) const;
        SymmetrizePnl* GetSymmetrizePnl(void) const;
        MirrorPnl* GetMirrorPnl(void) const;
        DeletePnl* GetDeletePnl(void) const;
        ProgressBarPnl* GetProgressBarPnl(void) const;
        DesignBondPnl* GetDesignBondPnl(void) const;
        wxWindow* GetWindow(const wxString name) const;
        void AddWindow(const wxString name, wxString caption, wxWindow* pWin, int minSizeX, int minSizeY);

        wxAuiManager* GetAuiManager(void) const;

private:
        AuiSharedWidgets();
        ~AuiSharedWidgets();


private:
        wxAuiManager* m_pAuiMgr;
        wxMenuBar* m_pMenuBar;
        wxToolBar* m_pToolbars[TOOLBARS_COUNT];

        ElemPnl* m_pPnlElem;
        MeasurePnl* m_pPnlMeasure;
        SymmetrizePnl* m_pPnlSymmetrize;
        MirrorPnl* m_pPnlMirror;
        DeletePnl* m_pPnlDelete;
        ProgressBarPnl* m_pPnlProgressBar;
        DesignBondPnl* m_pPnlDesignBond;
        WindowPtrHash m_winptrHash;
};

#endif
