/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
* Copyright (C) 2019,2020, Gerardo Raggi                               *
***********************************************************************/

#ifndef __BP_MOL_MOLGLCANVAS_H__
#define __BP_MOL_MOLGLCANVAS_H__

#include "wx.h"
#include <wx/glcanvas.h>
#include <wx/gdicmn.h>
#ifdef _DARWIN_
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif
#include "commons.h"
#include "abstractview.h"
#include "datatype.h"
#include "molviewdata.h"
#include "freetypefont.h"
/**
 * MolGLCanvas provides a 3D graphical view of a molecule.
 * In terms of the Model-View architecture,
 * we consider CtrlInst the model and MolGLCanvas a view of this model.
 */
class MolGLCanvas : public wxGLCanvas  {
public:
    MolGLCanvas(wxWindow *parent, ViewType viewType, wxWindowID id = -1, const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize, long style = 0, const wxString &name = wxT("GLCanvas"));
    virtual ~MolGLCanvas();

    // Methods
    void InitFont(void);
    void FreeFont(void);
    void InitMaterials(void);
    void Draw(void);
    void Paint(wxDC &dc);
    void PaintCanvas(wxDC &dc);

    void SetMolData(MolViewData *viewData);
    MolViewData *GetMolData() const;
    void ClearSelectedConnGroups(void);
    void InitGL(int subWinCount = 1, int subWinIndex = 0);

protected:
    void SetCanvasBgColor(void);;
    void SetProjectionMatrix(bool isInitViewport = false, int subWinCount = 1, int subWinIndex = 0);

    void DrawCtrlInst(CtrlInst *pCtrlInst, ActionType actionType, GLenum mode);
    void DrawBallSpoke(CtrlInst *pCtrlInst, ActionType actionType, GLenum mode);
    void DrawViewVolume(void);
    bool GetBondModelType(CtrlInst *pCtrlInst, LayerType atom1Layer, LayerType atom2Layer, ModelType *pModelType);

    /**
     * Take a point and figure out which is the closest primitive under that point.
    * @param pCtrlInst
     * @return true is selected or false if nothing.
     */
    bool GetSelectedBond(CtrlInst *pCtrlInst, int ctrlInstName, ActionType actionType, long xCoord, long yCoord, int atomIndex[2]);
    bool GetSelectedAtom(CtrlInst *pCtrlInst, int ctrlInstName, ActionType actionType, long xCoord, long yCoord, int &atomIndex);
    void GetSelectedElement(CtrlInst *pCtrlInst, int ctrlInstName, ActionType actionType, long xCoord, long yCoord, wxIntArray &intArray);

    void Rotate(CtrlInst *pCtrlInst, AtomPosition3F rotAngle);
    void Translate(CtrlInst *pCtrlInst, AtomPosition3F disp);

private:
    DECLARE_EVENT_TABLE()
    void OnPaint(wxPaintEvent &event);
    void OnSize(wxSizeEvent &event);
    void OnEraseBackground(wxEraseEvent &event);
    void OnIdle(wxIdleEvent &event);

    void OnLeftMouse(wxMouseEvent &event);
    void OnLeftMouseUp(wxMouseEvent &event);
    //-----
    void OnMiddleMouse(wxMouseEvent &event);
    void OnMiddleMouseUp(wxMouseEvent &event);
    //------

    void OnLeftMouseDClick(wxMouseEvent &event);
    void OnRightMouse(wxMouseEvent &event);
    void OnRightMouseUp(wxMouseEvent &event);
    void OnMouseMove(wxMouseEvent &event);
    void DoScale(bool enlarge);
    void OnMouseWheel(wxMouseEvent &event);
    void OnKeyChar(wxKeyEvent &event);
    void OnCanvasUpdateUI(wxUpdateUIEvent &event);

private:
    bool m_isGLInitialized;
    ViewType m_viewType;
    wxIntArray m_selectedConnGroups;;
    MolViewData *m_pMolData;
    wxGLContext *m_pContext;
public:
    FreeTypeFont *m_FreeTypeFont;
};

#endif
