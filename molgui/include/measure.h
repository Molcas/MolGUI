/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MEASURE_H__
#define __BP_MOL_MEASURE_H__

#include "commons.h"
#include "chematom.h"

typedef enum _measureInfo {
    MEASURE_SUCCESS,
    MEASURE_ERROR_IN_MEMORY_ALLOCATION,
    MEASURE_ERROR_IN_BONDLENGTH,
    MEASURE_ERROR_IN_BONDANGLE,
    MEASURE_ERROR_IN_DIHEDRAL,
    DIHEDRAL_CAN_NOT_DEFINE,
}MeasureInfo;

/* define some precision number used in the measure utility
   it's double type */
#define MEASURE_PREC   0.0000000001


/********************************************************
groups of function related to the distance calculation.
defined in the measure/bonddistance.cpp or distance.cpp
**********************************************************/
double MeasureBondDistance(int atom1Index, int atom2Index, const AtomBondArray& atomBondArray);

MeasureInfo ChangeBondDistance(double distance, int atomStill, int atomMove, AtomBondArray& atomBondArray, bool methodSwitch);

void ChangeBondDistanceCalculation (double distance, int atomStill, int atomMove,  MovingAtoms * movingAtoms, int movingAtomArrayLength, AtomBondArray& atomBondArray);


/********************************************************
groups of function related to the angle calculation.
defined in the measure/bondangle.cpp or angle.cpp
**********************************************************/
double MeasureBondAngle(double oldAngle, int atom1Index, int atom2Index, int atom3Index, const AtomBondArray& atomBondArray);

MeasureInfo ChangeBondAngle(double angle, double oldAngle, int atomStill, int atomAxis, int atomMove, AtomBondArray& atomBondArray, bool methodSwitch);

void ChangeBondAngleCalculation(double angle, double oldAngle, int atomStill, int atomAxis,int atomMove,  MovingAtoms * movingAtoms, int movingAtomArrayLength, AtomBondArray& atomBondArray);



/********************************************************
groups of function related to the dihedral calculation.
defined in the measure/bonddihedral.cpp or dihedral.cpp
**********************************************************/
MeasureInfo JudgeDihedral(int atom1Index, int atom2Index, int atom3Index, int atom4Index, const AtomBondArray& atomBondArray);

MeasureInfo MeasureDihedral(double & dihedral, int atom1Index, int atom2Index, int atom3Index, int atom4Index, const AtomBondArray& atomBondArray);

MeasureInfo ChangeDihedral(double dihedral, int atomStill, int atomAxis1, int atomAxis2, int atomMove, AtomBondArray& atomBondArray, bool methodSwitch);

void ChangeDihedralCalculation(double dihedral, int atomStill, int atomAxis1, int atomAxis2, int atomMove,  MovingAtoms * movingAtoms, int movingAtomArrayLength, AtomBondArray& atomBondArray);


#endif
