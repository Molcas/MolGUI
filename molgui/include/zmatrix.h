/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_ZMATRIX_H__
#define __BP_MOL_ZMATRIX_H__

#include "wx.h"
#include "chematom.h"
#include "measure.h"
#include "commons.h"

class ZMatrixItem {
 public:
  ZMatrixItem();
 public:
  wxString atomStringIndex; //SONG add
  wxString valueString; //SONG add
  int atomIndex;
  double value;
};

class ZMatrix {
public:
        ZMatrix();

 public:
  ZMatrixItem bond;
  ZMatrixItem angle;
  ZMatrixItem dihedral;
  wxString label; //SONG add
  int bondCount;
  ChemBond bonds[ATOM_MAX_BOND_COUNT];

};

WX_DECLARE_OBJARRAY(ZMatrix, ZMatrixArray);


void IntToXYZ (AtomBondArray& atomBondArray,ZMatrixArray& zmatrizArray);


void LoadInXYZ (AtomBondArray& atomBondArray, ZMatrixArray& zmatrizArray, int atom);

void FetchZMatrix (const AtomBondArray& atomBondArray, ZMatrix& intCor, int rootAtom);



#endif
