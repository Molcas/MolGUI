/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_GLPICTURE_H__
#define __BP_MOL_GLPICTURE_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "wx.h"
#ifdef _DARWIN_
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
#else
    #include <GL/gl.h>
    #include <GL/glu.h>
#endif
#include <wx/glcanvas.h>
#include <wx/image.h>

typedef enum _pictureFormatFilter {
    PICTURE_FORMAT_BMP = 0,
    PICTURE_FORMAT_XPM,
        PICTURE_FORMAT_JPEG,
        PICTURE_FORMAT_PNG,
    PICTURE_FORMAT_ALL
}PictureFormatFilter;

class GLPicture
{
public:
        bool GetOrFindMaskColour(unsigned char *r, unsigned char *g, unsigned char *b) const;
        unsigned char GetMaskRed(void) const;
        unsigned char GetMaskGreen(void) const;
        unsigned char GetMaskBlue(void) const;
        int GetHeight(void) const;
        wxImage* GetImage(void);
        void SetRGB(wxRect & rect, unsigned char red, unsigned char green, unsigned char blue);
        void SetRGB(int x, int y, unsigned char red, unsigned char green, unsigned char blue);
        void SetPalette(const wxPalette& palette);
        void SetOption(const wxString& name, int value);
        void SetOption(const wxString& name, const wxString& value);
        bool SetMaskFromImage(const wxImage& mask, unsigned char mr, unsigned char mg, unsigned char mb);
        void SetMaskColour(unsigned char red, unsigned char green, unsigned char blue);
        void SetMask(bool hasMask = true);
        wxBitmap ConvertToBitmap(void) const;
        void InitImage(wxGLCanvas* canvas);
        void InitImage(void);
        bool SaveFile(wxOutputStream& stream, const wxString& mimetype) const;
        bool SaveFile(wxOutputStream& stream, int type) const;
        bool SaveFile(const wxString& name) const;
        bool SaveFile(const wxString& name, const wxString& mimetype) const;
        bool SaveFile(const wxString& name, int type,wxRect rect) const;
        GLPicture(wxGLCanvas* canvas);
        GLPicture(){glCanvas=NULL;image=NULL;}
        virtual ~GLPicture();
private:
        wxImage* image;
        wxGLCanvas* glCanvas;
        wxGLContext* glContext;
        unsigned char* imageData;
private:
        void WriteBits(wxString fileName,unsigned char* data,long total);
        bool ReadGLPixels(void);
};
wxString AppendPictureFileType(wxString fileName, int filterIndex);

#endif // !defined(__GLPicture_H__)
