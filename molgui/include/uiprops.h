/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_UIPROPS_H__
#define __BP_MOL_UIPROPS_H__

#include "wx.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>

#include "commons.h"
#include "ctrlinst.h" //SONG add it for workshop2009

#define LIGHT_COUNT_MAX 2

class UIColor {
public:
    UIColor();
    UIColor(float cr, float cg, float cb);
    UIColor(float cr, float cg, float cb, float calpha);
    void ToColor(wxString colorStr);
    wxString ToString(void) const;
        void SetColor(const wxColour& color);
        wxColor GetColor(void) const;

public:
    float r, g, b, alpha;
};

class OpenGLProps {
public:
    OpenGLProps();
    wxString LoadProps(wxFileInputStream& fis, wxTextInputStream& tis);
    wxColour LoadColour();

public:
    UIColor previewBgColor;
    UIColor mainBgColor;
    UIColor mainDisableBgColor;
    int lightSourceCount;
    float lightPositions[LIGHT_COUNT_MAX][4];

    int staticObjectSlices;
    int staticObjectStacks;
    int movingObjectSlices;
    int movingObjectStacks;
    int isMovingWithBonds;

    UIColor globalLabelColor;
    UIColor atomLabelColor;
    UIColor bondColor;
    float bondRadius;
    UIColor selectionColor;
    UIColor manuSelectionColor;
    wxString labelFont;
    UIColor axesColor;
};

class MenuProps {
public:
    MenuProps();
    wxString LoadProps(wxFileInputStream& fis, wxTextInputStream& tis);
public:
    int isToolbarStyleText;
    int isToolbarStyleFlat;
};

class PanelProps {
public:
    PanelProps();
    wxString LoadProps(wxFileInputStream& fis, wxTextInputStream& tis);
public:

};

class ChemProps {
public:
        ChemProps();
        wxString LoadProps(wxFileInputStream& fis, wxTextInputStream& tis);
        ModelType GetLayerModelType(LayerType layerType);
        ModelType GetLayerModelType(wxString strModel);

public:
        ModelType layerModelLow;
        ModelType layerModelMedium;
        ModelType layerModelHigh;
};

class UIProps {
public:
    void LoadProps(void);
    void SaveColour(wxColour);
public:
    OpenGLProps openGLProps;
    MenuProps menuProps;
    PanelProps panelProps;
    ChemProps chemProps;
};

//SONG add it for workshop2009
class UIData {
public:
        bool IsFileLoaded(const wxString& strFileName);
        int GetCtrlInstCount(void) const;
        bool IsAllowAddCtrlInst(void);
        CtrlInst* GetCtrlInst(int index);

private:
        CtrlInstArray m_ctrlInstArray;

};


UIProps* GetUIProps(void);
UIData* GetUIData(void);
void FreeUIData();


#endif
