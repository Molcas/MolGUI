/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

/*
** symmetryutil.h
**
** Made by liufenglai
** Login   <liufenglai@liufenglai-desktop>
**
** Started on  Sat Mar 22 16:03:44 2008 liufenglai
** Last update Sat Mar 22 16:03:44 2008 liufenglai
*/


#ifndef __BP_MOL_SYMMETRYUTILS_H__
#define __BP_MOL_SYMMETRYUTILS_H__

#include "commons.h"
#include "chematom.h"
#include "atommanuutils.h"
#include "measureutils.h"

/* the return state of the function */
typedef enum _symInfor {
        SYMMETRY_ERROR_IN_MEMORY_ALLOCATION,
        SYMMETRY_ERROR_IN_REFLECTION,
        SYMMETRY_SUCCESS,
} SymInfor;


SymInfor SymmetryTranslation (int atomIndex, double symCoord[], AtomBondArray & atomBondArray, bool singleOrGroup);

SymInfor SymmetryInversion (int atomIndex, double symCoord[], AtomBondArray & atomBondArray, bool singleOrGroup);

SymInfor SymmetryRotation (int atomIndex, double symCoord[][3], double angle, AtomBondArray & atomBondArray, bool singleOrGroup);

SymInfor SymmetryReflection (int atomIndex, double symCoord[][3], AtomBondArray & atomBondArray, bool singleOrGroup);

#endif
