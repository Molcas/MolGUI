/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_MEASUREPNL_H__
#define __BP_MOL_MEASUREPNL_H__

#include <wx/spinctrl.h>

#include "wx.h"
#include <wx/listctrl.h>

#include "commons.h"
#include "measure.h"
#include "togglebitmapbutton.h"
#include "ctrlinst.h"

#define BP_SLIDER_PRECISION 1000.0

class MeasurePnl : public wxPanel {

private:
        DECLARE_EVENT_TABLE();

public:
        void ResetControls(void);
        void EnableControls(ActionType actionType);
    MeasurePnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30), long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panelmeasure"));
    void InitPanel(void);

    void SetMeasureOrConstrain(bool isMeasure);
    bool IsMeasureOrConstrain(void) const;

    bool IsMoveSingleAtom(void) const;

    void SetCtrlInst(CtrlInst* pCtrlInst);
    void ClearSelection(void);

    void AddSelectedIndex(int selectedAtomIndex);
    void AddSelectedIndex(int selectedAtomIndex[2]);

    static void ShowMeasureInfo(MeasureInfo mInfo);

    void ChangeBondValueByPageKey(bool isUp);

private:
    void OnTextEnter(wxCommandEvent& event);
    void DoTextValueChanged(void);
    void DoPositionChanged(int ctrlId, bool isSpinBtn);
    MeasureInfo DoBondChange(float value);
    void OnSliderScroll(wxScrollEvent& event);
    void OnReset(wxCommandEvent& event);
        void OnSpinButton(wxSpinEvent& event);
        void OnBtnConstrain(wxCommandEvent& event);

        void DoMeasure(void);
        void SetInitPositionValue(AtomPosition& atomPos);
        void SetPositionEnable(bool enable);
        void SetInitBondValue(float value);
        void UpdateMCType(int selectedMax);

        void UpdateConsFreezeStatus(void);
        void UpdateFreezeAtom(AtomPosition pos);
        void UpdateConstrainData(double value);

        void EnableEditors(bool enabled);
        void SetAngleRange(double & angle);

        void OnImportCons(wxCommandEvent & event);
        void OnClearCons(wxCommandEvent & event);
        void OnShowCons(wxCommandEvent & event);
private:
    wxCheckBox* m_pCkbMoveSingleAtom;
    wxStaticText* m_pStMCType;
    wxTextCtrl* m_pTcValue;
    wxStaticText* m_pStMin;
    wxStaticText* m_pStMax;
    wxSlider* m_pSlider;
    ToggleBitmapButton* m_pBtnConstrain;
    wxButton* m_pBtnReset;

        wxTextCtrl* m_pTcPoss[3];
        wxSpinButton* m_pSbPoss[3];
        wxBoxSizer* m_pBsPosition;
        wxBoxSizer* m_pBsBond;


        bool m_isError;
    bool m_isSelectionChanged;
    int m_mcType;
    wxString m_mcLabel;
    float m_initBondValue;
        AtomPosition m_initPos;
        bool m_isMeasure;
        bool m_cfStatus;
        CtrlInst* m_pCtrlInst;
        wxButton *m_pImportBtn;
        wxButton *m_pClearConsBtn;
        wxButton *m_pShowCons;
};

#include "simuproject.h"
class ConsDlg : public wxDialog{
public:
        ConsDlg(wxWindow* parent, SimuProject*   pProject=NULL,wxWindowID id = wxID_ANY, const wxString& title = wxT("Set Job Name"),
            const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
            long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER);
    ~ConsDlg();
public:
    wxString    GetJobName();
private:
    DECLARE_EVENT_TABLE();
 void OnLcActiveItem(wxListEvent& event);
        void OnButton(wxCommandEvent& event);
        void OnEnter(wxCommandEvent& event);
        void OnText(wxCommandEvent& event);
private:
        wxListBox* m_lcJobName;
        SimuProject* m_pProject;
        wxString m_txtJobName;
};

#endif
