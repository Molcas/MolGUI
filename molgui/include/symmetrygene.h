/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_SYMMETRYGENERATE_H__
#define __BP_MOL_SYMMETRYGENERATE_H__

#include "commons.h"
#include "measure.h"

/* the return state of the main function */
typedef enum _symGeneInfor {
  SUCCESS,
  MEMORY_DISTRIBUTION_FAILED,
  CAN_NOT_OPEN_RESULT_FILE,
} SymGeneInfor;


/********************************************************************
 this type of data struture used to preserve all the position
 information of atoms in the same kind.
 ********************************************************************/
typedef struct _symAtoms {
    int label;                        /* position in the atomBondArray */
    double x;
    double y;
    double z;
} SymAtoms;


/********************************************************************
 here is the basic data type needed to process in the whole
 symmetry generating program. the whole data is expressed by the
 list, each node in the list indicates one type of atom, such as "C"
 or "H".
*********************************************************************/
typedef struct _symGene {

  ElemId atomId;                /* the atom label */
  int number;                        /* how much atoms in this type */
  /* stores all the position information of atoms in this kind*/
  SymAtoms * atomPosArray;
  struct _symGene * next;        /* the pointer to link the next node */

} SymGene;



SymGeneInfor SymmetryGene (const AtomBondArray & atomBondArray);

#endif
