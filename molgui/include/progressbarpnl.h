/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_MOL_PROGRESSBARPNL_H__
#define __BP_MOL_PROGRESSBARPNL_H__

#include <wx/process.h>
#include "wx.h"
#include "ctrlinst.h"
class MMIProcessListener {
        public:
                virtual ~MMIProcessListener() {}
                virtual void OnProcessTerminated(int status) = 0;
};
class MMExecPipedProcess : public wxProcess {
        public:
                MMExecPipedProcess(MMIProcessListener* listener);
                ~MMExecPipedProcess();
                virtual void OnTerminate(int pid, int status);

        private:
                MMIProcessListener *m_listener;
};





class ProgressBarPnl : public wxPanel, public MMIProcessListener {

private:
        DECLARE_EVENT_TABLE();

public:
    ProgressBarPnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30), long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panelmeasure"));
    void InitPanel(void);
        void SetCtrlInst(CtrlInst* pCtrlInst);
        virtual void OnProcessTerminated(int status);
        bool RunProcess(wxString cmd);

private:
        void OnTimerIdle(wxTimerEvent& event);
        void OnBtnStop(wxCommandEvent& event);
    wxStaticText* m_pStatus;
        wxGauge* m_pGaugeProgress;
    wxButton* m_pBtnStop;
        CtrlInst* m_pCtrlInst;
        wxTimer timerIdleWakeUp;
        long m_processId;
        MMExecPipedProcess* m_pipedProcess;
        int m_break;
};

#endif
