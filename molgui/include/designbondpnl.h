/***********************************************************************
* This file is part of MolGUI.                                         *
*                                                                      *
* MolGUI is free software; you can redistribute it and/or modify it    *
* under the terms of the GNU Lesser General Public License, v. 2.1.    *
* MolGUI is distributed in the hope that it will be useful, but it is  *
* provided "as is" and without any express or implied warranties.      *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
***********************************************************************/

#ifndef __BP_DESIGNBONDPNL_H__
#define __BP_DESIGNBONDPNL_H__

#include "wx.h"
#include "commons.h"
#include "togglebitmapbutton.h"
#include "ctrlinst.h"
#define BOND_TYPE_COUNT_NEW 6

class DesignBondPnl : public wxPanel {

private:
        DECLARE_EVENT_TABLE();

public:
    DesignBondPnl(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(wxDefaultSize.GetWidth(), 30), long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panel design bond"));

    void SetCtrlInst(CtrlInst* pCtrlInst);
    void ClearSelection(void);
    void InitPanel(void);
    void OnReset(wxCommandEvent &event);
    void OnToggleButton(wxCommandEvent& event);
    void AddSelectedIndex(int selectedAtomIndex);
   // void AddSelectedIndex(int selectedAtomIndex[2]);

private:
        enum {
            CTRL_ID_TGL_BOND_TYPE_START = 1221,
            CTRL_ID_TGL_BOND_TYPE_END = 1230,
        };

    ToggleBitmapButton* m_pTglBmpBondTypes[BOND_TYPE_COUNT_NEW];
    CtrlInst* m_pCtrlInst;
    wxButton* m_pBtnReset;
    wxStaticText* m_pLbTitle;
        bool m_isSelectionChanged;

};

#endif
