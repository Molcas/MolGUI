#***********************************************************************
# This file is part of MolGUI.                                         *
#                                                                      *
# MolGUI is free software; you can redistribute it and/or modify it    *
# under the terms of the GNU Lesser General Public License, v. 2.1.    *
# MolGUI is distributed in the hope that it will be useful, but it is  *
# provided "as is" and without any express or implied warranties.      *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2019,2020, Gerardo Raggi                               *
#***********************************************************************

project (MolGUI C CXX)
cmake_minimum_required (VERSION 2.8.12)

# Prevent in-source builds
# If an in-source build is attempted, you will still need to clean up a few files manually.
set (CMAKE_DISABLE_SOURCE_CHANGES ON)
set (CMAKE_DISABLE_IN_SOURCE_BUILD ON)
get_filename_component (sourcedir "${CMAKE_SOURCE_DIR}" REALPATH)
get_filename_component (binarydir "${CMAKE_BINARY_DIR}" REALPATH)
if ("${sourcedir}" STREQUAL "${binarydir}")
  message (FATAL_ERROR "In-source builds in ${CMAKE_BINARY_DIR} are not "
                       "allowed, please remove ./CMakeCache.txt and ./CMakeFiles/, create a "
                       "separate build directory and run cmake from there."
  )
endif ()

# add_definitions (-D__MOLCAS__)

# options
# location of (Open)Molcas
set (MOLCAS "" CACHE PATH "(Open)Molcas directory.")
if ("${MOLCAS}" STREQUAL "")
  if (DEFINED ENV{MOLCAS})
    set (MOLCAS $ENV{MOLCAS})
  else ()
    set (MOLCAS "")
  endif ()
endif ()
if (NOT "${MOLCAS}" STREQUAL "")
  if (NOT IS_ABSOLUTE "${MOLCAS}")
    set (MOLCAS "${PROJECT_BINARY_DIR}/${MOLCAS}")
  endif ()
  get_filename_component (MOLCAS "${MOLCAS}" ABSOLUTE)
endif ()
set (MOLCAS "${MOLCAS}" CACHE PATH "(Open)Molcas directory." FORCE)
message ("-- OpenMolcas for MolGUI path: ${MOLCAS}")

# platform settings
if (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
  add_definitions (-D__LINUX__)
  if (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86")
    set (PLATFORM "LINUX")
  elseif (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
    set (PLATFORM "LINUX64")
  elseif (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "ia64")
    set (PLATFORM "LINUX64_IA")
  elseif (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "ppc_64")
    set (PLATFORM "PPC64")
  endif (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86")
elseif (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  add_definitions (-D_DARWIN_ -D__MAC__)
  set (PLATFORM "MacOS")
elseif (${CMAKE_SYSTEM_NAME} MATCHES "Windows")
  add_definitions (-D__WINDOWS__)
  if (CYGWIN)
    set (PLATFORM "CYGWIN")
  endif (CYGWIN)
endif ()

# Sources files
aux_source_directory (commons/src SRC_COMMONS)
aux_source_directory (job/src SRC_JOB)
aux_source_directory (ming/src SRC_MING)
aux_source_directory (molgui/src SRC_MOLGUI)
aux_source_directory (simuprj/src SRC_SIMUPRJ)

set (SRC_LIST ${SRC_COMMONS} ${SRC_JOB} ${SRC_MING} ${SRC_MOLGUI} ${SRC_SIMUPRJ})

# Include files
set (HEADER_FILES
    commons/include
    job/include
    ming/include
    molgui/include
    simuprj/include
)

# Dependencies
set (CMAKE_MODULE_PATH ${MolGUI_SOURCE_DIR}/cmake)
find_package (wxWidgets COMPONENTS std aui gl REQUIRED)
include (${wxWidgets_USE_FILE})
mark_as_advanced (wxWidgets_CONFIG_EXECUTABLE)
mark_as_advanced (wxWidgets_USE_DEBUG)
mark_as_advanced (wxWidgets_wxrc_EXECUTABLE)

set (OpenGL_GL_PREFERENCE "GLVND")

find_package (CURL REQUIRED)
find_package (OpenGL REQUIRED)
#find_package (GLUT REQUIRED)
find_package (LibSsh2 QUIET)
find_package (EXPAT REQUIRED)
find_package (Freetype REQUIRED)

set (EXE_NAME ${PROJECT_NAME}.exe)
set (RES_DIR share/MolGUI/res)

#--------------------------------------------------------------------------------
# For Apple set the icons file containing icons
if (APPLE)
  # set how it shows up in the Info.plist file
  set (MACOSX_BUNDLE_ICON_FILE app.png)
  # set where in the bundle to put the icns file
  set_source_files_properties (${CMAKE_CURRENT_SOURCE_DIR}/${RES_DIR}/app.png PROPERTIES MACOSX_PACKAGE_LOCATION Resources)
  # include the icns file in the target
  set (SRC_LIST ${SRC_LIST} ${CMAKE_CURRENT_SOURCE_DIR}/${RES_DIR}/app.png)
endif ()

add_executable (${EXE_NAME} MACOSX_BUNDLE ${SRC_LIST})

include_directories (${OPENGL_INCLUDE_DIRS}
                     ${GLUT_INCLUDE_DIRS}
                     ${CURL_INCLUDE_DIRS}
                     ${EXPAT_INCLUDE_DIRS}
                     ${FREETYPE_INCLUDE_DIRS}
                     ${FREETYPE_INCLUDE_DIRS}/freetype
                     ${HEADER_FILES}
)
target_link_libraries (${EXE_NAME}
                       ${wxWidgets_LIBRARIES}
                       ${CURL_LIBRARIES}
                       ${OPENGL_LIBRARIES}
                       ${EXPAT_LIBRARIES}
                       ${FREETYPE_LIBRARIES}
)

if (LIBSSH2_FOUND)
  include_directories (${LIBSSH2_INCLUDE_DIRS})
  target_link_libraries (${EXE_NAME}
                         ${LIBSSH2_LIBRARIES}
  )
  add_definitions (-D__LIBSSH2__)
  message ("-- Found LibSsh2")
else ()
  message ("NotFound LibSsh2 (perhaps HPC env?)")
endif ()

if (NOT CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
  set (PROJECT_BINARY_DIR ${CMAKE_BINARY_DIR}/${PROJECT_NAME})
endif ()

set (EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR})

set (BUNDLE_NAME ${CMAKE_INSTALL_PREFIX}/${EXE_NAME})
set (EXE_DIR ${PROJECT_BINARY_DIR})
set (EXE_INSTALL_DIR ${CMAKE_INSTALL_PREFIX})
set (XBIN_DIR ${CMAKE_SOURCE_DIR}/xbin/${PLATFORM})
set (XLIB_DIR ${CMAKE_SOURCE_DIR}/xlib/${PLATFORM})
set (SHARE_DIR ${PROJECT_BINARY_DIR}/share)

if (APPLE)
  set (BUNDLE_NAME ${CMAKE_INSTALL_PREFIX}/${EXE_NAME}.app)
  set (EXE_DIR ${PROJECT_BINARY_DIR}/${EXE_NAME}.app/Contents/MacOS)
  set (EXE_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/${EXE_NAME}.app/Contents/MacOS)
  set (SHARE_DIR ${EXE_NAME}.app/Contents/MacOS/share)
endif ()

#message ("CMAKE_BINARY_DIR+exe_name: ${CMAKE_BINARY_DIR}/bin/${EXE_NAME}")
#message ("CMAKE_INSTALL_PREFIX: ${CMAKE_INSTALL_PREFIX}")
#message ("CMAKE_SOURCE_DIR: ${CMAKE_SOURCE_DIR}")
#message ("EXE_NAME: ${EXE_NAME}")
#message ("EXE_DIR: ${EXE_DIR}")
#message ("HOME: $ENV{HOME}")
#message ("BUNDLE_NAME: ${BUNDLE_NAME}")

# Copy share directory into the build directory to be able to run the program without install
# and execute the initialize script
add_custom_target (initialize DEPENDS ${RES_DIR}/ming/data/keyword.xml)
add_custom_command (OUTPUT ${RES_DIR}/ming/data/keyword.xml
                    COMMAND ${CMAKE_COMMAND} -E copy_directory ${PROJECT_SOURCE_DIR}/share ${EXE_DIR}/share
                    COMMAND ${PROJECT_SOURCE_DIR}/initialize.sh "${MOLCAS}"
                    WORKING_DIRECTORY ${EXE_DIR}
                    COMMENT "Copying resource files"
                    VERBATIM
)
add_dependencies (${EXE_NAME} initialize)

if (CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
  #install the executable where it should be
  install (TARGETS ${EXE_NAME}
           RUNTIME DESTINATION .
           BUNDLE DESTINATION . COMPONENT Runtime
  )

  # Fixup bundle and install resources and xbin directories
  install (CODE "include (BundleUtilities)
                 set (BU_CHMOD_BUNDLE_ITEMS ON)

                 if (APPLE)
                   fixup_bundle (${BUNDLE_NAME} \"\" \"\")
                 endif ()

                 if (EXISTS ${XBIN_DIR}/)
                   file (INSTALL ${XBIN_DIR}
                         DESTINATION ${EXE_INSTALL_DIR}
                         RENAME xbin
                         USE_SOURCE_PERMISSIONS
                   )
                 endif ()

                 if (EXISTS ${XLIB_DIR}/)
                   file (INSTALL ${XLIB_DIR}
                         DESTINATION ${EXE_INSTALL_DIR}
                         RENAME xlib
                         USE_SOURCE_PERMISSIONS
                   )
                 endif ()

                 file (INSTALL ${SHARE_DIR}
                       DESTINATION ${EXE_INSTALL_DIR}
                       RENAME share
                       USE_SOURCE_PERMISSIONS
                 )"
  )
else ()
  install (DIRECTORY
           ${PROJECT_BINARY_DIR}
           USE_SOURCE_PERMISSIONS
           DESTINATION ${CMAKE_INSTALL_PREFIX}
  )
endif ()

# Package generation
set (MAJOR_VERSION "0")
set (MINOR_VERSION "1")
set (PATCH_VERSION "0")

set (GENERATORS "TGZ")
if (APPLE)
  set (GENERATORS ${GENERATORS} "DragNDrop")
endif (APPLE)

if (EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
  include (InstallRequiredSystemLibraries)

  set (CPACK_SET_DESTDIR "on")
  set (CPACK_PACKAGING_INSTALL_PREFIX "/")
  set (CPACK_GENERATOR "TGZ")

  set (CPACK_PACKAGE_DESCRIPTION "GUI for MOLCAS")
  set (CPACK_PACKAGE_DESCRIPTION_SUMMARY "Graphical User Interface for MOLCAS")
  set (CPACK_PACKAGE_VENDOR "Uppsala University")
  set (CPACK_PACKAGE_CONTACT "graggi@gmail.com")
  set (CPACK_PACKAGE_VERSION_MAJOR "${MAJOR_VERSION}")
  set (CPACK_PACKAGE_VERSION_MINOR "${MINOR_VERSION}")
  set (CPACK_PACKAGE_VERSION_PATCH "${PATCH_VERSION}")
  set (CPACK_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}")
  set (CPACK_SOURCE_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}")
  set (CPACK_PACKAGE_INSTALL_DIRECTORY "${CMAKE_PROJECT_NAME}")

  # autogenerate dependency information
  set (CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)

  # dependencies
  set (CPACK_DEBIAN_PACKAGE_DEPENDS " libwxgtk3.0-0 , libexpat1 , libcurlpp0 , libssh2-1 , libfreetype6 , libglu1-mesa")

  set (CPACK_COMPONENTS_ALL Libraries ApplicationData)
  include (CPack)

endif ()
