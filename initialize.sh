#!/bin/bash
#***********************************************************************
# This file is part of MolGUI.                                         *
#                                                                      *
# MolGUI is free software; you can redistribute it and/or modify it    *
# under the terms of the GNU Lesser General Public License, v. 2.1.    *
# MolGUI is distributed in the hope that it will be useful, but it is  *
# provided "as is" and without any express or implied warranties.      *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#***********************************************************************

resdir='share/MolGUI/res'

MOLCAS_HOME=$1

if [ ! -d "$1" ]; then
  echo "No valid (Open)Molcas provided."
  exit 1
else
  echo "Getting data from $MOLCAS_HOME"
fi

if type -p pymolcas >& /dev/null ; then
  MOLCAS_BASIS="pymolcas help_basis -bw"
elif type -p molcas >& /dev/null ; then
  MOLCAS_BASIS="molcas help -bw basis"
else
  echo "No driver found"
  exit 1
fi

mkdir -p $resdir/ming/data

cp $MOLCAS_HOME/data/inputs.tpl $resdir/ming/data/
cp $MOLCAS_HOME/data/keyword.xml $resdir/ming/data/
if type -p xmllint >& /dev/null ; then
  xmllint $resdir/ming/data/keyword.xml --noout
fi
if [ -f $MOLCAS_HOME/data/saved.db ]; then
  cp $MOLCAS_HOME/data/saved.db $resdir/ming/data/
else
  echo -e "gv\ntext" > $resdir/ming/data/saved.db
fi

mkdir -p $resdir/ming/basis

export MOLCAS=$MOLCAS_HOME
for x in $(less $resdir/ming/data/elesyms.dat); do
  # echo Generating basis set for element $x
  $MOLCAS_BASIS $x > $resdir/ming/basis/$x.basis
done
